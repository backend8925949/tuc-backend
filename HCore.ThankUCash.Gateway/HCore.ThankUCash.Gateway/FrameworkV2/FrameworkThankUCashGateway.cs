//==================================================================================
// FileName: FrameworkThankUCashGateway.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Actor;
using HCore.ThankUCash.Gateway.Core;
using HCore.ThankUCash.Gateway.IntegrationV2;
using HCore.ThankUCash.Gateway.ObjectV2;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Gateway.Helper.HelperGateway;

namespace HCore.ThankUCash.Gateway.FrameworkV2
{
    public class FrameworkThankUCashGateway
    {
        #region References 
        OCampaignProcessor _OCampaignProcessor;
        ManageCoreUserAccess _ManageCoreUserAccess;
        ManageCoreTransaction _ManageCoreTransaction;
        HCUAccountOwner _HCUAccountOwner;
        HCoreContext _HCoreContext;
        #endregion
        #region Objects
        OCoreTransaction.Request _CoreTransactionRequest;
        OThankUGateway.Response _GatewayResponse;
        OUserInfo _OUserInfo;
        OAppProfile.Request _AppProfileRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        OThankUGateway.Request _GatewayRequest;
        OAmountDistribution _AmountDistribution;
        FrameworkSoftcom _FrameworkSoftcom;
        #endregion
        private OGatewayInfo GetGatewayInfo(OThankUGateway.Request _Request)
        {
            using (_HCoreContext = new HCoreContext())
            {
                OGatewayInfo _OGatewayInfo = null;
                if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                {
                    _OGatewayInfo = _HCoreContext.HCUAccount
                      .Where(x => x.AccountTypeId == UserAccountType.Merchant &&
                                  x.StatusId == HelperStatus.Default.Active &&
                                  x.AccountCode == _Request.MerchantId)
                      .Select(x => new OGatewayInfo
                      {
                          MerchantId = x.Id,
                          MerchantDisplayName = x.DisplayName,
                          AcquirerId = x.OwnerId,
                          AcquirerAccountTypeId = x.Owner.AccountTypeId
                      }).FirstOrDefault();
                    _OGatewayInfo.PsspId = _Request.UserReference.AccountId;
                }
                else if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                {
                    //_OGatewayInfo = _HCoreContext.HCUAccountOwner
                    //                        .Where(x => x.Account.AccountTypeId == UserAccountType.TerminalAccount
                    //                               && x.Account.User.Username == _Request.TerminalId
                    //                               && x.Owner.AccountTypeId == UserAccountType.Merchant
                    //                               && x.Account.StatusId == HelperStatus.Default.Active
                    //                               && x.StatusId == HelperStatus.Default.Active
                    //                               && x.Owner.StatusId == HelperStatus.Default.Active
                    //                              )
                    //                           .Select(x => new OGatewayInfo
                    //                           {
                    //                               TerminalId = x.Account.Id,
                    //                               MerchantId = x.Owner.Id,
                    //                               MerchantDisplayName = x.Owner.DisplayName,
                    //                               AcquirerId = x.Owner.OwnerId,
                    //                               AcquirerAccountTypeId = x.Owner.Owner.AccountTypeId,
                    //                           }).FirstOrDefault();
                    _OGatewayInfo = _HCoreContext.TUCTerminal
                                       .Where(x => x.IdentificationNumber == _Request.TerminalId
                                              && x.StatusId == HelperStatus.Default.Active
                                              && x.Provider.StatusId == HelperStatus.Default.Active
                                             )
                                          .Select(x => new OGatewayInfo
                                          {
                                              TerminalId = x.Id,
                                              MerchantId = (long)x.MerchantId,
                                              MerchantDisplayName = x.Merchant.DisplayName,
                                              AcquirerId = x.AcquirerId,
                                              AcquirerAccountTypeId = x.Acquirer.AccountTypeId,
                                          }).FirstOrDefault();

                    if (_OGatewayInfo != null)
                    {
                        _OGatewayInfo.PtspId = _Request.UserReference.AccountId;
                        var TerminalAcquirer = _HCoreContext.HCUAccountOwner
                                                                .Where(x => x.AccountId == _OGatewayInfo.TerminalId
                                                                       && x.Owner.AccountTypeId == UserAccountType.Acquirer
                                                                       && x.StatusId == HelperStatus.Default.Active
                                                                       && x.Owner.StatusId == HelperStatus.Default.Active
                                                                      ).Select(x => new
                                                                      {
                                                                          OwnerId = x.OwnerId,
                                                                          OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                                      }).FirstOrDefault();
                        if (TerminalAcquirer != null)
                        {
                            _OGatewayInfo.AcquirerId = TerminalAcquirer.OwnerId;
                            _OGatewayInfo.AcquirerAccountTypeId = TerminalAcquirer.OwnerAccountTypeId;
                        }
                        var TerminalStore = _HCoreContext.HCUAccountOwner
                                                               .Where(x => x.AccountId == _OGatewayInfo.TerminalId
                                                                      && x.Owner.AccountTypeId == UserAccountType.MerchantStore
                                                                      && x.StatusId == HelperStatus.Default.Active
                                                                      && x.Owner.StatusId == HelperStatus.Default.Active
                                                                     ).Select(x => new
                                                                     {
                                                                         StoreId = x.OwnerId,
                                                                         StoreStatusId = x.Owner.StatusId,
                                                                     }).FirstOrDefault();
                        if (TerminalStore != null)
                        {
                            _OGatewayInfo.StoreId = TerminalStore.StoreId;
                            _OGatewayInfo.StoreStatusId = TerminalStore.StoreStatusId;
                        }
                    }
                }
                else if (_Request.UserReference.AccountTypeId == UserAccountType.Merchant)
                {
                    _OGatewayInfo = _HCoreContext.HCUAccount
                     .Where(x => x.StatusId == HelperStatus.Default.Active && x.Id == _Request.UserReference.AccountId)
                     .Select(x => new OGatewayInfo
                     {
                         MerchantId = x.Id,
                         MerchantDisplayName = x.DisplayName,
                         AcquirerId = x.OwnerId,
                         AcquirerAccountTypeId = x.Owner.AccountTypeId
                     }).FirstOrDefault();
                }
                if (_OGatewayInfo != null)
                {
                    if (!string.IsNullOrEmpty(_Request.CashierId))
                    {
                        var CashierDetails = _HCoreContext.HCUAccount
                            .Where(x => x.DisplayName == _Request.CashierId
                            && x.OwnerId == _OGatewayInfo.MerchantId
                            && x.AccountTypeId == UserAccountType.MerchantCashier
                            ).Select(x => new
                            {
                                Id = x.Id,
                                StatusId = x.StatusId,
                            }).FirstOrDefault();
                        if (CashierDetails != null)
                        {
                            _OGatewayInfo.CashierId = CashierDetails.Id;
                            _OGatewayInfo.CashierStatusId = CashierDetails.StatusId;
                        }
                    }
                    else
                    {
                        var CashierDetails = _HCoreContext.HCUAccount
                          .Where(x => x.DisplayName == "0001"
                          && x.OwnerId == _OGatewayInfo.MerchantId
                          && x.AccountTypeId == UserAccountType.MerchantCashier
                          ).Select(x => new
                          {
                              Id = x.Id,
                              StatusId = x.StatusId,
                          }).FirstOrDefault();
                        if (CashierDetails != null)
                        {
                            _OGatewayInfo.CashierId = CashierDetails.Id;
                            _OGatewayInfo.CashierStatusId = CashierDetails.StatusId;
                        }
                    }
                    if (_OGatewayInfo.AcquirerAccountTypeId == UserAccountType.Merchant
                    || _OGatewayInfo.AcquirerAccountTypeId == UserAccountType.Acquirer)
                    {

                    }
                    else
                    {
                        _OGatewayInfo.AcquirerId = 0;
                    }
                    _HCoreContext.Dispose();
                    if (_OGatewayInfo.AcquirerId == 0)
                    {
                        _OGatewayInfo.AcquirerId = SystemAccounts.ThankUCashMerchant;
                        _OGatewayInfo.AcquirerAccountTypeId = UserAccountType.Merchant;
                    }
                    else if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                    {
                        if (_OGatewayInfo.StoreId != 0)
                        {
                            _OGatewayInfo.TransactionIssuerId = _OGatewayInfo.StoreId;
                            _OGatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.MerchantStore;
                        }
                        else
                        {
                            _OGatewayInfo.TransactionIssuerId = SystemAccounts.ThankUCashMerchant;
                            _OGatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.Merchant;
                        }

                    }
                    return _OGatewayInfo;
                }
                else
                {
                    return null;
                }
            }
        }
        internal OUserInfo GetUserInfo(OThankUGateway.Request _Request, OGatewayInfo _GatewayInfo, bool CreateUser)
        {
            _OUserInfo = new OUserInfo();
            using (_HCoreContext = new HCoreContext())
            {
                if (!string.IsNullOrEmpty(_Request.CardNumber) && !string.IsNullOrEmpty(_Request.TagNumber))
                {
                    if (!string.IsNullOrEmpty(_Request.CardNumber) && !string.IsNullOrEmpty(_Request.TagNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber
                            && x.SerialNumber == _Request.TagNumber)
                            .Select(x => new OTUInfo
                            {
                                CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.CardNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber)
                            .Select(x => new OTUInfo
                            {
                                CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.TagNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.TagNumber)
                            .Select(x => new OTUInfo
                            {
                                CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    string AppUserCardId = _Request.MobileNumber;
                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    string AppUserLoginId = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    var UserAccount = _HCoreContext.HCUAccount
                    .Where(x => x.User.Username == AppUserLoginId &&
                                x.AccountTypeId == UserAccountType.Appuser)
                                                   .Select(x => new OUserInfo
                                                   {
                                                       UserAccountId = x.Id,
                                                       DisplayName = x.DisplayName,
                                                       Pin = x.AccessPin,
                                                       CreatedById = x.CreatedById,
                                                       CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                       OwnerId = x.OwnerId,
                                                       OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                       EmailAddress = x.EmailAddress,
                                                       AccountStatusId = x.StatusId,
                                                       MobileNumber = x.MobileNumber,
                                                       AccountNumber = x.MobileNumber,
                                                   }).FirstOrDefault();
                    if (UserAccount != null)
                    {
                        _OUserInfo.AccountNumber = UserAccount.AccountNumber;
                        _OUserInfo.MobileNumber = UserAccount.MobileNumber;
                        _OUserInfo.DisplayName = UserAccount.DisplayName;
                        _OUserInfo.UserAccountId = UserAccount.UserAccountId;
                        _OUserInfo.Pin = UserAccount.Pin;
                        _OUserInfo.EmailAddress = UserAccount.EmailAddress;
                        _OUserInfo.AccountStatusId = UserAccount.AccountStatusId;
                        if (UserAccount.OwnerId != null)
                        {
                            _OUserInfo.IssuerId = (long)UserAccount.OwnerId;
                            _OUserInfo.IssuerAccountTypeId = (long)UserAccount.OwnerAccountTypeId;
                        }
                        if (UserAccount.CreatedById != null)
                        {
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                            {
                                _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            }
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.Acquirer)
                            {
                                _OUserInfo.IssuerId = (long)UserAccount.CreatedById;
                                _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                            }
                        }
                        if (_OUserInfo.IssuerId == 0)
                        {
                            _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                            _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                        }
                        return _OUserInfo;
                    }
                    else
                    {
                        if (CreateUser == true)
                        {
                            _AppProfileRequest = new OAppProfile.Request();
                            if (_GatewayInfo != null && _GatewayInfo.MerchantId != 0)
                            {
                                _AppProfileRequest.OwnerId = _GatewayInfo.MerchantId;
                            }
                            if (_GatewayInfo != null && _GatewayInfo.StoreId != 0)
                            {
                                _AppProfileRequest.SubOwnerId = _GatewayInfo.StoreId;
                            }
                            if (_GatewayInfo != null && _GatewayInfo.TerminalId != 0)
                            {
                                _AppProfileRequest.CreatedById = _GatewayInfo.TerminalId;
                            }
                            _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                            _AppProfileRequest.UserReference = _Request.UserReference;
                            _ManageCoreUserAccess = new ManageCoreUserAccess();
                            OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                            if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                            {
                                if (_AppUserCreateResponse.StatusId != 0)
                                {
                                    _OUserInfo.AccountStatusId = _AppUserCreateResponse.StatusId;
                                }
                                else
                                {
                                    _OUserInfo.AccountStatusId = HelperStatus.Default.Active;
                                }

                                _OUserInfo.UserAccountId = _AppUserCreateResponse.AccountId;
                                if (_GatewayInfo != null && _GatewayInfo.MerchantId != 0)
                                {
                                    _OUserInfo.IssuerId = _GatewayInfo.MerchantId;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }

                    }
                }
                else
                {
                    return null;
                }
            }
        }
        private OAmountDistribution GetAmountDistribution(OThankUGateway.Request _UserRequest, OGatewayInfo _GatewayInfo, OUserInfo _UserInfo)
        {
            // Rewards allowed for active and suspended users  - VDisc with suraj suepkar - 11-05-2019 10:46 IST
            if (_UserInfo.AccountStatusId == HelperStatus.Default.Active || _UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
            {
                _AmountDistribution = new OAmountDistribution();
                double InvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                double ReferenceInvoiceAmount = InvoiceAmount;
                bool IsThankUCashCustomer = false;
                if (_UserInfo.MobileNumber != _UserInfo.DisplayName)
                {
                    IsThankUCashCustomer = true;
                }
                bool IsThankUCashEnabled = false;
                long ThankUCashPlusIsEnable = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", _GatewayInfo.MerchantId));
                if (ThankUCashPlusIsEnable != 0)
                {
                    IsThankUCashEnabled = true;
                }
                else
                {
                    IsThankUCashEnabled = false;
                }
                long RewardTucCustomersOnly = Convert.ToInt64(HCoreHelper.GetConfiguration("tucuserrewardonly", _GatewayInfo.MerchantId));
                if (RewardTucCustomersOnly == 1 && IsThankUCashCustomer == false)
                {
                    _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                    _AmountDistribution.AllowAcquirerSettlement = "0";
                    _AmountDistribution.InvoiceAmount = InvoiceAmount;
                    _AmountDistribution.ReferenceInvoiceAmount = 0;
                    _AmountDistribution.RewardPercentage = 0;
                    _AmountDistribution.RewardAmount = 0;
                    _AmountDistribution.RewardCommission = 0;
                    _AmountDistribution.MerchantAmount = 0;
                    _AmountDistribution.User = 0;
                    _AmountDistribution.UserPercentage = 0;
                    _AmountDistribution.Ptsp = 0;
                    _AmountDistribution.PtspPercentage = 0;
                    _AmountDistribution.Pssp = 0;
                    _AmountDistribution.PsspPercentage = 0;
                    _AmountDistribution.Ptsa = 0;
                    _AmountDistribution.PtsaPercentage = 0;
                    _AmountDistribution.Acquirer = 0;
                    _AmountDistribution.AcquirerPercentage = 0;
                    _AmountDistribution.Issuer = 0;
                    _AmountDistribution.IssuerPercentage = 0;
                    _AmountDistribution.TransactionIssuerAmount = 0;
                    _AmountDistribution.TransactionIssuerCharge = 0;
                    _AmountDistribution.TransactionIssuerTotalAmount = 0;
                    _AmountDistribution.TransactionIssuerPercentage = 0;
                    _AmountDistribution.ThankUCash = 0;
                    _AmountDistribution.ThankUCashPercentage = 0;
                    return _AmountDistribution;
                }
                double RewardPercentage = 0;
                double UserRewardPercentage = 0;
                double PsspPercentage = 0;
                double PtspPercentage = 0;
                double PtsaPercentage = 0;
                double AcquirerPercentage = 0;
                string AllowAcquirerSettlement = "0";
                double IssuerPercentage = 0;
                double TransactionIssuerPercentage = 0;
                double RewardAmount = 0;
                RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                UserRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                PtsaPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("ptsapercentage", SystemAccounts.SmashLabId)), _AppConfig.SystemRoundPercentage);
                AcquirerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("acquirerpercentage", (long)_GatewayInfo.AcquirerId)), _AppConfig.SystemRoundPercentage);
                IssuerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("issuerpercentage", _UserInfo.IssuerId)), _AppConfig.SystemRoundPercentage);
                double RewardMaxInvoiceAmount = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardmaxinvoiceamount", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                if (RewardMaxInvoiceAmount > 0)
                {
                    if (ReferenceInvoiceAmount > RewardMaxInvoiceAmount)
                    {
                        ReferenceInvoiceAmount = RewardMaxInvoiceAmount;
                    }
                    else
                    {
                        ReferenceInvoiceAmount = InvoiceAmount;
                    }
                }
                else
                {
                    ReferenceInvoiceAmount = InvoiceAmount;
                }
                TransactionIssuerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("transactionissuerpercentage", _GatewayInfo.TransactionIssuerId)), _AppConfig.SystemRoundPercentage);
                OConfiguration RewardDeductionType = HCoreHelper.GetConfigurationDetails("rewarddeductiontype", _GatewayInfo.MerchantId);
                if (_UserRequest.UserReference.AccountTypeId == UserAccountType.PgAccount)
                {
                    PsspPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("pgpercentage", _UserRequest.UserReference.AccountId)), _AppConfig.SystemRoundPercentage);
                    AllowAcquirerSettlement = "0";
                }
                else if (_UserRequest.UserReference.AccountTypeId == UserAccountType.PosAccount)
                {
                    PtspPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("pospercentage", _UserRequest.UserReference.AccountId)), _AppConfig.SystemRoundPercentage);
                    AllowAcquirerSettlement = HCoreHelper.GetConfiguration("banksettlementbyptsp", _UserRequest.UserReference.AccountId);
                }
                else if (_UserRequest.UserReference.AccountTypeId == UserAccountType.Merchant)
                {
                }
                else
                {
                    _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                    _AmountDistribution.AllowAcquirerSettlement = AllowAcquirerSettlement;
                    _AmountDistribution.InvoiceAmount = InvoiceAmount;
                    _AmountDistribution.ReferenceInvoiceAmount = 0;
                    _AmountDistribution.RewardPercentage = 0;
                    _AmountDistribution.RewardAmount = 0;
                    _AmountDistribution.RewardCommission = 0;
                    _AmountDistribution.MerchantAmount = 0;
                    _AmountDistribution.User = 0;
                    _AmountDistribution.UserPercentage = 0;
                    _AmountDistribution.Ptsp = 0;
                    _AmountDistribution.PtspPercentage = 0;
                    _AmountDistribution.Pssp = 0;
                    _AmountDistribution.PsspPercentage = 0;
                    _AmountDistribution.Ptsa = 0;
                    _AmountDistribution.PtsaPercentage = 0;
                    _AmountDistribution.Acquirer = 0;
                    _AmountDistribution.AcquirerPercentage = 0;
                    _AmountDistribution.Issuer = 0;
                    _AmountDistribution.IssuerPercentage = 0;
                    _AmountDistribution.TransactionIssuerAmount = 0;
                    _AmountDistribution.TransactionIssuerCharge = 0;
                    _AmountDistribution.TransactionIssuerTotalAmount = 0;
                    _AmountDistribution.TransactionIssuerPercentage = 0;
                    _AmountDistribution.ThankUCash = 0;
                    _AmountDistribution.ThankUCashPercentage = 0;
                    return _AmountDistribution;
                }
                RewardAmount = HCoreHelper.GetPercentage(ReferenceInvoiceAmount, RewardPercentage, _AppConfig.SystemEntryRoundDouble);
                if (_UserRequest.sProducts != null && _UserRequest.sProducts.Count() > 0)
                {
                    double ProductRewardAmount = 0;
                    foreach (var Product in _UserRequest.sProducts)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            double ProductRewardPercentage = _HCoreContext.TUCTerminalProduct.Where(x => x.AccountId == _GatewayInfo.MerchantId && x.ReferenceNumber == Product._id).Select(x => x.RewardPercentage).FirstOrDefault();
                            if (ProductRewardPercentage > 0)
                            {
                                ProductRewardAmount += HCoreHelper.GetPercentage(Product.price, ProductRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                            }
                        }
                    }
                    if (ProductRewardAmount > 0)
                    {
                        RewardAmount = ProductRewardAmount;
                    }
                }
                if (_UserRequest.Items != null && _UserRequest.Items.Count() > 0)
                {
                    double ProductRewardAmount = 0;
                    foreach (var Product in _UserRequest.Items)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            double ProductRewardPercentage = _HCoreContext.TUCTerminalProduct.Where(x => x.AccountId == _GatewayInfo.MerchantId && x.ReferenceNumber == Product.ReferenceId).Select(x => x.RewardPercentage).FirstOrDefault();
                            if (ProductRewardPercentage > 0)
                            {
                                ProductRewardAmount += HCoreHelper.GetPercentage(Product.Price, ProductRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                            }
                        }
                    }
                    if (ProductRewardAmount > 0)
                    {
                        RewardAmount = ProductRewardAmount;
                    }
                }
                if (ThankUCashPlusIsEnable != 0)
                {
                    IsThankUCashEnabled = true;
                    double CriteriaValue = 0;
                    OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", _GatewayInfo.MerchantId);
                    if (RewardCriteriaDetails != null)
                    {
                        if (!string.IsNullOrEmpty(RewardCriteriaDetails.Value))
                        {
                            CriteriaValue = Convert.ToDouble(RewardCriteriaDetails.Value);
                        }
                        if (RewardCriteriaDetails.TypeCode == "rewardcriteriatype.mininvoice")
                        {
                            if (ReferenceInvoiceAmount < CriteriaValue)
                            {
                                _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                                _AmountDistribution.AllowAcquirerSettlement = AllowAcquirerSettlement;
                                _AmountDistribution.InvoiceAmount = InvoiceAmount;
                                _AmountDistribution.ReferenceInvoiceAmount = ReferenceInvoiceAmount;
                                _AmountDistribution.RewardPercentage = 0;
                                _AmountDistribution.RewardAmount = 0;
                                _AmountDistribution.RewardCommission = 0;
                                _AmountDistribution.MerchantAmount = 0;
                                _AmountDistribution.User = 0;
                                _AmountDistribution.UserPercentage = 0;
                                _AmountDistribution.Ptsp = 0;
                                _AmountDistribution.PtspPercentage = 0;
                                _AmountDistribution.Pssp = 0;
                                _AmountDistribution.PsspPercentage = 0;
                                _AmountDistribution.Ptsa = 0;
                                _AmountDistribution.PtsaPercentage = 0;
                                _AmountDistribution.Acquirer = 0;
                                _AmountDistribution.AcquirerPercentage = 0;
                                _AmountDistribution.Issuer = 0;
                                _AmountDistribution.IssuerPercentage = 0;
                                _AmountDistribution.TransactionIssuerAmount = 0;
                                _AmountDistribution.TransactionIssuerCharge = 0;
                                _AmountDistribution.TransactionIssuerTotalAmount = 0;
                                _AmountDistribution.TransactionIssuerPercentage = 0;
                                _AmountDistribution.ThankUCash = 0;
                                _AmountDistribution.ThankUCashPercentage = 0;
                                return _AmountDistribution;
                            }
                        }
                        else if (RewardCriteriaDetails.TypeCode == "rewardcriteriatype.multipleofamount")
                        {
                            if (ReferenceInvoiceAmount < CriteriaValue)
                            {
                                _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                                _AmountDistribution.AllowAcquirerSettlement = AllowAcquirerSettlement;
                                _AmountDistribution.InvoiceAmount = InvoiceAmount;
                                _AmountDistribution.ReferenceInvoiceAmount = ReferenceInvoiceAmount;
                                _AmountDistribution.RewardPercentage = 0;
                                _AmountDistribution.RewardAmount = 0;
                                _AmountDistribution.RewardCommission = 0;
                                _AmountDistribution.MerchantAmount = 0;
                                _AmountDistribution.User = 0;
                                _AmountDistribution.UserPercentage = 0;
                                _AmountDistribution.Ptsp = 0;
                                _AmountDistribution.PtspPercentage = 0;
                                _AmountDistribution.Pssp = 0;
                                _AmountDistribution.PsspPercentage = 0;
                                _AmountDistribution.Ptsa = 0;
                                _AmountDistribution.PtsaPercentage = 0;
                                _AmountDistribution.Acquirer = 0;
                                _AmountDistribution.AcquirerPercentage = 0;
                                _AmountDistribution.Issuer = 0;
                                _AmountDistribution.IssuerPercentage = 0;
                                _AmountDistribution.TransactionIssuerAmount = 0;
                                _AmountDistribution.TransactionIssuerCharge = 0;
                                _AmountDistribution.TransactionIssuerTotalAmount = 0;
                                _AmountDistribution.TransactionIssuerPercentage = 0;
                                _AmountDistribution.ThankUCash = 0;
                                _AmountDistribution.ThankUCashPercentage = 0;
                                return _AmountDistribution;
                            }
                            long RewardSlot = (long)(ReferenceInvoiceAmount / CriteriaValue);
                            double CriteriaRewardAmount = HCoreHelper.GetPercentage(CriteriaValue, RewardPercentage, 2);
                            RewardAmount = CriteriaRewardAmount * RewardSlot;
                        }
                    }
                }
                else
                {
                    IsThankUCashEnabled = false;
                }
                if (_GatewayInfo.TerminalId != 0 && _GatewayInfo.PtspId != 0 && _GatewayInfo.StoreId != 0 && _GatewayInfo.StoreStatusId != 0)
                {
                    if (_GatewayInfo.StoreStatusId != HelperStatus.Default.Active)
                    {
                        RewardAmount = 0;
                    }
                }
                if (RewardAmount > 0)
                {
                    if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                    {
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        double Balance = _ManageCoreTransaction.GetAccountBalance(_GatewayInfo.MerchantId, TransactionSource.Merchant);
                        if (RewardAmount > Balance)
                        {
                            _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                            _AmountDistribution.AllowAcquirerSettlement = AllowAcquirerSettlement;
                            _AmountDistribution.InvoiceAmount = InvoiceAmount;
                            _AmountDistribution.ReferenceInvoiceAmount = 0;
                            _AmountDistribution.RewardPercentage = 0;
                            _AmountDistribution.RewardAmount = 0;
                            _AmountDistribution.RewardCommission = 0;
                            _AmountDistribution.MerchantAmount = 0;
                            _AmountDistribution.User = 0;
                            _AmountDistribution.UserPercentage = 0;
                            _AmountDistribution.Ptsp = 0;
                            _AmountDistribution.PtspPercentage = 0;
                            _AmountDistribution.Pssp = 0;
                            _AmountDistribution.PsspPercentage = 0;
                            _AmountDistribution.Ptsa = 0;
                            _AmountDistribution.PtsaPercentage = 0;
                            _AmountDistribution.Acquirer = 0;
                            _AmountDistribution.AcquirerPercentage = 0;
                            _AmountDistribution.Issuer = 0;
                            _AmountDistribution.IssuerPercentage = 0;
                            _AmountDistribution.TransactionIssuerAmount = 0;
                            _AmountDistribution.TransactionIssuerCharge = 0;
                            _AmountDistribution.TransactionIssuerTotalAmount = 0;
                            _AmountDistribution.TransactionIssuerPercentage = 0;
                            _AmountDistribution.ThankUCash = 0;
                            _AmountDistribution.ThankUCashPercentage = 0;
                            return _AmountDistribution;
                        }
                    }
                    double UserRewardAmount = 0;
                    double PsspAmount = 0;
                    double PtspAmount = 0;
                    double PtsaAmount = 0;
                    double AcquirerAmount = 0;
                    double IssuerAmount = 0;
                    double TransactionIssuerAmount = 0;
                    double TransactionIssuerCharge = 0;
                    double TransactionIssuerTotalAmount = 0;
                    double ThankUCashAmount = RewardAmount;
                    if (UserRewardPercentage > 0 && _UserInfo.UserAccountId > 0)
                    {
                        UserRewardAmount = HCoreHelper.GetPercentage(RewardAmount, UserRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                        ThankUCashAmount = ThankUCashAmount - UserRewardAmount;
                    }
                    if (PsspPercentage > 0 && _GatewayInfo.PsspId > 0)
                    {
                        PsspAmount = HCoreHelper.GetPercentage(RewardAmount, PsspPercentage, _AppConfig.SystemEntryRoundDouble);
                        ThankUCashAmount = ThankUCashAmount - PsspAmount;
                    }
                    if (PtspPercentage > 0 && _GatewayInfo.PtspId > 0)
                    {
                        PtspAmount = HCoreHelper.GetPercentage(RewardAmount, PtspPercentage, _AppConfig.SystemEntryRoundDouble);
                        ThankUCashAmount = ThankUCashAmount - PtspAmount;
                    }
                    if (AcquirerPercentage > 0 && _GatewayInfo.AcquirerId > 0)
                    {
                        AcquirerAmount = HCoreHelper.GetPercentage(RewardAmount, AcquirerPercentage, _AppConfig.SystemEntryRoundDouble);
                        ThankUCashAmount = ThankUCashAmount - AcquirerAmount;
                    }
                    if (PtsaPercentage > 0)
                    {
                        PtsaAmount = HCoreHelper.GetPercentage(RewardAmount, PtsaPercentage, _AppConfig.SystemEntryRoundDouble);
                        ThankUCashAmount = ThankUCashAmount - PtsaAmount;
                    }
                    if (IssuerPercentage > 0 && _UserInfo.IssuerId != 0)
                    {
                        IssuerAmount = HCoreHelper.GetPercentage(RewardAmount, IssuerPercentage, _AppConfig.SystemEntryRoundDouble);
                        ThankUCashAmount = ThankUCashAmount - IssuerAmount;
                    }
                    if (TransactionIssuerPercentage > 0 && _GatewayInfo.TransactionIssuerId > 0)
                    {
                        TransactionIssuerTotalAmount = HCoreHelper.GetPercentage(RewardAmount, TransactionIssuerPercentage, _AppConfig.SystemEntryRoundDouble);
                        ThankUCashAmount = ThankUCashAmount - TransactionIssuerAmount;
                        TransactionIssuerAmount = HCoreHelper.GetPercentage(TransactionIssuerTotalAmount, 40, _AppConfig.SystemEntryRoundDouble);
                        TransactionIssuerCharge = TransactionIssuerTotalAmount - TransactionIssuerAmount;
                    }
                    ThankUCashAmount = HCoreHelper.RoundNumber(ThankUCashAmount, _AppConfig.SystemEntryRoundDouble);
                    double ComissionAmount = HCoreHelper.RoundNumber((RewardAmount - UserRewardAmount), _AppConfig.SystemEntryRoundDouble);
                    if (RewardPercentage < 0)
                    {
                        RewardPercentage = 0;
                    }
                    if (RewardAmount < 0)
                    {
                        RewardAmount = 0;
                    }
                    if (ComissionAmount < 0)
                    {
                        ComissionAmount = 0;
                    }
                    if (UserRewardAmount < 0)
                    {
                        UserRewardAmount = 0;
                    }
                    if (PtspAmount < 0)
                    {
                        PtspAmount = 0;
                    }
                    if (PsspAmount < 0)
                    {
                        PsspAmount = 0;
                    }
                    if (PtsaAmount < 0)
                    {
                        PtsaAmount = 0;
                    }
                    if (AcquirerAmount < 0)
                    {
                        AcquirerAmount = 0;
                    }
                    if (IssuerAmount < 0)
                    {
                        IssuerAmount = 0;
                    }
                    if (TransactionIssuerAmount < 0)
                    {
                        TransactionIssuerAmount = 0;
                    }
                    if (TransactionIssuerCharge < 0)
                    {
                        TransactionIssuerCharge = 0;
                    }
                    if (TransactionIssuerTotalAmount < 0)
                    {
                        TransactionIssuerTotalAmount = 0;
                    }
                    if (ThankUCashAmount < 0)
                    {
                        ThankUCashAmount = 0;
                    }
                    _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                    _AmountDistribution.AllowAcquirerSettlement = AllowAcquirerSettlement;
                    _AmountDistribution.InvoiceAmount = InvoiceAmount;
                    _AmountDistribution.ReferenceInvoiceAmount = ReferenceInvoiceAmount;
                    _AmountDistribution.RewardPercentage = RewardPercentage;
                    _AmountDistribution.RewardAmount = RewardAmount;
                    _AmountDistribution.RewardCommission = ComissionAmount;
                    _AmountDistribution.MerchantAmount = (InvoiceAmount - RewardAmount);
                    _AmountDistribution.User = UserRewardAmount;
                    _AmountDistribution.UserPercentage = UserRewardPercentage;
                    _AmountDistribution.Ptsp = PtspAmount;
                    _AmountDistribution.PtspPercentage = PtspPercentage;
                    _AmountDistribution.Pssp = PsspAmount;
                    _AmountDistribution.PsspPercentage = PsspPercentage;
                    _AmountDistribution.Ptsa = PtsaAmount;
                    _AmountDistribution.PtsaPercentage = PtsaPercentage;
                    _AmountDistribution.Acquirer = AcquirerAmount;
                    _AmountDistribution.AcquirerPercentage = AcquirerPercentage;
                    _AmountDistribution.Issuer = IssuerAmount;
                    _AmountDistribution.IssuerPercentage = IssuerPercentage;
                    _AmountDistribution.TransactionIssuerAmount = TransactionIssuerAmount;
                    _AmountDistribution.TransactionIssuerCharge = TransactionIssuerCharge;
                    _AmountDistribution.TransactionIssuerTotalAmount = TransactionIssuerTotalAmount;
                    _AmountDistribution.TransactionIssuerPercentage = TransactionIssuerPercentage;
                    _AmountDistribution.ThankUCash = ThankUCashAmount;
                    _AmountDistribution.ThankUCashPercentage = HCoreHelper.GetAmountPercentage(ThankUCashAmount, ComissionAmount);
                    if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepayandpostpay")
                    {
                        _AmountDistribution.MerchantReverseAmount = UserRewardAmount;
                    }
                    return _AmountDistribution;
                }
                else
                {
                    _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                    _AmountDistribution.AllowAcquirerSettlement = AllowAcquirerSettlement;
                    _AmountDistribution.InvoiceAmount = InvoiceAmount;
                    _AmountDistribution.ReferenceInvoiceAmount = 0;
                    _AmountDistribution.RewardPercentage = 0;
                    _AmountDistribution.RewardAmount = 0;
                    _AmountDistribution.RewardCommission = 0;
                    _AmountDistribution.MerchantAmount = 0;
                    _AmountDistribution.User = 0;
                    _AmountDistribution.UserPercentage = 0;
                    _AmountDistribution.Ptsp = 0;
                    _AmountDistribution.PtspPercentage = 0;
                    _AmountDistribution.Pssp = 0;
                    _AmountDistribution.PsspPercentage = 0;
                    _AmountDistribution.Ptsa = 0;
                    _AmountDistribution.PtsaPercentage = 0;
                    _AmountDistribution.Acquirer = 0;
                    _AmountDistribution.AcquirerPercentage = 0;
                    _AmountDistribution.Issuer = 0;
                    _AmountDistribution.IssuerPercentage = 0;
                    _AmountDistribution.TransactionIssuerAmount = 0;
                    _AmountDistribution.TransactionIssuerCharge = 0;
                    _AmountDistribution.TransactionIssuerTotalAmount = 0;
                    _AmountDistribution.TransactionIssuerPercentage = 0;
                    _AmountDistribution.ThankUCash = 0;
                    _AmountDistribution.ThankUCashPercentage = 0;
                    return _AmountDistribution;
                }
            }
            else
            {
                double InvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                bool IsThankUCashEnabled = false;
                long ThankUCashPlusIsEnable = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", _GatewayInfo.MerchantId));
                if (ThankUCashPlusIsEnable != 0)
                {
                    IsThankUCashEnabled = true;
                }
                else
                {
                    IsThankUCashEnabled = false;
                }
                _AmountDistribution = new OAmountDistribution();
                _AmountDistribution.IsThankUCashEnabled = IsThankUCashEnabled;
                _AmountDistribution.AllowAcquirerSettlement = "0";
                _AmountDistribution.InvoiceAmount = InvoiceAmount;
                _AmountDistribution.ReferenceInvoiceAmount = 0;
                _AmountDistribution.RewardPercentage = 0;
                _AmountDistribution.RewardAmount = 0;
                _AmountDistribution.RewardCommission = 0;
                _AmountDistribution.MerchantAmount = 0;
                _AmountDistribution.User = 0;
                _AmountDistribution.UserPercentage = 0;
                _AmountDistribution.Ptsp = 0;
                _AmountDistribution.PtspPercentage = 0;
                _AmountDistribution.Pssp = 0;
                _AmountDistribution.PsspPercentage = 0;
                _AmountDistribution.Ptsa = 0;
                _AmountDistribution.PtsaPercentage = 0;
                _AmountDistribution.Acquirer = 0;
                _AmountDistribution.AcquirerPercentage = 0;
                _AmountDistribution.Issuer = 0;
                _AmountDistribution.IssuerPercentage = 0;
                _AmountDistribution.TransactionIssuerAmount = 0;
                _AmountDistribution.TransactionIssuerCharge = 0;
                _AmountDistribution.TransactionIssuerTotalAmount = 0;
                _AmountDistribution.TransactionIssuerPercentage = 0;
                _AmountDistribution.ThankUCash = 0;
                _AmountDistribution.ThankUCashPercentage = 0;
                return _AmountDistribution;
            }
        }
        private OResponse ProcessTransactionResponse(OThankUGateway.Request _Request, OGatewayInfo _GatewayInfo, OUserInfo _UserInfo, OAmountDistribution _AmountDistributionReference, OCoreTransaction.Response _TransactionReference)
        {
            //if (_AmountDistributionReference.RewardAmount < 0.1 && _GatewayInfo.MerchantId == 49648)
            //{
            //    if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
            //    {
            //        if (HostEnvironment == HostEnvironmentType.Live)
            //        {
            //            if (_AmountDistributionReference.User > 0)
            //            {
            //                string Message = "Thank U from Enyo. We are launching our New rewards soon, you will get our communication soon. Please update your profile on the TUC App https://bit.ly/tuc-app";
            //                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
            //                _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
            //                _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
            //                _GatewayResponse.TransactionReference = _TransactionReference.GroupKey;
            //                _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
            //                _GatewayResponse.RewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                _GatewayResponse.CommissionAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardCommission, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                _GatewayResponse.UserRewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                _GatewayResponse.AcquirerAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Acquirer, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                _GatewayResponse.MerchantAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.MerchantAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
            //                {
            //                    _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                    _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                }
            //                if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
            //                {
            //                    _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                    _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
            //                }
            //                #region Send Response
            //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
            //                #endregion
            //            }
            //        }
            //    }
            //}
            _ManageCoreTransaction = new ManageCoreTransaction();
            if (_AmountDistribution.IsThankUCashEnabled == true)
            {
                OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", _GatewayInfo.MerchantId);
                double TucPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", _GatewayInfo.MerchantId));
                double ThankUCashPlusBalance = _ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_UserInfo.UserAccountId, _GatewayInfo.MerchantId);
                if ((ThankUCashPlusBalance > (TucPlusMinTransferAmount - 1)))
                {
                    _CoreTransactionRequest = new OCoreTransaction.Request();
                    _CoreTransactionRequest.CashierId = _GatewayInfo.CashierId;
                    _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                    _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                    _CoreTransactionRequest.ParentId = _GatewayInfo.MerchantId;
                    _CoreTransactionRequest.SubParentId = _GatewayInfo.StoreId;
                    _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistributionReference.ReferenceInvoiceAmount;
                    _CoreTransactionRequest.InvoiceAmount = _AmountDistributionReference.InvoiceAmount;
                    if (_GatewayInfo.AcquirerId != null)
                    {
                        _CoreTransactionRequest.BankId = (long)_GatewayInfo.AcquirerId;
                    }
                    _CoreTransactionRequest.ReferenceAmount = _AmountDistributionReference.RewardAmount;
                    _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                    if (_GatewayInfo.TerminalId != 0)
                    {
                        _CoreTransactionRequest.CreatedById = _GatewayInfo.TerminalId;
                    }
                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                    {
                        UserAccountId = _UserInfo.UserAccountId,
                        ModeId = TransactionMode.Debit,
                        TypeId = TransactionType.ThankUCashPlusCredit,
                        SourceId = TransactionSource.ThankUCashPlus,
                        Amount = ThankUCashPlusBalance,
                        TotalAmount = ThankUCashPlusBalance
                    });
                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                    {
                        UserAccountId = _UserInfo.UserAccountId,
                        ModeId = TransactionMode.Credit,
                        TypeId = TransactionType.ThankUCashPlusCredit,
                        SourceId = TransactionSource.TUC,
                        Amount = ThankUCashPlusBalance,
                        TotalAmount = ThankUCashPlusBalance
                    });
                    _CoreTransactionRequest.Transactions = _TransactionItems;
                    _ManageCoreTransaction = new ManageCoreTransaction();
                    OCoreTransaction.Response TUCPlusTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                    double ThankUCashPlusBalanceUpdate = _ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_UserInfo.UserAccountId, _GatewayInfo.MerchantId);
                    double AccBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                    if (_UserInfo.AccountStatusId == HelperStatus.Default.Active && (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test))
                    {
                        if (_AmountDistributionReference.User > 0)
                        {
                            if (_GatewayInfo.MerchantId == 49648)
                            {
                                //string Message = "Credit Alert: You have been rewarded by ENYO ThankUCash, Bal: " + ThankUCashPlusBalance + ". Your ThankUCash Bal: N" + AccBalance + ".";
                                // HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                //Cr Alert: You’ve earned 101 ENYOThankU points. Continue to buy & earn points. Redeem from 1stOct. ETU Bal: 202points Main Bal N3245. Get App https://bit.ly/tuc-app

                                string Message = "Cr Alert: Your ThankUCash Plus " + Math.Round(ThankUCashPlusBalance, 2) + " Points now available for you to redeem. TUC Plus Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + " Main Bal: " + Math.Round(AccBalance, 2) + ". Download TUC App: https://bit.ly/tuc-app";
                                //string Message = "Cr Alert: You’ve earned " + Math.Round(ThankUCashPlusBalance, 2) + " ENYOThankU points. Continue to buy & earn points. Redeem from 1stOct. ETU Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + "pt Main Bal: " + Math.Round(AccBalance, 2) + ". Get App https://bit.ly/tuc-app";
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);
                            }
                            else
                            {
                                string Message = "Cr Alert: Your ThankUCash Plus " + Math.Round(ThankUCashPlusBalance, 2) + " Points now available for you to redeem. TUC Plus Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + " Main Bal: " + Math.Round(AccBalance, 2) + ". Download TUC App: https://bit.ly/tuc-app";
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);
                                //string RewardSms = HCoreHelper.GetConfiguration("rewardsms", _GatewayInfo.MerchantId);
                                //if (!string.IsNullOrEmpty(RewardSms))
                                //{
                                //    #region Send SMS
                                //    string Message = RewardSms
                                //    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString())
                                //    .Replace("[BALANCE]", HCoreHelper.RoundNumber(AccBalance, _AppConfig.SystemExitRoundDouble).ToString())
                                //    .Replace("[MERCHANT]", _GatewayInfo.MerchantDisplayName)
                                //    .Replace("[TCODE]", _TransactionReference.TCode);
                                //    //   HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                //    #endregion
                                //}
                                //else
                                //{
                                //    #region Send SMS
                                //    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(AccBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                //    #endregion
                                //}
                            }
                        }
                    }
                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                    _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                    _GatewayResponse.TransactionReference = _TransactionReference.GroupKey;
                    _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                    _GatewayResponse.RewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.CommissionAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardCommission, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.UserRewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.AcquirerAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Acquirer, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.MerchantAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.MerchantAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                    {
                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    }
                    if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                    {
                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
                    #endregion
                }
                else
                {
                    double Balance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);

                    if (_UserInfo.AccountStatusId == HelperStatus.Default.Active && HostEnvironment == HostEnvironmentType.Live)
                    {
                        if (_GatewayInfo.MerchantId == 49648)
                        {
                            if (_AmountDistributionReference.User > 0)
                            {
                                double ThankUCashPlusBalanceUpdate = _ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_UserInfo.UserAccountId, _GatewayInfo.MerchantId);
                                //string Message = "Cr Alert: You’ve earned " + Math.Round(_AmountDistributionReference.User, 2) + " ENYOThankU points. Continue to buy & earn points. Redeem from 1stOct. ETU Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + "pt Main Bal: " + Math.Round(Balance, 2) + ". Get App https://bit.ly/tuc-app";
                                //"Cr Alert: You've earned 2.18 ENYOThankU points. Continue to buy & earn points. Redeem from 1st Oct. ETU Bal: 101.4pt Main Bal: 8195.41. Get App https://bit.ly/tuc-app";
                                string Message = "Cr Alert: You got " + Math.Round(_AmountDistributionReference.User, 2) + " ENYO ThankU points. Continue to buy & earn points. ETU Plus Bal:  N" + Math.Round(ThankUCashPlusBalance, 2) + ". Main Bal is N" + Math.Round(Balance, 2) + " Get App: https://bit.ly/tuc-app";
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);
                            }
                            else
                            {
                                string Message = "Cr Alert: You got 0 TUC Plus points from ENYO ThankUCash. Continue to buy & earn points. TUC Plus Bal:  N" + Math.Round(ThankUCashPlusBalance, 2) + ". Main Balance is N" + Math.Round(Balance, 2) + " Get App: https://bit.ly/tuc-app";
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);
                            }
                        }
                        else
                        {
                            if (_AmountDistributionReference.User > 0)
                            {
                                //string RewardSms = HCoreHelper.GetConfiguration("rewardsms", _GatewayInfo.MerchantId);
                                //if (!string.IsNullOrEmpty(RewardSms))
                                //{
                                //    #region Send SMS
                                //    string Message = RewardSms
                                //    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString())
                                //    .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                                //    .Replace("[MERCHANT]", _GatewayInfo.MerchantDisplayName)
                                //    .Replace("[TCODE]", "");
                                //    HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                //    #endregion
                                //}
                                //else
                                //{
                                //    #region Send SMS
                                //    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download App https://bit.ly/tuc-app . ThankUCash";
                                //    HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                //    #endregion
                                //}
                                double TBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                                #region Send SMS
                                string Message = "Cr Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points from " + _GatewayInfo.MerchantDisplayName + ". Buy upto N" + RewardCriteriaDetails.Value + " to get cash reward. TUC Plus Bal: N" + HCoreHelper.RoundNumber(ThankUCashPlusBalance, _AppConfig.SystemExitRoundDouble).ToString() + ".Main Balance is N" + HCoreHelper.RoundNumber(TBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Get App: https://bit.ly/tuc-app";// + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download App https://bit.ly/tuc-app . ThankUCash";
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            //  HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                #endregion

                            }
                            else
                            {
                                double TBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                                #region Send SMS
                                string Message = "Cr Alert: You got 0 points from " + _GatewayInfo.MerchantDisplayName + ". Buy upto N" + RewardCriteriaDetails.Value + " to get cash reward. TUC Plus Bal: N" + HCoreHelper.RoundNumber(ThankUCashPlusBalance, _AppConfig.SystemExitRoundDouble).ToString() + ".Main Balance is N" + HCoreHelper.RoundNumber(TBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Get App: https://bit.ly/tuc-app";// + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download App https://bit.ly/tuc-app . ThankUCash";
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          //  HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                #endregion
                            }
                        }
                        #region Send Email 
                        if (!string.IsNullOrEmpty(_UserInfo.EmailAddress))
                        {
                            var _EmailParameters = new
                            {
                                UserDisplayName = _OUserInfo.DisplayName,
                                MerchantName = _GatewayInfo.MerchantDisplayName,
                                InvoiceAmount = _AmountDistributionReference.InvoiceAmount.ToString(),
                                Amount = _AmountDistributionReference.ToString(),
                                Balance = Balance.ToString(),
                            };
                            HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _UserInfo.DisplayName, _UserInfo.EmailAddress, _EmailParameters, _Request.UserReference);
                        }
                        #endregion
                    }
                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                    _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                    _GatewayResponse.TransactionReference = _TransactionReference.GroupKey;
                    _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                    _GatewayResponse.RewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.CommissionAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardCommission, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.UserRewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.AcquirerAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Acquirer, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.MerchantAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.MerchantAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                    {
                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    }
                    if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                    {
                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
                    #endregion
                }
            }
            else
            {
                double Balance = HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId), 2);
                if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                {
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        if (_AmountDistributionReference.User > 0)
                        {
                            string RewardSms = HCoreHelper.GetConfiguration("rewardsms", _GatewayInfo.MerchantId);
                            if (!string.IsNullOrEmpty(RewardSms))
                            {
                                #region Send SMS
                                string Message = RewardSms
                                .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString())
                                .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                                .Replace("[MERCHANT]", _GatewayInfo.MerchantDisplayName)
                                .Replace("[TCODE]", _TransactionReference.TCode);
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);
                                #endregion
                            }
                            else
                            {
                                #region Send SMS
                                //string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app Pls Give Cashier Code:" + _TransactionReference.TCode;
                                string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + _GatewayInfo.MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, _TransactionReference.GroupKey, _TransactionReference.ReferenceId);
                                #endregion
                            }
                            #region Send Email 
                            if (!string.IsNullOrEmpty(_UserInfo.EmailAddress))
                            {
                                var _EmailParameters = new
                                {
                                    UserDisplayName = _OUserInfo.DisplayName,
                                    MerchantName = _GatewayInfo.MerchantDisplayName,
                                    InvoiceAmount = _AmountDistributionReference.InvoiceAmount.ToString(),
                                    Amount = HCoreHelper.RoundNumber(_AmountDistributionReference.User, 2).ToString(),
                                    Balance = Balance.ToString(),
                                };
                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _UserInfo.DisplayName, _UserInfo.EmailAddress, _EmailParameters, _Request.UserReference);
                            }
                            #endregion
                        }
                    }
                }
                _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                _GatewayResponse.TransactionReference = _TransactionReference.GroupKey;
                _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
                _GatewayResponse.RewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                _GatewayResponse.CommissionAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.RewardCommission, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                _GatewayResponse.UserRewardAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                _GatewayResponse.AcquirerAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Acquirer, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                _GatewayResponse.MerchantAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.MerchantAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                {
                    _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                }
                if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                {
                    _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                    _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionReference.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
                #endregion
            }
        }
        internal OResponse RegisterUser(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG184, CoreResources.HCG184M);
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG145, CoreResources.HCG145M);
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG146, CoreResources.HCG146M);
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG147, CoreResources.HCG147M);
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        _AppProfileRequest = new OAppProfile.Request();
                        _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
                        _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                        _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                        _AppProfileRequest.EmailAddress = _Request.EmailAddress;
                        _AppProfileRequest.Name = _Request.Name;
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _AppProfileRequest.DisplayName = _Request.Name;
                        }
                        else
                        {
                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Gender))
                        {
                            _Request.Gender = _Request.Gender.ToLower();
                            if (_Request.Gender == "male")
                            {
                                _AppProfileRequest.GenderCode = "gender.male";
                            }
                            else if (_Request.Gender == "female")
                            {
                                _AppProfileRequest.GenderCode = "gender.female";
                            }
                        }
                        _AppProfileRequest.CardNumber = _Request.CardNumber;
                        _AppProfileRequest.CardSerialNumber = _Request.TagNumber;
                        _AppProfileRequest.UserReference = _Request.UserReference;
                        _ManageCoreUserAccess = new ManageCoreUserAccess();
                        OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                        if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                        {
                            _GatewayResponse.Name = _Request.Name;
                            _GatewayResponse.FirstName = _Request.FirstName;
                            _GatewayResponse.LastName = _Request.LastName;
                            _GatewayResponse.Name = _Request.Name;
                            _GatewayResponse.Gender = _Request.Gender;
                            _GatewayResponse.EmailAddress = _Request.EmailAddress;
                            _GatewayResponse.MobileNumber = _Request.MobileNumber;
                            _GatewayResponse.DateOfBirth = _Request.DateOfBirth;
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                            #endregion
                        }
                        //if (_AppUserCreateResponse.StatusResponseCode == "AUA105")
                        //{
                        //    _GatewayResponse.Name = _Request.Name;
                        //    _GatewayResponse.Gender = _Request.Gender;
                        //    _GatewayResponse.EmailAddress = _Request.EmailAddress;
                        //    _GatewayResponse.MobileNumber = _Request.MobileNumber;
                        //    #region Send Response
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                        //    #endregion
                        //}
                        //else
                        //{
                        //    if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                        //    {
                        //        #region Send Response
                        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                        //        #endregion
                        //    }
                        //    else
                        //    {
                        //        #region Send Response
                        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                        //        #endregion
                        //    }
                        //}
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "RegisterUser", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse UpdateUssdUser(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCA1132");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                        _AppProfileRequest = new OAppProfile.Request();
                        _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
                        _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                        _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                        _AppProfileRequest.EmailAddress = _Request.EmailAddress;
                        _AppProfileRequest.Name = _Request.Name;
                        _AppProfileRequest.DateOfBirth = _Request.DateOfBirth;
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _AppProfileRequest.DisplayName = _Request.Name;
                        }
                        else
                        {
                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Gender))
                        {
                            _Request.Gender = _Request.Gender.ToLower();
                            if (_Request.Gender == "male")
                            {
                                _AppProfileRequest.GenderCode = "gender.male";
                            }
                            else if (_Request.Gender == "female")
                            {
                                _AppProfileRequest.GenderCode = "gender.female";
                            }
                        }
                        _AppProfileRequest.CardNumber = _Request.CardNumber;
                        _AppProfileRequest.CardSerialNumber = _Request.TagNumber;
                        _AppProfileRequest.UserReference = _Request.UserReference;
                        string AppAccount = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        var AccountDetails = _HCoreContext.HCUAccount
                            .Where(x => x.User.Username == AppAccount && x.AccountTypeId == UserAccountType.Appuser)
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                AccountDetails.Name = _Request.Name;
                                if (AccountDetails.Name.Length > 25)
                                {
                                    AccountDetails.DisplayName = AccountDetails.Name.Substring(0, 24);
                                }
                                else
                                {
                                    AccountDetails.DisplayName = AccountDetails.Name;
                                }
                            }
                            if (!string.IsNullOrEmpty(_Request.EmailAddress))
                            {
                                AccountDetails.EmailAddress = _Request.EmailAddress;

                            }
                            if (_Request.DateOfBirth != null)
                            {
                                AccountDetails.DateOfBirth = _Request.DateOfBirth;
                            }
                            if (!string.IsNullOrEmpty(_Request.Gender))
                            {
                                int GenderId = _HCoreContext.HCCore.Where(x => x.SystemName == _AppProfileRequest.GenderCode).Select(x => x.Id).FirstOrDefault();
                                if (GenderId != 0)
                                {
                                    AccountDetails.GenderId = GenderId;
                                }
                            }
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();

                            _GatewayResponse.Name = _Request.Name;
                            _GatewayResponse.Gender = _Request.Gender;
                            _GatewayResponse.EmailAddress = _Request.EmailAddress;
                            _GatewayResponse.MobileNumber = _Request.MobileNumber;
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HC200", "Account details updated");
                            #endregion
                        }
                        else
                        {
                            _ManageCoreUserAccess = new ManageCoreUserAccess();
                            OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                            if (_AppUserCreateResponse.StatusResponseCode == "AUA105")
                            {
                                _GatewayResponse.Name = _Request.Name;
                                _GatewayResponse.Gender = _Request.Gender;
                                _GatewayResponse.EmailAddress = _Request.EmailAddress;
                                _GatewayResponse.MobileNumber = _Request.MobileNumber;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                #endregion
                            }
                            else
                            {
                                if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                    #endregion
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "RegisterUser", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse ResetUssdUserPin(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCA1132");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                        _AppProfileRequest = new OAppProfile.Request();
                        _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
                        _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                        _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                        _AppProfileRequest.EmailAddress = _Request.EmailAddress;
                        _AppProfileRequest.Name = _Request.Name;
                        _AppProfileRequest.DateOfBirth = _Request.DateOfBirth;
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _AppProfileRequest.DisplayName = _Request.Name;
                        }
                        else
                        {
                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Gender))
                        {
                            _Request.Gender = _Request.Gender.ToLower();
                            if (_Request.Gender == "male")
                            {
                                _AppProfileRequest.GenderCode = "gender.male";
                            }
                            else if (_Request.Gender == "female")
                            {
                                _AppProfileRequest.GenderCode = "gender.female";
                            }
                        }
                        _AppProfileRequest.CardNumber = _Request.CardNumber;
                        _AppProfileRequest.CardSerialNumber = _Request.TagNumber;
                        _AppProfileRequest.UserReference = _Request.UserReference;
                        string AppAccount = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        var AccountDetails = _HCoreContext.HCUAccount
                            .Where(x => x.User.Username == AppAccount && x.AccountTypeId == UserAccountType.Appuser)
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                AccountDetails.Name = _Request.Name;
                                if (AccountDetails.Name.Length > 25)
                                {
                                    AccountDetails.DisplayName = AccountDetails.Name.Substring(0, 24);
                                }
                                else
                                {
                                    AccountDetails.DisplayName = AccountDetails.Name;
                                }
                            }
                            if (!string.IsNullOrEmpty(_Request.EmailAddress))
                            {
                                AccountDetails.EmailAddress = _Request.EmailAddress;

                            }
                            if (_Request.DateOfBirth != null)
                            {
                                AccountDetails.DateOfBirth = _Request.DateOfBirth;
                            }
                            if (!string.IsNullOrEmpty(_Request.Gender))
                            {
                                int GenderId = _HCoreContext.HCCore.Where(x => x.SystemName == _AppProfileRequest.GenderCode).Select(x => x.Id).FirstOrDefault();
                                if (GenderId != 0)
                                {
                                    AccountDetails.GenderId = GenderId;
                                }
                            }
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                            string NPIN = HCoreHelper.GenerateRandomNumber(4);
                            AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(NPIN);
                            _HCoreContext.SaveChanges();
                            _GatewayResponse.Name = _Request.Name;
                            _GatewayResponse.Gender = _Request.Gender;
                            _GatewayResponse.EmailAddress = _Request.EmailAddress;
                            _GatewayResponse.MobileNumber = _Request.MobileNumber;
                            HCoreHelper.SendSMS(SmsType.Transaction, "234", AccountDetails.MobileNumber, "Pin reset done. To redeem your cash, use pin: " + NPIN + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", AccountDetails.Id, null);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HC200", "Pin reset completed");
                            #endregion
                        }
                        else
                        {
                            _ManageCoreUserAccess = new ManageCoreUserAccess();
                            OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                            if (_AppUserCreateResponse.StatusResponseCode == "AUA105")
                            {
                                _GatewayResponse.Name = _Request.Name;
                                _GatewayResponse.Gender = _Request.Gender;
                                _GatewayResponse.EmailAddress = _Request.EmailAddress;
                                _GatewayResponse.MobileNumber = _Request.MobileNumber;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                #endregion
                            }
                            else
                            {
                                if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                    #endregion
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "RegisterUser", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse RegisterUssdUser(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCA1132");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                        _AppProfileRequest = new OAppProfile.Request();
                        _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
                        _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                        _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                        _AppProfileRequest.EmailAddress = _Request.EmailAddress;
                        _AppProfileRequest.Name = _Request.Name;
                        _AppProfileRequest.DateOfBirth = _Request.DateOfBirth;
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _AppProfileRequest.DisplayName = _Request.Name;
                        }
                        else
                        {
                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Gender))
                        {
                            _Request.Gender = _Request.Gender.ToLower();
                            if (_Request.Gender == "male")
                            {
                                _AppProfileRequest.GenderCode = "gender.male";
                            }
                            else if (_Request.Gender == "female")
                            {
                                _AppProfileRequest.GenderCode = "gender.female";
                            }
                        }
                        _AppProfileRequest.CardNumber = _Request.CardNumber;
                        _AppProfileRequest.CardSerialNumber = _Request.TagNumber;
                        _AppProfileRequest.UserReference = _Request.UserReference;
                        string AppAccount = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        var AccountDetails = _HCoreContext.HCUAccount
                            .Where(x => x.User.Username == AppAccount && x.AccountTypeId == UserAccountType.Appuser)
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (string.IsNullOrEmpty(AccountDetails.Name) && !string.IsNullOrEmpty(_Request.Name))
                            {
                                AccountDetails.Name = _Request.Name;
                                if (AccountDetails.Name.Length > 25)
                                {
                                    AccountDetails.DisplayName = AccountDetails.Name.Substring(0, 24);
                                }
                                else
                                {
                                    AccountDetails.DisplayName = AccountDetails.Name;
                                }
                            }
                            if (string.IsNullOrEmpty(AccountDetails.EmailAddress) && !string.IsNullOrEmpty(_Request.EmailAddress))
                            {
                                AccountDetails.EmailAddress = _Request.EmailAddress;

                            }
                            if (AccountDetails.DateOfBirth == null && _Request.DateOfBirth != null)
                            {
                                AccountDetails.DateOfBirth = _Request.DateOfBirth;
                            }
                            if (AccountDetails.GenderId == null && !string.IsNullOrEmpty(_Request.Gender))
                            {
                                int GenderId = _HCoreContext.HCCore.Where(x => x.SystemName == _AppProfileRequest.GenderCode).Select(x => x.Id).FirstOrDefault();
                                if (GenderId != 0)
                                {
                                    AccountDetails.GenderId = GenderId;
                                }
                            }
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();

                            _GatewayResponse.Name = _Request.Name;
                            _GatewayResponse.Gender = _Request.Gender;
                            _GatewayResponse.EmailAddress = _Request.EmailAddress;
                            _GatewayResponse.MobileNumber = _Request.MobileNumber;
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HC200", "Account details updated");
                            #endregion
                        }
                        else
                        {
                            _ManageCoreUserAccess = new ManageCoreUserAccess();
                            OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                            if (_AppUserCreateResponse.StatusResponseCode == "AUA105")
                            {
                                _GatewayResponse.Name = _Request.Name;
                                _GatewayResponse.Gender = _Request.Gender;
                                _GatewayResponse.EmailAddress = _Request.EmailAddress;
                                _GatewayResponse.MobileNumber = _Request.MobileNumber;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                #endregion
                            }
                            else
                            {
                                if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, _AppUserCreateResponse.StatusResponseCode, _AppUserCreateResponse.StatusMessage);
                                    #endregion
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "RegisterUser", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse ConnectMerchant(OThankUGateway.Request _Request)
        {
            #region Declare

            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount || _Request.UserReference.AccountTypeId == UserAccountType.Acquirer || _Request.UserReference.AccountTypeId == UserAccountType.Merchant || _Request.UserReference.AccountTypeId == UserAccountType.Partner)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantDetails = _HCoreContext.HCUAccount
                                  .Where(x => x.AccountTypeId == UserAccountType.Merchant &&
                                              x.StatusId == HelperStatus.Default.Active &&
                                              x.AccountCode == _Request.MerchantId)
                                  .Select(x => new
                                  {
                                      MerchantId = x.Id,
                                      MerchantKey = x.Guid,
                                      DisplayName = x.DisplayName,
                                  }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                var MerchantOwnerDetails = _HCoreContext.HCUAccountOwner
                                    .Where(x => x.OwnerId == _Request.UserReference.AccountId &&
                                    x.AccountId == MerchantDetails.MerchantId &&
                                    x.StatusId == HelperStatus.Default.Active)
                                    .Select(x => new
                                    {
                                        ConnectionKey = x.Guid,
                                        StartDate = x.StartDate,
                                        EndDate = x.EndDate,
                                        MerchantKey = x.Account.Guid,
                                        DisplayName = x.Account.DisplayName,
                                    }).FirstOrDefault();
                                if (MerchantOwnerDetails != null)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG101");
                                    #endregion
                                }
                                else
                                {
                                    _HCUAccountOwner = new HCUAccountOwner();
                                    _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountOwner.AccountId = MerchantDetails.MerchantId;
                                    _HCUAccountOwner.AccountTypeId = UserAccountType.Merchant;
                                    _HCUAccountOwner.OwnerId = _Request.UserReference.AccountId;
                                    _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountOwner.CreatedById = _Request.UserReference.AccountId;
                                    _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCUAccountOwner.Add(_HCUAccountOwner);
                                    _HCoreContext.SaveChanges();
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG102");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG104");
                        #endregion
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "ConnectMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse RemoveMerchant(OThankUGateway.Request _Request)
        {
            #region Declare

            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else
                {
                    if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                    {
                        #region Perform Operations
                        using (_HCoreContext = new HCoreContext())
                        {
                            long MerchantId = _HCoreContext.HCUAccount
                                  .Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == HelperStatus.Default.Active && x.AccountCode == _Request.MerchantId)
                                  .Select(x => x.Id).FirstOrDefault();
                            if (MerchantId != 0)
                            {
                                var MerchantOwnerDetails = _HCoreContext.HCUAccountOwner
                                    .Where(x => x.OwnerId == _Request.UserReference.AccountId &&
                                           x.AccountId == MerchantId && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                                if (MerchantOwnerDetails != null)
                                {
                                    MerchantOwnerDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                    MerchantOwnerDetails.ModifyById = _Request.UserReference.AccountId;
                                    MerchantOwnerDetails.StatusId = HCoreConstant.HelperStatus.Default.Inactive;
                                    _HCoreContext.SaveChanges();

                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG106");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG107");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                                #endregion
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG104");
                        #endregion
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "RemoveMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse GetConfiguration(OThankUGateway.Request _Request)
        {
            #region Declare

            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else
                {
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                        {
                            var MerchantOwnerDetails = _HCoreContext.HCUAccountOwner
                            .Where(x => x.OwnerId == _Request.UserReference.AccountId &&
                            x.Account.AccountCode == _Request.MerchantId &&
                            x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new
                            {
                                MerchantKey = x.Account.Guid,
                            }).FirstOrDefault();
                            if (MerchantOwnerDetails != null)
                            {
                                _GatewayResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantOwnerDetails.MerchantKey));
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG108");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                                #endregion
                            }
                        }
                        else
                        {
                            var MerchantOwnerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.AccountCode == _Request.MerchantId &&
                            x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new
                            {
                                MerchantKey = x.Owner.Owner.Guid,
                            }).FirstOrDefault();
                            if (MerchantOwnerDetails != null)
                            {

                                _GatewayResponse.RewardPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", MerchantOwnerDetails.MerchantKey));
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG108");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "ConnectMerchant", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse ValidateAccount(OThankUGateway.Request _Request)
        {
            #region Declare

            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                    #endregion
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG145", CoreResources.HCG145);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG146", CoreResources.HCG146);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG147", CoreResources.HCG147);
                        #endregion
                    }

                    _OUserInfo = new OUserInfo();
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (!string.IsNullOrEmpty(_Request.CardNumber) || !string.IsNullOrEmpty(_Request.TagNumber))
                        {
                            var UserAccount = _HCoreContext.TUCard
                       .Where(x => x.CardNumber == _Request.CardNumber
                           && x.SerialNumber == _Request.TagNumber)
                           .Select(x => new OTUInfo
                           {
                               MobileNumber = x.ActiveUserAccount.MobileNumber,
                               DisplayName = x.ActiveUserAccount.DisplayName,
                           }).FirstOrDefault();
                            if (UserAccount != null)
                            {
                                _GatewayResponse.Name = UserAccount.DisplayName;
                                _GatewayResponse.MobileNumber = UserAccount.MobileNumber;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG999");
                                #endregion
                            }
                            else
                            {
                                _GatewayResponse.Name = "New user";
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG998");
                                #endregion
                            }
                        }
                        else if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                            _OUserInfo.AccountNumber = _Request.MobileNumber;
                            var UserAccount = _HCoreContext.HCUAccount
                            .Where(x => x.User.Username == _AppConfig.AppUserPrefix + _Request.MobileNumber &&
                                        x.AccountTypeId == UserAccountType.Appuser &&
                                        x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new OThankUGateway.RequestTemp
                            {
                                AccountId = x.Id,
                                AccountType = x.AccountTypeId,
                                DisplayName = x.DisplayName,
                            }).FirstOrDefault();
                            if (UserAccount != null)
                            {

                                _GatewayResponse.Name = UserAccount.DisplayName;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG999");
                                #endregion
                            }
                            else
                            {
                                _GatewayResponse.Name = "New user";
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG998");
                                #endregion
                            }
                        }
                        else if (string.IsNullOrEmpty(_Request.CardNumber))
                        {
                            var UserAccount = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber)
                               .Select(x => new OTUInfo
                               {
                                   MobileNumber = x.ActiveUserAccount.MobileNumber,
                                   DisplayName = x.ActiveUserAccount.DisplayName,
                               }).FirstOrDefault();
                            if (UserAccount != null)
                            {
                                _GatewayResponse.Name = UserAccount.DisplayName;
                                _GatewayResponse.MobileNumber = UserAccount.MobileNumber;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG999");
                                #endregion
                            }
                            else
                            {
                                _GatewayResponse.Name = "New user";
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG998");
                                #endregion
                            }
                        }
                        else if (string.IsNullOrEmpty(_Request.TagNumber))
                        {
                            var UserAccount = _HCoreContext.TUCard
                      .Where(x => x.SerialNumber == _Request.TagNumber)
                          .Select(x => new OTUInfo
                          {
                              MobileNumber = x.ActiveUserAccount.MobileNumber,
                              DisplayName = x.ActiveUserAccount.DisplayName,
                          }).FirstOrDefault();
                            if (UserAccount != null)
                            {
                                _GatewayResponse.Name = UserAccount.DisplayName;
                                _GatewayResponse.MobileNumber = UserAccount.MobileNumber;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG999");
                                #endregion
                            }
                            else
                            {
                                _GatewayResponse.Name = "New user";
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG998");
                                #endregion
                            }
                        }

                        else
                        {
                            _GatewayResponse.Name = "New user";
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG998");
                            #endregion
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "ValidateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse Reward(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                    #endregion
                }
                else if ((_Request.InvoiceAmount / 100) < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
                    #endregion
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG145", CoreResources.HCG145);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG146", CoreResources.HCG146);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG147", CoreResources.HCG147);
                        #endregion
                    }
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    #region Perform Operations
                    if (_OGatewayInfo != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ValidateTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.ReferenceNumber == _Request.ReferenceNumber && x.CreatedBy.OwnerId == _Request.UserReference.AccountId).Select(x => x.Id).FirstOrDefault();
                            if (ValidateTransaction == 0)
                            {
                                _HCoreContext.Dispose();

                                OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo, true);
                                if (_UserInfo != null)
                                {
                                    OAmountDistribution _AmountDistrubutionRequest = GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                                    if (_AmountDistrubutionRequest.RewardPercentage > 0)
                                    {
                                        if (_AmountDistrubutionRequest.RewardAmount > 0)
                                        {
                                            int TransactionTypeId = TransactionType.CardReward;
                                            if (_Request.TransactionMode == "cash")
                                            {
                                                TransactionTypeId = TransactionType.CashReward;
                                            }
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                            _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                            _CoreTransactionRequest.InvoiceAmount = _AmountDistrubutionRequest.InvoiceAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistrubutionRequest.ReferenceInvoiceAmount;
                                            _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                            //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                            if (_OGatewayInfo.AcquirerId != null)
                                            {
                                                _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                            }
                                            _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.MerchantId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Merchant,
                                                Amount = _AmountDistrubutionRequest.User,
                                                Charge = 0,
                                                Comission = _AmountDistrubutionRequest.RewardCommission,
                                                TotalAmount = _AmountDistrubutionRequest.RewardAmount,
                                            });
                                            if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.ThankUCashPlus,
                                                    Amount = _AmountDistrubutionRequest.User,
                                                    Charge = 0,
                                                    TotalAmount = _AmountDistrubutionRequest.User,
                                                });
                                            }
                                            else
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.TUC,
                                                    Amount = _AmountDistrubutionRequest.User,
                                                    Charge = 0,
                                                    TotalAmount = _AmountDistrubutionRequest.User,
                                                });
                                            }

                                            if (_AmountDistrubutionRequest.Pssp > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _Request.UserReference.AccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,
                                                    Amount = _AmountDistrubutionRequest.Pssp,
                                                    Charge = 0,
                                                    TotalAmount = _AmountDistrubutionRequest.Pssp,
                                                });
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _Request.UserReference.AccountId,
                                                    ModeId = TransactionMode.Debit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,
                                                    Amount = _AmountDistrubutionRequest.Pssp,
                                                    Charge = 0,
                                                    TotalAmount = _AmountDistrubutionRequest.Pssp,
                                                });
                                            }
                                            if (_AmountDistrubutionRequest.Acquirer > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,
                                                    Amount = _AmountDistrubutionRequest.Acquirer,
                                                    Charge = 0,
                                                    TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                                });

                                                if (_AmountDistrubutionRequest.AllowAcquirerSettlement == "1")
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionTypeId,
                                                        SourceId = TransactionSource.Settlement,

                                                        Amount = _AmountDistrubutionRequest.Acquirer,
                                                        TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                                    });
                                                }
                                            }
                                            if (_AmountDistrubutionRequest.Issuer > 0)
                                            {
                                                if (_UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.IssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.ReferralBonus,
                                                        SourceId = TransactionSource.TUC,
                                                        Amount = _AmountDistrubutionRequest.Issuer,
                                                        TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                    });
                                                }
                                                else
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.IssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.ReferralBonus,
                                                        SourceId = TransactionSource.Settlement,

                                                        Amount = _AmountDistrubutionRequest.Issuer,
                                                        TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                    });
                                                }
                                            }
                                            if (_AmountDistrubutionRequest.TransactionIssuerTotalAmount > 0)
                                            {
                                                if (_OGatewayInfo.TransactionIssuerAccountTypeId == UserAccountType.Appuser)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.TransactionBonus,
                                                        SourceId = TransactionSource.TUC,

                                                        Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                        Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                        TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                    });
                                                }
                                                else
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.TransactionBonus,
                                                        SourceId = TransactionSource.Settlement,

                                                        Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                        Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                        TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                    });
                                                }
                                            }
                                            if (_AmountDistrubutionRequest.Ptsa > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = SystemAccounts.SmashLabId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.Ptsa,
                                                    TotalAmount = _AmountDistrubutionRequest.Ptsa,
                                                });
                                            }
                                            if (_AmountDistrubutionRequest.ThankUCash > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = SystemAccounts.ThankUCashMerchant,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.ThankUCash,
                                                    TotalAmount = _AmountDistrubutionRequest.ThankUCash,
                                                });
                                            }
                                            if (_AmountDistrubutionRequest.MerchantReverseAmount > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _OGatewayInfo.MerchantId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.MerchantCredit,
                                                    SourceId = TransactionSource.Merchant,

                                                    Amount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                                    Comission = 0,
                                                    TotalAmount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                                });
                                            }
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }
                                        }
                                        else
                                        {
                                            int TransactionTypeId = TransactionType.CardReward;
                                            if (_Request.TransactionMode == "cash")
                                            {
                                                TransactionTypeId = TransactionType.CashReward;
                                            }
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                            _CoreTransactionRequest.InvoiceAmount = _AmountDistrubutionRequest.InvoiceAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistrubutionRequest.ReferenceInvoiceAmount;
                                            _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                            //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                            if (_OGatewayInfo.AcquirerId != null)
                                            {
                                                _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                            }
                                            _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.MerchantId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Merchant,

                                                Amount = 0,
                                                Comission = 0,
                                                TotalAmount = 0,
                                            });
                                            if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.ThankUCashPlus,
                                                    Amount = 0,
                                                    TotalAmount = 0,
                                                });
                                            }
                                            else
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.TUC,

                                                    Amount = 0,
                                                    TotalAmount = 0,
                                                });
                                            }
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG123");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                                    #endregion
                                }

                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG124");
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Reward", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse Change(OThankUGateway.Request _Request)
        {
            #region Send Response
            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100", "Api will be available soon");
            #endregion
            //#region Declare
            //_GatewayResponse = new OThankUGateway.Response();
            //#endregion
            //#region Manage Exception
            //try
            //{
            //    if (string.IsNullOrEmpty(_Request.TerminalId))
            //    {
            //        #region Send Response
            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
            //        #endregion
            //    }
            //    else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber))
            //    {
            //        #region Send Response
            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
            //        #endregion
            //    }
            //    else if ((_Request.ChangeAmount / 100) < 1)
            //    {
            //        #region Send Response
            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG130");
            //        #endregion
            //    }
            //    else
            //    {
            //        #region Perform Operations
            //        using (_HCoreContext = new HCoreContext())
            //        {
            //            OGatewayInfo _OGatewayInfo = null;
            //            if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
            //            {
            //                #region Send Response
            //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG104");
            //                #endregion
            //            }
            //            else
            //            {
            //                _OGatewayInfo = _HCoreContext.HCUAccount
            //                                             .Where(x => x.AccountTypeId == UserAccountType.TerminalAccount &&
            //                            x.Owner.StatusId == HelperStatus.Default.Active &&
            //                            x.StatusId == HelperStatus.Default.Active &&
            //                            x.Username == _Request.TerminalId)
            //                .Select(x => new OGatewayInfo
            //                {
            //                    TerminalId = x.Id,
            //                    TerminalKey = x.Guid,
            //                    TerminalDisplayName = x.DisplayName,
            //                    MerchantId = x.Owner.Id,
            //                    //MerchantKey = x.Owner.Guid,
            //                    MerchantDisplayName = x.Owner.DisplayName
            //                }).FirstOrDefault();
            //            }
            //            if (_OGatewayInfo != null)
            //            {
            //                _ManageCoreTransaction = new ManageCoreTransaction();
            //                double CashierBalance = (_ManageCoreTransaction.GetUserAccountBalance(_OGatewayInfo.TerminalId, TransactionSource.Cashier));
            //                string AccountNumber = null;
            //                string ReferenceNumber = _Request.ReferenceNumber;
            //                string Data = JsonConvert.SerializeObject(_Request);
            //                double InvoiceAmount = _Request.InvoiceAmount / 100;
            //                double ChangeAmount = _Request.ChangeAmount / 100;
            //                double ChangePoints = Math.Floor(ChangeAmount) * 10;// HCoreHelper.RoundNumber((double)(_Request.ChangeAmount / 100)) * 10;
            //                if (ChangePoints < (CashierBalance + 1))
            //                {
            //                    long UserAccountId = 0;
            //                    string UserAccountKey = null;
            //                    long UserMerchantId = 0;
            //                    string UserMerchantKey = null;
            //                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
            //                    {
            //                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
            //                        AccountNumber = _Request.MobileNumber;
            //                        var UserAccount = _HCoreContext.HCUAccount
            //                        .Where(x => x.Username == _AppConfig.AppUserPrefix + _Request.MobileNumber &&
            //                                    x.AccountTypeId == UserAccountType.Appuser &&
            //                                    x.StatusId == HelperStatus.Default.Active)
            //                        .Select(x => new
            //                        {
            //                            UserId = x.UserId,
            //                            AccountId = x.Id,
            //                            AccountKey = x.Guid,
            //                            AccountType = x.AccountType,
            //                            DisplayName = x.DisplayName,
            //                        }).FirstOrDefault();
            //                        if (UserAccount != null)
            //                        {
            //                            UserAccountId = UserAccount.AccountId;
            //                            UserAccountKey = UserAccount.AccountKey;
            //                        }
            //                        else
            //                        {
            //                            _OCreateUserAccount = new OCreateUserAccount();
            //                            _OCreateUserAccount.AccountId = 0;
            //                            _OCreateUser = new OCreateUser();
            //                            _OCreateUser.MobileNumber = _Request.MobileNumber;
            //                            _OCreateUser.DisplayName = _Request.MobileNumber;
            //                            _OCreateUser.AppAccount = _OCreateUserAccount;
            //                            _OCreateUser.UserReference = _Request.UserReference;
            //                            _ManageUserAccountAccess = new ManageUserAccountAccess();
            //                            OCreateUser AccountDetails = _ManageUserAccountAccess.CreateAccount(_OCreateUser);
            //                            UserAccountId = AccountDetails.AppAccount.AccountId;
            //                            UserAccountKey = AccountDetails.AppAccount.AccountKey;
            //                        }
            //                    }
            //                    else if (!string.IsNullOrEmpty(_Request.CardNumber))
            //                    {
            //                        AccountNumber = _Request.CardNumber;
            //                        var CardAccount = _HCoreContext.HCUAccount
            //                            .Where(x => x.AccountCode == _Request.CardNumber &&
            //                                    x.AccountTypeId == UserAccountType.Appuser &&
            //                                    x.StatusId == HelperStatus.Default.Active)
            //                            .Select(x => new
            //                            {
            //                                UserId = x.UserId,
            //                                AccountId = x.Id,
            //                                AccountKey = x.Guid,
            //                                AccountType = x.AccountTypeId,
            //                                DisplayName = x.DisplayName,
            //                                UserMerchantId = x.Owner.Owner.Id,
            //                                UserMerchantKey = x.Owner.Owner.Guid,
            //                                Pin = x.AccessPin
            //                            }).FirstOrDefault();
            //                        if (CardAccount != null)
            //                        {
            //                            if (CardAccount.AccountType == UserAccountType.Appuser)
            //                            {
            //                                var OnlineAccountId = _HCoreContext.HCUAccount
            //                                                    .Where(x => x.UserId == CardAccount.UserId &&
            //                                                    x.AccountTypeId == UserAccountType.Appuser &&
            //                                                    x.StatusId == HelperStatus.Default.Active)
            //                                             .Select(x => new
            //                                             {
            //                                                 UserId = x.UserId,
            //                                                 AccountId = x.Id,
            //                                                 AccountKey = x.Guid,
            //                                                 AccountType = x.AccountType,
            //                                                 DisplayName = x.DisplayName,
            //                                                 UserMerchantId = x.Owner.Owner.Id,
            //                                                 UserMerchantKey = x.Owner.Owner.Guid,
            //                                             }).FirstOrDefault();
            //                                if (OnlineAccountId != null)
            //                                {
            //                                    UserAccountId = OnlineAccountId.AccountId;
            //                                    UserAccountKey = OnlineAccountId.AccountKey;
            //                                    UserMerchantId = OnlineAccountId.UserMerchantId;
            //                                    UserMerchantKey = OnlineAccountId.UserMerchantKey;
            //                                }
            //                                else
            //                                {
            //                                    #region Send Response
            //                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG117");
            //                                    #endregion
            //                                }
            //                            }
            //                            else
            //                            {
            //                                UserAccountId = CardAccount.AccountId;
            //                                UserAccountKey = CardAccount.AccountKey;
            //                                UserMerchantId = CardAccount.UserMerchantId;
            //                                UserMerchantKey = CardAccount.UserMerchantKey;
            //                            }
            //                        }
            //                        else
            //                        {
            //                            #region Send Response
            //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG117");
            //                            #endregion
            //                        }
            //                    }
            //                    else
            //                    {
            //                        #region Send Response
            //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG109");
            //                        #endregion
            //                    }
            //                    double ChangeComissionPercentage = Convert.ToDouble(HCoreHelper.GetConfiguration("changecomissionpercentage", _OGatewayInfo.MerchantId));
            //                    double ChangeComissionAmount = HCoreHelper.GetPercentage(ChangeAmount, ChangeComissionPercentage, 2);// HCoreHelper.RoundNumber(((ChangePercentage * ChangeAmount) / 100), 2);
            //                    double UserAmount = Math.Floor(ChangeAmount - ChangeComissionAmount);
            //                    // double UserPoints = UserAmount * 10;
            //                    double PayableAmount = ChangeAmount;
            //                    #region Process Transaction - DEBIT FROM CASHIER
            //                    _OSaveTransaction = new OSaveTransaction();
            //                    _OSaveTransaction.UserReference = _Request.UserReference;
            //                    _OSaveTransaction.UserAccountId = _OGatewayInfo.TerminalId;
            //                    _OSaveTransaction.AccountNumber = _Request.SixDigitPan;
            //                    _OSaveTransaction.ModeId = TransactionMode.Debit;
            //                    _OSaveTransaction.TypeId = TransactionType.Change;
            //                    _OSaveTransaction.SourceId = TransactionSource.Cashier;
            //                    _OSaveTransaction.Amount = UserAmount;
            //                    _OSaveTransaction.AmountPercentage = HCoreHelper.GetAmountPercentage(UserAmount, InvoiceAmount);
            //                    _OSaveTransaction.Charge = 0;
            //                    _OSaveTransaction.ChargePercentage = 0;
            //                    _OSaveTransaction.ComissionAmount = ChangeComissionAmount;
            //                    _OSaveTransaction.ComissionPercentage = ChangeComissionPercentage;
            //                    _OSaveTransaction.PayableAmount = PayableAmount;
            //                    _OSaveTransaction.TotalAmount = PayableAmount;
            //                    _OSaveTransaction.TotalAmountPercentage = HCoreHelper.GetAmountPercentage(PayableAmount, InvoiceAmount); ;
            //                    _OSaveTransaction.PurchaseAmount = InvoiceAmount;
            //                    _OSaveTransaction.ReferenceAmount = InvoiceAmount;
            //                    _OSaveTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
            //                    _OSaveTransaction.ParentId = _OGatewayInfo.MerchantId;
            //                    _OSaveTransaction.AccountNumber = AccountNumber;
            //                    _OSaveTransaction.ReferenceNumber = ReferenceNumber;
            //                    _OSaveTransaction.Data = Data;
            //                    _OSaveTransaction.Status = HCoreConstant.HelperStatus.Transaction.Success;
            //                    _ManageCoreTransaction = new ManageCoreTransaction();
            //                    var TrCashierDebitResponse = _ManageCoreTransaction.ProcessTransaction(_OSaveTransaction);
            //                    #endregion
            //                    #region Process Transaction - CREDIT FROM CASHIER
            //                    _OSaveTransaction = new OSaveTransaction();
            //                    _OSaveTransaction.UserReference = _Request.UserReference;
            //                    _OSaveTransaction.GroupId = TrCashierDebitResponse.ReferenceId;
            //                    _OSaveTransaction.GroupKey = TrCashierDebitResponse.ReferenceKey;
            //                    _OSaveTransaction.ParentTransactionId = TrCashierDebitResponse.ReferenceId;
            //                    _OSaveTransaction.UserAccountId = UserAccountId;
            //                    _OSaveTransaction.ModeId = TransactionMode.Credit;
            //                    _OSaveTransaction.TypeId = TransactionType.Change;
            //                    _OSaveTransaction.SourceId = TransactionSource.TUC;
            //                    _OSaveTransaction.Amount = UserAmount;
            //                    _OSaveTransaction.AmountPercentage = HCoreHelper.GetAmountPercentage(UserAmount, InvoiceAmount);
            //                    _OSaveTransaction.Charge = 0;
            //                    _OSaveTransaction.ChargePercentage = 0;
            //                    _OSaveTransaction.ComissionAmount = 0;
            //                    _OSaveTransaction.ComissionPercentage = 0;
            //                    _OSaveTransaction.PayableAmount = UserAmount;
            //                    _OSaveTransaction.TotalAmount = UserAmount;
            //                    _OSaveTransaction.TotalAmountPercentage = HCoreHelper.GetAmountPercentage(UserAmount, InvoiceAmount);
            //                    _OSaveTransaction.PurchaseAmount = InvoiceAmount;
            //                    _OSaveTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
            //                    _OSaveTransaction.ParentId = _OGatewayInfo.MerchantId;
            //                    _OSaveTransaction.AccountNumber = _Request.SixDigitPan;
            //                    _OSaveTransaction.ReferenceNumber = ReferenceNumber;
            //                    _OSaveTransaction.Data = Data;
            //                    _OSaveTransaction.Status = HCoreConstant.HelperStatus.Transaction.Success;
            //                    _ManageCoreTransaction = new ManageCoreTransaction();
            //                    var TrUserCreditResponse = _ManageCoreTransaction.ProcessTransaction(_OSaveTransaction);
            //                    #endregion
            //                    if (ChangeComissionAmount > 0)
            //                    {
            //                        #region Process Transaction -   CREDIT TO THANK U USER
            //                        _OSaveTransaction = new OSaveTransaction();
            //                        _OSaveTransaction.UserReference = _Request.UserReference;
            //                        _OSaveTransaction.GroupId = TrCashierDebitResponse.ReferenceId;
            //                        _OSaveTransaction.GroupKey = TrCashierDebitResponse.ReferenceKey;
            //                        _OSaveTransaction.ParentTransactionId = TrUserCreditResponse.ReferenceId;
            //                        _OSaveTransaction.UserAccountId = SystemAccounts.ThankUCashMerchant;
            //                        _OSaveTransaction.ModeId = TransactionMode.Credit;
            //                        _OSaveTransaction.TypeId = TransactionType.Change;
            //                        _OSaveTransaction.SourceId = TransactionSource.Settlement;
            //                        _OSaveTransaction.AmountPercentage = ChangeComissionPercentage;
            //                        _OSaveTransaction.Charge = 0;
            //                        _OSaveTransaction.ChargePercentage = 0;
            //                        _OSaveTransaction.ComissionAmount = 0;
            //                        _OSaveTransaction.ComissionPercentage = 0;
            //                        _OSaveTransaction.PayableAmount = ChangeComissionAmount;
            //                        _OSaveTransaction.TotalAmount = ChangeComissionAmount;
            //                        _OSaveTransaction.TotalAmountPercentage = ChangeComissionAmount;
            //                        _OSaveTransaction.PurchaseAmount = InvoiceAmount;
            //                        _OSaveTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();

            //                        _OSaveTransaction.ParentId = _OGatewayInfo.MerchantId;

            //                        _OSaveTransaction.AccountNumber = _Request.SixDigitPan;
            //                        _OSaveTransaction.ReferenceNumber = ReferenceNumber;
            //                        _OSaveTransaction.Data = Data;

            //                        _OSaveTransaction.Status = HCoreConstant.HelperStatus.Transaction.Success;
            //                        _ManageCoreTransaction = new ManageCoreTransaction();
            //                        var TrThankUCreditCreditResponse = _ManageCoreTransaction.ProcessTransaction(_OSaveTransaction);
            //                        #endregion
            //                    }
            //                    _ManageCoreTransaction = new ManageCoreTransaction();
            //                    _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
            //                    _GatewayResponse.TransactionReference = TrCashierDebitResponse.ReferenceKey;
            //                    _GatewayResponse.Balance = (long)(HCoreHelper.RoundNumber((_ManageCoreTransaction.GetAppUserBalance(UserAccountId, _Request.UserReference)), 2) * 100);
            //                    _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
            //                    #region Send Response
            //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG119");
            //                    #endregion
            //                }
            //                else
            //                {
            //                    #region Send Response
            //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120");
            //                    #endregion
            //                }
            //            }
            //            else
            //            {
            //                #region Send Response
            //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
            //                #endregion
            //            }
            //        }
            //        #endregion
            //    }
            //}
            //catch (Exception _Exception)
            //{
            //    #region  Log Exception
            //    HCoreHelper.LogException(LogLevel.High, "Reward", _Exception, _Request.UserReference);
            //    #endregion
            //    #region Send Response
            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
            //    #endregion
            //}
            //#endregion
        }
        internal OResponse InitializeTransaction(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (!string.IsNullOrEmpty(_Request.TransactionMode))
                {
                    _Request.TransactionMode = _Request.TransactionMode.ToLower();
                }
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response 
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG551", CoreResources.HCG551);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109", CoreResources.HCG109);
                    #endregion 
                }
                else if (_Request.InvoiceAmount < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116", CoreResources.HCG116);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115", CoreResources.HCG115);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.CashierId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG155", CoreResources.HCG155);
                    #endregion
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG145", CoreResources.HCG145);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG146", CoreResources.HCG146);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG147", CoreResources.HCG147);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.TransactionMode) && (_Request.TransactionMode != "card" && _Request.TransactionMode != "cash"))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG148", CoreResources.HCG148);
                        #endregion
                    }
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    if (_OGatewayInfo != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.CashierId))
                        {
                            if (_OGatewayInfo.CashierId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG134", CoreResources.HCG134);
                                #endregion
                            }
                            else if (_OGatewayInfo.CashierId != 0 && _OGatewayInfo.CashierStatusId != HelperStatus.Default.Active)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG135", CoreResources.HCG135);
                                #endregion
                            }
                        }
                        OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo, true);
                        if (_UserInfo != null)
                        {
                            if (_Request.TransactionMode == "cash")
                            {
                                var SettleTransactionResponse = SettleTransaction(_Request, true, _UserInfo);
                                return SettleTransactionResponse;
                            }
                            else
                            {
                                if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                                {
                                    OAmountDistribution _AmountDistributionResponse = GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                                    _GatewayResponse.TransactionReference = HCoreHelper.GenerateGuid();
                                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                    _GatewayResponse.RewardPercentage = _AmountDistributionResponse.RewardPercentage;
                                    _GatewayResponse.RewardAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.RewardAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.UserRewardAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.User, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.AcquirerAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.Acquirer, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.MerchantAmount = (long)(((double)HCoreHelper.RoundNumber(_AmountDistributionResponse.MerchantAmount, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    _GatewayResponse.CommissionAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.RewardCommission, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
                                    {
                                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Pssp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    }
                                    else
                                    {
                                        _GatewayResponse.Commission = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                        _GatewayResponse.ProviderAmount = (long)((HCoreHelper.RoundNumber(_AmountDistributionResponse.Ptsp, _AppConfig.SystemExitRoundDouble)) * _AppConfig.SystemMultiplyByDigit);
                                    }
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG502");
                                    #endregion
                                }
                                else
                                {
                                    OAmountDistribution _AmountDistributionResponse = GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                                    _GatewayResponse.TransactionReference = HCoreHelper.GenerateGuid();
                                    _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
                                    _GatewayResponse.RewardPercentage = 0;
                                    _GatewayResponse.RewardAmount = 0;
                                    _GatewayResponse.UserRewardAmount = 0;
                                    _GatewayResponse.AcquirerAmount = 0;
                                    _GatewayResponse.MerchantAmount = 0;
                                    _GatewayResponse.CommissionAmount = 0;
                                    _GatewayResponse.Commission = 0;
                                    _GatewayResponse.ProviderAmount = 0;
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG502");
                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                        #endregion
                    }

                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "InitializeTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        OUserInfo _UserInfo;
        internal OResponse SettleTransaction(OThankUGateway.Request _Request, bool AllowCash, OUserInfo UserInfo)
        {
            if (!string.IsNullOrEmpty(_Request.TransactionMode))
            {
                _Request.TransactionMode = _Request.TransactionMode.ToLower();
            }
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG551", CoreResources.HCG551);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109", CoreResources.HCG109);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115", CoreResources.HCG115);
                    #endregion
                }
                else if (!string.IsNullOrEmpty(_Request.TransactionMode) && (_Request.TransactionMode != "card" && _Request.TransactionMode != "cash"))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG148", CoreResources.HCG148);
                }
                else if (AllowCash == false && _Request.TransactionMode == "cash")
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG153", CoreResources.HCG153);
                    #endregion
                }
                else if (_Request.TransactionMode == "card" && string.IsNullOrEmpty(_Request.TransactionReference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG152", CoreResources.HCG152);
                    #endregion
                }
                else if (_Request.TransactionMode == "card" && string.IsNullOrEmpty(_Request.bin) && string.IsNullOrEmpty(_Request.SixDigitPan))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG154", CoreResources.HCG154);
                    #endregion
                }
                else if (_Request.InvoiceAmount < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116", CoreResources.HCG116);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.CashierId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG155", CoreResources.HCG155);
                    #endregion
                }
                else
                {

                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG145", CoreResources.HCG145);
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG146", CoreResources.HCG146);
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG147", CoreResources.HCG147);
                    }
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    #region Perform Operations
                    if (_OGatewayInfo != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.CashierId))
                        {
                            if (_OGatewayInfo.CashierId == 0)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG134", CoreResources.HCG134);
                            }
                            else if (_OGatewayInfo.CashierId != 0 && _OGatewayInfo.CashierStatusId != HelperStatus.Default.Active)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG135", CoreResources.HCG135);
                            }
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ValidateTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.ReferenceNumber == _Request.ReferenceNumber && x.CreatedBy.OwnerId == _Request.UserReference.AccountId).Select(x => x.Id).FirstOrDefault();
                            if (ValidateTransaction == 0)
                            {
                                _HCoreContext.Dispose();
                                _UserInfo = new OUserInfo();
                                if (UserInfo != null)
                                {
                                    _UserInfo = UserInfo;
                                }
                                else
                                {
                                    _UserInfo = GetUserInfo(_Request, _OGatewayInfo, true);
                                }


                                //OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo, true);
                                if (_UserInfo != null)
                                {
                                    OAmountDistribution _AmountDistrubutionRequest = GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                                    if (_AmountDistrubutionRequest.RewardAmount > 0)
                                    {
                                        int TransactionTypeId = TransactionType.CardReward;
                                        switch (_Request.TransactionMode)
                                        {
                                            case "cash":
                                                TransactionTypeId = TransactionType.CashReward;
                                                break;
                                            case "card":
                                                TransactionTypeId = TransactionType.CardReward;
                                                break;
                                            case "redeemreward":
                                                TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                                break;
                                            default:
                                                TransactionTypeId = TransactionType.CashReward;
                                                if (!string.IsNullOrEmpty(_Request.bin) || !string.IsNullOrEmpty(_Request.SixDigitPan))
                                                {
                                                    TransactionTypeId = TransactionType.CardReward;
                                                }
                                                break;
                                        }
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                        _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                        _CoreTransactionRequest.InvoiceAmount = _AmountDistrubutionRequest.InvoiceAmount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistrubutionRequest.ReferenceInvoiceAmount;
                                        if (!string.IsNullOrEmpty(_Request.bin))
                                        {
                                            _CoreTransactionRequest.AccountNumber = _Request.bin;
                                        }
                                        else
                                        {
                                            _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                        }
                                        _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                        _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                        //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                        if (_OGatewayInfo.AcquirerId != null)
                                        {
                                            _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                        }
                                        _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                        _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _OGatewayInfo.MerchantId,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Merchant,

                                            Amount = _AmountDistrubutionRequest.User,
                                            Comission = _AmountDistrubutionRequest.RewardCommission,
                                            TotalAmount = _AmountDistrubutionRequest.RewardAmount,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                        if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _UserInfo.UserAccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.ThankUCashPlus,
                                                Amount = _AmountDistrubutionRequest.User,
                                                TotalAmount = _AmountDistrubutionRequest.User,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                        else
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _UserInfo.UserAccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.TUC,

                                                Amount = _AmountDistrubutionRequest.User,
                                                TotalAmount = _AmountDistrubutionRequest.User,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                        if (_AmountDistrubutionRequest.Ptsp > 0)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.UserReference.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Settlement,

                                                Amount = _AmountDistrubutionRequest.Ptsp,
                                                TotalAmount = _AmountDistrubutionRequest.Ptsp,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                        if (_AmountDistrubutionRequest.Acquirer > 0)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Settlement,

                                                Amount = _AmountDistrubutionRequest.Acquirer,
                                                TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                            if (_AmountDistrubutionRequest.AllowAcquirerSettlement == "1")
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                                    ModeId = TransactionMode.Debit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.Acquirer,
                                                    TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                                    TransactionDate = _Request.TransactionDate,
                                                });
                                            }
                                        }
                                        if (_AmountDistrubutionRequest.Issuer > 0)
                                        {
                                            if (_UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.IssuerId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.ReferralBonus,
                                                    SourceId = TransactionSource.TUC,
                                                    Amount = _AmountDistrubutionRequest.Issuer,
                                                    TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                    TransactionDate = _Request.TransactionDate,
                                                });
                                            }
                                            else
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.IssuerId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.ReferralBonus,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.Issuer,
                                                    TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                    TransactionDate = _Request.TransactionDate,
                                                });
                                            }
                                        }
                                        if (_AmountDistrubutionRequest.TransactionIssuerTotalAmount > 0)
                                        {
                                            if (_OGatewayInfo.TransactionIssuerAccountTypeId == UserAccountType.Appuser)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.TransactionBonus,
                                                    SourceId = TransactionSource.TUC,

                                                    Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                    Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                    TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                    TransactionDate = _Request.TransactionDate,
                                                });
                                            }
                                            else
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.TransactionBonus,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                    Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                    TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                    TransactionDate = _Request.TransactionDate,
                                                });
                                            }
                                        }
                                        if (_AmountDistrubutionRequest.Ptsa > 0)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = SystemAccounts.SmashLabId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Settlement,

                                                Amount = _AmountDistrubutionRequest.Ptsa,
                                                TotalAmount = _AmountDistrubutionRequest.Ptsa,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                        if (_AmountDistrubutionRequest.ThankUCash > 0)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = SystemAccounts.ThankUCashMerchant,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Settlement,

                                                Amount = _AmountDistrubutionRequest.ThankUCash,
                                                TotalAmount = _AmountDistrubutionRequest.ThankUCash,
                                                TransactionDate = _Request.TransactionDate,

                                            });
                                        }
                                        if (_AmountDistrubutionRequest.MerchantReverseAmount > 0)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.MerchantId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.MerchantCredit,
                                                SourceId = TransactionSource.Merchant,

                                                Amount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                                Comission = 0,
                                                TotalAmount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {
                                            _OCampaignProcessor = new OCampaignProcessor();
                                            _OCampaignProcessor._GatewayInfo = _OGatewayInfo;
                                            _OCampaignProcessor._UserInfo = _UserInfo;
                                            _OCampaignProcessor._UserRequest = _Request;
                                            _OCampaignProcessor.AccountNumber = _CoreTransactionRequest.AccountNumber;
                                            #region TransactionPostProcess
                                            var _TransactionPostProcessActor = ActorSystem.Create("AccountOtherCardCampaignProcessorActor");
                                            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<AccountOtherCardCampaignProcessorActor>("AccountOtherCardCampaignProcessorActor");
                                            _TransactionPostProcessActorNotify.Tell(_OCampaignProcessor);
                                            #endregion
                                            return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        string Comment = null;
                                        if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                                        {
                                            Comment = "No Reward";
                                        }
                                        else if (_UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
                                        {
                                            Comment = "User account suspended";
                                        }
                                        else
                                        {
                                            Comment = "User account blocked";
                                        }
                                        int TransactionTypeId = TransactionType.CardReward;
                                        switch (_Request.TransactionMode)
                                        {
                                            case "cash":
                                                TransactionTypeId = TransactionType.CashReward;
                                                break;
                                            case "card":
                                                TransactionTypeId = TransactionType.CardReward;
                                                break;
                                            case "redeemreward":
                                                TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                                break;
                                            default:
                                                TransactionTypeId = TransactionType.CashReward;
                                                if (!string.IsNullOrEmpty(_Request.bin) || !string.IsNullOrEmpty(_Request.SixDigitPan))
                                                {
                                                    TransactionTypeId = TransactionType.CardReward;
                                                }
                                                break;
                                        }
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                        _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                        _CoreTransactionRequest.InvoiceAmount = _AmountDistrubutionRequest.InvoiceAmount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistrubutionRequest.ReferenceInvoiceAmount;
                                        _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                        _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                        _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                        _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                        if (!string.IsNullOrEmpty(_Request.bin))
                                        {
                                            _CoreTransactionRequest.AccountNumber = _Request.bin;
                                        }
                                        else
                                        {
                                            _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                        }
                                        //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                        if (_OGatewayInfo.AcquirerId != null)
                                        {
                                            _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                        }
                                        _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                        _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _OGatewayInfo.MerchantId,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Merchant,

                                            Amount = 0,
                                            Comission = 0,
                                            TotalAmount = 0,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                        if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _UserInfo.UserAccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.ThankUCashPlus,
                                                Amount = 0,
                                                TotalAmount = 0,
                                                TransactionDate = _Request.TransactionDate,
                                                Comment = Comment,
                                            });
                                        }
                                        else
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _UserInfo.UserAccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.TUC,

                                                Amount = 0,
                                                TotalAmount = 0,
                                                TransactionDate = _Request.TransactionDate,
                                                Comment = Comment,
                                            });
                                        }
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {
                                            _OCampaignProcessor = new OCampaignProcessor();
                                            _OCampaignProcessor._GatewayInfo = _OGatewayInfo;
                                            _OCampaignProcessor._UserInfo = _UserInfo;
                                            _OCampaignProcessor._UserRequest = _Request;
                                            _OCampaignProcessor.AccountNumber = _CoreTransactionRequest.AccountNumber;
                                            #region TransactionPostProcess
                                            var _TransactionPostProcessActor = ActorSystem.Create("AccountOtherCardCampaignProcessorActor");
                                            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<AccountOtherCardCampaignProcessorActor>("AccountOtherCardCampaignProcessorActor");
                                            _TransactionPostProcessActorNotify.Tell(_OCampaignProcessor);
                                            #endregion
                                            return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                            #endregion
                                        }
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                                    #endregion
                                }
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG124", CoreResources.HCG124);
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse RewardRedeemTransaction(OThankUGateway.Request _Request, OGatewayInfo _OGatewayInfo, string? GroupKey, double RedeemAmount, double InvoiceAmount, bool AllowCash)
        {
            if (!string.IsNullOrEmpty(_Request.TransactionMode))
            {
                _Request.TransactionMode = _Request.TransactionMode.ToLower();
            }
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                    #endregion
                }
                else if (AllowCash == false && _Request.TransactionMode == "cash")
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                    #endregion
                }
                else if (_Request.InvoiceAmount < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
                    #endregion
                }
                else
                {
                    OUserInfo _UserInfo = FrameworkOperations.GetUserInfo(_Request, _OGatewayInfo, false);
                    if (_UserInfo != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ValidateTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.ReferenceNumber == _Request.ReferenceNumber && x.CreatedBy.OwnerId == _Request.UserReference.AccountId).Select(x => x.Id).FirstOrDefault();
                            if (ValidateTransaction == 0)
                            {
                                _HCoreContext.Dispose();
                                OAmountDistribution _AmountDistrubutionRequest = GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                                if (_AmountDistrubutionRequest.RewardAmount > 0)
                                {
                                    int TransactionTypeId = TransactionType.CardReward;
                                    switch (_Request.TransactionMode)
                                    {
                                        case "cash":
                                            TransactionTypeId = TransactionType.CashReward;
                                            break;
                                        case "card":
                                            TransactionTypeId = TransactionType.CardReward;
                                            break;
                                        case "redeemreward":
                                            TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                            break;
                                        default:
                                            TransactionTypeId = TransactionType.CashReward;
                                            if (!string.IsNullOrEmpty(_Request.bin) || !string.IsNullOrEmpty(_Request.SixDigitPan))
                                            {
                                                TransactionTypeId = TransactionType.CardReward;
                                            }
                                            break;
                                    }
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                    _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                    _CoreTransactionRequest.InvoiceAmount = (InvoiceAmount - RedeemAmount);
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                    if (!string.IsNullOrEmpty(_Request.bin))
                                    {
                                        _CoreTransactionRequest.AccountNumber = _Request.bin;
                                    }
                                    else
                                    {
                                        _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                    }
                                    _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                    _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                    //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                    if (_OGatewayInfo.AcquirerId != null)
                                    {
                                        _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                    }
                                    _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                    _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _OGatewayInfo.MerchantId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.Merchant,

                                        Amount = _AmountDistrubutionRequest.User,
                                        Comission = _AmountDistrubutionRequest.RewardCommission,
                                        TotalAmount = _AmountDistrubutionRequest.RewardAmount,
                                        TransactionDate = _Request.TransactionDate,
                                    });
                                    if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.ThankUCashPlus,
                                            Amount = _AmountDistrubutionRequest.User,
                                            TotalAmount = _AmountDistrubutionRequest.User,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                    }
                                    else
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.TUC,

                                            Amount = _AmountDistrubutionRequest.User,
                                            TotalAmount = _AmountDistrubutionRequest.User,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                    }

                                    if (_AmountDistrubutionRequest.Ptsp > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _Request.UserReference.AccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = _AmountDistrubutionRequest.Ptsp,
                                            TotalAmount = _AmountDistrubutionRequest.Ptsp,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                    }
                                    if (_AmountDistrubutionRequest.Acquirer > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = _AmountDistrubutionRequest.Acquirer,
                                            TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                        if (_AmountDistrubutionRequest.AllowAcquirerSettlement == "1")
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Settlement,

                                                Amount = _AmountDistrubutionRequest.Acquirer,
                                                TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                    }
                                    if (_AmountDistrubutionRequest.Issuer > 0)
                                    {
                                        if (_UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _UserInfo.IssuerId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.ReferralBonus,
                                                SourceId = TransactionSource.TUC,
                                                Amount = _AmountDistrubutionRequest.Issuer,
                                                TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                        else
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _UserInfo.IssuerId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.ReferralBonus,
                                                SourceId = TransactionSource.Settlement,

                                                Amount = _AmountDistrubutionRequest.Issuer,
                                                TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                    }
                                    if (_AmountDistrubutionRequest.TransactionIssuerTotalAmount > 0)
                                    {
                                        if (_OGatewayInfo.TransactionIssuerAccountTypeId == UserAccountType.Appuser)
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.TransactionBonus,
                                                SourceId = TransactionSource.TUC,

                                                Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                        else
                                        {
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.TransactionBonus,
                                                SourceId = TransactionSource.Settlement,

                                                Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                TransactionDate = _Request.TransactionDate,
                                            });
                                        }
                                    }
                                    if (_AmountDistrubutionRequest.Ptsa > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = SystemAccounts.SmashLabId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = _AmountDistrubutionRequest.Ptsa,
                                            TotalAmount = _AmountDistrubutionRequest.Ptsa,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                    }
                                    if (_AmountDistrubutionRequest.ThankUCash > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = SystemAccounts.ThankUCashMerchant,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = _AmountDistrubutionRequest.ThankUCash,
                                            TotalAmount = _AmountDistrubutionRequest.ThankUCash,
                                            TransactionDate = _Request.TransactionDate,

                                        });
                                    }
                                    if (_AmountDistrubutionRequest.MerchantReverseAmount > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _OGatewayInfo.MerchantId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.MerchantCredit,
                                            SourceId = TransactionSource.Merchant,

                                            Amount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                            Comission = 0,
                                            TotalAmount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                            TransactionDate = _Request.TransactionDate,
                                        });
                                    }
                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                    {
                                        _OCampaignProcessor = new OCampaignProcessor();
                                        _OCampaignProcessor._GatewayInfo = _OGatewayInfo;
                                        _OCampaignProcessor._UserInfo = _UserInfo;
                                        _OCampaignProcessor._UserRequest = _Request;
                                        _OCampaignProcessor.AccountNumber = _CoreTransactionRequest.AccountNumber;
                                        #region TransactionPostProcess
                                        var _TransactionPostProcessActor = ActorSystem.Create("AccountOtherCardCampaignProcessorActor");
                                        var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<AccountOtherCardCampaignProcessorActor>("AccountOtherCardCampaignProcessorActor");
                                        _TransactionPostProcessActorNotify.Tell(_OCampaignProcessor);
                                        #endregion
                                        return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    string Comment = null;
                                    if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                                    {
                                        Comment = "No Reward";
                                    }
                                    else if (_UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
                                    {
                                        Comment = "User account suspended";
                                    }
                                    else
                                    {
                                        Comment = "User account blocked";
                                    }
                                    int TransactionTypeId = TransactionType.CardReward;
                                    switch (_Request.TransactionMode)
                                    {
                                        case "cash":
                                            TransactionTypeId = TransactionType.CashReward;
                                            break;
                                        case "card":
                                            TransactionTypeId = TransactionType.CardReward;
                                            break;
                                        case "redeemreward":
                                            TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                            break;
                                        default:
                                            TransactionTypeId = TransactionType.CashReward;
                                            if (!string.IsNullOrEmpty(_Request.bin) || !string.IsNullOrEmpty(_Request.SixDigitPan))
                                            {
                                                TransactionTypeId = TransactionType.CardReward;
                                            }
                                            break;
                                    }
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                    _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                    _CoreTransactionRequest.InvoiceAmount = (InvoiceAmount - RedeemAmount);
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                    _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                    _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                    _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                    _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                    if (!string.IsNullOrEmpty(_Request.bin))
                                    {
                                        _CoreTransactionRequest.AccountNumber = _Request.bin;
                                    }
                                    else
                                    {
                                        _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                    }
                                    //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                    if (_OGatewayInfo.AcquirerId != null)
                                    {
                                        _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                    }
                                    _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                    _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _OGatewayInfo.MerchantId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.Merchant,

                                        Amount = 0,
                                        Comission = 0,
                                        TotalAmount = 0,
                                        TransactionDate = _Request.TransactionDate,
                                    });
                                    if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.ThankUCashPlus,
                                            Amount = 0,
                                            TotalAmount = 0,
                                            TransactionDate = _Request.TransactionDate,
                                            Comment = Comment,
                                        });
                                    }
                                    else
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.TUC,

                                            Amount = 0,
                                            TotalAmount = 0,
                                            TransactionDate = _Request.TransactionDate,
                                            Comment = Comment,
                                        });
                                    }
                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                    {
                                        _OCampaignProcessor = new OCampaignProcessor();
                                        _OCampaignProcessor._GatewayInfo = _OGatewayInfo;
                                        _OCampaignProcessor._UserInfo = _UserInfo;
                                        _OCampaignProcessor._UserRequest = _Request;
                                        _OCampaignProcessor.AccountNumber = _CoreTransactionRequest.AccountNumber;
                                        #region TransactionPostProcess
                                        var _TransactionPostProcessActor = ActorSystem.Create("AccountOtherCardCampaignProcessorActor");
                                        var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<AccountOtherCardCampaignProcessorActor>("AccountOtherCardCampaignProcessorActor");
                                        _TransactionPostProcessActorNotify.Tell(_OCampaignProcessor);
                                        #endregion
                                        return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                        #endregion
                                    }
                                }
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG124");
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse NotifySoftComTransaction(OThankUGateway.SCNotifyProductPaymentRequest _Request)
        {
            try
            {
                if (_Request.eventType == "CREATE_PRODUCT")
                {
                    _FrameworkSoftcom = new FrameworkSoftcom();
                    return _FrameworkSoftcom.SoftCon_SaveProduct(_Request);
                }
                else if (_Request.eventType == "NEW_PAYMENT")
                {
                    #region Declare
                    _GatewayResponse = new OThankUGateway.Response();
                    #endregion
                    #region Manage Exception
                    try
                    {
                        if (_Request.till.terminalID != null && string.IsNullOrEmpty(_Request.till.terminalID))
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                            #endregion
                        }
                        else if (string.IsNullOrEmpty(_Request.payment.billID))
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                            #endregion
                        }
                        else if (_Request.transaction.amount < 1)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
                            #endregion
                        }
                        else
                        {
                            string TerminalId = _Request.till.terminalID;
                            _GatewayRequest = new OThankUGateway.Request();
                            _GatewayRequest.TerminalId = TerminalId;
                            _GatewayRequest.MobileNumber = _Request.customer.phone;
                            _GatewayRequest.EmailAddress = _Request.customer.email;
                            _GatewayRequest.Name = _Request.customer.name;
                            _GatewayRequest.ReferenceNumber = _Request.payment.billID;
                            _GatewayRequest.TransactionDate = Convert.ToDateTime(_Request.transaction.createdAt);
                            _GatewayRequest.InvoiceAmount = (long)_Request.transaction.amount * 100;
                            if (_Request.payment != null && _Request.payment.channels != null && _Request.payment.channels.Count() > 0)
                            {
                                string PaymentMode = _Request.payment.channels[0].ToLower();
                                if (PaymentMode == "eyowo_pos")
                                {
                                    _GatewayRequest.TransactionMode = "card";
                                }
                                else
                                {
                                    _GatewayRequest.TransactionMode = "cash";
                                }
                            }
                            _GatewayRequest.UserReference = _Request.UserReference;
                            if (_Request.payment.status.ToLower() == "approved")
                            {
                                return SettleTransaction(_GatewayRequest, true, null);
                            }
                            else
                            {
                                return NotifyFailedTransaction(_GatewayRequest);

                            }
                            //_GatewayRequest.SixDigitPan = _Request.paymentInstrument.masked;
                        }
                    }
                    catch (Exception _Exception)
                    {
                        #region  Log Exception
                        HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                        #endregion
                    }
                    #endregion
                }
                else
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
        }
        internal OResponse NotifyTransaction(OThankUGateway.NotifyPayment.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (_Request.paymentInstrument != null && string.IsNullOrEmpty(_Request.paymentInstrument.userId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.userId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.referenceId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                    #endregion
                }
                else if (_Request.amount < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
                    #endregion
                }
                else
                {
                    string TerminalId = _Request.paymentInstrument.userId;
                    _GatewayRequest = new OThankUGateway.Request();
                    _GatewayRequest.TerminalId = _Request.paymentInstrument.userId;
                    _GatewayRequest.MobileNumber = _Request.userId;
                    _GatewayRequest.SixDigitPan = _Request.paymentInstrument.masked;
                    _GatewayRequest.ReferenceNumber = _Request.referenceId;
                    _GatewayRequest.TransactionDate = Convert.ToDateTime(_Request.completedAt);
                    _GatewayRequest.InvoiceAmount = (long)_Request.amount * 100;
                    if (!string.IsNullOrEmpty(_Request.paymentInstrument.instrumentType))
                    {
                        _GatewayRequest.TransactionMode = _Request.paymentInstrument.instrumentType;
                    }
                    _GatewayRequest.UserReference = _Request.UserReference;
                    if (!string.IsNullOrEmpty(_GatewayRequest.TransactionMode))
                    {
                        _GatewayRequest.TransactionMode = _GatewayRequest.TransactionMode.ToLower();
                    }
                    if (_Request.status.ToLower() == "success")
                    {
                        return SettleTransaction(_GatewayRequest, false, null);
                    }
                    else
                    {
                        return NotifyFailedTransaction(_GatewayRequest);

                    }

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse CancelTransaction(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.TransactionReference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG122");
                    #endregion
                }
                else
                {
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    #region Perform Operations

                    if (_OGatewayInfo != null)
                    {

                        using (_HCoreContext = new HCoreContext())
                        {
                            var TransactionDetails = _HCoreContext.HCUAccountTransaction.Where(x => (x.Guid == _Request.TransactionReference || x.GroupKey == _Request.TransactionReference) && x.ReferenceNumber == _Request.ReferenceNumber).ToList();
                            if (TransactionDetails.Count > 0)
                            {
                                foreach (var Transaction in TransactionDetails)
                                {
                                    Transaction.StatusId = HelperStatus.Transaction.Cancelled;
                                    Transaction.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Transaction.Comment = _Request.Comment;
                                    if (_OGatewayInfo.TerminalId != 0)
                                    {
                                        Transaction.ModifyById = _OGatewayInfo.TerminalId;
                                    }
                                    else
                                    {
                                        Transaction.ModifyById = _Request.UserReference.AccountId;
                                    }
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG157", CoreResources.HCG157);
                        #endregion

                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "CancelTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse NotifyFailedTransaction(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TerminalId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG551", CoreResources.HCG551);
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109", CoreResources.HCG109);
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115", CoreResources.HCG115);
                }
                else if (!string.IsNullOrEmpty(_Request.TransactionMode) && (_Request.TransactionMode != "card" && _Request.TransactionMode != "cash"))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG148", CoreResources.HCG148);
                }
                //else if (_Request.TransactionMode == "card" && string.IsNullOrEmpty(_Request.TransactionReference))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG152", CoreResources.HCG152);
                //}
                //else if (_Request.TransactionMode == "card" && string.IsNullOrEmpty(_Request.bin) && string.IsNullOrEmpty(_Request.SixDigitPan))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG154", CoreResources.HCG154);
                //}
                else if (_Request.InvoiceAmount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116", CoreResources.HCG116);
                }
                else
                {
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    #region Perform Operations
                    if (_OGatewayInfo != null)
                    {
                        OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo, true);
                        if (_UserInfo != null)
                        {
                            OAmountDistribution _AmountDistributionRequest = GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                            double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _OGatewayInfo.MerchantId)), 2);
                            if (RewardPercentage > 0)
                            {
                                #region Calculate Amounts
                                int TransactionTypeId = TransactionType.CardReward;
                                if (_Request.TransactionMode == "cash")
                                {
                                    TransactionTypeId = TransactionType.CashReward;
                                }
                                #endregion
                                #region PROCESS TRANSACTION
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Failed;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                _CoreTransactionRequest.InvoiceAmount = _AmountDistributionRequest.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistributionRequest.ReferenceInvoiceAmount;
                                _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                if (_OGatewayInfo.AcquirerId != null)
                                {
                                    _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                }
                                _CoreTransactionRequest.ReferenceAmount = 0;
                                _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                // Debit From Merchant
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _OGatewayInfo.MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Merchant,

                                    Amount = _AmountDistributionRequest.User,
                                    Comission = _AmountDistributionRequest.RewardCommission,
                                    TotalAmount = _AmountDistributionRequest.RewardAmount,
                                    Comment = _Request.ErrorMessage,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _UserInfo.UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.TUC,

                                    Amount = _AmountDistributionRequest.User,
                                    TotalAmount = _AmountDistributionRequest.User,
                                    Comment = _Request.ErrorMessage,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG156", CoreResources.HCG156);
                                #endregion
                                #endregion 
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG156", CoreResources.HCG156);
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG156", CoreResources.HCG156);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG156", CoreResources.HCG156);
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion

        }
        internal OResponse NotifySettlements(OThankUGateway.NotifySettlement _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCG11689");
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "InitializeTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }




        internal OResponse MerchantReward(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (_Request.UserReference.AccountTypeId != UserAccountType.Merchant)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                    #endregion
                }
                else if (_Request.InvoiceAmount < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
                    #endregion
                }
                else
                {
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    #region Perform Operations
                    if (_OGatewayInfo != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ValidateTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.ReferenceNumber == _Request.ReferenceNumber && x.CreatedBy.OwnerId == _Request.UserReference.AccountId).Select(x => x.Id).FirstOrDefault();
                            if (ValidateTransaction == 0)
                            {
                                _HCoreContext.Dispose();

                                OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo, true);
                                if (_UserInfo != null)
                                {
                                    OAmountDistribution _AmountDistrubutionRequest = GetAmountDistribution(_Request, _OGatewayInfo, _UserInfo);
                                    if (_AmountDistrubutionRequest.RewardPercentage > 0)
                                    {
                                        if (_AmountDistrubutionRequest.RewardAmount > 0)
                                        {
                                            int TransactionTypeId = TransactionType.CardReward;
                                            if (_Request.TransactionMode == "cash")
                                            {
                                                TransactionTypeId = TransactionType.CashReward;
                                            }
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                            _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                            _CoreTransactionRequest.InvoiceAmount = _AmountDistrubutionRequest.InvoiceAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistrubutionRequest.ReferenceInvoiceAmount;
                                            _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                            //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                            if (_OGatewayInfo.AcquirerId != null)
                                            {
                                                _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                            }
                                            _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.MerchantId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Merchant,

                                                Amount = _AmountDistrubutionRequest.User,
                                                Comission = _AmountDistrubutionRequest.RewardCommission,
                                                TotalAmount = _AmountDistrubutionRequest.RewardAmount,
                                            });
                                            if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.ThankUCashPlus,
                                                    Amount = _AmountDistrubutionRequest.User,
                                                    TotalAmount = _AmountDistrubutionRequest.User,
                                                });
                                            }
                                            else
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.TUC,

                                                    Amount = _AmountDistrubutionRequest.User,
                                                    TotalAmount = _AmountDistrubutionRequest.User,
                                                });
                                            }

                                            if (_AmountDistrubutionRequest.Ptsp > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _Request.UserReference.AccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.Ptsp,
                                                    TotalAmount = _AmountDistrubutionRequest.Ptsp,
                                                });
                                            }
                                            if (_AmountDistrubutionRequest.Acquirer > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.Acquirer,
                                                    TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                                });

                                                if (_AmountDistrubutionRequest.AllowAcquirerSettlement == "1")
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = (long)_OGatewayInfo.AcquirerId,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionTypeId,
                                                        SourceId = TransactionSource.Settlement,

                                                        Amount = _AmountDistrubutionRequest.Acquirer,
                                                        TotalAmount = _AmountDistrubutionRequest.Acquirer,
                                                    });
                                                }
                                            }
                                            if (_AmountDistrubutionRequest.Issuer > 0)
                                            {
                                                if (_UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.IssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.ReferralBonus,
                                                        SourceId = TransactionSource.TUC,
                                                        Amount = _AmountDistrubutionRequest.Issuer,
                                                        TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                    });
                                                }
                                                else
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _UserInfo.IssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.ReferralBonus,
                                                        SourceId = TransactionSource.Settlement,

                                                        Amount = _AmountDistrubutionRequest.Issuer,
                                                        TotalAmount = _AmountDistrubutionRequest.Issuer,
                                                    });
                                                }
                                            }
                                            if (_AmountDistrubutionRequest.TransactionIssuerTotalAmount > 0)
                                            {
                                                if (_OGatewayInfo.TransactionIssuerAccountTypeId == UserAccountType.Appuser)
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.TransactionBonus,
                                                        SourceId = TransactionSource.TUC,


                                                        Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                        Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                        TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                    });
                                                }
                                                else
                                                {
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = _OGatewayInfo.TransactionIssuerId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.TransactionBonus,
                                                        SourceId = TransactionSource.Settlement,


                                                        Amount = _AmountDistrubutionRequest.TransactionIssuerAmount,
                                                        Charge = _AmountDistrubutionRequest.TransactionIssuerCharge,
                                                        TotalAmount = _AmountDistrubutionRequest.TransactionIssuerTotalAmount,
                                                    });
                                                }
                                            }
                                            if (_AmountDistrubutionRequest.Ptsa > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = SystemAccounts.SmashLabId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.Ptsa,
                                                    TotalAmount = _AmountDistrubutionRequest.Ptsa,
                                                });
                                            }
                                            if (_AmountDistrubutionRequest.ThankUCash > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = SystemAccounts.ThankUCashMerchant,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = _AmountDistrubutionRequest.ThankUCash,
                                                    TotalAmount = _AmountDistrubutionRequest.ThankUCash,
                                                });
                                            }
                                            if (_AmountDistrubutionRequest.MerchantReverseAmount > 0)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _OGatewayInfo.MerchantId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.MerchantCredit,
                                                    SourceId = TransactionSource.Merchant,

                                                    Amount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                                    Comission = 0,
                                                    TotalAmount = _AmountDistrubutionRequest.MerchantReverseAmount,
                                                });
                                            }
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }
                                        }
                                        else
                                        {

                                            string Comment = null;
                                            if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                                            {
                                                Comment = "Reward credited";
                                            }
                                            else if (_UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
                                            {
                                                Comment = "User account suspended";
                                            }
                                            else
                                            {
                                                Comment = "User account blocked";
                                            }
                                            int TransactionTypeId = TransactionType.CardReward;
                                            if (_Request.TransactionMode == "cash")
                                            {
                                                TransactionTypeId = TransactionType.CashReward;
                                            }
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                            _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                            _CoreTransactionRequest.InvoiceAmount = _AmountDistrubutionRequest.InvoiceAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _AmountDistrubutionRequest.ReferenceInvoiceAmount;
                                            _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _CoreTransactionRequest.SubParentId = _OGatewayInfo.StoreId;
                                            //_CoreTransactionRequest.CardId = _UserInfo.CardId;
                                            if (_OGatewayInfo.AcquirerId != null)
                                            {
                                                _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                            }
                                            _CoreTransactionRequest.ReferenceAmount = _AmountDistrubutionRequest.RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _OGatewayInfo.MerchantId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionTypeId,
                                                SourceId = TransactionSource.Merchant,

                                                Amount = 0,
                                                Comission = 0,
                                                TotalAmount = 0,
                                            });
                                            if (_AmountDistrubutionRequest.IsThankUCashEnabled == true)
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.ThankUCashPlus,
                                                    Amount = 0,
                                                    TotalAmount = 0,
                                                    Comment = Comment,
                                                });
                                            }
                                            else
                                            {
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionTypeId,
                                                    SourceId = TransactionSource.TUC,

                                                    Amount = 0,
                                                    TotalAmount = 0,
                                                    Comment = Comment,

                                                });
                                            }
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                return ProcessTransactionResponse(_Request, _OGatewayInfo, _UserInfo, _AmountDistrubutionRequest, TransactionResponse);
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                #endregion
                                            }
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG123");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                                    #endregion
                                }

                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG124");
                                #endregion
                            }
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                        #endregion
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Reward", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse MerchantRedeem(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (_Request.UserReference.AccountTypeId != UserAccountType.Merchant)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber) && string.IsNullOrEmpty(_Request.TagNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
                    #endregion
                }
                else if ((_Request.RedeemAmount / 100) < 1)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG128");
                    #endregion
                }
                else if (_Request.RedeemAmount > _Request.InvoiceAmount)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG128");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Pin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG110");
                    #endregion
                }
                else
                {
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    #region Perform Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_OGatewayInfo != null)
                        {
                            OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo, false);
                            if (_UserInfo != null)
                            {
                                if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                                {
                                    if (string.IsNullOrEmpty(_UserInfo.Pin))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG112");
                                        #endregion
                                    }
                                    else
                                    {
                                        string DPin = HCoreEncrypt.DecryptHash(_UserInfo.Pin);
                                        if (DPin == _Request.Pin)
                                        {
                                            double RedeemAmount = (double)_Request.RedeemAmount / 100;
                                            double InvoiceAmount = (double)_Request.InvoiceAmount / 100;
                                            #region Get Balance 
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            double UserBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                                            if (RedeemAmount < (UserBalance + 1))
                                            {
                                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                                _CoreTransactionRequest.CashierId = _OGatewayInfo.CashierId;
                                                _CoreTransactionRequest.CustomerId = _UserInfo.UserAccountId;
                                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                                _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
                                                _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                                _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                                _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
                                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                                if (_OGatewayInfo.AcquirerId != null)
                                                {
                                                    _CoreTransactionRequest.BankId = (long)_OGatewayInfo.AcquirerId;
                                                }
                                                _CoreTransactionRequest.ReferenceAmount = 0;
                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                // Debit From User
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _UserInfo.UserAccountId,
                                                    ModeId = TransactionMode.Debit,
                                                    TypeId = TransactionType.Loyalty.TUCRedeem.PosRedeem,
                                                    SourceId = TransactionSource.TUC,

                                                    Amount = RedeemAmount,
                                                    TotalAmount = RedeemAmount,
                                                });
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = _OGatewayInfo.MerchantId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = TransactionType.Loyalty.TUCRedeem.PosRedeem,
                                                    SourceId = TransactionSource.Settlement,

                                                    Amount = RedeemAmount,
                                                    TotalAmount = RedeemAmount,
                                                });
                                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                                {

                                                    _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
                                                    _GatewayResponse.TransactionDate = TransactionResponse.TransactionDate;
                                                    _GatewayResponse.TransactionReference = TransactionResponse.ReferenceKey;
                                                    _GatewayResponse.RedeemAmount = _Request.RedeemAmount;
                                                    double Balance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
                                                    //if (HostEnvironment == HostEnvironmentType.Live)
                                                    //{
                                                    //    #region Send SMS
                                                    //    string Message = "Redeem Alert: You got " + RedeemAmount.ToString() + " points redeemed at " + _OGatewayInfo.MerchantDisplayName + " Bal: N" + Balance + ". Download App https://bit.ly/tuc-app . ThankUCash";
                                                    //    HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                                    //    #endregion
                                                    //}
                                                    if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test)
                                                    {
                                                        string RewardSms = HCoreHelper.GetConfiguration("redeemsms", _OGatewayInfo.MerchantId);
                                                        if (!string.IsNullOrEmpty(RewardSms))
                                                        {
                                                            #region Send SMS
                                                            string Message = RewardSms
                                                            .Replace("[AMOUNT]", HCoreHelper.RoundNumber(RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString())
                                                            .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                                                            .Replace("[MERCHANT]", _OGatewayInfo.MerchantDisplayName);
                                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                            #endregion
                                                        }
                                                        else
                                                        {
                                                            #region Send SMS
                                                            string Message = "Redeem Alert: You just redeemed N" + HCoreHelper.RoundNumber(RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " Cash at " + _OGatewayInfo.MerchantDisplayName + ". Your TUC Bal:N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ".  Download TUC App: https://bit.ly/tuc-app. Thank U Very Much";
                                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message, _UserInfo.UserAccountId, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                            #endregion
                                                        }
                                                    }
                                                    #region Send Email 
                                                    if (!string.IsNullOrEmpty(_UserInfo.EmailAddress))
                                                    {
                                                        var _EmailParameters = new
                                                        {
                                                            UserDisplayName = _UserInfo.DisplayName,
                                                            MerchantName = _OGatewayInfo.MerchantDisplayName,
                                                            InvoiceAmount = InvoiceAmount.ToString(),
                                                            Amount = RedeemAmount.ToString(),
                                                            Balance = Balance.ToString(),
                                                        };
                                                        HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RedeemEmail, _UserInfo.DisplayName, _UserInfo.EmailAddress, _EmailParameters, _Request.UserReference);
                                                    }
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                #region Send Response
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG120");
                                                #endregion
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG114");
                                            #endregion
                                        }
                                    }
                                }
                                else if (_UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "TUC202", CoreResources.TUC202);
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "TUC203", CoreResources.TUC203);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG111");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Redeem", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        internal OResponse ClaimReward(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.TCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG100", "Transaction code required");
                    #endregion
                }
                else if (_Request.TCode.Length != 4)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG101", "Invalid transaction code");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        //long UserId = _HCoreContext.HCUAccount
                        //.Where(x => x.Id == _Request.UserReference.AccountId)
                        //.Select(x => x.UserId).FirstOrDefault();
                        //if (UserId != 0)
                        //{
                        var UserAccountDetails = _HCoreContext.HCUAccount.Where(x =>
                          x.AccountTypeId == UserAccountType.Appuser
                          && x.Id == _Request.UserReference.AccountId
                          && x.StatusId == HelperStatus.Default.Active)
                          .Select(x => new
                          {
                              MobileNumber = x.MobileNumber,
                              UserAccountId = x.Id,
                          }).FirstOrDefault();
                        if (UserAccountDetails != null)
                        {
                            DateTime TodaysDate = HCoreHelper.GetGMTDate();
                            var TransactionDetails = _HCoreContext.HCUAccountTransaction
                            .Where(x => x.TCode == _Request.TCode
                                && x.TypeId == TransactionType.TransactionBonus
                                && x.SourceId == TransactionSource.Settlement
                                && x.AccountId == SystemAccounts.ThankUCashMerchant
                                && x.StatusId == HelperStatus.Transaction.Success
                                )
                                .Select(x => new
                                {
                                    TransactionId = x.Id,
                                    TotalAmount = x.TotalAmount,
                                    TransactionDate = x.TransactionDate,
                                    GroupKey = x.GroupKey,
                                    ParentTransactionKey = x.ParentTransaction.Guid,
                                    ParentId = x.ParentId,
                                    InvoiceAmount = x.PurchaseAmount,
                                    AccountNumber = x.AccountNumber,
                                    ReferenceNumber = x.ReferenceNumber,
                                    CreatedById = x.CreatedById,
                                    SubParentId = x.SubParentId,
                                    //CardId = x.CardId,
                                })
                                .FirstOrDefault();
                            if (TransactionDetails != null)
                            {
                                var ValidateTransactionBonus = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.GroupKey == TransactionDetails.GroupKey
                                     && x.TypeId == TransactionType.TransactionBonus
                                     && x.SourceId == TransactionSource.TUC
                                     && x.ModeId == TransactionMode.Credit
                                    && x.StatusId == HelperStatus.Transaction.Success)
                                    .Select(x => new
                                    {
                                        Id = x.Id,
                                    }).FirstOrDefault();
                                if (ValidateTransactionBonus == null)
                                {
                                    if (TransactionDetails.TransactionDate.Date == TodaysDate)
                                    {
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = UserAccountDetails.UserAccountId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = TransactionDetails.GroupKey;
                                        _CoreTransactionRequest.ParentTransactionKey = TransactionDetails.ParentTransactionKey;
                                        _CoreTransactionRequest.ParentId = (long)TransactionDetails.ParentId;
                                        _CoreTransactionRequest.InvoiceAmount = TransactionDetails.InvoiceAmount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = TransactionDetails.InvoiceAmount;
                                        _CoreTransactionRequest.AccountNumber = TransactionDetails.AccountNumber;
                                        _CoreTransactionRequest.ReferenceNumber = TransactionDetails.ReferenceNumber;
                                        _CoreTransactionRequest.CreatedById = (long)TransactionDetails.CreatedById;
                                        _CoreTransactionRequest.ReferenceAmount = TransactionDetails.TotalAmount;
                                        if (TransactionDetails.SubParentId != null)
                                        {
                                            _CoreTransactionRequest.SubParentId = (long)TransactionDetails.SubParentId;
                                        }
                                        //if (TransactionDetails.CardId != null)
                                        //{
                                        //    _CoreTransactionRequest.CardId = (long)TransactionDetails.CardId;
                                        //}
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>
                                {
                                    new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = SystemAccounts.ThankUCashMerchant,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.TransactionBonus,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = TransactionDetails.TotalAmount,
                                        Comission = 0,
                                        TotalAmount = TransactionDetails.TotalAmount,
                                    },
                                    new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _Request.UserReference.AccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.TransactionBonus,
                                        SourceId = TransactionSource.TUC,
                                        Amount = TransactionDetails.TotalAmount,
                                        Comission = 0,
                                        TotalAmount = TransactionDetails.TotalAmount,
                                    }
                                };
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                        {
                                            //_GatewayResponse.RewardAmount = (long)HCoreHelper.RoundNumber(TransactionDetails.TotalAmount, _AppConfig.SystemExitRoundDouble) * 100;
                                            //double Balance = HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_Request.UserReference.AccountId), _AppConfig.SystemExitRoundDouble);
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Math.Round(TransactionDetails.TotalAmount, 2), "TUCG106", "Bonus claim successfull. Amount credited to your account.");
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG105", "Transaction failed. Please try after some time.");
                                            #endregion
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG104", "Claim period expired. You must claim on the same day of transaction.");
                                        #endregion
                                    }
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG107", "Bonus already claimed.");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG103", "Invalid transaction code");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG102", "Account is disabled or not present. Contact ThankUCash support.");
                            #endregion
                        }
                        //}
                        //else
                        //{
                        //    #region Send Response
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "TUCG108", "Account is disabled or not present. Contact ThankUCash support.");
                        //    #endregion
                        //}
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "ClaimReward", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
                #endregion
            }
            #endregion
        }
        //internal OResponse SettleTransaction(OThankUGateway.Request _Request, bool AllowCash)
        //{
        //    #region Declare
        //    _GatewayResponse = new OThankUGateway.Response();
        //    #endregion
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.MerchantId) && string.IsNullOrEmpty(_Request.TerminalId))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG100");
        //            #endregion
        //        }
        //        else if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
        //            #endregion
        //        }
        //        else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
        //            #endregion
        //        }
        //        else if (AllowCash == false && _Request.TransactionMode == "cash")
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG115");
        //            #endregion
        //        }
        //        else if (_Request.InvoiceAmount < 1)
        //        {
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG116");
        //            #endregion
        //        }
        //        else
        //        {
        //            OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
        //            #region Perform Operations
        //            //using (_HCoreContext = new HCoreContext())
        //            //{
        //            if (_OGatewayInfo != null)
        //            {
        //                using (_HCoreContext = new HCoreContext())
        //                {
        //                    var ValidateTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.ReferenceNumber == _Request.ReferenceNumber && x.CreatedBy.OwnerId == _Request.UserReference.AccountId).Select(x => x.Id).FirstOrDefault();
        //                    if (ValidateTransaction == 0)
        //                    {
        //                        _HCoreContext.Dispose();
        //                        OUserInfo _UserInfo = GetUserInfo(_Request, _OGatewayInfo);
        //                        if (_UserInfo != null)
        //                        {
        //                            double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", _OGatewayInfo.MerchantId)), 2);
        //                            if (RewardPercentage > 0)
        //                            {
        //                                #region Calculate Amounts
        //                                double UserRewardPercentage = UserRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", _OGatewayInfo.MerchantId)), 2);
        //                                double PTSAPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("ptsapercentage", SmashLabId)), 2);
        //                                double PGPercentage = 0;
        //                                double PosPercentage = 0;
        //                                double IssuerPercentage = 0;
        //                                double AcquirerPercentage = 0;
        //                                string AllowAcquirerSettlement = "0";
        //                                if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
        //                                {
        //                                    PGPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("pgpercentage", _Request.UserReference.AccountId)), 2);
        //                                }
        //                                if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
        //                                {
        //                                    PosPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("pospercentage", _Request.UserReference.AccountId)), 2);
        //                                    AllowAcquirerSettlement = HCoreHelper.GetConfiguration("banksettlementbyptsp", _Request.UserReference.AccountId);
        //                                }
        //                                if (_UserInfo.IssuerId != 0)
        //                                {
        //                                    IssuerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("issuerpercentage", _UserInfo.IssuerId)), 2);
        //                                }
        //                                if (_OGatewayInfo.AcquirerId != null)
        //                                {
        //                                    if (_OGatewayInfo.AcquirerId != 0)
        //                                    {
        //                                        AcquirerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("acquirerpercentage", (long)_OGatewayInfo.AcquirerId)), 2);
        //                                    }
        //                                }
        //                                double InvoiceAmount = (double)_Request.InvoiceAmount / 100;
        //                                double RewardAmount = HCoreHelper.GetPercentage(InvoiceAmount, RewardPercentage, _AppConfig.SystemEntryRoundDouble);
        //                                long TransactionTypeId = TransactionType.CardReward;
        //                                if (_Request.TransactionMode == "cash")
        //                                {
        //                                    TransactionTypeId = TransactionType.CashReward;
        //                                    //_ManageCoreTransaction = new ManageCoreTransaction();
        //                                    //double MerchantBalance = _ManageCoreTransaction.GetUserAccountBalance(_OGatewayInfo.MerchantId, TransactionSource.Merchant, _Request.UserReference);
        //                                    //if (RewardAmount < (MerchantBalance + 1))
        //                                    //{
        //                                    //}
        //                                    //else
        //                                    //{
        //                                    //    #region Send Response
        //                                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
        //                                    //    #endregion
        //                                    //}
        //                                }
        //                                double CriteriaValue = 0;
        //                                long ThankUCashPlusIsEnable = Convert.ToInt64(HCoreHelper.GetConfiguration("thankucashplus", _OGatewayInfo.MerchantId));
        //                                if (ThankUCashPlusIsEnable != 0)
        //                                {
        //                                    OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", _OGatewayInfo.MerchantId);
        //                                    if (RewardCriteriaDetails != null)
        //                                    {
        //                                        if (!string.IsNullOrEmpty(RewardCriteriaDetails.Value))
        //                                        {
        //                                            CriteriaValue = Convert.ToDouble(RewardCriteriaDetails.Value);
        //                                        }

        //                                        if (RewardCriteriaDetails.TypeCode == "rewardcriteriatype.mininvoice")
        //                                        {
        //                                            if (InvoiceAmount < CriteriaValue)
        //                                            {
        //                                                _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
        //                                                _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
        //                                                _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
        //                                                _GatewayResponse.RewardAmount = 0;
        //                                                _GatewayResponse.UserRewardAmount = 0;
        //                                                _GatewayResponse.Comission = 0;
        //                                                #region Send Response
        //                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
        //                                                #endregion
        //                                            }
        //                                        }
        //                                        else if (RewardCriteriaDetails.TypeCode == "rewardcriteriatype.multipleofamount")
        //                                        {
        //                                            if (InvoiceAmount < CriteriaValue)
        //                                            {
        //                                                _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
        //                                                _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
        //                                                _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
        //                                                _GatewayResponse.RewardAmount = 0;
        //                                                _GatewayResponse.UserRewardAmount = 0;
        //                                                _GatewayResponse.Comission = 0;
        //                                                #region Send Response
        //                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
        //                                                #endregion
        //                                            }
        //                                            long RewardSlot = (long)Math.Round((InvoiceAmount / CriteriaValue), 0);
        //                                            double CriteriaRewardAmount = HCoreHelper.GetPercentage(CriteriaValue, RewardPercentage, 2);
        //                                            RewardAmount = CriteriaRewardAmount * RewardSlot;
        //                                        }
        //                                    }
        //                                }
        //                                double UserAmount = 0;
        //                                double PgAmount = 0;
        //                                double PosAmount = 0;
        //                                double IssuerAmount = 0;
        //                                double AcquirerAmount = 0;
        //                                double SmashLabAmount = 0;
        //                                double ThankUAmount = RewardAmount;
        //                                if (UserRewardPercentage > 0)
        //                                {
        //                                    UserAmount = HCoreHelper.GetPercentage(RewardAmount, UserRewardPercentage, _AppConfig.SystemEntryRoundDouble);
        //                                    ThankUAmount = ThankUAmount - UserAmount;
        //                                }
        //                                if (PGPercentage > 0)
        //                                {
        //                                    PgAmount = HCoreHelper.GetPercentage(RewardAmount, PGPercentage, _AppConfig.SystemEntryRoundDouble);
        //                                    ThankUAmount = ThankUAmount - PgAmount;
        //                                }
        //                                if (PosPercentage > 0)
        //                                {
        //                                    PosAmount = HCoreHelper.GetPercentage(RewardAmount, PosPercentage, _AppConfig.SystemEntryRoundDouble);
        //                                    ThankUAmount = ThankUAmount - PosAmount;
        //                                }
        //                                if (AcquirerPercentage > 0)
        //                                {
        //                                    AcquirerAmount = HCoreHelper.GetPercentage(RewardAmount, AcquirerPercentage, _AppConfig.SystemEntryRoundDouble);
        //                                    ThankUAmount = ThankUAmount - AcquirerAmount;
        //                                }
        //                                if (PTSAPercentage > 0)
        //                                {
        //                                    SmashLabAmount = HCoreHelper.GetPercentage(RewardAmount, PTSAPercentage, _AppConfig.SystemEntryRoundDouble);
        //                                    ThankUAmount = ThankUAmount - SmashLabAmount;
        //                                }
        //                                if (_UserInfo.IssuerId != 0)
        //                                {
        //                                    if (_UserInfo.IssuerId != _OGatewayInfo.MerchantId)
        //                                    {
        //                                        if (IssuerPercentage > 0)
        //                                        {
        //                                            IssuerAmount = HCoreHelper.GetPercentage(RewardAmount, IssuerPercentage, _AppConfig.SystemEntryRoundDouble);
        //                                            ThankUAmount = ThankUAmount - IssuerAmount;
        //                                        }
        //                                    }
        //                                }
        //                                ThankUAmount = HCoreHelper.RoundNumber(ThankUAmount, _AppConfig.SystemEntryRoundDouble);
        //                                double ComissionAmount = HCoreHelper.RoundNumber((RewardAmount - UserAmount), _AppConfig.SystemEntryRoundDouble);
        //                                #endregion
        //                                #region PROCESS TRANSACTION
        //                                _CoreTransactionRequest = new OCoreTransaction.Request();
        //                                _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                                _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
        //                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                                _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
        //                                _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
        //                                _CoreTransactionRequest.AccountNumber = _Request.SixDigitPan;
        //                                _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
        //                                _CoreTransactionRequest.CreatedById = _OGatewayInfo.TerminalId;
        //                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                                // Debit From Merchant
        //                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                {
        //                                    UserAccountId = _OGatewayInfo.MerchantId,
        //                                    ModeId = TransactionMode.Debit,
        //                                    TypeId = TransactionTypeId,
        //                                    SourceId = TransactionSource.Merchant,

        //                                    Amount = UserAmount,
        //                                    Comission = ComissionAmount,
        //                                    TotalAmount = RewardAmount,
        //                                    ReferenceAmount = InvoiceAmount,
        //                                });
        //                                if (ThankUCashPlusIsEnable == 1)
        //                                {

        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _UserInfo.UserAccountId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.ThankUCashPlus,
        //                                        Amount = UserAmount,
        //                                        TotalAmount = UserAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                }
        //                                else
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _UserInfo.UserAccountId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.TUC,

        //                                        Amount = UserAmount,
        //                                        TotalAmount = UserAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                }

        //                                if (PgAmount > 0)
        //                                {
        //                                    // Credit
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _Request.UserReference.AccountId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = PgAmount,
        //                                        TotalAmount = PgAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                    //Debit
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _Request.UserReference.AccountId,
        //                                        ModeId = TransactionMode.Debit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = PgAmount,
        //                                        TotalAmount = PgAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                }
        //                                if (PosAmount > 0)
        //                                {
        //                                    // Credit
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = _Request.UserReference.AccountId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = PosAmount,
        //                                        TotalAmount = PosAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                    //Debit
        //                                    //_TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    //{
        //                                    //    UserAccountId = _Request.UserReference.AccountId,
        //                                    //    ModeId = TransactionMode.Debit,
        //                                    //    TypeId = TransactionTypeId,
        //                                    //    SourceId = TransactionSource.Settlement,

        //                                    //    Amount = PosAmount,
        //                                    //    TotalAmount = PosAmount,
        //                                    //    ReferenceAmount = RewardAmount,
        //                                    //});
        //                                }
        //                                if (AcquirerAmount > 0)
        //                                {
        //                                    // Credit
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = (long)_OGatewayInfo.AcquirerId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = AcquirerAmount,
        //                                        TotalAmount = AcquirerAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                    //Debit
        //                                    if (AllowAcquirerSettlement == "1")
        //                                    {
        //                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                        {
        //                                            UserAccountId = (long)_OGatewayInfo.AcquirerId,
        //                                            ModeId = TransactionMode.Debit,
        //                                            TypeId = TransactionTypeId,
        //                                            SourceId = TransactionSource.Settlement,

        //                                            Amount = AcquirerAmount,
        //                                            TotalAmount = AcquirerAmount,
        //                                            ReferenceAmount = RewardAmount,
        //                                        });
        //                                    }
        //                                }
        //                                if (IssuerAmount > 0)
        //                                {
        //                                    // Credit
        //                                    if (_UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
        //                                    {
        //                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                        {
        //                                            UserAccountId = _UserInfo.IssuerId,
        //                                            ModeId = TransactionMode.Credit,
        //                                            TypeId = TransactionType.ReferralBonus,
        //                                            SourceId = TransactionSource.TUC,
        //                                            Amount = IssuerAmount,
        //                                            TotalAmount = IssuerAmount,
        //                                            ReferenceAmount = RewardAmount,
        //                                        });
        //                                    }
        //                                    else
        //                                    {
        //                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                        {
        //                                            UserAccountId = _UserInfo.IssuerId,
        //                                            ModeId = TransactionMode.Credit,
        //                                            TypeId = TransactionType.ReferralBonus,
        //                                            SourceId = TransactionSource.Settlement,

        //                                            Amount = IssuerAmount,
        //                                            TotalAmount = IssuerAmount,
        //                                            ReferenceAmount = RewardAmount,
        //                                        });
        //                                    }
        //                                }
        //                                if (SmashLabAmount > 0)
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = SmashLabId,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = SmashLabAmount,
        //                                        TotalAmount = SmashLabAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                }
        //                                if (ThankUAmount > 0)
        //                                {
        //                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                    {
        //                                        UserAccountId = SystemAccounts.ThankUCashMerchant,
        //                                        ModeId = TransactionMode.Credit,
        //                                        TypeId = TransactionTypeId,
        //                                        SourceId = TransactionSource.Settlement,

        //                                        Amount = ThankUAmount,
        //                                        TotalAmount = ThankUAmount,
        //                                        ReferenceAmount = RewardAmount,
        //                                    });
        //                                }
        //                                _CoreTransactionRequest.Transactions = _TransactionItems;
        //                                _ManageCoreTransaction = new ManageCoreTransaction();
        //                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                                {
        //                                    if (ThankUCashPlusIsEnable == 1)
        //                                    {
        //                                        double TucPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", _OGatewayInfo.MerchantId));
        //                                        double ThankUCashPlusBalance = _ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_UserInfo.UserAccountId);
        //                                        if ((ThankUCashPlusBalance > (TucPlusMinTransferAmount - 1)))
        //                                        {
        //                                            double NewRewardAmount = ThankUCashPlusBalance + RewardAmount;
        //                                            _CoreTransactionRequest = new OCoreTransaction.Request();
        //                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                                            _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
        //                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                                            _CoreTransactionRequest.ParentId = _OGatewayInfo.MerchantId;
        //                                            _CoreTransactionRequest.InvoiceAmount = ThankUCashPlusBalance;
        //                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
        //                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                            {
        //                                                UserAccountId = _UserInfo.UserAccountId,
        //                                                ModeId = TransactionMode.Debit,
        //                                                TypeId = TransactionType.ThankUCashPlusCredit,
        //                                                SourceId = TransactionSource.ThankUCashPlus,
        //                                                Amount = ThankUCashPlusBalance,
        //                                                TotalAmount = ThankUCashPlusBalance,
        //                                                ReferenceAmount = ThankUCashPlusBalance,
        //                                            });
        //                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                            {
        //                                                UserAccountId = _UserInfo.UserAccountId,
        //                                                ModeId = TransactionMode.Credit,
        //                                                TypeId = TransactionType.ThankUCashPlusCredit,
        //                                                SourceId = TransactionSource.TUC,
        //                                                Amount = ThankUCashPlusBalance,
        //                                                TotalAmount = ThankUCashPlusBalance,
        //                                                ReferenceAmount = ThankUCashPlusBalance,
        //                                            });

        //                                            _CoreTransactionRequest.Transactions = _TransactionItems;
        //                                            _ManageCoreTransaction = new ManageCoreTransaction();
        //                                            OCoreTransaction.Response TUCPlusTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                                            double AccBalance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);

        //                                            if (HostEnvironment == HostEnvironmentType.Live)
        //                                            {
        //                                                #region Send SMS
        //                                                string Message = "Credit Alert: You got " + NewRewardAmount + " points from back from " + _OGatewayInfo.MerchantDisplayName + " Bal: N" + AccBalance + ". Download App https://bit.ly/tuc-app . ThankUCash";
        //                                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
        //                                                #endregion
        //                                            }

        //                                            if (!string.IsNullOrEmpty(_UserInfo.EmailAddress))
        //                                            {
        //                                                var _EmailParameters = new
        //                                                {
        //                                                    UserDisplayName = _OUserInfo.DisplayName,
        //                                                    MerchantName = _OGatewayInfo.MerchantDisplayName,
        //                                                    InvoiceAmount = InvoiceAmount.ToString(),
        //                                                    Amount = UserAmount.ToString(),
        //                                                    Balance = AccBalance.ToString(),
        //                                                };
        //                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _UserInfo.DisplayName, _UserInfo.EmailAddress, _EmailParameters, _Request.UserReference);
        //                                            }
        //                                            _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
        //                                            _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
        //                                            _GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
        //                                            _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
        //                                            _GatewayResponse.RewardAmount = (long)((HCoreHelper.RoundNumber(RewardAmount, 2)) * 100);
        //                                            _GatewayResponse.UserRewardAmount = (long)((HCoreHelper.RoundNumber(UserAmount, 2)) * 100);
        //                                            if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
        //                                            {
        //                                                _GatewayResponse.Comission = (long)((HCoreHelper.RoundNumber(PgAmount, 2)) * 100);
        //                                            }
        //                                            else
        //                                            {
        //                                                _GatewayResponse.Comission = (long)((HCoreHelper.RoundNumber(PosAmount, 2)) * 100);
        //                                            }
        //                                            #region Send Response
        //                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
        //                                            #endregion

        //                                        }
        //                                        else
        //                                        {
        //                                            double Balance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
        //                                            if (HostEnvironment == HostEnvironmentType.Live)
        //                                            {
        //                                                #region Send SMS
        //                                                string Message = "Credit Alert: You got " + UserAmount + " points Cash back from " + _OGatewayInfo.MerchantDisplayName + " Bal: N" + Balance + ". Download App https://bit.ly/tuc-app . ThankUCash";
        //                                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
        //                                                #endregion
        //                                            }
        //                                            #region Send Email 
        //                                            if (!string.IsNullOrEmpty(_UserInfo.EmailAddress))
        //                                            {
        //                                                var _EmailParameters = new
        //                                                {
        //                                                    UserDisplayName = _OUserInfo.DisplayName,
        //                                                    MerchantName = _OGatewayInfo.MerchantDisplayName,
        //                                                    InvoiceAmount = InvoiceAmount.ToString(),
        //                                                    Amount = UserAmount.ToString(),
        //                                                    Balance = Balance.ToString(),
        //                                                };
        //                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _UserInfo.DisplayName, _UserInfo.EmailAddress, _EmailParameters, _Request.UserReference);
        //                                            }
        //                                            #endregion
        //                                            _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
        //                                            _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
        //                                            _GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
        //                                            _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
        //                                            _GatewayResponse.RewardAmount = (long)((HCoreHelper.RoundNumber(RewardAmount, 2)) * 100);
        //                                            _GatewayResponse.UserRewardAmount = (long)((HCoreHelper.RoundNumber(UserAmount, 2)) * 100);
        //                                            if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
        //                                            {
        //                                                _GatewayResponse.Comission = (long)((HCoreHelper.RoundNumber(PgAmount, 2)) * 100);
        //                                            }
        //                                            else
        //                                            {
        //                                                _GatewayResponse.Comission = (long)((HCoreHelper.RoundNumber(PosAmount, 2)) * 100);
        //                                            }
        //                                            #region Send Response
        //                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
        //                                            #endregion
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        double Balance = _ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId);
        //                                        if (HostEnvironment == HostEnvironmentType.Live)
        //                                        {
        //                                            #region Send SMS
        //                                            string Message = "Credit Alert: You got " + UserAmount + " points Cash back from " + _OGatewayInfo.MerchantDisplayName + " Bal: N" + Balance + ". Download App https://bit.ly/tuc-app . ThankUCash";
        //                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
        //                                            #endregion
        //                                        }
        //                                        #region Send Email 
        //                                        if (!string.IsNullOrEmpty(_UserInfo.EmailAddress))
        //                                        {
        //                                            var _EmailParameters = new
        //                                            {
        //                                                UserDisplayName = _OUserInfo.DisplayName,
        //                                                MerchantName = _OGatewayInfo.MerchantDisplayName,
        //                                                InvoiceAmount = InvoiceAmount.ToString(),
        //                                                Amount = UserAmount.ToString(),
        //                                                Balance = Balance.ToString(),
        //                                            };
        //                                            HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _UserInfo.DisplayName, _UserInfo.EmailAddress, _EmailParameters, _Request.UserReference);
        //                                        }
        //                                        #endregion

        //                                        _GatewayResponse.InvoiceAmount = _Request.InvoiceAmount;
        //                                        _GatewayResponse.ReferenceNumber = _Request.ReferenceNumber;
        //                                        _GatewayResponse.TransactionReference = TransactionResponse.GroupKey;
        //                                        _GatewayResponse.TransactionDate = HCoreHelper.GetGMTDateTime();
        //                                        _GatewayResponse.RewardAmount = (long)((HCoreHelper.RoundNumber(RewardAmount, 2)) * 100);
        //                                        _GatewayResponse.UserRewardAmount = (long)((HCoreHelper.RoundNumber(UserAmount, 2)) * 100);
        //                                        if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount)
        //                                        {
        //                                            _GatewayResponse.Comission = (long)((HCoreHelper.RoundNumber(PgAmount, 2)) * 100);
        //                                        }
        //                                        else
        //                                        {
        //                                            _GatewayResponse.Comission = (long)((HCoreHelper.RoundNumber(PosAmount, 2)) * 100);
        //                                        }
        //                                        #region Send Response
        //                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG125");
        //                                        #endregion
        //                                    }

        //                                }
        //                                else
        //                                {
        //                                    #region Send Response
        //                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
        //                                    #endregion
        //                                }
        //                                #endregion
        //                            }
        //                            else
        //                            {
        //                                #region Send Response
        //                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG123");
        //                                #endregion
        //                            }
        //                        }
        //                        else
        //                        {
        //                            #region Send Response
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109");
        //                            #endregion
        //                        }

        //                    }
        //                    else
        //                    {
        //                        _HCoreContext.Dispose();
        //                        #region Send Response
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG124");
        //                        #endregion
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG103");
        //                #endregion
        //            }
        //            //}
        //            #endregion
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException(LogLevel.High, "SettleTransaction", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG105");
        //        #endregion
        //    }
        //    #endregion
        //}


    }
}
