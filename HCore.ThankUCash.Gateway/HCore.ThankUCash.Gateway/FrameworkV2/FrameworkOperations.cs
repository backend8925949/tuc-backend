//==================================================================================
// FileName: FrameworkOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Actor;
using HCore.ThankUCash.Gateway.Core;
using HCore.ThankUCash.Gateway.ObjectV2;
using Z.EntityFramework.Plus;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.ThankUCash.Gateway.FrameworkV2
{
    internal class FrameworkOperations
    {
        #region Declare
        HCoreContext _HCoreContext;
        //ManageCoreUserAccess _ManageCoreUserAccess;
        ManageCoreTransaction _ManageCoreTransaction;
        //OUserInfo _OUserInfo;
        OThankUGateway.Response _GatewayResponse;
        //OAppProfile.Request _AppProfileRequest;
        #endregion


        internal static OGatewayInfo GetGatewayInfo(OThankUGateway.Request _Request)
        {
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                OGatewayInfo _OGatewayInfo = null;
                switch (_Request.UserReference.AccountTypeId)
                {
                    case UserAccountType.PgAccount:
                    case UserAccountType.Acquirer:
                        _OGatewayInfo = Data.Store.HCoreDataStore.Accounts
                                        .Where(x => x.AccountTypeId == UserAccountType.Merchant &&
                                        x.StatusId == HelperStatus.Default.Active &&
                                        x.AccountCode == _Request.MerchantId)
                                        .Select(x => new OGatewayInfo
                                        {
                                            MerchantId = x.ReferenceId,
                                            MerchantDisplayName = x.DisplayName,
                                            AcquirerId = x.OwnerId,
                                            AcquirerAccountTypeId = x.OwnerAccountTypeId
                                        })
                                        .FirstOrDefault();
                        if (_OGatewayInfo == null)
                        {
                            _OGatewayInfo = _HCoreContext.HCUAccount
                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant &&
                                    x.StatusId == HelperStatus.Default.Active &&
                                    x.AccountCode == _Request.MerchantId)
                                    .Select(x => new OGatewayInfo
                                    {
                                        MerchantId = x.Id,
                                        MerchantDisplayName = x.DisplayName,
                                        AcquirerId = x.OwnerId,
                                        AcquirerAccountTypeId = x.Owner.AccountTypeId
                                    }).FirstOrDefault();
                        }
                        _OGatewayInfo.PsspId = _Request.UserReference.AccountId;
                        break;
                    case UserAccountType.PosAccount:
                        _OGatewayInfo = Data.Store.HCoreDataStore.Terminals
                                        .Where(x => x.TerminalId == _Request.TerminalId
                                       && x.MerchantId != null
                                       && x.StoreId != null
                                       && x.StatusId == HelperStatus.Default.Active
                                       && x.StoreStatusId == HelperStatus.Default.Active
                                       && x.MerchantStatusId == HelperStatus.Default.Active)
                                         .Select(x => new OGatewayInfo
                                         {
                                             TerminalId = x.ReferenceId,
                                             MerchantId = (long)x.MerchantId,
                                             MerchantDisplayName = x.MerchantDisplayName,
                                             StoreId = (long)x.StoreId,
                                             StoreDisplayName = x.StoreDisplayName,
                                             AcquirerId = x.AcquirerId,
                                             AcquirerAccountTypeId = UserAccountType.Acquirer,
                                         }).FirstOrDefault();
                        if (_OGatewayInfo == null)
                        {
                            _OGatewayInfo = _HCoreContext.TUCTerminal
                                                                      .Where(x => x.DisplayName == _Request.TerminalId
                                                                             && x.StoreId != null
                                                                             && x.StatusId == HelperStatus.Default.Active
                                                                             && x.Acquirer.StatusId == HelperStatus.Default.Active
                                                                             && x.Store.StatusId == HelperStatus.Default.Active
                                                                             && x.Merchant.StatusId == HelperStatus.Default.Active
                                                                            )
                                                                         .Select(x => new OGatewayInfo
                                                                         {
                                                                             TerminalId = x.Id,
                                                                             MerchantId = (long)x.MerchantId,
                                                                             MerchantDisplayName = x.DisplayName,
                                                                             StoreId = (long)x.StoreId,
                                                                             StoreDisplayName = x.Store.DisplayName,
                                                                             AcquirerId = x.AcquirerId,
                                                                             AcquirerAccountTypeId = UserAccountType.Acquirer,
                                                                         }).FirstOrDefault();
                            //_OGatewayInfo = _HCoreContext.HCUAccount
                            //                                          .Where(x => x.AccountTypeId == UserAccountType.TerminalAccount
                            //                                                 && x.User.Username == _Request.TerminalId
                            //                                                 && x.SubOwnerId != null
                            //                                                 && x.StatusId == HelperStatus.Default.Active
                            //                                                 && x.Owner.StatusId == HelperStatus.Default.Active
                            //                                                 && x.SubOwner.StatusId == HelperStatus.Default.Active
                            //                                                 && x.SubOwner.Owner.StatusId == HelperStatus.Default.Active
                            //                                                )
                            //                                             .Select(x => new OGatewayInfo
                            //                                             {
                            //                                                 TerminalId = x.Id,
                            //                                                 MerchantId = (long)x.SubOwner.OwnerId,
                            //                                                 MerchantDisplayName = x.SubOwner.Owner.DisplayName,
                            //                                                 StoreId = (long)x.SubOwnerId,
                            //                                                 StoreDisplayName = x.SubOwner.DisplayName,
                            //                                                 AcquirerId = x.BankId,
                            //                                                 AcquirerAccountTypeId = UserAccountType.Acquirer,
                            //                                             }).FirstOrDefault();
                        }
                        if (_OGatewayInfo != null)
                        {
                            _OGatewayInfo.PtspId = _Request.UserReference.AccountId;
                        }
                        break;
                    case UserAccountType.Merchant:
                        OGatewayInfo MerchantDetails = Data.Store.HCoreDataStore.Accounts
                        .Where(x => x.AccountTypeId == _Request.UserReference.AccountId
                        && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OGatewayInfo
                        {
                            MerchantId = x.ReferenceId,
                            MerchantDisplayName = x.DisplayName,
                            AcquirerId = x.OwnerId,
                            AcquirerAccountTypeId = x.OwnerAccountTypeId
                        })
                        .FirstOrDefault();
                        if (MerchantDetails == null)
                        {
                            _OGatewayInfo = _HCoreContext.HCUAccount
                                           .Where(x => x.StatusId == HelperStatus.Default.Active && x.Id == _Request.UserReference.AccountId)
                                           .Select(x => new OGatewayInfo
                                           {
                                               MerchantId = x.Id,
                                               MerchantDisplayName = x.DisplayName,
                                               AcquirerId = x.OwnerId,
                                               AcquirerAccountTypeId = x.Owner.AccountTypeId
                                           }).FirstOrDefault();
                        }
                        break;
                }
                if (_OGatewayInfo != null)
                {
                    if (!string.IsNullOrEmpty(_Request.CashierId))
                    {
                        var CCashierDetails = Data.Store.HCoreDataStore.Accounts.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                               && x.OwnerId == _OGatewayInfo.MerchantId
                                               && x.DisplayName == _Request.CashierId)
                                                    .FirstOrDefault();
                        if (CCashierDetails != null)
                        {
                            _OGatewayInfo.CashierId = CCashierDetails.ReferenceId;
                            _OGatewayInfo.CashierStatusId = (long)CCashierDetails.StatusId;
                        }
                        else
                        {
                            var CashierDetails = _HCoreContext.HCUAccount
                                 .Where(x => x.DisplayName == _Request.CashierId
                                 && x.OwnerId == _OGatewayInfo.MerchantId
                                 && x.AccountTypeId == UserAccountType.MerchantCashier
                                 ).Select(x => new
                                 {
                                     Id = x.Id,
                                     StatusId = x.StatusId,
                                 }).FirstOrDefault();
                            if (CashierDetails != null)
                            {
                                _OGatewayInfo.CashierId = CashierDetails.Id;
                                _OGatewayInfo.CashierStatusId = CashierDetails.StatusId;
                            }
                        }
                    }
                    else
                    {

                        var CCashierDetails = Data.Store.HCoreDataStore.Accounts.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                              && x.OwnerId == _OGatewayInfo.MerchantId
                                              && x.DisplayName == "0001")
                                                   .FirstOrDefault();
                        if (CCashierDetails != null)
                        {
                            _OGatewayInfo.CashierId = CCashierDetails.ReferenceId;
                            _OGatewayInfo.CashierStatusId = (long)CCashierDetails.StatusId;
                        }
                        else
                        {
                            var CashierDetails = _HCoreContext.HCUAccount
                                                 .Where(x => x.DisplayName == "0001"
                                                 && x.OwnerId == _OGatewayInfo.MerchantId
                                                 && x.AccountTypeId == UserAccountType.MerchantCashier
                                                 ).Select(x => new
                                                 {
                                                     Id = x.Id,
                                                     StatusId = x.StatusId,
                                                 }).FirstOrDefault();
                            if (CashierDetails != null)
                            {
                                _OGatewayInfo.CashierId = CashierDetails.Id;
                                _OGatewayInfo.CashierStatusId = CashierDetails.StatusId;
                            }
                        }
                    }
                    if (_OGatewayInfo.AcquirerAccountTypeId != UserAccountType.Merchant
                    && _OGatewayInfo.AcquirerAccountTypeId != UserAccountType.Acquirer)
                    {
                        _OGatewayInfo.AcquirerId = 0;
                    }
                    _HCoreContext.Dispose();
                    if (_OGatewayInfo.AcquirerId == 0)
                    {
                        _OGatewayInfo.AcquirerId = SystemAccounts.ThankUCashMerchant;
                        _OGatewayInfo.AcquirerAccountTypeId = UserAccountType.Merchant;
                    }
                    else if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                    {
                        if (_OGatewayInfo.StoreId != 0)
                        {
                            _OGatewayInfo.TransactionIssuerId = _OGatewayInfo.StoreId;
                            _OGatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.MerchantStore;
                        }
                        else
                        {
                            _OGatewayInfo.TransactionIssuerId = SystemAccounts.ThankUCashMerchant;
                            _OGatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.Merchant;
                        }

                    }
                    return _OGatewayInfo;
                }
                else
                {
                    return null;
                }
            }
        }




        internal static OUserInfo GetUserInfo(OThankUGateway.Request _Request, OGatewayInfo _GatewayInfo, bool CreateUser)
        {
            OUserInfo _OUserInfo = new OUserInfo();
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                if (!string.IsNullOrEmpty(_Request.CardNumber) && !string.IsNullOrEmpty(_Request.TagNumber))
                {
                    if (!string.IsNullOrEmpty(_Request.CardNumber) && !string.IsNullOrEmpty(_Request.TagNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber
                            && x.SerialNumber == _Request.TagNumber)
                            .Select(x => new OTUInfo
                            {
                                CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.CardNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber)
                            .Select(x => new OTUInfo
                            {
                                CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.TagNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.TagNumber)
                            .Select(x => new OTUInfo
                            {
                                CardId = x.Id,
                                UserAccountId = x.ActiveUserAccountId,
                                UserId = x.ActiveUserAccount.UserId,
                                AccountType = x.ActiveUserAccount.AccountTypeId,
                                MobileNumber = x.ActiveUserAccount.MobileNumber,
                                DisplayName = x.ActiveUserAccount.DisplayName,
                                UserPin = x.ActiveUserAccount.AccessPin,
                                CreatedById = x.ActiveUserAccount.CreatedById,
                                CreatedByAccountTypeId = x.ActiveUserAccount.CreatedBy.AccountTypeId,
                                EmailAddress = x.ActiveUserAccount.EmailAddress,
                                OwnerId = x.ActiveUserAccount.OwnerId,
                                OwnerAccountTypeId = x.ActiveUserAccount.Owner.AccountTypeId,
                                CardStatusId = x.StatusId,
                                AccountStatusId = x.ActiveUserAccount.StatusId,
                            }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                //_OUserInfo.CardId = CardDetails.CardId;
                                _OUserInfo.DisplayName = CardDetails.DisplayName;
                                _OUserInfo.MobileNumber = CardDetails.MobileNumber;
                                _OUserInfo.EmailAddress = CardDetails.EmailAddress;
                                _OUserInfo.UserAccountId = (long)CardDetails.UserAccountId;
                                _OUserInfo.Pin = CardDetails.UserPin;
                                if (CardDetails.AccountStatusId != null)
                                {
                                    _OUserInfo.AccountStatusId = (long)CardDetails.AccountStatusId;
                                }
                                if (CardDetails.OwnerId != null)
                                {
                                    _OUserInfo.IssuerId = (long)CardDetails.OwnerId;
                                    _OUserInfo.IssuerAccountTypeId = (long)CardDetails.OwnerAccountTypeId;
                                }
                                if (CardDetails.CreatedById != null)
                                {
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == CardDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (CardDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _OUserInfo.IssuerId = (long)CardDetails.CreatedById;
                                        _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }

                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;

                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else if (!string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    string AppUserCardId = _Request.MobileNumber;
                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                    string AppUserLoginId = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    var UserAccount = _HCoreContext.HCUAccount 
                    .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && (x.User.Username == AppUserLoginId || x.AccountCode == AppUserCardId))
                                                   .Select(x => new OUserInfo
                                                   {
                                                       UserAccountId = x.Id,
                                                       DisplayName = x.DisplayName,
                                                       Pin = x.AccessPin,
                                                       CreatedById = x.CreatedById,
                                                       CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                       OwnerId = x.OwnerId,
                                                       OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                       EmailAddress = x.EmailAddress,
                                                       AccountStatusId = x.StatusId,
                                                       MobileNumber = x.MobileNumber,
                                                       AccountNumber = x.MobileNumber,
                                                   }).FirstOrDefault();
                    if (UserAccount != null)
                    {
                        _OUserInfo.AccountNumber = UserAccount.AccountNumber;
                        _OUserInfo.MobileNumber = UserAccount.MobileNumber;
                        _OUserInfo.DisplayName = UserAccount.DisplayName;
                        _OUserInfo.UserAccountId = UserAccount.UserAccountId;
                        _OUserInfo.Pin = UserAccount.Pin;
                        _OUserInfo.EmailAddress = UserAccount.EmailAddress;
                        _OUserInfo.AccountStatusId = UserAccount.AccountStatusId;
                        if (UserAccount.OwnerId != null)
                        {
                            _OUserInfo.IssuerId = (long)UserAccount.OwnerId;
                            _OUserInfo.IssuerAccountTypeId = (long)UserAccount.OwnerAccountTypeId;
                        }
                        if (UserAccount.CreatedById != null)
                        {
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                            {
                                _OUserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccount.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            }
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.Acquirer)
                            {
                                _OUserInfo.IssuerId = (long)UserAccount.CreatedById;
                                _OUserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                            }
                        }
                        if (_OUserInfo.IssuerId == 0)
                        {
                            _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                            _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                        }
                        return _OUserInfo;
                    }
                    else
                    {
                        if (CreateUser == true && !AppUserCardId.StartsWith("20"))
                        {
                            OAppProfile.Request _AppProfileRequest = new OAppProfile.Request();
                            if (_GatewayInfo != null && _GatewayInfo.MerchantId != 0)
                            {
                                _AppProfileRequest.OwnerId = _GatewayInfo.MerchantId;
                            }
                            if (_GatewayInfo != null && _GatewayInfo.StoreId != 0)
                            {
                                _AppProfileRequest.SubOwnerId = _GatewayInfo.StoreId;
                            }
                            if (_GatewayInfo != null && _GatewayInfo.TerminalId != 0)
                            {
                                _AppProfileRequest.CreatedById = _GatewayInfo.TerminalId;
                            }
                            _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                            _AppProfileRequest.DisplayName = _Request.MobileNumber;
                            _AppProfileRequest.UserReference = _Request.UserReference;
                            ManageCoreUserAccess _ManageCoreUserAccess = new ManageCoreUserAccess();
                            OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                            if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                            {
                                if (_AppUserCreateResponse.StatusId != 0)
                                {
                                    _OUserInfo.AccountStatusId = _AppUserCreateResponse.StatusId;
                                }
                                else
                                {
                                    _OUserInfo.AccountStatusId = HelperStatus.Default.Active;
                                }

                                _OUserInfo.UserAccountId = _AppUserCreateResponse.AccountId;
                                if (_GatewayInfo != null && _GatewayInfo.MerchantId != 0)
                                {
                                    _OUserInfo.IssuerId = _GatewayInfo.MerchantId;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                if (_OUserInfo.IssuerId == 0)
                                {
                                    _OUserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _OUserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                return _OUserInfo;
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }

                    }
                }
                else
                {
                    return null;
                }
            }
        }
        internal OResponse GetBalance(OThankUGateway.Request _Request)
        {
            #region Declare
            _GatewayResponse = new OThankUGateway.Response();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber) && string.IsNullOrEmpty(_Request.CardNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG109", CoreResources.HCG109);
                    #endregion
                }
                else
                {
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG145", CoreResources.HCG145);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG146", CoreResources.HCG146);
                        #endregion
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber) &&  !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG147", CoreResources.HCG147);
                        #endregion
                    }
                    #region Perform Operations
                    OGatewayInfo _OGatewayInfo = GetGatewayInfo(_Request);
                    if (_OGatewayInfo != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.CashierId))
                        {
                            if (_OGatewayInfo.CashierId == 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG134", CoreResources.HCG134);
                                #endregion
                            }
                            else if (_OGatewayInfo.CashierId != 0 && _OGatewayInfo.CashierStatusId != HelperStatus.Default.Active)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG135", CoreResources.HCG135);
                                #endregion
                            }
                        }
                        OUserInfo _UserInfo = GetUserInfo(_Request, null, false);
                        if (_UserInfo != null)
                        {
                            if (_UserInfo.AccountStatusId == HelperStatus.Default.Active)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    double ProductAmount = 0;
                                    #region Get User Products
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        //if (!string.IsNullOrEmpty(_UserInfo.DealKey))
                                        //{
                                        //    var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Guid == _UserInfo.DealKey
                                        //                    && x.Deal.AccountId == _OGatewayInfo.MerchantId
                                        //                    && x.StatusId == HelperStatus.DealCodes.Unused)
                                        //                  .Select(x => new ODealOperation.DealCode.Response
                                        //                  {
                                        //                      ReferenceId = x.Id,
                                        //                      ReferenceKey = x.Guid,
                                        //                      ItemCode = x.ItemCode,

                                        //                      StartDate = x.StartDate,
                                        //                      EndDate = x.EndDate,
                                        //                      Amount = x.ItemAmount,
                                        //                      MerchantReferenceId = x.Deal.AccountId,
                                        //                      CustomerId = x.Account.Id,
                                        //                      StatusId = x.StatusId,
                                        //                  })
                                        //                  .FirstOrDefault();
                                        //    if (DealCodeDetails != null)
                                        //    {

                                        //        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                        //        if (CurrentTime > DealCodeDetails.StartDate && CurrentTime < DealCodeDetails.EndDate)
                                        //        {
                                        //            DealBalance = (double)DealCodeDetails.Amount;
                                        //        }
                                        //    }
                                        //}
                                        var ProductBalance = _HCoreContext.CAProductCode
                                                                  .Where(x => x.AccountId == _UserInfo.UserAccountId
                                                                          && ((x.Product.UsageTypeId == HelperType.ProductUsageType.FullValue && x.AvailableAmount > 0)
                                                                               || (x.Product.UsageTypeId == HelperType.ProductUsageType.PartialValue && x.AvailableAmount > 0))
                                                                          && (x.Product.TypeId == Product.GiftCard || x.Product.TypeId == Product.QuickGiftCard)
                                                                          && x.AvailableAmount > 0
                                                                          && x.StatusId == HelperStatus.ProdutCode.Unused)
                                                                  .Select(x => new
                                                                  {
                                                                      ProductId = x.ProductId,
                                                                      CodeId = x.Id,
                                                                      x.AvailableAmount,
                                                                      UsageTypeId = x.Product.UsageTypeId
                                                                  })
                                                                  .FirstOrDefault();
                                        if (ProductBalance != null)
                                        {
                                            ProductAmount = (double)ProductBalance.AvailableAmount;
                                        } 
                                        _HCoreContext.Dispose();
                                    }
                                    #endregion
                                    #region Get Balance
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    _GatewayResponse.ThankUCashBalance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_UserInfo.UserAccountId) * 100, 0);
                                    _GatewayResponse.GiftPointBalance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserGiftPointsBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId) * 100, 0);
                                    _GatewayResponse.GiftCardBalance = (long)HCoreHelper.RoundNumber(ProductAmount * 100, 0);
                                    _GatewayResponse.ThankUCashPlusBalance = (long)HCoreHelper.RoundNumber(((_ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_UserInfo.UserAccountId, _OGatewayInfo.MerchantId)) * 100), 0);
                                    _GatewayResponse.Balance = _GatewayResponse.GiftPointBalance + _GatewayResponse.ThankUCashBalance + _GatewayResponse.GiftCardBalance;
                                    if (_GatewayResponse.Balance < 0)
                                    {
                                        _GatewayResponse.Balance = 0;
                                    }
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG113", CoreResources.HCG113);
                                    #endregion
                                }
                            }
                            else 
                            {
                                _GatewayResponse.Balance = 0;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GatewayResponse, "HCG129", CoreResources.HCG129);
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG111", CoreResources.HCG111);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _GatewayResponse, "HCG103", CoreResources.HCG103);
                        #endregion
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
    }
}
 