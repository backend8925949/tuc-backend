//==================================================================================
// FileName: FrameworkSoftcom.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

//using HCore.Data;
//using HCore.Data.Models;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Linq.Dynamic.Core;
//using HCore.Helper;
//using static HCore.Helper.HCoreConstant;
//using HCore.ThankUCash.Gateway.Object;

//namespace HCore.ThankUCash.Gateway.Integration
//{
//    public class ResourceItems
//    {
//        public const string HCP100 = "Merchant code required";
//        public const string HCP101 = "Merchant details not found";
//        public const string HCP102 = "Product udpate successful";
//        public const string HCP500 = "Error occured while processing request. Try after some time";
//        public const string HCC104 = "Configuration value required";
//        public const string HCC105 = "Configuration data type required";
//        public const string HCC106 = "Configuration already exists";
//        public const string HCC107 = "Configuration added successfully";
//        public const string HCC108 = "Configuration key missing";
//        public const string HCC109 = "Configuration details not found";
//        public const string HCC110 = "Configuration details  updated successfully";
//        public const string HCC111 = "Configuration deleted successfully";
//        public const string HCC112 = "Configuration value updated successfully";
//        public const string HCC113 = "Configuration loaded";
//        public const string HCC114 = "Account type required";
//        public const string HCC115 = "Account type added to configuration";
//        public const string HCC116 = "Account type removed from configuration";
//        public const string HCC117 = "Account type not found for configuration";
//        public const string HCC118 = "Merchant code rquired";
//        public const string HCC119 = "No products to process";
//        public const string HCC120 = "Product sku required";
//        public const string HCC121 = "Product name required";
//        public const string HCC122 = "Product amount must be greater than 0";
//        public const string HCC123 = "Product reference id required";
//        public const string HCC124 = "Product reference missing";
//        public const string HCC125 = "Product details updated";
//        public const string HCC126 = "New product created";
//        public const string HCC127 = "Products processed successfully";
//    }
//    public class FrameworkSoftcom
//    {
//        HCoreContext _HCoreContext;
//        TUCTerminalProduct _TUCTerminalProduct;
//        OThankUGateway.ProductResponse _Prodresponse;
//        List<OProductManager.DetailsResponse> _Products;
//        internal OResponse SoftCon_SaveProduct(OThankUGateway.SCNotifyProductPaymentRequest _Request)
//        {
//            #region Manage Exception
//            try
//            {
//                using (_HCoreContext = new HCoreContext())
//                {
//                    long MerchantId = 0;
//                    if (HostEnvironment == HostEnvironmentType.Live)
//                    {
//                        MerchantId = 49648;
//                    }
//                    else if (HostEnvironment == HostEnvironmentType.Test)
//                    {
//                        MerchantId = 95070;
//                    }
//                    else if (HostEnvironment == HostEnvironmentType.Local)
//                    {
//                        MerchantId = 49648;
//                    }
//                    var MerchantDetails = _HCoreContext.HCUAccount
//                        .Where(x => x.AccountTypeId == Helpers.UserAccountType.Merchant
//                        && x.StatusId == HelperStatus.Default.Active
//                        && x.Id == MerchantId)
//                        .FirstOrDefault();
//                    if (MerchantDetails != null)
//                    {
//                        if (_Request.Product != null)
//                        {
//                            var ProductDetails = _HCoreContext.TUCTerminalProduct
//                                                              .Where(x => x.AccountId == MerchantDetails.Id && x.ReferenceNumber == _Request.Product._id)
//                                                              .FirstOrDefault();
//                            if (ProductDetails != null)
//                            {
//                                if (!string.IsNullOrEmpty(_Request.Product.name))
//                                {
//                                    ProductDetails.Name = _Request.Product.name;
//                                    ProductDetails.SystemName = HCoreHelper.GenerateSystemName(_Request.Product.name);
//                                }
//                                //if (!string.IsNullOrEmpty(Product.CategoryName) && ProductDetails.CategoryName != Product.CategoryName)
//                                //{
//                                //    ProductDetails.CategoryName = Product.CategoryName;
//                                //}
//                                //if (!string.IsNullOrEmpty(Product.SubCategoryName) && ProductDetails.SubCategoryName != Product.SubCategoryName)
//                                //{
//                                //    ProductDetails.SubCategoryName = Product.SubCategoryName;
//                                //}
//                                ProductDetails.ActualPrice = (double)_Request.Product.price / 100;
//                                ProductDetails.SellingPrice = (double)_Request.Product.price / 100;
//                                ProductDetails.TotalStock = _Request.Product.totalStock;
//                                ProductDetails.AvailableStock = _Request.Product.totalStock;
//                                ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                ProductDetails.ModifyById = _Request.UserReference.AccountId;
//                                if (_Request.Product.visibleOnNearby)
//                                {
//                                    ProductDetails.StatusId = HelperStatus.Default.Active;
//                                }
//                                else
//                                {
//                                    ProductDetails.StatusId = HelperStatus.Default.Inactive;
//                                }
//                                if (_Request.Product.deleted)
//                                {
//                                    ProductDetails.StatusId = HelperStatus.Default.Blocked;
//                                }
//                                ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                _HCoreContext.SaveChanges();
//                            }
//                            else
//                            {
//                                using (_HCoreContext = new HCoreContext())
//                                {
//                                    _TUCTerminalProduct = new TUCTerminalProduct();
//                                    _TUCTerminalProduct.Guid = HCoreHelper.GenerateGuid();
//                                    _TUCTerminalProduct.ReferenceNumber = _Request.Product._id;
//                                    _TUCTerminalProduct.Sku = _Request.Product._id;
//                                    _TUCTerminalProduct.AccountId = MerchantDetails.Id;
//                                    _TUCTerminalProduct.Name = _Request.Product.name;
//                                    _TUCTerminalProduct.SystemName = HCoreHelper.GenerateSystemName(_Request.Product.name);
//                                    _TUCTerminalProduct.ActualPrice = _Request.Product.price;
//                                    _TUCTerminalProduct.SellingPrice = _Request.Product.price;
//                                    _TUCTerminalProduct.TotalStock = _Request.Product.totalStock;
//                                    _TUCTerminalProduct.CreateDate = HCoreHelper.GetGMTDateTime();
//                                    _TUCTerminalProduct.CreatedById = _Request.UserReference.AccountId;
//                                    _TUCTerminalProduct.StatusId = HelperStatus.Default.Active;
//                                    if (_Request.Product.visibleOnNearby)
//                                    {
//                                        _TUCTerminalProduct.StatusId = HelperStatus.Default.Active;
//                                    }
//                                    else
//                                    {
//                                        _TUCTerminalProduct.StatusId = HelperStatus.Default.Inactive;
//                                    }
//                                    if (_Request.Product.deleted)
//                                    {
//                                        _TUCTerminalProduct.StatusId = HelperStatus.Default.Blocked;
//                                    }
//                                    _HCoreContext.TUCTerminalProduct.Add(_TUCTerminalProduct);
//                                    _HCoreContext.SaveChanges();
//                                }
//                            }
//                        }

//                        //if (_Request.Product != null)
//                        //{
//                        //    var ProductDetails = _HCoreContext.HCPProduct
//                        //                                      .Where(x => x.AccountId == MerchantDetails.Id && x.ReferenceNumber == _Request.Product._id)
//                        //                                      .FirstOrDefault();
//                        //    if (ProductDetails != null)
//                        //    { 
//                        //        ProductDetails.Name = _Request.Product.name;
//                        //        ProductDetails.SystemName = HCoreHelper.GenerateSystemName(_Request.Product.name);
//                        //        ProductDetails.ReferenceNumber = _Request.Product._id;
//                        //        ProductDetails.MinimumAmount = _Request.Product.price;
//                        //        ProductDetails.MaximumAmount = _Request.Product.price;
//                        //        ProductDetails.MinimumQuantity = 0;
//                        //        ProductDetails.MaximumQuantity = 0;
//                        //        ProductDetails.ActualPrice = _Request.Product.price;
//                        //        ProductDetails.SellingPrice = _Request.Product.price;
//                        //        ProductDetails.AllowMultiple = 1;
//                        //        ProductDetails.TotalStock = _Request.Product.totalStock;
//                        //        ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
//                        //        if (_Request.Product.visibleOnNearby)
//                        //        {
//                        //            ProductDetails.StatusId = HelperStatus.Default.Active;
//                        //        }
//                        //        else
//                        //        {
//                        //            ProductDetails.StatusId = HelperStatus.Default.Inactive;
//                        //        }
//                        //        if (_Request.Product.deleted)
//                        //        {
//                        //            ProductDetails.StatusId = HelperStatus.Default.Blocked;
//                        //        }
//                        //        _HCoreContext.SaveChanges();
//                        //    }
//                        //    else
//                        //    {
//                        //        using (_HCoreContext = new HCoreContext())
//                        //        {
//                        //            _HCPProduct = new HCPProduct();
//                        //            _HCPProduct.Guid = HCoreHelper.GenerateGuid();
//                        //            _HCPProduct.Sku = HCoreHelper.GenerateRandomNumber(10);
//                        //            _HCPProduct.AccountId = MerchantDetails.Id;
//                        //            _HCPProduct.Name = _Request.Product.name;
//                        //            _HCPProduct.SystemName = HCoreHelper.GenerateSystemName(_Request.Product.name);
//                        //            _HCPProduct.MinimumAmount = _Request.Product.price;
//                        //            _HCPProduct.MaximumAmount = _Request.Product.price;
//                        //            _HCPProduct.MinimumQuantity = 0;
//                        //            _HCPProduct.MaximumQuantity = 0;
//                        //            _HCPProduct.ActualPrice = _Request.Product.price;
//                        //            _HCPProduct.SellingPrice = _Request.Product.price;
//                        //            _HCPProduct.AllowMultiple = 1;
//                        //            _HCPProduct.ReferenceNumber = _Request.Product._id;
//                        //            _HCPProduct.TotalStock = _Request.Product.totalStock;
//                        //            _HCPProduct.CreateDate = HCoreHelper.GetGMTDateTime();
//                        //            _HCPProduct.CreatedById = _Request.UserReference.AccountId;
//                        //            _HCPProduct.StatusId = HelperStatus.Default.Active;
//                        //            if (_Request.Product.visibleOnNearby)
//                        //            {
//                        //                _HCPProduct.StatusId = HelperStatus.Default.Active;
//                        //            }
//                        //            else
//                        //            {
//                        //                _HCPProduct.StatusId = HelperStatus.Default.Inactive;
//                        //            }
//                        //            if (_Request.Product.deleted)
//                        //            {
//                        //                _HCPProduct.StatusId = HelperStatus.Default.Blocked;
//                        //            }
//                        //            _HCoreContext.HCPProduct.Add(_HCPProduct);
//                        //            _HCoreContext.SaveChanges();
//                        //        }
//                        //    }
//                        //}
//                        #region Send Response
//                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP102", ResourceItems.HCP102);
//                        #endregion
//                    }
//                    else
//                    {
//                        #region Send Response
//                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP101", ResourceItems.HCP101);
//                        #endregion
//                    }
//                }
//            }
//            catch (Exception _Exception)
//            {
//                #region  Log Exception
//                HCoreHelper.LogException("SaveProduct", _Exception, _Request.UserReference);
//                #endregion
//                #region Send Response
//                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP500", ResourceItems.HCP500);
//                #endregion
//            }
//            #endregion
//        }
//        internal OResponse SaveProduct(OThankUGateway.Request _Request)
//        {
//            #region Manage Exception
//            try
//            {
//                if (string.IsNullOrEmpty(_Request.MerchantId))
//                {
//                    #region Send Response
//                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC118", ResourceItems.HCC118);
//                    #endregion
//                }
//                if (_Request.Products == null || _Request.Products.Count == 0)
//                {
//                    #region Send Response
//                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC119", ResourceItems.HCC119);
//                    #endregion
//                }
//                using (_HCoreContext = new HCoreContext())
//                {
//                    var MerchantDetails = _HCoreContext.HCUAccount
//                        .Where(x => x.AccountTypeId == Helpers.UserAccountType.Merchant
//                        && x.StatusId == HelperStatus.Default.Active
//                        && x.AccountCode == _Request.MerchantId)
//                        .FirstOrDefault();
//                    if (MerchantDetails != null)
//                    {
//                        _Prodresponse = new OThankUGateway.ProductResponse();
//                        _Products = new List<OProductManager.DetailsResponse>();
//                        _Prodresponse.MerchantId = _Request.MerchantId;
//                        foreach (var Product in _Request.Products)
//                        {
//                            if (string.IsNullOrEmpty(Product.Sku))
//                            {
//                                Product.ResponseStatus = "Error";
//                                Product.ResponseCode = "HCC120";
//                                Product.ResponseMessage = ResourceItems.HCC120;
//                            }
//                            else if (string.IsNullOrEmpty(Product.Name))
//                            {
//                                Product.ResponseStatus = "Error";
//                                Product.ResponseCode = "HCC121";
//                                Product.ResponseMessage = ResourceItems.HCC121;
//                            }
//                            else if (Product.Price < 1)
//                            {
//                                Product.ResponseStatus = "Error";
//                                Product.ResponseCode = "HCC122";
//                                Product.ResponseMessage = ResourceItems.HCC122;
//                            }
//                            else if (string.IsNullOrEmpty(Product.ReferenceId))
//                            {
//                                Product.ResponseStatus = "Error";
//                                Product.ResponseCode = "HCC123";
//                                Product.ResponseMessage = ResourceItems.HCC123;
//                            }
//                            else if (string.IsNullOrEmpty(Product.Status))
//                            {
//                                Product.ResponseStatus = "Error";
//                                Product.ResponseCode = "HCC124";
//                                Product.ResponseMessage = ResourceItems.HCC124;
//                            }
//                            else
//                            {
//                                var ProductDetails = _HCoreContext.TUCTerminalProduct.Where(x => x.AccountId == MerchantDetails.Id && x.ReferenceNumber == Product.ReferenceId).FirstOrDefault();
//                                if (ProductDetails != null)
//                                {
//                                    if (!string.IsNullOrEmpty(Product.Name))
//                                    {
//                                        ProductDetails.Name = Product.Name;
//                                        ProductDetails.SystemName = HCoreHelper.GenerateSystemName(Product.Name);
//                                    }
//                                    if (!string.IsNullOrEmpty(Product.CategoryName) && ProductDetails.CategoryName != Product.CategoryName)
//                                    {
//                                        ProductDetails.CategoryName = Product.CategoryName;
//                                    }
//                                    if (!string.IsNullOrEmpty(Product.SubCategoryName) && ProductDetails.SubCategoryName != Product.SubCategoryName)
//                                    {
//                                        ProductDetails.SubCategoryName = Product.SubCategoryName;
//                                    }
//                                    ProductDetails.ActualPrice = (double)Product.Price / 100;
//                                    ProductDetails.SellingPrice = (double)Product.Price / 100;
//                                    ProductDetails.TotalStock = Product.TotalStock;
//                                    ProductDetails.AvailableStock = Product.TotalStock;
//                                    ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
//                                    ProductDetails.ModifyById = _Request.UserReference.AccountId;
//                                    ProductDetails.StatusId = HelperStatus.Default.Active;
//                                    if (!string.IsNullOrEmpty(Product.Status))
//                                    {
//                                        if (Product.Status == "active")
//                                        {
//                                            ProductDetails.StatusId = HelperStatus.Default.Active;
//                                        }
//                                        if (Product.Status == "inactive")
//                                        {
//                                            ProductDetails.StatusId = HelperStatus.Default.Inactive;
//                                        }
//                                        if (Product.Status == "deleted")
//                                        {
//                                            ProductDetails.StatusId = HelperStatus.Default.Blocked;
//                                        }
//                                    }
//                                    _HCoreContext.SaveChanges();
//                                    Product.ResponseStatus = "Success";
//                                    Product.ResponseCode = "HCC125";
//                                    Product.ResponseMessage = ResourceItems.HCC125;

//                                }
//                                else
//                                {
//                                    using (_HCoreContext = new HCoreContext())
//                                    {
//                                        _TUCTerminalProduct = new TUCTerminalProduct();
//                                        _TUCTerminalProduct.Guid = HCoreHelper.GenerateGuid();
//                                        _TUCTerminalProduct.ReferenceNumber = Product.ReferenceId;
//                                        _TUCTerminalProduct.AccountId = MerchantDetails.Id;
//                                        if (!string.IsNullOrEmpty(Product.Sku))
//                                        {
//                                            _TUCTerminalProduct.Sku = Product.Sku;
//                                        }
//                                        else
//                                        {
//                                            _TUCTerminalProduct.Sku = HCoreHelper.GenerateRandomNumber(10);
//                                        }
//                                        _TUCTerminalProduct.Name = Product.Name;
//                                        _TUCTerminalProduct.SystemName = HCoreHelper.GenerateSystemName(Product.Name);
//                                        _TUCTerminalProduct.CategoryName = Product.CategoryName;
//                                        _TUCTerminalProduct.SubCategoryName = Product.SubCategoryName;
//                                        _TUCTerminalProduct.ActualPrice = (double)Product.Price / 100;
//                                        _TUCTerminalProduct.SellingPrice = (double)Product.Price / 100;
//                                        _TUCTerminalProduct.TotalStock = Product.TotalStock;
//                                        _TUCTerminalProduct.AvailableStock = Product.TotalStock;
//                                        _TUCTerminalProduct.CreateDate = HCoreHelper.GetGMTDateTime();
//                                        _TUCTerminalProduct.CreatedById = _Request.UserReference.AccountId;
//                                        _TUCTerminalProduct.StatusId = HelperStatus.Default.Active;
//                                        if (!string.IsNullOrEmpty(Product.Status))
//                                        {
//                                            if (Product.Status == "active")
//                                            {
//                                                _TUCTerminalProduct.StatusId = HelperStatus.Default.Active;
//                                            }
//                                            if (Product.Status == "inactive")
//                                            {
//                                                _TUCTerminalProduct.StatusId = HelperStatus.Default.Inactive;
//                                            }
//                                            if (Product.Status == "deleted")
//                                            {
//                                                _TUCTerminalProduct.StatusId = HelperStatus.Default.Blocked;
//                                            }
//                                        }
//                                        _HCoreContext.TUCTerminalProduct.Add(_TUCTerminalProduct);
//                                        _HCoreContext.SaveChanges();
//                                        Product.ResponseStatus = "Success";
//                                        Product.ResponseCode = "HCC126";
//                                        Product.ResponseMessage = ResourceItems.HCC126;
//                                    }
//                                }
//                            }
//                            _Products.Add(new OProductManager.DetailsResponse
//                            {
//                                ReferenceId = Product.ReferenceId,
//                                Sku = Product.Sku,
//                                StatusCode = Product.ResponseCode,
//                                Status = Product.ResponseStatus,
//                                Message = Product.ResponseMessage
//                            });
//                        }
//                        _Prodresponse.Products = _Products;
//                        #region Send Response
//                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Prodresponse, "HCC127", ResourceItems.HCC127);
//                        #endregion
//                    }
//                    else
//                    {
//                        #region Send Response
//                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP101", ResourceItems.HCP101);
//                        #endregion
//                    }
//                }
//            }
//            catch (Exception _Exception)
//            {
//                #region  Log Exception
//                HCoreHelper.LogException("SaveProduct", _Exception, _Request.UserReference);
//                #endregion
//                #region Send Response
//                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP500", ResourceItems.HCP500);
//                #endregion
//            }
//            #endregion
//        }
//    }
//}
