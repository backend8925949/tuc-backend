//==================================================================================
// FileName: ActorCampaignProcessor.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System.IO;
using System.Net;
using System.Net.Http;
using Akka.Actor;
using HCore.Helper;
using HCore.ThankUCash.Gateway.Framework;
using HCore.Operations.Framework;
using HCore.Operations.Object;
using Newtonsoft.Json;
using HCore.ThankUCash.Gateway.Object;

namespace HCore.ThankUCash.Gateway.Actor
{
     public class AccountOtherCardCampaignProcessorActor : ReceiveActor
    {
        FrameworkCampaignProcessor _FrameworkCampaignProcessor;
        public AccountOtherCardCampaignProcessorActor()
        {
            Receive<OCampaignProcessor>(_Request =>
            {
                _FrameworkCampaignProcessor = new FrameworkCampaignProcessor();
                _FrameworkCampaignProcessor.ProcessBinNumber(_Request);
            });
        }
    }
}
