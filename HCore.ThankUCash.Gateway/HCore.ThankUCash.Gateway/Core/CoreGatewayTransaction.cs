//==================================================================================
// FileName: CoreGatewayTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;
using static HCore.Helper.HCoreConstant;
using Akka.Actor;
using System.Net;
using Newtonsoft.Json;
using HCore.ThankUCash.Gateway.Object;

namespace HCore.ThankUCash.Gateway.Core
{
    public class CoreGatewayTransaction
    {
        HCCoreCommon _HCCoreCommon;
        HCCoreBinNumber _HCCoreBinNumber;
        HCoreContext _HCoreContext;
        OTrCoreProcess _OTrCoreProcess;
        HCCoreCountry _HCCoreCountry;
        internal void ProcessRewardTransaction(string GroupKey, OThankUGateway.Request _ExternalRequest, OGatewayInfo _GatewayInfo, OUserInfo _UserInfo, OAmountDistribution _AmountDistributionReference)
        {
            _OTrCoreProcess = new OTrCoreProcess();
            _OTrCoreProcess.GroupKey = GroupKey;
            _OTrCoreProcess._ExternalRequest = _ExternalRequest;
            _OTrCoreProcess._GatewayInfo = _GatewayInfo;
            _OTrCoreProcess._UserInfo = _UserInfo;
            _OTrCoreProcess._AmountDistributionReference = _AmountDistributionReference;
            var _TransactionPostProcessActor = ActorSystem.Create("PostProcessRewardTransactionActor");
            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<PostProcessRewardTransactionActor>("PostProcessRewardTransactionActor");
            _TransactionPostProcessActorNotify.Tell(_OTrCoreProcess);
        }
        internal void ProcessBinNumber(long TransactionId)
        {
            int CardBinNumberId = 0;
            long BrandId = 0;
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Transaction = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == TransactionId).FirstOrDefault();
                    if (!string.IsNullOrEmpty(Transaction.AccountNumber))
                    {
                        if (Transaction.AccountNumber.Length >= 6)
                        {
                            int CountryId = 0;
                            long CardTypeId = 0;
                            long BankId = 0;
                            string AccountNumber = Transaction.AccountNumber;
                            string TBinStart = AccountNumber.Substring(0, 6);
                            string TBinEnd = AccountNumber.Replace(TBinStart, "").Replace("X", "").Replace("*", "").Replace("x", "");
                            var BinCheck = _HCoreContext.HCCoreBinNumber.Where(x => x.Bin == TBinStart).FirstOrDefault();
                            if (BinCheck != null)
                            {
                                CardBinNumberId = BinCheck.Id;
                                if (BinCheck.BrandId != null)
                                {
                                    BrandId = (long)BinCheck.BrandId;
                                }
                                if (BinCheck.CountryId != null)
                                {
                                    CountryId = (int)BinCheck.CountryId;
                                }
                                if (BinCheck.BankId != null)
                                {
                                    BankId = (long)BinCheck.BankId;
                                }
                                _HCoreContext.Dispose();
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var smsapiurl = "https://api.paystack.co/decision/bin/" + TBinStart;
                                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(smsapiurl);
                                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                                    System.IO.StreamReader _StreamReader = new System.IO.StreamReader(_HttpWebResponse.GetResponseStream());
                                    string responseString = _StreamReader.ReadToEnd();
                                    _StreamReader.Close();
                                    _HttpWebResponse.Close();
                                    BinStatus _BinInfo = JsonConvert.DeserializeObject<BinStatus>(responseString);
                                    if (_BinInfo.status == true)
                                    {
                                        if (_BinInfo.data.brand != "Unknown")
                                        {
                                            _BinInfo.data.brand_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.brand);
                                            _BinInfo.data.sub_brand_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.sub_brand);
                                            _BinInfo.data.country_code_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.country_code);
                                            _BinInfo.data.country_name_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.country_name);
                                            _BinInfo.data.card_type_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.card_type);
                                            _BinInfo.data.bank_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.bank);
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                BrandId = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand && x.SystemName == _BinInfo.data.brand_systemname).Select(x => x.Id).FirstOrDefault();
                                                CountryId = _HCoreContext.HCCoreCountry.Where(x => x.SystemName == _BinInfo.data.country_name_systemname).Select(x => x.Id).FirstOrDefault();
                                                CardTypeId = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardType && x.SystemName == _BinInfo.data.card_type_systemname).Select(x => x.Id).FirstOrDefault();
                                                BankId = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank && x.SystemName == _BinInfo.data.bank_systemname).Select(x => x.Id).FirstOrDefault();
                                                _HCoreContext.Dispose();
                                                #region Save Bin Country
                                                if (BrandId == 0 && !string.IsNullOrEmpty(_BinInfo.data.brand))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCommon = new HCCoreCommon();
                                                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCommon.TypeId = HelperType.BinCardBrand;
                                                        _HCCoreCommon.Name = _BinInfo.data.brand;
                                                        _HCCoreCommon.SystemName = _BinInfo.data.brand_systemname;
                                                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                                        _HCoreContext.SaveChanges();
                                                        BrandId = _HCCoreCommon.Id;
                                                    }
                                                }
                                                #endregion
                                                #region Save Bin Country
                                                if (CountryId == 0 && !string.IsNullOrEmpty(_BinInfo.data.country_name))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCountry = new HCCoreCountry();
                                                        _HCCoreCountry.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCountry.Name = _BinInfo.data.country_name;
                                                        _HCCoreCountry.SystemName = _BinInfo.data.country_name_systemname;
                                                        _HCCoreCountry.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCountry.CreatedById = 1;
                                                        _HCCoreCountry.StatusId = HelperStatus.Default.Active;
                                                        _HCoreContext.HCCoreCountry.Add(_HCCoreCountry);
                                                        _HCoreContext.SaveChanges();
                                                        CountryId = _HCCoreCountry.Id;
                                                    }
                                                }
                                                #endregion
                                                #region Save Bin Card Type
                                                if (CardTypeId == 0 && !string.IsNullOrEmpty(_BinInfo.data.card_type))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCommon = new HCCoreCommon();
                                                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCommon.TypeId = HelperType.BinCardType;
                                                        _HCCoreCommon.Name = _BinInfo.data.card_type;
                                                        _HCCoreCommon.SystemName = _BinInfo.data.card_type_systemname;
                                                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                                        _HCoreContext.SaveChanges();
                                                        CardTypeId = _HCCoreCommon.Id;
                                                    }
                                                }
                                                #endregion
                                                #region Save Bin Bank
                                                if (BankId == 0 && !string.IsNullOrEmpty(_BinInfo.data.bank))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCommon = new HCCoreCommon();
                                                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCommon.TypeId = HelperType.BinCardBank;
                                                        _HCCoreCommon.Name = _BinInfo.data.bank;
                                                        _HCCoreCommon.SystemName = _BinInfo.data.bank_systemname;
                                                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                                        _HCoreContext.SaveChanges();
                                                        BankId = _HCCoreCommon.Id;
                                                    }
                                                }
                                                #endregion
                                            }
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                _HCCoreBinNumber = new HCCoreBinNumber();
                                                _HCCoreBinNumber.Guid = HCoreHelper.GenerateGuid();
                                                _HCCoreBinNumber.Bin = TBinStart;
                                                //_HCCoreBinNumber.BinEnd =TBinEnd;
                                                //_HCCoreBinNumber.BinNumber = AccountNumber;
                                                if (CardTypeId != 0)
                                                {
                                                    _HCCoreBinNumber.TypeId = CardTypeId;
                                                }
                                                if (BrandId != 0)
                                                {
                                                    _HCCoreBinNumber.BrandId = BrandId;
                                                }
                                                if (BankId != 0)
                                                {
                                                    _HCCoreBinNumber.BankId = BankId;
                                                }
                                                if (CountryId != 0)
                                                {
                                                    _HCCoreBinNumber.CountryId = CountryId;
                                                }
                                                _HCCoreBinNumber.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCCoreBinNumber.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                                _HCoreContext.HCCoreBinNumber.Add(_HCCoreBinNumber);
                                                _HCoreContext.SaveChanges();
                                                CardBinNumberId = _HCCoreBinNumber.Id;
                                            }
                                        }
                                    }
                                }
                            }
                            if (CardBinNumberId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var BinNumberTransactions = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Id == TransactionId || x.ParentTransactionId == TransactionId).ToList();
                                    foreach (var BinNumberTransaction in BinNumberTransactions)
                                    {
                                        if (CardBinNumberId != 0)
                                        {
                                            BinNumberTransaction.BinNumberId = CardBinNumberId;
                                        }
                                        if (BrandId != 0)
                                        {
                                            BinNumberTransaction.CardBrandId = BrandId;
                                        }
                                        if (CountryId != 0)
                                        {
                                            BinNumberTransaction.CardTypeId = CountryId;
                                        }
                                        if (CardTypeId != 0)
                                        {
                                            BinNumberTransaction.CardTypeId = CardTypeId;
                                        }
                                        if (BankId != 0)
                                        {
                                            BinNumberTransaction.CardBankId = BankId;
                                        }
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("PROCESS TRANSACTION - ProcessBinNumber", _Exception);
            }
        }
    }
    public class Transaction_Failed_Track_Actor : ReceiveActor
    {
        public Transaction_Failed_Track_Actor()
        {
            Receive<OTrCoreRedeemProcess>(_RequestData =>
            {

            });
        }
    }







    public class ProcessRewardActor : ReceiveActor
    {
        OCoreGatewayTransaction.Reward.TransactionItem ReferralTransaction;
        OPostTransaction _OPostTransaction;
        HCoreContext _HCoreContext;
        HCUAccountBalance _HCUAccountBalance;
        HCUAccountTransaction _HCUAccountTransaction;
        HCUAccountTransaction _HCUAccountTransactionFrom;
        OCoreGatewayTransaction.Reward.Request _CoreTransactionRewardRequest;
        OCoreGatewayTransaction.Reward.TransactionItem _CoreTransactionRewardItem;
        List<OCoreGatewayTransaction.Reward.TransactionItem> _CoreTransactionRewardItems;
        CoreOperations _CoreOperations;
        public ProcessRewardActor()
        {
            Receive<OTrCoreProcess>(_RequestData =>
            {
                string TCode = HCoreHelper.GenerateRandomNumber(4);
                DateTime TransctionDate = HCoreHelper.GetGMTDateTime();
                double ParentTransactionBalance = 0;
                bool AllowTransaction = false;

                #region Manage Exception
                try
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var ValidateTransaction = _HCoreContext.HCUAccountTransaction.Any(x => x.ReferenceNumber == _RequestData._ExternalRequest.ReferenceNumber && x.ProviderId == _RequestData._ExternalRequest.UserReference.AccountId);
                        _HCoreContext.Dispose();
                        _CoreOperations = new CoreOperations();
                        if (!ValidateTransaction)
                        {
                            OTransactionValidator TransactionValidate = _CoreOperations.ValidateTransaction(_RequestData._GatewayInfo.MerchantId, _RequestData._UserInfo.UserAccountId, _RequestData._GatewayInfo.StoreId, _RequestData._AmountDistributionReference.InvoiceAmount, _RequestData._GatewayInfo.TerminalId, _RequestData._ExternalRequest.UserReference);
                            AllowTransaction = true;
                            if (_RequestData._AmountDistributionReference.TUCRewardAmount > 0)
                            {
                                int TransactionTypeId = TransactionType.CardReward;
                                switch (_RequestData._ExternalRequest.TransactionMode)
                                {
                                    case "cash":
                                        TransactionTypeId = TransactionType.CashReward;
                                        break;
                                    case "card":
                                        TransactionTypeId = TransactionType.CardReward;
                                        break;
                                    case "redeemreward":
                                        TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                        break;
                                    default:
                                        TransactionTypeId = TransactionType.CashReward;
                                        if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin) || !string.IsNullOrEmpty(_RequestData._ExternalRequest.SixDigitPan))
                                        {
                                            TransactionTypeId = TransactionType.CardReward;
                                        }
                                        break;
                                }
                                _CoreTransactionRewardRequest = new OCoreGatewayTransaction.Reward.Request();
                                _CoreTransactionRewardRequest.MerchantId = _RequestData._GatewayInfo.MerchantId;
                                _CoreTransactionRewardRequest.CustomerId = _RequestData._UserInfo.UserAccountId;
                                _CoreTransactionRewardRequest.StoreId = _RequestData._GatewayInfo.StoreId;
                                _CoreTransactionRewardRequest.CashierId = _RequestData._GatewayInfo.CashierId;
                                _CoreTransactionRewardRequest.TerminalId = _RequestData._GatewayInfo.TerminalId;
                                _CoreTransactionRewardRequest.AcquirerId = _RequestData._GatewayInfo.AcquirerId;
                                _CoreTransactionRewardRequest.ProviderId = _RequestData._GatewayInfo.PtspId;
                                //_CoreTransactionRewardRequest.CreatedById = _RequestData._GatewayInfo.TerminalId;
                                _CoreTransactionRewardRequest.RewardAmount = _RequestData._AmountDistributionReference.User;
                                _CoreTransactionRewardRequest.CommissionAmount = _RequestData._AmountDistributionReference.TUCRewardCommissionAmount;
                                _CoreTransactionRewardRequest.TotalAmount = _RequestData._AmountDistributionReference.TUCRewardAmount;
                                _CoreTransactionRewardRequest.ReferenceAmount = _RequestData._AmountDistributionReference.TUCRewardAmount;
                                _CoreTransactionRewardRequest.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                                _CoreTransactionRewardRequest.InvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                                _CoreTransactionRewardRequest.ReferenceNumber = _RequestData._ExternalRequest.ReferenceNumber;
                                if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin))
                                {
                                    _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.bin;
                                }
                                else
                                {
                                    _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.SixDigitPan;
                                }
                                //if (_RequestData._AmountDistributionReference.IsMerchantLowBalance == true)
                                //{
                                //    _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.Pending;
                                //}
                                //else
                                //{
                                //    _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.Success;
                                //}
                                if (TransactionValidate.IsValid)
                                {
                                    _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.Success;
                                }
                                else
                                {
                                    _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.OnHold;
                                }
                                _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Merchant,

                                    Amount = _RequestData._AmountDistributionReference.TUCRewardAmount,
                                    Comission = _RequestData._AmountDistributionReference.TUCRewardCommissionAmount,
                                    TotalAmount = _RequestData._AmountDistributionReference.TUCRewardAmount,
                                    Comment = TransactionValidate.Comment,
                                    StatusId = _CoreTransactionRewardRequest.StatusId,
                                    Description = TransactionValidate.Comment
                                };
                                _CoreTransactionRewardRequest.FromAccount = _CoreTransactionRewardItem;
                                if (_RequestData._AmountDistributionReference.IsThankUCashGold)
                                {
                                    _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._UserInfo.UserAccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.TUCBlack,

                                        Amount = _RequestData._AmountDistributionReference.User,
                                        TotalAmount = _RequestData._AmountDistributionReference.User,

                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment

                                    };
                                    _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                                }
                                else
                                {
                                    if (_RequestData._AmountDistributionReference.IsThankUCashEnabled == true)
                                    {
                                        _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                        {
                                            UserAccountId = _RequestData._UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.ThankUCashPlus,
                                            Amount = _RequestData._AmountDistributionReference.User,
                                            TotalAmount = _RequestData._AmountDistributionReference.User,
                                            Comment = TransactionValidate.Comment,
                                            StatusId = _CoreTransactionRewardRequest.StatusId,
                                            Description = TransactionValidate.Comment
                                        };
                                        _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                                    }
                                    else
                                    {
                                        _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                        {
                                            UserAccountId = _RequestData._UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.TUC,

                                            Amount = _RequestData._AmountDistributionReference.User,
                                            TotalAmount = _RequestData._AmountDistributionReference.User,
                                            Comment = TransactionValidate.Comment,
                                            StatusId = _CoreTransactionRewardRequest.StatusId,
                                            Description = TransactionValidate.Comment
                                        };
                                        _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                                    }
                                }
                                _CoreTransactionRewardItems = new List<OCoreGatewayTransaction.Reward.TransactionItem>();
                                if (_RequestData._AmountDistributionReference.Ptsp > 0)
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._ExternalRequest.UserReference.AccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = _RequestData._AmountDistributionReference.Ptsp,
                                        TotalAmount = _RequestData._AmountDistributionReference.Ptsp,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                                if (_RequestData._AmountDistributionReference.Acquirer > 0)
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = (long)_RequestData._GatewayInfo.AcquirerId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = _RequestData._AmountDistributionReference.Acquirer,
                                        TotalAmount = _RequestData._AmountDistributionReference.Acquirer,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                    if (_RequestData._AmountDistributionReference.AllowAcquirerSettlement == "1")
                                    {
                                        _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                        {
                                            UserAccountId = (long)_RequestData._GatewayInfo.AcquirerId,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionTypeId,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = _RequestData._AmountDistributionReference.Acquirer,
                                            TotalAmount = _RequestData._AmountDistributionReference.Acquirer,
                                            Comment = TransactionValidate.Comment,
                                            StatusId = _CoreTransactionRewardRequest.StatusId,
                                            Description = TransactionValidate.Comment
                                        });
                                    }
                                }
                                if (_RequestData._AmountDistributionReference.Issuer > 0)
                                {
                                    if (_RequestData._UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
                                    {
                                        _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                        {
                                            UserAccountId = _RequestData._UserInfo.IssuerId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.ReferralBonus,
                                            SourceId = TransactionSource.TUC,
                                            Amount = _RequestData._AmountDistributionReference.Issuer,
                                            TotalAmount = _RequestData._AmountDistributionReference.Issuer,
                                            //StatusId = HelperStatus.Transaction.Pending,
                                            Comment = TransactionValidate.Comment,
                                            StatusId = _CoreTransactionRewardRequest.StatusId,
                                            Description = TransactionValidate.Comment
                                        });
                                    }
                                    else
                                    {
                                        _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                        {
                                            UserAccountId = _RequestData._UserInfo.IssuerId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.ReferralBonus,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = _RequestData._AmountDistributionReference.Issuer,
                                            TotalAmount = _RequestData._AmountDistributionReference.Issuer,
                                            Comment = TransactionValidate.Comment,
                                            StatusId = _CoreTransactionRewardRequest.StatusId,
                                            Description = TransactionValidate.Comment
                                        });
                                    }
                                }
                                if (_RequestData._AmountDistributionReference.TransactionIssuerTotalAmount > 0)
                                {
                                    if (_RequestData._GatewayInfo.TransactionIssuerAccountTypeId == UserAccountType.Appuser)
                                    {
                                        _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                        {
                                            UserAccountId = _RequestData._GatewayInfo.TransactionIssuerId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.TransactionBonus,
                                            SourceId = TransactionSource.TUC,

                                            Amount = _RequestData._AmountDistributionReference.TransactionIssuerAmount,
                                            Charge = _RequestData._AmountDistributionReference.TransactionIssuerCharge,
                                            TotalAmount = _RequestData._AmountDistributionReference.TransactionIssuerTotalAmount,
                                            Comment = TransactionValidate.Comment,
                                            StatusId = _CoreTransactionRewardRequest.StatusId,
                                            Description = TransactionValidate.Comment
                                        });
                                    }
                                    else
                                    {
                                        _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                        {
                                            UserAccountId = _RequestData._GatewayInfo.TransactionIssuerId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.TransactionBonus,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = _RequestData._AmountDistributionReference.TransactionIssuerAmount,
                                            Charge = _RequestData._AmountDistributionReference.TransactionIssuerCharge,
                                            TotalAmount = _RequestData._AmountDistributionReference.TransactionIssuerTotalAmount,
                                            Comment = TransactionValidate.Comment,
                                            StatusId = _CoreTransactionRewardRequest.StatusId,
                                            Description = TransactionValidate.Comment
                                        });
                                    }
                                }
                                if (_RequestData._AmountDistributionReference.Ptsa > 0)
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = SystemAccounts.SmashLabId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = _RequestData._AmountDistributionReference.Ptsa,
                                        TotalAmount = _RequestData._AmountDistributionReference.Ptsa,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                                if (_RequestData._AmountDistributionReference.ThankUCash > 0)
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = SystemAccounts.ThankUCashMerchant,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = _RequestData._AmountDistributionReference.ThankUCash,
                                        TotalAmount = _RequestData._AmountDistributionReference.ThankUCash,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment

                                    });
                                }
                                if (_RequestData._AmountDistributionReference.MerchantReverseAmount > 0)
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.MerchantCredit,
                                        SourceId = TransactionSource.Merchant,

                                        Amount = _RequestData._AmountDistributionReference.MerchantReverseAmount,
                                        Comission = 0,
                                        TotalAmount = _RequestData._AmountDistributionReference.MerchantReverseAmount,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                                _CoreTransactionRewardRequest.PartnerTransactions = _CoreTransactionRewardItems;
                                _CoreTransactionRewardRequest.UserReference = _RequestData._ExternalRequest.UserReference;
                                _RequestData._Request = _CoreTransactionRewardRequest;
                            }
                            else
                            {
                                string Comment = null;
                                if (_RequestData._UserInfo.AccountStatusId == HelperStatus.Default.Active)
                                {
                                    Comment = "No Reward";
                                }
                                else if (_RequestData._UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
                                {
                                    Comment = "User account suspended";
                                }
                                else
                                {
                                    Comment = "User account blocked";
                                }
                                int TransactionTypeId = TransactionType.CardReward;
                                switch (_RequestData._ExternalRequest.TransactionMode)
                                {
                                    case "cash":
                                        TransactionTypeId = TransactionType.CashReward;
                                        break;
                                    case "card":
                                        TransactionTypeId = TransactionType.CardReward;
                                        break;
                                    case "redeemreward":
                                        TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                        break;
                                    default:
                                        TransactionTypeId = TransactionType.CashReward;
                                        if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin) || !string.IsNullOrEmpty(_RequestData._ExternalRequest.SixDigitPan))
                                        {
                                            TransactionTypeId = TransactionType.CardReward;
                                        }
                                        break;
                                }

                                _CoreTransactionRewardRequest = new OCoreGatewayTransaction.Reward.Request();
                                _CoreTransactionRewardRequest.MerchantId = _RequestData._GatewayInfo.MerchantId;
                                _CoreTransactionRewardRequest.CustomerId = _RequestData._UserInfo.UserAccountId;
                                _CoreTransactionRewardRequest.StoreId = _RequestData._GatewayInfo.StoreId;
                                _CoreTransactionRewardRequest.CashierId = _RequestData._GatewayInfo.CashierId;
                                _CoreTransactionRewardRequest.TerminalId = _RequestData._GatewayInfo.TerminalId;
                                _CoreTransactionRewardRequest.AcquirerId = _RequestData._GatewayInfo.AcquirerId;
                                _CoreTransactionRewardRequest.ProviderId = _RequestData._GatewayInfo.PtspId;
                                //_CoreTransactionRewardRequest.CreatedById = _RequestData._GatewayInfo.TerminalId;
                                _CoreTransactionRewardRequest.RewardAmount = 0;
                                _CoreTransactionRewardRequest.CommissionAmount = 0;
                                _CoreTransactionRewardRequest.TotalAmount = 0;
                                _CoreTransactionRewardRequest.ReferenceAmount = 0;
                                _CoreTransactionRewardRequest.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                                _CoreTransactionRewardRequest.InvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                                _CoreTransactionRewardRequest.ReferenceNumber = _RequestData._ExternalRequest.ReferenceNumber;
                                if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin))
                                {
                                    _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.bin;
                                }
                                else
                                {
                                    _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.SixDigitPan;
                                }
                                //_CoreTransactionRewardRequest.InvoiceNumber = null;
                                _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Merchant,
                                    Amount = 0,
                                    Comission = 0,
                                    TotalAmount = 0,
                                };

                                _CoreTransactionRewardRequest.FromAccount = _CoreTransactionRewardItem;
                                _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Merchant,
                                    Amount = 0,
                                    Comission = 0,
                                    TotalAmount = 0,
                                };
                                if (_RequestData._AmountDistributionReference.IsThankUCashEnabled == true)
                                {
                                    _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._UserInfo.UserAccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.ThankUCashPlus,
                                        Amount = 0,
                                        TotalAmount = 0,
                                        Comment = Comment,
                                    };
                                    _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                                }
                                else
                                {
                                    _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._UserInfo.UserAccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.TUC,

                                        Amount = 0,
                                        TotalAmount = 0,
                                        Comment = Comment,
                                    };
                                    _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                                }
                                _CoreTransactionRewardRequest.UserReference = _RequestData._ExternalRequest.UserReference;
                                _RequestData._Request = _CoreTransactionRewardRequest;
                            }
                            //double ReferenceAmountPercentage = HCoreHelper.GetAmountPercentage(_RequestData._Request.InvoiceAmount, _RequestData._Request.ReferenceAmount);
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region Parent Transaction
                                _HCUAccountTransactionFrom = new HCUAccountTransaction();
                                _HCUAccountTransactionFrom.Guid = _RequestData.GroupKey;
                                _HCUAccountTransactionFrom.InoviceNumber = _RequestData._Request.InvoiceNumber;
                                _HCUAccountTransactionFrom.ParentId = _RequestData._Request.MerchantId;
                                if (_RequestData._Request.StoreId != 0)
                                {
                                    _HCUAccountTransactionFrom.SubParentId = _RequestData._Request.StoreId;
                                }
                                _HCUAccountTransactionFrom.AccountId = _RequestData._Request.FromAccount.UserAccountId;
                                _HCUAccountTransactionFrom.ModeId = _RequestData._Request.FromAccount.ModeId;
                                _HCUAccountTransactionFrom.TypeId = _RequestData._Request.FromAccount.TypeId;
                                _HCUAccountTransactionFrom.SourceId = _RequestData._Request.FromAccount.SourceId;
                                _HCUAccountTransactionFrom.Amount = _RequestData._Request.FromAccount.Amount;
                                _HCUAccountTransactionFrom.Charge = _RequestData._Request.FromAccount.Charge;
                                _HCUAccountTransactionFrom.ComissionAmount = _RequestData._Request.FromAccount.Comission;
                                _HCUAccountTransactionFrom.TotalAmount = _RequestData._Request.FromAccount.TotalAmount;
                                //_HCUAccountTransactionFrom.TotalAmountPercentage = ReferenceAmountPercentage;
                                _HCUAccountTransactionFrom.Balance = 0;
                                _HCUAccountTransactionFrom.PurchaseAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                                if (_RequestData._Request.ReferenceInvoiceAmount != 0)
                                {
                                    _HCUAccountTransactionFrom.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                                }
                                else
                                {
                                    _HCUAccountTransactionFrom.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                                }
                                _HCUAccountTransactionFrom.ReferenceAmount = _RequestData._Request.ReferenceAmount;
                                if (_RequestData._Request.AcquirerId != 0)
                                {
                                    _HCUAccountTransactionFrom.BankId = _RequestData._Request.AcquirerId;
                                }
                                if (_RequestData._Request.CustomerId != 0)
                                {
                                    _HCUAccountTransactionFrom.CustomerId = _RequestData._Request.CustomerId;
                                }
                                if (_RequestData._Request.CashierId != 0)
                                {
                                    _HCUAccountTransactionFrom.CashierId = _RequestData._Request.CashierId;
                                }
                                if (_RequestData._Request.TerminalId != 0)
                                {
                                    _HCUAccountTransactionFrom.TerminalId = _RequestData._Request.TerminalId;
                                }
                                if (_RequestData._ExternalRequest.TransactionDate != null)
                                {
                                    _HCUAccountTransactionFrom.TransactionDate = TransctionDate;
                                }
                                else
                                {
                                    _HCUAccountTransactionFrom.TransactionDate = TransctionDate;
                                }
                                if (!string.IsNullOrEmpty(_RequestData._Request.AccountNumber))
                                {
                                    _RequestData._Request.AccountNumber = _RequestData._Request.AccountNumber.Trim().Replace("*", "X").Replace("x", "X").ToUpper();
                                    _HCUAccountTransactionFrom.AccountNumber = _RequestData._Request.AccountNumber;
                                }
                                _HCUAccountTransactionFrom.ReferenceNumber = _RequestData._Request.ReferenceNumber;
                                _HCUAccountTransactionFrom.Comment = _RequestData._Request.FromAccount.Comment;
                                _HCUAccountTransactionFrom.CreateDate = TransctionDate;
                                if (_RequestData._Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccountTransactionFrom.CreatedById = _RequestData._Request.UserReference.AccountId;
                                }
                                if (_RequestData._Request.ProviderId != 0)
                                {
                                    _HCUAccountTransactionFrom.ProviderId = _RequestData._Request.ProviderId;
                                }
                                if (!string.IsNullOrEmpty(_RequestData.GroupKey))
                                {
                                    _HCUAccountTransactionFrom.GroupKey = _RequestData.GroupKey;
                                }
                                if (!string.IsNullOrEmpty(_RequestData._Request.UserReference.RequestKey))
                                {
                                    _HCUAccountTransactionFrom.RequestKey = _RequestData._Request.UserReference.RequestKey;
                                }
                                _HCUAccountTransactionFrom.TCode = TCode;
                                _HCUAccountTransactionFrom.StatusId = _RequestData._Request.StatusId;
                                #endregion
                                _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransactionFrom);
                                _HCoreContext.SaveChanges();
                                _RequestData._Request.ParentTransactionId = _HCUAccountTransactionFrom.Id;
                            }
                            if (!TransactionValidate.IsValid)
                            {
                                _CoreOperations.ValidateTransactionNotification(_RequestData._GatewayInfo.MerchantId, _RequestData._Request.ParentTransactionId, TransactionValidate, _RequestData._ExternalRequest.UserReference);
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                        }
                    }
                }
                catch (Exception _Exception)
                {
                    #region Log Bug
                    HCoreHelper.LogException("ProcessTransaction", _Exception, _RequestData._Request.UserReference);
                    #endregion
                }
                #endregion
                try
                {
                    if (AllowTransaction)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {

                            double TotalCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.AccountId == _RequestData._Request.FromAccount.UserAccountId &&
                                                             x.SourceId == _RequestData._Request.FromAccount.SourceId &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == Transaction.Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                            double TotalDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                             .Where(x => x.AccountId == _RequestData._Request.FromAccount.UserAccountId &&
                                                                 x.SourceId == _RequestData._Request.FromAccount.SourceId &&
                                                                     x.ModeId == TransactionMode.Debit &&
                                                                     x.StatusId == Transaction.Success)
                                                              .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                            ParentTransactionBalance = HCoreHelper.RoundNumber((TotalCredit - TotalDebit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                            var StoreMerchant = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _RequestData._Request.FromAccount.UserAccountId).FirstOrDefault();
                            if (StoreMerchant != null)
                            {
                                StoreMerchant.Balance = ParentTransactionBalance;
                            }
                            if (_RequestData._Request.FromAccount.SourceId == TransactionSource.Merchant)
                            {
                                var AccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _RequestData._Request.FromAccount.UserAccountId).FirstOrDefault();
                                if (AccDetails != null)
                                {
                                    AccDetails.LastTransactionDate = TransctionDate;
                                }
                            }

                            var UserBalance = _HCoreContext.HCUAccountBalance
                                .Where(x => x.AccountId == _RequestData._Request.FromAccount.UserAccountId
                                && x.ParentId == 1
                                && x.SourceId == _RequestData._Request.FromAccount.SourceId)
                                .FirstOrDefault();
                            if (UserBalance != null)
                            {
                                UserBalance.Credit = TotalCredit;
                                UserBalance.Debit = TotalDebit;
                                UserBalance.Balance = ParentTransactionBalance;
                                UserBalance.Transactions = 0;
                            }
                            else
                            {
                                _HCUAccountBalance = new HCUAccountBalance();
                                _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountBalance.AccountId = _RequestData._Request.FromAccount.UserAccountId;
                                _HCUAccountBalance.ParentId = 1;
                                _HCUAccountBalance.SourceId = (int)_RequestData._Request.FromAccount.SourceId;
                                _HCUAccountBalance.Credit = TotalCredit;
                                _HCUAccountBalance.Debit = TotalDebit;
                                _HCUAccountBalance.Balance = ParentTransactionBalance;
                                _HCUAccountBalance.Transactions = 0;
                                _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                            }
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                        }
                        bool AllowReferralNotification = false;
                        ReferralTransaction = new OCoreGatewayTransaction.Reward.TransactionItem();
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ParentTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _RequestData._Request.ParentTransactionId).FirstOrDefault();
                            ParentTransaction.Balance = ParentTransactionBalance;
                            #region To Account Transaction
                            _HCUAccountTransaction = new HCUAccountTransaction();
                            _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountTransaction.InoviceNumber = _RequestData._Request.InvoiceNumber;
                            _HCUAccountTransaction.ParentTransactionId = _RequestData._Request.ParentTransactionId;
                            _HCUAccountTransaction.ParentId = _RequestData._Request.MerchantId;
                            if (_RequestData._Request.StoreId != 0)
                            {
                                _HCUAccountTransaction.SubParentId = _RequestData._Request.StoreId;
                            }
                            _HCUAccountTransaction.AccountId = _RequestData._Request.ToAccount.UserAccountId;
                            _HCUAccountTransaction.ModeId = _RequestData._Request.ToAccount.ModeId;
                            _HCUAccountTransaction.TypeId = _RequestData._Request.ToAccount.TypeId;
                            _HCUAccountTransaction.SourceId = _RequestData._Request.ToAccount.SourceId;
                            _HCUAccountTransaction.Amount = _RequestData._Request.ToAccount.Amount;
                            _HCUAccountTransaction.Charge = _RequestData._Request.ToAccount.Charge;
                            _HCUAccountTransaction.ComissionAmount = _RequestData._Request.ToAccount.Comission;
                            _HCUAccountTransaction.TotalAmount = _RequestData._Request.ToAccount.TotalAmount;
                            //_HCUAccountTransaction.TotalAmountPercentage = ParentTransaction.TotalAmountPercentage;
                            _HCUAccountTransaction.Balance = 0;
                            _HCUAccountTransaction.PurchaseAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            if (_RequestData._Request.ReferenceInvoiceAmount != 0)
                            {
                                _HCUAccountTransaction.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                            }
                            else
                            {
                                _HCUAccountTransaction.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            }
                            _HCUAccountTransaction.ReferenceAmount = _RequestData._Request.ReferenceAmount;
                            if (_RequestData._Request.AcquirerId != 0)
                            {
                                _HCUAccountTransaction.BankId = _RequestData._Request.AcquirerId;
                            }
                            if (_RequestData._Request.CustomerId != 0)
                            {
                                _HCUAccountTransaction.CustomerId = _RequestData._Request.CustomerId;
                            }
                            if (_RequestData._Request.CashierId != 0)
                            {
                                _HCUAccountTransaction.CashierId = _RequestData._Request.CashierId;
                            }
                            if (_RequestData._Request.TerminalId != 0)
                            {
                                _HCUAccountTransaction.TerminalId = _RequestData._Request.TerminalId;
                            }
                            if (_RequestData._ExternalRequest.TransactionDate != null)
                            {
                                _HCUAccountTransaction.TransactionDate = (DateTime)_RequestData._ExternalRequest.TransactionDate;
                            }
                            else
                            {
                                _HCUAccountTransaction.TransactionDate = TransctionDate;
                            }
                            if (!string.IsNullOrEmpty(_RequestData._Request.AccountNumber))
                            {
                                _RequestData._Request.AccountNumber = _RequestData._Request.AccountNumber.Trim().Replace("*", "X").Replace("x", "X").ToUpper();
                                _HCUAccountTransaction.AccountNumber = _RequestData._Request.AccountNumber;
                            }
                            _HCUAccountTransaction.ReferenceNumber = _RequestData._Request.ReferenceNumber;
                            _HCUAccountTransaction.Comment = _RequestData._Request.ToAccount.Comment;
                            _HCUAccountTransaction.CreateDate = TransctionDate;
                            //if (_RequestData._Request.CreatedById != 0)
                            //{
                            //    _HCUAccountTransaction.CreatedById = _RequestData._Request.CreatedById;
                            //}
                            //else
                            //{
                            //    if (_RequestData._Request.UserReference.AccountId != 0)
                            //    {
                            //        _HCUAccountTransaction.CreatedById = _RequestData._Request.UserReference.AccountId;
                            //    }
                            //}
                            if (_RequestData._Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountTransaction.CreatedById = _RequestData._Request.UserReference.AccountId;
                            }
                            if (_RequestData._Request.ProviderId != 0)
                            {
                                _HCUAccountTransaction.ProviderId = _RequestData._Request.ProviderId;
                            }
                            _HCUAccountTransaction.GroupKey = ParentTransaction.GroupKey;
                            if (!string.IsNullOrEmpty(_RequestData._Request.UserReference.RequestKey))
                            {
                                _HCUAccountTransaction.RequestKey = _RequestData._Request.UserReference.RequestKey;
                            }
                            _HCUAccountTransaction.TCode = ParentTransaction.TCode;
                            _HCUAccountTransaction.StatusId = _RequestData._Request.StatusId;
                            _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                            #endregion
                            if (_RequestData._Request.PartnerTransactions != null && _RequestData._Request.PartnerTransactions.Count > 0)
                            {
                                foreach (var Transaction in _RequestData._Request.PartnerTransactions)
                                {
                                    _HCUAccountTransaction = new HCUAccountTransaction();
                                    _HCUAccountTransaction.InoviceNumber = ParentTransaction.InoviceNumber;
                                    _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountTransaction.ParentId = ParentTransaction.ParentId;
                                    if (Transaction.UserAccountId != 0)
                                    {
                                        _HCUAccountTransaction.AccountId = Transaction.UserAccountId;
                                    }
                                    #region Save Transaction
                                    _HCUAccountTransaction.ModeId = Transaction.ModeId;
                                    _HCUAccountTransaction.TypeId = Transaction.TypeId;
                                    _HCUAccountTransaction.SourceId = Transaction.SourceId;
                                    _HCUAccountTransaction.Amount = Transaction.Amount;
                                    _HCUAccountTransaction.Charge = Transaction.Charge;
                                    _HCUAccountTransaction.ComissionAmount = Transaction.Comission;
                                    _HCUAccountTransaction.TotalAmount = Transaction.TotalAmount;
                                    //_HCUAccountTransaction.TotalAmountPercentage = ParentTransaction.TotalAmountPercentage;
                                    _HCUAccountTransaction.Balance = 0;
                                    _HCUAccountTransaction.PurchaseAmount = ParentTransaction.PurchaseAmount;
                                    if (ParentTransaction.ReferenceInvoiceAmount != 0)
                                    {
                                        _HCUAccountTransaction.ReferenceInvoiceAmount = ParentTransaction.ReferenceInvoiceAmount;
                                    }
                                    else
                                    {
                                        _HCUAccountTransaction.ReferenceInvoiceAmount = ParentTransaction.ReferenceInvoiceAmount;
                                    }
                                    _HCUAccountTransaction.ReferenceAmount = ParentTransaction.ReferenceAmount;
                                    if (ParentTransaction.BankId != 0)
                                    {
                                        _HCUAccountTransaction.BankId = ParentTransaction.BankId;
                                    }
                                    if (ParentTransaction.CustomerId != 0)
                                    {
                                        _HCUAccountTransaction.CustomerId = ParentTransaction.CustomerId;
                                    }
                                    else
                                    {
                                        if (Transaction.SourceId == TransactionSource.TUC)
                                        {
                                            _HCUAccountTransaction.CustomerId = Transaction.UserAccountId;
                                        }
                                    }
                                    if (ParentTransaction.SubParentId != 0)
                                    {
                                        _HCUAccountTransaction.SubParentId = ParentTransaction.SubParentId;
                                    }
                                    //if (ParentTransaction.CardId != 0)
                                    //{
                                    //    _HCUAccountTransaction.CardId = ParentTransaction.CardId;
                                    //}
                                    if (ParentTransaction.CashierId != 0)
                                    {
                                        _HCUAccountTransaction.CashierId = ParentTransaction.CashierId;
                                    }
                                    if (ParentTransaction.TerminalId != 0)
                                    {
                                        _HCUAccountTransaction.TerminalId = ParentTransaction.TerminalId;
                                    }
                                    if (ParentTransaction.BinNumberId != 0)
                                    {
                                        _HCUAccountTransaction.BinNumberId = ParentTransaction.BinNumberId;
                                    }
                                    if (ParentTransaction.TransactionDate != null)
                                    {
                                        _HCUAccountTransaction.TransactionDate = ParentTransaction.TransactionDate;
                                    }
                                    else
                                    {
                                        _HCUAccountTransaction.TransactionDate = TransctionDate;
                                    }
                                    if (!string.IsNullOrEmpty(ParentTransaction.AccountNumber))
                                    {
                                        _HCUAccountTransaction.AccountNumber = ParentTransaction.AccountNumber;
                                    }
                                    _HCUAccountTransaction.ReferenceNumber = ParentTransaction.ReferenceNumber;
                                    _HCUAccountTransaction.Comment = Transaction.Comment;
                                    _HCUAccountTransaction.CreateDate = TransctionDate;
                                    _HCUAccountTransaction.ParentTransactionId = ParentTransaction.Id;
                                    //if (ParentTransaction.CreatedById != 0)
                                    //{
                                    //    _HCUAccountTransaction.CreatedById = ParentTransaction.CreatedById;
                                    //}
                                    //else
                                    //{
                                    //    if (_RequestData._Request.UserReference.AccountId != 0)
                                    //    {
                                    //        _HCUAccountTransaction.CreatedById = _RequestData._Request.UserReference.AccountId;
                                    //    }
                                    //}
                                    if (_RequestData._Request.UserReference.AccountId != 0)
                                    {
                                        _HCUAccountTransaction.CreatedById = _RequestData._Request.UserReference.AccountId;
                                    }
                                    if (_RequestData._Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                                    {
                                        _HCUAccountTransaction.ProviderId = _RequestData._Request.UserReference.AccountId;
                                    }
                                    _HCUAccountTransaction.GroupKey = ParentTransaction.GroupKey;

                                    if (!string.IsNullOrEmpty(_RequestData._Request.UserReference.RequestKey))
                                    {
                                        _HCUAccountTransaction.RequestKey = ParentTransaction.RequestKey;
                                    }

                                    _HCUAccountTransaction.TCode = ParentTransaction.TCode;
                                    if (Transaction.StatusId > 0)
                                    {
                                        _HCUAccountTransaction.StatusId = Transaction.StatusId;
                                    }
                                    else
                                    {
                                        _HCUAccountTransaction.StatusId = _RequestData._Request.StatusId;
                                    }
                                    _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                                    #endregion
                                    var TrAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Id == Transaction.UserAccountId).FirstOrDefault();
                                    if (TrAccountInfo != null)
                                    {
                                        TrAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (Transaction.TypeId == TransactionType.ReferralBonus && Transaction.SourceId == TransactionSource.TUC && Transaction.UserAccountTypeId == UserAccountType.Appuser)
                                    {
                                        AllowReferralNotification = true;
                                        ReferralTransaction = Transaction;
                                    }
                                }
                            }
                            _HCoreContext.SaveChanges();
                        }
                        if (AllowReferralNotification && ReferralTransaction != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                string NotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == ReferralTransaction.UserAccountId && x.StatusId == HelperStatus.Default.Active && x.NotificationUrl != null).Select(x => x.NotificationUrl).FirstOrDefault();
                                _HCoreContext.SaveChanges();
                                if (!string.IsNullOrEmpty(NotificationUrl))
                                {
                                    HCoreHelper.SendPushToDevice(NotificationUrl, "referralhistory", "You have new scratch card"
                                        ,
                                        "Open app to earn more points from scratch card",
                                        "referralhistory", 0, null, "Continue", false, null, null);
                                }
                            }
                        }
                        long CustomerId = 0;
                        int CustomerTransactionSourceId = 0;
                        if (_RequestData._Request.FromAccount.SourceId == TransactionSource.TUC || _RequestData._Request.FromAccount.SourceId == TransactionSource.TUCBlack || _RequestData._Request.FromAccount.SourceId == TransactionSource.ThankUCashPlus)
                        {
                            CustomerId = _RequestData._Request.FromAccount.UserAccountId;
                            CustomerTransactionSourceId = _RequestData._Request.FromAccount.SourceId;
                        }
                        if (_RequestData._Request.ToAccount.SourceId == TransactionSource.TUC || _RequestData._Request.FromAccount.SourceId == TransactionSource.TUCBlack || _RequestData._Request.ToAccount.SourceId == TransactionSource.ThankUCashPlus)
                        {
                            CustomerId = _RequestData._Request.ToAccount.UserAccountId;
                            CustomerTransactionSourceId = _RequestData._Request.ToAccount.SourceId;
                        }
                        if (CustomerId > 0)
                        {
                            if (_RequestData._Request.TotalAmount > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var TerminalDetails = _HCoreContext.HCUAccount.
                                    Where(x => x.Id == _RequestData._Request.TerminalId).FirstOrDefault();
                                    if (TerminalDetails != null)
                                    {
                                        TerminalDetails.ApplicationStatusId = 2;
                                        TerminalDetails.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        TerminalDetails.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    var FromAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Id == _RequestData._Request.FromAccount.UserAccountId).FirstOrDefault();
                                    if (FromAccountInfo != null)
                                    {
                                        FromAccountInfo.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        FromAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    var ToAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Id == _RequestData._Request.ToAccount.UserAccountId).FirstOrDefault();
                                    if (ToAccountInfo != null)
                                    {
                                        ToAccountInfo.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        ToAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }


                                    DateTime CurrentDate = HCoreHelper.GetGMTDate();
                                    double TotalCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                  .Where(x => x.AccountId == CustomerId &&
                                                                         x.SourceId == CustomerTransactionSourceId &&
                                                                         x.ModeId == TransactionMode.Credit &&
                                                                         x.StatusId == Transaction.Success)
                                                                  .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                    double TotalDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                     .Where(x => x.AccountId == CustomerId &&
                                                                              x.SourceId == CustomerTransactionSourceId &&
                                                                             x.ModeId == TransactionMode.Debit &&
                                                                             x.StatusId == Transaction.Success)
                                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                    long Transactions = _HCoreContext.HCUAccountTransaction
                                                                    .Where(x => x.AccountId == CustomerId &&
                                                                             x.SourceId == CustomerTransactionSourceId &&
                                                                            (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit) &&
                                                                            x.StatusId == Transaction.Success).Count();

                                    double Balance = HCoreHelper.RoundNumber((TotalCredit - TotalDebit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                                    var CustomerTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == CustomerId && x.GroupKey == _RequestData.GroupKey).FirstOrDefault();
                                    if (CustomerTransaction != null)
                                    {
                                        CustomerTransaction.Balance = Balance;
                                    }
                                    var AccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == CustomerId).FirstOrDefault();
                                    if (AccDetails != null)
                                    {
                                    }
                                    var UserBalance = _HCoreContext.HCUAccountBalance
                                        .Where(x => x.AccountId == CustomerId
                                        && x.ParentId == 1
                                        && x.SourceId == CustomerTransactionSourceId)
                                        .FirstOrDefault();
                                    if (UserBalance != null)
                                    {
                                        UserBalance.Credit = TotalCredit;
                                        UserBalance.Debit = TotalDebit;
                                        UserBalance.Balance = Balance;
                                        UserBalance.Transactions = Transactions;
                                    }
                                    else
                                    {
                                        if (CustomerTransactionSourceId != TransactionSource.GiftCards)
                                        {
                                            _HCUAccountBalance = new HCUAccountBalance();
                                            _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountBalance.AccountId = CustomerId;
                                            _HCUAccountBalance.ParentId = 1;
                                            _HCUAccountBalance.SourceId = CustomerTransactionSourceId;
                                            _HCUAccountBalance.Credit = TotalCredit;
                                            _HCUAccountBalance.Debit = TotalDebit;
                                            _HCUAccountBalance.Balance = Balance;
                                            _HCUAccountBalance.Transactions = Transactions;
                                            _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                            _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                                        }
                                    }
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                }
                            }
                        }
                        CoreGatewayTransaction _CoreGatewayTransaction = new CoreGatewayTransaction();
                        _CoreGatewayTransaction.ProcessBinNumber(_RequestData._Request.ParentTransactionId);
                        _OPostTransaction = new OPostTransaction();
                        _OPostTransaction._Request = _RequestData._ExternalRequest;
                        _OPostTransaction._GatewayInfo = _RequestData._GatewayInfo;
                        _OPostTransaction._UserInfo = _RequestData._UserInfo;
                        _OPostTransaction._AmountDistributionReference = _RequestData._AmountDistributionReference;
                        _OPostTransaction.GroupKey = _RequestData.GroupKey;
                        _OPostTransaction.TCode = TCode;
                        var _TransactionPostProcessActor = ActorSystem.Create("ActorRewardPostTransaction");
                        var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<ActorRewardPostTransaction>("ActorRewardPostTransaction");
                        _TransactionPostProcessActorNotify.Tell(_OPostTransaction);
                    }
                }
                catch (Exception _Exception)
                {
                    #region Log Bug
                    HCoreHelper.LogException("ProcessTransaction2", _Exception, _RequestData._Request.UserReference);
                    #endregion
                }
            });
        }
    }



    public class PostProcessRewardTransactionActor : ReceiveActor
    {
        OCoreGatewayTransaction.Reward.TransactionItem ReferralTransaction;
        OPostTransaction _OPostTransaction;
        HCoreContext _HCoreContext;
        HCUAccountBalance _HCUAccountBalance;
        HCUAccountTransaction _HCUAccountTransaction;
        HCUAccountTransaction _HCUAccountTransactionFrom;
        OCoreGatewayTransaction.Reward.Request _CoreTransactionRewardRequest;
        OCoreGatewayTransaction.Reward.TransactionItem _CoreTransactionRewardItem;
        List<OCoreGatewayTransaction.Reward.TransactionItem> _CoreTransactionRewardItems;
        CoreOperations _CoreOperations;
        public PostProcessRewardTransactionActor()
        {
            Receive<OTrCoreProcess>(_RequestData =>
            {
                string TCode = HCoreHelper.GenerateRandomNumber(4);
                DateTime TransctionDate = HCoreHelper.GetGMTDateTime();
                double ParentTransactionBalance = 0;
                bool AllowTransaction = false;

                #region Manage Exception
                try
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        //bool ValidateTransactionReference = _HCoreContext.HCUAccountTransaction.Any(x => x.ReferenceNumber == _RequestData._ExternalRequest.ReferenceNumber && x.ProviderId == _RequestData._ExternalRequest.UserReference.AccountId);
                        //_HCoreContext.Dispose();
                        _CoreOperations = new CoreOperations();
                        //if (!ValidateTransactionReference)
                        //{
                        OTransactionValidator TransactionValidate = _CoreOperations.ValidateTransaction(_RequestData._GatewayInfo.MerchantId, _RequestData._UserInfo.UserAccountId, _RequestData._GatewayInfo.StoreId, _RequestData._AmountDistributionReference.InvoiceAmount, _RequestData._GatewayInfo.TerminalId, _RequestData._ExternalRequest.UserReference);
                        AllowTransaction = true;
                        // Bank Loyalty Program - Start
                        var _BankLoyaltyProcessActor = ActorSystem.Create("ActorCoreBankLoyaltyProgram");
                        var _BankLoyaltyProcessActorNotify = _BankLoyaltyProcessActor.ActorOf<ActorCoreBankLoyaltyProgram>("ActorCoreBankLoyaltyProgram");
                        _BankLoyaltyProcessActorNotify.Tell(_RequestData);
                        // Bank Loyalty Program - End
                        #region Transactions Package
                        if (_RequestData._AmountDistributionReference.TUCRewardAmount > 0)
                        {
                            int TransactionTypeId = TransactionType.CardReward;
                            switch (_RequestData._ExternalRequest.TransactionMode)
                            {
                                case "cash":
                                    TransactionTypeId = TransactionType.CashReward;
                                    break;
                                case "card":
                                    TransactionTypeId = TransactionType.CardReward;
                                    break;
                                case "redeemreward":
                                    TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                    break;
                                default:
                                    TransactionTypeId = TransactionType.CashReward;
                                    if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin) || !string.IsNullOrEmpty(_RequestData._ExternalRequest.SixDigitPan))
                                    {
                                        TransactionTypeId = TransactionType.CardReward;
                                    }
                                    break;
                            }
                            _CoreTransactionRewardRequest = new OCoreGatewayTransaction.Reward.Request();
                            _CoreTransactionRewardRequest.MerchantId = _RequestData._GatewayInfo.MerchantId;
                            _CoreTransactionRewardRequest.CustomerId = _RequestData._UserInfo.UserAccountId;
                            _CoreTransactionRewardRequest.StoreId = _RequestData._GatewayInfo.StoreId;
                            _CoreTransactionRewardRequest.CashierId = _RequestData._GatewayInfo.CashierId;
                            _CoreTransactionRewardRequest.TerminalId = _RequestData._GatewayInfo.TerminalId;
                            _CoreTransactionRewardRequest.AcquirerId = _RequestData._GatewayInfo.AcquirerId;
                            _CoreTransactionRewardRequest.ProviderId = _RequestData._GatewayInfo.PtspId;
                            _CoreTransactionRewardRequest.RewardAmount = _RequestData._AmountDistributionReference.TUCRewardUserAmount;
                            _CoreTransactionRewardRequest.CommissionAmount = _RequestData._AmountDistributionReference.TUCRewardCommissionAmount;
                            _CoreTransactionRewardRequest.TotalAmount = _RequestData._AmountDistributionReference.TUCRewardAmount;
                            _CoreTransactionRewardRequest.ReferenceAmount = _RequestData._AmountDistributionReference.TUCRewardAmount;
                            _CoreTransactionRewardRequest.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                            _CoreTransactionRewardRequest.InvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            _CoreTransactionRewardRequest.ReferenceNumber = _RequestData._ExternalRequest.ReferenceNumber;
                            if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin))
                            {
                                _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.bin;
                            }
                            else
                            {
                                _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.SixDigitPan;
                            }
                            if (TransactionValidate.IsValid)
                            {
                                if (_RequestData._AmountDistributionReference.TransactionStatus > 0)
                                {
                                    _CoreTransactionRewardRequest.StatusId = _RequestData._AmountDistributionReference.TransactionStatus;
                                }
                                else
                                {
                                    _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.Success;
                                }
                            }
                            else
                            {
                                _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.OnHold;
                            }
                            #region From Account
                            _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                            {
                                UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionTypeId,
                                SourceId = TransactionSource.Merchant,

                                Amount = _RequestData._AmountDistributionReference.TUCRewardAmount,
                                Comission = _RequestData._AmountDistributionReference.TUCRewardCommissionAmount,
                                TotalAmount = _RequestData._AmountDistributionReference.TUCRewardAmount,
                                Comment = TransactionValidate.Comment,
                                StatusId = _CoreTransactionRewardRequest.StatusId,
                                Description = TransactionValidate.Comment
                            };
                            _CoreTransactionRewardRequest.FromAccount = _CoreTransactionRewardItem;
                            #endregion
                            #region To Account
                            if (_RequestData._AmountDistributionReference.IsThankUCashGold)
                            {
                                _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._UserInfo.UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.TUCBlack,

                                    Amount = _RequestData._AmountDistributionReference.User,
                                    TotalAmount = _RequestData._AmountDistributionReference.User,

                                    Comment = TransactionValidate.Comment,
                                    StatusId = _CoreTransactionRewardRequest.StatusId,
                                    Description = TransactionValidate.Comment
                                };
                                _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                            }
                            else
                            {
                                if (_RequestData._AmountDistributionReference.IsThankUCashEnabled == true)
                                {
                                    _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._UserInfo.UserAccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.ThankUCashPlus,
                                        Amount = _RequestData._AmountDistributionReference.User,
                                        TotalAmount = _RequestData._AmountDistributionReference.User,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    };
                                    _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                                }
                                else
                                {
                                    _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._UserInfo.UserAccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.TUC,

                                        Amount = _RequestData._AmountDistributionReference.User,
                                        TotalAmount = _RequestData._AmountDistributionReference.User,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    };
                                    _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                                }
                            }
                            #endregion

                            _CoreTransactionRewardItems = new List<OCoreGatewayTransaction.Reward.TransactionItem>();
                            if (_RequestData._AmountDistributionReference.Ptsp > 0)
                            {
                                _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._ExternalRequest.UserReference.AccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Settlement,

                                    Amount = _RequestData._AmountDistributionReference.Ptsp,
                                    TotalAmount = _RequestData._AmountDistributionReference.Ptsp,
                                    Comment = TransactionValidate.Comment,
                                    StatusId = _CoreTransactionRewardRequest.StatusId,
                                    Description = TransactionValidate.Comment
                                });
                            }
                            if (_RequestData._AmountDistributionReference.Acquirer > 0)
                            {
                                _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = (long)_RequestData._GatewayInfo.AcquirerId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Settlement,

                                    Amount = _RequestData._AmountDistributionReference.Acquirer,
                                    TotalAmount = _RequestData._AmountDistributionReference.Acquirer,
                                    Comment = TransactionValidate.Comment,
                                    StatusId = _CoreTransactionRewardRequest.StatusId,
                                    Description = TransactionValidate.Comment
                                });
                                if (_RequestData._AmountDistributionReference.AllowAcquirerSettlement == "1")
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = (long)_RequestData._GatewayInfo.AcquirerId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionTypeId,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = _RequestData._AmountDistributionReference.Acquirer,
                                        TotalAmount = _RequestData._AmountDistributionReference.Acquirer,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                            }
                            if (_RequestData._AmountDistributionReference.Issuer > 0)
                            {
                                if (_RequestData._UserInfo.IssuerAccountTypeId == UserAccountType.Appuser)
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._UserInfo.IssuerId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.ReferralBonus,
                                        SourceId = TransactionSource.TUC,
                                        Amount = _RequestData._AmountDistributionReference.Issuer,
                                        TotalAmount = _RequestData._AmountDistributionReference.Issuer,
                                        //StatusId = HelperStatus.Transaction.Pending,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                                else
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._UserInfo.IssuerId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.ReferralBonus,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = _RequestData._AmountDistributionReference.Issuer,
                                        TotalAmount = _RequestData._AmountDistributionReference.Issuer,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                            }
                            if (_RequestData._AmountDistributionReference.TransactionIssuerTotalAmount > 0)
                            {
                                if (_RequestData._GatewayInfo.TransactionIssuerAccountTypeId == UserAccountType.Appuser)
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._GatewayInfo.TransactionIssuerId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.TransactionBonus,
                                        SourceId = TransactionSource.TUC,

                                        Amount = _RequestData._AmountDistributionReference.TransactionIssuerAmount,
                                        Charge = _RequestData._AmountDistributionReference.TransactionIssuerCharge,
                                        TotalAmount = _RequestData._AmountDistributionReference.TransactionIssuerTotalAmount,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                                else
                                {
                                    _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                    {
                                        UserAccountId = _RequestData._GatewayInfo.TransactionIssuerId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.TransactionBonus,
                                        SourceId = TransactionSource.Settlement,

                                        Amount = _RequestData._AmountDistributionReference.TransactionIssuerAmount,
                                        Charge = _RequestData._AmountDistributionReference.TransactionIssuerCharge,
                                        TotalAmount = _RequestData._AmountDistributionReference.TransactionIssuerTotalAmount,
                                        Comment = TransactionValidate.Comment,
                                        StatusId = _CoreTransactionRewardRequest.StatusId,
                                        Description = TransactionValidate.Comment
                                    });
                                }
                            }
                            if (_RequestData._AmountDistributionReference.Ptsa > 0)
                            {
                                _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.SmashLabId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Settlement,

                                    Amount = _RequestData._AmountDistributionReference.Ptsa,
                                    TotalAmount = _RequestData._AmountDistributionReference.Ptsa,
                                    Comment = TransactionValidate.Comment,
                                    StatusId = _CoreTransactionRewardRequest.StatusId,
                                    Description = TransactionValidate.Comment
                                });
                            }
                            if (_RequestData._AmountDistributionReference.ThankUCash > 0)
                            {
                                _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashMerchant,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.Settlement,

                                    Amount = _RequestData._AmountDistributionReference.ThankUCash,
                                    TotalAmount = _RequestData._AmountDistributionReference.ThankUCash,
                                    Comment = TransactionValidate.Comment,
                                    StatusId = _CoreTransactionRewardRequest.StatusId,
                                    Description = TransactionValidate.Comment

                                });
                            }
                            if (_RequestData._AmountDistributionReference.MerchantReverseAmount > 0)
                            {
                                _CoreTransactionRewardItems.Add(new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.MerchantCredit,
                                    SourceId = TransactionSource.Merchant,

                                    Amount = _RequestData._AmountDistributionReference.MerchantReverseAmount,
                                    Comission = 0,
                                    TotalAmount = _RequestData._AmountDistributionReference.MerchantReverseAmount,
                                    Comment = TransactionValidate.Comment,
                                    StatusId = _CoreTransactionRewardRequest.StatusId,
                                    Description = TransactionValidate.Comment
                                });
                            }
                            _CoreTransactionRewardRequest.PartnerTransactions = _CoreTransactionRewardItems;
                            _CoreTransactionRewardRequest.UserReference = _RequestData._ExternalRequest.UserReference;
                            _RequestData._Request = _CoreTransactionRewardRequest;
                        }
                        else
                        {
                            string Comment = null;
                            if (_RequestData._UserInfo.AccountStatusId == HelperStatus.Default.Active)
                            {
                                Comment = "No Reward";
                            }
                            else if (_RequestData._UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
                            {
                                Comment = "User account suspended";
                            }
                            else
                            {
                                Comment = "User account blocked";
                            }
                            int TransactionTypeId = TransactionType.CardReward;
                            switch (_RequestData._ExternalRequest.TransactionMode)
                            {
                                case "cash":
                                    TransactionTypeId = TransactionType.CashReward;
                                    break;
                                case "card":
                                    TransactionTypeId = TransactionType.CardReward;
                                    break;
                                case "redeemreward":
                                    TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                    break;
                                default:
                                    TransactionTypeId = TransactionType.CashReward;
                                    if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin) || !string.IsNullOrEmpty(_RequestData._ExternalRequest.SixDigitPan))
                                    {
                                        TransactionTypeId = TransactionType.CardReward;
                                    }
                                    break;
                            }

                            _CoreTransactionRewardRequest = new OCoreGatewayTransaction.Reward.Request();
                            _CoreTransactionRewardRequest.MerchantId = _RequestData._GatewayInfo.MerchantId;
                            _CoreTransactionRewardRequest.CustomerId = _RequestData._UserInfo.UserAccountId;
                            _CoreTransactionRewardRequest.StoreId = _RequestData._GatewayInfo.StoreId;
                            _CoreTransactionRewardRequest.CashierId = _RequestData._GatewayInfo.CashierId;
                            _CoreTransactionRewardRequest.TerminalId = _RequestData._GatewayInfo.TerminalId;
                            _CoreTransactionRewardRequest.AcquirerId = _RequestData._GatewayInfo.AcquirerId;
                            _CoreTransactionRewardRequest.ProviderId = _RequestData._GatewayInfo.PtspId;
                            //_CoreTransactionRewardRequest.CreatedById = _RequestData._GatewayInfo.TerminalId;
                            _CoreTransactionRewardRequest.RewardAmount = 0;
                            _CoreTransactionRewardRequest.CommissionAmount = 0;
                            _CoreTransactionRewardRequest.TotalAmount = 0;
                            _CoreTransactionRewardRequest.ReferenceAmount = 0;
                            _CoreTransactionRewardRequest.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                            _CoreTransactionRewardRequest.InvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            _CoreTransactionRewardRequest.ReferenceNumber = _RequestData._ExternalRequest.ReferenceNumber;
                            if (!string.IsNullOrEmpty(_RequestData._ExternalRequest.bin))
                            {
                                _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.bin;
                            }
                            else
                            {
                                _CoreTransactionRewardRequest.AccountNumber = _RequestData._ExternalRequest.SixDigitPan;
                            }
                            //_CoreTransactionRewardRequest.InvoiceNumber = null;
                            _CoreTransactionRewardRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                            {
                                UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionTypeId,
                                SourceId = TransactionSource.Merchant,
                                Amount = 0,
                                Comission = 0,
                                TotalAmount = 0,
                            };

                            _CoreTransactionRewardRequest.FromAccount = _CoreTransactionRewardItem;
                            _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                            {
                                UserAccountId = _RequestData._GatewayInfo.MerchantId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionTypeId,
                                SourceId = TransactionSource.Merchant,
                                Amount = 0,
                                Comission = 0,
                                TotalAmount = 0,
                            };
                            if (_RequestData._AmountDistributionReference.IsThankUCashEnabled == true)
                            {
                                _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._UserInfo.UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.ThankUCashPlus,
                                    Amount = 0,
                                    TotalAmount = 0,
                                    Comment = Comment,
                                };
                                _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                            }
                            else
                            {
                                _CoreTransactionRewardItem = new OCoreGatewayTransaction.Reward.TransactionItem
                                {
                                    UserAccountId = _RequestData._UserInfo.UserAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSource.TUC,

                                    Amount = 0,
                                    TotalAmount = 0,
                                    Comment = Comment,
                                };
                                _CoreTransactionRewardRequest.ToAccount = _CoreTransactionRewardItem;
                            }
                            _CoreTransactionRewardRequest.UserReference = _RequestData._ExternalRequest.UserReference;
                            _RequestData._Request = _CoreTransactionRewardRequest;
                        }
                        #endregion
                        //double ReferenceAmountPercentage = HCoreHelper.GetAmountPercentage(_RequestData._Request.InvoiceAmount, _RequestData._Request.ReferenceAmount);
                        using (_HCoreContext = new HCoreContext())
                        {
                            #region Parent Transaction
                            _HCUAccountTransactionFrom = new HCUAccountTransaction();
                            _HCUAccountTransactionFrom.Guid = _RequestData.GroupKey;
                            _HCUAccountTransactionFrom.InoviceNumber = _RequestData._Request.InvoiceNumber;
                            _HCUAccountTransactionFrom.ParentId = _RequestData._Request.MerchantId;
                            if (_RequestData._Request.StoreId != 0)
                            {
                                _HCUAccountTransactionFrom.SubParentId = _RequestData._Request.StoreId;
                            }
                            _HCUAccountTransactionFrom.AccountId = _RequestData._Request.FromAccount.UserAccountId;
                            _HCUAccountTransactionFrom.ModeId = _RequestData._Request.FromAccount.ModeId;
                            _HCUAccountTransactionFrom.TypeId = _RequestData._Request.FromAccount.TypeId;
                            _HCUAccountTransactionFrom.SourceId = _RequestData._Request.FromAccount.SourceId;
                            _HCUAccountTransactionFrom.Amount = _RequestData._Request.FromAccount.Amount;
                            _HCUAccountTransactionFrom.Charge = _RequestData._Request.FromAccount.Charge;
                            _HCUAccountTransactionFrom.ComissionAmount = _RequestData._Request.FromAccount.Comission;
                            _HCUAccountTransactionFrom.TotalAmount = _RequestData._Request.FromAccount.TotalAmount;
                            //_HCUAccountTransactionFrom.TotalAmountPercentage = ReferenceAmountPercentage;
                            _HCUAccountTransactionFrom.Balance = 0;
                            _HCUAccountTransactionFrom.PurchaseAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            if (_RequestData._Request.ReferenceInvoiceAmount != 0)
                            {
                                _HCUAccountTransactionFrom.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                            }
                            else
                            {
                                _HCUAccountTransactionFrom.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            }
                            _HCUAccountTransactionFrom.ReferenceAmount = _RequestData._Request.ReferenceAmount;
                            if (_RequestData._Request.AcquirerId != 0)
                            {
                                _HCUAccountTransactionFrom.BankId = _RequestData._Request.AcquirerId;
                            }
                            if (_RequestData._Request.CustomerId != 0)
                            {
                                _HCUAccountTransactionFrom.CustomerId = _RequestData._Request.CustomerId;
                            }
                            if (_RequestData._Request.CashierId != 0)
                            {
                                _HCUAccountTransactionFrom.CashierId = _RequestData._Request.CashierId;
                            }
                            if (_RequestData._Request.TerminalId != 0)
                            {
                                _HCUAccountTransactionFrom.TerminalId = _RequestData._Request.TerminalId;
                            }
                            if (_RequestData._ExternalRequest.TransactionDate != null)
                            {
                                _HCUAccountTransactionFrom.TransactionDate = TransctionDate;
                            }
                            else
                            {
                                _HCUAccountTransactionFrom.TransactionDate = TransctionDate;
                            }
                            if (!string.IsNullOrEmpty(_RequestData._Request.AccountNumber))
                            {
                                _RequestData._Request.AccountNumber = _RequestData._Request.AccountNumber.Trim().Replace("*", "X").Replace("x", "X").ToUpper();
                                _HCUAccountTransactionFrom.AccountNumber = _RequestData._Request.AccountNumber;
                            }
                            _HCUAccountTransactionFrom.ReferenceNumber = _RequestData._Request.ReferenceNumber;
                            _HCUAccountTransactionFrom.Comment = _RequestData._Request.FromAccount.Comment;
                            _HCUAccountTransactionFrom.CreateDate = TransctionDate;
                            if (_RequestData._Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountTransactionFrom.CreatedById = _RequestData._Request.UserReference.AccountId;
                            }
                            if (_RequestData._Request.ProviderId != 0)
                            {
                                _HCUAccountTransactionFrom.ProviderId = _RequestData._Request.ProviderId;
                            }
                            if (!string.IsNullOrEmpty(_RequestData.GroupKey))
                            {
                                _HCUAccountTransactionFrom.GroupKey = _RequestData.GroupKey;
                            }
                            if (!string.IsNullOrEmpty(_RequestData._Request.UserReference.RequestKey))
                            {
                                _HCUAccountTransactionFrom.RequestKey = _RequestData._Request.UserReference.RequestKey;
                            }
                            _HCUAccountTransactionFrom.TCode = TCode;
                            _HCUAccountTransactionFrom.StatusId = _RequestData._Request.StatusId;
                            #endregion
                            _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransactionFrom);
                            _HCoreContext.SaveChanges();
                            _RequestData._Request.ParentTransactionId = _HCUAccountTransactionFrom.Id;
                        }
                        if (!TransactionValidate.IsValid)
                        {
                            _CoreOperations.ValidateTransactionNotification(_RequestData._GatewayInfo.MerchantId, _RequestData._Request.ParentTransactionId, TransactionValidate, _RequestData._ExternalRequest.UserReference);
                        }
                        //}
                        //else
                        //{
                        //    _HCoreContext.Dispose();
                        //}
                    }
                }
                catch (Exception _Exception)
                {
                    #region Log Bug
                    HCoreHelper.LogException("ProcessTransaction", _Exception, _RequestData._Request.UserReference);
                    #endregion
                }
                #endregion
                try
                {
                    if (AllowTransaction)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {

                            double TotalCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                      .Where(x => x.AccountId == _RequestData._Request.FromAccount.UserAccountId &&
                                                             x.SourceId == _RequestData._Request.FromAccount.SourceId &&
                                                             x.ModeId == TransactionMode.Credit &&
                                                             x.StatusId == Transaction.Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                            double TotalDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                             .Where(x => x.AccountId == _RequestData._Request.FromAccount.UserAccountId &&
                                                                 x.SourceId == _RequestData._Request.FromAccount.SourceId &&
                                                                     x.ModeId == TransactionMode.Debit &&
                                                                     x.StatusId == Transaction.Success)
                                                              .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                            ParentTransactionBalance = HCoreHelper.RoundNumber((TotalCredit - TotalDebit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);

                            var StoreMerchant = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _RequestData._Request.FromAccount.UserAccountId).FirstOrDefault();
                            if (StoreMerchant != null)
                            {
                                StoreMerchant.Balance = ParentTransactionBalance;
                            }
                            if (_RequestData._Request.FromAccount.SourceId == TransactionSource.Merchant)
                            {
                                var AccDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _RequestData._Request.FromAccount.UserAccountId).FirstOrDefault();
                                if (AccDetails != null)
                                {
                                    AccDetails.LastTransactionDate = TransctionDate;
                                }
                            }

                            var UserBalance = _HCoreContext.HCUAccountBalance
                                .Where(x => x.AccountId == _RequestData._Request.FromAccount.UserAccountId
                                && x.ParentId == 1
                                && x.SourceId == _RequestData._Request.FromAccount.SourceId)
                                .FirstOrDefault();
                            if (UserBalance != null)
                            {
                                UserBalance.Credit = TotalCredit;
                                UserBalance.Debit = TotalDebit;
                                UserBalance.Balance = ParentTransactionBalance;
                                UserBalance.Transactions = 0;
                            }
                            else
                            {
                                _HCUAccountBalance = new HCUAccountBalance();
                                _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountBalance.AccountId = _RequestData._Request.FromAccount.UserAccountId;
                                _HCUAccountBalance.ParentId = 1;
                                _HCUAccountBalance.SourceId = (int)_RequestData._Request.FromAccount.SourceId;
                                _HCUAccountBalance.Credit = TotalCredit;
                                _HCUAccountBalance.Debit = TotalDebit;
                                _HCUAccountBalance.Balance = ParentTransactionBalance;
                                _HCUAccountBalance.Transactions = 0;
                                _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                            }
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                        }
                        bool AllowReferralNotification = false;
                        ReferralTransaction = new OCoreGatewayTransaction.Reward.TransactionItem();
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ParentTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _RequestData._Request.ParentTransactionId).FirstOrDefault();
                            ParentTransaction.Balance = ParentTransactionBalance;
                            #region To Account Transaction
                            _HCUAccountTransaction = new HCUAccountTransaction();
                            _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountTransaction.InoviceNumber = _RequestData._Request.InvoiceNumber;
                            _HCUAccountTransaction.ParentTransactionId = _RequestData._Request.ParentTransactionId;
                            _HCUAccountTransaction.ParentId = _RequestData._Request.MerchantId;
                            if (_RequestData._Request.StoreId != 0)
                            {
                                _HCUAccountTransaction.SubParentId = _RequestData._Request.StoreId;
                            }
                            _HCUAccountTransaction.AccountId = _RequestData._Request.ToAccount.UserAccountId;
                            _HCUAccountTransaction.ModeId = _RequestData._Request.ToAccount.ModeId;
                            _HCUAccountTransaction.TypeId = _RequestData._Request.ToAccount.TypeId;
                            _HCUAccountTransaction.SourceId = _RequestData._Request.ToAccount.SourceId;
                            _HCUAccountTransaction.Amount = _RequestData._Request.ToAccount.Amount;
                            _HCUAccountTransaction.Charge = _RequestData._Request.ToAccount.Charge;
                            _HCUAccountTransaction.ComissionAmount = _RequestData._Request.ToAccount.Comission;
                            _HCUAccountTransaction.TotalAmount = _RequestData._Request.ToAccount.TotalAmount;
                            //_HCUAccountTransaction.TotalAmountPercentage = ParentTransaction.TotalAmountPercentage;
                            _HCUAccountTransaction.Balance = 0;
                            _HCUAccountTransaction.PurchaseAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            if (_RequestData._Request.ReferenceInvoiceAmount != 0)
                            {
                                _HCUAccountTransaction.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.ReferenceInvoiceAmount;
                            }
                            else
                            {
                                _HCUAccountTransaction.ReferenceInvoiceAmount = _RequestData._AmountDistributionReference.InvoiceAmount;
                            }
                            _HCUAccountTransaction.ReferenceAmount = _RequestData._Request.ReferenceAmount;
                            if (_RequestData._Request.AcquirerId != 0)
                            {
                                _HCUAccountTransaction.BankId = _RequestData._Request.AcquirerId;
                            }
                            if (_RequestData._Request.CustomerId != 0)
                            {
                                _HCUAccountTransaction.CustomerId = _RequestData._Request.CustomerId;
                            }
                            if (_RequestData._Request.CashierId != 0)
                            {
                                _HCUAccountTransaction.CashierId = _RequestData._Request.CashierId;
                            }
                            if (_RequestData._Request.TerminalId != 0)
                            {
                                _HCUAccountTransaction.TerminalId = _RequestData._Request.TerminalId;
                            }
                            if (_RequestData._ExternalRequest.TransactionDate != null)
                            {
                                _HCUAccountTransaction.TransactionDate = (DateTime)_RequestData._ExternalRequest.TransactionDate;
                            }
                            else
                            {
                                _HCUAccountTransaction.TransactionDate = TransctionDate;
                            }
                            if (!string.IsNullOrEmpty(_RequestData._Request.AccountNumber))
                            {
                                _RequestData._Request.AccountNumber = _RequestData._Request.AccountNumber.Trim().Replace("*", "X").Replace("x", "X").ToUpper();
                                _HCUAccountTransaction.AccountNumber = _RequestData._Request.AccountNumber;
                            }
                            _HCUAccountTransaction.ReferenceNumber = _RequestData._Request.ReferenceNumber;
                            _HCUAccountTransaction.Comment = _RequestData._Request.ToAccount.Comment;
                            _HCUAccountTransaction.CreateDate = TransctionDate;
                            if (_RequestData._Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountTransaction.CreatedById = _RequestData._Request.UserReference.AccountId;
                            }
                            if (_RequestData._Request.ProviderId != 0)
                            {
                                _HCUAccountTransaction.ProviderId = _RequestData._Request.ProviderId;
                            }
                            _HCUAccountTransaction.GroupKey = ParentTransaction.GroupKey;
                            if (!string.IsNullOrEmpty(_RequestData._Request.UserReference.RequestKey))
                            {
                                _HCUAccountTransaction.RequestKey = _RequestData._Request.UserReference.RequestKey;
                            }
                            _HCUAccountTransaction.TCode = ParentTransaction.TCode;
                            _HCUAccountTransaction.StatusId = _RequestData._Request.StatusId;
                            _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                            #endregion
                            if (_RequestData._Request.PartnerTransactions != null && _RequestData._Request.PartnerTransactions.Count > 0)
                            {
                                foreach (var Transaction in _RequestData._Request.PartnerTransactions)
                                {
                                    _HCUAccountTransaction = new HCUAccountTransaction();
                                    _HCUAccountTransaction.InoviceNumber = ParentTransaction.InoviceNumber;
                                    _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountTransaction.ParentId = ParentTransaction.ParentId;
                                    if (Transaction.UserAccountId != 0)
                                    {
                                        _HCUAccountTransaction.AccountId = Transaction.UserAccountId;
                                    }
                                    #region Save Transaction
                                    _HCUAccountTransaction.ModeId = Transaction.ModeId;
                                    _HCUAccountTransaction.TypeId = Transaction.TypeId;
                                    _HCUAccountTransaction.SourceId = Transaction.SourceId;
                                    _HCUAccountTransaction.Amount = Transaction.Amount;
                                    _HCUAccountTransaction.Charge = Transaction.Charge;
                                    _HCUAccountTransaction.ComissionAmount = Transaction.Comission;
                                    _HCUAccountTransaction.TotalAmount = Transaction.TotalAmount;
                                    //_HCUAccountTransaction.TotalAmountPercentage = ParentTransaction.TotalAmountPercentage;
                                    _HCUAccountTransaction.Balance = 0;
                                    _HCUAccountTransaction.PurchaseAmount = ParentTransaction.PurchaseAmount;
                                    if (ParentTransaction.ReferenceInvoiceAmount != 0)
                                    {
                                        _HCUAccountTransaction.ReferenceInvoiceAmount = ParentTransaction.ReferenceInvoiceAmount;
                                    }
                                    else
                                    {
                                        _HCUAccountTransaction.ReferenceInvoiceAmount = ParentTransaction.ReferenceInvoiceAmount;
                                    }
                                    _HCUAccountTransaction.ReferenceAmount = ParentTransaction.ReferenceAmount;
                                    if (ParentTransaction.BankId != 0)
                                    {
                                        _HCUAccountTransaction.BankId = ParentTransaction.BankId;
                                    }
                                    if (ParentTransaction.CustomerId != 0)
                                    {
                                        _HCUAccountTransaction.CustomerId = ParentTransaction.CustomerId;
                                    }
                                    else
                                    {
                                        if (Transaction.SourceId == TransactionSource.TUC)
                                        {
                                            _HCUAccountTransaction.CustomerId = Transaction.UserAccountId;
                                        }
                                    }
                                    if (ParentTransaction.SubParentId != 0)
                                    {
                                        _HCUAccountTransaction.SubParentId = ParentTransaction.SubParentId;
                                    }
                                    //if (ParentTransaction.CardId != 0)
                                    //{
                                    //    _HCUAccountTransaction.CardId = ParentTransaction.CardId;
                                    //}
                                    if (ParentTransaction.CashierId != 0)
                                    {
                                        _HCUAccountTransaction.CashierId = ParentTransaction.CashierId;
                                    }
                                    if (ParentTransaction.TerminalId != 0)
                                    {
                                        _HCUAccountTransaction.TerminalId = ParentTransaction.TerminalId;
                                    }
                                    if (ParentTransaction.BinNumberId != 0)
                                    {
                                        _HCUAccountTransaction.BinNumberId = ParentTransaction.BinNumberId;
                                    }
                                    if (ParentTransaction.TransactionDate != null)
                                    {
                                        _HCUAccountTransaction.TransactionDate = ParentTransaction.TransactionDate;
                                    }
                                    else
                                    {
                                        _HCUAccountTransaction.TransactionDate = TransctionDate;
                                    }
                                    if (!string.IsNullOrEmpty(ParentTransaction.AccountNumber))
                                    {
                                        _HCUAccountTransaction.AccountNumber = ParentTransaction.AccountNumber;
                                    }
                                    _HCUAccountTransaction.ReferenceNumber = ParentTransaction.ReferenceNumber;
                                    _HCUAccountTransaction.Comment = Transaction.Comment;
                                    _HCUAccountTransaction.CreateDate = TransctionDate;
                                    _HCUAccountTransaction.ParentTransactionId = ParentTransaction.Id;
                                    //if (ParentTransaction.CreatedById != 0)
                                    //{
                                    //    _HCUAccountTransaction.CreatedById = ParentTransaction.CreatedById;
                                    //}
                                    //else
                                    //{
                                    //    if (_RequestData._Request.UserReference.AccountId != 0)
                                    //    {
                                    //        _HCUAccountTransaction.CreatedById = _RequestData._Request.UserReference.AccountId;
                                    //    }
                                    //}
                                    if (_RequestData._Request.UserReference.AccountId != 0)
                                    {
                                        _HCUAccountTransaction.CreatedById = _RequestData._Request.UserReference.AccountId;
                                    }
                                    if (_RequestData._Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                                    {
                                        _HCUAccountTransaction.ProviderId = _RequestData._Request.UserReference.AccountId;
                                    }
                                    _HCUAccountTransaction.GroupKey = ParentTransaction.GroupKey;

                                    if (!string.IsNullOrEmpty(_RequestData._Request.UserReference.RequestKey))
                                    {
                                        _HCUAccountTransaction.RequestKey = ParentTransaction.RequestKey;
                                    }

                                    _HCUAccountTransaction.TCode = ParentTransaction.TCode;
                                    if (Transaction.StatusId > 0)
                                    {
                                        _HCUAccountTransaction.StatusId = Transaction.StatusId;
                                    }
                                    else
                                    {
                                        _HCUAccountTransaction.StatusId = _RequestData._Request.StatusId;
                                    }
                                    _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);
                                    #endregion
                                    var TrAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Id == Transaction.UserAccountId).FirstOrDefault();
                                    if (TrAccountInfo != null)
                                    {
                                        TrAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (Transaction.TypeId == TransactionType.ReferralBonus && Transaction.SourceId == TransactionSource.TUC && Transaction.UserAccountTypeId == UserAccountType.Appuser)
                                    {
                                        AllowReferralNotification = true;
                                        ReferralTransaction = Transaction;
                                    }
                                }
                            }
                            _HCoreContext.SaveChanges();
                        }
                        if (AllowReferralNotification && ReferralTransaction != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                string NotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == ReferralTransaction.UserAccountId && x.StatusId == HelperStatus.Default.Active && x.NotificationUrl != null).Select(x => x.NotificationUrl).FirstOrDefault();
                                _HCoreContext.SaveChanges();
                                if (!string.IsNullOrEmpty(NotificationUrl))
                                {
                                    HCoreHelper.SendPushToDevice(NotificationUrl, "referralhistory", "You have new scratch card"
                                        ,
                                        "Open app to earn more points from scratch card",
                                        "referralhistory", 0, null, "Continue", false, null, null);
                                }
                            }
                        }
                        long CustomerId = 0;
                        int CustomerTransactionSourceId = 0;
                        if (_RequestData._Request.FromAccount.SourceId == TransactionSource.TUC || _RequestData._Request.FromAccount.SourceId == TransactionSource.TUCBlack || _RequestData._Request.FromAccount.SourceId == TransactionSource.ThankUCashPlus)
                        {
                            CustomerId = _RequestData._Request.FromAccount.UserAccountId;
                            CustomerTransactionSourceId = _RequestData._Request.FromAccount.SourceId;
                        }
                        if (_RequestData._Request.ToAccount.SourceId == TransactionSource.TUC || _RequestData._Request.ToAccount.SourceId == TransactionSource.TUCBlack || _RequestData._Request.ToAccount.SourceId == TransactionSource.ThankUCashPlus)
                        {
                            CustomerId = _RequestData._Request.ToAccount.UserAccountId;
                            CustomerTransactionSourceId = _RequestData._Request.ToAccount.SourceId;
                        }
                        if (CustomerId > 0)
                        {
                            if (_RequestData._Request.TotalAmount > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var TerminalDetails = _HCoreContext.HCUAccount.
                                    Where(x => x.Id == _RequestData._Request.TerminalId).FirstOrDefault();
                                    if (TerminalDetails != null)
                                    {
                                        TerminalDetails.ApplicationStatusId = 2;
                                        TerminalDetails.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        TerminalDetails.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    var FromAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Id == _RequestData._Request.FromAccount.UserAccountId).FirstOrDefault();
                                    if (FromAccountInfo != null)
                                    {
                                        FromAccountInfo.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        FromAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    var ToAccountInfo = _HCoreContext.HCUAccount.Where(x => x.Id == _RequestData._Request.ToAccount.UserAccountId).FirstOrDefault();
                                    if (ToAccountInfo != null)
                                    {
                                        ToAccountInfo.LastActivityDate = HCoreHelper.GetGMTDateTime();
                                        ToAccountInfo.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }

                                    // Parent Transaction Balance
                                    if (CustomerTransactionSourceId == TransactionSource.ThankUCashPlus || CustomerTransactionSourceId == TransactionSource.TUCBlack)
                                    {
                                        double SourceTotalCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                 .Where(x => x.AccountId == CustomerId
                                                                       && x.SourceId == CustomerTransactionSourceId
                                                                       && x.ParentId == _RequestData._GatewayInfo.MerchantId
                                                                       && x.ModeId == TransactionMode.Credit
                                                                       && x.StatusId == Transaction.Success)
                                                                 .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                        double SourceTotalDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                         .Where(x => x.AccountId == CustomerId
                                                                                 && x.SourceId == CustomerTransactionSourceId
                                                                                 && x.ParentId == _RequestData._GatewayInfo.MerchantId
                                                                                 && x.ModeId == TransactionMode.Debit
                                                                                 && x.StatusId == Transaction.Success)
                                                                          .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                        long SourceTransactions = _HCoreContext.HCUAccountTransaction
                                                                        .Count(x => x.AccountId == CustomerId
                                                                                 && x.StatusId == Transaction.Success
                                                                                 && x.SourceId == CustomerTransactionSourceId
                                                                                 && x.ParentId == _RequestData._GatewayInfo.MerchantId
                                                                                 && (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit)
                                                                             );
                                        double SourceBalance = HCoreHelper.RoundNumber((SourceTotalCredit - SourceTotalDebit), _AppConfig.SystemEntryRoundDouble);
                                        var SourceCustomerTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == CustomerId && x.GroupKey == _RequestData.GroupKey).FirstOrDefault();
                                        if (SourceCustomerTransaction != null)
                                        {
                                            SourceCustomerTransaction.Balance = SourceBalance;
                                        }
                                        var SourceUserBalance = _HCoreContext.HCUAccountBalance
                                           .Where(x => x.AccountId == CustomerId
                                           && x.StatusId == Transaction.Success
                                           && x.ParentId == _RequestData._GatewayInfo.MerchantId
                                           && x.SourceId == CustomerTransactionSourceId)
                                           .FirstOrDefault();
                                        if (SourceUserBalance != null)
                                        {
                                            SourceUserBalance.Credit = SourceTotalCredit;
                                            SourceUserBalance.Debit = SourceTotalDebit;
                                            SourceUserBalance.Balance = SourceBalance;
                                            SourceUserBalance.Transactions = SourceTransactions;
                                        }
                                        else
                                        {
                                            _HCUAccountBalance = new HCUAccountBalance();
                                            _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountBalance.AccountId = CustomerId;
                                            _HCUAccountBalance.ParentId = _RequestData._GatewayInfo.MerchantId;
                                            _HCUAccountBalance.SourceId = CustomerTransactionSourceId;
                                            _HCUAccountBalance.Credit = SourceTotalCredit;
                                            _HCUAccountBalance.Debit = SourceTotalDebit;
                                            _HCUAccountBalance.Balance = SourceBalance;
                                            _HCUAccountBalance.Transactions = SourceTransactions;
                                            _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                            _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                                        }
                                    }
                                    double TotalCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                 .Where(x => x.AccountId == CustomerId &&
                                                                        x.SourceId == CustomerTransactionSourceId &&
                                                                        x.ModeId == TransactionMode.Credit &&
                                                                        x.StatusId == Transaction.Success)
                                                                 .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                    double TotalDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                     .Where(x => x.AccountId == CustomerId &&
                                                                              x.SourceId == CustomerTransactionSourceId &&
                                                                             x.ModeId == TransactionMode.Debit &&
                                                                             x.StatusId == Transaction.Success)
                                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                    long Transactions = _HCoreContext.HCUAccountTransaction
                                                                    .Where(x => x.AccountId == CustomerId &&
                                                                             x.SourceId == CustomerTransactionSourceId &&
                                                                            (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit) &&
                                                                            x.StatusId == Transaction.Success).Count();
                                    double Balance = HCoreHelper.RoundNumber((TotalCredit - TotalDebit), _AppConfig.SystemEntryRoundDouble);
                                    if (CustomerTransactionSourceId != TransactionSource.ThankUCashPlus && CustomerTransactionSourceId != TransactionSource.TUCBlack)
                                    {
                                        var CustomerTransaction = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == CustomerId && x.GroupKey == _RequestData.GroupKey).FirstOrDefault();
                                        if (CustomerTransaction != null)
                                        {
                                            CustomerTransaction.Balance = Balance;
                                        }
                                    }
                                    var UserBalance = _HCoreContext.HCUAccountBalance
                                                     .Where(x => x.AccountId == CustomerId
                                                     && x.ParentId == 1
                                                     && x.SourceId == CustomerTransactionSourceId)
                                                     .FirstOrDefault();
                                    if (UserBalance != null)
                                    {
                                        UserBalance.Credit = TotalCredit;
                                        UserBalance.Debit = TotalDebit;
                                        UserBalance.Balance = Balance;
                                        UserBalance.Transactions = Transactions;
                                        UserBalance.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    else
                                    {
                                        if (CustomerTransactionSourceId != TransactionSource.GiftCards)
                                        {
                                            _HCUAccountBalance = new HCUAccountBalance();
                                            _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountBalance.AccountId = CustomerId;
                                            _HCUAccountBalance.ParentId = 1;
                                            _HCUAccountBalance.SourceId = CustomerTransactionSourceId;
                                            _HCUAccountBalance.Credit = TotalCredit;
                                            _HCUAccountBalance.Debit = TotalDebit;
                                            _HCUAccountBalance.Balance = Balance;
                                            _HCUAccountBalance.Transactions = Transactions;
                                            _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                            _HCUAccountBalance.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                                        }
                                    }
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                }
                            }
                        }

                        CoreGatewayTransaction _CoreGatewayTransaction = new CoreGatewayTransaction();
                        _CoreGatewayTransaction.ProcessBinNumber(_RequestData._Request.ParentTransactionId);
                        _OPostTransaction = new OPostTransaction();
                        _OPostTransaction._Request = _RequestData._ExternalRequest;
                        _OPostTransaction._GatewayInfo = _RequestData._GatewayInfo;
                        _OPostTransaction._UserInfo = _RequestData._UserInfo;
                        _OPostTransaction._AmountDistributionReference = _RequestData._AmountDistributionReference;
                        _OPostTransaction.GroupKey = _RequestData.GroupKey;
                        _OPostTransaction.TCode = TCode;
                        var _TransactionPostProcessActor = ActorSystem.Create("ActorRewardPostTransaction");
                        var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<ActorRewardPostTransaction>("ActorRewardPostTransaction");
                        _TransactionPostProcessActorNotify.Tell(_OPostTransaction);


                    }

                }
                catch (Exception _Exception)
                {
                    #region Log Bug
                    HCoreHelper.LogException("ProcessTransaction2", _Exception, _RequestData._Request.UserReference);
                    #endregion
                }
            });
        }
    }
}
