//==================================================================================
// FileName: OCoreOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.ThankUCash.Gateway.Object;

namespace HCore.ThankUCash.Gateway.Core
{
    public class OPostTransaction
    {
        internal OThankUGateway.Request _Request { get; set; }
        internal OGatewayInfo _GatewayInfo { get; set; }
        internal OUserInfo _UserInfo { get; set; }
        internal OAmountDistribution _AmountDistributionReference { get; set; }
        internal string? GroupKey { get; set; }
        internal string? TCode { get; set; }
        //internal OCoreGatewayTransaction.Reward.Response _TransactionReference { get; set; }
    }
    internal class OCustomer
    {
        public class Request
        {
            public string? MobileNumber { get; set; }
            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? GenderCode { get; set; }
            //public long GenderId { get; set; }

            public string? Address { get; set; }
            //public double AddressLatitude { get; set; }
            //public double AddressLongitude { get; set; }

            public long OwnerId { get; set; }
            public long SubOwnerId { get; set; }
            public long CreatedById { get; set; }

            public string? CardNumber { get; set; }
            public string? CardSerialNumber { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public HCoreConstant.ResponseStatus Status { get; set; } // users
            public string? StatusResponseCode { get; set; } // used
            public string? StatusMessage { get; set; } // used

            public long AccountId { get; set; } // used
            public string? AccountKey { get; set; } // used
            public string? AccountNumber { get; set; }

            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? Gender { get; set; }
            public string? Pin { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; } // used
            public long? OwnerId { get; set; } // used
            public long? OwnerAccountTypeId { get; set; }
            public long? SubOwnerId { get; set; } // used
            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public long? CreatedByAccountTypeId { get; set; }
            public int StatusId { get; set; }
        }
    }
    internal class OConfigurationInfo
    {
        public string? Value { get; set; }
        public string? TypeCode { get; set; }
        public string? TypeName { get; set; }
    }
    internal class TCustAccount
    {
        public string? UserName { get; set; }
        public long AccountId { get; set; }
        public string? AccountKey { get; set; }
        public string? DisplayName { get; set; }
        public string? Name { get; set; }
        //public string? Gender { get; set; }
        public string? EmailAddress { get; set; }
        public string? MobileNumber { get; set; }
        public string? AccountNumber { get; set; }
        public string? Pin { get; set; }
        public long? GenderId { get; set; }
        public long? OwnerId { get; set; }
        public long? OwnerAccountTypeId { get; set; }
        public long? SubOwnerId { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? CreatedById { get; set; }
        public long? CreatedByAccountTypeId { get; set; }
        public int StatusId { get; set; }
    }
    public class DealCode
    {
        public string? DealKey { get; set; }
        public string? DealPin { get; set; }
        public long? MerchantId { get; set; }
        public double? Amount { get; set; }
        public long? DealStatusId { get; set; }
        public long? AccountId { get; set; }

        public string? AccountDisplayName { get; set; }
        public string? AccountEmailAddress { get; set; }
        public long? AccountStatusId { get; set; }
        public string? AccountMobileNumber { get; set; }
        public string? AccountNumber { get; set; }
        public string? AccountPin { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public class LoanCode
    {
        public string? LoanKey { get; set; }
        public string? LoanPin { get; set; }
        public long? MerchantId { get; set; }
        public double? Amount { get; set; }
        public long? LoanStatusId { get; set; }
        public long? AccountId { get; set; }

        public string? AccountDisplayName { get; set; }
        public string? AccountEmailAddress { get; set; }
        public long? AccountStatusId { get; set; }
        public string? AccountMobileNumber { get; set; }
        public string? AccountNumber { get; set; }
        public string? AccountPin { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public sbyte IsRedeemed { get; set; }
    }
    internal class OPendingRewardActorItem
    {
        internal OAmountDistribution AmountDistribution { get; set; }
        internal OGatewayInfo GatewayInfo { get; set; }
        internal OUserInfo UserInfo { get; set; }
        internal OThankUGateway.Request ExternalRequest { get; set; }
    }
    public class OCoreOperations
    {

    }
}
