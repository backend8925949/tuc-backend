﻿using System;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Core;
using Irony.Parsing;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;
using static HCore.ThankUCash.Gateway.Helper.HelperGateway;

namespace HCore.ThankUCash.Gateway.Core
{
    internal class CoreRedeem
    {
        CoreOperations _CoreOperations;
        HCoreContext _HCoreContext;
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        internal void Transaction_Redeem_ConfirmNotification(OCoreRedeem.Redeem_Confirm_Notification _RedeemPackage)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            try
            {

                if (_RedeemPackage.ProductId != 0 && _RedeemPackage.ProductCodeId != 0)
                {
                    long UseLocationId = 0;
                    long ProductOwnerId = 0;
                    using (_HCoreContext = new HCoreContext())
                    {
                        var ProductLocationDetails = _HCoreContext.CAProductLocation.Where(x => x.ProductId == _RedeemPackage.ProductId && x.AccountId == _RedeemPackage.GatewayInfo.MerchantId && x.SubAccountId == _RedeemPackage.GatewayInfo.StoreId).Select(x => x.Id).FirstOrDefault();
                        if (ProductLocationDetails != 0)
                        {
                            UseLocationId = ProductLocationDetails;
                        }
                        else
                        {
                            CAProductLocation _CAProductLocation;
                            _CAProductLocation = new CAProductLocation();
                            _CAProductLocation.Guid = HCoreHelper.GenerateGuid();
                            _CAProductLocation.ProductId = _RedeemPackage.ProductId;
                            _CAProductLocation.AccountId = _RedeemPackage.GatewayInfo.MerchantId;
                            _CAProductLocation.SubAccountId = _RedeemPackage.GatewayInfo.StoreId;
                            _CAProductLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                            _CAProductLocation.CreatedById = _RedeemPackage.UserReference.AccountId;
                            _CAProductLocation.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.CAProductLocation.Add(_CAProductLocation);
                            _HCoreContext.SaveChanges();
                            UseLocationId = _CAProductLocation.Id;
                        }
                        _HCoreContext.Dispose();
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var ProductDetails = _HCoreContext.CAProduct.Where(x => x.Id == _RedeemPackage.ProductId).FirstOrDefault();
                        if (ProductDetails != null)
                        {
                            ProductOwnerId = ProductDetails.AccountId;
                            var ProductCodeDetails = _HCoreContext.CAProductCode.Where(x => x.Id == _RedeemPackage.ProductCodeId).FirstOrDefault();
                            if (ProductCodeDetails != null)
                            {
                                ProductCodeDetails.LastUseLocationId = UseLocationId;
                                ProductCodeDetails.LastUseDate = HCoreHelper.GetGMTDateTime();
                                ProductCodeDetails.UseCount += 1;
                                //ProductCodeDetails.TransactionId = TransactionDetails.Id;
                                ProductCodeDetails.AvailableAmount -= _RedeemPackage.RedeemAmount;
                                if (ProductCodeDetails.AvailableAmount >= ProductCodeDetails.AvailableAmount)
                                {
                                    ProductCodeDetails.StatusId = HelperStatus.ProdutCode.Used;
                                }
                                ProductDetails.TotalUsed += 1;
                                ProductDetails.TotalUsedAmount += _RedeemPackage.RedeemAmount;
                                ProductDetails.LastUseLocationId = UseLocationId;
                                ProductDetails.LastUseDate = HCoreHelper.GetGMTDateTime();

                                CAProductUseHistory _CAProductUseHistory;
                                _CAProductUseHistory = new CAProductUseHistory();
                                _CAProductUseHistory.Guid = HCoreHelper.GenerateGuid();
                                _CAProductUseHistory.ProductId = _RedeemPackage.ProductId;
                                _CAProductUseHistory.AccountId = _RedeemPackage.UserInfo.UserAccountId;
                                _CAProductUseHistory.ProductCodeId = ProductCodeDetails.Id;
                                _CAProductUseHistory.Amount = _RedeemPackage.RedeemAmount;
                                _CAProductUseHistory.LocationId = UseLocationId;
                                _CAProductUseHistory.CreateDate = HCoreHelper.GetGMTDateTime();
                                _CAProductUseHistory.CreatedById = _RedeemPackage.UserReference.AccountId;
                                _CAProductUseHistory.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.CAProductUseHistory.Add(_CAProductUseHistory);
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    if (ProductOwnerId != 0)
                    {
                        _ManageCoreTransaction.UpdateAccountBalance(_RedeemPackage.UserInfo.UserAccountId, TransactionSource.GiftCards, ProductOwnerId);
                    }
                }

                _ManageCoreTransaction.UpdateAccountBalance(_RedeemPackage.UserInfo.UserAccountId, TransactionSource.TUC);
                if (_RedeemPackage.TransactionSourceId == TransactionSource.GiftPoints)
                {
                    _ManageCoreTransaction.UpdateAccountBalance(_RedeemPackage.UserInfo.UserAccountId, TransactionSource.GiftPoints, (long)_RedeemPackage.GatewayInfo.MerchantId);
                }
                double Balance = 0;
                if (_RedeemPackage.ProgramId > 0)
                {
                    var ProgramDetails = _ManageCoreTransaction.GetLoyaltyProgramBalance((long)_RedeemPackage.UserInfo.UserAccountId, _RedeemPackage.GatewayInfo.MerchantId, (long)_RedeemPackage.GatewayInfo.AcquirerId);
                    if (ProgramDetails != null)
                    {
                        Balance = ProgramDetails.Balance;
                    }
                }
                else
                {
                    Balance = _ManageCoreTransaction.GetAppUserBalance((long)_RedeemPackage.UserInfo.UserAccountId);
                }
                if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test)
                {
                    string RedeemSms = HCoreHelper.GetConfiguration("redeemsms", _RedeemPackage.GatewayInfo.MerchantId);
                    if (!string.IsNullOrEmpty(RedeemSms))
                    {
                        #region Send SMS
                        string Message = RedeemSms
                        .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_RedeemPackage.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString())
                        .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                        .Replace("[MERCHANT]", _RedeemPackage.GatewayInfo.MerchantDisplayName);
                        HCoreHelper.SendSMS(SmsType.Transaction, "234", _RedeemPackage.UserInfo.MobileNumber, Message, _RedeemPackage.UserInfo.UserAccountId, _RedeemPackage.TransactionReference);
                        #endregion
                    }
                    else
                    {
                        #region Send SMS
                        string Message = "Redeem Alert: You just redeemed N" + HCoreHelper.RoundNumber(_RedeemPackage.RedeemAmount, _AppConfig.SystemExitRoundDouble).ToString() + " Cash at " + _RedeemPackage.GatewayInfo.MerchantDisplayName + ". Your TUC Bal:N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ".  Download TUC App: https://bit.ly/tuc-app. Thank U Very Much";
                        HCoreHelper.SendSMS(SmsType.Transaction, "234", _RedeemPackage.UserInfo.MobileNumber, Message, _RedeemPackage.UserInfo.UserAccountId, _RedeemPackage.TransactionReference);
                        #endregion
                    }
                    #region Send Email 
                    if (!string.IsNullOrEmpty(_RedeemPackage.UserInfo.EmailAddress))
                    {
                        var _EmailParameters = new
                        {
                            UserDisplayName = _RedeemPackage.UserInfo.DisplayName,
                            MerchantName = _RedeemPackage.GatewayInfo.MerchantDisplayName,
                            InvoiceAmount = _RedeemPackage.InvoiceAmount.ToString(),
                            Amount = _RedeemPackage.RedeemAmount.ToString(),
                            Balance = Balance.ToString(),
                        };
                        HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RedeemEmail, _RedeemPackage.UserInfo.DisplayName, _RedeemPackage.UserInfo.EmailAddress, _EmailParameters, _RedeemPackage.UserReference);
                    }
                    #endregion
                }
                //if (TransactionDetails.SourceId != TransactionSource.TUCBlack)
                //{
                //    OThankUGateway.Request _RedeemRewardRequest = new OThankUGateway.Request();
                //    if (!string.IsNullOrEmpty(_Request.TransactionMode))
                //    {
                //        _RedeemRewardRequest.TransactionMode = _Request.TransactionMode;
                //    }
                //    else
                //    {
                //        _RedeemRewardRequest.TransactionMode = "redeemreward";
                //    }
                //    _RedeemRewardRequest.MerchantId = _RedeemPackage.GatewayInfo.MerchantId;
                //    _RedeemRewardRequest.TerminalId = _RedeemPackage.GatewayInfo.TerminalId;
                //    //_RedeemRewardRequest.CardNumber = _Request.CardNumber;
                //    //_RedeemRewardRequest.TagNumber = _Request.TagNumber;
                //    //_RedeemRewardRequest.SixDigitPan = _Request.SixDigitPan;
                //    if (!string.IsNullOrEmpty(_Request.ReferenceNumber))
                //    {
                //        _RedeemRewardRequest.ReferenceNumber = _Request.ReferenceNumber;
                //    }
                //    else
                //    {
                //        _RedeemRewardRequest.ReferenceNumber = TransactionDetails.Guid;
                //    }
                //    _RedeemRewardRequest.MobileNumber = TransactionDetails.MobileNumber;
                //    _RedeemRewardRequest.InvoiceAmount = (long)(Math.Round(InvoiceAmount, 2) * 100);
                //    _RedeemRewardRequest.TransactionDate = _Request.TransactionDate;
                //    _RedeemRewardRequest.bin = _Request.bin;
                //    _RedeemRewardRequest.UserReference = _RedeemPackage.UserReference;
                //    _FrameworkThankUCashGateway = new FrameworkThankUCashGateway();
                //    _FrameworkThankUCashGateway.RewardRedeemTransaction(_RedeemRewardRequest, _RedeemPackage.GatewayInfo, _Request.TransactionReference, RedeemAmount, TransactionDetails.PurchaseAmount, true);
                //}

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "Transaction_Redeem_ConfirmNotification", _Exception, _RedeemPackage.UserReference);
            }

        }
        internal void Transaction_DealRedeem_Confirm_Notification(OCoreRedeem.Redeem_DealConfirm_Notification _Request)
        {
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Id == _Request.DealCodeId).FirstOrDefault();
                    var DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == DealCodeDetails.DealId).FirstOrDefault();

                    DealCodeDetails.UseAttempts = 1;
                    DealCodeDetails.UseCount = 1;
                    DealCodeDetails.LastUseSource = DealRedeemSource.Pos;
                    DealCodeDetails.LastUseDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.GatewayInfo.StoreId != 0)
                    {
                        DealCodeDetails.LastUseLocationId = _Request.GatewayInfo.StoreId;
                    }
                    if (_Request.GatewayInfo.TerminalId > 0)
                    {
                        DealCodeDetails.TerminalId = _Request.GatewayInfo.TerminalId;
                    }
                    if (_Request.GatewayInfo.CashierId > 0)
                    {
                        DealCodeDetails.CashierId = _Request.GatewayInfo.CashierId;
                    }
                    DealCodeDetails.StatusId = DealCodes.Used;
                    _HCoreContext.SaveChanges();
                    using (_HCoreContext = new HCoreContext())
                    {
                        string MerchantNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == DealDetails.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                        string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == DealCodeDetails.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                        if (!string.IsNullOrEmpty(UserNotificationUrl))
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Redeemed", DealDetails.Title + " deal redeemed", "dealpurchasedetails", DealDetails.Id, DealDetails.Guid, "View details", true, null);
                        }
                        if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                        {
                            HCoreHelper.SendPushToDeviceMerchant(UserNotificationUrl, "dealpurchasedetails", "Deal Redeemed", DealDetails.Title + " deal redeemed by customer", "dealpurchasedetails", DealDetails.Id, DealDetails.Guid, "View details");
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogData(HCoreConstant.LogType.Alert, "Transaction_DealRedeem_Confirm_Notification_Failure", "PTSP DEAL REDEEM FAILED", JsonConvert.SerializeObject(_Request));
                HCoreHelper.LogException(LogLevel.High, "Transaction_DealRedeem_Confirm_Notification", _Exception, _Request.UserReference);
            }
        }
    }
}

public class Transaction_Redeem_InitializeActor : ReceiveActor
{
    public Transaction_Redeem_InitializeActor()
    {
        Receive<OTrCoreRedeemProcess>(_RequestData =>
        {

        });
    }
}
public class Transaction_Redeem_ConfirmActor : ReceiveActor
{
    public Transaction_Redeem_ConfirmActor()
    {
        Receive<OTrCoreRedeemProcess>(_RequestData =>
        {

        });
    }
}
public class Transaction_Redeem_ConfirmNotification_Actor : ReceiveActor
{
    CoreRedeem _CoreRedeem;
    public Transaction_Redeem_ConfirmNotification_Actor()
    {
        Receive<OCoreRedeem.Redeem_Confirm_Notification>(_RequestData =>
        {
            _CoreRedeem = new CoreRedeem();
            _CoreRedeem.Transaction_Redeem_ConfirmNotification(_RequestData);
        });
    }
}

public class Transaction_DealRedeem_Confirm_Notification_Actor : ReceiveActor
{
    CoreRedeem _CoreRedeem;
    public Transaction_DealRedeem_Confirm_Notification_Actor()
    {
        Receive<OCoreRedeem.Redeem_DealConfirm_Notification>(_RequestData =>
        {
            _CoreRedeem = new CoreRedeem();
            _CoreRedeem.Transaction_DealRedeem_Confirm_Notification(_RequestData);
        });
    }
}