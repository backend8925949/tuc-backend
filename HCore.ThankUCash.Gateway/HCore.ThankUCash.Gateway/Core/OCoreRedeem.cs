﻿using System;
using HCore.Helper;
using HCore.ThankUCash.Gateway.Object;

namespace HCore.ThankUCash.Gateway.Core
{
    internal class OCoreRedeem
    {
        //internal class RedeemPackage
        //{
        //    internal OGatewayInfo GatewayInfo { get; set; }
        //    internal OUserReference UserReference { get; set; }
        //}
        internal class Redeem_Confirm_Notification
        {

            public long ProductId { get; set; }
            public long ProductCodeId { get; set; }
            internal string TransactionReference { get; set; }
            internal long ProgramId { get; set; }
            internal long TransactionSourceId { get; set; }
            internal double RedeemAmount { get; set; }
            internal double InvoiceAmount { get; set; }
            internal OUserInfo UserInfo { get; set; }
            internal OGatewayInfo GatewayInfo { get; set; }
            internal OUserReference UserReference { get; set; }
        }

        internal class Redeem_DealConfirm_Notification
        {
            //internal long DealId { get; set; }
            //internal string DealTitle { get; set; }

            internal long DealCodeId { get; set; }
            internal OGatewayInfo GatewayInfo { get; set; }
            internal OUserReference UserReference { get; set; }
        }
    }
}

