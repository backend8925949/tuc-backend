//==================================================================================
// FileName: CoreResources.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace HCore.ThankUCash.Gateway.Core
{
    public static class CoreResources
    {
        public static string HCG500 = "HCG500";
        public static string HCG500M = "Unable to process your request. Please try after some time";
        public static string HCG100 = "Invalid account type";
        public static string HCG100M = "Merchant or Terminal id required";
        public static string HCG101 = "Merchant already connected";
        public static string HCG102 = "Merchant connected";
        public static string HCG103 = "Invalid merchant id";
        public static string HCG104 = "Invalid account";
        //public static string HCG105 = "Error occured while processing your request";
        public static string HCG106 = "Merchant removed";
        public static string HCG107 = "Merchant removed";
        public static string HCG108 = "Configuration loaded";
        public static string HCG109 = "Mobile number or code required";
        public static string HCG109M = "Mobile number required";
        public static string HCG110 = "Pin missing";
        public static string HCG111 = "Account details not found";
        public static string HCG112 = "Account pin is not set. Download Thank U Cash app to set pin";
        public static string HCG113 = "Balance loaded";
        public static string HCG114 = "Invalid pin";
        public static string HCG115 = "HCG115";
        public static string HCG115M = "Transaction reference required";
        public static string HCG116 = "HCG116";
        public static string HCG116M = "Invoice amount must be greater than 0";
        public static string HCG117 = "Redeem amount must be less than or equal to invoice amount";
        public static string HCG118 = "Reward percentage value not set";
        public static string HCG119 = "Transaction successful";
        public static string HCG120 = "Account balance is low";
        public static string HCG121 = "Redeem initialised. Confirm transaction pending";
        public static string HCG122 = "HCG122";
        public static string HCG122M = "Transaction reference required";
        public static string HCG123 = "'Amount must be greater than 0";
        public static string HCG124 = "Transaction already processed";
        public static string HCG125 = "HCG125";
        public static string HCG125M = "Transaction successful";
        public static string HCG126 = "Transaction already processed";
        public static string HCG127 = "Invoice amount must be greater than 0";
        public static string HCG128 = "Redeem amount must be greater than 0";
        public static string HCG129 = "Account suspended or blocked please contact support";
        public static string HCG130 = "Change amount must be greater than 0 ";
        public static string HCG131 = "Account suspended contact support";
        public static string HCG132 = "Account blocked contact support";
        public static string HCG133 = "Account suspended or blocked contact support";
        public static string HCG134 = "Invalid cashier id";
        public static string HCG135 = "Cashier account not active";
        public static string HCG136 = "Redeem has been disabled for merchant";
        public static string HCG137 = "Under maintainance";
        public static string HCG138 = "Feature not enabled";
        public static string HCG139 = "Balance low, reinitailize transaction";
        public static string HCG140 = "Balance low, reinitailize transaction";
        public static string HCG141 = "Invalid code, reinitailize transaction";
        public static string HCG142 = "Invalid code, reinitailize transaction";
        public static string HCG143 = "Balance low, reinitailize transaction";
        public static string HCG144 = "Invalid source, reinitailize transaction";
        public static string HCG145 = "HCG145";
        public static string HCG145M = "Invalid mobile number";
        public static string HCG146 = "HCG146";
        public static string HCG146M = "Invalid mobile number";
        public static string HCG147 = "HCG147";
        public static string HCG147M = "Invalid mobile number";
        public static string HCG148 = "HCG148";
        public static string HCG148M = "Invalid transaction mode";
        public static string HCG149 = "Reference number already present";
        public static string HCG150 = "Invalid amount";
        public static string HCG151 = "Invalid transaction reference";
        public static string HCG152 = "Transaction reference missing";
        public static string HCG153 = "Cash transaction not accepted";
        public static string HCG154 = "Card bin missing";
        public static string HCG155 = "Cashier id missing";
        public static string HCG156 = "Transaction received";
        public static string HCG157 = "Transaction cancelled";

        //public static string HCG500 = "Transaction cancelled";
        public static string HCG502 = "Transaction initialized";
        public static string HCG502M = "Transaction initialized";
        public static string HCG548 = "Pin is not set. Download Thank U Cash App to set pin";
        public static string HCG549 = "Transaction failed. Please try after some time";
        public static string HCG550 = "Transaction failed. Reward amount is less than 0";
        public static string HCG551 = "Terminal id required";
        //public static string HCG552 = "XXXXXXXXXXX";
        //public static string HCG553 = "XXXXXXXXXXX";
        //public static string HCG554 = "XXXXXXXXXXX";
        //public static string HCG555 = "XXXXXXXXXXX";
        //public static string HCG556 = "XXXXXXXXXXX";
        public static string HCG998 = "New user registration";
        public static string HCG999 = "Account already present";
        //public static string XXXXXXXXXX = "XXXXXXXXXXX";
        //public static string XXXXXXXXXX = "XXXXXXXXXXX";
        //public static string XXXXXXXXXX = "XXXXXXXXXXX";
        //public static string XXXXXXXXXX = "XXXXXXXXXXX";
        //public static string XXXXXXXXXX = "XXXXXXXXXXX";
        //public static string XXXXXXXXXX = "XXXXXXXXXXX";



        public static string TUC200 = "Account does not exists";
        public static string TUC201 = "Account suspended or blocked please contact support";
        public static string TUC202 = "";
        public static string TUC203 = "";
        public static string TUC204 = "Account blocked contact support";
        public static string TUC205 = "Account suspended contact support";
        public static string TUC206 = "Invalid transaction reference";
        public static string TUC207 = "Invalid merchant or terminal id";


        public static string HCG161 = "HCG161";
        public static string HCG161M = "Invalid merchant details";
        public static string HCG162 = "HCG162";
        public static string HCG162M = "Invalid terminal id";
        public static string HCG163 = "HCG163";
        public static string HCG163M = "Invalid cashier details";
        public static string HCG164 = "HCG164";
        public static string HCG164M = "Cashier suspended";
        public static string HCG165 = "HCG165";
        public static string HCG165M = "Customer mobile number or card number required";
        public static string HCG166 = "HCG166";
        public static string HCG166M = "Mobile number missing";
        public static string HCG167 = "HCG167";
        public static string HCG167M = "Account loaded";
        public static string HCG168 = "HCG168";
        public static string HCG168M = "Account created";
        public static string HCG169 = "HCG169";
        public static string HCG169M = "Account does not exists";
        public static string HCG170 = "HCG170";
        public static string HCG170M = "HCG170 : Operation failed";


        public static string HCG171 = "HCG171";
        public static string HCG171M = "Merchant id missing";
        public static string HCG172 = "HCG172";
        public static string HCG172M = "Operation failed";
        public static string HCG173 = "HCG173";
        public static string HCG173M = "Invalid payment mode";
        public static string HCG174 = "HCG174";
        public static string HCG174M = "Invalid deal code";
        public static string HCG175 = "HCG175";
        public static string HCG175M = "Deal code loaded";
        public static string HCG176 = "HCG176";
        public static string HCG176M = "Code not valid";
        public static string HCG177 = "HCG177";
        public static string HCG177M = "Code expired";
        public static string HCG178 = "HCG178";
        public static string HCG178M = "Invalid code status";
        public static string HCG179 = "HCG179";
        public static string HCG179M = "Invalid loan status code";
        public static string HCG180 = "HCG180";
        public static string HCG180M = "Loan account loaded";



        public static string HCG181 = "HCG181";
        public static string HCG181M = "Transaction processed successfully";
        public static string HCG182 = "HCG182";
        public static string HCG182M = "Transaction cancelled successfully";
        public static string HCG183 = "HCG183";
        public static string HCG183M = "Terminal id required";
        public static string HCG184 = "HCG184";
        public static string HCG184M = "Mobile number required";
        public static string HCG185 = "HCG185";
        public static string HCG185M = "Rewards allowed only for ThankUCash customers";
        public static string HCG186 = "HCG186";
        public static string HCG186M = "Invalid rewards configuration";
        public static string HCG187 = "HCG187";
        public static string HCG187M = "Reward criteria does not match";
        public static string HCG188 = "HCG188";
        public static string HCG188M = "Reward criteria does not match";
        public static string HCG189 = "HCG189";
        public static string HCG189M = "Reward balance low";
        public static string HCG190 = "HCG190";
        public static string HCG190M = "Reward amount generated";


        public static string HCG191 = "HCG191";
        public static string HCG191M = "Invalid loan code";
        public static string HCG192 = "HCG192";
        public static string HCG192M = "Transaction already cancelled";
        public static string HCG193 = "HCG193";
        public static string HCG193M = "Invalid transaction reference";
        public static string HCG194 = "HCG194";
        public static string HCG194M = "Duplicate transaction reference";
        public static string HCG195 = "HCG195";
        public static string HCG195M = "Invalid source parameter";
        public static string HCG196 = "HCG196";
        public static string HCG196M = "Invalid deal code merchant";
        public static string HCG197 = "HCG197";
        public static string HCG197M = "Code already used";
        public static string HCG198 = "HCG198";
        public static string HCG198M = "Code expired";
        public static string HCG199 = "HCG199";
        public static string HCG199M = "Gateway information loaded";



        public static string HCG201 = "HCG201";
        public static string HCG201M = "Code blocked";
        public static string HCG202 = "HCG202";
        public static string HCG202M = "Invalid code";
        public static string HCG203 = "HCG203";
        public static string HCG203M = "Invalid pin";
        public static string HCG204 = "HCG204";
        public static string HCG204M = "Deal code redeemed";
        public static string HCG205 = "HCG205";
        public static string HCG205M = "Invalid transaction";
        public static string HCG206 = "HCG206";
        public static string HCG206M = "xxxxxxxx";
        public static string HCG207 = "HCG207";
        public static string HCG207M = "xxxxxxxx";
        public static string HCG208 = "HCG208";
        public static string HCG208M = "xxxxxxxx";
        public static string HCG209 = "HCG209";
        public static string HCG209M = "xxxxxxxx";
    }
}
