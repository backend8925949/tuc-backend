﻿using System;
using System.Transactions;
using Akka.Actor;
using Amazon.Runtime.Internal;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.Object;
using SendGrid;
using SendGrid.Helpers.Mail;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Gateway.DataStore.ODataStore.Account;
using static HCore.ThankUCash.Gateway.Helper.HelperGateway;
using static HCore.ThankUCash.Gateway.OpenApi.OOpenApi;

namespace HCore.ThankUCash.Gateway.Core
{
    public class ActorCoreBankLoyaltyProgram : ReceiveActor
    {
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        HCoreContext _HCoreContext;
        CoreOperations _CoreOperations;
        public ActorCoreBankLoyaltyProgram()
        {
            Receive<OTrCoreProcess>(_RequestItem =>
            {
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    var AcquirerLoyaltyProgram = _HCoreContext.TUCLProgramGroup
                    .Where(x => x.AccountId == _RequestItem._GatewayInfo.MerchantId
                    && x.Program.AccountId == _RequestItem._GatewayInfo.AcquirerId
                    && x.Program.StatusId == HelperStatus.Default.Active
                    && x.Program.Account.AccountTypeId == UserAccountType.Acquirer
                    && x.StatusId == HelperStatus.Default.Active
                    && x.StartDate < ActiveTime
                    && (x.EndDate == null || x.EndDate > ActiveTime))
                    .Select(x => new
                    {
                        ProgramId = x.ProgramId,
                        Name = x.Program.Name,
                        RewardPercentage = x.Program.RewardPercentage,
                        ComissionPercentage = x.Program.Comission,
                        MinimumInvoiceAmount = x.Program.MinInvoiceAmount,
                        CashierCommission = x.Program.CashierCommission,
                        CashierMaxReward = x.Program.CashierMaxReward,
                    }).FirstOrDefault();
                    if (AcquirerLoyaltyProgram != null)
                    {
                        if (_RequestItem._GatewayInfo.AcquirerId == 48415 && _RequestItem._ExternalRequest.TransactionMode != PaymentMethod.CardS)
                        {

                        }
                        else
                        {



                            double MinimumInvoiceAmount = 0;
                            if (AcquirerLoyaltyProgram.MinimumInvoiceAmount != null && AcquirerLoyaltyProgram.MinimumInvoiceAmount > 0)
                            {
                                MinimumInvoiceAmount = (double)AcquirerLoyaltyProgram.MinimumInvoiceAmount;
                            }
                            if (_RequestItem._AmountDistributionReference.InvoiceAmount >= MinimumInvoiceAmount)
                            {
                                double RewardAmount = HCoreHelper.GetPercentage(_RequestItem._AmountDistributionReference.InvoiceAmount, (double)AcquirerLoyaltyProgram.RewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                if (RewardAmount > 0)
                                {
                                    double CashierRewardCommission = 0;
                                    string? CashierMobileNumber = null;
                                    if (_RequestItem._GatewayInfo.CashierId > 0)
                                    {
                                        var _CashierDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantCashier
                                                               && x.StatusId == HelperStatus.Default.Active
                                                               && x.Id == _RequestItem._GatewayInfo.CashierId)
                                                               .Select(x => new
                                                               {
                                                                   x.Id,
                                                                   x.ContactNumber,
                                                               }).FirstOrDefault();
                                        if (_CashierDetails != null)
                                        {
                                            CashierRewardCommission = HCoreHelper.GetPercentage(_RequestItem._AmountDistributionReference.InvoiceAmount, AcquirerLoyaltyProgram.CashierCommission, _AppConfig.SystemEntryRoundDouble);
                                            if (AcquirerLoyaltyProgram.CashierMaxReward > 0 && CashierRewardCommission > AcquirerLoyaltyProgram.CashierMaxReward)
                                            {
                                                CashierRewardCommission = AcquirerLoyaltyProgram.CashierMaxReward;
                                            }
                                            if (!string.IsNullOrEmpty(_CashierDetails.ContactNumber))
                                            {
                                                CashierMobileNumber = HCoreHelper.FormatMobileNumber(_RequestItem._ExternalRequest.UserReference.CountryIsd, _CashierDetails.ContactNumber);
                                            }
                                        }
                                        //_CoreOperations = new CoreOperations();
                                        //OUserInfo _UserInfo = _CoreOperations.GetUserInfo(_RequestItem._ExternalRequest, _RequestItem._GatewayInfo, true);
                                        //if (_UserInfo.Status == ResponseStatus.Success)
                                        //{
                                        //    CashierMobileNumber = _UserInfo.MobileNumber;
                                        //    CashierCustomerAccountId = _UserInfo.UserAccountId;
                                        //}
                                    }
                                    double ComissionAmount = HCoreHelper.GetPercentage(_RequestItem._AmountDistributionReference.InvoiceAmount, AcquirerLoyaltyProgram.ComissionPercentage, _AppConfig.SystemEntryRoundDouble);
                                    double TotalLoyaltyAmount = RewardAmount + ComissionAmount + CashierRewardCommission;
                                    string GroupKey = HCoreHelper.GenerateGuid();
                                    #region Process Customer Reward
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.GroupKey = GroupKey;
                                    _CoreTransactionRequest.CustomerId = _RequestItem._UserInfo.UserAccountId;
                                    _CoreTransactionRequest.UserReference = _RequestItem._Request.UserReference;
                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                    _CoreTransactionRequest.ParentId = _RequestItem._GatewayInfo.MerchantId;
                                    if (_RequestItem._GatewayInfo.StoreId > 0)
                                    {
                                        _CoreTransactionRequest.SubParentId = _RequestItem._GatewayInfo.StoreId;
                                    }
                                    _CoreTransactionRequest.InvoiceAmount = _RequestItem._AmountDistributionReference.InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = _RequestItem._AmountDistributionReference.InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceAmount = TotalLoyaltyAmount;
                                    _CoreTransactionRequest.TerminalId = _RequestItem._GatewayInfo.TerminalId;
                                    _CoreTransactionRequest.ReferenceNumber = _RequestItem._Request.ReferenceNumber;
                                    if (CashierRewardCommission > 0)
                                    {
                                        _CoreTransactionRequest.CashierReward = CashierRewardCommission;
                                    }
                                    if (!string.IsNullOrEmpty(_RequestItem._ExternalRequest.bin))
                                    {
                                        _CoreTransactionRequest.AccountNumber = _RequestItem._ExternalRequest.bin;
                                    }
                                    else
                                    {
                                        _CoreTransactionRequest.AccountNumber = _RequestItem._ExternalRequest.SixDigitPan;
                                    }
                                    if (_RequestItem._GatewayInfo.AcquirerId > 0)
                                    {
                                        _CoreTransactionRequest.BankId = (long)_RequestItem._GatewayInfo.AcquirerId;
                                    }
                                    _CoreTransactionRequest.ProgramId = AcquirerLoyaltyProgram.ProgramId;
                                    if (_RequestItem._GatewayInfo.CashierId > 0)
                                    {
                                        _CoreTransactionRequest.CashierId = _RequestItem._GatewayInfo.CashierId;
                                    }
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>
                                    {
                                        new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = (long)_RequestItem._GatewayInfo.AcquirerId,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = _RequestItem._AmountDistributionReference.TransactionTypeId,
                                            SourceId = TransactionSource.TUCBlack,
                                            Amount = RewardAmount,
                                            Charge = CashierRewardCommission,
                                            Comission = ComissionAmount,
                                            TotalAmount = TotalLoyaltyAmount,
                                        },
                                        new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _RequestItem._UserInfo.UserAccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = _RequestItem._AmountDistributionReference.TransactionTypeId,
                                            SourceId = TransactionSource.TUCBlack,
                                            Amount = RewardAmount,
                                            TotalAmount = RewardAmount,
                                        },
                                        new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = SystemAccounts.ThankUCashMerchant,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = _RequestItem._AmountDistributionReference.TransactionTypeId,
                                            SourceId = TransactionSource.Settlement,
                                            Amount = ComissionAmount,
                                            TotalAmount = ComissionAmount,
                                        }
                                    };
                                    if (_RequestItem._GatewayInfo.CashierId > 0 && CashierRewardCommission > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _RequestItem._GatewayInfo.CashierId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = _RequestItem._AmountDistributionReference.TransactionTypeId,
                                            SourceId = TransactionSource.Cashier,
                                            Amount = CashierRewardCommission,
                                            TotalAmount = CashierRewardCommission,
                                        });
                                    }
                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                    {

                                        var ProgramBalance = _ManageCoreTransaction.GetLoyaltyProgramBalance(_RequestItem._UserInfo.UserAccountId, _RequestItem._GatewayInfo.MerchantId, (long)_RequestItem._GatewayInfo.AcquirerId);
                                        if (!string.IsNullOrEmpty(_RequestItem._UserInfo.MobileNumber))
                                        {
                                            string Message = "Credit Alert! You earned " + RewardAmount + " " + AcquirerLoyaltyProgram.Name + ". Your Bal: " + ProgramBalance.Balance.ToString() + " points.";
                                            if (_RequestItem._GatewayInfo.AcquirerId == 48415)
                                            {
                                                Message = "Credit Alert! You earned " + RewardAmount + " " + AcquirerLoyaltyProgram.Name + ". Your Bal: " + ProgramBalance.Balance.ToString() + " points. https://accessrewards.thankucash.com to check balance";
                                            }
                                            HCoreHelper.SendSMS(SmsType.Transaction, _RequestItem._ExternalRequest.UserReference.CountryIsd, _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, null);
                                        }
                                        if (!string.IsNullOrEmpty(_RequestItem._UserInfo.EmailAddress)) // email
                                        {
                                            #region Send Email 
                                            if (!string.IsNullOrEmpty(_RequestItem._UserInfo.EmailAddress))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = _RequestItem._UserInfo.DisplayName,
                                                    MerchantName = AcquirerLoyaltyProgram.Name,
                                                    InvoiceAmount = _RequestItem._AmountDistributionReference.InvoiceAmount.ToString(),
                                                    Amount = HCoreHelper.RoundNumber(RewardAmount, 2).ToString(),
                                                    Balance = ProgramBalance.Balance.ToString(),
                                                };
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _RequestItem._UserInfo.DisplayName, _RequestItem._UserInfo.EmailAddress, _EmailParameters, _RequestItem._Request.UserReference);
                                            }
                                            #endregion
                                        }

                                        #region Send Cashier SMS Notificaiton
                                        if (_RequestItem._GatewayInfo.CashierId > 0 && CashierRewardCommission > 0 && !string.IsNullOrEmpty(CashierMobileNumber))
                                        {
                                            var CashierBalance = _ManageCoreTransaction.GetLoyaltyProgramBalance(_RequestItem._GatewayInfo.CashierId, _RequestItem._GatewayInfo.MerchantId, (long)_RequestItem._GatewayInfo.AcquirerId, TransactionSource.Cashier);
                                            string Message = "Credit Alert! You earned " + CashierRewardCommission + " " + AcquirerLoyaltyProgram.Name + ". Your Bal: " + CashierBalance.Balance.ToString() + " points.";
                                            HCoreHelper.SendSMS(SmsType.Transaction, _RequestItem._ExternalRequest.UserReference.CountryIsd, CashierMobileNumber, Message, _RequestItem._GatewayInfo.CashierId, null);
                                        }
                                        #endregion

                                    }
                                    #endregion
                                }

                            }
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }
                }
            });
        }
    }
}

