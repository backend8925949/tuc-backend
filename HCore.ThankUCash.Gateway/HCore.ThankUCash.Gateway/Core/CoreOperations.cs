//==================================================================================
// FileName: CoreOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.ThankUCash.Gateway.DataStore;
using HCore.ThankUCash.Gateway.Framework;
using HCore.ThankUCash.Gateway.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Gateway.Helper.HelperGateway;

namespace HCore.ThankUCash.Gateway.Core
{
    internal class CoreOperations
    {
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCoreContext _HCoreContext;
        OAmountDistribution _AmountDistribution;
        OCustomer.Request _CustomerRequest;
        OCustomer.Response _CustomerResponse;
        OPostTransaction _OPostTransaction;
        /// <summary>
        /// Create Account Of App User / Customer registering through PTSP / PSSP / USSD And Other Providers
        /// </summary>
        /// <returns>The profile information</returns>
        private OCustomer.Response CreateCustomerAccount(OCustomer.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CustomerResponse = new OCustomer.Response();
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    _CustomerResponse.Status = ResponseStatus.Error;
                    _CustomerResponse.StatusResponseCode = CoreResources.HCG166;
                    _CustomerResponse.StatusMessage = CoreResources.HCG166M;
                    return _CustomerResponse;
                }
                #region Manage Operations
                using (_HCoreContext = new HCoreContext())
                {
                    #region  Process Registration
                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, (int)_Request.UserReference.CountryMobileNumberLength);
                    string CustomerAppId = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                    int GenderId = 0;
                    string MerchantDisplayName = "";
                    if (_Request.OwnerId != 0)
                    {
                        MerchantDisplayName = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _Request.OwnerId).Select(x => x.DisplayName).FirstOrDefault();
                        if (string.IsNullOrEmpty(MerchantDisplayName))
                        {
                            MerchantDisplayName = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _Request.OwnerId && (x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.Partner)).Select(x => x.DisplayName).FirstOrDefault();
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        if (_Request.GenderCode == "gender.male" || _Request.GenderCode == "male")
                        {
                            GenderId = Gender.Male;
                        }
                        if (_Request.GenderCode == "gender.female" || _Request.GenderCode == "female")
                        {
                            GenderId = Gender.Female;
                        }
                        if (_Request.GenderCode == "gender.other" || _Request.GenderCode == "other")
                        {
                            GenderId = Gender.Other;
                        }
                    }
                    string UserKey = HCoreHelper.GenerateGuid();
                    string UserAccountKey = HCoreHelper.GenerateGuid();
                    _Random = new Random();
                    string AccessPin = _Random.Next(1111, 9999).ToString();
                    if (HostEnvironment == HostEnvironmentType.Test)
                    {
                        AccessPin = "1234";
                    }
                    #region Save User
                    _HCUAccountAuth = new HCUAccountAuth();
                    _HCUAccountAuth.Guid = UserKey;
                    _HCUAccountAuth.Username = CustomerAppId;
                    _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                    _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                    _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                    _HCUAccountAuth.CreateDate = CreateDate;
                    _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                    if (_Request.CreatedById != 0)
                    {
                        _HCUAccountAuth.CreatedById = _Request.CreatedById;
                    }
                    else
                    {
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                        }
                    }
                    #endregion
                    #region Online Account
                    _HCUAccount = new HCUAccount();
                    _HCUAccount.Guid = UserAccountKey;
                    _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                    _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                    if (_Request.OwnerId != 0)
                    {
                        _HCUAccount.OwnerId = _Request.OwnerId;
                    }
                    if (_Request.SubOwnerId != 0)
                    {
                        _HCUAccount.SubOwnerId = _Request.SubOwnerId;
                    }
                    _HCUAccount.MobileNumber = _Request.MobileNumber;
                    if (!string.IsNullOrEmpty(_Request.DisplayName))
                    {
                        _HCUAccount.DisplayName = _Request.DisplayName;
                    }
                    else
                    {
                        _HCUAccount.DisplayName = _Request.MobileNumber;
                    }
                    _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                    _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                    if (!string.IsNullOrEmpty(_Request.Name))
                    {
                        _HCUAccount.Name = _Request.Name;
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _HCUAccount.MobileNumber = _Request.MobileNumber;
                        _HCUAccount.ContactNumber = _Request.MobileNumber;
                    }
                    if (!string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                    }
                    if (!string.IsNullOrEmpty(_Request.Address))
                    {
                        _HCUAccount.Address = _Request.Address;
                    }
                    if (GenderId != 0)
                    {
                        _HCUAccount.GenderId = GenderId;
                    }
                    if (_Request.DateOfBirth != null)
                    {
                        _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                    }
                    _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                    _HCUAccount.EmailVerificationStatus = 0;
                    _HCUAccount.EmailVerificationStatusDate = CreateDate;
                    _HCUAccount.NumberVerificationStatus = 0;
                    _HCUAccount.NumberVerificationStatusDate = CreateDate;
                    _Random = new Random();
                    string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                    _HCUAccount.AccountCode = AccountCode;
                    _HCUAccount.ReferralCode = _Request.MobileNumber;
                    _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                    if (_Request.UserReference.AppVersionId != 0)
                    {
                        _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                    }

                    if (_Request.CreatedById != 0)
                    {
                        _HCUAccount.CreatedById = _Request.CreatedById;
                    }
                    else
                    {
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                        }
                    }
                    _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                    _HCUAccount.CreateDate = CreateDate;
                    _HCUAccount.StatusId = HelperStatus.Default.Active;
                    _HCUAccount.User = _HCUAccountAuth;
                    _HCoreContext.HCUAccount.Add(_HCUAccount);
                    _HCoreContext.SaveChanges();
                    #endregion
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                    {
                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            if (!string.IsNullOrEmpty(MerchantDisplayName))
                            {
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, "Welcome to " + MerchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                            }
                            else
                            {
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                            }
                        }
                    }
                    _CustomerResponse.AccountId = _HCUAccount.Id;
                    _CustomerResponse.AccountKey = _HCUAccount.Guid;
                    _CustomerResponse.DisplayName = _HCUAccount.DisplayName;
                    _CustomerResponse.Name = _HCUAccount.Name;
                    _CustomerResponse.EmailAddress = _HCUAccount.EmailAddress;
                    _CustomerResponse.MobileNumber = _HCUAccount.MobileNumber;
                    _CustomerResponse.Pin = AccessPin;
                    _CustomerResponse.AccountNumber = AccountCode;
                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        if (_Request.GenderCode == "gender.male" || _Request.GenderCode == "male")
                        {
                            _CustomerResponse.Gender = "Male";
                        }
                        if (_Request.GenderCode == "gender.female" || _Request.GenderCode == "female")
                        {
                            _CustomerResponse.Gender = "Female";
                        }
                        if (_Request.GenderCode == "gender.other" || _Request.GenderCode == "other")
                        {
                            _CustomerResponse.Gender = "Other";
                        }
                    }
                    _CustomerResponse.OwnerId = _HCUAccount.OwnerId;
                    if (_CustomerResponse.OwnerId != null)
                    {
                        var MerchantCheck = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _HCUAccount.OwnerId).FirstOrDefault();
                        if (MerchantCheck != null)
                        {
                            _CustomerResponse.OwnerAccountTypeId = UserAccountType.Merchant;
                        }
                        var PartnerDetails = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _HCUAccount.OwnerId).FirstOrDefault();
                        if (PartnerDetails != null)
                        {
                            _CustomerResponse.OwnerAccountTypeId = PartnerDetails.AccountTypeId;
                        }
                    }
                    _CustomerResponse.SubOwnerId = _HCUAccount.SubOwnerId;
                    _CustomerResponse.CreateDate = _HCUAccount.CreateDate;
                    _CustomerResponse.CreatedById = _HCUAccount.CreatedById;
                    if (_CustomerResponse.OwnerId != null)
                    {
                        var MerchantCheck = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _HCUAccount.CreatedById).FirstOrDefault();
                        if (MerchantCheck != null)
                        {
                            _CustomerResponse.CreatedByAccountTypeId = UserAccountType.Merchant;
                        }
                        if (_CustomerResponse.CreatedByAccountTypeId < 1)
                        {
                            var PartnerDetails = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _HCUAccount.CreatedById).FirstOrDefault();
                            if (PartnerDetails != null)
                            {
                                _CustomerResponse.CreatedByAccountTypeId = PartnerDetails.AccountTypeId;
                            }
                        }
                    }
                    _CustomerResponse.StatusId = _HCUAccount.StatusId;
                    _CustomerResponse.Status = ResponseStatus.Success;
                    _CustomerResponse.StatusResponseCode = CoreResources.HCG168;
                    _CustomerResponse.StatusMessage = CoreResources.HCG168M;
                    return _CustomerResponse;
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("CreateAppUserAccount", _Exception, _Request.UserReference);
                #endregion
                _CustomerResponse.Status = ResponseStatus.Error;
                _CustomerResponse.StatusResponseCode = CoreResources.HCG170;
                _CustomerResponse.StatusMessage = CoreResources.HCG170M;
                return _CustomerResponse;
            }
            #endregion
        }
        internal string GetConfiguration(string ConfigurationCode)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                var _Configuration = DataStore.DataStore.Configurations.Where(x => x.SystemName == ConfigurationCode && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                if (_Configuration != null)
                {
                    if (!string.IsNullOrEmpty(_Configuration.ActiveValue))
                    {
                        return _Configuration.ActiveValue;
                    }
                    else
                    {
                        return "0";
                    }
                }
                else
                {
                    return "0";
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetConfiguration", _Exception);
                return "0";
            }
            #endregion
        }
        internal string GetConfiguration(string ConfigurationCode, long? UserAccountId)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                var _Configuration = DataStore.DataStore.Configurations.Where(x => x.SystemName == ConfigurationCode && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                if (_Configuration != null)
                {
                    string ConfigValue = _Configuration.ActiveValue;
                    string AccountConfigValue = DataStore.DataStore.AccountConfigurations.Where(x => x.AccountId == UserAccountId && x.ConfigurationId == _Configuration.ReferenceId && x.StatusId == HelperStatus.Default.Active).Select(x => x.Value).FirstOrDefault();
                    if (AccountConfigValue != null)
                    {
                        ConfigValue = AccountConfigValue;
                    }
                    if (!string.IsNullOrEmpty(ConfigValue))
                    {
                        return ConfigValue;
                    }
                    else
                    {
                        return "0";
                    }
                }
                else
                {
                    return "0";
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetConfiguration", _Exception);
                return "0";
            }
            #endregion
        }
        internal string GetAccountConfiguration(string ConfigurationCode, long? UserAccountId)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                var _Configuration = DataStore.DataStore.Configurations.Where(x => x.SystemName == ConfigurationCode && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                if (_Configuration != null)
                {
                    //string ConfigValue = _Configuration.ActiveValue;
                    string AccountConfigValue = DataStore.DataStore.AccountConfigurations.Where(x => x.AccountId == UserAccountId && x.ConfigurationId == _Configuration.ReferenceId && x.StatusId == HelperStatus.Default.Active).Select(x => x.Value).FirstOrDefault();
                    if (!string.IsNullOrEmpty(AccountConfigValue))
                    {
                        return AccountConfigValue;
                    }
                    else
                    {
                        return "0";
                    }
                    //if (AccountConfigValue != null)
                    //{
                    //    ConfigValue = AccountConfigValue;
                    //}
                    //if (!string.IsNullOrEmpty(ConfigValue))
                    //{
                    //    return ConfigValue;
                    //}
                    //else
                    //{
                    //    return "0";
                    //}
                }
                else
                {
                    return "0";
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetConfiguration", _Exception);
                return "0";
            }
            #endregion
        }

        internal OConfigurationInfo GetConfigurationDetails(string ConfigurationCode, long? UserAccountId)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                var _Configuration = DataStore.DataStore.Configurations.Where(x => x.SystemName == ConfigurationCode && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                if (_Configuration != null)
                {
                    string ConfigValue = _Configuration.ActiveValue;
                    ODataStore.Configuration.Account AccountConfigValue = DataStore.DataStore.AccountConfigurations.Where(x => x.AccountId == UserAccountId && x.ConfigurationId == _Configuration.ReferenceId && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                    if (AccountConfigValue != null)
                    {
                        OConfigurationInfo _OConfiguration = new OConfigurationInfo();
                        _OConfiguration.Value = AccountConfigValue.Value;
                        _OConfiguration.TypeCode = AccountConfigValue.TypeCode;
                        _OConfiguration.TypeName = AccountConfigValue.TypeName;
                        return _OConfiguration;
                    }
                    else
                    {
                        OConfigurationInfo _OConfiguration = new OConfigurationInfo();
                        _OConfiguration.Value = _Configuration.ActiveValue;
                        _OConfiguration.TypeCode = _Configuration.TypeCode;
                        _OConfiguration.TypeName = _Configuration.TypeName;
                        return _OConfiguration;
                    }
                }
                else
                {
                    return null;
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetConfigurationDetails", _Exception);
                return null;
            }
            #endregion
        }
        internal OGatewayInfo GetGatewayInfo(OThankUGateway.Request _Request)
        {
            OGatewayInfo _GatewayInfo = new OGatewayInfo();
            _GatewayInfo.Status = ResponseStatus.Error;
            _GatewayInfo.Message = CoreResources.HCG500M;
            _GatewayInfo.ResponseCode = CoreResources.HCG500;
            try
            {
                if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                {
                    var AccInfo = DataStore.DataStore.Terminals
                      .Where(x => x.TerminalId == _Request.TerminalId)
                      .Select(x => new OGatewayInfo
                      {
                          TerminalId = x.ReferenceId,
                          PtspId = (long)x.PtspId,
                          AcquirerId = x.BankId,
                          MerchantId = (long)x.MerchantId,
                          StoreId = (long)x.StoreId,
                      }).FirstOrDefault();
                    if (AccInfo != null)
                    {
                        var MerchantDetails = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == AccInfo.MerchantId).FirstOrDefault();
                        if (MerchantDetails != null)
                        {
                            _GatewayInfo.MerchantId = MerchantDetails.ReferenceId;
                            _GatewayInfo.MerchantPrimaryCategoryId = MerchantDetails.PrimaryCategoryId;
                            _GatewayInfo.MerchantSecondaryCategoryId = MerchantDetails.SecondaryCategoryId;
                            _GatewayInfo.MerchantDisplayName = MerchantDetails.DisplayName;
                            _GatewayInfo.MerchantReferrerId = MerchantDetails.OwnerId;
                            _GatewayInfo.MerchantReferrerAccountTypeId = MerchantDetails.OwnerAccountTypeId;
                        }
                        _GatewayInfo.TerminalId = AccInfo.TerminalId;
                        _GatewayInfo.PtspId = AccInfo.PtspId;
                        _GatewayInfo.AcquirerId = AccInfo.AcquirerId;
                        var StoreDetails = DataStore.DataStore.Stores.Where(x => x.ReferenceId == AccInfo.StoreId).FirstOrDefault();
                        if (StoreDetails != null)
                        {
                            _GatewayInfo.StoreId = StoreDetails.ReferenceId;
                            _GatewayInfo.StoreStatusId = StoreDetails.StatusId;
                        }
                        _GatewayInfo.Status = ResponseStatus.Success;
                        _GatewayInfo.ResponseCode = CoreResources.HCG199;
                        _GatewayInfo.Message = CoreResources.HCG199M;
                    }
                    else
                    {
                        HCore.ThankUCash.Gateway.DataStore.DataStore.RefreshDataStore();
                        var NewAccInfo = DataStore.DataStore.Terminals
                     .Where(x => x.TerminalId == _Request.TerminalId)
                     .Select(x => new OGatewayInfo
                     {
                         TerminalId = x.ReferenceId,
                         PtspId = (long)x.PtspId,
                         AcquirerId = x.BankId,
                         MerchantId = (long)x.MerchantId,
                         StoreId = (long)x.StoreId,
                     }).FirstOrDefault();
                        if (NewAccInfo != null)
                        {
                            var MerchantDetails = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == NewAccInfo.MerchantId).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                _GatewayInfo.MerchantId = MerchantDetails.ReferenceId;
                                _GatewayInfo.MerchantPrimaryCategoryId = MerchantDetails.PrimaryCategoryId;
                                _GatewayInfo.MerchantSecondaryCategoryId = MerchantDetails.SecondaryCategoryId;
                                _GatewayInfo.MerchantDisplayName = MerchantDetails.DisplayName;
                                _GatewayInfo.MerchantReferrerId = MerchantDetails.OwnerId;
                                _GatewayInfo.MerchantReferrerAccountTypeId = MerchantDetails.OwnerAccountTypeId;
                            }
                            _GatewayInfo.TerminalId = NewAccInfo.TerminalId;
                            _GatewayInfo.PtspId = NewAccInfo.PtspId;
                            _GatewayInfo.AcquirerId = NewAccInfo.AcquirerId;
                            var StoreDetails = DataStore.DataStore.Stores.Where(x => x.ReferenceId == NewAccInfo.StoreId).FirstOrDefault();
                            if (StoreDetails != null)
                            {
                                _GatewayInfo.StoreId = StoreDetails.ReferenceId;
                                _GatewayInfo.StoreStatusId = StoreDetails.StatusId;
                            }
                            _GatewayInfo.Status = ResponseStatus.Success;
                            _GatewayInfo.ResponseCode = CoreResources.HCG199;
                            _GatewayInfo.Message = CoreResources.HCG199M;
                        }
                        else
                        {
                            _GatewayInfo.Status = ResponseStatus.Error;
                            _GatewayInfo.ResponseCode = CoreResources.HCG162;
                            _GatewayInfo.Message = CoreResources.HCG162M;
                            return _GatewayInfo;
                        }
                    }
                }
                else if (_Request.UserReference.AccountTypeId == UserAccountType.PgAccount || _Request.UserReference.AccountTypeId == UserAccountType.Partner || _Request.UserReference.AccountTypeId == UserAccountType.Acquirer)
                {
                    var AccInfo = DataStore.DataStore.Merchants
                        .Where(x => x.AccountCode == _Request.MerchantId)
                        .Select(x => new OGatewayInfo
                        {
                            MerchantId = x.ReferenceId,
                            MerchantDisplayName = x.DisplayName,
                            MerchantReferrerId = x.OwnerId,
                            MerchantReferrerAccountTypeId = x.OwnerAccountTypeId,
                            MerchantPrimaryCategoryId = x.PrimaryCategoryId,
                            MerchantSecondaryCategoryId = x.SecondaryCategoryId,
                        }).FirstOrDefault();
                    if (AccInfo != null)
                    {
                        _GatewayInfo.MerchantId = AccInfo.MerchantId;
                        _GatewayInfo.MerchantPrimaryCategoryId = AccInfo.MerchantPrimaryCategoryId;
                        _GatewayInfo.MerchantSecondaryCategoryId = AccInfo.MerchantSecondaryCategoryId;
                        _GatewayInfo.MerchantDisplayName = AccInfo.MerchantDisplayName;
                        _GatewayInfo.MerchantReferrerId = AccInfo.MerchantReferrerId;
                        _GatewayInfo.MerchantReferrerAccountTypeId = AccInfo.MerchantReferrerAccountTypeId;
                        _GatewayInfo.Status = ResponseStatus.Success;
                        _GatewayInfo.ResponseCode = CoreResources.HCG199;
                        _GatewayInfo.Message = CoreResources.HCG199M;
                    }
                    else
                    {
                        _GatewayInfo.Status = ResponseStatus.Error;
                        _GatewayInfo.ResponseCode = CoreResources.HCG161;
                        _GatewayInfo.Message = CoreResources.HCG161M;
                        return _GatewayInfo;
                    }
                }
                else if (_Request.UserReference.AccountTypeId == UserAccountType.Merchant)
                {
                    var AccInfo = DataStore.DataStore.Merchants
                        .Where(x => x.ReferenceId == _Request.UserReference.AccountId)
                        .Select(x => new OGatewayInfo
                        {
                            MerchantId = x.ReferenceId,
                            MerchantDisplayName = x.DisplayName,
                            MerchantReferrerId = x.OwnerId,
                            MerchantReferrerAccountTypeId = x.OwnerAccountTypeId,
                            MerchantPrimaryCategoryId = x.PrimaryCategoryId,
                            MerchantSecondaryCategoryId = x.SecondaryCategoryId,
                        }).FirstOrDefault();
                    if (AccInfo != null)
                    {
                        _GatewayInfo.MerchantId = AccInfo.MerchantId;
                        _GatewayInfo.MerchantPrimaryCategoryId = AccInfo.MerchantPrimaryCategoryId;
                        _GatewayInfo.MerchantSecondaryCategoryId = AccInfo.MerchantSecondaryCategoryId;
                        _GatewayInfo.MerchantDisplayName = AccInfo.MerchantDisplayName;
                        _GatewayInfo.MerchantReferrerId = AccInfo.MerchantReferrerId;
                        _GatewayInfo.MerchantReferrerAccountTypeId = AccInfo.MerchantReferrerAccountTypeId;
                        _GatewayInfo.Status = ResponseStatus.Success;
                        _GatewayInfo.ResponseCode = CoreResources.HCG199;
                        _GatewayInfo.Message = CoreResources.HCG199M;
                    }
                    else
                    {
                        _GatewayInfo.Status = ResponseStatus.Error;
                        _GatewayInfo.ResponseCode = CoreResources.HCG161;
                        _GatewayInfo.Message = CoreResources.HCG161M;
                        return _GatewayInfo;
                    }
                }
                if (!string.IsNullOrEmpty(_Request.CashierId))
                {
                    var CashierDetails = DataStore.DataStore.Cashiers.Where(x => x.CashierId == _Request.CashierId && x.MerchantId == _GatewayInfo.MerchantId).FirstOrDefault();
                    if (CashierDetails != null)
                    {
                        using (HCoreContext _HCoreContext = new HCoreContext())
                        {
                            CashierDetails.StatusId = _HCoreContext.HCUAccount.Where(x => x.Id == CashierDetails.ReferenceId).Select(x => x.StatusId).FirstOrDefault();
                            _HCoreContext.Dispose();
                        }
                        _GatewayInfo.CashierId = CashierDetails.ReferenceId;
                        _GatewayInfo.CashierStatusId = CashierDetails.StatusId;
                        if (CashierDetails.StatusId != HelperStatus.Default.Active)
                        {
                            _GatewayInfo = new OGatewayInfo();
                            _GatewayInfo.Status = ResponseStatus.Error;
                            _GatewayInfo.ResponseCode = CoreResources.HCG164;
                            _GatewayInfo.Message = CoreResources.HCG164M;
                            return _GatewayInfo;
                        }
                    }
                    else
                    {
                        _GatewayInfo = new OGatewayInfo();
                        _GatewayInfo.Status = ResponseStatus.Error;
                        _GatewayInfo.ResponseCode = CoreResources.HCG163;
                        _GatewayInfo.Message = CoreResources.HCG163M;
                        return _GatewayInfo;
                    }
                }
                else
                {
                    var CashierDetails = DataStore.DataStore.Cashiers.Where(x => x.MerchantId == _GatewayInfo.MerchantId).OrderBy(x => x.CashierId).FirstOrDefault();
                    if (CashierDetails != null)
                    {
                        _GatewayInfo.CashierId = CashierDetails.ReferenceId;
                    }
                }

                if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                {
                    if (_GatewayInfo.StoreId != 0)
                    {
                        _GatewayInfo.TransactionIssuerId = _GatewayInfo.StoreId;
                        _GatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.MerchantStore;
                    }
                    else
                    {
                        _GatewayInfo.TransactionIssuerId = SystemAccounts.ThankUCashMerchant;
                        _GatewayInfo.TransactionIssuerAccountTypeId = UserAccountType.Merchant;
                    }
                }
                return _GatewayInfo;
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetGatewayInfo", _Exception);
                return _GatewayInfo;
            }
        }
        internal OUserInfo GetUserInfo(OThankUGateway.Request _Request, OGatewayInfo _GatewayInfo, bool CreateUser)
        {
            OUserInfo _UserInfo = new OUserInfo();
            _UserInfo.Status = ResponseStatus.Error;
            _UserInfo.Message = CoreResources.HCG500M;
            _UserInfo.ResponseCode = CoreResources.HCG500;
            if (!string.IsNullOrEmpty(_Request.MobileNumber))
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    if (_Request.MobileNumber.StartsWith("12"))
                    {

                        var DealCode = _HCoreContext.MDDealCode.Where(x => x.ItemCode == _Request.MobileNumber)
                            .Select(x => new DealCode
                            {
                                DealKey = x.Guid,
                                DealPin = x.ItemPin,
                                MerchantId = x.Deal.AccountId,

                                Amount = x.Amount,
                                DealStatusId = x.Deal.StatusId,

                                AccountId = x.AccountId,
                                AccountDisplayName = x.Account.DisplayName,
                                AccountEmailAddress = x.Account.EmailAddress,
                                AccountStatusId = x.Account.StatusId,
                                AccountMobileNumber = x.Account.MobileNumber,
                                AccountNumber = x.Account.MobileNumber,
                                AccountPin = x.Account.AccessPin,

                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                            }).FirstOrDefault();
                        if (DealCode != null)
                        {
                            if (DealCode.MerchantId != _GatewayInfo.MerchantId)
                            {
                                _UserInfo.Status = ResponseStatus.Error;
                                _UserInfo.Message = CoreResources.HCG196;
                                _UserInfo.ResponseCode = CoreResources.HCG196M;
                                return _UserInfo;
                            }
                            else
                            {
                                switch (DealCode.DealStatusId)
                                {
                                    case HelperStatus.DealCodes.Unused:
                                        {
                                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                            if (CurrentTime < DealCode.StartDate)
                                            {
                                                _UserInfo.Status = ResponseStatus.Success;
                                                _UserInfo.Message = CoreResources.HCG176M;
                                                _UserInfo.ResponseCode = CoreResources.HCG176;
                                                return _UserInfo;
                                            }
                                            else if (CurrentTime > DealCode.EndDate)
                                            {
                                                _UserInfo.Status = ResponseStatus.Success;
                                                _UserInfo.Message = CoreResources.HCG177M;
                                                _UserInfo.ResponseCode = CoreResources.HCG177;
                                                return _UserInfo;
                                            }
                                            else if (CurrentTime > DealCode.StartDate && CurrentTime < DealCode.EndDate)
                                            {
                                                if (DealCode.Amount != null)
                                                {
                                                    _UserInfo.DealAmount = (double)DealCode.Amount;
                                                }
                                                else
                                                {
                                                    _UserInfo.DealAmount = 0;
                                                }

                                                _UserInfo.InvoiceAmount = (double)DealCode.Amount;
                                                _UserInfo.RedeemAmount = (double)DealCode.Amount;
                                                _UserInfo.DealKey = DealCode.DealKey;
                                                _UserInfo.FeaturePin = DealCode.DealPin;

                                                _UserInfo.UserAccountId = (long)DealCode.AccountId;
                                                _UserInfo.AccountNumber = DealCode.AccountNumber;
                                                _UserInfo.MobileNumber = DealCode.AccountMobileNumber;
                                                _UserInfo.DisplayName = DealCode.AccountDisplayName;
                                                _UserInfo.Pin = DealCode.AccountPin;
                                                _UserInfo.EmailAddress = DealCode.AccountEmailAddress;
                                                _UserInfo.AccountStatusId = (long)DealCode.AccountStatusId;


                                                //if (DealCode.OwnerId != null)
                                                //{
                                                //    _UserInfo.IssuerId = (long)DealCode.OwnerId;
                                                //    _UserInfo.IssuerAccountTypeId = (long)DealCode.OwnerAccountTypeId;
                                                //}
                                                //if (DealCode.CreatedById != null)
                                                //{
                                                //    if (DealCode.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                                //    {
                                                //        _UserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == DealCode.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                                                //        _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                                //    }
                                                //    if (DealCode.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                                //    {
                                                //        _UserInfo.IssuerId = (long)DealCode.CreatedById;
                                                //        _UserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                                //    }
                                                //}
                                                //if (_UserInfo.IssuerId == 0)
                                                //{
                                                //    _UserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                                //    _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                                //}
                                                _UserInfo.Status = ResponseStatus.Success;
                                                _UserInfo.Message = CoreResources.HCG175M;
                                                _UserInfo.ResponseCode = CoreResources.HCG175;
                                                return _UserInfo;
                                            }
                                            else
                                            {
                                                _UserInfo.Status = ResponseStatus.Success;
                                                _UserInfo.Message = CoreResources.HCG178M;
                                                _UserInfo.ResponseCode = CoreResources.HCG178;
                                                return _UserInfo;
                                            }
                                        }
                                    case HelperStatus.DealCodes.Used:
                                        _UserInfo.Status = ResponseStatus.Error;
                                        _UserInfo.Message = CoreResources.HCG197;
                                        _UserInfo.ResponseCode = CoreResources.HCG197M;
                                        return _UserInfo;
                                    case HelperStatus.DealCodes.Expired:
                                        _UserInfo.Status = ResponseStatus.Error;
                                        _UserInfo.Message = CoreResources.HCG198;
                                        _UserInfo.ResponseCode = CoreResources.HCG198M;
                                        return _UserInfo;
                                    case HelperStatus.DealCodes.Blocked:
                                        _UserInfo.Status = ResponseStatus.Error;
                                        _UserInfo.Message = CoreResources.HCG201;
                                        _UserInfo.ResponseCode = CoreResources.HCG201M;
                                        return _UserInfo;
                                    default:
                                        _UserInfo.Status = ResponseStatus.Error;
                                        _UserInfo.Message = CoreResources.HCG202;
                                        _UserInfo.ResponseCode = CoreResources.HCG202M;
                                        return _UserInfo;
                                }
                            }
                        }
                        else
                        {
                            _UserInfo.Status = ResponseStatus.Error;
                            _UserInfo.Message = CoreResources.HCG174M;
                            _UserInfo.ResponseCode = CoreResources.HCG174;
                            return _UserInfo;
                        }
                    }
                    if (_Request.MobileNumber.StartsWith("45"))
                    {

                        var LoanDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.LoanCode == _Request.MobileNumber && x.Merchant.Account.Id == _GatewayInfo.MerchantId)
                            .Select(x => new LoanCode
                            {
                                LoanKey = x.Guid,
                                AccountId = x.Account.Account.Id,
                                AccountDisplayName = x.Account.Account.DisplayName,
                                AccountPin = x.Account.Account.AccessPin,
                                LoanPin = x.LoanPin,
                                AccountEmailAddress = x.Account.EmailAddress,
                                AccountMobileNumber = x.Account.MobileNumber,
                                AccountNumber = x.Account.MobileNumber,
                                Amount = x.Amount,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                LoanStatusId = x.StatusId,
                                IsRedeemed = x.IsRedeemed,
                            }).FirstOrDefault();
                        if (LoanDetails != null)
                        {
                            if (LoanDetails.LoanStatusId != 740)
                            {
                                _UserInfo.Status = ResponseStatus.Error;
                                _UserInfo.Message = CoreResources.HCG179M;
                                _UserInfo.ResponseCode = CoreResources.HCG179;
                                return _UserInfo;
                            }

                            _UserInfo.LoanKey = LoanDetails.LoanKey;
                            _UserInfo.IsRedeemed = LoanDetails.IsRedeemed;
                            _UserInfo.AccountNumber = LoanDetails.AccountNumber;
                            _UserInfo.MobileNumber = LoanDetails.AccountMobileNumber;
                            _UserInfo.DisplayName = LoanDetails.AccountDisplayName;
                            _UserInfo.UserAccountId = (long)LoanDetails.AccountId;
                            _UserInfo.Pin = LoanDetails.LoanPin;
                            _UserInfo.EmailAddress = LoanDetails.AccountEmailAddress;
                            _UserInfo.AccountStatusId = (long)LoanDetails.AccountStatusId;
                            //if (LoanDetails.OwnerId != null)
                            //{
                            //    _UserInfo.IssuerId = (long)LoanDetails.OwnerId;
                            //    _UserInfo.IssuerAccountTypeId = (long)LoanDetails.OwnerAccountTypeId;
                            //}
                            //if (LoanDetails.CreatedById != null)
                            //{
                            //    if (LoanDetails.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                            //    {
                            //        _UserInfo.IssuerId = _HCoreContext.HCUAccount.Where(x => x.Id == LoanDetails.CreatedById).Select(x => x.Owner.OwnerId).FirstOrDefault() ?? 0;
                            //        _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            //    }
                            //    if (LoanDetails.CreatedByAccountTypeId == UserAccountType.Acquirer)
                            //    {
                            //        _UserInfo.IssuerId = (long)LoanDetails.CreatedById;
                            //        _UserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                            //    }
                            //}
                            //if (_UserInfo.IssuerId == 0)
                            //{
                            //    _UserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                            //    _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            //}
                            _UserInfo.Status = ResponseStatus.Success;
                            _UserInfo.Message = CoreResources.HCG180M;
                            _UserInfo.ResponseCode = CoreResources.HCG180;
                            return _UserInfo;
                        }
                        else
                        {
                            _UserInfo.Status = ResponseStatus.Error;
                            _UserInfo.Message = CoreResources.HCG191M;
                            _UserInfo.ResponseCode = CoreResources.HCG191;
                            return _UserInfo;
                        }
                    }
                    string AppUserCardId = _Request.MobileNumber;
                    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, (int)_Request.UserReference.CountryMobileNumberLength);
                    string AppUserLoginId = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    var CustomerDataStore = DataStore.DataStore.Customers.Where(x => x.Username == AppUserLoginId || x.AccountNumber == AppUserCardId).FirstOrDefault();
                    if (CustomerDataStore != null)
                    {
                        _UserInfo.UserAccountId = CustomerDataStore.AccountId;
                        _UserInfo.UserAccountKey = CustomerDataStore.AccountKey;
                        _UserInfo.DisplayName = CustomerDataStore.DisplayName;
                        _UserInfo.Name = CustomerDataStore.Name;
                        _UserInfo.MobileNumber = CustomerDataStore.MobileNumber;
                        _UserInfo.EmailAddress = CustomerDataStore.EmailAddress;
                        _UserInfo.Gender = CustomerDataStore.Gender;
                        _UserInfo.AccountNumber = CustomerDataStore.AccountNumber;
                        _UserInfo.Pin = CustomerDataStore.Pin;
                        _UserInfo.AccountStatusId = CustomerDataStore.StatusId;
                        if (CustomerDataStore.OwnerId != null)
                        {
                            _UserInfo.IssuerId = (long)CustomerDataStore.OwnerId;
                            _UserInfo.IssuerAccountTypeId = (long)CustomerDataStore.OwnerAccountTypeId;
                        }
                        if (CustomerDataStore.CreatedById != null)
                        {
                            if (CustomerDataStore.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                            {
                                long TIssuer = DataStore.DataStore.Cashiers.Where(x => x.ReferenceId == CustomerDataStore.CreatedById).Select(x => x.MerchantId).FirstOrDefault() ?? 0;
                                if (TIssuer > 0)
                                {
                                    _UserInfo.IssuerId = TIssuer;
                                }
                                _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            }
                            if (CustomerDataStore.CreatedByAccountTypeId == UserAccountType.Acquirer)
                            {
                                _UserInfo.IssuerId = (long)CustomerDataStore.CreatedById;
                                _UserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                            }
                        }
                        if (_UserInfo.IssuerId == 0)
                        {
                            _UserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                            _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                        }
                        _UserInfo.Status = ResponseStatus.Success;
                        _UserInfo.Message = CoreResources.HCG167M;
                        _UserInfo.ResponseCode = CoreResources.HCG167;
                        return _UserInfo;
                    }
                    TCustAccount? UserAccount = _HCoreContext.HCUAccount
                                                   .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                                    && (x.User.Username == AppUserLoginId || x.AccountCode == AppUserCardId))
                                                       .Select(x => new TCustAccount
                                                       {
                                                           UserName = AppUserLoginId,
                                                           AccountId = x.Id,
                                                           AccountKey = x.Guid,
                                                           DisplayName = x.DisplayName,
                                                           Name = x.Name,
                                                           GenderId = x.GenderId,
                                                           EmailAddress = x.EmailAddress,
                                                           MobileNumber = x.MobileNumber,
                                                           AccountNumber = x.AccountCode,
                                                           Pin = x.AccessPin,

                                                           OwnerId = x.OwnerId,
                                                           OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                           SubOwnerId = x.SubOwnerId,
                                                           CreateDate = x.CreateDate,
                                                           CreatedById = x.CreatedById,
                                                           CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,

                                                           StatusId = x.StatusId,
                                                       }).FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (UserAccount != null)
                    {
                        string? GenderName = null;
                        switch (UserAccount.GenderId)
                        {
                            case 103:
                                GenderName = "Male";
                                break;
                            case 104:
                                GenderName = "Female";
                                break;
                            case 105:
                                GenderName = "Other";
                                break;
                        }
                        DataStore.DataStore.Customers.Add(new ODataStore.Account.Customer
                        {
                            Username = AppUserLoginId,
                            AccountId = UserAccount.AccountId,
                            AccountKey = UserAccount.AccountKey,
                            DisplayName = UserAccount.DisplayName,
                            Name = UserAccount.Name,
                            Gender = GenderName,
                            EmailAddress = UserAccount.EmailAddress,
                            MobileNumber = UserAccount.MobileNumber,
                            AccountNumber = UserAccount.AccountNumber,
                            Pin = UserAccount.Pin,

                            OwnerId = UserAccount.OwnerId,
                            OwnerAccountTypeId = UserAccount.OwnerAccountTypeId,
                            SubOwnerId = UserAccount.SubOwnerId,
                            CreateDate = UserAccount.CreateDate,
                            CreatedById = UserAccount.CreatedById,
                            CreatedByAccountTypeId = UserAccount.CreatedByAccountTypeId,

                            StatusId = UserAccount.StatusId,
                            SyncDate = HCoreHelper.GetGMTDateTime(),
                        });
                        _UserInfo.UserAccountId = UserAccount.AccountId;
                        _UserInfo.UserAccountKey = UserAccount.AccountKey;

                        _UserInfo.DisplayName = UserAccount.DisplayName;
                        _UserInfo.Name = UserAccount.Name;
                        _UserInfo.MobileNumber = UserAccount.MobileNumber;
                        _UserInfo.EmailAddress = UserAccount.EmailAddress;
                        _UserInfo.Gender = GenderName;
                        _UserInfo.AccountNumber = UserAccount.AccountNumber;
                        _UserInfo.Pin = UserAccount.Pin;
                        _UserInfo.AccountStatusId = UserAccount.StatusId;
                        if (UserAccount.OwnerId != null)
                        {
                            _UserInfo.IssuerId = (long)UserAccount.OwnerId;
                            _UserInfo.IssuerAccountTypeId = (long)UserAccount.OwnerAccountTypeId;
                        }
                        if (UserAccount.CreatedById != null)
                        {
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                            {
                                long TIssuer = DataStore.DataStore.Cashiers.Where(x => x.ReferenceId == UserAccount.CreatedById).Select(x => x.MerchantId).FirstOrDefault() ?? 0;
                                if (TIssuer > 0)
                                {
                                    _UserInfo.IssuerId = TIssuer;
                                }
                                _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                            }
                            if (UserAccount.CreatedByAccountTypeId == UserAccountType.Acquirer)
                            {
                                _UserInfo.IssuerId = (long)UserAccount.CreatedById;
                                _UserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                            }
                        }
                        if (_UserInfo.IssuerId == 0)
                        {
                            _UserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                            _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                        }
                        _UserInfo.Status = ResponseStatus.Success;
                        _UserInfo.Message = CoreResources.HCG167M;
                        _UserInfo.ResponseCode = CoreResources.HCG167;
                        return _UserInfo;
                    }
                    else
                    {
                        if (CreateUser == true && !AppUserCardId.StartsWith("20"))
                        {
                            _CustomerRequest = new OCustomer.Request();
                            if (_GatewayInfo != null && _GatewayInfo.MerchantId != 0)
                            {
                                _CustomerRequest.OwnerId = _GatewayInfo.MerchantId;
                            }
                            if (_GatewayInfo != null && _GatewayInfo.StoreId != 0)
                            {
                                _CustomerRequest.SubOwnerId = _GatewayInfo.StoreId;
                            }
                            //if (_GatewayInfo != null && _GatewayInfo.TerminalId != 0)
                            //{
                            //    _CustomerRequest.CreatedById = _GatewayInfo.TerminalId;
                            //}
                            _CustomerRequest.MobileNumber = _Request.MobileNumber;
                            _CustomerRequest.DisplayName = _Request.MobileNumber;
                            _CustomerRequest.GenderCode = _Request.Gender;
                            _CustomerRequest.EmailAddress = _Request.EmailAddress;
                            _CustomerRequest.DateOfBirth = _Request.DateOfBirth;
                            _CustomerRequest.UserReference = _Request.UserReference;
                            ManageCoreUserAccess _ManageCoreUserAccess = new ManageCoreUserAccess();
                            OCustomer.Response _AppUserCreateResponse = CreateCustomerAccount(_CustomerRequest);
                            if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                            {
                                DataStore.DataStore.Customers.Add(new ODataStore.Account.Customer
                                {
                                    Username = AppUserLoginId,
                                    AccountId = _AppUserCreateResponse.AccountId,
                                    AccountKey = _AppUserCreateResponse.AccountKey,
                                    DisplayName = _AppUserCreateResponse.DisplayName,
                                    Name = _AppUserCreateResponse.Name,
                                    Gender = _AppUserCreateResponse.Gender,
                                    EmailAddress = _AppUserCreateResponse.EmailAddress,
                                    MobileNumber = _AppUserCreateResponse.MobileNumber,
                                    AccountNumber = _AppUserCreateResponse.AccountNumber,
                                    Pin = _AppUserCreateResponse.Pin,

                                    OwnerId = _AppUserCreateResponse.OwnerId,
                                    OwnerAccountTypeId = _AppUserCreateResponse.OwnerAccountTypeId,
                                    SubOwnerId = _AppUserCreateResponse.SubOwnerId,
                                    CreateDate = _AppUserCreateResponse.CreateDate,
                                    CreatedById = _AppUserCreateResponse.CreatedById,
                                    CreatedByAccountTypeId = _AppUserCreateResponse.CreatedByAccountTypeId,
                                    StatusId = _AppUserCreateResponse.StatusId,
                                    SyncDate = HCoreHelper.GetGMTDateTime(),
                                });


                                _UserInfo.UserAccountId = _AppUserCreateResponse.AccountId;
                                _UserInfo.UserAccountKey = _AppUserCreateResponse.AccountKey;
                                _UserInfo.DisplayName = _AppUserCreateResponse.DisplayName;
                                _UserInfo.Name = _AppUserCreateResponse.Name;
                                _UserInfo.MobileNumber = _AppUserCreateResponse.MobileNumber;
                                _UserInfo.EmailAddress = _AppUserCreateResponse.EmailAddress;
                                _UserInfo.Gender = _AppUserCreateResponse.Gender;
                                _UserInfo.AccountNumber = _AppUserCreateResponse.AccountNumber;
                                _UserInfo.Pin = _AppUserCreateResponse.Pin;
                                _UserInfo.AccountStatusId = _AppUserCreateResponse.StatusId;
                                if (_AppUserCreateResponse.OwnerId != null)
                                {
                                    _UserInfo.IssuerId = (long)_AppUserCreateResponse.OwnerId;
                                    _UserInfo.IssuerAccountTypeId = (long)_AppUserCreateResponse.OwnerAccountTypeId;
                                }
                                if (_AppUserCreateResponse.CreatedById != null)
                                {
                                    if (_AppUserCreateResponse.CreatedByAccountTypeId == UserAccountType.MerchantCashier)
                                    {
                                        long TIssuer = DataStore.DataStore.Cashiers.Where(x => x.ReferenceId == _AppUserCreateResponse.CreatedById).Select(x => x.MerchantId).FirstOrDefault() ?? 0;
                                        if (TIssuer > 0)
                                        {
                                            _UserInfo.IssuerId = TIssuer;
                                        }
                                        _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                    }
                                    if (_AppUserCreateResponse.CreatedByAccountTypeId == UserAccountType.Acquirer)
                                    {
                                        _UserInfo.IssuerId = (long)_AppUserCreateResponse.CreatedById;
                                        _UserInfo.IssuerAccountTypeId = UserAccountType.Acquirer;
                                    }
                                }
                                if (_UserInfo.IssuerId == 0)
                                {
                                    _UserInfo.IssuerId = SystemAccounts.ThankUCashMerchant;
                                    _UserInfo.IssuerAccountTypeId = UserAccountType.Merchant;
                                }
                                _UserInfo.Status = ResponseStatus.Success;
                                _UserInfo.Message = CoreResources.HCG167M;
                                _UserInfo.ResponseCode = CoreResources.HCG167;
                                return _UserInfo;
                            }
                            else
                            {
                                _UserInfo = new OUserInfo();
                                _UserInfo.Status = ResponseStatus.Error;
                                _UserInfo.Message = CoreResources.HCG169M;
                                _UserInfo.ResponseCode = CoreResources.HCG169;
                                return _UserInfo;
                            }
                        }
                        else
                        {
                            _UserInfo = new OUserInfo();
                            _UserInfo.Status = ResponseStatus.Error;
                            _UserInfo.Message = CoreResources.HCG169M;
                            _UserInfo.ResponseCode = CoreResources.HCG169;
                            return _UserInfo;
                        }
                    }
                }
            }
            else
            {

                _UserInfo.Status = ResponseStatus.Error;
                _UserInfo.Message = CoreResources.HCG165M;
                _UserInfo.ResponseCode = CoreResources.HCG165;
                return _UserInfo;
            }
        }
        ManageCoreTransaction _ManageCoreTransaction;
        internal OAmountDistribution GetAmountDistribution(OThankUGateway.Request _UserRequest, OGatewayInfo _GatewayInfo, OUserInfo _UserInfo)
        {
            _AmountDistribution = new OAmountDistribution();
            _AmountDistribution.InvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
            _AmountDistribution.ReferenceInvoiceAmount = _AmountDistribution.InvoiceAmount;
            _AmountDistribution.MerchantAmount = _AmountDistribution.InvoiceAmount; ;
            long ThankUCashPlusIsEnable = Convert.ToInt64(GetConfiguration("thankucashplus", _GatewayInfo.MerchantId));
            long TucBlack = Convert.ToInt64(GetConfiguration("thankucashgold", _GatewayInfo.MerchantId));
            if (ThankUCashPlusIsEnable != 0)
            {
                _AmountDistribution.IsThankUCashEnabled = true;
                _AmountDistribution.TransactionSourceId = TransactionSource.ThankUCashPlus;
            }
            if (TucBlack != 0)
            {
                _AmountDistribution.IsThankUCashGold = true;
            }
            switch (_UserRequest.TransactionMode)
            {
                case "cash":
                    _AmountDistribution.TransactionTypeId = TransactionType.CashReward;
                    break;
                case "card":
                    _AmountDistribution.TransactionTypeId = TransactionType.CardReward;
                    break;
                case "redeemreward":
                    _AmountDistribution.TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                    break;
                default:
                    _AmountDistribution.TransactionTypeId = TransactionType.CashReward;
                    if (!string.IsNullOrEmpty(_UserRequest.bin) || !string.IsNullOrEmpty(_UserRequest.SixDigitPan))
                    {
                        _AmountDistribution.TransactionTypeId = TransactionType.CardReward;
                    }
                    break;
            }
            try
            {
                if (_UserRequest.UserReference.AccountTypeId == UserAccountType.Merchant || _UserRequest.UserReference.AccountTypeId == UserAccountType.PosAccount || _UserRequest.UserReference.AccountTypeId == UserAccountType.PgAccount)
                {
                    if (_UserInfo.AccountStatusId == HelperStatus.Default.Active || _UserInfo.AccountStatusId == HelperStatus.Default.Suspended)
                    {
                        bool IsThankUCashCustomer = false;
                        if (_UserInfo.MobileNumber != _UserInfo.DisplayName)
                        {
                            IsThankUCashCustomer = true;
                        }
                        long RewardTucCustomersOnly = Convert.ToInt64(GetConfiguration("tucuserrewardonly", _GatewayInfo.MerchantId));
                        if (RewardTucCustomersOnly == 1 && IsThankUCashCustomer == false)
                        {
                            return _AmountDistribution;
                        }
                        double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("rewardpercentage", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                        if (RewardPercentage > 30)
                        {
                            return _AmountDistribution;
                        }
                        double RewardMaxInvoiceAmount = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("rewardmaxinvoiceamount", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                        if (RewardMaxInvoiceAmount > 0)
                        {
                            if (_AmountDistribution.ReferenceInvoiceAmount > RewardMaxInvoiceAmount)
                            {
                                _AmountDistribution.ReferenceInvoiceAmount = RewardMaxInvoiceAmount;
                            }
                        }
                        double RewardAmount = HCoreHelper.GetPercentage(_AmountDistribution.ReferenceInvoiceAmount, RewardPercentage, _AppConfig.SystemEntryRoundDouble);
                        if (_UserRequest.SProducts != null && _UserRequest.SProducts.Count() > 0)
                        {
                            double ProductRewardAmount = 0;
                            foreach (var Product in _UserRequest.SProducts)
                            {
                                double ProductRewardPercentage = DataStore.DataStore.TerminalProducts.Where(x => x.AccountId == _GatewayInfo.MerchantId && x.ReferenceNumber == Product._id).Select(x => x.RewardPercentage).FirstOrDefault();
                                if (ProductRewardPercentage > 0)
                                {
                                    ProductRewardAmount += HCoreHelper.GetPercentage(Product.price, ProductRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                }
                            }
                            if (ProductRewardAmount > _AmountDistribution.ReferenceInvoiceAmount)
                            {
                                RewardAmount = 0;
                            }
                            if (ProductRewardAmount > 0)
                            {
                                RewardAmount = ProductRewardAmount;
                            }
                        }
                        if (_UserRequest.Items != null && _UserRequest.Items.Count() > 0)
                        {
                            double ProductRewardAmount = 0;
                            foreach (var Product in _UserRequest.Items)
                            {
                                double ProductRewardPercentage = DataStore.DataStore.TerminalProducts.Where(x => x.AccountId == _GatewayInfo.MerchantId && x.ReferenceNumber == Product.ReferenceId).Select(x => x.RewardPercentage).FirstOrDefault();
                                if (ProductRewardPercentage > 0)
                                {
                                    ProductRewardAmount += HCoreHelper.GetPercentage(Product.Price, ProductRewardPercentage, _AppConfig.SystemEntryRoundDouble);
                                }
                            }
                            if (ProductRewardAmount > _AmountDistribution.ReferenceInvoiceAmount)
                            {
                                RewardAmount = 0;
                            }
                            if (ProductRewardAmount > 0)
                            {
                                RewardAmount = ProductRewardAmount;
                            }
                        }
                        if (_AmountDistribution.IsThankUCashEnabled)
                        {
                            double CriteriaValue = 0;
                            OConfigurationInfo RewardCriteriaDetails = GetConfigurationDetails("thankucashplusrewardcriteria", _GatewayInfo.MerchantId);
                            if (RewardCriteriaDetails != null)
                            {
                                if (!string.IsNullOrEmpty(RewardCriteriaDetails.Value))
                                {
                                    CriteriaValue = Convert.ToDouble(RewardCriteriaDetails.Value);
                                }

                                if (RewardCriteriaDetails.TypeCode == "rewardcriteriatype.mininvoice" && _AmountDistribution.ReferenceInvoiceAmount < CriteriaValue)
                                {
                                    return _AmountDistribution;
                                }
                                else if (RewardCriteriaDetails.TypeCode == "rewardcriteriatype.multipleofamount")
                                {
                                    if (_AmountDistribution.ReferenceInvoiceAmount < CriteriaValue)
                                    {
                                        return _AmountDistribution;
                                    }
                                    long RewardSlot = (long)(_AmountDistribution.ReferenceInvoiceAmount / CriteriaValue);
                                    double CriteriaRewardAmount = HCoreHelper.GetPercentage(CriteriaValue, RewardPercentage, 2);
                                    RewardAmount = CriteriaRewardAmount * RewardSlot;
                                }
                            }
                            else
                            {
                                return _AmountDistribution;
                            }
                        }
                        if (_GatewayInfo.StoreId != 0 && _GatewayInfo.StoreStatusId != HelperStatus.Default.Active)
                        {
                            RewardAmount = 0;
                        }
                        if (RewardAmount > 0)
                        {
                            int ThankUCashPercentageFromRewardAmount = Convert.ToInt32(GetConfiguration("rewardcommissionfromrewardamount", _GatewayInfo.MerchantId));
                            if (ThankUCashPercentageFromRewardAmount > 0)
                            {
                                _AmountDistribution.ThankUCashPercentageFromRewardAmount = true;
                            }
                            else
                            {
                                _AmountDistribution.ThankUCashPercentageFromRewardAmount = false;
                            }
                            if (_AmountDistribution.ThankUCashPercentageFromRewardAmount == true)
                            {
                                //double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
                                //double SystemDefaultCommissionPercentage =30;
                                //double SystemCategoryCommissionPercentage = 0;
                                //if (_GatewayInfo.MerchantPrimaryCategoryId != null && _GatewayInfo.MerchantPrimaryCategoryId > 0)
                                //{
                                //    using (_HCoreContext = new HCoreContext())
                                //    {
                                //        var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == _GatewayInfo.MerchantPrimaryCategoryId)
                                //            .Select(x => new
                                //            {
                                //                Commission = x.Commission,
                                //                CommissionCap = 5000,
                                //            }).FirstOrDefault();
                                //        _HCoreContext.Dispose();
                                //        if (CategoryCommission != null && CategoryCommission.Commission > 0)
                                //        {
                                //            SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                                //        }
                                //    }
                                //}

                                //double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("rewardcomissionpercentage", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                                //if (SystemMerchantCommissionPercentage > 0)
                                //{
                                //    _AmountDistribution.ThankUCashPercentage = SystemMerchantCommissionPercentage;
                                //}
                                //else
                                //{
                                //    _AmountDistribution.ThankUCashPercentage = SystemDefaultCommissionPercentage;
                                //}


                                _AmountDistribution.UserPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("userrewardpercentage", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                                _AmountDistribution.ThankUCashPercentage = 100 - _AmountDistribution.UserPercentage;
                                //_AmountDistribution.UserPercentage = 100 - _AmountDistribution.ThankUCashPercentage;
                                _AmountDistribution.PtsaPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("ptsapercentage", SystemAccounts.SmashLabId)), _AppConfig.SystemRoundPercentage);
                                _AmountDistribution.AcquirerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("acquirerpercentage", (long)_GatewayInfo.AcquirerId)), _AppConfig.SystemRoundPercentage);
                                _AmountDistribution.IssuerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("issuerpercentage", _UserInfo.IssuerId)), _AppConfig.SystemRoundPercentage);
                                _AmountDistribution.TransactionIssuerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("transactionissuerpercentage", _GatewayInfo.TransactionIssuerId)), _AppConfig.SystemRoundPercentage);
                                if (_UserRequest.UserReference.AccountTypeId == UserAccountType.PgAccount)
                                {
                                    _AmountDistribution.PsspPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("pgpercentage", _UserRequest.UserReference.AccountId)), _AppConfig.SystemRoundPercentage);
                                }
                                else if (_UserRequest.UserReference.AccountTypeId == UserAccountType.PosAccount)
                                {
                                    _AmountDistribution.PtspPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("pospercentage", _UserRequest.UserReference.AccountId)), _AppConfig.SystemRoundPercentage);
                                    _AmountDistribution.AllowAcquirerSettlement = GetConfiguration("banksettlementbyptsp", _UserRequest.UserReference.AccountId);
                                }
                                OConfigurationInfo RewardDeductionType = GetConfigurationDetails("rewarddeductiontype", _GatewayInfo.MerchantId);
                                double TransactionIssuerCharge = 0;
                                double TransactionIssuerTotalAmount = 0;
                                double ThankUCashAmount = RewardAmount;
                                if (_AmountDistribution.UserPercentage > 0 && _UserInfo.UserAccountId > 0)
                                {
                                    _AmountDistribution.User = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.UserPercentage, _AppConfig.SystemEntryRoundDouble);
                                    ThankUCashAmount = ThankUCashAmount - _AmountDistribution.User;
                                }
                                if (_AmountDistribution.PsspPercentage > 0 && _GatewayInfo.PsspId > 0)
                                {
                                    _AmountDistribution.Pssp = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.PsspPercentage, _AppConfig.SystemEntryRoundDouble);
                                    ThankUCashAmount = ThankUCashAmount - _AmountDistribution.Pssp;
                                }
                                if (_AmountDistribution.PtspPercentage > 0 && _GatewayInfo.PtspId > 0)
                                {
                                    _AmountDistribution.Ptsp = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.PtspPercentage, _AppConfig.SystemEntryRoundDouble);
                                    ThankUCashAmount = ThankUCashAmount - _AmountDistribution.Ptsp;
                                }
                                if (_AmountDistribution.AcquirerPercentage > 0 && _GatewayInfo.AcquirerId > 0)
                                {
                                    _AmountDistribution.Acquirer = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.AcquirerPercentage, _AppConfig.SystemEntryRoundDouble);
                                    ThankUCashAmount = ThankUCashAmount - _AmountDistribution.Acquirer;
                                }
                                if (_AmountDistribution.PtsaPercentage > 0)
                                {
                                    _AmountDistribution.Ptsp = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.PtsaPercentage, _AppConfig.SystemEntryRoundDouble);
                                    ThankUCashAmount = ThankUCashAmount - _AmountDistribution.Ptsp;
                                }
                                if (_AmountDistribution.IssuerPercentage > 0 && _UserInfo.IssuerId != 0)
                                {
                                    _AmountDistribution.Issuer = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.IssuerPercentage, _AppConfig.SystemEntryRoundDouble);
                                    ThankUCashAmount = ThankUCashAmount - _AmountDistribution.Issuer;
                                }
                                if (_AmountDistribution.TransactionIssuerPercentage > 0 && _GatewayInfo.TransactionIssuerId > 0)
                                {
                                    TransactionIssuerTotalAmount = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.TransactionIssuerPercentage, _AppConfig.SystemEntryRoundDouble);
                                    ThankUCashAmount = ThankUCashAmount - _AmountDistribution.TransactionIssuerAmount;
                                    _AmountDistribution.TransactionIssuerAmount = HCoreHelper.GetPercentage(TransactionIssuerTotalAmount, 40, _AppConfig.SystemEntryRoundDouble);
                                    TransactionIssuerCharge = TransactionIssuerTotalAmount - _AmountDistribution.TransactionIssuerAmount;
                                }
                                ThankUCashAmount = HCoreHelper.RoundNumber(ThankUCashAmount, _AppConfig.SystemEntryRoundDouble);
                                double ComissionAmount = HCoreHelper.RoundNumber((RewardAmount - _AmountDistribution.User), _AppConfig.SystemEntryRoundDouble);
                                if (_AmountDistribution.Issuer < 0)
                                {
                                    _AmountDistribution.Issuer = 0;
                                }
                                if (_AmountDistribution.TransactionIssuerAmount < 0)
                                {
                                    _AmountDistribution.TransactionIssuerAmount = 0;
                                }
                                if (TransactionIssuerCharge < 0)
                                {
                                    TransactionIssuerCharge = 0;
                                }
                                if (TransactionIssuerTotalAmount < 0)
                                {
                                    TransactionIssuerTotalAmount = 0;
                                }
                                if (ThankUCashAmount < 0)
                                {
                                    ThankUCashAmount = 0;
                                }
                                _AmountDistribution.RewardPercentage = RewardPercentage;

                                _AmountDistribution.TUCRewardAmount = RewardAmount;
                                _AmountDistribution.TUCRewardCommissionAmount = ComissionAmount;
                                _AmountDistribution.TUCRewardUserAmount = _AmountDistribution.User;

                                _AmountDistribution.MerchantAmount = (_AmountDistribution.InvoiceAmount - RewardAmount);
                                _AmountDistribution.User = _AmountDistribution.User;
                                _AmountDistribution.UserPercentage = _AmountDistribution.UserPercentage;
                                _AmountDistribution.Ptsp = _AmountDistribution.Ptsp;
                                _AmountDistribution.PtspPercentage = _AmountDistribution.PtspPercentage;
                                _AmountDistribution.Pssp = _AmountDistribution.Pssp;
                                _AmountDistribution.PsspPercentage = _AmountDistribution.PsspPercentage;
                                _AmountDistribution.Ptsa = _AmountDistribution.Ptsa;
                                _AmountDistribution.Acquirer = _AmountDistribution.Acquirer;
                                _AmountDistribution.Issuer = _AmountDistribution.Issuer;
                                _AmountDistribution.TransactionIssuerAmount = _AmountDistribution.TransactionIssuerAmount;
                                _AmountDistribution.TransactionIssuerCharge = TransactionIssuerCharge;
                                _AmountDistribution.TransactionIssuerTotalAmount = TransactionIssuerTotalAmount;
                                _AmountDistribution.ThankUCash = ThankUCashAmount;
                                _AmountDistribution.ThankUCashPercentage = HCoreHelper.GetAmountPercentage(ThankUCashAmount, ComissionAmount);
                                if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepayandpostpay")
                                {
                                    _AmountDistribution.MerchantReverseAmount = _AmountDistribution.User;
                                }
                                if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                                {
                                    double Balance = (double)DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _GatewayInfo.MerchantId).Select(x => x.Balance).FirstOrDefault();
                                    //if (RewardAmount > Balance)
                                    //{
                                    //    _AmountDistribution.IsMerchantLowBalance = true;
                                    //}
                                    //_ManageCoreTransaction = new ManageCoreTransaction();
                                    //using (_HCoreContext = new HCoreContext())
                                    //{
                                    //    Balance = _HCoreContext.HCUAccountBalance.Where(x => x.AccountId == _GatewayInfo.MerchantId && x.SourceId == TransactionSource.Merchant && x.ParentId == 1).Select(x => x.Balance).FirstOrDefault();
                                    //    _HCoreContext.Dispose();
                                    //}
                                    //double Balance = (double)DataStore.DataStore.Merchants.Where(x=>x.ReferenceId == _GatewayInfo.MerchantId).Select(x=>x.Balance).FirstOrDefault();
                                    if ((RewardAmount > Balance) && _AmountDistribution.IsThankUCashGold == false)
                                    {
                                        //Call Actor
                                        OPendingRewardActorItem _OPendingRewardActorItem = new OPendingRewardActorItem();
                                        _OPendingRewardActorItem.AmountDistribution = _AmountDistribution;
                                        _OPendingRewardActorItem.ExternalRequest = _UserRequest;
                                        _OPendingRewardActorItem.GatewayInfo = _GatewayInfo;
                                        _OPendingRewardActorItem.UserInfo = _UserInfo;
                                        var _PendingRewardCreateActorActor = ActorSystem.Create("PendingRewardCreateActor");
                                        var _PendingRewardCreateActorActorNotify = _PendingRewardCreateActorActor.ActorOf<PendingRewardCreateActor>("PendingRewardCreateActor");
                                        _PendingRewardCreateActorActorNotify.Tell(_OPendingRewardActorItem);

                                        _AmountDistribution = new OAmountDistribution();
                                        _AmountDistribution.ReferenceInvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                                        _AmountDistribution.InvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                                        _AmountDistribution.RewardPercentage = 0;
                                        _AmountDistribution.TUCRewardUserAmount = 0;
                                        _AmountDistribution.TUCRewardCommissionAmount = 0;
                                        _AmountDistribution.TUCRewardAmount = 0;
                                        _AmountDistribution.MerchantAmount = 0;
                                        _AmountDistribution.User = 0;
                                        _AmountDistribution.UserPercentage = 0;
                                        _AmountDistribution.Ptsp = 0;
                                        _AmountDistribution.PtspPercentage = 0;
                                        _AmountDistribution.Pssp = 0;
                                        _AmountDistribution.PsspPercentage = 0;
                                        _AmountDistribution.Ptsa = 0;
                                        _AmountDistribution.PtsaPercentage = 0;
                                        _AmountDistribution.Acquirer = 0;
                                        _AmountDistribution.AcquirerPercentage = 0;
                                        _AmountDistribution.Issuer = 0;
                                        _AmountDistribution.IssuerPercentage = 0;
                                        _AmountDistribution.TransactionIssuerAmount = 0;
                                        _AmountDistribution.TransactionIssuerCharge = 0;
                                        _AmountDistribution.TransactionIssuerTotalAmount = 0;
                                        _AmountDistribution.TransactionIssuerPercentage = 0;
                                        _AmountDistribution.ThankUCash = 0;
                                        _AmountDistribution.ThankUCashPercentage = 0;
                                        return _AmountDistribution;
                                    }
                                }
                                return _AmountDistribution;
                            }
                            else
                            {
                                double MaximumCommissionAmount = 2500;
                                double SystemDefaultCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("rewardcomissionpercentage")), _AppConfig.SystemRoundPercentage);
                                double SystemCategoryCommissionPercentage = 0;
                                if (_GatewayInfo.MerchantPrimaryCategoryId != null && _GatewayInfo.MerchantPrimaryCategoryId > 0)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var CategoryCommission = _HCoreContext.TUCMerchantCategory.Where(x => x.RootCategoryId == _GatewayInfo.MerchantPrimaryCategoryId)
                                            .Select(x => new
                                            {
                                                Commission = x.Commission,
                                            }).FirstOrDefault();
                                        _HCoreContext.Dispose();
                                        if (CategoryCommission != null && CategoryCommission.Commission > 0)
                                        {
                                            SystemCategoryCommissionPercentage = (double)CategoryCommission.Commission;
                                        }
                                    }
                                }
                                double SystemMerchantCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetAccountConfiguration("rewardcomissionpercentage", _GatewayInfo.MerchantId)), _AppConfig.SystemRoundPercentage);
                                if (SystemMerchantCommissionPercentage > 0)
                                {
                                    _AmountDistribution.ThankUCashPercentage = SystemMerchantCommissionPercentage;
                                }
                                else if (SystemCategoryCommissionPercentage > 0)
                                {
                                    _AmountDistribution.ThankUCashPercentage = SystemCategoryCommissionPercentage;
                                }
                                else
                                {
                                    _AmountDistribution.ThankUCashPercentage = SystemDefaultCommissionPercentage;
                                }
                                if (_AmountDistribution.IsThankUCashGold == true)
                                {
                                    _AmountDistribution.ThankUCashPercentage = 0;
                                }
                                _AmountDistribution.ThankUCash = HCoreHelper.GetPercentage(_AmountDistribution.InvoiceAmount, _AmountDistribution.ThankUCashPercentage);
                                if (_AmountDistribution.ThankUCash > MaximumCommissionAmount)
                                {
                                    _AmountDistribution.ThankUCash = MaximumCommissionAmount;
                                }

                                _AmountDistribution.TUCRewardCommissionAmount = _AmountDistribution.ThankUCash;
                                _AmountDistribution.TUCRewardUserAmount = RewardAmount;
                                _AmountDistribution.TUCRewardAmount = _AmountDistribution.TUCRewardCommissionAmount + _AmountDistribution.TUCRewardUserAmount;
                                _AmountDistribution.UserPercentage = 100;
                                _AmountDistribution.User = RewardAmount;
                                double ComissionAmount = _AmountDistribution.ThankUCash;
                                _AmountDistribution.PtsaPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("ptsapercentage", SystemAccounts.SmashLabId)), _AppConfig.SystemRoundPercentage);
                                _AmountDistribution.AcquirerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("acquirerpercentage", (long)_GatewayInfo.AcquirerId)), _AppConfig.SystemRoundPercentage);
                                _AmountDistribution.IssuerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("issuerpercentage", _UserInfo.IssuerId)), _AppConfig.SystemRoundPercentage);
                                _AmountDistribution.TransactionIssuerPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("transactionissuerpercentage", _GatewayInfo.TransactionIssuerId)), _AppConfig.SystemRoundPercentage);
                                if (_UserRequest.UserReference.AccountTypeId == UserAccountType.PgAccount)
                                {
                                    _AmountDistribution.PsspPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("pgpercentage", _UserRequest.UserReference.AccountId)), _AppConfig.SystemRoundPercentage);
                                }
                                else if (_UserRequest.UserReference.AccountTypeId == UserAccountType.PosAccount)
                                {
                                    _AmountDistribution.PtspPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(GetConfiguration("pospercentage", _UserRequest.UserReference.AccountId)), _AppConfig.SystemRoundPercentage);
                                    _AmountDistribution.AllowAcquirerSettlement = GetConfiguration("banksettlementbyptsp", _UserRequest.UserReference.AccountId);
                                }
                                OConfigurationInfo RewardDeductionType = GetConfigurationDetails("rewarddeductiontype", _GatewayInfo.MerchantId);
                                double TransactionIssuerCharge = 0;
                                double TransactionIssuerTotalAmount = 0;
                                if (_AmountDistribution.PsspPercentage > 0 && _GatewayInfo.PsspId > 0)
                                {
                                    _AmountDistribution.Pssp = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.PsspPercentage, _AppConfig.SystemEntryRoundDouble);
                                    _AmountDistribution.ThankUCash = _AmountDistribution.ThankUCash - _AmountDistribution.Pssp;
                                }
                                if (_AmountDistribution.PtspPercentage > 0 && _GatewayInfo.PtspId > 0)
                                {
                                    _AmountDistribution.Ptsp = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.PtspPercentage, _AppConfig.SystemEntryRoundDouble);
                                    _AmountDistribution.ThankUCash = _AmountDistribution.ThankUCash - _AmountDistribution.Ptsp;
                                }
                                if (_AmountDistribution.AcquirerPercentage > 0 && _GatewayInfo.AcquirerId > 0)
                                {
                                    _AmountDistribution.Acquirer = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.AcquirerPercentage, _AppConfig.SystemEntryRoundDouble);
                                    _AmountDistribution.ThankUCash = _AmountDistribution.ThankUCash - _AmountDistribution.Acquirer;
                                }
                                if (_AmountDistribution.PtsaPercentage > 0)
                                {
                                    _AmountDistribution.Ptsp = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.PtsaPercentage, _AppConfig.SystemEntryRoundDouble);
                                    _AmountDistribution.ThankUCash = _AmountDistribution.ThankUCash - _AmountDistribution.Ptsp;
                                }
                                if (_AmountDistribution.IssuerPercentage > 0 && _UserInfo.IssuerId != 0)
                                {
                                    _AmountDistribution.Issuer = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.IssuerPercentage, _AppConfig.SystemEntryRoundDouble);
                                    _AmountDistribution.ThankUCash = _AmountDistribution.ThankUCash - _AmountDistribution.Issuer;
                                }
                                if (_AmountDistribution.TransactionIssuerPercentage > 0 && _GatewayInfo.TransactionIssuerId > 0)
                                {
                                    TransactionIssuerTotalAmount = HCoreHelper.GetPercentage(RewardAmount, _AmountDistribution.TransactionIssuerPercentage, _AppConfig.SystemEntryRoundDouble);
                                    _AmountDistribution.ThankUCash = _AmountDistribution.ThankUCash - _AmountDistribution.TransactionIssuerAmount;
                                    _AmountDistribution.TransactionIssuerAmount = HCoreHelper.GetPercentage(TransactionIssuerTotalAmount, 40, _AppConfig.SystemEntryRoundDouble);
                                    TransactionIssuerCharge = TransactionIssuerTotalAmount - _AmountDistribution.TransactionIssuerAmount;
                                }
                                if (_AmountDistribution.Issuer < 0)
                                {
                                    _AmountDistribution.Issuer = 0;
                                }
                                if (_AmountDistribution.TransactionIssuerAmount < 0)
                                {
                                    _AmountDistribution.TransactionIssuerAmount = 0;
                                }
                                if (TransactionIssuerCharge < 0)
                                {
                                    TransactionIssuerCharge = 0;
                                }
                                if (TransactionIssuerTotalAmount < 0)
                                {
                                    TransactionIssuerTotalAmount = 0;
                                }
                                if (_AmountDistribution.ThankUCash < 0)
                                {
                                    _AmountDistribution.ThankUCash = 0;
                                }
                                _AmountDistribution.RewardPercentage = RewardPercentage;
                                _AmountDistribution.MerchantAmount = (_AmountDistribution.InvoiceAmount - RewardAmount - ComissionAmount);
                                _AmountDistribution.User = _AmountDistribution.User;
                                _AmountDistribution.UserPercentage = _AmountDistribution.UserPercentage;
                                _AmountDistribution.Ptsp = _AmountDistribution.Ptsp;
                                _AmountDistribution.PtspPercentage = _AmountDistribution.PtspPercentage;
                                _AmountDistribution.Pssp = _AmountDistribution.Pssp;
                                _AmountDistribution.PsspPercentage = _AmountDistribution.PsspPercentage;
                                _AmountDistribution.Ptsa = _AmountDistribution.Ptsa;
                                _AmountDistribution.Acquirer = _AmountDistribution.Acquirer;
                                _AmountDistribution.Issuer = _AmountDistribution.Issuer;
                                _AmountDistribution.TransactionIssuerAmount = _AmountDistribution.TransactionIssuerAmount;
                                _AmountDistribution.TransactionIssuerCharge = TransactionIssuerCharge;
                                _AmountDistribution.TransactionIssuerTotalAmount = TransactionIssuerTotalAmount;
                                //_AmountDistribution.ThankUCash = ThankUCashAmount;
                                //_AmountDistribution.ThankUCashPercentage = HCoreHelper.GetAmountPercentage(ThankUCashAmount, ComissionAmount);
                                //if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepayandpostpay")
                                //{
                                //    _AmountDistribution.MerchantReverseAmount = _AmountDistribution.User + ComissionAmount;
                                //}
                                //if (RewardDeductionType.TypeCode == "rewarddeductiontype.prepay")
                                //{
                                //double Balance = (double)DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _GatewayInfo.MerchantId).Select(x => x.Balance).FirstOrDefault();
                                //if (RewardAmount > Balance)
                                //{
                                //    _AmountDistribution.IsMerchantLowBalance = true;
                                //}
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                if (_AmountDistribution.IsThankUCashGold == true)
                                {
                                    _AmountDistribution.TransactionStatus = HelperStatus.Transaction.Success;
                                }
                                else
                                {
                                    double MerchantBalance = _ManageCoreTransaction.GetMerchantBalance(_GatewayInfo.MerchantId, TransactionSource.Merchant);
                                    _AmountDistribution.TransactionStatus = HelperStatus.Transaction.Success;
                                    if (MerchantBalance > 0)
                                    {
                                        if (_AmountDistribution.TUCRewardAmount <= MerchantBalance)
                                        {
                                            _AmountDistribution.TransactionStatus = HelperStatus.Transaction.Success;
                                        }
                                        else
                                        {
                                            _AmountDistribution.TransactionStatus = HelperStatus.Transaction.Pending;
                                        }
                                    }
                                    else
                                    {
                                        if (MerchantBalance > -5000)
                                        {
                                            _AmountDistribution.TransactionStatus = HelperStatus.Transaction.Pending;
                                        }
                                        else
                                        {
                                            _AmountDistribution = new OAmountDistribution();
                                            _AmountDistribution.ReferenceInvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                                            _AmountDistribution.InvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                                            _AmountDistribution.RewardPercentage = 0;
                                            _AmountDistribution.TUCRewardAmount = 0;
                                            _AmountDistribution.TUCRewardCommissionAmount = 0;
                                            _AmountDistribution.MerchantAmount = 0;
                                            _AmountDistribution.User = 0;
                                            _AmountDistribution.UserPercentage = 0;
                                            _AmountDistribution.Ptsp = 0;
                                            _AmountDistribution.PtspPercentage = 0;
                                            _AmountDistribution.Pssp = 0;
                                            _AmountDistribution.PsspPercentage = 0;
                                            _AmountDistribution.Ptsa = 0;
                                            _AmountDistribution.PtsaPercentage = 0;
                                            _AmountDistribution.Acquirer = 0;
                                            _AmountDistribution.AcquirerPercentage = 0;
                                            _AmountDistribution.Issuer = 0;
                                            _AmountDistribution.IssuerPercentage = 0;
                                            _AmountDistribution.TransactionIssuerAmount = 0;
                                            _AmountDistribution.TransactionIssuerCharge = 0;
                                            _AmountDistribution.TransactionIssuerTotalAmount = 0;
                                            _AmountDistribution.TransactionIssuerPercentage = 0;
                                            _AmountDistribution.ThankUCash = 0;
                                            _AmountDistribution.ThankUCashPercentage = 0;
                                            _AmountDistribution.TransactionStatus = HelperStatus.Transaction.Success;
                                            return _AmountDistribution;
                                        }
                                    }
                                }



                                //using (_HCoreContext = new HCoreContext())
                                //{
                                //    Balance = _HCoreContext.HCUAccountBalance.Where(x => x.AccountId == _GatewayInfo.MerchantId && x.SourceId == TransactionSource.Merchant && x.ParentId == 1).Select(x => x.Balance).FirstOrDefault();
                                //    _HCoreContext.Dispose();
                                //}
                                //double Balance = (double)DataStore.DataStore.Merchants.Where(x=>x.ReferenceId == _GatewayInfo.MerchantId).Select(x=>x.Balance).FirstOrDefault();
                                //if (RewardAmount > Balance)
                                //{
                                //    //Call Actor
                                //    OPendingRewardActorItem _OPendingRewardActorItem = new OPendingRewardActorItem();
                                //    _OPendingRewardActorItem.AmountDistribution = _AmountDistribution;
                                //    _OPendingRewardActorItem.ExternalRequest = _UserRequest;
                                //    _OPendingRewardActorItem.GatewayInfo = _GatewayInfo;
                                //    _OPendingRewardActorItem.UserInfo = _UserInfo;
                                //    var _PendingRewardCreateActorActor = ActorSystem.Create("PendingRewardCreateActor");
                                //    var _PendingRewardCreateActorActorNotify = _PendingRewardCreateActorActor.ActorOf<PendingRewardCreateActor>("PendingRewardCreateActor");
                                //    _PendingRewardCreateActorActorNotify.Tell(_OPendingRewardActorItem);
                                //    _AmountDistribution = new OAmountDistribution();
                                //    _AmountDistribution.ReferenceInvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                                //    _AmountDistribution.InvoiceAmount = (double)_UserRequest.InvoiceAmount / 100;
                                //    _AmountDistribution.RewardPercentage = 0;
                                //    _AmountDistribution.TUCRewardAmount = 0;
                                //    _AmountDistribution.TUCRewardCommissionAmount = 0;
                                //    _AmountDistribution.MerchantAmount = 0;
                                //    _AmountDistribution.User = 0;
                                //    _AmountDistribution.UserPercentage = 0;
                                //    _AmountDistribution.Ptsp = 0;
                                //    _AmountDistribution.PtspPercentage = 0;
                                //    _AmountDistribution.Pssp = 0;
                                //    _AmountDistribution.PsspPercentage = 0;
                                //    _AmountDistribution.Ptsa = 0;
                                //    _AmountDistribution.PtsaPercentage = 0;
                                //    _AmountDistribution.Acquirer = 0;
                                //    _AmountDistribution.AcquirerPercentage = 0;
                                //    _AmountDistribution.Issuer = 0;
                                //    _AmountDistribution.IssuerPercentage = 0;
                                //    _AmountDistribution.TransactionIssuerAmount = 0;
                                //    _AmountDistribution.TransactionIssuerCharge = 0;
                                //    _AmountDistribution.TransactionIssuerTotalAmount = 0;
                                //    _AmountDistribution.TransactionIssuerPercentage = 0;
                                //    _AmountDistribution.ThankUCash = 0;
                                //    _AmountDistribution.ThankUCashPercentage = 0;
                                //    return _AmountDistribution;
                                //}
                                //}
                                return _AmountDistribution;
                            }
                        }
                        else
                        {
                            return _AmountDistribution;
                        }
                    }
                    else
                    {
                        return _AmountDistribution;
                    }
                }
                else
                {
                    return _AmountDistribution;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAmountDistribution", _Exception);
                return _AmountDistribution;
            }
        }
        internal void ProcessTransactionResponse(OThankUGateway.Request _Request, OGatewayInfo _GatewayInfo, OUserInfo _UserInfo, OAmountDistribution _AmountDistributionReference, string? GroupKey, string? TCode)
        {
            _OPostTransaction = new OPostTransaction();
            _OPostTransaction._Request = _Request;
            _OPostTransaction._GatewayInfo = _GatewayInfo;
            _OPostTransaction._UserInfo = _UserInfo;
            _OPostTransaction._AmountDistributionReference = _AmountDistributionReference;
            _OPostTransaction.GroupKey = GroupKey;
            _OPostTransaction.TCode = TCode;
            var _TransactionPostProcessActor = ActorSystem.Create("ActorRewardPostTransaction");
            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<ActorRewardPostTransaction>("ActorRewardPostTransaction");
            _TransactionPostProcessActorNotify.Tell(_OPostTransaction);
        }
        internal OTransactionValidator ValidateTransaction(long MerchantId, long CustomerId, long StoreId, double InvoiceAmount, long TerminalReferenceId, OUserReference UserReference)
        {
            OTransactionValidator _OTransactionValidator = new OTransactionValidator();
            _OTransactionValidator.IsValid = true;
            _OTransactionValidator.Comment = null;
            // User Suspension Code Removed As Per Discussion With Suraj Sir, Simeon -21 - 06 - 2019 05:01 IST - HARSHAL GANDOLE
            try
            {
                if (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Local)
                {
                    double SecurityMaximumInvoiceAmountPerTransaction = 0;
                    double SecurityMaximumTransactionPerDayPerCustomerCap = 0;
                    double SecurityMaximumInvoiceAmountPerDayPerCustomerCap = 0;
                    string TSecurityMaximumInvoiceAmountPerTransaction = HCoreHelper.GetConfigurationValueByUserAccount("securitymaximuminvoiceamountpertransactioncap", MerchantId, UserReference);
                    string TSecurityMaximumTransactionPerDayCap = HCoreHelper.GetConfigurationValueByUserAccount("securitymaximumtransactionperdaycap", MerchantId, UserReference);
                    string TSecurtiyMaximumInvoiceAmountPerDayCap = HCoreHelper.GetConfigurationValueByUserAccount("securitymaximuminvoiceamountperdaycap", MerchantId, UserReference);
                    try
                    {
                        if (!string.IsNullOrEmpty(TSecurityMaximumInvoiceAmountPerTransaction))
                        {
                            if (!string.IsNullOrEmpty(TSecurityMaximumInvoiceAmountPerTransaction))
                            {
                                SecurityMaximumInvoiceAmountPerTransaction = Convert.ToDouble(TSecurityMaximumInvoiceAmountPerTransaction);
                            }
                        }
                        if (!string.IsNullOrEmpty(TSecurityMaximumTransactionPerDayCap))
                        {
                            //if (TSecurityMaximumTransactionPerDayCap != "0")
                            //{
                            SecurityMaximumTransactionPerDayPerCustomerCap = Convert.ToInt64(TSecurityMaximumTransactionPerDayCap);
                            //}
                        }
                        if (!string.IsNullOrEmpty(TSecurtiyMaximumInvoiceAmountPerDayCap))
                        {
                            //if (TSecurtiyMaximumInvoiceAmountPerDayCap != "0")
                            //{
                            SecurityMaximumInvoiceAmountPerDayPerCustomerCap = Convert.ToDouble(TSecurtiyMaximumInvoiceAmountPerDayCap);
                            //}
                        }
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("ValidateTransactions-CONFIG-CONVERSION-ERROR", _Exception);
                    }
                    DateTime StartDate = HCoreHelper.GetGMTDate();
                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1).AddSeconds(-1);
                    using (_HCoreContext = new HCoreContext())
                    {
                        // Validate Invoice Amount
                        if (SecurityMaximumInvoiceAmountPerTransaction > 0 && InvoiceAmount > SecurityMaximumInvoiceAmountPerTransaction)
                        {
                            _OTransactionValidator.IsValid = false;
                            _OTransactionValidator.Comment = "Invoice amount greater than " + SecurityMaximumInvoiceAmountPerTransaction;
                            return _OTransactionValidator;
                        }
                        // Customer Transactions at Merchant Location
                        if (_OTransactionValidator.IsValid)
                        {
                            long Transactions = _HCoreContext.HCUAccountTransaction
                                                                         .Where(m => m.ParentId == MerchantId
                                                                          && m.AccountId == CustomerId
                                                                          && m.TransactionDate > StartDate
                                                                          && m.TransactionDate < EndDate
                                                                          && m.TypeId != TransactionType.TUCWalletTopup
                                                                          && (((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit) || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                          && m.StatusId == HelperStatus.Transaction.Success
                                                                        ).Count();
                            if (SecurityMaximumTransactionPerDayPerCustomerCap > 0 && Transactions > SecurityMaximumTransactionPerDayPerCustomerCap)
                            {
                                _OTransactionValidator.IsValid = false;
                                _OTransactionValidator.Comment = "Customer transactions exceeded maximum limit of " + SecurityMaximumTransactionPerDayPerCustomerCap;
                                return _OTransactionValidator;
                            }
                        }
                        if (_OTransactionValidator.IsValid)
                        {
                            double? CustomerTotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                     .Where(m => m.ParentId == MerchantId
                                                                      && m.AccountId == CustomerId
                                                                      && m.TransactionDate > StartDate
                                                                      && m.TransactionDate < EndDate
                                                                      && m.TypeId != TransactionType.TUCWalletTopup
                                                                      && (((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.ModeId == TransactionMode.Credit && m.TypeId != TransactionType.ThankUCashPlusCredit) || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                    ).Sum(x => (double?)x.PurchaseAmount) ?? 0;
                            if (SecurityMaximumInvoiceAmountPerDayPerCustomerCap > 0 && CustomerTotalInvoiceAmount > SecurityMaximumInvoiceAmountPerDayPerCustomerCap)
                            {
                                _OTransactionValidator.IsValid = false;
                                _OTransactionValidator.Comment = "Customer transaction invoice amount limit exceeded maximum limit of " + SecurityMaximumTransactionPerDayPerCustomerCap;
                                return _OTransactionValidator;
                            }
                        }
                        return _OTransactionValidator;
                    }
                }
                else
                {
                    return _OTransactionValidator;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("PROCESS TRANSACTION - TRANSACTION CHECK", _Exception);
                return _OTransactionValidator;
            }
        }
        internal void ValidateTransactionNotification(long MerchantId, long TransactionId, OTransactionValidator ValidationResponse, OUserReference UserReference)
        {
            try
            {
                if (HostEnvironment == HostEnvironmentType.Live)
                {

                    DateTime StartDate = HCoreHelper.GetGMTDate();
                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1).AddSeconds(-1);
                    string SecurityNotificationEmails = HCoreHelper.GetConfigurationValueByUserAccount("securitynotificationemails", MerchantId, UserReference);
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (ValidationResponse.IsValid == false)
                        {
                            if (!string.IsNullOrEmpty(SecurityNotificationEmails))
                            {
                                var TransactionParameters = _HCoreContext.HCUAccountTransaction
                                    .Where(x => x.Id == TransactionId && x.ParentId == MerchantId)
                                    .Select(x => new
                                    {
                                        ReferenceId = x.Id,
                                        MerchantId = x.ParentId,

                                        StoreDisplayName = x.SubParent.DisplayName,
                                        StoreAddress = x.SubParent.Address,

                                        CustomerDisplayName = x.Customer.DisplayName,
                                        CustomerMobileNumber = x.Customer.MobileNumber,

                                        PaymentMode = x.Type.Name,

                                        InvoiceAmount = x.PurchaseAmount,

                                        TerminalId = x.Terminal.IdentificationNumber,

                                        CashierCode = x.Cashier.DisplayName,
                                        CashierName = x.Cashier.Name,
                                        TransactionDate = x.TransactionDate.ToString("dddd, dd MMMM yyyy HH:mm:ss")
                                    }).FirstOrDefault();

                                if (SecurityNotificationEmails.Contains(";"))
                                {
                                    string[] TEmails = SecurityNotificationEmails.Split(';');
                                    foreach (var EmailAddress in TEmails)
                                    {
                                        HCoreHelper.BroadCastEmail(NotificationTemplates.Merchant_Transaction_HeavyAmount, "Thank U Cash Transaction Alert", EmailAddress, TransactionParameters, null);
                                    }
                                }
                                else
                                {
                                    HCoreHelper.BroadCastEmail(NotificationTemplates.Merchant_Transaction_HeavyAmount, "Thank U Cash Transaction Alert", SecurityNotificationEmails, TransactionParameters, null);
                                }

                                //var _EmailBoradCasterParameter = new
                                //{
                                //    StoreDisplayName = null,
                                //    StoreAddress = null,

                                //    UserDisplayName = null,
                                //    UserMobileNumber = null,

                                //    PaymentMode = null,

                                //    InvoiceAmount = null,

                                //    TransactionDate = null,
                                //    TerminalId = null,
                                //    PaymentMode = null,
                                //    ReferenceId = null,
                                //    ReceiverEmailAddress = null,
                                //};



                                //using (_HCoreContext = new HCoreContext())
                                //{
                                //    var StoreDetails = _HCoreContext.HCUAccount.Where(x => x.Id == StoreId).Select(x => new
                                //    {
                                //        DisplayName = x.DisplayName,
                                //        Address = x.Address,
                                //    }).FirstOrDefault();

                                //    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == CustomerId).Select(x => new
                                //    {
                                //        DisplayName = x.DisplayName,
                                //        MobileNumber = x.MobileNumber,
                                //    });
                                //}







                                //EmailBoradCasterParameter _EmailBoradCasterParameter = new EmailBoradCasterParameter();
                                //_EmailBoradCasterParameter.UserDisplayname = UserAccountDetails.DisplayName;
                                //_EmailBoradCasterParameter.UserMobileNumber = UserAccountDetails.MobileNumber;
                                //_EmailBoradCasterParameter.Message = "Suspecious activity detected by user " + UserAccountDetails.DisplayName + ". User current status is " + UserAccountDetails.StatusName;
                                //_EmailBoradCasterParameter.InvoiceAmount = _Request.InvoiceAmount.ToString();



                            }
                        }
                        //using (_HCoreContext = new HCoreContext())
                        //{
                        //    var UserAccountDetails = _HCoreContext.HCUAccount
                        //                            .Where(x => x.Id == _TransactionItem.UserAccountId)
                        //                            .Select(x => new
                        //                            {
                        //                                ReferenceId = x.Id,
                        //                                ReferenceKey = x.Guid,
                        //                                DisplayName = x.DisplayName,
                        //                                MobileNumber = x.MobileNumber,
                        //                                StatusName = x.Status.Name,
                        //                            }).FirstOrDefault();
                        //    if (UserAccountDetails != null)
                        //    {
                        //        EmailBoradCasterParameter _EmailBoradCasterParameter = new EmailBoradCasterParameter();
                        //        _EmailBoradCasterParameter.UserDisplayname = UserAccountDetails.DisplayName;
                        //        _EmailBoradCasterParameter.UserMobileNumber = UserAccountDetails.MobileNumber;
                        //        _EmailBoradCasterParameter.Message = "Suspecious activity detected by user " + UserAccountDetails.DisplayName + ". User current status is " + UserAccountDetails.StatusName;
                        //        _EmailBoradCasterParameter.InvoiceAmount = _Request.InvoiceAmount.ToString();
                        //        var StoreDetails = _HCoreContext.HCUAccount
                        //                                        .Where(x => x.Id == _Request.SubParentId)
                        //                                        .Select(x => new
                        //                                        {
                        //                                            StoreId = x.Id,
                        //                                            MerchantId = x.OwnerId,
                        //                                            MerchantDisplayName = x.Owner.DisplayName,
                        //                                            StoreDisplayName = x.DisplayName,
                        //                                            StoreAddress = x.Address
                        //                                        }).FirstOrDefault();
                        //        if (StoreDetails != null)
                        //        {
                        //            _EmailBoradCasterParameter.MerchantDisplayName = StoreDetails.MerchantDisplayName;
                        //            _EmailBoradCasterParameter.StoreDisplayName = StoreDetails.StoreDisplayName;
                        //            _EmailBoradCasterParameter.StoreAddress = StoreDetails.StoreAddress;
                        //        }
                        //        var CashierDetails = _HCoreContext.HCUAccount
                        //                                      .Where(x => x.Id == _Request.CashierId)
                        //                                      .Select(x => new
                        //                                      {
                        //                                          CashierCode = x.DisplayName,
                        //                                          CashierName = x.Name,
                        //                                      }).FirstOrDefault();
                        //        if (CashierDetails != null)
                        //        {
                        //            _EmailBoradCasterParameter.CashierCode = CashierDetails.CashierCode;
                        //            _EmailBoradCasterParameter.CashierName = CashierDetails.CashierName;
                        //        }
                        //        if (HostEnvironment == HostEnvironmentType.Live)
                        //        {
                        //            HCoreHelper.BroadCastEmail(NotificationTemplates.Admin_Transaction_HeavyAmount, "Thank U Cash Alert", "harshal@thankucash.com", _EmailBoradCasterParameter, null);
                        //        }
                        //    }
                        //}
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("PROCESS TRANSACTION - TRANSACTION CHECK", _Exception);
            }
        }
    }
    public class PendingRewardCreateActor : ReceiveActor
    {
        HCoreContext _HCoreContext;
        TUCLoyaltyPending _TUCLoyaltyPending;
        ManageCoreTransaction _ManageCoreTransaction;
        public PendingRewardCreateActor()
        {
            Receive<OPendingRewardActorItem>(_RequestItem =>
            {
                try
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        int TransactionTypeId = TransactionType.CardReward;
                        switch (_RequestItem.ExternalRequest.TransactionMode)
                        {
                            case "cash":
                                TransactionTypeId = TransactionType.CashReward;
                                break;
                            case "card":
                                TransactionTypeId = TransactionType.CardReward;
                                break;
                            case "redeemreward":
                                TransactionTypeId = TransactionType.Loyalty.TUCRedeem.RedeemReward;
                                break;
                            default:
                                TransactionTypeId = TransactionType.CashReward;
                                if (!string.IsNullOrEmpty(_RequestItem.ExternalRequest.bin) || !string.IsNullOrEmpty(_RequestItem.ExternalRequest.SixDigitPan))
                                {
                                    TransactionTypeId = TransactionType.CardReward;
                                }
                                break;
                        }
                        _TUCLoyaltyPending = new TUCLoyaltyPending();
                        _TUCLoyaltyPending.Guid = HCoreHelper.GenerateGuid();
                        _TUCLoyaltyPending.TransactionDate = HCoreHelper.GetGMTDateTime();
                        //_TUCLoyaltyPending.TransactionId = 1;
                        _TUCLoyaltyPending.LoyaltyTypeId = Loyalty.Reward;
                        _TUCLoyaltyPending.TypeId = TransactionTypeId;
                        _TUCLoyaltyPending.ModeId = TransactionMode.Credit;
                        if (_RequestItem.AmountDistribution.IsThankUCashEnabled == true)
                        {
                            _TUCLoyaltyPending.SourceId = TransactionSource.ThankUCashPlus;
                        }
                        else
                        {
                            _TUCLoyaltyPending.SourceId = TransactionSource.TUC;
                        }
                        if (_RequestItem.GatewayInfo.MerchantId > 0)
                        {
                            _TUCLoyaltyPending.FromAccountId = _RequestItem.GatewayInfo.MerchantId;
                        }
                        if (_RequestItem.UserInfo.UserAccountId > 0)
                        {
                            _TUCLoyaltyPending.ToAccountId = _RequestItem.UserInfo.UserAccountId;
                        }
                        _TUCLoyaltyPending.Amount = _RequestItem.AmountDistribution.TUCRewardAmount;
                        _TUCLoyaltyPending.CommissionAmount = _RequestItem.AmountDistribution.TUCRewardCommissionAmount;
                        _TUCLoyaltyPending.TotalAmount = _RequestItem.AmountDistribution.TUCRewardAmount;
                        _TUCLoyaltyPending.InvoiceAmount = _RequestItem.AmountDistribution.InvoiceAmount;
                        _TUCLoyaltyPending.LoyaltyInvoiceAmount = _RequestItem.AmountDistribution.ReferenceInvoiceAmount;
                        _TUCLoyaltyPending.Balance = 0;

                        if (_RequestItem.UserInfo.UserAccountId > 0)
                        {
                            _TUCLoyaltyPending.CustomerId = _RequestItem.UserInfo.UserAccountId;
                        }
                        if (_RequestItem.GatewayInfo.MerchantId > 0)
                        {
                            _TUCLoyaltyPending.MerchantId = _RequestItem.GatewayInfo.MerchantId;
                        }
                        if (_RequestItem.GatewayInfo.StoreId > 0)
                        {
                            _TUCLoyaltyPending.StoreId = _RequestItem.GatewayInfo.StoreId;
                        }
                        if (_RequestItem.GatewayInfo.CashierId > 0)
                        {
                            _TUCLoyaltyPending.CashierId = _RequestItem.GatewayInfo.CashierId;
                        }
                        if (_RequestItem.GatewayInfo.TerminalId > 0)
                        {
                            _TUCLoyaltyPending.TerminalId = _RequestItem.GatewayInfo.TerminalId;
                        }
                        if (_RequestItem.GatewayInfo.PtspId > 0)
                        {
                            _TUCLoyaltyPending.ProviderId = _RequestItem.GatewayInfo.PtspId;
                        }
                        if (_RequestItem.GatewayInfo.AcquirerId > 0)
                        {
                            _TUCLoyaltyPending.AcquirerId = _RequestItem.GatewayInfo.AcquirerId;
                        }
                        //_TUCLoyaltyPending.InvoiceNumber = 1;
                        _TUCLoyaltyPending.AccountNumber = _RequestItem.ExternalRequest.ReferenceNumber;
                        _TUCLoyaltyPending.ReferenceNumber = _RequestItem.ExternalRequest.ReferenceNumber;
                        _TUCLoyaltyPending.CreateDate = HCoreHelper.GetGMTDateTime();
                        //_TUCLoyaltyPending.CreatedById = _RequestItem.GatewayInfo.TerminalId;
                        _TUCLoyaltyPending.CreatedById = _RequestItem.ExternalRequest.UserReference.AccountId;
                        _TUCLoyaltyPending.StatusId = HelperStatus.Transaction.Pending;
                        _HCoreContext.TUCLoyaltyPending.Add(_TUCLoyaltyPending);
                        _HCoreContext.SaveChanges();
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        double Balance = HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_RequestItem.UserInfo.UserAccountId), 2);

                        using (_HCoreContext = new HCoreContext())
                        {
                            string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RequestItem.UserInfo.UserAccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                            _HCoreContext.Dispose();
                            if (!string.IsNullOrEmpty(UserNotificationUrl))
                            {
                                if (HostEnvironment == HostEnvironmentType.Live)
                                {
                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "N " + _RequestItem.AmountDistribution.User + " pending rewards received.", "Your have received N" + +_RequestItem.AmountDistribution.User + " from " + _RequestItem.GatewayInfo.MerchantDisplayName + " for purchase of N" + _RequestItem.AmountDistribution.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                }
                                else
                                {
                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : N " + +_RequestItem.AmountDistribution.User + " pending rewards received.", "Your have received N" + +_RequestItem.AmountDistribution.User + " from " + _RequestItem.GatewayInfo.MerchantDisplayName + " for purchase of N" + _RequestItem.AmountDistribution.InvoiceAmount, "dashboard", 0, null, "View details", false, null, null);
                                }
                            }
                            else
                            {
                                string RewardSms = HCoreHelper.GetConfiguration("pendingrewardsms", _RequestItem.GatewayInfo.MerchantId);
                                if (!string.IsNullOrEmpty(RewardSms))
                                {
                                    #region Send SMS
                                    string Message = RewardSms
                                    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_RequestItem.AmountDistribution.User, _AppConfig.SystemExitRoundDouble).ToString())
                                    .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                                    .Replace("[MERCHANT]", _RequestItem.GatewayInfo.MerchantDisplayName)
                                    .Replace("[TCODE]", HCoreHelper.GenerateRandomNumber(4));
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem.UserInfo.MobileNumber, Message, _RequestItem.UserInfo.UserAccountId, _RequestItem.UserInfo.UserAccountKey, null);
                                    #endregion
                                }
                                else
                                {
                                    #region Send SMS
                                    //string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app Pls Give Cashier Code:" + _TransactionReference.TCode;
                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_RequestItem.AmountDistribution.User, _AppConfig.SystemExitRoundDouble).ToString() + " pending points cash reward from " + _RequestItem.GatewayInfo.MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem.UserInfo.MobileNumber, Message, _RequestItem.UserInfo.UserAccountId, _RequestItem.UserInfo.UserAccountKey);
                                    #endregion
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(_RequestItem.UserInfo.EmailAddress))
                        {
                            var _EmailParameters = new
                            {
                                UserDisplayName = _RequestItem.UserInfo.DisplayName,
                                MerchantName = _RequestItem.GatewayInfo.MerchantDisplayName,
                                InvoiceAmount = _RequestItem.AmountDistribution.InvoiceAmount.ToString(),
                                Amount = HCoreHelper.RoundNumber(_RequestItem.AmountDistribution.User, 2).ToString(),
                                Balance = Balance.ToString(),
                            };
                            HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.PendingRewardEmail, _RequestItem.UserInfo.DisplayName, _RequestItem.UserInfo.EmailAddress, _EmailParameters, _RequestItem.ExternalRequest.UserReference);
                        }
                    }
                }
                catch (Exception _Exception)
                {
                    HCoreHelper.LogException("SAVEPENDINGREWARD", _Exception);
                }
            });
        }
    }
    public class ActorRewardPostTransaction : ReceiveActor
    {
        ManageCoreTransaction _ManageCoreTransaction;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        HCoreContext _HCoreContext;
        public ActorRewardPostTransaction()
        {
            Receive<OPostTransaction>(_RequestItem =>
            {
                _ManageCoreTransaction = new ManageCoreTransaction();
                if (_RequestItem._AmountDistributionReference.IsThankUCashEnabled == true)
                {
                    OConfiguration RewardCriteriaDetails = HCoreHelper.GetConfigurationDetails("thankucashplusrewardcriteria", _RequestItem._GatewayInfo.MerchantId);
                    double TucPlusMinTransferAmount = Convert.ToDouble(HCoreHelper.GetConfiguration("thankucashplusmintransferamount", _RequestItem._GatewayInfo.MerchantId));
                    double ThankUCashPlusBalance = _ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_RequestItem._UserInfo.UserAccountId, _RequestItem._GatewayInfo.MerchantId);

                    if ((ThankUCashPlusBalance > (TucPlusMinTransferAmount - 1)))
                    {
                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CashierId = _RequestItem._GatewayInfo.CashierId;
                        _CoreTransactionRequest.CustomerId = _RequestItem._UserInfo.UserAccountId;
                        _CoreTransactionRequest.UserReference = _RequestItem._Request.UserReference;
                        _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest.ParentId = _RequestItem._GatewayInfo.MerchantId;
                        _CoreTransactionRequest.SubParentId = _RequestItem._GatewayInfo.StoreId;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _RequestItem._AmountDistributionReference.ReferenceInvoiceAmount;
                        _CoreTransactionRequest.InvoiceAmount = _RequestItem._AmountDistributionReference.InvoiceAmount;
                        if (_RequestItem._GatewayInfo.AcquirerId != null)
                        {
                            _CoreTransactionRequest.BankId = (long)_RequestItem._GatewayInfo.AcquirerId;
                        }
                        _CoreTransactionRequest.ReferenceAmount = _RequestItem._AmountDistributionReference.TUCRewardAmount;
                        _CoreTransactionRequest.ReferenceNumber = _RequestItem._Request.ReferenceNumber;
                        _CoreTransactionRequest.TerminalId = _RequestItem._GatewayInfo.TerminalId;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _RequestItem._UserInfo.UserAccountId,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.ThankUCashPlusCredit,
                            SourceId = TransactionSource.ThankUCashPlus,
                            Amount = ThankUCashPlusBalance,
                            TotalAmount = ThankUCashPlusBalance
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _RequestItem._UserInfo.UserAccountId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.ThankUCashPlusCredit,
                            SourceId = TransactionSource.TUC,
                            Amount = ThankUCashPlusBalance,
                            TotalAmount = ThankUCashPlusBalance
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        OCoreTransaction.Response TUCPlusTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        double ThankUCashPlusBalanceUpdate = _ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_RequestItem._UserInfo.UserAccountId, _RequestItem._GatewayInfo.MerchantId);
                        double AccBalance = _ManageCoreTransaction.GetAppUserBalance(_RequestItem._UserInfo.UserAccountId);
                        if (_RequestItem._UserInfo.AccountStatusId == HelperStatus.Default.Active && (HostEnvironment == HostEnvironmentType.Live || HostEnvironment == HostEnvironmentType.Test))
                        {
                            //if (_RequestItem._AmountDistributionReference.User > 0 && _RequestItem._AmountDistributionReference.IsMerchantLowBalance  == false)
                            if (_RequestItem._AmountDistributionReference.User > 0)
                            {
                                if (_RequestItem._GatewayInfo.MerchantId == 49648)
                                {
                                    //string Message = "Credit Alert: You have been rewarded by ENYO ThankUCash, Bal: " + ThankUCashPlusBalance + ". Your ThankUCash Bal: N" + AccBalance + ".";
                                    // HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                    //Cr Alert: You’ve earned 101 ENYOThankU points. Continue to buy & earn points. Redeem from 1stOct. ETU Bal: 202points Main Bal N3245. Get App https://bit.ly/tuc-app
                                    string Message = "Cr Alert: Your ThankUCash Plus " + Math.Round(ThankUCashPlusBalance, 2) + " Points now available for you to redeem. TUC Plus Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + " Main Bal: " + Math.Round(AccBalance, 2) + ". Download TUC App: https://bit.ly/tuc-app";
                                    //string Message = "Cr Alert: You’ve earned " + Math.Round(ThankUCashPlusBalance, 2) + " ENYOThankU points. Continue to buy & earn points. Redeem from 1stOct. ETU Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + "pt Main Bal: " + Math.Round(AccBalance, 2) + ". Get App https://bit.ly/tuc-app";
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);
                                }
                                else
                                {
                                    string Message = "Cr Alert: Your ThankUCash Plus " + Math.Round(ThankUCashPlusBalance, 2) + " Points now available for you to redeem. TUC Plus Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + " Main Bal: " + Math.Round(AccBalance, 2) + ". Download TUC App: https://bit.ly/tuc-app";
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);
                                    //string RewardSms = HCoreHelper.GetConfiguration("rewardsms", _GatewayInfo.MerchantId);
                                    //if (!string.IsNullOrEmpty(RewardSms))
                                    //{
                                    //    #region Send SMS
                                    //    string Message = RewardSms
                                    //    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString())
                                    //    .Replace("[BALANCE]", HCoreHelper.RoundNumber(AccBalance, _AppConfig.SystemExitRoundDouble).ToString())
                                    //    .Replace("[MERCHANT]", _GatewayInfo.MerchantDisplayName)
                                    //    .Replace("[TCODE]", _TransactionReference.TCode);
                                    //    //   HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                    //    #endregion
                                    //}
                                    //else
                                    //{
                                    //    #region Send SMS
                                    //    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(AccBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                    //    #endregion
                                    //}
                                }
                            }
                        }
                    }
                    else
                    {
                        double Balance = _ManageCoreTransaction.GetAppUserBalance(_RequestItem._UserInfo.UserAccountId);
                        if (_RequestItem._UserInfo.AccountStatusId == HelperStatus.Default.Active && HostEnvironment == HostEnvironmentType.Live)
                        {
                            if (_RequestItem._GatewayInfo.MerchantId == 49648)
                            {
                                if (_RequestItem._AmountDistributionReference.User > 0)
                                {
                                    double ThankUCashPlusBalanceUpdate = _ManageCoreTransaction.GetAppUserThankUCashPlusBalance(_RequestItem._UserInfo.UserAccountId, _RequestItem._GatewayInfo.MerchantId);
                                    //string Message = "Cr Alert: You’ve earned " + Math.Round(_AmountDistributionReference.User, 2) + " ENYOThankU points. Continue to buy & earn points. Redeem from 1stOct. ETU Bal: " + Math.Round(ThankUCashPlusBalanceUpdate, 2) + "pt Main Bal: " + Math.Round(Balance, 2) + ". Get App https://bit.ly/tuc-app";
                                    //"Cr Alert: You've earned 2.18 ENYOThankU points. Continue to buy & earn points. Redeem from 1st Oct. ETU Bal: 101.4pt Main Bal: 8195.41. Get App https://bit.ly/tuc-app";
                                    string Message = "Cr Alert: You got " + Math.Round(_RequestItem._AmountDistributionReference.User, 2) + " ENYO ThankU points. Continue to buy & earn points. ETU Plus Bal:  N" + Math.Round(ThankUCashPlusBalance, 2) + ". Main Bal is N" + Math.Round(Balance, 2) + " Get App: https://bit.ly/tuc-app";
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);
                                }
                                else
                                {
                                    string Message = "Cr Alert: You got 0 TUC Plus points from ENYO ThankUCash. Continue to buy & earn points. TUC Plus Bal:  N" + Math.Round(ThankUCashPlusBalance, 2) + ". Main Balance is N" + Math.Round(Balance, 2) + " Get App: https://bit.ly/tuc-app";
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);
                                }
                            }
                            else
                            {
                                //if (_RequestItem._AmountDistributionReference.User > 0 && _RequestItem._AmountDistributionReference.IsMerchantLowBalance == false)
                                if (_RequestItem._AmountDistributionReference.User > 0)
                                {
                                    //string RewardSms = HCoreHelper.GetConfiguration("rewardsms", _GatewayInfo.MerchantId);
                                    //if (!string.IsNullOrEmpty(RewardSms))
                                    //{
                                    //    #region Send SMS
                                    //    string Message = RewardSms
                                    //    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString())
                                    //    .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                                    //    .Replace("[MERCHANT]", _GatewayInfo.MerchantDisplayName)
                                    //    .Replace("[TCODE]", "");
                                    //    HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                    //    #endregion
                                    //}
                                    //else
                                    //{
                                    //    #region Send SMS
                                    //    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download App https://bit.ly/tuc-app . ThankUCash";
                                    //    HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                    //    #endregion
                                    //}
                                    double TBalance = _ManageCoreTransaction.GetAppUserBalance(_RequestItem._UserInfo.UserAccountId);
                                    #region Send SMS
                                    string Message = "Cr Alert: You got " + HCoreHelper.RoundNumber(_RequestItem._AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points from " + _RequestItem._GatewayInfo.MerchantDisplayName + ". Buy upto N" + RewardCriteriaDetails.Value + " to get cash reward. TUC Plus Bal: N" + HCoreHelper.RoundNumber(ThankUCashPlusBalance, _AppConfig.SystemExitRoundDouble).ToString() + ".Main Balance is N" + HCoreHelper.RoundNumber(TBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Get App: https://bit.ly/tuc-app";// + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download App https://bit.ly/tuc-app . ThankUCash";
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            //  HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                    #endregion
                                }
                                else
                                {
                                    double TBalance = _ManageCoreTransaction.GetAppUserBalance(_RequestItem._UserInfo.UserAccountId);
                                    #region Send SMS
                                    string Message = "Cr Alert: You got 0 points from " + _RequestItem._GatewayInfo.MerchantDisplayName + ". Buy upto N" + RewardCriteriaDetails.Value + " to get cash reward. TUC Plus Bal: N" + HCoreHelper.RoundNumber(ThankUCashPlusBalance, _AppConfig.SystemExitRoundDouble).ToString() + ".Main Balance is N" + HCoreHelper.RoundNumber(TBalance, _AppConfig.SystemExitRoundDouble).ToString() + ". Get App: https://bit.ly/tuc-app";// + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download App https://bit.ly/tuc-app . ThankUCash";
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            //  HCoreHelper.SendSMS(SmsType.Transaction, "234", _UserInfo.MobileNumber, Message);
                                    #endregion
                                }
                            }
                            #region Send Email 
                            if (!string.IsNullOrEmpty(_RequestItem._UserInfo.EmailAddress))
                            {
                                var _EmailParameters = new
                                {
                                    UserDisplayName = _RequestItem._UserInfo.DisplayName,
                                    MerchantName = _RequestItem._GatewayInfo.MerchantDisplayName,
                                    InvoiceAmount = _RequestItem._AmountDistributionReference.InvoiceAmount.ToString(),
                                    Amount = HCoreHelper.RoundNumber(_RequestItem._AmountDistributionReference.User, 2).ToString(),
                                    Balance = Balance.ToString(),
                                };
                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _RequestItem._UserInfo.DisplayName, _RequestItem._UserInfo.EmailAddress, _EmailParameters, _RequestItem._Request.UserReference);
                            }
                            #endregion
                        }
                    }
                }
                else
                {
                    //double Balance = HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_RequestItem._UserInfo.UserAccountId), 2);
                    if (_RequestItem._UserInfo.AccountStatusId == HelperStatus.Default.Active)
                    {
                        if (HostEnvironment == HostEnvironmentType.Live)
                        {
                            double Balance = 0;
                            if (_RequestItem._AmountDistributionReference.IsThankUCashGold == true)
                            {
                                Balance = HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_RequestItem._UserInfo.UserAccountId, _RequestItem._GatewayInfo.MerchantId, TransactionSource.TUCBlack), 2);
                            }
                            else
                            {
                                Balance = HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(_RequestItem._UserInfo.UserAccountId), 2);
                            }
                            if (_RequestItem._AmountDistributionReference.User > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string NotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _RequestItem._UserInfo.UserAccountId && x.StatusId == HelperStatus.Default.Active && x.NotificationUrl != null).Select(x => x.NotificationUrl).FirstOrDefault();
                                    _HCoreContext.SaveChanges();
                                    if (!string.IsNullOrEmpty(NotificationUrl))
                                    {
                                        HCoreHelper.SendPushToDevice(NotificationUrl, "pointshistory",
                                            "Credit Alert: You have earned N" + _RequestItem._AmountDistributionReference.User + ".",
                                            "Your have earned N" + _RequestItem._AmountDistributionReference.User + " points Cash from " + _RequestItem._GatewayInfo.MerchantDisplayName,
                                            "pointshistory", 0, null, "Continue", false, null, null);
                                    }
                                }
                                //string NotificationUrl = _HCo
                                string RewardSms = HCoreHelper.GetConfiguration("rewardsms", _RequestItem._GatewayInfo.MerchantId);
                                if (!string.IsNullOrEmpty(RewardSms))
                                {
                                    #region Send SMS
                                    string Message = RewardSms
                                    .Replace("[AMOUNT]", HCoreHelper.RoundNumber(_RequestItem._AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString())
                                    .Replace("[BALANCE]", HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString())
                                    .Replace("[MERCHANT]", _RequestItem._GatewayInfo.MerchantDisplayName)
                                    .Replace("[TCODE]", _RequestItem.TCode);
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);
                                    #endregion
                                }
                                else
                                {
                                    #region Send SMS
                                    //string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points Cash from " + _GatewayInfo.MerchantDisplayName + " Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app Pls Give Cashier Code:" + _TransactionReference.TCode;
                                    string Message = "Credit Alert: You got " + HCoreHelper.RoundNumber(_RequestItem._AmountDistributionReference.User, _AppConfig.SystemExitRoundDouble).ToString() + " points cash reward from " + _RequestItem._GatewayInfo.MerchantDisplayName + "  Bal: N" + HCoreHelper.RoundNumber(Balance, _AppConfig.SystemExitRoundDouble).ToString() + ". Download TUC App: https://bit.ly/tuc-app";// Pls Give Cashier Code:" + _TransactionReference.TCode;
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestItem._UserInfo.MobileNumber, Message, _RequestItem._UserInfo.UserAccountId, _RequestItem.GroupKey);
                                    #endregion
                                }
                                #region Send Email 
                                if (!string.IsNullOrEmpty(_RequestItem._UserInfo.EmailAddress))
                                {
                                    var _EmailParameters = new
                                    {
                                        UserDisplayName = _RequestItem._UserInfo.DisplayName,
                                        MerchantName = _RequestItem._GatewayInfo.MerchantDisplayName,
                                        InvoiceAmount = _RequestItem._AmountDistributionReference.InvoiceAmount.ToString(),
                                        Amount = HCoreHelper.RoundNumber(_RequestItem._AmountDistributionReference.User, 2).ToString(),
                                        Balance = Balance.ToString(),
                                    };
                                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.RewardEmail, _RequestItem._UserInfo.DisplayName, _RequestItem._UserInfo.EmailAddress, _EmailParameters, _RequestItem._Request.UserReference);
                                }
                                #endregion
                            }
                        }
                    }
                }
            });
        }
    }
}