//==================================================================================
// FileName: OCoreGatewayTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using HCore.ThankUCash.Gateway.Object;

namespace HCore.ThankUCash.Gateway.Core
{

    public class OFailedTransaction
    {
        public  long CustomerId { get; set; }
        public long MerchantId { get; set; }
        public long StoreId { get; set; }
        public long CashierId { get; set; }
        public long PaymentMode { get; set; }
        public long SourceId { get; set; }
        public double InvoiceAmount { get; set; }
    }
    public class OTransactionValidator
    {
        public bool IsValid { get; set; }
        public string? Comment { get; set; }
    }

    public class BinStatus
    {
        public bool status { get; set; }
        public string? message { get; set; }
        public BinInfo data { get; set; }
    }
    public class BinInfo
    {
        public string? bin { get; set; }
        public string? bin_systemname { get; set; }
        public string? brand { get; set; }
        public string? brand_systemname { get; set; }
        public string? sub_brand { get; set; }
        public string? sub_brand_systemname { get; set; }
        public string? country_code { get; set; }
        public string? country_code_systemname { get; set; }
        public string? country_name { get; set; }
        public string? country_name_systemname { get; set; }
        public string? card_type { get; set; }
        public string? card_type_systemname { get; set; }
        public string? bank { get; set; }
        public string? bank_systemname { get; set; }
    }

    public class OTrCoreProcess
    {
        public string? GroupKey { get; set; }
        public OCoreGatewayTransaction.Reward.Request _Request { get; set; }
        public OThankUGateway.Request _ExternalRequest { get; set; }
        internal OGatewayInfo _GatewayInfo { get; set; }
        internal OUserInfo _UserInfo { get; set; }
        internal OAmountDistribution _AmountDistributionReference { get; set; }
    }

    public class OCore
    {
        public class Reward
        {
            public class Request
            {
                public string? GroupKey { get; set; }
                public long CustomerId { get; set; }
                public long MerchantId { get; set; }
                public long StoreId { get; set; }
                public long CashierId { get; set; }
                public long AcquirerId { get; set; }
                public long TerminalId { get; set; }
            }
        }
    }



    public class OTrCoreRedeemProcess
    {
        public string? GroupKey { get; set; }
        public OCoreGatewayTransaction.Reward.Request _Request { get; set; }
        public OThankUGateway.Request _GatewayRequest { get; set; }
        internal OGatewayInfo _GatewayInfo { get; set; }
        internal OUserInfo _UserInfo { get; set; }
    }
    public class OCoreGatewayTransaction
    {
        public class Reward
        {
            public class Request
            {
                public long ParentTransactionId { get; set; }
                public long MerchantId { get; set; }
                public long CustomerId { get; set; }

                public long? StoreId { get; set; }
                public long? CashierId { get; set; }
                public long? TerminalId { get; set; }
                public long? AcquirerId { get; set; }
                public long? ProviderId { get; set; }
                public long? CreatedById { get; set; }

                public double RewardAmount { get; set; }
                public double CommissionAmount { get; set; }
                public double TotalAmount { get; set; }
                public double ReferenceAmount { get; set; }
                public double ReferenceInvoiceAmount { get; set; }
                public double InvoiceAmount { get; set; }

                public string? ReferenceNumber { get; set; }
                public string? AccountNumber { get; set; }
                public string? InvoiceNumber { get; set; }
                public int StatusId { get; set; }
                public TransactionItem FromAccount { get; set; }
                public TransactionItem ToAccount { get; set; }
                public List<TransactionItem> PartnerTransactions { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class TransactionItem
            {
                public long UserAccountTypeId { get; set; }
                public long UserAccountId { get; set; }
                public int ModeId { get; set; }
                public int TypeId { get; set; }
                public int SourceId { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double Comission { get; set; }
                public double TotalAmount { get; set; }
                public string? Description { get; set; }
                public string? Comment { get; set; }
                public int StatusId { get; set; }
            }
            //public class Response
            //{
            //    public long Status { get; set; }
            //    public string? Message { get; set; }
            //    public long ReferenceId { get; set; }
            //    public string? ReferenceKey { get; set; }
            //    public string? TCode { get; set; }
            //    public string? GroupKey { get; set; }
            //    public DateTime TransactionDate { get; set; }
            //}
        }
    }
}
