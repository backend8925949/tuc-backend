//==================================================================================
// FileName: FrameworkCoralPay.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Gateway.ObjectV2;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using Akka.Actor;

namespace HCore.ThankUCash.Gateway.IntegrationV2
{
    public class ActorCoralPayTransactionProcessor : ReceiveActor
    {
        FrameworkCoralPay _FrameworkCoralPay;
        public ActorCoralPayTransactionProcessor()
        {
            Receive<OThankUGateway.CoralPay.Request>(_Request =>
            {
                //_FrameworkCoralPay = new FrameworkCoralPay();
                //_FrameworkCoralPay.CoralPay_SaveTransactions(_Request);
            });
        }
    }
    internal class FrameworkCoralPay
    {
        HCoreContextOperations _HCoreContextOperations;
        //HCOCoralPaySync _HCOCoralPaySync;
        //List<HCOCoralPaySync> _HCOCoralPaySyncList;
        //internal void CoralPay_SaveTransactions(OThankUGateway.CoralPay.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (_Request.TransactionList != null && _Request.TransactionList.Count > 0)
        //        {
        //            _HCOCoralPaySyncList = new List<HCOCoralPaySync>();
        //            using (_HCoreContextOperations = new HCoreContextOperations())
        //            {
        //                foreach (var TransactionItem in _Request.TransactionList)
        //                {
        //                    bool TransactionCheck = _HCoreContextOperations.HCOCoralPaySync.Any(x => x.TransactionId == TransactionItem.TransactionId);
        //                    if (TransactionCheck == false)
        //                    {
        //                        _HCOCoralPaySync = new HCOCoralPaySync();
        //                        _HCOCoralPaySync.Guid = HCoreHelper.GenerateGuid();
        //                        _HCOCoralPaySync.BatchId = _Request.ResponseHeader.BatchId;
        //                        _HCOCoralPaySync.ClientId = TransactionItem.ClientId;
        //                        _HCOCoralPaySync.TransactionDate = TransactionItem.Date;
        //                        _HCOCoralPaySync.TransactionDate = TransactionItem.Date;
        //                        _HCOCoralPaySync.TransactionId = TransactionItem.TransactionId;
        //                        _HCOCoralPaySync.MerchantId = TransactionItem.MerchantId;
        //                        _HCOCoralPaySync.TerminalId = TransactionItem.TerminalId;
        //                        _HCOCoralPaySync.DeviceId = TransactionItem.DeviceId;
        //                        _HCOCoralPaySync.ShortCode = TransactionItem.ShortCode;
        //                        _HCOCoralPaySync.Phone = TransactionItem.Phone;
        //                        _HCOCoralPaySync.RefCode = TransactionItem.RefCode;
        //                        _HCOCoralPaySync.Amount = TransactionItem.Amount;
        //                        _HCOCoralPaySync.ResponseCode = TransactionItem.ResponseCode;
        //                        _HCOCoralPaySync.CreateDate = HCoreHelper.GetGMTDateTime();
        //                        _HCOCoralPaySync.StatusId = 1;
        //                        _HCoreContextOperations.HCOCoralPaySync.Add(_HCOCoralPaySync);
        //                    }
        //                }
        //                _HCoreContextOperations.SaveChanges();
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException("CoralPay_SaveTransactions", _Exception, null);
        //        #endregion
        //    }
        //    #endregion
        //}

    }
}
