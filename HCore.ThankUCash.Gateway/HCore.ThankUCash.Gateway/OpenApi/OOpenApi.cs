//==================================================================================
// FileName: OOpenApi.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.ThankUCash.Gateway.OpenApi
{
    public class OOpenApi
    {
        public class ERequest
        {
            public string? request { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Account
        {
            public class Request
            {
                public string? profileCode { get; set; }
                public string? pin { get; set; }

                public string? accountNumber { get; set; }
                public string? firstName { get; set; }
                public string? middleName { get; set; }
                public string? lastName { get; set; }
                public string? emailAddress { get; set; }
                public string? gender { get; set; }
                public DateTime? dateOfBirth { get; set; }
                public string? address { get; set; }

                public OUserReference? UserReference { get; set; }
            }
            public class Details
            {
                public string? accountNumber { get; set; }
                public string? firstName { get; set; }
                public string? middleName { get; set; }
                public string? lastName { get; set; }
                public string? emailAddress { get; set; }
                public string? gender { get; set; }
                public DateTime? dateOfBirth { get; set; }
                public string? address { get; set; }
            }
        }
        public class UserAppRequest
        {
            public class Request
            {
                public int countryId { get; set; }
                public string? countryIsd { get; set; }
                public string? firstName { get; set; }
                public string? lastName { get; set; }
                public string? emailAddress { get; set; }
                public string? mobileNumber { get; set; }
                public string? source { get; set; }
                public string? referralCode { get; set; }



                public string? deviceType { get; set; }
                public string? browser { get; set; }
                public string? browserVersion { get; set; }
                public string? device { get; set; }
                public string? os { get; set; }
                public string? osVersion { get; set; }
                public string? userAgent { get; set; }

                public double? latitude { get; set; }
                public double? longitude { get; set; }
                public string? app { get; set; }

            }
            public class Response
            {
                public string? reference { get; set; }
            }
        }
        public class Balance
        {
            public class ERequest
            {
                public string? request { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Request
            {
                public string? accountNumber { get; set; }
                public string? pin { get; set; }
            }
            public class Response
            {
                public string? displayName { get; set; }
                public string? firstName { get; set; }
                public string? lastName { get; set; }
                public long balance { get; set; }
                public string? profileLink { get; set; }
                public string? profileCode { get; set; }
                public string? accountNumber { get; set; }
            }
        }
        public class Register
        {
            public class ERequest
            {
                public string? request { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Request
            {
                public string? firstName { get; set; }
                public string? lastName { get; set; }
                public string? middleName { get; set; }
                public string? emailAddress { get; set; }
                public string? mobileNumber { get; set; }
                public string? gender { get; set; }
                public string? address { get; set; }
                public string? countryIsd { get; set; }
                public DateTime? dateOfBirth { get; set; }
            }
            public class Response
            {
                public string? accountNumber { get; set; }
            }
        }
    }
}
