//==================================================================================
// FileName: ManageAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.ThankUCash.Gateway.OpenApi
{
    public class ManageAccount
    {
        FrameworkAccount _FrameworkAccount;
        public OResponse GetCountries(OOpenApi.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetCountries(_Request);
        }
        public OResponse SaveUserAppRequest(OOpenApi.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.SaveUserAppRequest(_Request);
        }
        public OResponse GetAccount(OOpenApi.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetAccount(_Request);
        }
        public OResponse UpdateAccount(OOpenApi.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.UpdateAccount(_Request);
        }
        public OResponse ResetPin(OOpenApi.Balance.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.ResetPin(_Request);
        }
        public OResponse GetBalance(OOpenApi.Balance.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetBalance(_Request);
        }
        public OResponse Register(OOpenApi.Register.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.Register(_Request);
        }
        public OResponse RegisterWeb(OOpenApi.Register.ERequest _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.RegisterWeb(_Request);
        }
        public DeleteCustomerInfo.Response DeleteAccount(DeleteCustomerInfo.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.DeleteCustomer(_Request);
        }
    }
}
