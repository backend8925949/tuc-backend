//==================================================================================
// FileName: FrameworkAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.IO;
using System.Linq;
using System.Net;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.ThankUCash.Gateway.Core;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.ThankUCash.Gateway.OpenApi
{
    internal class FrameworkAccount
    {
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCoreContext _HCoreContext;
        CoreOperations _CoreOperations;
        OOpenApi.Balance.Response _BalanceResponse;
        OOpenApi.Account.Details _AccountDetails;
        Random _Random;
        ManageCoreTransaction _ManageCoreTransaction;
        cmt_loyalty_customer _cmt_loyalty_customer;
        HCoreContextOperations _HCoreContextLogging;
        HCOCustomerRegistration _HCOCustomerRegistration;
        internal OResponse SaveUserAppRequest(OOpenApi.ERequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                string _RequestContentS = HCoreEncrypt.DecodeText(_Request.request);
                OOpenApi.UserAppRequest.Request _RequestContent = JsonConvert.DeserializeObject<OOpenApi.UserAppRequest.Request>(_RequestContentS);

                if (string.IsNullOrEmpty(_RequestContent.mobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Mobile number required");
                }
                if (_RequestContent.countryId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Country required");
                }
                var MobileNumber = HCoreHelper.FormatMobileNumber(_RequestContent.countryIsd, _RequestContent.mobileNumber);

                var MobileNumberValidate = HCoreHelper.ValidateMobileNumber(MobileNumber);
                if (!MobileNumberValidate.IsNumberValid)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Please enter valid mobile number");
                }
                using (_HCoreContextLogging = new HCoreContextOperations())
                {
                    _HCOCustomerRegistration = new HCOCustomerRegistration();
                    _HCOCustomerRegistration.Guid = HCoreHelper.GenerateGuid();
                    _HCOCustomerRegistration.CountryId = _RequestContent.countryId;
                    _HCOCustomerRegistration.FirstName = _RequestContent.firstName;
                    _HCOCustomerRegistration.LastName = _RequestContent.lastName;
                    _HCOCustomerRegistration.EmailAddress = _RequestContent.emailAddress;
                    _HCOCustomerRegistration.MobileNumber = MobileNumber;
                    _HCOCustomerRegistration.Source = _RequestContent.source;
                    _HCOCustomerRegistration.ReferralCode = _RequestContent.referralCode;
                    _HCOCustomerRegistration.CreateDate = HCoreHelper.GetGMTDateTime();
                    _HCOCustomerRegistration.StatusId = 1;

                    _HCOCustomerRegistration.Browser = _RequestContent.browser;
                    _HCOCustomerRegistration.BrowserVersion = _RequestContent.browserVersion;
                    _HCOCustomerRegistration.Device = _RequestContent.device;
                    _HCOCustomerRegistration.DeviceType = _RequestContent.deviceType;
                    _HCOCustomerRegistration.Os = _RequestContent.os;
                    _HCOCustomerRegistration.OsVersion = _RequestContent.osVersion;
                    _HCOCustomerRegistration.UserAgent = _RequestContent.userAgent;
                    _HCOCustomerRegistration.Latitude = _RequestContent.latitude;
                    _HCOCustomerRegistration.Longitude = _RequestContent.longitude;
                    _HCOCustomerRegistration.App = _RequestContent.app;

                    _HCOCustomerRegistration.IpAddress = _Request.UserReference.RequestIpAddress;
                    _HCoreContextLogging.HCOCustomerRegistration.Add(_HCOCustomerRegistration);
                    _HCoreContextLogging.SaveChanges();
                    var _Response = new
                    {
                        referenceKey = _HCOCustomerRegistration.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CAO004", "Success");
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SaveUserAppRequest", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }

        internal OResponse GetAccount(OOpenApi.ERequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                string _RequestContentS = HCoreEncrypt.DecodeText(_Request.request);
                OOpenApi.Account.Request _RequestContent = JsonConvert.DeserializeObject<OOpenApi.Account.Request>(_RequestContentS);

                if (string.IsNullOrEmpty(_RequestContent.profileCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Profile code required");
                }
                string AccountKey = HCoreEncrypt.DecryptHash(HCoreEncrypt.DecodeText(_RequestContent.profileCode));
                using (_HCoreContext = new HCoreContext())
                {
                    var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Guid == AccountKey)
                            .Select(x => new
                            {
                                AccountId = x.Id,
                                Pin = x.AccessPin,
                                accountNumber = x.AccountCode,
                                StatusId = x.StatusId,
                                firstName = x.FirstName,
                                middleName = x.MiddleName,
                                lastName = x.LastName,
                                gender = x.Gender.Name,
                                emailAddress = x.EmailAddress,
                                mobileNumber = x.MobileNumber,
                                dateOfBirth = x.DateOfBirth,
                            }).FirstOrDefault();
                    if (UAccount != null)
                    {
                        if (UAccount.StatusId == HelperStatus.Default.Active)
                        {
                            _AccountDetails = new OOpenApi.Account.Details();
                            _AccountDetails.accountNumber = UAccount.accountNumber;
                            _AccountDetails.firstName = UAccount.firstName;
                            _AccountDetails.middleName = UAccount.middleName;
                            _AccountDetails.lastName = UAccount.lastName;
                            _AccountDetails.emailAddress = UAccount.emailAddress;
                            if (!string.IsNullOrEmpty(UAccount.gender))
                            {
                                _AccountDetails.gender = UAccount.gender.ToLower();
                            }
                            _AccountDetails.dateOfBirth = UAccount.dateOfBirth;
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AccountDetails, "CAO007", "Account loaded");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO004", "Account not active. Contact support");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO005", "Account not registered");
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
        internal OResponse UpdateAccount(OOpenApi.ERequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                string _RequestContentS = HCoreEncrypt.DecodeText(_Request.request);
                OOpenApi.Account.Request _RequestContent = JsonConvert.DeserializeObject<OOpenApi.Account.Request>(_RequestContentS);

                if (string.IsNullOrEmpty(_RequestContent.profileCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Profile code required");
                }
                if (string.IsNullOrEmpty(_RequestContent.pin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Pin required");
                }
                string AccountKey = HCoreEncrypt.DecryptHash(HCoreEncrypt.DecodeText(_RequestContent.profileCode));
                using (_HCoreContext = new HCoreContext())
                {
                    var UAccount = _HCoreContext.HCUAccount
                            .Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Guid == AccountKey)
                            .FirstOrDefault();
                    if (UAccount != null)
                    {
                        if (UAccount.StatusId == HelperStatus.Default.Active)
                        {
                            string DApin = HCoreEncrypt.DecryptHash(UAccount.AccessPin);
                            if (DApin == _RequestContent.pin)
                            {
                                if (!string.IsNullOrEmpty(_RequestContent.emailAddress))
                                {
                                    UAccount.EmailAddress = _RequestContent.emailAddress;
                                }
                                if (!string.IsNullOrEmpty(_RequestContent.gender))
                                {
                                    if (!string.IsNullOrEmpty(_RequestContent.gender))
                                    {
                                        if (_RequestContent.gender == "gender.male" || _RequestContent.gender == "male")
                                        {
                                            UAccount.GenderId = Gender.Male;
                                        }
                                        if (_RequestContent.gender == "gender.female" || _RequestContent.gender == "female")
                                        {
                                            UAccount.GenderId = Gender.Female;
                                        }
                                        if (_RequestContent.gender == "gender.other" || _RequestContent.gender == "other")
                                        {
                                            UAccount.GenderId = Gender.Other;
                                        }
                                    }
                                }
                                if (!string.IsNullOrEmpty(_RequestContent.firstName))
                                {
                                    UAccount.FirstName = _RequestContent.firstName;
                                    UAccount.Name = _RequestContent.firstName;
                                }
                                if (!string.IsNullOrEmpty(_RequestContent.middleName))
                                {
                                    UAccount.MiddleName = _RequestContent.middleName;
                                    UAccount.Name = UAccount.Name + " " + UAccount.MiddleName;
                                }
                                if (!string.IsNullOrEmpty(_RequestContent.lastName))
                                {
                                    UAccount.LastName = _RequestContent.lastName;
                                    if (!string.IsNullOrEmpty(UAccount.Name))
                                    {
                                        UAccount.Name = UAccount.Name + " " + UAccount.LastName;
                                    }
                                }
                                if (!string.IsNullOrEmpty(UAccount.FirstName))
                                {
                                    UAccount.DisplayName = UAccount.FirstName;
                                }
                                else
                                {
                                    UAccount.DisplayName = UAccount.MobileNumber;
                                }
                                if (_RequestContent.dateOfBirth != null)
                                {
                                    UAccount.DateOfBirth = _RequestContent.dateOfBirth;
                                }
                                UAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                UAccount.ModifyById = _Request.UserReference.AccountId;
                                _HCoreContext.SaveChanges();
                                if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                                {
                                    if (_Request.UserReference.AccountId == 583505)
                                    {
                                        if (!string.IsNullOrEmpty(_RequestContent.emailAddress)) // Wakanow email
                                        {
                                            string Template = "";
                                            string currentDirectory = Directory.GetCurrentDirectory();
                                            string path = "templates";
                                            string fullPath = Path.Combine(currentDirectory, path, "wakapoints_emailtemplate.html");
                                            using (StreamReader reader = File.OpenText(fullPath))
                                            {
                                                Template = reader.ReadToEnd();
                                            }
                                            var _EmailParameters = new
                                            {
                                                MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                MerchantDisplayName = "WakaPoints",
                                                UserDisplayName = _RequestContent.firstName,
                                                AccountNumber = UAccount.AccountCode,
                                                Pin = HCoreEncrypt.DecryptHash(UAccount.AccessPin),
                                            };

                                            Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                            Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                            Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                            Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                            Template = Template.Replace("{{Pin}}", _EmailParameters.Pin);

                                            var client = new SendGridClient("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");
                                            var msg = new SendGridMessage()
                                            {
                                                From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                                Subject = "Welcome to WakaPoints",
                                                HtmlContent = Template
                                            };
                                            msg.AddTo(new EmailAddress(_RequestContent.emailAddress, _RequestContent.firstName));
                                            var response = client.SendEmailAsync(msg);
                                        }
                                        //if (!string.IsNullOrEmpty(_RequestContent.mobileNumber))
                                        //{
                                        //    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestContent.mobileNumber, "Welcome to WakaPoints. Your WakaPoints ID:" + AccountCode + " To redeem your point, use pin: " + AccessPin + ". Book now on www.wakanow.com ", _HCUAccount.Id, null);
                                        //}
                                    }
                                }
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CAO007", "Profile updated");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CAO008", "Invalid pin");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO004", "Account not active. Contact support");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO005", "Account not registered");
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
        internal OResponse ResetPin(OOpenApi.Balance.ERequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                string _RequestContentS = HCoreEncrypt.DecodeText(_Request.request);
                OOpenApi.Balance.Request _RequestContent = JsonConvert.DeserializeObject<OOpenApi.Balance.Request>(_RequestContentS);
                if (string.IsNullOrEmpty(_RequestContent.accountNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Account number required");
                    #endregion
                }
                else
                {
                    if (_Request.UserReference.AccountTypeId != UserAccountType.Merchant)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid call");
                        #endregion
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        //var MobileNumberValidate = HCoreHelper.ValidateMobileNumber(_RequestContent.accountNumber);
                        //if (!MobileNumberValidate.IsNumberValid)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid mobile number");
                        //}
                        //var CountryDetails = _HCoreContext.HCCoreCountry
                        //    .Where(x => x.Isd == MobileNumberValidate.Isd)
                        //    .Select(x => new
                        //    {
                        //        CountryId = x.Id,
                        //        Isd = x.Isd,
                        //        Name = x.Name
                        //    }).FirstOrDefault();
                        //if (CountryDetails == null)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Service is not active for your country");
                        //}
                        //string MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, MobileNumberValidate.MobileNumber);
                        string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _RequestContent.accountNumber);
                        string tAccountCode = _RequestContent.accountNumber;
                        _RequestContent.accountNumber = MobileNumber;
                        string FormatM = _AppConfig.AppUserPrefix + MobileNumber;
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == FormatM || x.AccountCode == tAccountCode))
                                .FirstOrDefault();
                        if (UAccount != null)
                        {
                            if (!string.IsNullOrEmpty(UAccount.EmailAddress)) // Wakanow email
                            {
                                string NewPin = HCoreHelper.GenerateRandomNumber(4);
                                UAccount.AccessPin = HCoreEncrypt.EncryptHash(NewPin);
                                UAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();



                                string Template = "";
                                string currentDirectory = Directory.GetCurrentDirectory();
                                string path = "templates";
                                string fullPath = Path.Combine(currentDirectory, path, "wakapoints_pinreset.html");
                                using (StreamReader reader = File.OpenText(fullPath))
                                {
                                    Template = reader.ReadToEnd();
                                }
                                var _EmailParameters = new
                                {
                                    MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                    MerchantDisplayName = "WakaPoints",
                                    UserDisplayName = UAccount.FirstName,
                                    AccountNumber = UAccount.AccountCode,
                                    Pin = NewPin,
                                };

                                Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                Template = Template.Replace("{{Pin}}", _EmailParameters.Pin);

                                var client = new SendGridClient("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");
                                var msg = new SendGridMessage()
                                {
                                    From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                    Subject = "WakaPoints Redeem Pin Reset",
                                    HtmlContent = Template
                                };
                                msg.AddTo(new EmailAddress(UAccount.EmailAddress, UAccount.FirstName));
                                var response = client.SendEmailAsync(msg);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CAO005", "New pin has been sent to your registered email address.");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO005", "Email address is not linked to account. Please contact support");
                            }


                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO005", "Account not registered");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
        internal OResponse GetCountries(OOpenApi.ERequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Countries = _HCoreContext.HCCoreCountry.Where(x => x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Isd = x.Isd
                        })
                        .OrderBy(x => x.Name).ToList();
                    var ListDetails = HCoreHelper.GetListResponse(Countries.Count, Countries, 0, Countries.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ListDetails, "CAO007", "Results loaded");
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
        internal OResponse GetBalance(OOpenApi.Balance.ERequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                string _RequestContentS = HCoreEncrypt.DecodeText(_Request.request);
                OOpenApi.Balance.Request _RequestContent = JsonConvert.DeserializeObject<OOpenApi.Balance.Request>(_RequestContentS);
                if (string.IsNullOrEmpty(_RequestContent.accountNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Account number required");
                    #endregion
                }
                else
                {
                    if (_Request.UserReference.AccountTypeId != UserAccountType.Merchant)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid call");
                        #endregion
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        //var MobileNumberValidate = HCoreHelper.ValidateMobileNumber(_RequestContent.accountNumber);
                        //if (!MobileNumberValidate.IsNumberValid)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid mobile number");
                        //}
                        //var CountryDetails = _HCoreContext.HCCoreCountry
                        //    .Where(x => x.Isd == MobileNumberValidate.Isd)
                        //    .Select(x => new
                        //    {
                        //        CountryId = x.Id,
                        //        Isd = x.Isd,
                        //        Name = x.Name
                        //    }).FirstOrDefault();
                        //if (CountryDetails == null)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Service is not active for your country");
                        //}
                        //string MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, MobileNumberValidate.MobileNumber);
                        string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _RequestContent.accountNumber);
                        string tAccountCode = _RequestContent.accountNumber;
                        _RequestContent.accountNumber = MobileNumber;
                        string FormatM = _AppConfig.AppUserPrefix + MobileNumber;
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == FormatM || x.AccountCode == tAccountCode))
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    Pin = x.AccessPin,
                                    StatusId = x.StatusId,
                                    DisplayName = x.DisplayName,
                                    Name = x.Name,
                                    FirstName = x.FirstName,
                                    LastName = x.LastName,
                                    AccountCode = x.AccountCode,
                                }).FirstOrDefault();
                        if (UAccount != null)
                        {
                            string DApin = HCoreEncrypt.DecryptHash(UAccount.Pin);
                            if (DApin == _RequestContent.pin)
                            {
                                if (UAccount.StatusId == HelperStatus.Default.Active)
                                {
                                    _BalanceResponse = new OOpenApi.Balance.Response();
                                    _BalanceResponse.displayName = UAccount.DisplayName;
                                    _BalanceResponse.firstName = UAccount.FirstName;
                                    _BalanceResponse.lastName = UAccount.LastName;
                                    _BalanceResponse.accountNumber = UAccount.AccountCode;

                                    string AccKey = HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(UAccount.AccountKey));
                                    _BalanceResponse.profileCode = AccKey;
                                    _CoreOperations = new CoreOperations();
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    //long ThankUCashPlusIsEnable = Convert.ToInt64(_CoreOperations.GetConfiguration("thankucashplus", _Request.UserReference.AccountId));
                                    long TucGold = Convert.ToInt64(_CoreOperations.GetConfiguration("thankucashgold", _Request.UserReference.AccountId));
                                    if (TucGold > 0)
                                    {
                                        _BalanceResponse.balance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(UAccount.AccountId, _Request.UserReference.AccountId, TransactionSource.TUCBlack) * 100, 0);
                                    }
                                    else
                                    {
                                        _BalanceResponse.balance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(UAccount.AccountId) * 100, 0);
                                    }
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "CAO007", "Balance loaded");
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO004", "Account not active. Contact support");
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO006", "Invalid account pin");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO005", "Account not registered");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
        internal OResponse Register(OOpenApi.Register.ERequest _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                string _RequestContentS = HCoreEncrypt.DecodeText(_Request.request);
                OOpenApi.Register.Request _RequestContent = JsonConvert.DeserializeObject<OOpenApi.Register.Request>(_RequestContentS);
                if (string.IsNullOrEmpty(_RequestContent.mobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Mobile number required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_RequestContent.firstName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "First name required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_RequestContent.lastName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Last name required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_RequestContent.emailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Email address required");
                    #endregion
                }
                else if (_RequestContent.dateOfBirth == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Date of birth required");
                    #endregion
                }
                else
                {
                    if (_Request.UserReference.AccountTypeId != UserAccountType.Merchant)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid call");
                        #endregion
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        //var MobileNumberValidate = HCoreHelper.ValidateMobileNumber(_RequestContent.mobileNumber);
                        //if (!MobileNumberValidate.IsNumberValid)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid mobile number");
                        //}
                        //var CountryDetails = _HCoreContext.HCCoreCountry
                        //    .Where(x => x.Isd == MobileNumberValidate.Isd)
                        //    .Select(x => new
                        //    {
                        //        CountryId = x.Id,
                        //        Isd = x.Isd,
                        //        Name = x.Name
                        //    }).FirstOrDefault();
                        //if (CountryDetails == null)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Service is not active for your country");
                        //}
                        //string MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, MobileNumberValidate.MobileNumber);
                        if (!string.IsNullOrEmpty(_RequestContent.countryIsd))
                        {
                            var CountryDetails = _HCoreContext.HCCoreCountry
                                .Where(x => x.Isd == _RequestContent.countryIsd)
                                .Select(x => new
                                {
                                    CountryId = x.Id,
                                    Isd = x.Isd,
                                    Name = x.Name,
                                    Length = x.MobileNumberLength,
                                }).FirstOrDefault();
                            if (CountryDetails == null)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Service is not active for your country");
                            }
                            _RequestContent.mobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, _RequestContent.mobileNumber, CountryDetails.Length);
                        }
                        else
                        {
                            _RequestContent.mobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _RequestContent.mobileNumber, 10);
                        }


                        string CustomerAppId = _AppConfig.AppUserPrefix + _RequestContent.mobileNumber;
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == CustomerAppId))
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    Pin = x.AccessPin,
                                    StatusId = x.StatusId
                                }).FirstOrDefault();
                        if (UAccount == null)
                        {
                            var UAccountEmailCheck = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.EmailAddress == _RequestContent.emailAddress))
                               .Select(x => new
                               {
                                   AccountId = x.Id,
                                   Pin = x.AccessPin,
                                   StatusId = x.StatusId
                               }).FirstOrDefault();
                            if (UAccountEmailCheck != null)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CACUST1424", "Email address already exists");
                            }
                            #region Manage Operations
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region  Process Registration
                                //_RequestContent.mobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _RequestContent.mobileNumber);
                                //string CustomerAppId = _AppConfig.AppUserPrefix + _RequestContent.mobileNumber;
                                DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                                int GenderId = 0;
                                string MerchantDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.DisplayName).FirstOrDefault();
                                //if (_RequestContent.OwnerId != 0)
                                //{
                                //    MerchantDisplayName = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _RequestContent.OwnerId).Select(x => x.DisplayName).FirstOrDefault();
                                //    if (string.IsNullOrEmpty(MerchantDisplayName))
                                //    {
                                //        MerchantDisplayName = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _RequestContent.OwnerId && (x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.Partner)).Select(x => x.DisplayName).FirstOrDefault();
                                //    }
                                //}
                                if (!string.IsNullOrEmpty(_RequestContent.gender))
                                {
                                    if (_RequestContent.gender == "gender.male" || _RequestContent.gender == "male")
                                    {
                                        GenderId = Gender.Male;
                                    }
                                    if (_RequestContent.gender == "gender.female" || _RequestContent.gender == "female")
                                    {
                                        GenderId = Gender.Female;
                                    }
                                    if (_RequestContent.gender == "gender.other" || _RequestContent.gender == "other")
                                    {
                                        GenderId = Gender.Other;
                                    }
                                }
                                string UserKey = HCoreHelper.GenerateGuid();
                                string UserAccountKey = HCoreHelper.GenerateGuid();
                                _Random = new Random();
                                string AccessPin = _Random.Next(1111, 9999).ToString();
                                if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    AccessPin = "1234";
                                }
                                #region Save User
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = UserKey;
                                _HCUAccountAuth.Username = CustomerAppId;
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_RequestContent.mobileNumber);
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = CreateDate;
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                #endregion
                                #region Online Account
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = UserAccountKey;
                                _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                _HCUAccount.OwnerId = _Request.UserReference.AccountId;
                                _HCUAccount.MobileNumber = _RequestContent.mobileNumber;
                                _HCUAccount.DisplayName = _RequestContent.firstName;
                                _HCUAccount.FirstName = _RequestContent.firstName;
                                _HCUAccount.MiddleName = _RequestContent.middleName;
                                _HCUAccount.LastName = _RequestContent.lastName;
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                                _HCUAccount.Name = _RequestContent.firstName + " " + _RequestContent.lastName;
                                _HCUAccount.ContactNumber = _RequestContent.mobileNumber;
                                _HCUAccount.EmailAddress = _RequestContent.emailAddress;
                                _HCUAccount.SecondaryEmailAddress = _RequestContent.emailAddress;
                                if (!string.IsNullOrEmpty(_RequestContent.address))
                                {
                                    _HCUAccount.Address = _RequestContent.address;
                                }
                                if (GenderId != 0)
                                {
                                    _HCUAccount.GenderId = GenderId;
                                }
                                if (_RequestContent.dateOfBirth != null)
                                {
                                    _HCUAccount.DateOfBirth = _RequestContent.dateOfBirth;
                                }
                                _HCUAccount.CountryId = _Request.UserReference.CountryId;
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = CreateDate;
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatusDate = CreateDate;
                                _Random = new Random();
                                //string AccountCode = "20" + _Random.Next(0000000, 9999999).ToString() + _Random.Next(00, 99).ToString();
                                string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                _HCUAccount.AccountCode = AccountCode;
                                _HCUAccount.ReferralCode = _RequestContent.mobileNumber;
                                if (_Request.UserReference.AccountId == 583505)
                                {
                                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.Website;
                                }
                                else
                                {
                                    _HCUAccount.RegistrationSourceId = 764;
                                    _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.OpenApi;
                                }
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }

                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.CreateDate = CreateDate;
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.User = _HCUAccountAuth;
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();

                                #region Add Merchant Customer
                                using (_HCoreContext = new HCoreContext())
                                {
                                    #region Track Custoemr 
                                    _cmt_loyalty_customer = new cmt_loyalty_customer();
                                    _cmt_loyalty_customer.guid = HCoreHelper.GenerateGuid();
                                    _cmt_loyalty_customer.customer_id = _HCUAccount.Id;
                                    _cmt_loyalty_customer.owner_id = _Request.UserReference.AccountId;
                                    _cmt_loyalty_customer.source_id = TransactionSource.TUC;
                                    _cmt_loyalty_customer.credit = 0;
                                    _cmt_loyalty_customer.debit = 0;
                                    _cmt_loyalty_customer.balance = 0;
                                    _cmt_loyalty_customer.invoice_amount = 0;

                                    _cmt_loyalty_customer.status_id = HelperStatus.Default.Active;
                                    _cmt_loyalty_customer.create_date = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.cmt_loyalty_customer.Add(_cmt_loyalty_customer);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                }
                                #endregion
                                #endregion
                                if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                                {
                                    if (_Request.UserReference.AccountId == 583505)
                                    {
                                        if (!string.IsNullOrEmpty(_RequestContent.emailAddress)) // Wakanow email
                                        {
                                            string Template = "";
                                            string currentDirectory = Directory.GetCurrentDirectory();
                                            string path = "templates";
                                            string fullPath = Path.Combine(currentDirectory, path, "wakapoints_emailtemplate.html");
                                            using (StreamReader reader = File.OpenText(fullPath))
                                            {
                                                Template = reader.ReadToEnd();
                                            }
                                            var _EmailParameters = new
                                            {
                                                MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                MerchantDisplayName = "WakaPoints",
                                                UserDisplayName = _RequestContent.firstName,
                                                AccountNumber = AccountCode,
                                                Pin = AccessPin,
                                            };

                                            Template = Template.Replace("{{MerchantLogo}}", _EmailParameters.MerchantLogo);
                                            Template = Template.Replace("{{MerchantDisplayName}}", _EmailParameters.MerchantDisplayName);
                                            Template = Template.Replace("{{UserDisplayName}}", _EmailParameters.UserDisplayName);
                                            Template = Template.Replace("{{AccountNumber}}", _EmailParameters.AccountNumber);
                                            Template = Template.Replace("{{Pin}}", _EmailParameters.Pin);

                                            var client = new SendGridClient("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");
                                            var msg = new SendGridMessage()
                                            {
                                                From = new EmailAddress("noreply@wakanow.com", "WakaPoints"),
                                                Subject = "Welcome to WakaPoints",
                                                HtmlContent = Template
                                            };
                                            msg.AddTo(new EmailAddress(_RequestContent.emailAddress, _RequestContent.firstName));
                                            var response = client.SendEmailAsync(msg);
                                        }
                                        if (!string.IsNullOrEmpty(_RequestContent.mobileNumber))
                                        {
                                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestContent.mobileNumber, "Welcome to WakaPoints. Your WakaPoints ID:" + AccountCode + " To redeem your point, use pin: " + AccessPin + ". Book now on www.wakanow.com ", _HCUAccount.Id, null);
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(_RequestContent.emailAddress))
                                        {
                                            var _EmailParameters = new
                                            {
                                                MerchantLogo = "https://points.wakanow.com/assets/img/wakanow-logo.png",
                                                MerchantDisplayName = "Wakanow",
                                                UserDisplayName = _RequestContent.firstName,
                                                AccountNumber = AccountCode,
                                                Pin = AccessPin,
                                            };
                                            HCoreHelper.BroadCastEmail("d-0acc0a553a8a46eebb31c4179c026741", _RequestContent.firstName, _RequestContent.emailAddress, _EmailParameters, _Request.UserReference);
                                        }
                                        if (!string.IsNullOrEmpty(_RequestContent.mobileNumber))
                                        {
                                            if (!string.IsNullOrEmpty(MerchantDisplayName))
                                            {
                                                HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestContent.mobileNumber, "Welcome to " + MerchantDisplayName + " Points. To redeem your cash, use pin: " + AccessPin + ".", _HCUAccount.Id, null);
                                            }
                                        }
                                    }
                                }
                                var _Response = new
                                {
                                    accountNumber = AccountCode,
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA2000", "Account registration successful");
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                #region Track Custoemr 
                                _cmt_loyalty_customer = new cmt_loyalty_customer();
                                _cmt_loyalty_customer.guid = HCoreHelper.GenerateGuid();
                                _cmt_loyalty_customer.customer_id = UAccount.AccountId;
                                _cmt_loyalty_customer.owner_id = _Request.UserReference.AccountId;
                                _cmt_loyalty_customer.source_id = TransactionSource.TUC;
                                _cmt_loyalty_customer.credit = 0;
                                _cmt_loyalty_customer.debit = 0;
                                _cmt_loyalty_customer.balance = 0;
                                _cmt_loyalty_customer.invoice_amount = 0;
                                _cmt_loyalty_customer.status_id = HelperStatus.Default.Active;
                                _cmt_loyalty_customer.create_date = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.cmt_loyalty_customer.Add(_cmt_loyalty_customer);
                                _HCoreContext.SaveChanges();
                                #endregion
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CACUST1423", "Account already registered");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "Register", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
        internal OResponse RegisterWeb(OOpenApi.Register.ERequest _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.request))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO001", "Invalid request");
                }
                string _RequestContentS = HCoreEncrypt.DecodeText(_Request.request);
                OOpenApi.Register.Request _RequestContent = JsonConvert.DeserializeObject<OOpenApi.Register.Request>(_RequestContentS);
                if (string.IsNullOrEmpty(_RequestContent.mobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Mobile number required");
                    #endregion
                }
                if (_RequestContent.mobileNumber.Length < 9)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Invalid mobile number");
                }
                if (_RequestContent.mobileNumber.Length > 16)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Invalid mobile number");
                }
                //else if (string.IsNullOrEmpty(_RequestContent.firstName))
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "First name required");
                //    #endregion
                //}
                //else if (string.IsNullOrEmpty(_RequestContent.lastName))
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Last name required");
                //    #endregion
                //}
                //else if (string.IsNullOrEmpty(_RequestContent.emailAddress))
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Email address required");
                //    #endregion
                //}
                //else if (_RequestContent.dateOfBirth == null)
                //{
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Date of birth required");
                //    #endregion
                //}
                else
                {
                    if (_Request.UserReference.AppId != 5921)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO005", "Invalid call");
                        #endregion
                    }
                    if (_Request.UserReference.AccountTypeId != UserAccountType.Controller)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid call");
                        #endregion
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var MobileNumberValidate = HCoreHelper.ValidateMobileNumber(_RequestContent.mobileNumber);
                        if (!MobileNumberValidate.IsNumberValid)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Invalid mobile number");
                        }
                        var CountryDetails = _HCoreContext.HCCoreCountry
                            .Where(x => x.Isd == MobileNumberValidate.Isd)
                            .Select(x => new
                            {
                                CountryId = x.Id,
                                Isd = x.Isd,
                                Name = x.Name
                            }).FirstOrDefault();
                        if (CountryDetails == null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Service is not active for your country");
                        }
                        string MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, MobileNumberValidate.MobileNumber);
                        _RequestContent.mobileNumber = MobileNumber;
                        string CustomerAppId = _AppConfig.AppUserPrefix + MobileNumber;
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == CustomerAppId))
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    Pin = x.AccessPin,
                                    StatusId = x.StatusId,
                                    AccountCode = x.AccountCode,
                                }).FirstOrDefault();
                        if (UAccount == null)
                        {
                            #region Manage Operations
                            using (_HCoreContext = new HCoreContext())
                            {

                                #region  Process Registration
                                DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                                int GenderId = 0;
                                string MerchantDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.DisplayName).FirstOrDefault();
                                //if (_RequestContent.OwnerId != 0)
                                //{
                                //    MerchantDisplayName = DataStore.DataStore.Merchants.Where(x => x.ReferenceId == _RequestContent.OwnerId).Select(x => x.DisplayName).FirstOrDefault();
                                //    if (string.IsNullOrEmpty(MerchantDisplayName))
                                //    {
                                //        MerchantDisplayName = DataStore.DataStore.Partners.Where(x => x.ReferenceId == _RequestContent.OwnerId && (x.AccountTypeId == UserAccountType.Acquirer || x.AccountTypeId == UserAccountType.Partner)).Select(x => x.DisplayName).FirstOrDefault();
                                //    }
                                //}
                                if (!string.IsNullOrEmpty(_RequestContent.gender))
                                {
                                    if (_RequestContent.gender == "gender.male" || _RequestContent.gender == "male")
                                    {
                                        GenderId = Gender.Male;
                                    }
                                    if (_RequestContent.gender == "gender.female" || _RequestContent.gender == "female")
                                    {
                                        GenderId = Gender.Female;
                                    }
                                    if (_RequestContent.gender == "gender.other" || _RequestContent.gender == "other")
                                    {
                                        GenderId = Gender.Other;
                                    }
                                }
                                string UserKey = HCoreHelper.GenerateGuid();
                                string UserAccountKey = HCoreHelper.GenerateGuid();
                                _Random = new Random();
                                string AccessPin = _Random.Next(1111, 9999).ToString();
                                if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    AccessPin = "1234";
                                }
                                #region Save User
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = UserKey;
                                _HCUAccountAuth.Username = CustomerAppId;
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_RequestContent.mobileNumber);
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = CreateDate;
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                #endregion
                                #region Online Account
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = UserAccountKey;
                                _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                _HCUAccount.OwnerId = _Request.UserReference.AccountId;
                                _HCUAccount.MobileNumber = _RequestContent.mobileNumber;
                                if (!string.IsNullOrEmpty(_RequestContent.firstName))
                                {
                                    _HCUAccount.DisplayName = _RequestContent.firstName;
                                }
                                else
                                {
                                    _HCUAccount.DisplayName = _RequestContent.mobileNumber;
                                }
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                                _HCUAccount.Name = _RequestContent.firstName + " " + _RequestContent.lastName;
                                _HCUAccount.ContactNumber = _RequestContent.mobileNumber;
                                _HCUAccount.EmailAddress = _RequestContent.emailAddress;
                                _HCUAccount.SecondaryEmailAddress = _RequestContent.emailAddress;
                                if (!string.IsNullOrEmpty(_RequestContent.address))
                                {
                                    _HCUAccount.Address = _RequestContent.address;
                                }
                                if (GenderId != 0)
                                {
                                    _HCUAccount.GenderId = GenderId;
                                }
                                if (_RequestContent.dateOfBirth != null)
                                {
                                    _HCUAccount.DateOfBirth = _RequestContent.dateOfBirth;
                                }
                                _HCUAccount.CountryId = _Request.UserReference.CountryId;
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = CreateDate;
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatusDate = CreateDate;
                                _Random = new Random();
                                string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                _HCUAccount.AccountCode = AccountCode;
                                _HCUAccount.ReferralCode = _RequestContent.mobileNumber;
                                _HCUAccount.RegistrationSourceId = 763;
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }

                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.CreateDate = CreateDate;
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.User = _HCUAccountAuth;
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                                #endregion
                                if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestContent.mobileNumber, "Welcome to ThankUCash. Your ThankUCash ID:" + AccountCode + " To redeem your cash, use pin: " + AccessPin + ". Download App to see more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                                    //if (!string.IsNullOrEmpty(_RequestContent.mobileNumber))
                                    //{
                                    //    if (_Request.UserReference.AccountId == 583505) // WAKANOW
                                    //    {
                                    //        //Welcome to Trebet Superstore.  Your ThankUCash ID: 12345678901 To redeem your cash, use pin: 1234. Download App to see more benefits: https://bit.ly/tuc-app
                                    //        //d-0acc0a553a8a46eebb31c4179c026741 
                                    //        //Welcome to WakaPoints. Your WakaPoints ID: 12345678901 To redeem your point use pin: 1234. Download App to see more benefits: https://bit.ly/tuc-app
                                    //    }
                                    //    else
                                    //    {
                                    //        if (!string.IsNullOrEmpty(MerchantDisplayName))
                                    //        {
                                    //            HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestContent.mobileNumber, "Welcome to ThankUCash Points. To redeem your cash, use pin: " + AccessPin + ".", _HCUAccount.Id, null);
                                    //        }
                                    //    }
                                    //}
                                }
                                var _Response = new
                                {
                                    accountNumber = AccountCode,
                                };
                                //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA2000", "Account registration successful");
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA2000", "App download link sent to your mobile number");
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            HCoreHelper.SendSMS(SmsType.Transaction, "234", _RequestContent.mobileNumber, "Welcome to ThankUCash. Your ThankUCash ID:" + UAccount.AccountCode + " To redeem your cash, use pin: " + HCoreEncrypt.DecryptHash(UAccount.Pin) + ". Download App to see more benefits: https://bit.ly/tuc-app", UAccount.AccountId, null);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CAO005", "App download link sent to your mobile number");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "RegisterWeb", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, CoreResources.HCG500, CoreResources.HCG500M);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Delete Account: Updates the statusId from Active to InActive
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal DeleteCustomerInfo.Response DeleteCustomer(DeleteCustomerInfo.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return new DeleteCustomerInfo.Response
                    {
                        ResponseMessage = "Invalid request",
                        StatusCode = (int)HttpStatusCode.BadRequest
                    };
                }
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var accountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                               && x.MobileNumber == _Request.MobileNumber).FirstOrDefault();
                    if (accountDetails != null)
                    {
                        var customerStatus = _HCoreContext.cmt_loyalty_customer.Where(x => x.customer_id == accountDetails.Id && x.owner_id == _Request.MerchantId).FirstOrDefault();
                        if (customerStatus != null)
                        {
                            customerStatus.status_id = HelperStatus.Default.Blocked;
                            _HCoreContext.SaveChanges();
                            return new DeleteCustomerInfo.Response
                            {
                                ResponseMessage = "Deleted",
                                StatusCode = (int)HttpStatusCode.OK
                            };
                        }
                        else
                        {
                            // Save TUCLoyaltyMerchantCustomer
                            cmt_loyalty_customer _cmt_loyalty_customer = new cmt_loyalty_customer();
                            _cmt_loyalty_customer.customer_id = accountDetails.Id;
                            _cmt_loyalty_customer.owner_id = (long)_Request.MerchantId;
                            _cmt_loyalty_customer.status_id = HelperStatus.Default.Active;
                            _cmt_loyalty_customer.source_id = TransactionSource.TUC;
                            _cmt_loyalty_customer.create_date = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.cmt_loyalty_customer.Add(_cmt_loyalty_customer);
                            _HCoreContext.SaveChanges();
                            return new DeleteCustomerInfo.Response
                            {
                                ResponseMessage = "Deleted",
                                StatusCode = (int)HttpStatusCode.OK
                            };
                        }
                    }
                    else
                    {
                        return new DeleteCustomerInfo.Response
                        {
                            ResponseMessage = "User not found",
                            StatusCode = (int)HttpStatusCode.NotFound
                        };
                    }
                }

            }
            catch (Exception exception)
            {
                HCoreHelper.LogException("Deleted Customer", exception);
                return new DeleteCustomerInfo.Response
                {
                    ResponseMessage = "Internal Server error",
                    StatusCode = (int)HttpStatusCode.InternalServerError
                };
            }
        }
    }
}
