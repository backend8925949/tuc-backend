﻿using System;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals;
using HCore.TUC.Plugins.MadDeals.Object;
using Microsoft.AspNetCore.Mvc;

namespace HCore.Api.ThankUCash.MadDeals
{
    [Produces("application/json")]
    [Route("api/maddeals/search/[action]")]
	public class DealWebSearchController
	{
		ManageDealsWebSearch? _ManageDealsWebSearch;

        [HttpPost]
        [ActionName("savesearch")]
		public async Task<object> SaveSearch([FromBody] ODealsWebSearch.Save.Request _Request)
		{
			_ManageDealsWebSearch = new ManageDealsWebSearch();
			OResponse _OResponse = await _ManageDealsWebSearch.SaveSearch(_Request);
			return HCoreAuth.Auth_ResponseDefaultObject(_OResponse);
        }

        [HttpPost]
        [ActionName("getsearch")]
        public async Task<object> GetSearch([FromBody] OList.Request _Request)
        {
            _ManageDealsWebSearch = new ManageDealsWebSearch();
            OResponse _OResponse = await _ManageDealsWebSearch.GetSearch(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_OResponse);
        }
    }
}

