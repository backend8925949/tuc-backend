//==================================================================================
// FileName: AccountController.cs
// Author : Harshal Gandole
// Created On : 
// Description : controller to manage users(app) related operations 
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Connect.Object;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Object.Operations;
using HCore.TUC.Core.Operations.Customer;
using HCore.TUC.Core.Operations.CustomerWeb;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using HCore.Operations;
using HCore.TUC.Plugins.Operations;
using HCore.TUC.Plugins.Accounts;
using HCore.TUC.Plugins.Accounts.Objects;


namespace HCore.Api.ThankUCash.MadDeals
{
    [Produces("application/json")]
    [Route("api/maddeals/account/[action]")]
    public class AccountController
    {
        ManageAccount? _ManageAccount;
        ManageVerification? _ManageVerification;
        ManageBuyPoint? _ManageBuyPoint;
        ManageCoreVerification? _ManageCoreVerification;
        ManageWallet? _ManageWallet;
        ManageMerchantOnboarding? _ManageMerchantOnboarding;

        /// <summary>
        /// Request OTP
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("requestotp")]
        public object RequestOtp([FromBody] OCoreVerificationManager.Request _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.RequestOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Verify OTP
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("verifyotp")]
        public object VerifyOtp([FromBody] OCoreVerificationManager.RequestVerify _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.VerifyOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Add users details who want to check out as guest.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("guestcheckout")]
        public object GuestCheckout([FromBody] OCoreVerificationManager.RequestGuestCheckout _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.GuestCheckout(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Upadate profile of user
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateaccount")]
        public object UpdateAccount([FromBody] TUC.Core.Object.CustomerWeb.OAccount.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Upadate referral code of user
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updaterefid")]
        public object UpdateReferralId([FromBody] TUC.Core.Object.CustomerWeb.OAccount.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.UpdateReferralId(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        
        /// <summary>
        /// Gets the App Logo
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getapplogo")]
        public object GetAppLogo([FromBody] HCore.TUC.Core.Object.Console.OAdminUser.AuthUpdate.LogoRequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAppLogo(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get users account details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccount")]
        public object GetAccount([FromBody] TUC.Core.Object.CustomerWeb.OAccount.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get users account balance
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaccountbalance")]
        public object GetAccountBalance([FromBody] TUC.Core.Object.CustomerWeb.OAccount.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetAccountBalance(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Validate users access pin
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("validatepin")]
        public object ValidatePin([FromBody] TUC.Core.Object.CustomerWeb.OAccount.ValidatePin.Request _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.ValidatePin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        ManagePayment _ManagePayment;

        /// <summary>
        /// initialize users transaction
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("buypointinitialize")]
        public object BuyPointInitialize([FromBody] OBuyPoint.Initialize.Request _Request)
        {
            _ManagePayment = new ManagePayment();
            OResponse _Response = _ManagePayment.BuyPointInitialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Verify users transaction 
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("buypointverify")]
        public object BuyPointVerify([FromBody] OBuyPoint.Verify.Request _Request)
        {
            _ManagePayment = new ManagePayment();
            OResponse _Response = _ManagePayment.BuyPointVerify(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// cancel users transaction
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("buypointcancel")]
        public object BuyPointCancel([FromBody] OBuyPoint.Cancel.Request _Request)
        {
            _ManagePayment = new ManagePayment();
            OResponse _Response = _ManagePayment.BuyPointCancel(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        // <summary>
        /// Credits user wallet.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("fundwallet")]
        public object CreditWallet([FromBody] HCore.TUC.Plugins.Operations.OOperations.Wallet.Credit.Request _OAuthRequest)
        {
            _ManageWallet = new ManageWallet();
            OResponse _Response = _ManageWallet.FundWallet(_OAuthRequest);
            return HCoreAuth.Auth_Response(_Response, _OAuthRequest.UserReference);
        }


        // <summary>
        /// Gets referral bonus amount.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getreferralamount")]
        public object GetReferralBonusAmount([FromBody] HCore.TUC.Core.Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetReferralAmount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        
        [HttpPost]
        [ActionName("setreferralamount")]
        public object SetReferralBonusAmount([FromBody] HCore.TUC.Core.Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.SetReferralAmount(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        // <summary>
        /// Gets referral bonus history for a user.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getreferralbonushistory")]
        public object GetReferralBonusHoistory([FromBody] HCore.TUC.Core.Object.CustomerWeb.OAccount.ReferralAmountRequest _Request)
        {
            _ManageAccount = new ManageAccount();
            OResponse _Response = _ManageAccount.GetBonusHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        #region Address Manager
        ManageAddress _ManageAddress;
        /// <summary>
        /// Save users address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("saveaddress")]
        public object SaveAddress([FromBody] OAddressManager.Save.Request _Request)
        {
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.SaveAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// update users address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("updateaddress")]
        public object UpdateAddress([FromBody] OAddressManager.Save.Request _Request)
        {
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.UpdateAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Delete users address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("deleteaddress")]
        public object DeleteAddress([FromBody] OAddressManager.Save.Request _Request)
        {
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.DeleteAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get users address
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaddress")]
        public object GetAddress([FromBody] OReference _Request)
        {
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.GetAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get address list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getaddresslist")]
        public object GetAddressList([FromBody] OList.Request _Request)
        {
            _ManageAddress = new ManageAddress();
            OResponse _Response = _ManageAddress.GetAddress(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
        #endregion


        /// <summary>
        /// generate signup otp
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("requestsignupotp")]
        public object RequestSignupOtp([FromBody] OCoreVerificationManager.Request _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.RequestSignupOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// verify otp for signup otp
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("verifysignupotp")]
        public object VerifySignupOtp([FromBody] OCoreVerificationManager.RequestVerify _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.VerifySignupOtp(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// signup user
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("signupuser")]
        public object SignupUser([FromBody] OCoreVerificationManager.RequestSignupUser _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.SignupUser(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Signin user 
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("signinuser")]
        public object SigninUser([FromBody] OCoreVerificationManager.RequestSignIn _Request)
        {
            _ManageVerification = new ManageVerification();
            OResponse _Response = _ManageVerification.SigninUser(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the account point purchase history.
        /// </summary>
        /// <param name="_OAuthRequest">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getpointpurchasehistory")]
        public object GetAccountPointPurchaseHistory([FromBody] OList.Request _Request)
        {
            _ManageBuyPoint = new ManageBuyPoint();
            OResponse _Response = _ManageBuyPoint.GetAccountPointPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("getmerchant")]
        public async Task<object> GetMerchantDetails([FromBody] OReference _Request)
        {
            _ManageMerchantOnboarding = new ManageMerchantOnboarding();
            OResponse _Response = await _ManageMerchantOnboarding.GetMerchantDetails(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
