//==================================================================================
// FileName: DealsController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Controller for deals website related operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Operations;
using HCore.TUC.Plugins.MadDeals;
using HCore.TUC.Plugins.MadDeals.Object;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static HCore.Helper.OList;
using static HCore.TUC.Plugins.Delivery.Objects.OOperations.Pricing;

namespace HCore.Api.ThankUCash.MadDeals
{
    [Produces("application/json")]
    [Route("api/maddeals/deals/[action]")]
    public class DealsController
    {
        ManageDeal? _ManageDeal;
        HCore.TUC.Core.Operations.CustomerWeb.ManageDeal? _ManageDealOp;
        ManageOrderProcess? _ManageOrderProcess;
        ManageOperations? _ManageOperations;

        /// <summary>
        /// get states
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstats")]
        public object GetStats([FromBody] OList.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetStats(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get categories list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcategories")]
        public object GetCategories([FromBody] OList.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetCategories(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// search states
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsearchstates")]
        public object GetSearchStates([FromBody] OList.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetSearchStates(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// search city
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsearchcities")]
        public object GetSearchCities([FromBody] OList.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetSearchCities(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get cities
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcities")]
        public object GetCities([FromBody] OList.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetCities(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get merchants list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getmerchants")]
        public object GetMerchants([FromBody] OList.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetMerchant(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get deals list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getdeals")]
        public object GetDeals([FromBody] OMadDeals.Deals.List.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetDeal(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get deal details
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getdeal")]
        public object GetDeal([FromBody] OMadDeals.Deal.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetDeal(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get total store reviews and average ratings
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstoreratings")]
        public object GetStoreRatings([FromBody] OMadDeals.Deal.StoreRequest _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetstoreRatings(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get total store full data
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstoredata")]
        public object GetStoreData([FromBody] OMadDeals.Deal.StoreRequest _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetstoreData(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("buydeal")]
        public object BuyDeal([FromBody] ODeal.Purchase.Request _Request)
        {
            _ManageDealOp = new TUC.Core.Operations.CustomerWeb.ManageDeal();
            OResponse _Response = _ManageDealOp.BuyDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get deal code
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getdealcode")]
        public object GetDealCode([FromBody] TUC.Plugins.Deals.Object.ODealOperation.DealCode.Request _Request)
        {
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetDealCode(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// deal purchase history
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getpurchasehistory")]
        public object GetPurchaseHistory([FromBody] OList.Request _Request)
        {
            _ManageDealOp = new TUC.Core.Operations.CustomerWeb.ManageDeal();
            OResponse _Response = _ManageDealOp.GetPurchaseHistory(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// product purchase/order history
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getproductpurchasehistory")]
        public object GetProductDeal([FromBody] OList.Request _Request)
        {
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.GetProductDeal(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get slider image
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getsliderimages")]
        public object GetSliderImages([FromBody] OMadDeals.Slider.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetSliderImages(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get promotional deal
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getdealpromotions")]
        public object GetDealPromotions([FromBody] OMadDeals.Slider.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetDealPromotions(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }


        ManageDealBookmark _ManageDealBookmark;
        /// <summary>
        /// save update bookmark
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("savebookmark")]
        public object SaveBookMark([FromBody] ODealBookmark.Save.Request _Request)
        {
            _ManageDealBookmark = new ManageDealBookmark();
            OResponse _Response = _ManageDealBookmark.SaveBookMark(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// get bookmark
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbookmarks")]
        public object GetBookmark([FromBody] OList.Request _Request)
        {
            _ManageDealBookmark = new ManageDealBookmark();
            OResponse _Response = _ManageDealBookmark.GetBookmark(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        /// <summary>
        /// save deal review
        /// </summary>
        ManageDealReview _ManageDealReview;
        [HttpPost]
        [ActionName("savedealreview")]
        public object SaveDealReview([FromBody] ODealReview.Save _Request)
        {
            _ManageDealReview = new ManageDealReview();
            OResponse _Response = _ManageDealReview.SaveDealReview(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }



        /// <summary>
        /// get deal review
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getdealreviews")]
        public object GetDealReview([FromBody] OList.Request _Request)
        {
            _ManageDealReview = new ManageDealReview();
            OResponse _Response = _ManageDealReview.GetDealReview(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        ManageDealOperation _ManageDealOperation;
        /// <summary>
        /// initialize deal buying process
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("buydeal_initizalize")]
        public object BuyDeal_Initialize([FromBody] TUC.Plugins.Deals.Object.ODealOperation.DealPayment.Initialize _Request)
        {
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.BuyDeal_Initialize(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// buy deal confirmation
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("buydeal_confirm")]
        public object BuyDeal_Confirm([FromBody] HCore.TUC.Plugins.Deals.Object.ODealOperation.DealPayment.Confirm _Request)
        {
            _ManageDealOperation = new ManageDealOperation();
            OResponse _Response = _ManageDealOperation.BuyDeal_Confirm(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// getting delivery price & delivery agent list
        /// </summary>
        [HttpPost]
        [ActionName("getdeliverypricing")]
        public async Task<object> GetDeliveryPricing([FromBody] GetDeliveryPricing _Request)
        {
            _ManageOperations = new ManageOperations();
            OResponse _Response = await _ManageOperations.GetDeliveryPricing(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// This API is to get the list of the promotional banners uploaded from the console and the merchant panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getbannerimages")]
        public object GetBannerImages([FromBody] OMadDeals.Banner.Request _Request)
        {
            _ManageDeal = new ManageDeal();
            OResponse _Response = _ManageDeal.GetBannerImages(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [HttpPost]
        [ActionName("trackorder")]
        public async Task<object> TrackOrder([FromBody] OOrderProcess.TrackOrder.Request _Request)
        {
            _ManageOrderProcess = new ManageOrderProcess();
            OResponse _Response = await _ManageOrderProcess.TrackOrder(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}
