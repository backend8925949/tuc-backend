﻿using System;
using HCore.Helper;
using HCore.TUC.Core.Object.CustomerWeb;
using HCore.TUC.Core.Operations.CustomerWeb;
using Microsoft.AspNetCore.Mvc;

namespace HCore.Api.ThankUCash.MadDeals
{
    [Produces("application/json")]
    [Route("api/maddeals/register/[action]")]
    public class UserRegistrationController : Controller
    {
        ManageUserRegistration _ManageUserRegistration;

        [HttpPost]
        [ActionName("userlogin")]
        public object UserLogIn([FromBody] OUserRegistration.UserLogInRequest _Request)
        {
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UserLogIn(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("userregistration")]
        public object UserRegistration([FromBody] OUserRegistration.UserRegistration _Request)
        {
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UserRegistration(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("userverification")]
        public object UserVerification([FromBody] OUserRegistration.UserVerificationRequest _Request)
        {
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UserVerification(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("changepin")]
        public object UpdateAccountPin([FromBody] OUserRegistration.ChangeUserAccountPinRequest _Request)
        {
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.UpdateAccountPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("resendpin")]
        public object ResendUserverificationPin([FromBody] OUserRegistration.ResendUserVerificationRequest _Request)
        {
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.ResendUserverificationPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("resetpin")]
        public object ResetUserAccountPin([FromBody] OUserRegistration.ResetUserAccountPinRequest _Request)
        {
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.ResetUserAccountPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        [HttpPost]
        [ActionName("updatepin")]
        public object ResetAccountPin([FromBody] OUserRegistration.UpdateAccountPinRequest _Request)
        {
            _ManageUserRegistration = new ManageUserRegistration();
            OResponse _Response = _ManageUserRegistration.ResetAccountPin(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}