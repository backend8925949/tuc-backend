using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using Microsoft.AspNetCore.Mvc;

namespace HCore.Api.App.v3.Tuc.Plugins.Deals
{
    [Produces("application/json")]
    [Route("api/maddeals/cart/[action]")]
    public class TUCDealsCartController :Controller
    {
        ManageDealCart _ManageDealCart;

        /// <summary>
        /// Description: Saves the cart.
        /// </summary>
        /// <param name="_Request">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("savecart")]
        public async Task<object> SaveCart([FromBody] OCart.Save.Request _Request)
        {
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.SaveCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Updates the cart.
        /// </summary>
        /// <param name="_Request">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("updatecart")]
        public async Task<object> UpdateCart([FromBody] OCart.Update.Request _Request)
        {
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.UpdateCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Deletes the cart.
        /// </summary>
        /// <param name="_Request">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("deletecart")]
        public async Task<object> DeleteCart([FromBody] OReference _Request)
        {
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.DeleteCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cart details.
        /// </summary>
        /// <param name="_Request">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcart")]
        public async Task<object> GetCart([FromBody] OReference _Request)
        {
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.GetCart(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// Description: Gets the cart list.
        /// </summary>
        /// <param name="_Request">The o authentication request.</param>
        /// <returns>System.Object.</returns>
        [HttpPost]
        [ActionName("getcarts")]
        public async Task<object> GetCarts([FromBody] OList.Request _Request)
        {
            _ManageDealCart = new ManageDealCart();
            OResponse _Response = await _ManageDealCart.GetCarts(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
