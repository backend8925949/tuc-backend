//==================================================================================
// FileName: SystemController.cs
// Author : Harshal Gandole
// Created On : 
// Description : controller for general operation
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Core.Operations.Operations;
using Microsoft.AspNetCore.Mvc;

namespace HCore.Api.ThankUCash.MadDeals
{
    [Produces("application/json")]
    [Route("api/maddeals/sys/[action]")]
    public class SystemController
    {
        ManageCountryManager _ManageCountryManager;
        ManageStateManager _ManageStateManager;
        ManageCityManager _ManageCityManager;

        /// <summary>
        /// get countries
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcountries")]
        public object GetCountries([FromBody] OList.Request _Request)
        {
            _ManageCountryManager = new ManageCountryManager();
            OResponse _Response = _ManageCountryManager.GetCountries(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get stgates
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getstates")]
        public object GetStates([FromBody] OList.Request _Request)
        {
            _ManageStateManager = new ManageStateManager();
            OResponse _Response = _ManageStateManager.GetStates(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }

        /// <summary>
        /// get cities
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("getcities")]
        public object GetCities([FromBody] OList.Request _Request)
        {
            _ManageCityManager = new ManageCityManager();
            OResponse _Response = _ManageCityManager.GetCities(_Request);
            return HCoreAuth.Auth_Response(_Response, _Request.UserReference);
        }
    }
}
