//==================================================================================
// FileName: Startup.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System.Linq;
using System.Threading.Tasks;
using HCore.Api.ThankUCash.AuthCore;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Helper;
using HCore.Helper;
using HCore.ThankUCash.Gateway;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Api.ThankUCash
{
    public class WhiteListing
    {
        public string[] Development { get; set; }
        public string[] Qa { get; set; }
        public string[] Staging { get; set; }
        public string[] Production { get; set; }
    }
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        private static async Task ShedularStartAsync()
        {
            Jobs.JobFactory _JobFactory = new Jobs.JobFactory();
            await _JobFactory.StartJobAsync();
        }
        public class AddHeaderParameter : IOperationFilter
        {
            public void Apply(OpenApiOperation operation, OperationFilterContext context)
            {
                operation.Parameters = new List<OpenApiParameter>();
                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Required = true,
                    Schema = new OpenApiSchema
                    {
                        Type = "string"
                    }
                });
            }
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options => options.EnableEndpointRouting = false);
            services.AddControllers().AddNewtonsoftJson();
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(options =>
            {
                options.CustomSchemaIds(type => type.FullName.Replace("+", "."));
                options.OperationFilter<AddHeaderParameter>();
            });
        }
        public void Configure(IApplicationBuilder _IApplicationBuilder)
        {
            _IApplicationBuilder.Build();
            _IApplicationBuilder.UseSwagger();
            _IApplicationBuilder.UseSwaggerUI();

            _IApplicationBuilder.Use(async (context, next) =>
            {
                if (string.IsNullOrEmpty(HCoreConstant.HostName))
                {
                    HCoreConstant.HostName = context.Request.Host.Host;
                    LoadConfiguration(context.Request.Host.Host);
                    if (HostName == "connect.thankucash.com" || HostName == "beta-connect.thankucash.com" || HostName == "webconnect.thankucash.com" || HostName == "appconnect.thankucash.com"
                    || HostName == "connect.thankucash.dev" || HostName == "connect.thankucash.tech" || HostName == "connect.thankucash.co"
                    || HostName == "webconnect.thankucash.dev" || HostName == "webconnect.thankucash.tech" || HostName == "webconnect.thankucash.co"
                    || HostName == "appconnect.thankucash.dev" || HostName == "appconnect.thankucash.tech" || HostName == "appconnect.thankucash.co")
                    {
                        HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new TUC.Plugins.MadDeals.ManageCore();
                        _ManageCore.StartMadDealsStorage();
                    }
                    else
                    {
                        ManageThankUCashGatewayStore.RefreshGatewayDataStore();
                        Data.Store.HCoreDataStoreManager.DataStore_Start();
                        HCore.TUC.Plugins.MadDeals.ManageCore _ManageCore = new TUC.Plugins.MadDeals.ManageCore();
                        _ManageCore.StartMadDealsStorage();
                    }
                    HCoreSystem.DataStore_InitializeSystem();
                    ShedularStartAsync().GetAwaiter().GetResult();
                }
                await next.Invoke();
            });
            _IApplicationBuilder.UseCors(_Builder => _Builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            _IApplicationBuilder.HCoreAuth();
            _IApplicationBuilder.UseMvc();
        }


        internal static void LoadConfiguration(string Host)
        {
            var configurationBuilder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").AddEnvironmentVariables();
            var configuration = configurationBuilder.Build();
            WhiteListing hostItems = configuration.GetSection("WhiteListing").Get<WhiteListing>()!;
            if (hostItems.Development.Any(x => x == Host))
            {
                HCoreConstant._AppConfig = configuration.GetSection("Development").Get<Globals>()!;
                HostHelper.Host = configuration["Development:Host:Master"]!;
                HostHelper.HostLogging = configuration["Development:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Development:Host:Operations"]!;
            }
            else if (hostItems.Staging.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Test;
                HCoreConstant._AppConfig = configuration.GetSection("Staging").Get<Globals>()!;
                HostHelper.Host = configuration["Staging:Host:Master"]!;
                HostHelper.HostLogging = configuration["Staging:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Staging:Host:Operations"]!;
            }
            else if (hostItems.Qa.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Tech;
                HCoreConstant._AppConfig = configuration.GetSection("Qa").Get<Globals>()!;
                HostHelper.Host = configuration["Qa:Host:Master"]!;
                HostHelper.HostLogging = configuration["Qa:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Qa:Host:Operations"]!;
            }
            else if (hostItems.Production.Any(x => x == Host))
            {
                HCoreConstant.HostEnvironment = HCoreConstant.HostEnvironmentType.Live;
                HCoreConstant._AppConfig = configuration.GetSection("Production").Get<Globals>()!;
                HostHelper.Host = configuration["Production:Host:Master"]!;
                HostHelper.HostLogging = configuration["Production:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Production:Host:Operations"]!;
            }
            else
            {
                HCoreConstant._AppConfig = configuration.GetSection("Development").Get<Globals>()!;
                HostHelper.Host = configuration["Development:Host:Master"]!;
                HostHelper.HostLogging = configuration["Development:Host:Logging"]!;
                HostHelper.HostOperations = configuration["Development:Host:Operations"]!;
            }
        }
    }
}
