//==================================================================================
// FileName: ThankUCashGatewayController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using HCore.ThankUCash.Gateway;
using HCore.ThankUCash.Gateway.ObjectV2;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.ThankUCash.App.V2
{

    [Produces("application/json")]
    [Route("api/v2/thankuconnect/[action]")]
    public class ThankUCashGatewayController : Controller
    {
        //ManageThankUCashGatewayV2 _ManageThankUCashGatewayV2;
        //// Operations
        //[HttpPost]
        //[ActionName("connectaccount")]
        //public object ConnectMerchant([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.ConnectMerchant(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("removeaccount")]
        //public object RemoveMerchant([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.RemoveMerchant(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("getconfig")]
        //public object GetConfiguration([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.GetConfiguration(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("getbalance")]
        //public object GetBalance([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.GetBalance(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("validateaccount")]
        //public object ValidateAccount([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.ValidateAccount(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}
        //[ActionName("change")]
        //public object Change([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.Change(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //// Registration
        //[ActionName("registeruser")]
        //public object RegisterUser([FromBody]OThankUGateway.Request _Request)
        //{

        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.RegisterUser(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //// USSD
        //[ActionName("resetussduserpin")]
        //public object ResetUssdUserPin([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.ResetUssdUserPin(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("updateussduser")]
        //public object UpdateUssdUser([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.UpdateUssdUser(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("registerussduser")]
        //public object RegisterUssdUser([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.RegisterUssdUser(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        ////Rewards
        //[ActionName("initializetransaction")]
        //public object InitializeTransaction([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.InitializeTransaction(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("settletransaction")]
        //public object SettleTransaction([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.SettleTransaction(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("rewardaccount")]
        //public object MerchantReward([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.MerchantReward(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}
            
        //[ActionName("reward")]
        //public object CreditReward([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.CreditReward(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

       

        //[ActionName("canceltransaction")]
        //public object CancelTransaction([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.CancelTransaction(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("notifyfailedtransaction")]
        //public object NotifyFailedTransaction([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.NotifyFailedTransaction(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("notifysettlements")]
        //public object NotifySettlements([FromBody]OThankUGateway.NotifySettlement _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.NotifySettlements(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

     
        //// Redeem
        //[ActionName("redeem")]
        //public object Redeem([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.Redeem(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

        //[ActionName("confirmredeem")]
        //public object ConfirmRedeem([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.ConfirmRedeem(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}

      

        //[ActionName("redeemaccount")]
        //public object MerchantRedeem([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.MerchantRedeem(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}
        //[ActionName("notifycppay")]
        //public object CoralPay_SaveTransactions([FromBody]OThankUGateway.CoralPay.Request _Request)
        //{
        //    _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
        //    OResponse _Response = _ManageThankUCashGatewayV2.CoralPay_SaveTransactions(_Request);
        //    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //}
    }
}
