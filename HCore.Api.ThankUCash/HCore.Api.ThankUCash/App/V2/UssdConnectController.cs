//==================================================================================
// FileName: UssdConnectController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.ThankUCash.App.V2
{

    [Produces("application/json")]
    [Route("api/v2/ussdconnect/[action]")]
    public class UssdConnectController : Controller
    {
        public class OUssdParameter
        {
            public string? sessionId { get; set; }
            public string? serviceCode { get; set; }
            public string? phoneNumber { get; set; }
            public string? text { get; set; }
        }
        [HttpPost]
        [ActionName("notifyussd")]
        public IActionResult NotifyTransaction([FromBody]OUssdParameter _Request)
        {
            string ResponseContent = "";
            if (_Request.text == "")
            {
                ResponseContent = "CON Welcome To Thank U Cash \n";
                ResponseContent += "Enter your name \n";
            }
            else if (_Request.text == "1")
            {
                // Business logic for first level response
                ResponseContent = "CON Select your gender \n";
                ResponseContent += "1. Male \n";
                ResponseContent += "2. Female";
            }
            else if (_Request.text == "2")
            {
                ResponseContent = "CON Enter date of birth \n";
                ResponseContent += "ddmmyy \n";
            }
            else if (_Request.text == "3")
            {
                ResponseContent = "CON Enter email address \n";
                ResponseContent += "ddmmyy \n";
            }
            else if (_Request.text == "4")
            {
                ResponseContent = "END Thank U " + _Request + " for registration with us. Download ThankUCash for crazy deals and offers \n";
            }
            return StatusCode(200, ResponseContent);
        }
    }
}
