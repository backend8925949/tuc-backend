//==================================================================================
// FileName: ProductController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using HCore.ThankUCash.Gateway;
using HCore.ThankUCash.Gateway.Object;
using Microsoft.AspNetCore.Mvc;
namespace HCore.Api.ThankUCash.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/product/[action]")]
    public class ProductController : Controller
    {
        //ManageThankUCashGateway _ManageThankUCashGateway;
        //[HttpPost]
        //[ActionName("notifyproducts")]
        //public object SaveProduct([FromBody]OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGateway = new ManageThankUCashGateway();
        //    OResponse _Response = _ManageThankUCashGateway.SaveProduct(_Request);
        //    if (_Request.UserReference.IsRsa)
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
        //    }
        //    else
        //    {
        //        return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        //    }
        //}
    }
}
