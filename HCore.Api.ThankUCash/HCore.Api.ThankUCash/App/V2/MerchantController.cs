//==================================================================================
// FileName: MerchantController.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using HCore.ThankUCash.Gateway;
using HCore.ThankUCash.Gateway.ObjectV2;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
namespace HCore.Api.ThankUCash.App.V2
{
    [Produces("application/json")]
    [Route("api/v2/merchant/[action]")]
    public class MerchantController : Controller
    {
        ManageThankUCashGatewayV2 _ManageThankUCashGatewayV2;
        [HttpPost]
        [ActionName("savemerchant")]
        public object SaveMerchant([FromBody]OAccounts.Merchant.Onboarding _Request)
        {
            _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
            OResponse _Response = _ManageThankUCashGatewayV2.SaveMerchant(_Request);
            if (_Request.UserReference.IsRsa)
            {
                return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
            }
            else
            {
                return HCoreAuth.Auth_ResponseDefaultObject(_Response);
            }
        }

        [HttpPost]
        [ActionName("savestore")]
        public object SaveStore([FromBody]OAccounts.Store.Request _Request)
        {
            _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
            OResponse _Response = _ManageThankUCashGatewayV2.SaveStore(_Request);
            if (_Request.UserReference.IsRsa)
            {
                return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
            }
            else
            {
                return HCoreAuth.Auth_ResponseDefaultObject(_Response);
            }
        }

        [HttpPost]
        [ActionName("saveterminal")]
        public object SaveTerminal([FromBody]OAccounts.Terminal.Request _Request)
        {
            _ManageThankUCashGatewayV2 = new ManageThankUCashGatewayV2();
            OResponse _Response = _ManageThankUCashGatewayV2.SaveTerminal(_Request);
            if (_Request.UserReference.IsRsa)
            {
                return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
            }
            else
            {
                return HCoreAuth.Auth_ResponseDefaultObject(_Response);
            }
        }

    }
}
