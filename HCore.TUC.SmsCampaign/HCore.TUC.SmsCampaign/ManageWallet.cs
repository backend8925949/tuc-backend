//==================================================================================
// FileName: ManageWallet.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;

namespace HCore.TUC.SmsCampaign
{
    public class ManageWallet
    {
        FrameworkWallet _FrameworkWallet;
        /// <summary>
        /// Description: Gets the balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBalance(OWallet.Topup.Request _Request)
        {
            _FrameworkWallet = new FrameworkWallet();
            return _FrameworkWallet.GetBalance(_Request);
        }
        /// <summary>
        /// Description: Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse TopupAccount(OWallet.Topup.Request _Request)
        {
            _FrameworkWallet = new FrameworkWallet();
            return _FrameworkWallet.TopupAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the topup history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTopupHistory(OList.Request _Request)
        {
            _FrameworkWallet = new FrameworkWallet();
            return _FrameworkWallet.GetTopupHistory(_Request);
        }
    }
}
