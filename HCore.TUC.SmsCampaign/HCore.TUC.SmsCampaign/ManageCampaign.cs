//==================================================================================
// FileName: ManageCampaign.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;
namespace HCore.TUC.SmsCampaign
{
    public class ManageCampaign
    {
        FrameworkCampaign _FrameworkCampaign;
        /// <summary>
        /// Description: Saves the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCampaign(OCampaign.Save.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.SaveCampaign(_Request);
        }
        /// <summary>
        /// Description: Updates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCampaign(OCampaign.Save.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.UpdateCampaign(_Request);
        }
        /// <summary>
        /// Description: Gets the campaign details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaign(OCampaign.Reference _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.GetCampaign(_Request);
        }
        /// <summary>
        /// Description: Gets the list of all campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaign(OList.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.GetCampaign(_Request);
        }
        /// <summary>
        /// Description: Gets the campaign item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCampaignItem(OList.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.GetCampaignItem(_Request);
        }
        /// <summary>
        /// Description: Deletes the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCampaign(OCampaign.Save.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.DeleteCampaign(_Request);
        }
        /// <summary>
        /// Description: Duplicates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DuplicateCampaign(OCampaign.Duplicate.Request _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.DuplicateCampaign(_Request);
        }
        /// <summary>
        /// Description: Updates the campaign status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCampaignStatus(OCampaign.Reference _Request)
        {
            _FrameworkCampaign = new FrameworkCampaign();
            return _FrameworkCampaign.UpdateCampaignStatus(_Request);
        }
    }
}
