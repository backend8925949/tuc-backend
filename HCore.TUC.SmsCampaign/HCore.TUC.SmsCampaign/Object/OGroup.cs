//==================================================================================
// FileName: OGroup.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.SmsCampaign.Object
{
    public class OGroup
    {
        public class Reference
        {
            public long GroupId { get; set; }
            public string? GroupKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long GroupId { get; set; }
            public string? GroupKey { get; set; }
            public string? Title { get; set; }
            public long ItemCount { get; set; }
            public string? FileName { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public List<Item> Items { get; set; }
            public DateTime CreateDate { get; set; }
        }
        public class Item
        {
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? IconUrl { get; set; }
        }
        public class Save
        {
            public class Request
            {
                public long GroupId { get; set; }
                public string? GroupKey { get; set; }
                public string? Title { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? SourceCode { get; set; }
                public string? FileName { get; set; }
                public string? Condition { get; set; }
                public bool IsItemUpdated { get; set; }
                public List<Item> Items { get; set; }
                public int ItemCount { get; set; }
                public List<Condition> Conditions { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Condition
            {
                public string? Name { get; set; }
                public string? Value { get; set; }
            }
            public class Item
            {
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
            }
        }
        public class Update
        {
            public class Request
            {
                public long GroupId { get; set; }
                public string? GroupKey { get; set; }
                public string? AccountKey { get; set; }
                public string? SourceCode { get; set; }
                public string? Title { get; set; }
                public string? Condition { get; set; }
                public bool IsItemUpdated { get; set; }
                public List<Condition> Conditions { get; set; }
                public List<Item> Items { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Condition
            {
                public string? Name { get; set; }
                public string? Value { get; set; }
            }
            public class Item
            {
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
            }
        }

        public class Details
        {
            public class Info
            {
                public long GroupId { get; set; }
                public string? GroupKey { get; set; }
                public string? SourceCode { get; set; }
                public string? FileName { get; set; }
                public string? Title { get; set; }
                public string? Condition { get; set; }
                public long ItemCount { get; set; }
                public List<Condition> Conditions { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                //public List<Item> Items { get; set; }
            }

            public class Condition
            {
                public string? Name { get; set; }
                public string? Value { get; set; }
            }
            public class Item
            {
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
            }

        }

    }
    public class OGroupItem
    {
        public class List
        {
            public long GroupItemId { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? IconUrl { get; set; }
            public DateTime CreateDate { get; set; }
        }

        public class Delete
        {
            public class BulkDelete
            {
                public bool IsDeleteAll { get; set; }
                public long GroupId { get; set; }
                public List<Item> Items { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Item
        {
            public long GroupItemId { get; set; }
        }
    }
}
