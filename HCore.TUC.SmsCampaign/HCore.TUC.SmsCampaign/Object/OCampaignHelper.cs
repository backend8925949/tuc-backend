//==================================================================================
// FileName: OCampaignHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;

namespace HCore.TUC.SmsCampaign.Object
{
    public class OCampaignHelper
    {
        public class SenderId
        {
            public long ReferenceId { get; set; }
            public string? Name { get; set; }
            public string? ProviderName { get; set; }
        }
    }
    internal class SMSSender
    {
        public long CampaignId { get; set; }
        public string? Message { get; set; }
        public string? SenderId { get; set; }
        public List<SMSSenderItem> Items { get; set; }
    }
    internal class SMSSenderItem
    {
        public long ReferenceId { get; set; }
        public string? MobileNumber { get; set; }
        public string? MessageId { get; set; }
    }

    public class OSmsCallBack
    {
        public string? message_id { get; set; }
        public string? status { get; set; }
        public string? status_code { get; set; }
        public DateTime? done_date { get; set; }
        public int length { get; set; }
        public int page { get; set; }
        public double cost { get; set; }
        //public string? operator {get;set;}
    }

    public class OAccounts
    {
        internal class Customer
        {
            public class Response
            {
                public long Total { get; set; }
            }


            public class List
            {
                public long ReferenceId { get; set; }
                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public long? GenderId { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public double? TotalInvoiceAmount { get; set; }
                public double? TotalRewardAmount { get; set; }
                public double? TotalRedeemAmount { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                public DateTime CreateDate { get; set; }
            }
        }
    }

}
