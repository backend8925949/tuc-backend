//==================================================================================
// FileName: OWallet.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.SmsCampaign.Object
{
    public class OWallet
    {

        public class Topup
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public double Amount { get; set; }
                public long TransactionId { get; set; }
                public string? PaymentReference { get; set; }
                public string? PaymentSource { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public double Amount { get; set; }

                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountDisplayName { get; set; }
                public string? AccountIconUrl { get; set; }

                public long? ModeId { get; set; }
                public string? ModeCode { get; set; }
                public string? ModeName { get; set; }

                public DateTime? CreateDate { get; set; }
                public DateTime TransactionDate { get; set; }
                public long? PaymentMethodId { get; set; }
                public string? PaymentReference { get; set; }
                public string? PaymentMethodCode { get; set; }
                public string? PaymentMethodName { get; set; }
                public long? CreatedByReferenceId { get; set; }
                public string? CreatedByReferenceKey { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public long? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class Balance
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public double Credit { get; set; }
                public double Debit { get; set; }
                public double Balance { get; set; }
                public double LastTransactionAmount { get; set; }
                public double LastTopupBalance { get; set; }
                public DateTime? LastTransactionDate { get; set; }
                // Balance After Topup
            }
        }

    }
}
