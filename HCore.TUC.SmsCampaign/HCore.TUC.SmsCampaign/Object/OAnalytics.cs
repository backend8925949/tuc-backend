//==================================================================================
// FileName: OAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.SmsCampaign.Object
{
    public class OAnalytics
    {
        public class AccountOverview
        {
            public  long Total { get; set; }
            public long? Active { get; set; }
            //public long? Idle { get; set; }
            public long? Dead { get; set; }
            public long? NotUsing { get; set; }

        }

        public class SmsOverview
        {
            public long Total { get; set; }
            public long UnSent { get; set; }
            public long Sent { get; set; }
            public long Delivered { get; set; }
            public long Failed { get; set; }
            public long Pending { get; set; }
        }

        public class CampaignOverview
        {
            public long? Total { get; set; }
            public double? TotalCampaignAmount { get; set; }
            public long? Creating { get; set; }
            public long? Deleteting { get; set; }
            public long? Draft { get; set; }
            public long? WaitingForReview { get; set; }
            public long? InReview { get; set; }
            public long? Approved { get; set; }
            public long? Rejected { get; set; }
            public long? Published { get; set; }
            public long? Running { get; set; }
            public long? Paused { get; set; }
            public long Cancelled { get; set; }
            public long? Expired { get; set; }
            public long? Completed { get; set; }
            public long? Upcoming { get; set; }
            public long? PendingApproval { get; set; }
        }
    }
}
