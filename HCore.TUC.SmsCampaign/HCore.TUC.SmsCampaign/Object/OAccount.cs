//==================================================================================
// FileName: OAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.SmsCampaign.Object
{
    public class OAccount
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }

            public string? EmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public string? Address { get; set; }
            public double? Balance { get; set; }
            public long? Campaigns { get; set; }
            public long SmsSent { get; set; }
            public DateTime? CampaignDate { get; set; }

            public DateTime? CreateDate { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
