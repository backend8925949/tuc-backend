//==================================================================================
// FileName: OCampaign.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
namespace HCore.TUC.SmsCampaign.Object
{
    public class OCampaign
    {
        public class Duplicate
        {
           public class Request
            {
                public long NewCampaignId { get; set; }

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Reference
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
                public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountIconUrl { get; set; }

            public string? Title { get; set; }
            public string? Message { get; set; }
            public DateTime SendDate { get; set; }

            public string? SenderId { get; set; }
            public long? TotalItem { get; set; }

            public double? Budget { get; set; }

            public long? ItemCount { get; set; }
            public long? Unsent { get; set; }
            public long? Sent { get; set; }
            public long? Delivered { get; set; }
            public long? Pending { get; set; }
            public long? Failed { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long GroupCount { get; set; }
            public List<GroupList> Groups { get; set; }
            public DateTime? ModifyDate { get; set; }
            public DateTime CreateDate { get; set; }

        }

        public class GroupList
        {
            public string? GroupName { get; set; }
        }

        public class Save
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public string? Title { get; set; }
                public bool IsSendNow { get; set; }
                public DateTime SendDate { get; set; }
                public string? Message { get; set; }
                public int SenderId { get; set; }

                public string? RecipientTypeCode { get; set; }
                public string? RecipientSubTypeCode { get; set; }

                public string? FileName { get; set; }
                public string? Condition { get; set; }

                public string? StatusCode { get; set; }
                public int TotalItem { get; set; }

                public List<Condition> Conditions { get; set; }
                public List<Group> Groups { get; set; }
                public List<Item> Items { get; set; }
                public bool IsItemUpdated { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Group
            {
                public long GroupId { get; set; }
                public string? GroupKey{ get; set; }
            }
            public class Condition
            {
                public string? Name { get; set; }
                public string? Value { get; set; }
            }
            public class Item
            {
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
            }
        }
        public class Update
        {
            public class Request
            {
                public long GroupId { get; set; }
                public string? GroupKey { get; set; }
                public string? AccountKey { get; set; }
                public string? SourceCode { get; set; }
                public string? Title { get; set; }
                public string? Condition { get; set; }
                public bool IsItemUpdated { get; set; }
                public List<Condition> Conditions { get; set; }
                public List<Item> Items { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Condition
            {
                public string? Name { get; set; }
                public string? Value { get; set; }
            }
            public class Item
            {
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
            }
        }
        public class Details
        {
            public class Info
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long? TotalItem { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountDisplayName { get; set; }
                public string? AccountIconUrl { get; set; }

                public string? Title { get; set; }
                public DateTime SendDate { get; set; }
                public string? Message { get; set; }

                public int SenderId { get; set; }
                public string? SenderIdName { get; set; }

                public string? RecipientTypeCode { get; set; }
                public string? RecipientTypeName { get; set; }
                public string? RecipientSubTypeCode { get; set; }
                public string? RecipientSubTypeName { get; set; }

                public double? Budget { get; set; }
                public double? ItemCost { get; set; }
                public long? ItemCount { get; set; }

                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public long? ReviewerId { get; set; }
                public string? ReviewerKey { get; set; }
                public string? ReviewerDisplayName { get; set; }
                public string? ReviewerComment { get; set; }

                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }

                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public long? UnSent { get; set; }
                public long? Sent { get; set; }
                public long? Failed { get; set; }
                public long? Pending { get; set; }
                public long? Delivered { get; set; }
                //public string? SourceCode { get; set; }
                //public string? FileName { get; set; }
                //public string? Condition { get; set; }
                //public bool IsItemUpdated { get; set; }
                public List<Condition> Conditions { get; set; }
                public List<Group> Groups { get; set; }
                //public List<Item> Items { get; set; }


                //public string? SourceCode { get; set; }
                //public string? FileName { get; set; }
                //public string? Title { get; set; }
                //public string? Condition { get; set; }
                //public List<Condition> Conditions { get; set; }
                //public List<Item> Items { get; set; }
            }
            public class Group
            {
                public string? Title { get; set; }
            }
            public class Condition
            {
                public string? Name { get; set; }
                public string? Value { get; set; }
            }
            public class Item
            {
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
            }

        }
    }

public class OCampaignGroupItem
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? GroupName { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? IconUrl { get; set; }
            public DateTime? SendDate { get; set; }
            public DateTime? DeliveryDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }
        }

        public class Delete
        {
            public class BulkDelete
            {
                public bool IsDeleteAll { get; set; }
                public long GroupId { get; set; }
                public List<Item> Items { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class Item
        {
            public long GroupItemId { get; set; }
        }
    }



}
