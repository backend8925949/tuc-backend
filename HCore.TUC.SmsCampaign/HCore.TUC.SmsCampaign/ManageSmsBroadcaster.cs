//==================================================================================
// FileName: ManageSmsBroadcaster.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using Akka.Actor;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Actor;
using HCore.TUC.SmsCampaign.Core;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;
namespace HCore.TUC.SmsCampaign
{
    public class ManageSmsBroadcaster
    {
        CoreSmsBroadCaster _CoreSmsBroadCaster;
        /// <summary>
        /// Description: Opens the campaign.
        /// </summary>
        public void OpenCampaign()
        {
            var system = ActorSystem.Create("ActorCampaignOpen");
            var greeter = system.ActorOf<ActorCampaignOpen>("ActorCampaignOpen");
            greeter.Tell(1);
        }
        //public void CloseCampaign()
        //{
        //    var system = ActorSystem.Create("ActorCloseCampaign");
        //    var greeter = system.ActorOf<ActorCloseCampaign>("ActorCloseCampaign");
        //    greeter.Tell(1);
        //}
        /// <summary>
        /// Description: Updates the SMS status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Net.HttpStatusCode.</returns>
        public System.Net.HttpStatusCode UpdateSmsStatus(OSmsCallBack _Request)
        {
            _CoreSmsBroadCaster = new CoreSmsBroadCaster();
          return  _CoreSmsBroadCaster.UpdateSmsStatus(_Request);
        }
    }
}
