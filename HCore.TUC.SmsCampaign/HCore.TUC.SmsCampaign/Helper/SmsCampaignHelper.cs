//==================================================================================
// FileName: SmsCampaignHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.SmsCampaign.Helper
{
    public class SmsCampaignHelper
    {
       public class StatusHelper
        {
            public class SmsCampaign
            {
                public const int creating = 710;
                public const int deleteting = 711;
                public const int draft = 712;
                public const string draftS = "draft";
                public const int waitingforreview = 713;
                public const int inreview = 714;
                public const int approved = 715;
                public const int rejected = 716;
                public const int published = 717;
                public const string publishedS = "published";
                public const int running = 718;
                public const int paused = 719;
                public const int cancelled = 720;
                public const int expired = 721;
                public const int completed = 722;
            }
            public class SmsStatus
            {
                public const int Unsent = 725;
                public const int Sending = 726;
                public const int Sent = 727;
                public const int Delivered = 728;
                public const int Pending = 729;
                public const int Failed = 730;
                public const int Undeliverable = 731;
                public const int SendingFailed = 732;
                
            }

            public class Group
            {
                public const int Creating = 698;
                public const int Active = 699;
                public const int Inactive = 700;
                public const int Deleting = 701;
            }
            public class GroupItem
            {
                public const int Inactive = 1;
                public const int Active = 2;
            }
        }
        public class TypeHelper
        {
            public class GroupSourceType
            {
                public const int TUC = 703;
                public const string TUCS = "tuc";
                public const int External = 704;
                public const string ExternalS = "external";
                public const int Group = 705;
                public const string GroupS = "group";
                public const int Custom = 723;
                public const string CustomS = "custom";
            }

            public class CampaignRecepientType
            {
                public const int Group = 703;
                public const string GroupS = "group";

                public const int Individual = 704;
                public const string IndividualS = "individual";
            }



        }
    }
}
