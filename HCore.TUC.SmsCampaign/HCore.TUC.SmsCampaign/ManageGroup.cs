//==================================================================================
// FileName: ManageGroup.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;

namespace HCore.TUC.SmsCampaign
{
    public class ManageGroup
    {
        FrameworkGroup _FrameworkGroup;
        /// <summary>
        /// Description: Saves the group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveGroup(OGroup.Save.Request _Request)
        {
            _FrameworkGroup = new FrameworkGroup();
            return _FrameworkGroup.SaveGroup(_Request);
        }
        /// <summary>
        /// Description: Updates the group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateGroup(OGroup.Save.Request _Request)
        {
            _FrameworkGroup = new FrameworkGroup();
            return _FrameworkGroup.UpdateGroup(_Request);
        }

        /// <summary>
        /// Description: Gets the group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetGroup(OGroup.Reference _Request)
        {
            _FrameworkGroup = new FrameworkGroup();
            return _FrameworkGroup.GetGroup(_Request);
        }
        /// <summary>
        /// Description: Deletes the group items.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteGroupItems(OGroupItem.Delete.BulkDelete _Request)
        {
            _FrameworkGroup = new FrameworkGroup();
            return _FrameworkGroup.DeleteGroupItems(_Request);
        }

        /// <summary>
        /// Description: Gets the group list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetGroup(OList.Request _Request)
        {
            _FrameworkGroup = new FrameworkGroup();
            return _FrameworkGroup.GetGroup(_Request);
        }
        /// <summary>
        /// Description: Gets the group item list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetGroupItem(OList.Request _Request)
        {
            _FrameworkGroup = new FrameworkGroup();
            return _FrameworkGroup.GetGroupItem(_Request);
        }
        /// <summary>
        /// Description: Deletes the group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteGroup(OGroup.Save.Request _Request)
        {
            _FrameworkGroup = new FrameworkGroup();
            return _FrameworkGroup.DeleteGroup(_Request);
        }
    }
}
