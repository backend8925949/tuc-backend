//==================================================================================
// FileName: ActorCampaignEdit.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;
namespace HCore.TUC.SmsCampaign.Actor
{
    public class ActorCampaignEdit : ReceiveActor
    {
        FrameworkCampaign _FrameworkCampaign;
        public ActorCampaignEdit()
        {
            Receive<OCampaign.Save.Request>(_RequestContent =>
            {
                _FrameworkCampaign = new FrameworkCampaign();
                _FrameworkCampaign.CleanCampaignItems(_RequestContent);
            });
        }
    }
}
