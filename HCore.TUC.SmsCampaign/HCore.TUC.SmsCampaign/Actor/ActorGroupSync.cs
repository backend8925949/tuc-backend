//==================================================================================
// FileName: ActorGroupSync.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;

namespace HCore.TUC.SmsCampaign.Actor
{
    public class ActorGroupSync : ReceiveActor
    {
        FrameworkGroup _FrameworkGroup;
        public ActorGroupSync()
        {
            Receive<OGroup.Save.Request>(_RequestContent =>
            {
                 _FrameworkGroup = new FrameworkGroup();
                _FrameworkGroup.SaveGroupItem(_RequestContent);
            });
        }
    }
}
