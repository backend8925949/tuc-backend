//==================================================================================
// FileName: ActorSmsBroascaster.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.TUC.SmsCampaign.Core;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;

namespace HCore.TUC.SmsCampaign.Actor
{
    public class ActorCampaignOpen : ReceiveActor
    {
        CoreSmsBroadCaster _CoreSmsBroadCaster;
        public ActorCampaignOpen()
        {
            Receive<int>(_RequestContent =>
            {
                _CoreSmsBroadCaster = new CoreSmsBroadCaster();
                _CoreSmsBroadCaster.OpenCampaign();
            });
        }
    }

    public class ActorBroadcastInitialize : ReceiveActor
    {
        CoreSmsBroadCaster _CoreSmsBroadCaster;
        public ActorBroadcastInitialize()
        {
            Receive<int>(_RequestContent =>
            {
                _CoreSmsBroadCaster = new CoreSmsBroadCaster();
                _CoreSmsBroadCaster.BroadcastSms_Initialize();
            });
        }
    }

    public class ActorBroadcastSms : ReceiveActor
    {
        CoreSmsBroadCaster _CoreSmsBroadCaster;
        public ActorBroadcastSms()
        {
            Receive<SMSSender>(_RequestContent =>
            {
                _CoreSmsBroadCaster = new CoreSmsBroadCaster();
                _CoreSmsBroadCaster.BroadcastSms(_RequestContent);
            });
        }
    }
    //public class ActorCloseCampaign : ReceiveActor
    //{
    //    CoreSmsBroadCaster _CoreSmsBroadCaster;
    //    public ActorCloseCampaign()
    //    {
    //        Receive<long>(_RequestContent =>
    //        {
    //            _CoreSmsBroadCaster = new CoreSmsBroadCaster();
    //            _CoreSmsBroadCaster.CloseCampaign();
    //        });
    //    }
    //}
}
