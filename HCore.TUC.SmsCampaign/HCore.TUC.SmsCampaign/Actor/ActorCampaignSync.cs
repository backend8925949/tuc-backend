//==================================================================================
// FileName: ActorCampaignSync.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;


namespace HCore.TUC.SmsCampaign.Actor
{
    public class ActorCampaignSync : ReceiveActor
    {
        FrameworkCampaign _FrameworkCampaign;
        public ActorCampaignSync()
        {
            Receive<OCampaign.Save.Request>(_RequestContent =>
            {
                _FrameworkCampaign = new FrameworkCampaign();
                _FrameworkCampaign.SaveCampaignItem(_RequestContent);
            });
        }
    }
}
