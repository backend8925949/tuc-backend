//==================================================================================
// FileName: ManageCampaignHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;
namespace HCore.TUC.SmsCampaign
{
    public class ManageCampaignHelper
    {
        FrameworkHelper _FrameworkHelper;
        /// <summary>
        /// Description: Gets the sender identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSenderId(OList.Request _Request)
        {
            _FrameworkHelper = new FrameworkHelper();
            return _FrameworkHelper.GetSenderId(_Request);
        }
        /// <summary>
        /// Description: Gets the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomer(OList.Request _Request)
        {
            _FrameworkHelper = new FrameworkHelper();
            return _FrameworkHelper.GetCustomer(_Request);
        }
        /// <summary>
        /// Description: Gets the customer count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerCount(OList.Request _Request)
        {
            _FrameworkHelper = new FrameworkHelper();
            return _FrameworkHelper.GetCustomerCount(_Request);
        }
    }
}
