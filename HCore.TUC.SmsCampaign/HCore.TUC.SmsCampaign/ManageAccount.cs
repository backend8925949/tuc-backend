//==================================================================================
// FileName: ManageAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Framework;
using HCore.TUC.SmsCampaign.Object;
namespace HCore.TUC.SmsCampaign
{
    public class ManageAccount
    {
        FrameworkAccount _FrameworkAccount;
        /// <summary>
        /// Description: Gets the list of accounts.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccounts(OList.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetAccounts(_Request);
        }
    }
}
