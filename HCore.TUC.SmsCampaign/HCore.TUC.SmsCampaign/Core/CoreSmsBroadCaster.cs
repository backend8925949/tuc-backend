//==================================================================================
// FileName: CoreSmsBroadCaster.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to sms broadcast
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Actor;
using HCore.TUC.SmsCampaign.Object;
using RestSharp;
using static HCore.Helper.HCoreConstant;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper;
namespace HCore.TUC.SmsCampaign.Core
{
    public class CoreSmsBroadCaster
    {
        HCoreContext _HCoreContext;
        SMSSender _SMSSender;
        List<SMSSenderItem> _SMSSenderItems;
        /// <summary>
        /// Description: Updates the SMS status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Net.HttpStatusCode.</returns>
        internal System.Net.HttpStatusCode UpdateSmsStatus(OSmsCallBack _Request)
        {
            try
            {
                if (_Request != null && !string.IsNullOrEmpty(_Request.message_id))
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CampaignSmsItem = _HCoreContext.SMSCampaignGroupItem.Where(x => x.ExternalId == _Request.message_id).FirstOrDefault();
                        if (CampaignSmsItem != null)
                        {
                            var CampaignDetails = _HCoreContext.SMSCampaign.Where(x => x.Id == CampaignSmsItem.CampaignId).FirstOrDefault();
                            if (CampaignDetails != null)
                            {
                            }
                            if (_Request.done_date != null)
                            {
                                CampaignSmsItem.DeliveryDate = _Request.done_date;
                            }
                            if (_Request.status == "DELIVRD")
                            {
                                CampaignSmsItem.StatusId = StatusHelper.SmsStatus.Delivered;
                                CampaignDetails.Delivered++;
                            }
                            if (_Request.status == "UNDELIV")
                            {
                                CampaignSmsItem.StatusId = StatusHelper.SmsStatus.Failed;
                                CampaignDetails.Failed++;
                            }
                            if (_Request.status == "UNDELIV_DND")
                            {
                                CampaignSmsItem.StatusId = StatusHelper.SmsStatus.Failed;
                                CampaignDetails.Failed++;
                            }
                            if (_Request.status == "EXPIRED")
                            {
                                CampaignSmsItem.StatusId = StatusHelper.SmsStatus.Failed;
                                CampaignDetails.Failed++;
                            }
                            if (_Request.done_date != null)
                            {
                                CampaignSmsItem.DeliveryDate = _Request.done_date;
                            }
                            CampaignSmsItem.ExStatus = _Request.status;
                            CampaignSmsItem.ExStatusCode = _Request.status_code;
                            CampaignSmsItem.SmsLength = _Request.length;
                            CampaignSmsItem.PageCount = _Request.page;
                            CampaignSmsItem.SystemCost = _Request.cost;
                            CampaignSmsItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();
                            return System.Net.HttpStatusCode.OK;
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return System.Net.HttpStatusCode.Unauthorized;
                        }
                    }
                }
                else
                {
                    return System.Net.HttpStatusCode.Unauthorized;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateSmsStatus", _Exception);
                return System.Net.HttpStatusCode.Unauthorized;
            }
        }
        /// <summary>
        /// Description: Opens the campaign.
        /// </summary>
        internal void OpenCampaign()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var StartTime = HCoreHelper.GetGMTDateTime().Date;
                    var EndTime = HCoreHelper.GetGMTDateTime().AddSeconds(30);
                    var SmsQueue = _HCoreContext.SMSCampaign
                        .Where(x => x.StatusId == StatusHelper.SmsCampaign.published && x.SendDate > StartTime && x.SendDate < EndTime)
                        .Skip(0)
                        .Take(1000)
                        .ToList();
                    if (SmsQueue.Count > 0)
                    {
                        foreach (var SmsItem in SmsQueue)
                        {
                            SmsItem.StartDate = HCoreHelper.GetGMTDateTime();
                            SmsItem.StatusId = StatusHelper.SmsCampaign.running;
                            SmsItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            SmsItem.ModifyById = 1;
                        }
                        _HCoreContext.SaveChanges();
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }
                }
                var system = ActorSystem.Create("ActorBroadcastInitialize");
                var greeter = system.ActorOf<ActorBroadcastInitialize>("ActorBroadcastInitialize");
                greeter.Tell(1);
                CloseCampaign();
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("OpenCampaign", _Exception);
            }
        }
        /// <summary>
        /// Description: Closes the campaign.
        /// </summary>
        public void CloseCampaign()
        {

            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var SmsQueue = _HCoreContext.SMSCampaign
                        .Where(x => (x.StatusId == StatusHelper.SmsCampaign.running && x.SMSCampaignGroupItem.Count(a => a.SendDate != null) >= x.ItemLimit)
                        || (x.StatusId == StatusHelper.SmsCampaign.running && x.SMSCampaignGroupItem.Count() == 0))
                        .Skip(0)
                        .Take(1000)
                        .ToList();
                    if (SmsQueue.Count > 0)
                    {
                        foreach (var SmsItem in SmsQueue)
                        {
                            long SentItem = _HCoreContext.SMSCampaignGroupItem.Count(x => x.CampaignId == SmsItem.Id && (x.StatusId != StatusHelper.SmsStatus.Unsent && x.StatusId != StatusHelper.SmsStatus.Sending && x.StatusId != StatusHelper.SmsStatus.SendingFailed));
                            SmsItem.Unsent = SmsItem.ItemLimit - SentItem;
                            SmsItem.Sent = SentItem;
                            SmsItem.EndDate = HCoreHelper.GetGMTDateTime();
                            SmsItem.StatusId = StatusHelper.SmsCampaign.completed;
                            SmsItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            SmsItem.ModifyById = 1;
                        }
                        _HCoreContext.SaveChanges();
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }
                }


            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CloseCampaign", _Exception);
            }

        }
        /// <summary>
        /// Description: Broadcasts the SMS initialize.
        /// </summary>
        public void BroadcastSms_Initialize()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var RunningCampaigns = _HCoreContext.SMSCampaign
                        .Where(x => x.StatusId == StatusHelper.SmsCampaign.running && x.SMSCampaignGroupItem.Any(a => (a.StatusId == StatusHelper.SmsStatus.Unsent || a.StatusId == StatusHelper.SmsStatus.SendingFailed)))
                        .Select(x => new
                        {
                            CampaignId = x.Id,
                            Message = x.Message,
                            SenderId = x.Sender.SenderId,
                        })
                        .Skip(0)
                        .Take(1000)
                        .ToList();
                    _HCoreContext.Dispose();
                    if (RunningCampaigns.Count > 0)
                    {
                        foreach (var RunningCampaign in RunningCampaigns)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var SmsItems = _HCoreContext.SMSCampaignGroupItem
                                     .Where(x => x.CampaignId == RunningCampaign.CampaignId && x.StatusId == StatusHelper.SmsStatus.Unsent)
                                     .Skip(0)
                                     .Take(5000)
                                     .ToList();
                                foreach (var item in SmsItems)
                                {
                                    item.StatusId = StatusHelper.SmsStatus.Sending;
                                }
                                _HCoreContext.SaveChanges();
                                if (SmsItems.Count > 0)
                                {
                                    int LoopCount = 1;
                                    int Offset = 0;
                                    if (SmsItems.Count > 500)
                                    {
                                        LoopCount = SmsItems.Count / 500;
                                    }
                                    _SMSSender = new SMSSender();
                                    for (int i = 0; i < LoopCount; i++)
                                    {
                                        var Items = SmsItems.Skip(0).Take(500);
                                        _SMSSender.CampaignId = RunningCampaign.CampaignId;
                                        _SMSSender.Message = RunningCampaign.Message;
                                        _SMSSender.SenderId = RunningCampaign.SenderId;
                                        _SMSSenderItems = new List<SMSSenderItem>();
                                        foreach (var SmsItem in SmsItems)
                                        {
                                            _SMSSenderItems.Add(new SMSSenderItem
                                            {
                                                ReferenceId = SmsItem.Id,
                                                MobileNumber = SmsItem.MobileNumber
                                            });
                                        }
                                        _SMSSender.Items = _SMSSenderItems;
                                        Offset += 500;
                                    }
                                    var system = ActorSystem.Create("ActorBroadcastSms");
                                    var greeter = system.ActorOf<ActorBroadcastSms>("ActorBroadcastSms");
                                    greeter.Tell(_SMSSender);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BroadcastSms_Initialize", _Exception);
            }
        }
        /// <summary>
        /// Description: Broadcasts the SMS.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void BroadcastSms(SMSSender _Request)
        {
            try
            {
                if (_Request.Items.Count > 0)
                {
                    string Recepients = "";
                    int LoopCount = _Request.Items.Count;
                    if (LoopCount == 1)
                    {
                        Recepients = _Request.Items.FirstOrDefault().MobileNumber;
                    }
                    else
                    {
                        for (int i = 0; i < LoopCount; i++)
                        {
                            if (i == (LoopCount - 1))
                            {
                                Recepients += _Request.Items[i].MobileNumber;
                            }
                            else
                            {
                                Recepients += _Request.Items[i].MobileNumber + ",";
                            }
                        }
                    }
                    if (HostEnvironment == HostEnvironmentType.Live)
                    {
                        var client = new RestClient("https://sms.hollatags.com/api/send/");
                        //client.Timeout = -1;
                        var request = new RestRequest();
                        request.Method = Method.Post;
                        request.AlwaysMultipartFormData = true;
                        request.AddParameter("user", "ThankUCA");
                        request.AddParameter("pass", "TUC@321");
                        request.AddParameter("from", _Request.SenderId);
                        request.AddParameter("msg", _Request.Message.Replace("&", "0x26") );
                        request.AddParameter("to", Recepients);
                        if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                        {
                            request.AddParameter("callback_url", "https://webconnect.thankucash.com/api/ext/campaigns/sms/notifysmsdelivery");
                        }
                        else if (HCoreConstant.HostEnvironment == HostEnvironmentType.Test)
                        {
                            request.AddParameter("callback_url", "https://testwebconnect.thankucash.com/api/ext/campaigns/sms/notifysmsdelivery");
                        }
                        request.AddParameter("enable_msg_id", "TRUE");
                        var _IRestResponse = client.Execute(request);
                        if (_IRestResponse.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            string Content = _IRestResponse.Content;
                            int StatusId = StatusHelper.SmsStatus.SendingFailed;
                            if (Content == "error_param")
                            {
                                StatusId = StatusHelper.SmsStatus.SendingFailed;
                            }
                            else if (Content == "error_credit")
                            {
                                StatusId = StatusHelper.SmsStatus.SendingFailed;
                            }
                            else if (Content == "error_billing")
                            {
                                StatusId = StatusHelper.SmsStatus.SendingFailed;
                            }
                            else if (Content == "error_gw")
                            {
                                StatusId = StatusHelper.SmsStatus.SendingFailed;
                            }
                            else if (Content == "error_user")
                            {
                                StatusId = StatusHelper.SmsStatus.SendingFailed;
                            }
                            else if (!string.IsNullOrEmpty(Content))
                            {
                                if (Content.Contains("~"))
                                {
                                    string[] TContent = Content.Split('~');
                                    if (TContent.Length > 0)
                                    {
                                        foreach (var TContentItem in TContent)
                                        {
                                            if (TContentItem.Contains(','))
                                            {
                                                string[] TContentItemDetails = TContentItem.Split(',');
                                                string MobileNumber = TContentItemDetails[0];
                                                string MessageId = TContentItemDetails[1];
                                                var ItemIndex = _Request.Items.Where(x => x.MobileNumber == MobileNumber).FirstOrDefault(); //.Where().FirstOrDefault();
                                                if (ItemIndex != null)
                                                {
                                                    ItemIndex.MessageId = MessageId;
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (Content.Contains(','))
                                {
                                    string[] TContentItemDetails = Content.Split(',');
                                    string MobileNumber = TContentItemDetails[0];
                                    string MessageId = TContentItemDetails[1];
                                    var ItemIndex = _Request.Items.Where(x => x.MobileNumber == MobileNumber).FirstOrDefault(); //.Where().FirstOrDefault();
                                    if (ItemIndex != null)
                                    {
                                        ItemIndex.MessageId = MessageId;
                                    }
                                }
                                StatusId = StatusHelper.SmsStatus.Sent;
                            }
                            else
                            {
                                StatusId = StatusHelper.SmsStatus.SendingFailed;
                            }
                            var ItemsLists = _Request.Items.ToList();
                            using (_HCoreContext = new HCoreContext())
                            {
                                foreach (var ItemsList in ItemsLists)
                                {
                                    var SmsItem = _HCoreContext.SMSCampaignGroupItem.Where(x => x.Id == ItemsList.ReferenceId).FirstOrDefault();
                                    if (SmsItem != null)
                                    {
                                        SmsItem.ExternalId = ItemsList.MessageId;
                                        if (StatusId == StatusHelper.SmsStatus.Sent)
                                        {
                                            SmsItem.IsSent = 1;
                                            SmsItem.SendDate = HCoreHelper.GetGMTDateTime();
                                            var CampaignItem = _HCoreContext.SMSCampaign.Where(x => x.Id == SmsItem.CampaignId).FirstOrDefault();
                                            if (CampaignItem != null)
                                            {
                                                CampaignItem.Unsent--;
                                                CampaignItem.Sent++;
                                            }
                                        }
                                        SmsItem.StatusId = StatusId;
                                       
                                    }
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                        else
                        {
                            var ItemsLists = _Request.Items.ToList();
                            using (_HCoreContext = new HCoreContext())
                            {
                                foreach (var ItemsList in ItemsLists)
                                {
                                    var SmsItem = _HCoreContext.SMSCampaignGroupItem.Where(x => x.Id == ItemsList.ReferenceId).FirstOrDefault();
                                    if (SmsItem != null)
                                    {
                                        SmsItem.IsSent = 1;
                                        SmsItem.ExternalId = ItemsList.MessageId;
                                        SmsItem.StatusId = StatusHelper.SmsStatus.SendingFailed;
                                        //var CampaignItem = _HCoreContext.SMSCampaign.Where(x => x.Id == SmsItem.CampaignId).FirstOrDefault();
                                        //if (CampaignItem != null)
                                        //{
                                        //    CampaignItem.Unsent--;
                                        //    CampaignItem.Sent++;
                                        //}
                                    }
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }

                    }
                    else
                    {
                        var ItemsLists = _Request.Items.ToList();
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var ItemsList in ItemsLists)
                            {
                                var SmsItem = _HCoreContext.SMSCampaignGroupItem.Where(x => x.Id == ItemsList.ReferenceId).FirstOrDefault();
                                if (SmsItem != null)
                                {
                                    SmsItem.IsSent = 1;
                                    SmsItem.ExternalId = ItemsList.MessageId;
                                    SmsItem.SendDate = HCoreHelper.GetGMTDateTime();
                                    SmsItem.StatusId = StatusHelper.SmsStatus.Sent;
                                    var CampaignItem = _HCoreContext.SMSCampaign.Where(x => x.Id == SmsItem.CampaignId).FirstOrDefault();
                                    if (CampaignItem != null)
                                    {
                                        CampaignItem.Unsent--;
                                        CampaignItem.Sent++;
                                    }
                                }
                            }
                            _HCoreContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BroadcastSms", _Exception);
            }
        }
    }
}
