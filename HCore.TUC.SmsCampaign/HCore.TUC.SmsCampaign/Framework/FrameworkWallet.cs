//==================================================================================
// FileName: FrameworkWallet.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to wallets
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Paystack;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.SmsCampaign.Object;
using HCore.TUC.SmsCampaign.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.SmsCampaign.Framework
{
    public class FrameworkWallet
    {
        ManageCoreTransaction _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        HCoreContext _HCoreContext;
        OCoreTransaction.Request _CoreTransactionRequest;
        /// <summary>
        /// Description: Gets the balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBalance(OWallet.Topup.Request _Request)
        {
            #region Manage Exception
            try
            {

                _ManageCoreTransaction = new ManageCoreTransaction();
                var _Response = new
                {
                    AccountBalance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.Merchant),
                    Balance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, HCoreConstant.Helpers.TransactionSource.PromotionCampaign),
                };
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HC0001");
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBalance", _Exception, _Request.UserReference, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse TopupAccount(OWallet.Topup.Request _Request)
        {
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0337", ResponseCode.CA0337);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0338", ResponseCode.CA0338);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0339", ResponseCode.CA0339);
                }
                if (_Request.TransactionId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0340", ResponseCode.CA0340);
                }
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0341", ResponseCode.CA0341);
                }
                double MerchantRewardBalance = 0;
                if (_Request.PaymentSource == "wallet")
                {
                    _ManageCoreTransaction = new ManageCoreTransaction();
                    MerchantRewardBalance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, HCoreConstant.Helpers.TransactionSource.Merchant);
                    if (_Request.Amount > MerchantRewardBalance)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0344", ResponseCode.CA0344);
                    }
                }
                if (_Request.UserReference.AccountTypeId == UserAccountType.Admin || _Request.UserReference.AccountTypeId == UserAccountType.Controller)
                {

                    List<OCoreTransaction.TransactionItem> _TransactionItems;
                    _CoreTransactionRequest = new OCoreTransaction.Request();
                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                    _CoreTransactionRequest.ParentId = _Request.AccountId;
                    _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                    _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                    _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                    _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                    if (_Request.PaymentSource == "wallet")
                    {
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
                            SourceId = TransactionSource.Merchant,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
                            SourceId = TransactionSource.PromotionCampaign,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                        });
                        _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.TucWallet;
                    }
                    else
                    {
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = SystemAccounts.ThankUCashSystemId,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
                            SourceId = TransactionSource.PromotionCampaign,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
                            SourceId = TransactionSource.PromotionCampaign,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                        });
                        _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Bank;
                    }
                    _CoreTransactionRequest.Transactions = _TransactionItems;
                    _ManageCoreTransaction = new ManageCoreTransaction();
                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0343", ResponseCode.CA0343);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0342", ResponseCode.CA0342);
                        #endregion
                    }
                }
                else
                {
                    if (_Request.PaymentSource == "wallet")
                    {

                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest.ParentId = _Request.AccountId;
                        _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                        _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                        _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                        _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.TucWallet;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
                            SourceId = TransactionSource.Merchant,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.AccountId,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
                            SourceId = TransactionSource.PromotionCampaign,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0343", ResponseCode.CA0343);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0342", ResponseCode.CA0342);
                            #endregion
                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(_Request.PaymentSource) && _Request.PaymentSource == "quickteller")
                        {
                            //OPayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, _AppConfig.PaystackPrivateKey);
                            //if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                            //{
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = _Request.AccountId;
                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                            _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.QuickTeller;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = SystemAccounts.ThankUCashSystemId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                SourceId = TransactionSource.PromotionCampaign,
                                Amount = _Request.Amount,
                                Comission = 0,
                                TotalAmount = _Request.Amount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                SourceId = TransactionSource.PromotionCampaign,
                                Amount = _Request.Amount,
                                Comission = 0,
                                TotalAmount = _Request.Amount,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0343", ResponseCode.CA0343);
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0342", ResponseCode.CA0342);
                                #endregion
                            }
                            //}
                            //else
                            //{
                            //    #region Send Response
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCCoreResource.CA1089, TUCCoreResource.CA1089M);
                            //    #endregion
                            //}
                        }
                        else
                        {
                            OPayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, _AppConfig.PaystackPrivateKey);
                            if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                            {
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = _Request.AccountId;
                                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Paystack;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashSystemId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                    SourceId = TransactionSource.PromotionCampaign,
                                    Amount = _Request.Amount,
                                    Comission = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                    SourceId = TransactionSource.PromotionCampaign,
                                    Amount = _Request.Amount,
                                    Comission = 0,
                                    TotalAmount = _Request.Amount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0343", ResponseCode.CA0343);
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0342", ResponseCode.CA0342);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0342", ResponseCode.CA0342);
                                #endregion
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TopupAccount", _Exception);
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG549");
                #endregion
            }

        }

        /// <summary>
        /// Description: Gets the topup history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTopupHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.AccountId
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                                    && x.SourceId == TransactionSource.PromotionCampaign)
                                                    .Select(x => new OWallet.Topup.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        ModeId = x.ModeId,
                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,
                                                        TransactionDate = x.TransactionDate,
                                                        PaymentMethodId = x.PaymentMethodId,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        PaymentReference = x.ReferenceNumber,
                                                        CreatedByReferenceId = x.CreatedById,
                                                        CreatedByReferenceKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.Name,
                                                        CreateDate = x.CreateDate,
                                                        Amount = x.Amount,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OWallet.Topup.List> _List = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _Request.AccountId
                                                   && x.Account.CountryId == _Request.UserReference.CountryId
                                                    && x.SourceId == TransactionSource.PromotionCampaign)
                                                    .Select(x => new OWallet.Topup.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        ModeId = x.ModeId,
                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,
                                                        TransactionDate = x.TransactionDate,
                                                        PaymentMethodId = x.PaymentMethodId,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        PaymentReference = x.ReferenceNumber,
                                                        CreatedByReferenceId = x.CreatedById,
                                                        CreatedByReferenceKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.Name,
                                                        CreateDate = x.CreateDate,
                                                        Amount = x.Amount,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Merchant
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                                    && x.SourceId == TransactionSource.PromotionCampaign)
                                                    .Select(x => new OWallet.Topup.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        ModeId = x.ModeId,
                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,
                                                        TransactionDate = x.TransactionDate,
                                                        PaymentMethodId = x.PaymentMethodId,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        PaymentReference = x.ReferenceNumber,
                                                        CreatedByReferenceId = x.CreatedById,
                                                        CreatedByReferenceKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.Name,
                                                        CreateDate = x.CreateDate,
                                                        Amount = x.Amount,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OWallet.Topup.List> _List = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.Account.AccountTypeId == UserAccountType.Merchant
                                                    && x.Account.CountryId == _Request.UserReference.CountryId
                                                    && x.SourceId == TransactionSource.PromotionCampaign)
                                                    .Select(x => new OWallet.Topup.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        ModeId = x.ModeId,
                                                        ModeCode = x.Mode.SystemName,
                                                        ModeName = x.Mode.Name,
                                                        TransactionDate = x.TransactionDate,
                                                        PaymentMethodId = x.PaymentMethodId,
                                                        PaymentMethodCode = x.PaymentMethod.SystemName,
                                                        PaymentMethodName = x.PaymentMethod.Name,
                                                        PaymentReference = x.ReferenceNumber,
                                                        CreatedByReferenceId = x.CreatedById,
                                                        CreatedByReferenceKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.Name,
                                                        CreateDate = x.CreateDate,
                                                        Amount = x.Amount,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTopupHistory", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }



    }
}
