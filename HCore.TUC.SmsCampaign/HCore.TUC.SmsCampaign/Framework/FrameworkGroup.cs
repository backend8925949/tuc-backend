//==================================================================================
// FileName: FrameworkGroup.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to groups
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Actor;
using HCore.TUC.SmsCampaign.Helper;
using HCore.TUC.SmsCampaign.Object;
using HCore.TUC.SmsCampaign.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper.TypeHelper;

namespace HCore.TUC.SmsCampaign.Framework
{
    public class FrameworkGroup
    {
        HCoreContext _HCoreContext;
        SMSGroup _SMSGroup;
        List<SMSGroupCondition> _SMSGroupConditions;
        List<SMSGroupItem> _SMSGroupItems;
        FrameworkHelper _FrameworkHelper;
        /// <summary>
        /// Description: Saves the group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveGroup(OGroup.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0302", ResponseCode.CA0302);
                }
                if (string.IsNullOrEmpty(_Request.SourceCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0304", ResponseCode.CA0304);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.Merchant
                        && x.Guid == _Request.AccountKey)
                        .Select(x => new
                        {
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            bool IsGroupExist = _HCoreContext.SMSGroup.Any(x => x.AccountId == _Request.AccountId && x.Title == _Request.Title);
                            if (IsGroupExist)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0336", ResponseCode.CA0336);
                            }
                            _SMSGroup = new SMSGroup();
                            if (_Request.Conditions != null && _Request.Conditions.Count > 0)
                            {
                                _SMSGroupConditions = new List<SMSGroupCondition>();
                                foreach (var Item in _Request.Conditions)
                                {
                                    _SMSGroupConditions.Add(new SMSGroupCondition
                                    {
                                        Name = Item.Name,
                                        Value = Item.Value,
                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                        CreatedById = _Request.UserReference.AccountId
                                    });
                                }
                                _SMSGroup.SMSGroupCondition = _SMSGroupConditions;
                            }
                            _SMSGroup.Guid = HCoreHelper.GenerateGuid();
                            _SMSGroup.AccountId = _Request.AccountId;
                            if (_Request.SourceCode == GroupSourceType.ExternalS)
                            {
                                _SMSGroup.SourceId = GroupSourceType.External;
                            }
                            else
                            {
                                _SMSGroup.SourceId = GroupSourceType.TUC;
                            }
                            _SMSGroup.Title = _Request.Title;
                            _SMSGroup.Condition = _Request.Condition;
                            _SMSGroup.FileName = _Request.FileName;
                            _SMSGroup.TotalItem = _Request.ItemCount;
                            _SMSGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                            _SMSGroup.CreatedById = _Request.UserReference.AccountId;
                            _SMSGroup.StatusId = StatusHelper.Group.Creating;
                            _HCoreContext.SMSGroup.Add(_SMSGroup);
                            _HCoreContext.SaveChanges();
                            _Request.GroupId = _SMSGroup.Id;
                            var system = ActorSystem.Create("ActorGroupSync");
                            var greeter = system.ActorOf<ActorGroupSync>("ActorGroupSync");
                            greeter.Tell(_Request);
                            var _Response = new
                            {
                                ReferenceId = _Request.GroupId,
                                ReferenceKey = _SMSGroup.Guid,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA0303", ResponseCode.CA0303);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAAACCNOTACTIVE", ResponseCode.CAAACCNOTACTIVE);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCNOTFOUND", ResponseCode.CAACCNOTFOUND);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SaveGroup", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the group item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void SaveGroupItem(OGroup.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.GroupId > 0)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var GroupDetails = _HCoreContext.SMSGroup.Where(x => x.Id == _Request.GroupId).FirstOrDefault();
                        if (GroupDetails != null)
                        {
                            _HCoreContext.Dispose();
                            if (GroupDetails.SourceId == SmsCampaignHelper.TypeHelper.GroupSourceType.TUC)
                            {
                                _FrameworkHelper = new FrameworkHelper();
                                var Customers = _FrameworkHelper.GetCustomer(GroupDetails.Condition, GroupDetails.AccountId, _Request.ItemCount);
                                Customers = Customers.Where(x => x.MobileNumber != null).ToList();
                                int Limit = 1;
                                if (Customers.Count > 500)
                                {
                                    Limit = Customers.Count / 500;
                                }
                                Limit++;
                                int Offset = 0;
                                for (int i = 0; i < Limit; i++)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _SMSGroupItems = new List<SMSGroupItem>();
                                        var Items = Customers.Skip(Offset).Take(500).ToList();
                                        Offset += 500;
                                        if (Items.Count > 0)
                                        {
                                            foreach (var mItem in Items)
                                            {
                                                 _SMSGroupItems.Add(new SMSGroupItem
                                                {
                                                    GroupId = _Request.GroupId,
                                                    Name = mItem.Name,
                                                     MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
                                                    AccountId = mItem.ReferenceId,
                                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                                    StatusId = StatusHelper.GroupItem.Active
                                                });
                                            }
                                        }
                                        _HCoreContext.SMSGroupItem.AddRange(_SMSGroupItems);
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var GroupDetailsUpdate = _HCoreContext.SMSGroup.Where(x => x.Id == _Request.GroupId).FirstOrDefault();
                                    if (GroupDetailsUpdate != null)
                                    {
                                        GroupDetailsUpdate.StatusId = StatusHelper.Group.Active;
                                        GroupDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            else if (GroupDetails.SourceId == SmsCampaignHelper.TypeHelper.GroupSourceType.External && _Request.Items != null && _Request.Items.Count > 0)
                            {
                                _Request.Items = _Request.Items.Where(x => x.MobileNumber != null).ToList();
                                int Limit = 1;
                                if (_Request.Items.Count > 500)
                                {
                                    Limit = _Request.Items.Count / 500;
                                }
                                Limit++;
                                int Offset = 0;
                                for (int i = 0; i < Limit; i++)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _SMSGroupItems = new List<SMSGroupItem>();
                                        var Items = _Request.Items.Skip(Offset).Take(500).ToList();
                                        Offset += 500;
                                        if (Items.Count > 0)
                                        {
                                            foreach (var mItem in Items)
                                            {
                                                
                                                _SMSGroupItems.Add(new SMSGroupItem
                                                {
                                                    GroupId = _Request.GroupId,
                                                    Name = mItem.Name,
                                                    MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
                                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                                    StatusId = StatusHelper.GroupItem.Active
                                                });
                                            }
                                        }
                                        _HCoreContext.SMSGroupItem.AddRange(_SMSGroupItems);
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var GroupDetailsUpdate = _HCoreContext.SMSGroup.Where(x => x.Id == _Request.GroupId).FirstOrDefault();
                                    if (GroupDetailsUpdate != null)
                                    {
                                        GroupDetailsUpdate.StatusId = StatusHelper.Group.Active;
                                        GroupDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SaveGroupItem", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteGroup(OGroup.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                }
                if (_Request.GroupId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0305", ResponseCode.CA0305);
                }
                if (string.IsNullOrEmpty(_Request.GroupKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0306", ResponseCode.CA0306);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.SMSGroup
                        .Where(x => x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        && x.Id == _Request.GroupId
                        && x.Guid == _Request.GroupKey
                        ).Select(x => new
                        {
                            Id = x.Id,
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        var SmsGroupItems = _HCoreContext.SMSGroupItem.Where(x => x.GroupId == _Request.GroupId).ToList();
                        if (SmsGroupItems.Count > 0)
                        {
                            _HCoreContext.SMSGroupItem.RemoveRange(SmsGroupItems);
                        }
                        var SmsConditions = _HCoreContext.SMSGroupCondition.Where(x => x.GroupId == _Request.GroupId).ToList();
                        if (SmsConditions.Count > 0)
                        {
                            _HCoreContext.SMSGroupCondition.RemoveRange(SmsConditions);
                        }
                        _HCoreContext.SaveChanges();
                        using (_HCoreContext = new HCoreContext())
                        {
                            var GroupDetails = _HCoreContext.SMSGroup
                                                .Where(x => x.Id == Details.Id)
                                                .FirstOrDefault();
                            _HCoreContext.SMSGroup.Remove(GroupDetails);
                            _HCoreContext.SaveChanges();
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0307", ResponseCode.CA0307);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DeleteGroup", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the group items.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteGroupItems(OGroupItem.Delete.BulkDelete _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.GroupId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0308", ResponseCode.CA0308);
                }
                if (_Request.IsDeleteAll == false && (_Request.Items == null || _Request.Items.Count == 0))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0309", ResponseCode.CA0309);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.IsDeleteAll)
                    {
                        var Items = _HCoreContext.SMSGroupItem.Where(x => x.GroupId == _Request.GroupId).ToList();
                        if (Items.Count > 0)
                        {
                            _HCoreContext.SMSGroupItem.RemoveRange(Items);
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0310", ResponseCode.CA0310);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0311", ResponseCode.CA0311);
                        }
                    }
                    else
                    {
                        foreach (var GroupItem in _Request.Items)
                        {
                            var Items = _HCoreContext.SMSGroupItem.Where(x => x.Id == GroupItem.GroupItemId && x.GroupId == _Request.GroupId).FirstOrDefault();
                            if (Items != null)
                            {
                                _HCoreContext.SMSGroupItem.Remove(Items);
                            }
                        }
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0310", ResponseCode.CA0310);

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DeleteGroupItems", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the group list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetGroup(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "GroupId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.SMSGroup
                                                .Where(x => x.AccountId == _Request.AccountId)
                                                .Select(x => new OGroup.List
                                                {
                                                    GroupId = x.Id,
                                                    GroupKey = x.Guid,
                                                    Title = x.Title,
                                                    CreateDate = x.CreateDate,
                                                    ItemCount = x.SMSGroupItem.Count,
                                                    FileName = x.FileName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OGroup.List> _List = _HCoreContext.SMSGroup
                                                .Where(x => x.AccountId == _Request.AccountId)
                                                .Select(x => new OGroup.List
                                                {
                                                    GroupId = x.Id,
                                                    GroupKey = x.Guid,
                                                    Title = x.Title,
                                                    CreateDate = x.CreateDate,
                                                    ItemCount = x.SMSGroupItem.Count,
                                                    FileName = x.FileName,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        DataItem.Items = _HCoreContext.SMSGroupItem.Where(x => x.GroupId == DataItem.GroupId)
                            .Select(x => new OGroup.Item
                            {
                                Name = x.Name,
                                MobileNumber = x.MobileNumber,
                                IconUrl = x.Account.IconStorage.Path,
                            }).Skip(0).Take(5).ToList();
                        foreach (var item in DataItem.Items)
                        {
                            if (!string.IsNullOrEmpty(item.IconUrl))
                            {
                                item.IconUrl = _AppConfig.StorageUrl + item.IconUrl;
                            }
                            else
                            {
                                item.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetGroup", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the group item list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetGroupItem(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "GroupItemId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.SMSGroupItem
                                                .Where(x => x.GroupId == _Request.ReferenceId)
                                                .Select(x => new OGroupItem.List
                                                {
                                                    GroupItemId = x.Id,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    IconUrl = x.Account.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OGroupItem.List> _List = _HCoreContext.SMSGroupItem
                                                .Where(x => x.GroupId == _Request.ReferenceId)
                                                .Select(x => new OGroupItem.List
                                                {
                                                    GroupItemId = x.Id,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    IconUrl = x.Account.IconStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetGroupItem", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the group.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateGroup(OGroup.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.GroupId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0305", ResponseCode.CA0305);
                }
                if (string.IsNullOrEmpty(_Request.GroupKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0306", ResponseCode.CA0306);
                }
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0302", ResponseCode.CA0302);
                }
                if (string.IsNullOrEmpty(_Request.SourceCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0304", ResponseCode.CA0304);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var GroupDetails = _HCoreContext.SMSGroup
                        .Where(x => x.Id == _Request.GroupId
                        && x.Guid == _Request.GroupKey)
                        .FirstOrDefault();
                    if (GroupDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Title) && GroupDetails.Title != _Request.Title)
                        {
                            GroupDetails.Title = _Request.Title;
                        }
                        if (!string.IsNullOrEmpty(_Request.Condition) && GroupDetails.Title != _Request.Condition)
                        {
                            GroupDetails.Condition = _Request.Condition;
                        }
                        if (!string.IsNullOrEmpty(_Request.FileName) && GroupDetails.FileName != _Request.FileName)
                        {
                            GroupDetails.FileName = _Request.FileName;
                        }
                        GroupDetails.ModifyById = _Request.UserReference.AccountId;
                        GroupDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        if (_Request.IsItemUpdated)
                        {
                            var SmsGroupItems = _HCoreContext.SMSGroupItem.Where(x => x.GroupId == _Request.GroupId).ToList();
                            _HCoreContext.SMSGroupItem.RemoveRange(SmsGroupItems);
                            var SmsConditions = _HCoreContext.SMSGroupCondition.Where(x => x.GroupId == _Request.GroupId).ToList();
                            _HCoreContext.SMSGroupCondition.RemoveRange(SmsConditions);
                            _HCoreContext.SaveChanges();
                            using (_HCoreContext = new HCoreContext())
                            {
                                if (_Request.Conditions != null && _Request.Conditions.Count > 0)
                                {
                                    _SMSGroupConditions = new List<SMSGroupCondition>();
                                    foreach (var Item in _Request.Conditions)
                                    {
                                        _SMSGroupConditions.Add(new SMSGroupCondition
                                        {
                                            GroupId = GroupDetails.Id,
                                            Name = Item.Name,
                                            Value = Item.Value,
                                            CreateDate = HCoreHelper.GetGMTDateTime(),
                                            CreatedById = _Request.UserReference.AccountId
                                        });
                                    }
                                    _HCoreContext.SMSGroupCondition.AddRange(_SMSGroupConditions);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            var system = ActorSystem.Create("ActorGroupSync");
                            var greeter = system.ActorOf<ActorGroupSync>("ActorGroupSync");
                            greeter.Tell(_Request);
                        }
                        var _Response = new
                        {
                            ReferenceId = GroupDetails.Id,
                            ReferenceKey = GroupDetails.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA0312", ResponseCode.CA0312);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "UpdateGroup", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the group details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetGroup(OGroup.Reference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.GroupId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0305", ResponseCode.CA0305);
                }
                if (string.IsNullOrEmpty(_Request.GroupKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0306", ResponseCode.CA0306);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.SMSGroup
                        .Where(x => x.Id == _Request.GroupId
                        && x.Guid == _Request.GroupKey
                        && x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        ).Select(x => new OGroup.Details.Info
                        {
                            GroupId = x.Id,
                            GroupKey = x.Guid,
                            Title = x.Title,
                            SourceCode = x.Source.SystemName,
                            Condition = x.Condition,
                            FileName = x.FileName,
                            ItemCount = x.SMSGroupItem.Count,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        })
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        Details.Conditions = _HCoreContext.SMSGroupCondition
                            .Where(x => x.GroupId == Details.GroupId)
                            .Select(x => new OGroup.Details.Condition
                            {
                                Name = x.Name,
                                Value = x.Value,
                            }).ToList();
                        //Details.Items = _HCoreContext.SMSGroupItem
                        //    .Where(x => x.GroupId == Details.GroupId)
                        //    .Select(x => new OGroup.Details.Item
                        //    {
                        //         Name = x.Name,
                        //         MobileNumber = x.
                        //    }).ToList();
                        //var SmsGroupItems = _HCoreContext.SMSGroupItem.Where(x => x.GroupId == _Request.GroupId).ToList();
                        //_HCoreContext.SMSGroupItem.RemoveRange(SmsGroupItems);
                        //var SmsConditions = _HCoreContext.SMSGroupCondition.Where(x => x.GroupId == _Request.GroupId).ToList();
                        //_HCoreContext.SMSGroupCondition.RemoveRange(SmsConditions);
                        //_HCoreContext.SaveChanges();
                        //using (_HCoreContext = new HCoreContext())
                        //{
                        //    var GroupDetails = _HCoreContext.SMSGroup
                        //                        .Where(x => x.AccountId == _Request.AccountId
                        //                        && x.Account.Guid == _Request.AccountKey
                        //                        && x.Id == _Request.GroupId
                        //                        && x.Guid == _Request.GroupKey)
                        //                        .FirstOrDefault();
                        //    _HCoreContext.SMSGroup.RemoveRange(GroupDetails);
                        //    _HCoreContext.SaveChanges();
                        //}
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "CA0200", ResponseCode.CA0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetGroup", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
    }
}
