//==================================================================================
// FileName: FrameworkAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Actor;
using HCore.TUC.SmsCampaign.Helper;
using HCore.TUC.SmsCampaign.Object;
using HCore.TUC.SmsCampaign.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper.TypeHelper;
namespace HCore.TUC.SmsCampaign.Framework
{
    public class FrameworkAccount
    {
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the list of accounts.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccounts(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime T30 = HCoreHelper.GetGMTDate().AddMonths(-1);
                    DateTime TNow = HCoreHelper.GetGMTDate().AddDays(1);
                    if (_Request.Type == "Active")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.StatusId > StatusHelper.SmsCampaign.deleteting
                                                    && x.SMSCampaignAccount.Any(a => a.SendDate > T30 && a.SendDate < TNow)
                                                    && x.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = x.SMSCampaignAccount.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OAccount.List> _List = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.StatusId > StatusHelper.SmsCampaign.deleteting
                                                    && x.SMSCampaignAccount.Any(a => a.SendDate > T30 && a.SendDate < TNow)
                                                    && x.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = _HCoreContext.SMSCampaign.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else if(_Request.Type == "NotUsed")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.CountryId == _Request.UserReference.CountryId
                                                    && !x.SMSCampaignAccount.Any())
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = x.SMSCampaignAccount.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OAccount.List> _List = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.CountryId == _Request.UserReference.CountryId
                                                    && !x.SMSCampaignAccount.Any())
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = _HCoreContext.SMSCampaign.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else if(_Request.Type == "Dead")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.SMSCampaignAccount.Any(x => x.StatusId > StatusHelper.SmsCampaign.deleteting && x.SendDate > T30)
                                                    && x.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = x.SMSCampaignAccount.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OAccount.List> _List = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.SMSCampaignAccount.Any(x => x.StatusId > StatusHelper.SmsCampaign.deleteting && x.SendDate > T30)
                                                    && x.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = _HCoreContext.SMSCampaign.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = x.SMSCampaignAccount.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OAccount.List> _List = _HCoreContext.HCUAccount
                                                    .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OAccount.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        IconUrl = x.IconStorage.Path,
                                                        EmailAddress = x.EmailAddress,
                                                        MobileNumber = x.MobileNumber,
                                                        Address = x.Address,

                                                        Balance = x.HCUAccountBalanceAccount.Where(z => z.SourceId == TransactionSource.PromotionCampaign).Select(z => z.Balance).FirstOrDefault(),

                                                        Campaigns = x.SMSCampaignAccount.Count,
                                                        SmsSent = _HCoreContext.SMSCampaignGroupItem.Count(z => z.Campaign.AccountId == x.Id && z.StatusId != StatusHelper.SmsStatus.Unsent),
                                                        CampaignDate = _HCoreContext.SMSCampaign.Where(a => a.AccountId == x.Id).Select(a => a.SendDate).FirstOrDefault(),

                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAccounts", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }


    }
}
