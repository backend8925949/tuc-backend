//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to sms analytics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Actor;
using HCore.TUC.SmsCampaign.Helper;
using HCore.TUC.SmsCampaign.Object;
using HCore.TUC.SmsCampaign.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper.TypeHelper;
namespace HCore.TUC.SmsCampaign.Framework
{
    public class FrameworkAnalytics
    {
        OAnalytics.SmsOverview _SmsOverview;
        OAnalytics.CampaignOverview _CampaignOverview;
        HCoreContext _HCoreContext;
        OAnalytics.AccountOverview _AccountOverview;
        /// <summary>
        /// Description: Gets the SMS overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSmsOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                // 2,807,772
                // 17708
                //if (_Request.AccountId < 1)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                //}
                //if (string.IsNullOrEmpty(_Request.AccountKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                //}
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ReferenceId > 0)
                    {
                        _SmsOverview = new OAnalytics.SmsOverview();
                        _SmsOverview.Total = _HCoreContext.SMSCampaignGroupItem.Count(x => x.CampaignId == _Request.ReferenceId && x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _SmsOverview.Sent = _HCoreContext.SMSCampaignGroupItem.Count(x => x.CampaignId == _Request.ReferenceId && x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.IsSent == 1);
                        _SmsOverview.UnSent = _HCoreContext.SMSCampaignGroupItem.Count(x => x.CampaignId == _Request.ReferenceId && x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _SmsOverview.Delivered = _HCoreContext.SMSCampaignGroupItem.Count(x => x.CampaignId == _Request.ReferenceId && x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsStatus.Delivered);
                        _SmsOverview.Pending = _HCoreContext.SMSCampaignGroupItem.Count(x => x.CampaignId == _Request.ReferenceId && x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsStatus.Pending);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SmsOverview, "CA0200", ResponseCode.CA0200);
                    }
                    else if (_Request.AccountId > 0)
                    {
                        _SmsOverview = new OAnalytics.SmsOverview();
                        _SmsOverview.Total = _HCoreContext.SMSCampaignGroupItem.Count(x => x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _SmsOverview.Sent = _HCoreContext.SMSCampaignGroupItem.Count(x => x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.IsSent == 1);
                        _SmsOverview.UnSent = _HCoreContext.SMSCampaignGroupItem.Count(x => x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _SmsOverview.Delivered = _HCoreContext.SMSCampaignGroupItem.Count(x => x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsStatus.Delivered);
                        _SmsOverview.Pending = _HCoreContext.SMSCampaignGroupItem.Count(x => x.Campaign.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsStatus.Pending);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SmsOverview, "CA0200", ResponseCode.CA0200);
                    }
                    else
                    {
                        _SmsOverview = new OAnalytics.SmsOverview();
                        _SmsOverview.Total = _HCoreContext.SMSCampaignGroupItem.Count(x => x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _SmsOverview.Sent = _HCoreContext.SMSCampaignGroupItem.Count(x => x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.IsSent == 1);
                        _SmsOverview.UnSent = _HCoreContext.SMSCampaignGroupItem.Count(x => x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _SmsOverview.Delivered = _HCoreContext.SMSCampaignGroupItem.Count(x => x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsStatus.Delivered);
                        _SmsOverview.Pending = _HCoreContext.SMSCampaignGroupItem.Count(x => x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsStatus.Pending);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SmsOverview, "CA0200", ResponseCode.CA0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetSmsOverview", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the campaign overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaignOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                //if (_Request.AccountId < 1)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                //}
                //if (string.IsNullOrEmpty(_Request.AccountKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                //}
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0)
                    {
                        _CampaignOverview = new OAnalytics.CampaignOverview();
                        _CampaignOverview.Total = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _CampaignOverview.TotalCampaignAmount = _HCoreContext.SMSCampaign.Where(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId).Sum(x => x.Budget);
                        //_CampaignOverview.Running = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsCampaign.running);
                        _CampaignOverview.Running = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId  && x.StatusId == StatusHelper.SmsCampaign.running);
                        _CampaignOverview.Upcoming = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsCampaign.published);
                        _CampaignOverview.PendingApproval = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsCampaign.waitingforreview);
                        _CampaignOverview.Completed = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsCampaign.completed);
                        _CampaignOverview.Rejected = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsCampaign.rejected);
                        _CampaignOverview.Draft = _HCoreContext.SMSCampaign.Count(x => x.AccountId == _Request.AccountId && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.StatusId == StatusHelper.SmsCampaign.draft);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CampaignOverview, "CA0200", ResponseCode.CA0200);
                    }
                    else
                    {
                        _CampaignOverview = new OAnalytics.CampaignOverview();
                        _CampaignOverview.Total = _HCoreContext.SMSCampaign.Count(x => x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.TotalCampaignAmount = _HCoreContext.SMSCampaign.Where(x => x.Account.CountryId == _Request.UserReference.CountryId).Sum(x => x.Budget);
                        _CampaignOverview.Creating = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.creating && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Deleteting = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.deleteting && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Draft = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.draft && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.WaitingForReview = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.waitingforreview && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.InReview = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.inreview && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Approved = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.approved && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Rejected = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.rejected && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        //_CampaignOverview.Published = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.published && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _CampaignOverview.Published = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.published && x.Account.CountryId == _Request.UserReference.CountryId);
                        //_CampaignOverview.Running = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.running && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate);
                        _CampaignOverview.Running = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.running && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Paused = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.paused && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Cancelled = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.cancelled && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Expired = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.expired && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        _CampaignOverview.Completed = _HCoreContext.SMSCampaign.Count(x => x.StatusId == StatusHelper.SmsCampaign.completed && x.SendDate > _Request.StartDate && x.SendDate < _Request.EndDate && x.Account.CountryId == _Request.UserReference.CountryId);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CampaignOverview, "CA0200", ResponseCode.CA0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetCampaignOverview", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the account overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                //if (_Request.AccountId < 1)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                //}
                //if (string.IsNullOrEmpty(_Request.AccountKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                //}

                using (_HCoreContext = new HCoreContext())
                {
                    DateTime T30 = HCoreHelper.GetGMTDate().AddMonths(-1);
                    DateTime TNow = HCoreHelper.GetGMTDate().AddDays(1);
                    _AccountOverview = new OAnalytics.AccountOverview();
                    _AccountOverview.Total = _HCoreContext.HCUAccount.Count(x => x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Merchant && x.CountryId == _Request.UserReference.CountryId);
                    _AccountOverview.NotUsing = _AccountOverview.Total - _HCoreContext.SMSCampaign.Where(x => x.Account.CountryId == _Request.UserReference.CountryId).Select(x => x.AccountId).Distinct().Count();
                    _AccountOverview.Active = _HCoreContext.SMSCampaign.Where(x => x.StatusId > StatusHelper.SmsCampaign.deleteting && x.SendDate > T30 && x.SendDate < TNow &&  x.Account.CountryId == _Request.UserReference.CountryId).Select(x => x.AccountId).Distinct().Count();
                    _AccountOverview.Dead = _HCoreContext.SMSCampaign.Where(x => x.StatusId > StatusHelper.SmsCampaign.deleteting && x.SendDate > T30 && x.Account.CountryId == _Request.UserReference.CountryId).Select(x => x.AccountId).Distinct().Count();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AccountOverview, "CA0200", ResponseCode.CA0200);
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetCampaignOverview", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
    }
}
