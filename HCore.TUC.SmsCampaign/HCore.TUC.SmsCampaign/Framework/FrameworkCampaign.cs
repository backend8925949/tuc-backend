//==================================================================================
// FileName: FrameworkCampaign.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to campaign
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.SmsCampaign.Actor;
using HCore.TUC.SmsCampaign.Helper;
using HCore.TUC.SmsCampaign.Object;
using HCore.TUC.SmsCampaign.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper.TypeHelper;
namespace HCore.TUC.SmsCampaign.Framework
{
    public class FrameworkCampaign
    {
        SMSCampaign _SMSCampaign;
        SMSCampaignGroup _SMSCampaignGroup;
        FrameworkHelper _FrameworkHelper;
        HCoreContext _HCoreContext;
        List<SMSCampaignGroupCondition> _SMSCampaignGroupConditions;
        List<SMSCampaignGroupItem> _SMSCampaignGroupItems;
        ManageCoreTransaction _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        OCoreTransaction.Request _CoreTransactionRequest;
        /// <summary>
        /// Description: Calculates the length of the SMS.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns>System.Int32.</returns>
        public static int CalculateSmsLength(string text)
        {
            //if (IsEnglishText(text))
            //{
            //    return text.Length <= 160 ? 1 : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(text.Length) / 153));
            //}
            return text.Length <= 160 ? 1 : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(text.Length) / 153));
            //return text.Length <= 70 ? 1 : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(text.Length) / 67));
        }
        /// <summary>
        /// Description: Determines whether [is english text] [the specified text].
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns><c>true</c> if [is english text] [the specified text]; otherwise, <c>false</c>.</returns>
        public static bool IsEnglishText(string text)
        {
            return Regex.IsMatch(text, @"^[\u0000-\u007F]+$");
        }
        /// <summary>
        /// Description: Cleans the campaign items.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void CleanCampaignItems(OCampaign.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId > 0)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CampaignItems = _HCoreContext.SMSCampaignGroupItem.Where(x => x.CampaignId == _Request.ReferenceId).ToList();
                        if (CampaignItems.Count > 0)
                        {
                            _HCoreContext.SMSCampaignGroupItem.RemoveRange(CampaignItems);
                            _HCoreContext.SaveChanges();
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var SMSCampaignGroupConditions = _HCoreContext.SMSCampaignGroupCondition.Where(x => x.CampaignGroup.CampaignId == _Request.ReferenceId).ToList();
                        if (SMSCampaignGroupConditions.Count > 0)
                        {
                            _HCoreContext.SMSCampaignGroupCondition.RemoveRange(SMSCampaignGroupConditions);
                            _HCoreContext.SaveChanges();
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var SMSCampaignGroups = _HCoreContext.SMSCampaignGroup.Where(x => x.CampaignId == _Request.ReferenceId).ToList();
                        if (SMSCampaignGroups.Count > 0)
                        {
                            _HCoreContext.SMSCampaignGroup.RemoveRange(SMSCampaignGroups);
                            _HCoreContext.SaveChanges();
                        }
                    }
                    var system = ActorSystem.Create("ActorCampaignSync");
                    var greeter = system.ActorOf<ActorCampaignSync>("ActorCampaignSync");
                    greeter.Tell(_Request);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "SaveCampaignItem", _Exception, _Request.UserReference);
            }
            #endregion
        }

        //internal void SaveCampaignItem(OCampaign.Save.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (_Request.ReferenceId > 0)
        //        {
        //            using (_HCoreContext = new HCoreContext())
        //            {
        //                var CampaignDetails = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
        //                if (CampaignDetails != null)
        //                {
        //                    _HCoreContext.Dispose();
        //                    if (_Request.RecipientSubTypeCode == GroupSourceType.GroupS && _Request.Groups != null && _Request.Groups.Count > 0)
        //                    {
        //                        _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
        //                        foreach (var Group in _Request.Groups)
        //                        {
        //                            using (_HCoreContext = new HCoreContext())
        //                            {
        //                                var GroupDetails = _HCoreContext.SMSGroup.Where(x => x.Id == Group.GroupId).FirstOrDefault();
        //                                var GroupConditionsDetails = _HCoreContext.SMSGroupCondition.Where(x => x.GroupId == Group.GroupId).ToList();
        //                                _SMSCampaignGroup = new SMSCampaignGroup();
        //                                if (GroupConditionsDetails != null && GroupConditionsDetails.Count > 0)
        //                                {
        //                                    _SMSCampaignGroupConditions = new List<SMSCampaignGroupCondition>();
        //                                    foreach (var Item in GroupConditionsDetails)
        //                                    {
        //                                        _SMSCampaignGroupConditions.Add(new SMSCampaignGroupCondition
        //                                        {
        //                                            Name = Item.Name,
        //                                            Value = Item.Value,
        //                                            CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                            CreatedById = _Request.UserReference.AccountId
        //                                        });
        //                                    }
        //                                    _SMSCampaignGroup.SMSCampaignGroupCondition = _SMSCampaignGroupConditions;
        //                                }
        //                                _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
        //                                _SMSCampaignGroup.GroupId = GroupDetails.Id;
        //                                _SMSCampaignGroup.SourceId = GroupDetails.SourceId;
        //                                _SMSCampaignGroup.Title = GroupDetails.Title;
        //                                _SMSCampaignGroup.Condition = GroupDetails.Condition;
        //                                _SMSCampaignGroup.FileName = GroupDetails.FileName;
        //                                _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
        //                                _SMSCampaignGroup.StatusId = 2;
        //                                _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
        //                                _HCoreContext.SaveChanges();
        //                                long GroupId = _SMSCampaignGroup.Id;
        //                                using (_HCoreContext = new HCoreContext())
        //                                {
        //                                    var GroupItems = _HCoreContext.SMSGroupItem.Where(x => x.GroupId == Group.GroupId && x.StatusId == 2 && x.MobileNumber != null)
        //                                                    .Select(x => new
        //                                                    {
        //                                                        MobileNumber = x.MobileNumber,
        //                                                        Name = x.Name,
        //                                                        AccountId = x.AccountId,
        //                                                    }).Distinct().ToList();
        //                                    if (GroupItems.Count > 0)
        //                                    {
        //                                        foreach (var GroupItem in GroupItems)
        //                                        {
        //                                            _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
        //                                            {
        //                                                MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, GroupItem.MobileNumber),
        //                                                CampaignId = CampaignDetails.Id,
        //                                                GroupId = GroupId,
        //                                                Name = GroupItem.Name,
        //                                                AccountId = GroupItem.AccountId,
        //                                                CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                                StatusId = StatusHelper.SmsStatus.Unsent
        //                                            });
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        if (_SMSCampaignGroupItems.Count > 0)
        //                        {
        //                            int Limit = 1;
        //                            var ItemsList = _SMSCampaignGroupItems.Distinct().ToList();
        //                            if (ItemsList.Count > 0)
        //                            {
        //                                if (CampaignDetails.ItemLimit > 0)
        //                                {
        //                                    ItemsList = ItemsList.Skip(0).Take(CampaignDetails.ItemLimit).ToList();
        //                                }
        //                                if (ItemsList.Count > 500)
        //                                {
        //                                    Limit = ItemsList.Count / 500;
        //                                }
        //                        Limit++;
        //                                int Offset = 0;
        //                                for (int i = 0; i < Limit; i++)
        //                                {
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        var Items = ItemsList.Skip(Offset).Take(500).ToList();
        //                                        Offset += 500;
        //                                        if (Items.Count > 0)
        //                                        {
        //                                            _HCoreContext.SMSCampaignGroupItem.AddRange(Items);
        //                                            _HCoreContext.SaveChanges();
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
        //                            if (CampaignDetailsUpdate != null)
        //                            {
        //                                //#region UPDATE SMS COST
        //                                //int MessageCount = CalculateSmsLength(CampaignDetailsUpdate.Message);
        //                                //int MSD = 0;
        //                                //if (_Request.TotalItem > 0)
        //                                //{
        //                                //    MSD = MessageCount * CampaignDetailsUpdate.ItemLimit;
        //                                //}
        //                                //else
        //                                //{
        //                                //    MSD = MessageCount;
        //                                //}
        //                                //double PerSmsItem = 4;
        //                                //double SmsCost = PerSmsItem * MSD;
        //                                //CampaignDetailsUpdate.Budget = SmsCost;
        //                                //#endregion
        //                                if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
        //                                {
        //                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
        //                                }
        //                                else
        //                                {
        //                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
        //                                }
        //                                CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                _HCoreContext.SaveChanges();
        //                            }
        //                        }
        //                    }
        //                    else if (_Request.RecipientSubTypeCode == GroupSourceType.ExternalS && !string.IsNullOrEmpty(_Request.FileName) && _Request.Items != null && _Request.Items.Count > 0)
        //                    {
        //                        _Request.Items = _Request.Items.Where(x => x.MobileNumber != null).Distinct().ToList();
        //                        if (_Request.Items.Count > 0)
        //                        {
        //                            _Request.Items = _Request.Items.Skip(0).Take(CampaignDetails.ItemLimit).ToList();
        //                            using (_HCoreContext = new HCoreContext())
        //                            {
        //                                _SMSCampaignGroup = new SMSCampaignGroup();
        //                                _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
        //                                _SMSCampaignGroup.Title = HCoreHelper.GenerateSystemName(CampaignDetails.Title) + "_file";
        //                                _SMSCampaignGroup.FileName = _Request.FileName;
        //                                _SMSCampaignGroup.SourceId = GroupSourceType.External;
        //                                _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
        //                                _SMSCampaignGroup.StatusId = 2;
        //                                _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
        //                                _HCoreContext.SaveChanges();
        //                                long GroupId = _SMSCampaignGroup.Id;
        //                                int Limit = 1;
        //                                if (_Request.Items.Count > 500)
        //                                {
        //                                    Limit = _Request.Items.Count / 500;
        //                                }
        //                        Limit++;
        //                                int Offset = 0;
        //                                for (int i = 0; i < Limit; i++)
        //                                {
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
        //                                        var Items = _Request.Items.Skip(Offset).Take(500).ToList();
        //                                        Offset += 500;
        //                                        if (Items.Count > 0)
        //                                        {
        //                                            foreach (var mItem in Items)
        //                                            {
        //                                                _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
        //                                                {
        //                                                    CampaignId = CampaignDetails.Id,
        //                                                    GroupId = GroupId,
        //                                                    Name = mItem.Name,
        //                                                    MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
        //                                                    CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                                    StatusId = StatusHelper.SmsStatus.Unsent
        //                                                });
        //                                            }
        //                                        }
        //                                        _HCoreContext.SMSCampaignGroupItem.AddRange(_SMSCampaignGroupItems);
        //                                        _HCoreContext.SaveChanges();
        //                                    }
        //                                }
        //                                using (_HCoreContext = new HCoreContext())
        //                                {
        //                                    var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
        //                                    if (CampaignDetailsUpdate != null)
        //                                    {
        //                                        if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
        //                                        {
        //                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
        //                                        }
        //                                        else
        //                                        {
        //                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
        //                                        }
        //                                        CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                        _HCoreContext.SaveChanges();
        //                                    }
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (_Request.RecipientSubTypeCode == GroupSourceType.TUCS && string.IsNullOrEmpty(_Request.Condition))
        //                    {
        //                        _FrameworkHelper = new FrameworkHelper();
        //                        var Customers = _FrameworkHelper.GetCustomer(_Request.Condition, CampaignDetails.AccountId, _Request.TotalItem);
        //                        if (Customers.Count > 0)
        //                        {
        //                            using (_HCoreContext = new HCoreContext())
        //                            {
        //                                _SMSCampaignGroup = new SMSCampaignGroup();
        //                                _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
        //                                _SMSCampaignGroup.Title = HCoreHelper.GenerateSystemName(CampaignDetails.Title) + "_tuc";
        //                                _SMSCampaignGroup.FileName = _Request.FileName;
        //                                _SMSCampaignGroup.SourceId = GroupSourceType.TUC;
        //                                _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
        //                                _SMSCampaignGroup.StatusId = 2;
        //                                _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
        //                                _HCoreContext.SaveChanges();
        //                                long GroupId = _SMSCampaignGroup.Id;
        //                                int Limit = 1;
        //                                if (Customers.Count > 500)
        //                                {
        //                                    Limit = Customers.Count / 500;
        //                                }
        //                        Limit++;
        //                                int Offset = 0;
        //                                for (int i = 0; i < Limit; i++)
        //                                {
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
        //                                        var Items = Customers.Skip(Offset).Take(500).ToList();
        //                                        Offset += 500;
        //                                        if (Items.Count > 0)
        //                                        {
        //                                            foreach (var mItem in Items)
        //                                            {
        //                                                _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
        //                                                {
        //                                                    CampaignId = CampaignDetails.Id,
        //                                                    GroupId = GroupId,
        //                                                    Name = mItem.Name,
        //                                                    MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
        //                                                    AccountId = mItem.ReferenceId,
        //                                                    CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                                    StatusId = StatusHelper.SmsStatus.Unsent
        //                                                });
        //                                            }
        //                                        }
        //                                        _HCoreContext.SMSCampaignGroupItem.AddRange(_SMSCampaignGroupItems);
        //                                        _HCoreContext.SaveChanges();
        //                                    }
        //                                }
        //                                using (_HCoreContext = new HCoreContext())
        //                                {
        //                                    var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
        //                                    if (CampaignDetailsUpdate != null)
        //                                    {
        //                                        if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
        //                                        {
        //                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
        //                                        }
        //                                        else
        //                                        {
        //                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
        //                                        }
        //                                        CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                        _HCoreContext.SaveChanges();
        //                                    }
        //                                }
        //                            }
        //                        }


        //                    }
        //                    else if (_Request.RecipientSubTypeCode == GroupSourceType.CustomS && _Request.Items != null && _Request.Items.Count > 0)
        //                    {
        //                        _Request.Items = _Request.Items.Where(x => x.MobileNumber != null).Distinct().ToList();
        //                        if (_Request.Items.Count > 0)
        //                        {
        //                            _Request.Items = _Request.Items.Skip(0).Take(CampaignDetails.ItemLimit).ToList();
        //                            using (_HCoreContext = new HCoreContext())
        //                            {
        //                                _SMSCampaignGroup = new SMSCampaignGroup();
        //                                _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
        //                                _SMSCampaignGroup.Title = HCoreHelper.GenerateSystemName(CampaignDetails.Title) + "_custom";
        //                                //_SMSCampaignGroup.FileName = _Request.FileName;
        //                                _SMSCampaignGroup.SourceId = GroupSourceType.Custom;
        //                                _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
        //                                _SMSCampaignGroup.StatusId = 2;
        //                                _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
        //                                _HCoreContext.SaveChanges();
        //                                long GroupId = _SMSCampaignGroup.Id;
        //                                int Limit = _Request.Items.Count / 500;
        //                        Limit++;
        //                                int Offset = 0;
        //                                for (int i = 0; i < Limit; i++)
        //                                {
        //                                    using (_HCoreContext = new HCoreContext())
        //                                    {
        //                                        _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
        //                                        var Items = _Request.Items.Skip(Offset).Take(500).ToList();
        //                                        Offset += 500;
        //                                        if (Items.Count > 0)
        //                                        {
        //                                            foreach (var mItem in Items)
        //                                            {
        //                                                _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
        //                                                {
        //                                                    CampaignId = CampaignDetails.Id,
        //                                                    GroupId = GroupId,
        //                                                    Name = mItem.Name,
        //                                                    MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
        //                                                    CreateDate = HCoreHelper.GetGMTDateTime(),
        //                                                    StatusId = StatusHelper.SmsStatus.Unsent
        //                                                });
        //                                            }
        //                                        }
        //                                        _HCoreContext.SMSCampaignGroupItem.AddRange(_SMSCampaignGroupItems);
        //                                        _HCoreContext.SaveChanges();
        //                                    }
        //                                }
        //                                using (_HCoreContext = new HCoreContext())
        //                                {
        //                                    var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
        //                                    if (CampaignDetailsUpdate != null)
        //                                    {
        //                                        if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
        //                                        {
        //                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
        //                                        }
        //                                        else
        //                                        {
        //                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
        //                                        }
        //                                        CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                        _HCoreContext.SaveChanges();
        //                                    }
        //                                }

        //                            }
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException(LogLevel.High, "SaveCampaignItem", _Exception, _Request.UserReference);
        //    }
        //    #endregion
        //}

        /// <summary>
        /// Description: Duplicates the campaign item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void DuplicateCampaignItem(OCampaign.Duplicate.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId > 0)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CampaignGroupConditions = _HCoreContext.SMSCampaignGroupCondition.Where(x => x.CampaignGroup.CampaignId == _Request.ReferenceId)
                            .Select(x => new
                            {
                                x.Id,
                                x.CampaignGroupId,
                                x.Name,
                                x.Value,
                            }).ToList();
                        var CampaignGroups = _HCoreContext.SMSCampaignGroup.Where(x => x.CampaignId == _Request.ReferenceId)
                           .Select(x => new
                           {
                               x.Id,
                               x.GroupId,
                               x.SourceId,
                               Title = x.Title + "_copy",
                               x.Condition,
                               x.FileName,
                           }).ToList();
                        var CampaignItems = _HCoreContext.SMSCampaignGroupItem.Where(x => x.CampaignId == _Request.ReferenceId)
                           .Select(x => new
                           {
                               x.Id,
                               GroupId = x.GroupId,
                               Name = x.Name,
                               MobileNumber = x.MobileNumber,
                               AccountId = x.AccountId,
                           }).ToList();
                        _HCoreContext.Dispose();
                        foreach (var CampaignGroup in CampaignGroups)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                _SMSCampaignGroup = new SMSCampaignGroup();
                                _SMSCampaignGroup.CampaignId = _Request.NewCampaignId;
                                _SMSCampaignGroup.GroupId = CampaignGroup.GroupId;
                                _SMSCampaignGroup.SourceId = CampaignGroup.SourceId;
                                _SMSCampaignGroup.Title = CampaignGroup.Title;
                                _SMSCampaignGroup.Condition = CampaignGroup.Condition;
                                _SMSCampaignGroup.FileName = CampaignGroup.FileName;
                                _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                                _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
                                _SMSCampaignGroup.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                var GrpCons = CampaignGroupConditions.Where(x => x.CampaignGroupId == CampaignGroup.Id).ToList();
                                if (GrpCons.Count > 0)
                                {
                                    _SMSCampaignGroupConditions = new List<SMSCampaignGroupCondition>();
                                    foreach (var GrpCon in GrpCons)
                                    {
                                        _SMSCampaignGroupConditions.Add(new SMSCampaignGroupCondition
                                        {
                                            Name = GrpCon.Name,
                                            Value = GrpCon.Value,
                                            CreateDate = HCoreHelper.GetGMTDateTime(),
                                            CreatedById = _Request.UserReference.AccountId
                                        });
                                    }
                                    _SMSCampaignGroup.SMSCampaignGroupCondition = _SMSCampaignGroupConditions;
                                }
                                _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
                                _HCoreContext.SaveChanges();
                                long NewGroupId = _SMSCampaignGroup.Id;
                                var GrpItems = CampaignItems.Where(x => x.GroupId == CampaignGroup.Id).ToList();
                                if (GrpItems.Count > 0)
                                {
                                    int Limit = 1;
                                    int Offset = 0;
                                    if (GrpItems.Count > 500)
                                    {
                                        Limit = GrpItems.Count / 500;
                                    }
                                Limit++;
                                    for (int i = 0; i < Limit; i++)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
                                            var Items = CampaignItems.Skip(Offset).Take(500).ToList();
                                            Offset += 500;
                                            if (Items.Count > 0)
                                            {
                                                foreach (var mItem in Items)
                                                {
                                                    _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
                                                    {
                                                        CampaignId = _Request.NewCampaignId,
                                                        GroupId = NewGroupId,
                                                        Name = mItem.Name,
                                                        MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
                                                        AccountId = mItem.AccountId,
                                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                                        StatusId = StatusHelper.SmsStatus.Unsent
                                                    });
                                                }
                                            }
                                            _HCoreContext.SMSCampaignGroupItem.AddRange(_SMSCampaignGroupItems);
                                            _HCoreContext.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            var GroupDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.NewCampaignId).FirstOrDefault();
                            if (GroupDetailsUpdate != null)
                            {
                                GroupDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
                                GroupDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                            }
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DuplicateCampaignItem", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }


        /// <summary>
        /// Description: Saves the campaign item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void SaveCampaignItem(OCampaign.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId > 0)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var CampaignDetails = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                        if (CampaignDetails != null)
                        {
                            _HCoreContext.Dispose();
                            if (_Request.RecipientSubTypeCode == GroupSourceType.GroupS && _Request.Groups != null && _Request.Groups.Count > 0)
                            {
                                _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
                                foreach (var Group in _Request.Groups)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var GroupDetails = _HCoreContext.SMSGroup.Where(x => x.Id == Group.GroupId).FirstOrDefault();
                                        var GroupConditionsDetails = _HCoreContext.SMSGroupCondition.Where(x => x.GroupId == Group.GroupId).ToList();
                                        _SMSCampaignGroup = new SMSCampaignGroup();
                                        if (GroupConditionsDetails != null && GroupConditionsDetails.Count > 0)
                                        {
                                            _SMSCampaignGroupConditions = new List<SMSCampaignGroupCondition>();
                                            foreach (var Item in GroupConditionsDetails)
                                            {
                                                _SMSCampaignGroupConditions.Add(new SMSCampaignGroupCondition
                                                {
                                                    Name = Item.Name,
                                                    Value = Item.Value,
                                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                                    CreatedById = _Request.UserReference.AccountId
                                                });
                                            }
                                            _SMSCampaignGroup.SMSCampaignGroupCondition = _SMSCampaignGroupConditions;
                                        }
                                        _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
                                        _SMSCampaignGroup.GroupId = GroupDetails.Id;
                                        _SMSCampaignGroup.SourceId = GroupDetails.SourceId;
                                        _SMSCampaignGroup.Title = GroupDetails.Title;
                                        _SMSCampaignGroup.Condition = GroupDetails.Condition;
                                        _SMSCampaignGroup.FileName = GroupDetails.FileName;
                                        _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
                                        _SMSCampaignGroup.StatusId = 2;
                                        _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
                                        _HCoreContext.SaveChanges();
                                        long GroupId = _SMSCampaignGroup.Id;
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var GroupItems = _HCoreContext.SMSGroupItem.Where(x => x.GroupId == Group.GroupId && x.StatusId == 2 && x.MobileNumber != null)
                                                            .Select(x => new
                                                            {
                                                                MobileNumber = x.MobileNumber,
                                                                Name = x.Name,
                                                                AccountId = x.AccountId,
                                                            }).Distinct().ToList();
                                            if (GroupItems.Count > 0)
                                            {
                                                foreach (var GroupItem in GroupItems)
                                                {
                                                    _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
                                                    {
                                                        MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, GroupItem.MobileNumber),
                                                        CampaignId = CampaignDetails.Id,
                                                        GroupId = GroupId,
                                                        Name = GroupItem.Name,
                                                        AccountId = GroupItem.AccountId,
                                                        CreateDate = HCoreHelper.GetGMTDateTime(),
                                                        StatusId = StatusHelper.SmsStatus.Unsent
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                                if (_SMSCampaignGroupItems.Count > 0)
                                {
                                    int Limit = 1;
                                    var ItemsList = _SMSCampaignGroupItems.Distinct().ToList();
                                    if (ItemsList.Count > 0)
                                    {
                                        if (CampaignDetails.ItemLimit > 0)
                                        {
                                            ItemsList = ItemsList.Skip(0).Take(CampaignDetails.ItemLimit).ToList();
                                        }
                                        if (ItemsList.Count > 500)
                                        {
                                            Limit = ItemsList.Count / 500;
                                        }
                                        Limit++;
                                        int Offset = 0;
                                        for (int i = 0; i < Limit; i++)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                var Items = ItemsList.Skip(Offset).Take(500).ToList();
                                                Offset += 500;
                                                if (Items.Count > 0)
                                                {
                                                    _HCoreContext.SMSCampaignGroupItem.AddRange(Items);
                                                    _HCoreContext.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                }
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                                    if (CampaignDetailsUpdate != null)
                                    {
                                        //#region UPDATE SMS COST
                                        //int MessageCount = CalculateSmsLength(CampaignDetailsUpdate.Message);
                                        //int MSD = 0;
                                        //if (_Request.TotalItem > 0)
                                        //{
                                        //    MSD = MessageCount * CampaignDetailsUpdate.ItemLimit;
                                        //}
                                        //else
                                        //{
                                        //    MSD = MessageCount;
                                        //}
                                        //double PerSmsItem = 4;
                                        //double SmsCost = PerSmsItem * MSD;
                                        //CampaignDetailsUpdate.Budget = SmsCost;
                                        //#endregion
                                        if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
                                        {
                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
                                        }
                                        else
                                        {
                                            CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
                                        }
                                        CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            else if (_Request.RecipientSubTypeCode == GroupSourceType.ExternalS && !string.IsNullOrEmpty(_Request.FileName) && _Request.Items != null && _Request.Items.Count > 0)
                            {
                                _Request.Items = _Request.Items.Where(x => x.MobileNumber != null).Distinct().ToList();
                                if (_Request.Items.Count > 0)
                                {
                                    _Request.Items = _Request.Items.Skip(0).Take(CampaignDetails.ItemLimit).ToList();
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _SMSCampaignGroup = new SMSCampaignGroup();
                                        _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
                                        _SMSCampaignGroup.Title = HCoreHelper.GenerateSystemName(CampaignDetails.Title) + "_file";
                                        _SMSCampaignGroup.FileName = _Request.FileName;
                                        _SMSCampaignGroup.SourceId = GroupSourceType.External;
                                        _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
                                        _SMSCampaignGroup.StatusId = 2;
                                        _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
                                        _HCoreContext.SaveChanges();
                                        long GroupId = _SMSCampaignGroup.Id;
                                        int Limit = 1;
                                        if (_Request.Items.Count > 500)
                                        {
                                            Limit = _Request.Items.Count / 500;
                                        }
                                        Limit++;
                                        int Offset = 0;
                                        for (int i = 0; i < Limit; i++)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
                                                var Items = _Request.Items.Skip(Offset).Take(500).ToList();
                                                Offset += 500;
                                                if (Items.Count > 0)
                                                {
                                                    foreach (var mItem in Items)
                                                    {
                                                        _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
                                                        {
                                                            CampaignId = CampaignDetails.Id,
                                                            GroupId = GroupId,
                                                            Name = mItem.Name,
                                                            MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
                                                            CreateDate = HCoreHelper.GetGMTDateTime(),
                                                            StatusId = StatusHelper.SmsStatus.Unsent
                                                        });
                                                    }
                                                }
                                                _HCoreContext.SMSCampaignGroupItem.AddRange(_SMSCampaignGroupItems);
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                                            if (CampaignDetailsUpdate != null)
                                            {
                                                if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
                                                {
                                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
                                                }
                                                else
                                                {
                                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
                                                }
                                                CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                            else if (_Request.RecipientSubTypeCode == GroupSourceType.TUCS && string.IsNullOrEmpty(_Request.Condition))
                            {
                                _FrameworkHelper = new FrameworkHelper();
                                var Customers = _FrameworkHelper.GetCustomer(_Request.Condition, CampaignDetails.AccountId, _Request.TotalItem);
                                if (Customers.Count > 0)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _SMSCampaignGroup = new SMSCampaignGroup();
                                        _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
                                        _SMSCampaignGroup.Title = HCoreHelper.GenerateSystemName(CampaignDetails.Title) + "_tuc";
                                        _SMSCampaignGroup.FileName = _Request.FileName;
                                        _SMSCampaignGroup.SourceId = GroupSourceType.TUC;
                                        _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
                                        _SMSCampaignGroup.StatusId = 2;
                                        _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
                                        _HCoreContext.SaveChanges();
                                        long GroupId = _SMSCampaignGroup.Id;
                                        int Limit = 1;
                                        if (Customers.Count > 500)
                                        {
                                            Limit = Customers.Count / 500;
                                        }
                                        Limit++;
                                        int Offset = 0;
                                        for (int i = 0; i < Limit; i++)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
                                                var Items = Customers.Skip(Offset).Take(500).ToList();
                                                Offset += 500;
                                                if (Items.Count > 0)
                                                {
                                                    foreach (var mItem in Items)
                                                    {
                                                        _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
                                                        {
                                                            CampaignId = CampaignDetails.Id,
                                                            GroupId = GroupId,
                                                            Name = mItem.Name,
                                                            MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
                                                            AccountId = mItem.ReferenceId,
                                                            CreateDate = HCoreHelper.GetGMTDateTime(),
                                                            StatusId = StatusHelper.SmsStatus.Unsent
                                                        });
                                                    }
                                                }
                                                _HCoreContext.SMSCampaignGroupItem.AddRange(_SMSCampaignGroupItems);
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                                            if (CampaignDetailsUpdate != null)
                                            {
                                                if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
                                                {
                                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
                                                }
                                                else
                                                {
                                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
                                                }
                                                CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                    }
                                }


                            }
                            else if (_Request.RecipientSubTypeCode == GroupSourceType.CustomS && _Request.Items != null && _Request.Items.Count > 0)
                            {
                                _Request.Items = _Request.Items.Where(x => x.MobileNumber != null).Distinct().ToList();
                                if (_Request.Items.Count > 0)
                                {
                                    _Request.Items = _Request.Items.Skip(0).Take(CampaignDetails.ItemLimit).ToList();
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _SMSCampaignGroup = new SMSCampaignGroup();
                                        _SMSCampaignGroup.CampaignId = _Request.ReferenceId;
                                        _SMSCampaignGroup.Title = HCoreHelper.GenerateSystemName(CampaignDetails.Title) + "_custom";
                                        //_SMSCampaignGroup.FileName = _Request.FileName;
                                        _SMSCampaignGroup.SourceId = GroupSourceType.Custom;
                                        _SMSCampaignGroup.CreateDate = HCoreHelper.GetGMTDateTime();
                                        _SMSCampaignGroup.CreatedById = _Request.UserReference.AccountId;
                                        _SMSCampaignGroup.StatusId = 2;
                                        _HCoreContext.SMSCampaignGroup.Add(_SMSCampaignGroup);
                                        _HCoreContext.SaveChanges();
                                        long GroupId = _SMSCampaignGroup.Id;
                                        int Limit = _Request.Items.Count / 500;
                                        Limit++;
                                        int Offset = 0;
                                        for (int i = 0; i < Limit; i++)
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                _SMSCampaignGroupItems = new List<SMSCampaignGroupItem>();
                                                var Items = _Request.Items.Skip(Offset).Take(500).ToList();
                                                Offset += 500;
                                                if (Items.Count > 0)
                                                {
                                                    foreach (var mItem in Items)
                                                    {
                                                        _SMSCampaignGroupItems.Add(new SMSCampaignGroupItem
                                                        {
                                                            CampaignId = CampaignDetails.Id,
                                                            GroupId = GroupId,
                                                            Name = mItem.Name,
                                                            MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, mItem.MobileNumber),
                                                            CreateDate = HCoreHelper.GetGMTDateTime(),
                                                            StatusId = StatusHelper.SmsStatus.Unsent
                                                        });
                                                    }
                                                }
                                                _HCoreContext.SMSCampaignGroupItem.AddRange(_SMSCampaignGroupItems);
                                                _HCoreContext.SaveChanges();
                                            }
                                        }
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var CampaignDetailsUpdate = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                                            if (CampaignDetailsUpdate != null)
                                            {
                                                if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
                                                {
                                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.published;
                                                }
                                                else
                                                {
                                                    CampaignDetailsUpdate.StatusId = StatusHelper.SmsCampaign.draft;
                                                }
                                                CampaignDetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                _HCoreContext.SaveChanges();
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException(LogLevel.High, "SaveCampaignItem", _Exception, _Request.UserReference);
            }
            #endregion
        }

        /// <summary>
        /// Description: Saves the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCampaign(OCampaign.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime().AddMinutes(-1);
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                }
                if (_Request.TotalItem < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0347", ResponseCode.CA0347);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                }
                if (_Request.SendDate < CurrentTime)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0313", ResponseCode.CA0313);
                }
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0314", ResponseCode.CA0314);
                }
                if (string.IsNullOrEmpty(_Request.Message))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0315", ResponseCode.CA0315);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0334", ResponseCode.CA0334);
                }
                if (_Request.StatusCode != StatusHelper.SmsCampaign.publishedS && _Request.StatusCode != StatusHelper.SmsCampaign.draftS)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0348", ResponseCode.CA0348);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RecipientSubTypeCode == GroupSourceType.TUCS && string.IsNullOrEmpty(_Request.Condition))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0317", ResponseCode.CA0317);
                    }
                    if (_Request.RecipientSubTypeCode == GroupSourceType.ExternalS && string.IsNullOrEmpty(_Request.FileName))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0318", ResponseCode.CA0318);
                    }
                    if (_Request.RecipientSubTypeCode == GroupSourceType.GroupS && _Request.Groups == null && _Request.Groups.Count == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0318", ResponseCode.CA0318);
                    }
                    var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.Merchant
                        && x.Guid == _Request.AccountKey)
                        .Select(x => new
                        {
                            StatusId = x.StatusId,
                            DisplayName = x.DisplayName,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            bool CampaignTitleCheck = _HCoreContext.SMSCampaign.Any(x => x.Title == _Request.Title && x.AccountId == _Request.AccountId);
                            if (CampaignTitleCheck)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0349", ResponseCode.CA0349);
                            }
                            #region UPDATE SMS COST
                            string DisplayName = AccountDetails.DisplayName;
                            if (AccountDetails.DisplayName.Length > 17)
                            {
                                DisplayName = AccountDetails.DisplayName.Substring(0, 16);
                            }
                            string FinalMessage = _Request.Message.Trim() + " - " + DisplayName;
                            int MessageCount = CalculateSmsLength(FinalMessage);
                            int MSD = 0;
                            if (_Request.TotalItem > 0)
                            {
                                MSD = MessageCount * _Request.TotalItem;
                            }
                            else
                            {
                                MSD = MessageCount;
                            }
                            double PerSmsItem = 4;
                            double SmsCost = PerSmsItem * MSD;
                            #endregion
                            long TransactionId = 0;
                            if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
                            {
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                double Balance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.PromotionCampaign);
                                if (SmsCost > Balance)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0345", ResponseCode.CA0345);
                                }
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = _Request.AccountId;
                                _CoreTransactionRequest.InvoiceAmount = SmsCost;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = SmsCost;
                                _CoreTransactionRequest.ReferenceAmount = SmsCost;
                                _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Bank;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                    SourceId = TransactionSource.PromotionCampaign,
                                    Amount = SmsCost,
                                    Comission = 0,
                                    TotalAmount = SmsCost,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = SystemAccounts.ThankUCashSystemId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                    SourceId = TransactionSource.PromotionCampaign,
                                    Amount = SmsCost,
                                    Comission = 0,
                                    TotalAmount = SmsCost,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    TransactionId = TransactionResponse.ReferenceId;
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0346", ResponseCode.CA0346);
                                    #endregion
                                }
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _SMSCampaign = new SMSCampaign();
                                _SMSCampaign.Guid = HCoreHelper.GenerateGuid();
                                _SMSCampaign.AccountId = _Request.AccountId;
                                _SMSCampaign.Title = _Request.Title;
                                if (_Request.IsSendNow)
                                {
                                    _SMSCampaign.PriorityId = 1;
                                    _SMSCampaign.SendDate = HCoreHelper.GetGMTDateTime().AddMinutes(3);
                                }
                                else
                                {
                                    _SMSCampaign.PriorityId = 0;
                                    _SMSCampaign.SendDate = _Request.SendDate;
                                }
                                //_SMSCampaign.SendDate = _Request.SendDate;
                                _SMSCampaign.SenderId = _Request.SenderId;
                                _SMSCampaign.ItemLimit = _Request.TotalItem;
                                _SMSCampaign.Total = _Request.TotalItem;
                                _SMSCampaign.Message = FinalMessage;
                                _SMSCampaign.PerItemCost = PerSmsItem;
                                _SMSCampaign.Budget = SmsCost;
                                _SMSCampaign.Unsent = _Request.TotalItem;
                                _SMSCampaign.Sent = 0;
                                _SMSCampaign.Delivered = 0;
                                _SMSCampaign.Pending = 0;
                                _SMSCampaign.Failed = 0;
                                if (_Request.RecipientTypeCode == CampaignRecepientType.IndividualS)
                                {
                                    _SMSCampaign.RecipientTypeId = CampaignRecepientType.Individual;
                                }
                                else
                                {
                                    _SMSCampaign.RecipientTypeId = CampaignRecepientType.Group;
                                }

                                if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.GroupS)
                                {
                                    _SMSCampaign.RecipientSubTypeId = GroupSourceType.Group;
                                }
                                else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.TUCS)
                                {
                                    _SMSCampaign.RecipientSubTypeId = GroupSourceType.TUC;
                                }
                                else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.ExternalS)
                                {
                                    _SMSCampaign.RecipientSubTypeId = GroupSourceType.External;
                                }

                                else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.CustomS)
                                {
                                    _SMSCampaign.RecipientSubTypeId = GroupSourceType.Custom;
                                }
                                else
                                {
                                    _SMSCampaign.RecipientSubTypeId = GroupSourceType.Group;
                                }
                                _SMSCampaign.CreateDate = HCoreHelper.GetGMTDateTime();
                                _SMSCampaign.CreatedById = _Request.UserReference.AccountId;
                                _SMSCampaign.StatusId = StatusHelper.SmsCampaign.creating;
                                if (TransactionId > 0)
                                {
                                    _SMSCampaign.TransactionId = TransactionId;
                                }
                                _HCoreContext.SMSCampaign.Add(_SMSCampaign);
                                _HCoreContext.SaveChanges();
                                _Request.ReferenceId = _SMSCampaign.Id;
                                _Request.ReferenceKey = _SMSCampaign.Guid;
                                var system = ActorSystem.Create("ActorCampaignSync");
                                var greeter = system.ActorOf<ActorCampaignSync>("ActorCampaignSync");
                                greeter.Tell(_Request);
                                var _Response = new
                                {
                                    ReferenceId = _Request.ReferenceId,
                                    ReferenceKey = _Request.ReferenceKey,
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA0320", ResponseCode.CA0320);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAAACCNOTACTIVE", ResponseCode.CAAACCNOTACTIVE);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCNOTFOUND", ResponseCode.CAACCNOTFOUND);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "SaveCampaign", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }

        //internal OResponse SaveCampaign(OCampaign.Save.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        DateTime CurrentTime = HCoreHelper.GetGMTDateTime().AddMinutes(-1);
        //        if (_Request.AccountId < 1)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
        //        }
        //        if (_Request.TotalItem < 1)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0347", ResponseCode.CA0347);
        //        }
        //        if (string.IsNullOrEmpty(_Request.AccountKey))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
        //        }
        //        if (_Request.SendDate < CurrentTime)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0313", ResponseCode.CA0313);
        //        }
        //        if (string.IsNullOrEmpty(_Request.Title))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0314", ResponseCode.CA0314);
        //        }
        //        if (string.IsNullOrEmpty(_Request.Message))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0315", ResponseCode.CA0315);
        //        }
        //        if (string.IsNullOrEmpty(_Request.StatusCode))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0334", ResponseCode.CA0334);
        //        }
        //        if (_Request.StatusCode != StatusHelper.SmsCampaign.publishedS && _Request.StatusCode != StatusHelper.SmsCampaign.draftS)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0348", ResponseCode.CA0348);
        //        }

        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            if (_Request.RecipientSubTypeCode == GroupSourceType.TUCS && string.IsNullOrEmpty(_Request.Condition))
        //            {
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0317", ResponseCode.CA0317);
        //            }
        //            if (_Request.RecipientSubTypeCode == GroupSourceType.ExternalS && string.IsNullOrEmpty(_Request.FileName))
        //            {
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0318", ResponseCode.CA0318);
        //            }
        //            if (_Request.RecipientSubTypeCode == GroupSourceType.GroupS && _Request.Groups == null && _Request.Groups.Count == 0)
        //            {
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0318", ResponseCode.CA0318);
        //            }
        //            var AccountDetails = _HCoreContext.HCUAccount
        //                .Where(x => x.Id == _Request.AccountId
        //                && x.AccountTypeId == UserAccountType.Merchant
        //                && x.Guid == _Request.AccountKey)
        //                .Select(x => new
        //                {
        //                    StatusId = x.StatusId,
        //                    DisplayName = x.DisplayName,
        //                })
        //                .FirstOrDefault();
        //            if (AccountDetails != null)
        //            {
        //                if (AccountDetails.StatusId == HelperStatus.Default.Active)
        //                {
        //                    bool CampaignTitleCheck = _HCoreContext.SMSCampaign.Any(x => x.Title == _Request.Title && x.AccountId == _Request.AccountId);
        //                    if (CampaignTitleCheck)
        //                    {
        //                        _HCoreContext.Dispose();
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0349", ResponseCode.CA0349);
        //                    }
        //                    #region UPDATE SMS COST
        //                    string DisplayName = AccountDetails.DisplayName;
        //                    if (AccountDetails.DisplayName.Length > 17)
        //                    {
        //                        DisplayName = AccountDetails.DisplayName.Substring(0, 16);
        //                    }
        //                    string FinalMessage = _Request.Message.Trim() + " - " + DisplayName;
        //                    int MessageCount = CalculateSmsLength(FinalMessage);
        //                    int MSD = 0;
        //                    if (_Request.TotalItem > 0)
        //                    {
        //                        MSD = MessageCount * _Request.TotalItem;
        //                    }
        //                    else
        //                    {
        //                        MSD = MessageCount;
        //                    }
        //                    double PerSmsItem = 4;
        //                    double SmsCost = PerSmsItem * MSD;
        //                    #endregion
        //                    long TransactionId = 0;
        //                    if (_Request.StatusCode == StatusHelper.SmsCampaign.publishedS)
        //                    {
        //                        _ManageCoreTransaction = new ManageCoreTransaction();
        //                        double Balance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.PromotionCampaign);
        //                        if (SmsCost > Balance)
        //                        {
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0345", ResponseCode.CA0345);
        //                        }
        //                        _CoreTransactionRequest = new OCoreTransaction.Request();
        //                        _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
        //                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                        _CoreTransactionRequest.ParentId = _Request.AccountId;
        //                        _CoreTransactionRequest.InvoiceAmount = SmsCost;
        //                        _CoreTransactionRequest.ReferenceInvoiceAmount = SmsCost;
        //                        _CoreTransactionRequest.ReferenceAmount = SmsCost;
        //                        _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Bank;
        //                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                        {
        //                            UserAccountId = _Request.AccountId,
        //                            ModeId = TransactionMode.Debit,
        //                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
        //                            SourceId = TransactionSource.PromotionCampaign,
        //                            Amount = SmsCost,
        //                            Comission = 0,
        //                            TotalAmount = SmsCost,
        //                        });
        //                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                        {
        //                            UserAccountId = SystemAccounts.ThankUCashSystemId,
        //                            ModeId = TransactionMode.Credit,
        //                            TypeId = TransactionType.PromotionCampaign.WalletCredit,
        //                            SourceId = TransactionSource.PromotionCampaign,
        //                            Amount = SmsCost,
        //                            Comission = 0,
        //                            TotalAmount = SmsCost,
        //                        });
        //                        _CoreTransactionRequest.Transactions = _TransactionItems;
        //                        _ManageCoreTransaction = new ManageCoreTransaction();
        //                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                        {
        //                            TransactionId = TransactionResponse.ReferenceId;
        //                        }
        //                        else
        //                        {
        //                            #region Send Response
        //                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0346", ResponseCode.CA0346);
        //                            #endregion
        //                        }
        //                    }
        //                    using (_HCoreContext = new HCoreContext())
        //                    {
        //                        _SMSCampaign = new SMSCampaign();
        //                        _SMSCampaign.Guid = HCoreHelper.GenerateGuid();
        //                        _SMSCampaign.AccountId = _Request.AccountId;
        //                        _SMSCampaign.Title = _Request.Title;
        //                        if (_Request.IsSendNow)
        //                        {
        //                            _SMSCampaign.PriorityId = 1;
        //                            _SMSCampaign.SendDate = HCoreHelper.GetGMTDateTime().AddMinutes(3);
        //                        }
        //                        else
        //                        {
        //                            _SMSCampaign.PriorityId = 0;
        //                            _SMSCampaign.SendDate = _Request.SendDate;
        //                        }
        //                        //_SMSCampaign.SendDate = _Request.SendDate;
        //                        _SMSCampaign.SenderId = _Request.SenderId;
        //                        _SMSCampaign.ItemLimit = _Request.TotalItem;
        //                        _SMSCampaign.Total = _Request.TotalItem;
        //                        _SMSCampaign.Message = FinalMessage;
        //                        _SMSCampaign.PerItemCost = PerSmsItem;
        //                        _SMSCampaign.Budget = SmsCost;
        //                        _SMSCampaign.Unsent = _Request.TotalItem;
        //                        _SMSCampaign.Sent = 0;
        //                        _SMSCampaign.Delivered = 0;
        //                        _SMSCampaign.Pending = 0;
        //                        _SMSCampaign.Failed = 0;
        //                        if (_Request.RecipientTypeCode == CampaignRecepientType.IndividualS)
        //                        {
        //                            _SMSCampaign.RecipientTypeId = CampaignRecepientType.Individual;
        //                        }
        //                        else
        //                        {
        //                            _SMSCampaign.RecipientTypeId = CampaignRecepientType.Group;
        //                        }

        //                        if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.GroupS)
        //                        {
        //                            _SMSCampaign.RecipientSubTypeId = GroupSourceType.Group;
        //                        }
        //                        else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.TUCS)
        //                        {
        //                            _SMSCampaign.RecipientSubTypeId = GroupSourceType.TUC;
        //                        }
        //                        else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.ExternalS)
        //                        {
        //                            _SMSCampaign.RecipientSubTypeId = GroupSourceType.External;
        //                        }

        //                        else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.CustomS)
        //                        {
        //                            _SMSCampaign.RecipientSubTypeId = GroupSourceType.Custom;
        //                        }
        //                        else
        //                        {
        //                            _SMSCampaign.RecipientSubTypeId = GroupSourceType.Group;
        //                        }
        //                        _SMSCampaign.CreateDate = HCoreHelper.GetGMTDateTime();
        //                        _SMSCampaign.CreatedById = _Request.UserReference.AccountId;
        //                        _SMSCampaign.StatusId = StatusHelper.SmsCampaign.creating;
        //                        if (TransactionId > 0)
        //                        {
        //                            _SMSCampaign.TransactionId = TransactionId;
        //                        }
        //                        _HCoreContext.SMSCampaign.Add(_SMSCampaign);
        //                        _HCoreContext.SaveChanges();
        //                        _Request.ReferenceId = _SMSCampaign.Id;
        //                        _Request.ReferenceKey = _SMSCampaign.Guid;
        //                        var system = ActorSystem.Create("ActorCampaignSync");
        //                        var greeter = system.ActorOf<ActorCampaignSync>("ActorCampaignSync");
        //                        greeter.Tell(_Request);
        //                        var _Response = new
        //                        {
        //                            ReferenceId = _Request.ReferenceId,
        //                            ReferenceKey = _Request.ReferenceKey,
        //                        };
        //                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA0320", ResponseCode.CA0320);
        //                    }
        //                }
        //                else
        //                {
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAAACCNOTACTIVE", ResponseCode.CAAACCNOTACTIVE);
        //                }
        //            }
        //            else
        //            {
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCNOTFOUND", ResponseCode.CAACCNOTFOUND);
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException(LogLevel.High, "SaveCampaign", _Exception, _Request.UserReference);
        //        #endregion
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
        //    }
        //    #endregion
        //}

        /// <summary>
        /// Description: Updates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCampaign(OCampaign.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                DateTime CurrentTime = HCoreHelper.GetGMTDateTime().AddMinutes(-1);
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0321", ResponseCode.CA0321);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0322", ResponseCode.CA0322);
                }
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                }
                if (_Request.SendDate < CurrentTime)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0313", ResponseCode.CA0313);
                }
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0314", ResponseCode.CA0314);
                }
                if (string.IsNullOrEmpty(_Request.Message))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0315", ResponseCode.CA0315);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0334", ResponseCode.CA0334);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount
                       .Where(x => x.Id == _Request.AccountId
                       && x.AccountTypeId == UserAccountType.Merchant
                       && x.Guid == _Request.AccountKey)
                       .Select(x => new
                       {
                           StatusId = x.StatusId,
                           DisplayName = x.DisplayName,
                       })
                       .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        var CampaignDetails = _HCoreContext.SMSCampaign
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                        if (CampaignDetails != null)
                        {
                            if (CampaignDetails.StatusId != StatusHelper.SmsCampaign.draft)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0335", ResponseCode.CA0335);
                            }
                            if (!string.IsNullOrEmpty(_Request.Title) && CampaignDetails.Title != _Request.Title)
                            {
                                CampaignDetails.Title = _Request.Title;
                            }
                            //if (!string.IsNullOrEmpty(_Request.Condition) && CampaignDetails.Title != _Request.Condition)
                            //{
                            //    CampaignDetails.Condition = _Request.Condition;
                            //}
                            //if (!string.IsNullOrEmpty(_Request.FileName) && CampaignDetails.FileName != _Request.FileName)
                            //{
                            //    CampaignDetails.FileName = _Request.FileName;
                            //}
                            if (StatusHelper.SmsCampaign.publishedS == _Request.StatusCode && _Request.IsItemUpdated == false)
                            {
                                CampaignDetails.StatusId = StatusHelper.SmsCampaign.published;
                            }
                            if (_Request.IsItemUpdated)
                            {
                                if (!string.IsNullOrEmpty(_Request.Message))
                                {
                                    CampaignDetails.Message = _Request.Message;
                                }
                                CampaignDetails.StatusId = StatusHelper.SmsCampaign.creating;
                                //#region UPDATE SMS COST
                                //string DisplayName = AccountDetails.DisplayName;
                                //if (AccountDetails.DisplayName.Length > 17)
                                //{
                                //    DisplayName = AccountDetails.DisplayName.Substring(0, 16);
                                //}
                                //string FinalMessage = _Request.Message.Trim() + " - " + DisplayName;
                                //int MessageCount = CalculateSmsLength(FinalMessage);
                                //int MSD = MessageCount * _Request.TotalItem;
                                //double PerSmsItem = 4;
                                //double SmsCost = PerSmsItem * MSD;
                                //#endregion
                                #region UPDATE SMS COST
                                string DisplayName = AccountDetails.DisplayName;
                                if (AccountDetails.DisplayName.Length > 17)
                                {
                                    DisplayName = AccountDetails.DisplayName.Substring(0, 16);
                                }
                                string FinalMessage = _Request.Message.Trim() + " - " + DisplayName;
                                int MessageCount = CalculateSmsLength(FinalMessage);
                                int MSD = 0;
                                if (_Request.TotalItem > 0)
                                {
                                    MSD = MessageCount * _Request.TotalItem;
                                }
                                else
                                {
                                    MSD = MessageCount;
                                }
                                double PerSmsItem = 4;
                                double SmsCost = PerSmsItem * MSD;
                                #endregion

                                CampaignDetails.PerItemCost = PerSmsItem;
                                CampaignDetails.Budget = SmsCost;
                                CampaignDetails.SendDate = _Request.SendDate;
                                CampaignDetails.SenderId = _Request.SenderId;
                                CampaignDetails.Message = _Request.Message;
                                if (_Request.RecipientTypeCode == TypeHelper.CampaignRecepientType.IndividualS)
                                {
                                    CampaignDetails.RecipientTypeId = CampaignRecepientType.Individual;
                                }
                                else
                                {
                                    CampaignDetails.RecipientTypeId = CampaignRecepientType.Group;
                                }

                                if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.TUCS)
                                {
                                    CampaignDetails.RecipientSubTypeId = GroupSourceType.TUC;
                                }
                                else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.ExternalS)
                                {
                                    CampaignDetails.RecipientSubTypeId = GroupSourceType.External;
                                }
                                else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.GroupS)
                                {
                                    CampaignDetails.RecipientSubTypeId = GroupSourceType.Group;
                                }
                                else if (_Request.RecipientSubTypeCode == TypeHelper.GroupSourceType.CustomS)
                                {
                                    CampaignDetails.RecipientSubTypeId = GroupSourceType.Custom;
                                }
                                else
                                {
                                    CampaignDetails.RecipientSubTypeId = GroupSourceType.Group;
                                }
                                CampaignDetails.ModifyById = _Request.UserReference.AccountId;
                                CampaignDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                                var system = ActorSystem.Create("ActorCampaignEdit");
                                var greeter = system.ActorOf<ActorCampaignEdit>("ActorCampaignEdit");
                                greeter.Tell(_Request);
                            }
                            else
                            {
                                CampaignDetails.ModifyById = _Request.UserReference.AccountId;
                                CampaignDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                _HCoreContext.SaveChanges();
                            }
                            var _Response = new
                            {
                                ReferenceId = _Request.ReferenceId,
                                ReferenceKey = _Request.ReferenceKey,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA0312", ResponseCode.CA0312);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCNOTFOUND", ResponseCode.CAACCNOTFOUND);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "UpdateCampaign", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the list of all campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaign(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.SMSCampaign
                                                    .Where(x => x.AccountId == _Request.AccountId 
                                                        && x.Account.Guid == _Request.AccountKey
                                                        && x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OCampaign.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.Title,
                                                        Message = x.Message,
                                                        SendDate = x.SendDate, 
                                                        TotalItem = x.Total,
                                                        Budget = x.Budget,

                                                        Unsent = x.Unsent,
                                                        Sent = x.Sent,
                                                        Pending = x.Pending,
                                                        Failed = x.Failed,
                                                        Delivered = x.Delivered,

                                                        SenderId = x.Sender.SenderId,
                                                        ItemCount = x.SMSCampaignGroupItem.Count,


                                                        GroupCount = x.SMSCampaignGroup.Count,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OCampaign.List> _List = _HCoreContext.SMSCampaign
                                                    .Where(x => x.AccountId == _Request.AccountId 
                                                    && x.Account.Guid == _Request.AccountKey
                                                    && x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OCampaign.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.Title,
                                                        Message = x.Message,
                                                        SendDate = x.SendDate,
                                                        ItemCount = x.SMSCampaignGroupItem.Count,
                                                        Budget = x.Budget,
                                                        Unsent = x.Unsent,
                                                        TotalItem = x.Total,
                                                        Sent = x.Sent,
                                                        Pending = x.Pending,
                                                        Failed = x.Failed,
                                                        Delivered = x.Delivered,
                                                        SenderId = x.Sender.SenderId,


                                                        GroupCount = x.SMSCampaignGroup.Count,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (DataItem.GroupCount > 0)
                            {
                                DataItem.Groups = _HCoreContext.SMSCampaignGroup.Where(x => x.CampaignId == DataItem.ReferenceId)
                              .Select(x => new OCampaign.GroupList
                              {
                                  GroupName = x.Title,
                              }).Skip(0).Take(2).ToList();
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.SMSCampaign
                                                     .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OCampaign.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.Title,
                                                        Message = x.Message,
                                                        SendDate = x.SendDate,
                                                        ItemCount = x.SMSCampaignGroupItem.Count,
                                                        Budget = x.Budget,
                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        //ItemCount = x.SMSCampaignGroupItem.Count,
                                                        TotalItem = x.Total,
                                                        SenderId = x.Sender.SenderId,


                                                        Unsent = x.Unsent,
                                                        Sent = x.Sent,
                                                        Pending = x.Pending,
                                                        Failed = x.Failed,
                                                        Delivered = x.Delivered,


                                                        GroupCount = x.SMSCampaignGroup.Count,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OCampaign.List> _List = _HCoreContext.SMSCampaign
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Select(x => new OCampaign.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountIconUrl = x.Account.IconStorage.Path,

                                                        Title = x.Title,
                                                        Message = x.Message,
                                                        SendDate = x.SendDate,
                                                        ItemCount = x.SMSCampaignGroupItem.Count,
                                                        Budget = x.Budget,
                                                        Unsent = x.Unsent,
                                                        Sent = x.Sent,
                                                        Pending = x.Pending,
                                                        Failed = x.Failed,
                                                        Delivered = x.Delivered,
                                                        TotalItem = x.Total,

                                                        SenderId = x.Sender.SenderId,


                                                        GroupCount = x.SMSCampaignGroup.Count,
                                                        CreateDate = x.CreateDate,
                                                        ModifyDate = x.ModifyDate,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (DataItem.GroupCount > 0)
                            {
                                DataItem.Groups = _HCoreContext.SMSCampaignGroup.Where(x => x.CampaignId == DataItem.ReferenceId)
                              .Select(x => new OCampaign.GroupList
                              {
                                  GroupName = x.Title,
                              }).Skip(0).Take(2).ToList();
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
            }    
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCampaign", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the list of all campaign item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaignItem(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.SMSCampaignGroupItem
                                                .Where(x => x.CampaignId == _Request.ReferenceId)
                                                .Select(x => new OCampaignGroupItem.List
                                                {
                                                    ReferenceId = x.Id,
                                                    GroupName = x.Group.FileName,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    IconUrl = x.Account.IconStorage.Path,
                                                    SendDate = x.SendDate,
                                                    DeliveryDate = x.DeliveryDate,
                                                    StatusName = x.Status.Name,
                                                    StatusCode = x.Status.SystemName,
                                                    CreateDate = x.CreateDate,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCampaignGroupItem.List> _List = _HCoreContext.SMSCampaignGroupItem
                                                .Where(x => x.CampaignId == _Request.ReferenceId)
                                                .Select(x => new OCampaignGroupItem.List
                                                {
                                                    ReferenceId = x.Id,
                                                    GroupName = x.Group.FileName,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    IconUrl = x.Account.IconStorage.Path,
                                                    SendDate = x.SendDate,
                                                    DeliveryDate = x.DeliveryDate,
                                                    StatusName = x.Status.Name,
                                                    StatusCode = x.Status.SystemName,
                                                    CreateDate = x.CreateDate,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCampaignItem", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the campaign details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCampaign(OCampaign.Reference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0321", ResponseCode.CA0321);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0322", ResponseCode.CA0322);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.SMSCampaign
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey
                        && x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        ).Select(x => new OCampaign.Details.Info
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AccountId = x.AccountId,
                            AccountKey = x.Account.Guid,
                            AccountDisplayName = x.Account.DisplayName,
                            AccountIconUrl = x.Account.IconStorage.Path,

                            Title = x.Title,
                            Message = x.Message,
                            SendDate = x.SendDate,

                            SenderId = x.SenderId,
                            SenderIdName = x.Sender.SenderId,

                            RecipientTypeCode = x.RecipientType.SystemName,
                            RecipientTypeName = x.RecipientType.Name,


                            RecipientSubTypeCode = x.RecipientSubType.SystemName,
                            RecipientSubTypeName = x.RecipientSubType.Name,


                            ItemCount = x.Total,
                            TotalItem = x.Total,


                            Budget = x.Budget,
                            ItemCost = x.PerItemCost,

                            UnSent = x.Unsent,
                            Sent = x.Sent,
                            Failed = x.Failed,
                            Pending = x.Pending,
                            Delivered = x.Delivered,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,

                            ReviewerId = x.ReviewerId,
                            ReviewerKey = x.Reviewer.Guid,
                            ReviewerDisplayName = x.Reviewer.DisplayName,
                            ReviewerComment = x.ReviewerComment,

                        })
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        Details.Conditions = _HCoreContext.SMSCampaignGroupCondition
                            .Where(x => x.CampaignGroup.CampaignId == Details.ReferenceId)
                            .Select(x => new OCampaign.Details.Condition
                            {
                                Name = x.Name,
                                Value = x.Value,
                            }).ToList();
                        Details.Groups = _HCoreContext.SMSCampaignGroup
                            .Where(x => x.CampaignId == Details.ReferenceId)
                            .Select(x => new OCampaign.Details.Group
                            {
                                Title = x.Title,
                            }).ToList();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "CA0200", ResponseCode.CA0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DeleteGroup", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCampaign(OCampaign.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0321", ResponseCode.CA0321);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0322", ResponseCode.CA0322);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.SMSCampaign
                        .Where(x => x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        && x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey
                        ).Select(x => new
                        {
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == StatusHelper.SmsCampaign.draft)
                        {
                            var SmsGroupItems = _HCoreContext.SMSCampaignGroupItem.Where(x => x.CampaignId == _Request.ReferenceId).ToList();
                            _HCoreContext.SMSCampaignGroupItem.RemoveRange(SmsGroupItems);

                            var SmsConditions = _HCoreContext.SMSCampaignGroupCondition.Where(x => x.CampaignGroup.CampaignId == _Request.ReferenceId).ToList();
                            _HCoreContext.SMSCampaignGroupCondition.RemoveRange(SmsConditions);

                            var SmsCampaignGroup = _HCoreContext.SMSCampaignGroup.Where(x => x.CampaignId == _Request.ReferenceId).ToList();
                            _HCoreContext.SMSCampaignGroup.RemoveRange(SmsCampaignGroup);

                            _HCoreContext.SaveChanges();
                            using (_HCoreContext = new HCoreContext())
                            {
                                var CampaignDetails = _HCoreContext.SMSCampaign
                                                   .Where(x => x.AccountId == _Request.AccountId
                                                    && x.Account.Guid == _Request.AccountKey
                                                    && x.Id == _Request.ReferenceId
                                                    && x.Guid == _Request.ReferenceKey)
                                                    .FirstOrDefault();
                                _HCoreContext.SMSCampaign.Remove(CampaignDetails);
                                _HCoreContext.SaveChanges();
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0323", ResponseCode.CA0323);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0324", ResponseCode.CA0324);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DeleteGroup", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the campaign status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCampaignStatus(OCampaign.Reference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0321", ResponseCode.CA0321);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0322", ResponseCode.CA0322);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0327", ResponseCode.CA0327);
                }
                long? StatusCodeId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, 709,  _Request.UserReference);
                if (StatusCodeId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0331", ResponseCode.CA0331);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.SMSCampaign
                        .Where(x => x.AccountId == _Request.AccountId
                        && x.Account.Guid == _Request.AccountKey
                        && x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.StatusId == StatusHelper.SmsCampaign.creating)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0329", ResponseCode.CA0330);
                        }
                        else if (Details.StatusId == StatusHelper.SmsCampaign.deleteting)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0329", ResponseCode.CA0329);
                        }
                        else if (Details.StatusId == StatusHelper.SmsCampaign.expired)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0328", ResponseCode.CA0328);
                        }
                        else if (Details.StatusId == StatusHelper.SmsCampaign.cancelled)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0353", ResponseCode.CA0353);
                        }
                        else if (Details.StatusId == StatusHelper.SmsCampaign.completed)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0354", ResponseCode.CA0354);
                        }
                        else if (Details.StatusId == StatusHelper.SmsCampaign.running)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0352", ResponseCode.CA0352);
                        }
                        else if (Details.StatusId == StatusHelper.SmsCampaign.inreview)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0355", ResponseCode.CA0355);
                        }
                        else if (Details.StatusId == StatusHelper.SmsCampaign.rejected)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0356", ResponseCode.CA0356);
                        }
                        //else if (Details.StatusId == StatusHelper.SmsCampaign.waitingforreview)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0358", ResponseCode.CA0358);
                        //}
                        else
                        {
                            if (Details.StatusId == StatusHelper.SmsCampaign.draft)
                            {
                                if (StatusCodeId == StatusHelper.SmsCampaign.waitingforreview || StatusCodeId == StatusHelper.SmsCampaign.published)
                                {
                                    if (Details.Budget == null || Details.Budget == 0)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0350", ResponseCode.CA0350);
                                    }
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    double Balance = _ManageCoreTransaction.GetAccountBalance(_Request.AccountId, TransactionSource.PromotionCampaign);
                                    double CampaignCost = (double)Details.Budget;
                                    if (CampaignCost > Balance)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0345", ResponseCode.CA0345);
                                    }
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    _CoreTransactionRequest.ParentId = _Request.AccountId;
                                    _CoreTransactionRequest.InvoiceAmount = CampaignCost;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = CampaignCost;
                                    _CoreTransactionRequest.ReferenceAmount = CampaignCost;
                                    _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Bank;
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = _Request.AccountId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                        SourceId = TransactionSource.PromotionCampaign,
                                        Amount = CampaignCost,
                                        Comission = 0,
                                        TotalAmount = CampaignCost,
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = SystemAccounts.ThankUCashSystemId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.PromotionCampaign.WalletCredit,
                                        SourceId = TransactionSource.PromotionCampaign,
                                        Amount = CampaignCost,
                                        Comission = 0,
                                        TotalAmount = CampaignCost,
                                    });
                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var DetailsUpdate = _HCoreContext.SMSCampaign
                                                .Where(x => x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey
                                                && x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .FirstOrDefault();
                                            DetailsUpdate.TransactionId = TransactionResponse.ReferenceId;
                                            DetailsUpdate.StatusId = StatusHelper.SmsCampaign.cancelled;
                                            DetailsUpdate.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            DetailsUpdate.ModifyById = _Request.UserReference.AccountId;
                                            _HCoreContext.SaveChanges();
                                        }
                                        if (StatusCodeId == StatusHelper.SmsCampaign.published)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0358", ResponseCode.CA0358);
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0360", ResponseCode.CA0360);
                                        }
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0346", ResponseCode.CA0346);
                                        #endregion
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0359", ResponseCode.CA0359);
                                }
                            }
                            else if (Details.StatusId == StatusHelper.SmsCampaign.approved)
                            {
                                if (StatusCodeId == StatusHelper.SmsCampaign.published)
                                {
                                    Details.StatusId = StatusHelper.SmsCampaign.published;
                                    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Details.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0361", ResponseCode.CA0361);
                                }
                                else if (StatusCodeId == StatusHelper.SmsCampaign.paused)
                                {
                                    Details.StatusId = StatusHelper.SmsCampaign.paused;
                                    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Details.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0362", ResponseCode.CA0362);
                                }
                                else if (StatusCodeId == StatusHelper.SmsCampaign.cancelled)
                                {
                                    Details.StatusId = StatusHelper.SmsCampaign.cancelled;
                                    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Details.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0363", ResponseCode.CA0363);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0359", ResponseCode.CA0359);
                                }
                            }
                            else if (Details.StatusId == StatusHelper.SmsCampaign.published)
                            {
                                if (StatusCodeId == StatusHelper.SmsCampaign.paused)
                                {
                                    Details.StatusId = StatusHelper.SmsCampaign.paused;
                                    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Details.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0362", ResponseCode.CA0362);
                                }
                                else if (StatusCodeId == StatusHelper.SmsCampaign.cancelled)
                                {
                                    Details.StatusId = StatusHelper.SmsCampaign.cancelled;
                                    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Details.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0363", ResponseCode.CA0363);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0359", ResponseCode.CA0359);
                                }
                            }
                            else if (Details.StatusId == StatusHelper.SmsCampaign.paused)
                            {
                                if (StatusCodeId == StatusHelper.SmsCampaign.published)
                                {
                                    Details.StatusId = StatusHelper.SmsCampaign.published;
                                    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Details.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0361", ResponseCode.CA0361);
                                }
                                else if (StatusCodeId == StatusHelper.SmsCampaign.cancelled)
                                {
                                    Details.StatusId = StatusHelper.SmsCampaign.cancelled;
                                    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    Details.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0363", ResponseCode.CA0363);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0359", ResponseCode.CA0359);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0354", ResponseCode.CA0354);
                            }
                            //if (Details.StatusId == StatusHelper.SmsCampaign.draft && StatusCodeId == StatusHelper.SmsCampaign.published)
                            //{
                            //}
                            //else
                            //{
                            //    if (StatusCodeId == StatusHelper.SmsCampaign.published
                            //  &&
                            //  (Details.StatusId == StatusHelper.SmsCampaign.published
                            //  || Details.StatusId == StatusHelper.SmsCampaign.running
                            //  ))
                            //    {
                            //        Details.StatusId = StatusHelper.SmsCampaign.paused;
                            //        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //        Details.ModifyById = _Request.UserReference.AccountId;
                            //        _HCoreContext.SaveChanges();
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0333", ResponseCode.CA0333);
                            //    }
                            //    else if (StatusCodeId == StatusHelper.SmsCampaign.cancelled
                            //        &&
                            //        (Details.StatusId == StatusHelper.SmsCampaign.approved
                            //        || Details.StatusId == StatusHelper.SmsCampaign.rejected
                            //        || Details.StatusId == StatusHelper.SmsCampaign.published
                            //        || Details.StatusId == StatusHelper.SmsCampaign.paused
                            //        || Details.StatusId == StatusHelper.SmsCampaign.waitingforreview
                            //        || Details.StatusId == StatusHelper.SmsCampaign.approved
                            //        ))
                            //    {
                            //        Details.StatusId = StatusHelper.SmsCampaign.cancelled;
                            //        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //        Details.ModifyById = _Request.UserReference.AccountId;
                            //        _HCoreContext.SaveChanges();
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0333", ResponseCode.CA0333);
                            //    }
                            //    else if (StatusCodeId == StatusHelper.SmsCampaign.paused
                            //        &&
                            //        (Details.StatusId == StatusHelper.SmsCampaign.published
                            //        || Details.StatusId == StatusHelper.SmsCampaign.running
                            //        ))
                            //    {
                            //        Details.StatusId = StatusHelper.SmsCampaign.paused;
                            //        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //        Details.ModifyById = _Request.UserReference.AccountId;
                            //        _HCoreContext.SaveChanges();
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0333", ResponseCode.CA0333);
                            //    }
                            //    else if (StatusCodeId == StatusHelper.SmsCampaign.running
                            //        &&
                            //        (Details.StatusId == StatusHelper.SmsCampaign.approved
                            //        || Details.StatusId == StatusHelper.SmsCampaign.rejected
                            //        || Details.StatusId == StatusHelper.SmsCampaign.published
                            //        || Details.StatusId == StatusHelper.SmsCampaign.paused
                            //        || Details.StatusId == StatusHelper.SmsCampaign.waitingforreview
                            //        || Details.StatusId == StatusHelper.SmsCampaign.approved
                            //        ))
                            //    {
                            //        Details.StatusId = StatusHelper.SmsCampaign.cancelled;
                            //        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //        Details.ModifyById = _Request.UserReference.AccountId;
                            //        _HCoreContext.SaveChanges();
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0333", ResponseCode.CA0333);
                            //    }
                            //    else
                            //    {
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0332", ResponseCode.CA0332);
                            //    }
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0328", ResponseCode.CA0328);

                            //}

                        }
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "CA0326", ResponseCode.CA0326);
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0328", ResponseCode.CA0328);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DeleteGroup", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Duplicates the campaign.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DuplicateCampaign(OCampaign.Duplicate.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREF", ResponseCode.CAACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCREFKEY", ResponseCode.CAACCREFKEY);
                }
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0321", ResponseCode.CA0321);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0322", ResponseCode.CA0322);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.AccountId
                        && x.AccountTypeId == UserAccountType.Merchant
                        && x.Guid == _Request.AccountKey)
                        .Select(x => new
                        {
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            var Campaign = _HCoreContext.SMSCampaign.Where(x => x.Id == _Request.ReferenceId
                               && x.Guid == _Request.ReferenceKey
                               && x.Account.Guid == _Request.AccountKey
                               && x.AccountId == _Request.AccountId).FirstOrDefault();
                            if (Campaign == null)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0404", ResponseCode.CA0404);
                            }
                            _SMSCampaign = new SMSCampaign();
                            _SMSCampaign.Guid = HCoreHelper.GenerateGuid();
                            _SMSCampaign.AccountId = Campaign.AccountId;
                            _SMSCampaign.Title = Campaign.Title;
                            _SMSCampaign.SendDate = Campaign.SendDate;
                            _SMSCampaign.SenderId = Campaign.SenderId;
                            _SMSCampaign.Message = Campaign.Message;
                            _SMSCampaign.RecipientTypeId = Campaign.RecipientTypeId;
                            _SMSCampaign.RecipientSubTypeId = Campaign.RecipientSubTypeId;
                            _SMSCampaign.Budget = Campaign.Budget;
                            _SMSCampaign.PerItemCost = Campaign.PerItemCost;
                            _SMSCampaign.ItemLimit = Campaign.ItemLimit;
                            _SMSCampaign.Total = Campaign.Total;
                            _SMSCampaign.PriorityId = Campaign.PriorityId;
                            _SMSCampaign.Unsent = Campaign.ItemLimit;
                            _SMSCampaign.Sent = 0;
                            _SMSCampaign.Delivered = 0;
                            _SMSCampaign.Pending = 0;
                            _SMSCampaign.Failed = 0;
                            _SMSCampaign.CreateDate = HCoreHelper.GetGMTDateTime();
                            _SMSCampaign.CreatedById = _Request.UserReference.AccountId;
                            _SMSCampaign.StatusId = StatusHelper.SmsCampaign.creating;
                            _HCoreContext.SMSCampaign.Add(_SMSCampaign);
                            _HCoreContext.SaveChanges();
                            _Request.NewCampaignId = _SMSCampaign.Id;
                            var _Response = new
                            {
                                ReferenceId = _SMSCampaign.Id,
                                ReferenceKey = _SMSCampaign.Guid,
                            };
                            var system = ActorSystem.Create("ActorDuplicateCampaign");
                            var greeter = system.ActorOf<ActorDuplicateCampaign>("ActorDuplicateCampaign");
                            greeter.Tell(_Request);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "CA0325", ResponseCode.CA0325);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAAACCNOTACTIVE", ResponseCode.CAAACCNOTACTIVE);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAACCNOTFOUND", ResponseCode.CAACCNOTFOUND);
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "DuplicateCampaign", _Exception, _Request.UserReference);
                #endregion
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
    }
}
