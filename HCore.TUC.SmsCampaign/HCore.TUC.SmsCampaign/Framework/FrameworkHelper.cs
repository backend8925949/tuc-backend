//==================================================================================
// FileName: FrameworkHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to sms campaign helper
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.SmsCampaign.Actor;
using HCore.TUC.SmsCampaign.Helper;
using HCore.TUC.SmsCampaign.Object;
using HCore.TUC.SmsCampaign.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper;
using static HCore.TUC.SmsCampaign.Helper.SmsCampaignHelper.TypeHelper;
namespace HCore.TUC.SmsCampaign.Framework
{
    public class FrameworkHelper
    {
        HCoreContext _HCoreContext;
        List<OAccounts.Customer.List> _Customers;
        /// <summary>
        /// Description: Gets the sender identifier.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSenderId(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.SMSSenderProvider
                                                .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OCampaignHelper.SenderId
                                                {
                                                    ReferenceId = x.Id,
                                                    Name = x.SenderId,
                                                    ProviderName = x.ProviderName,
                                                })
                                                .Distinct()
                                               .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCampaignHelper.SenderId> _List = _HCoreContext.SMSSenderProvider
                                                .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OCampaignHelper.SenderId
                                                {
                                                    ReferenceId = x.Id,
                                                    Name = x.SenderId,
                                                    ProviderName = x.ProviderName,
                                                })
                                                .Distinct()
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSenderId", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer list.
        /// </summary>
        /// <param name="SearchCondition">The search condition.</param>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="RecordsLimit">The records limit.</param>
        /// <returns>List&lt;OAccounts.Customer.List&gt;.</returns>
        internal List<OAccounts.Customer.List> GetCustomer(string SearchCondition, long AccountId, int RecordsLimit)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(SearchCondition))
                {
                    SearchCondition = HCoreHelper.GetSearchCondition("ReferenceId", "0", "<>");
                }
                if (RecordsLimit == 0)
                {
                    RecordsLimit = 10000000;
                }
                _Customers = new List<OAccounts.Customer.List>();
                using (_HCoreContext = new HCoreContext())
                {
                    #region Data
                    _Customers = _HCoreContext.HCUAccount
                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                           && x.StatusId == HelperStatus.Default.Active
                                           && x.MobileNumber != null
                                           && (x.OwnerId == AccountId || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.ParentId == AccountId && m.StatusId == HelperStatus.Transaction.Success)))
                                               .Select(x => new OAccounts.Customer.List
                                               {
                                                   ReferenceId = x.Id,
                                                   DisplayName = x.DisplayName,
                                                   Name = x.Name,
                                                   MobileNumber = x.MobileNumber,
                                                   GenderId = x.GenderId,
                                                   CreateDate = x.CreateDate,
                                                   DateOfBirth = x.DateOfBirth,
                                                   TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.ParentId == AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && (((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit) || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus)))
                                                                                            .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                                   TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.ParentId == AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                            && (((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit) || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus)))
                                                                                            .Sum(m => (double?)m.ReferenceAmount) ?? 0,

                                                   TotalRedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.ParentId == AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                            && m.ModeId == Helpers.TransactionMode.Debit
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .Sum(m => (double?)m.TotalAmount) ?? 0,
                                                   LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                            .Where(m => m.AccountId == x.Id
                                                                                            && m.ParentId == AccountId
                                                                                            && m.StatusId == HelperStatus.Transaction.Success
                                                                                            && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                            .OrderByDescending(m => m.TransactionDate)
                                                                                            .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                               })
                                            .Where(SearchCondition)
                                            .Skip(0)
                                            .Take(RecordsLimit)
                                            .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                }
                return _Customers;
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetCustomer", _Exception, null);
                return null;
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer count.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerCount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    int TotalRecords = _HCoreContext.HCUAccount
                                      .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                     && x.StatusId == HelperStatus.Default.Active
                                           && x.MobileNumber != null
                                     && (x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId && m.StatusId == HelperStatus.Transaction.Success)))
                                         .Select(x => new OAccounts.Customer.List
                                         {
                                             ReferenceId = x.Id,
                                             DisplayName = x.DisplayName,
                                             Name = x.Name,
                                             MobileNumber = x.MobileNumber,
                                             GenderId = x.GenderId,
                                             CreateDate = x.CreateDate,
                                             DateOfBirth = x.DateOfBirth,
                                             TotalInvoiceAmount = _HCoreContext.HCUAccountTransaction
                                                                                      .Where(m => m.AccountId == x.Id
                                                                                      && m.ParentId == _Request.AccountId
                                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                                      && (((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit) || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus)))
                                                                                      .Sum(m => (double?)m.PurchaseAmount) ?? 0,

                                             TotalRewardAmount = _HCoreContext.HCUAccountTransaction
                                                                                      .Where(m => m.AccountId == x.Id
                                                                                      && m.ParentId == _Request.AccountId
                                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Reward
                                                                                      && (((m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack) && m.TypeId != TransactionType.ThankUCashPlusCredit) || (m.ModeId == Helpers.TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus)))
                                                                                      .Sum(m => (double?)m.ReferenceAmount) ?? 0,


                                             TotalRedeemAmount = _HCoreContext.HCUAccountTransaction
                                                                                      .Where(m => m.AccountId == x.Id
                                                                                      && m.ParentId == _Request.AccountId
                                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                                      && m.Type.SubParentId == TransactionTypeCategory.Redeem
                                                                                      && m.ModeId == Helpers.TransactionMode.Debit
                                                                                      && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                      .Sum(m => (double?)m.TotalAmount) ?? 0,
                                             LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                                                      .Where(m => m.AccountId == x.Id
                                                                                      && m.ParentId == _Request.AccountId
                                                                                      && m.StatusId == HelperStatus.Transaction.Success
                                                                                      && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                                      .OrderByDescending(m => m.TransactionDate)
                                                                                      .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault(),
                                         })
                                      .Where(_Request.SearchCondition)
                                      .Count();
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    object[] array = { };
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, array, 0, TotalRecords);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCustomerCount", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                           && x.MobileNumber != null
                                               && x.StatusId == HelperStatus.Default.Active
                                               && (x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId)))
                                               .Select(x => new OAccounts.Customer.List
                                               {
                                                   ReferenceId = x.Id,
                                                   DisplayName = x.DisplayName,
                                                   Name = x.Name,
                                                   MobileNumber = x.MobileNumber,
                                               })
                                               .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OAccounts.Customer.List> _List = _HCoreContext.HCUAccount
                                            .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                           && x.MobileNumber != null
                                           && x.StatusId == HelperStatus.Default.Active
                                           && (x.OwnerId == _Request.AccountId || x.HCUAccountTransactionAccount.Any(m => m.AccountId == x.Id && m.ParentId == _Request.AccountId)))
                                               .Select(x => new OAccounts.Customer.List
                                               {
                                                   ReferenceId = x.Id,
                                                   DisplayName = x.DisplayName,
                                                   Name = x.Name,
                                                   MobileNumber = x.MobileNumber,
                                               })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCustomer", _Exception, _Request.UserReference, _OResponse, "CA0500", ResponseCode.CA0500);
            }
            #endregion
        }
    }
}
