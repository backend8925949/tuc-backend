//==================================================================================
// FileName: ManageAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Connect.Framework;
using HCore.TUC.Connect.Object;
namespace HCore.TUC.Connect
{
    public class ManageAccount
    {
        FrameworkAccount _FrameworkAccount;
        /// <summary>
        /// Connects the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ConnectAccount(OAccount.Profile.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.ConnectAccount(_Request);
        }
        /// <summary>
        /// Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountBalance(OAccount.Balance.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetAccountBalance(_Request);
        }
        /// <summary>
        /// Updates the account pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAccountPin(OAccount.UpdatePin.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.UpdateAccountPin(_Request);
        }

        /// <summary>
        /// Resets the account pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetAccountPin(OAccount.ResetPin.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.ResetAccountPin(_Request);
        }
        /// <summary>
        /// Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse TopupAccount(OAccount.Toupup.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.TopupAccount(_Request);
        }
        /// <summary>
        /// Transfers the point.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse TransferPoint(OAccount.Transfer.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.TransferPoint(_Request);
        }
        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransaction(OAccount.Transaction.List.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetTransaction(_Request);
        }
        /// <summary>
        /// Gets the merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchants(OAccount.Merchant.List.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetMerchants(_Request);
        }
        /// <summary>
        /// Gets the deals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeals(OAccount.Deal.List.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetDeals(_Request);
        }
        /// <summary>
        /// Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(OAccount.Deal.Details.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetDeal(_Request);
        }
        /// <summary>
        /// Buys the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyDeal(OAccount.Deal.Buy.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.BuyDeal_Confirm(_Request);
        }
        /// <summary>
        /// Gets the deal codes.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealCodes(OAccount.Deal.Buy.List.Request _Request)
        {
            _FrameworkAccount = new FrameworkAccount();
            return _FrameworkAccount.GetDealCodes(_Request);
        }
    }
}
