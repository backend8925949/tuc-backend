//==================================================================================
// FileName: OAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using static HCore.TUC.Connect.Object.OAccount.Deal.Details;

namespace HCore.TUC.Connect.Object
{
    public class OAccount
    {
        public class Profile
        {
            public class Request
            {
                public string? AccountId { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? Gender { get; set; }
                public DateTime? DateOfBirth { get; set; }
                public string? ReferenceNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? AccountId { get; set; }
                public string? Pin { get; set; }
                public string? AccountType { get; set; }
                public string? Status { get; set; }
            }
        }
        public class Transaction
        {
            public class List
            {
                public class Request
                {
                    public string? AccountId { get; set; }
                    public int Offset { get; set; }
                    public int Limit { get; set; }
                    public OUserReference? UserReference { get; set; }
                }

                public class Response
                {
                    public string? ReferenceId { get; set; }
                    public string? MerchantId { get; set; }

                    public string? DisplayName { get; set; }
                    public string? IconUrl { get; set; }
                    internal double? TInvoiceAmount { get; set; }
                    public long InvoiceAmount { get; set; }
                    internal double? TAmount { get; set; }
                    public long Amount { get; set; }
                    public string? Type { get; set; }
                    public DateTime? Date { get; set; }
                    public string? StatusName { get; set; }
                }
            }
        }
        public class Balance
        {
            public class Request
            {
                public string? AccountId { get; set; }
                public string? ReferenceNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long Balance { get; set; }
                public long BalanceValidity { get; set; }
                public long TotalEarning { get; set; }
                public long TotalSpend { get; set; }
            }
        }
        public class UpdatePin
        {
            public class Request
            {
                public string? AccountId { get; set; }
                public string? ReferenceNumber { get; set; }
                public string? OldPin { get; set; }
                public string? NewPin { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? NewPin { get; set; }
            }
        }
        public class ResetPin
        {
            public class Request
            {
                public string? AccountId { get; set; }
                public string? ReferenceNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? NewPin { get; set; }
            }
        }


        public class Toupup
        {
            public class Request
            {
                public string? AccountId { get; set; }
                public string? MobileNumber { get; set; }
                public long Amount { get; set; }
                public string? ReferenceNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long Amount { get; set; }
                public long Charge { get; set; }
                public long TotalAmount { get; set; }
                public DateTime Date { get; set; }
                public string? Reference { get; set; }
            }
        }


        public class Transfer
        {
            public class Request
            {
                public string? AccountId { get; set; }
                public string? ToAccountId { get; set; }

                public long Amount { get; set; }
                public string? ReferenceNumber { get; set; }
                public string? Comment { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long Amount { get; set; }
                public long Charge { get; set; }
                public long TotalAmount { get; set; }
                public DateTime Date { get; set; }
                public string? Reference { get; set; }
            }
        }


        public class Merchant
        {
            public class List
            {
                public class Request
                {
                    public string? AccountId { get; set; }
                    public int Offset { get; set; }
                    public int Limit { get; set; }

                    public double Latitude { get; set; }
                    public double Longitude { get; set; }
                    public double Coverage { get; set; }
                    public OUserReference? UserReference { get; set; }
                }

                public class Response
                {
                    public string? MerchantId { get; set; }
                    public string? DisplayName { get; set; }
                    public string? IconUrl { get; set; }
                    internal long Deals { get; set; }
                }
            }
        }


        public class Deal
        {

            public class Buy
            {
                public class Request
                {
                    public string? DealId { get; set; }
                    public string? AccountId { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public string? DealRefId { get; set; }
                    public string? DealCode { get; set; }
                    public string? DealRedeemPin { get; set; }
                    public string? DealId { get; set; }
                    public string? Title { get; set; }
                    public string? Description { get; set; }
                    public string? ImageUrl { get; set; }
                    public string? MerchantId { get; set; }
                    public string? MerchantDisplayName { get; set; }
                    public string? MerchantIconUrl { get; set; }
                    public DateTime? StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    public string? Status { get; set; }
                    public string? Terms { get; set; }

                    internal double? TActualPrice { get; set; }
                    internal double? TSellingPrice { get; set; }
                    internal double? TAmount { get; set; }

                    public long ActualPrice { get; set; }
                    public long SellingPrice { get; set; }
                    public long Amount { get; set; }
                    public List<ImageUrlItem> Images { get; set; }
                }

                public class List
                {

                    public class Request
                    {
                        public string? AccountId { get; set; }
                        public int Offset { get; set; }
                        public int Limit { get; set; }

                        public double Latitude { get; set; }
                        public double Longitude { get; set; }
                        public double Coverage { get; set; }
                        public OUserReference? UserReference { get; set; }
                    }

                    public class Response
                    {
                        public string? DealRefId { get; set; }
                        public string? DealCode { get; set; }
                        public string? DealRedeemPin { get; set; }
                        public string? DealId { get; set; }
                        internal string Title { get; set; }
                        public string? Description { get; set; }
                        public string? ImageUrl { get; set; }
                        public string? MerchantId { get; set; }
                        public string? MerchantDisplayName { get; set; }
                        public string? MerchantIconUrl { get; set; }
                        public DateTime? StartDate { get; set; }
                        public DateTime? EndDate { get; set; }
                        public string? Status { get; set; }
                        public long ActualPrice { get; set; }
                        public long SellingPrice { get; set; }
                        public long Amount { get; set; }
                        internal double? TAmount { get; set; }
                    }
                }
            }

            public class Details
            {
                public class Request
                {
                    public string? AccountId { get; set; }
                    public string? DealId { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public string? DealId { get; set; }
                    public string? Title { get; set; }
                    public string? Description { get; set; }
                    internal long MerchantReferenceId { get; set; }
                    internal string MerchantId { get; set; }
                    public string? MerchantDisplayName { get; set; }
                    public string? MerchantIconUrl { get; set; }
                    public string? ImageUrl { get; set; }
                    public string? CategoryId { get; set; }
                    public string? CategoryName { get; set; }
                    public DateTime? StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    internal double? TActualPrice { get; set; }
                    internal double? TSellingPrice { get; set; }
                    internal double? TAmount { get; set; }

                    public long ActualPrice { get; set; }
                    public long SellingPrice { get; set; }
                    public long Amount { get; set; }
                    public long TotalPurchase { get; set; }
                    public string? Terms { get; set; }
                    public List<ImageUrlItem> Images { get; set; }
                    public List<Location> Locations { get; set; }
                }
                public class ImageUrlItem
                {
                    public string? ImageUrl { get; set; }
                }
                public class Location
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? DisplayName { get; set; }
                    public string? Address { get; set; }
                    public double Latitude { get; set; }
                    public double Longitude { get; set; }
                }

            }

            public class List
            {

                public class Request
                {
                    public string? AccountId { get; set; }
                    public int Offset { get; set; }
                    public int Limit { get; set; }

                    public double Latitude { get; set; }
                    public double Longitude { get; set; }
                    public double Coverage { get; set; }
                    public OUserReference? UserReference { get; set; }
                }

                public class Response
                {
                    public string? DealId { get; set; }
                    public string? Title { get; set; }
                    public string? ImageUrl { get; set; }
                    public string? Description { get; set; }
                    internal string MerchantId { get; set; }
                    public string? MerchantDisplayName { get; set; }
                    public string? MerchantIconUrl { get; set; }
                    public string? CategoryId { get; set; }
                    public string? CategoryName { get; set; }
                    public DateTime? StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    internal double? TActualPrice { get; set; }
                    internal double? TSellingPrice { get; set; }
                    internal double? TAmount { get; set; }
                    public string? Address { get; set; }
                    public long ActualPrice { get; set; }
                    public long SellingPrice { get; set; }
                    public long Amount { get; set; }
                }
            }
        }

    }
}
