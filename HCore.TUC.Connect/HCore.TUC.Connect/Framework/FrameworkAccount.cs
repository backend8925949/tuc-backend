//==================================================================================
// FileName: FrameworkAccount.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to accounts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Connect.Object;
using HCore.TUC.Connect.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Connect.Framework
{
    internal class FrameworkAccount
    {
        HCoreContext _HCoreContext;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        HCUAccountBalance _HCUAccountBalance;
        OAccount.Profile.Response _AccountResponse;
        Random _Random;
        OAccount.Balance.Response _BalanceResponse;
        OAccount.UpdatePin.Response _UpdatePinResponse;
        OAccount.ResetPin.Response _ResetPinResponse;
        ManageCoreTransaction _ManageCoreTransaction;
        OAccount.Toupup.Response _TopupResponse;
        OCoreTransaction.Request _CoreTransactionRequest;
        OAccount.Transfer.Response _TransferResponse;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        List<OAccount.Transaction.List.Response> _Transactions;
        OAccount.Deal.Details.Response _DealDetails;
        List<OAccount.Deal.List.Response> _Deals;
        List<OAccount.Deal.Buy.List.Response> _DealPurchaseHistory;
        List<OAccount.Deal.Details.ImageUrlItem> _DealDetailImages;
        MDDealCode _MDDealCode;  //ManageCoreTransaction _ManageCoreTransaction;
        OAccount.Deal.Buy.Response _DealPurchaseDetails;
        List<OAccount.Merchant.List.Response> _Merchants;
        /// <summary>
        /// Connects the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ConnectAccount(OAccount.Profile.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (!string.IsNullOrEmpty(_Request.AccountId))
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Guid == _Request.AccountId).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.EmailAddress) && AccountDetails.EmailAddress != _Request.EmailAddress)
                            {
                                AccountDetails.EmailAddress = _Request.EmailAddress;
                                AccountDetails.SecondaryEmailAddress = _Request.EmailAddress;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                string DisplayName = _Request.Name;
                                if (DisplayName.Length > 30)
                                {
                                    DisplayName = _Request.Name.Substring(0, 29);
                                }
                                AccountDetails.DisplayName = DisplayName;
                                AccountDetails.Name = _Request.Name;
                            }

                            if (_Request.DateOfBirth != null)
                            {
                                AccountDetails.DateOfBirth = _Request.DateOfBirth;
                            }

                            if (!string.IsNullOrEmpty(_Request.Gender))
                            {
                                if (_Request.Gender == "male")
                                {
                                    AccountDetails.GenderId = Gender.Male;
                                }
                                if (_Request.Gender == "female")
                                {
                                    AccountDetails.GenderId = Gender.Female;
                                }
                                if (_Request.Gender == "other")
                                {
                                    AccountDetails.GenderId = Gender.Other;
                                }
                            }
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();
                            _AccountResponse = new OAccount.Profile.Response();
                            _AccountResponse.Status = "Active";
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _AccountResponse, "HC2000", ResponseCode.HC2000);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.Name))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4001", ResponseCode.HC4001);
                    }
                    else if (string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4002", ResponseCode.HC4002);
                    }
                    else if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length < 10)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4005", ResponseCode.HC4005);
                    }
                    else if (!string.IsNullOrEmpty(_Request.MobileNumber) && _Request.MobileNumber.Length > 13)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4006", ResponseCode.HC4006);
                    }
                    else if (!string.IsNullOrEmpty(_Request.MobileNumber) && !System.Text.RegularExpressions.Regex.IsMatch(_Request.MobileNumber, "^[0-9]*$"))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4007", ResponseCode.HC4007);
                    }
                    else if (string.IsNullOrEmpty(_Request.EmailAddress))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4003", ResponseCode.HC4003);
                    }
                    else if (string.IsNullOrEmpty(_Request.Gender))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4008", ResponseCode.HC4008);
                    }
                    else if (_Request.Gender != "male" && _Request.Gender != "female" && _Request.Gender != "other")
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4009", ResponseCode.HC4009);
                    }
                    else if (string.IsNullOrEmpty(_Request.ReferenceNumber))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4010", ResponseCode.HC4010);
                    }
                    else
                    {
                        #region Manage Operations
                        using (_HCoreContext = new HCoreContext())
                        {
                            var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.UserReference.CountryIsd).Select(x => new
                            {
                                Id = x.Id,
                                Isd = x.Isd,
                                Key = x.Guid,
                                MSISDNLength = x.MobileNumberLength
                            }).FirstOrDefault();
                            #region  Process Registration
                            _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, CountryDetails.MSISDNLength);
                            string CustomerAppId = HCoreConstant._AppConfig.AppUserPrefix + _Request.MobileNumber;

                            var CustomerDetails = _HCoreContext.HCUAccount
                                .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.User.Username == CustomerAppId)
                                .Select(x => new
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    AccessPin = x.AccessPin,
                                    StatusName = x.Status.Name,
                                }).FirstOrDefault();
                            if (CustomerDetails != null)
                            {
                                _AccountResponse = new OAccount.Profile.Response();
                                _AccountResponse.AccountId = CustomerDetails.ReferenceKey;
                                _AccountResponse.AccountType = "Existing";
                                _AccountResponse.Pin = HCoreEncrypt.DecryptHash(CustomerDetails.AccessPin);
                                _AccountResponse.Status = CustomerDetails.StatusName;
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _AccountResponse, "HC2000", ResponseCode.HC2000);

                            }
                            else
                            {
                                DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                                int GenderId = 0;
                                if (!string.IsNullOrEmpty(_Request.Gender))
                                {
                                    if (_Request.Gender == "male")
                                    {
                                        GenderId = Gender.Male;
                                    }
                                    if (_Request.Gender == "female")
                                    {
                                        GenderId = Gender.Female;
                                    }
                                    if (_Request.Gender == "other")
                                    {
                                        GenderId = Gender.Other;
                                    }
                                }
                                string UserKey = HCoreHelper.GenerateGuid();
                                string UserAccountKey = HCoreHelper.GenerateGuid();
                                _Random = new Random();
                                string AccessPin = _Random.Next(1111, 9999).ToString();
                                #region Save User
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = UserKey;
                                _HCUAccountAuth.Username = CustomerAppId;
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = CreateDate;
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                #endregion
                                #region Online Account
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = UserAccountKey;
                                _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                                _HCUAccount.OwnerId = _Request.UserReference.AccountId;
                                _HCUAccount.MobileNumber = _Request.MobileNumber;
                                string DisplayName = _Request.Name;
                                if (DisplayName.Length > 30)
                                {
                                    DisplayName = _Request.Name.Substring(0, 29);
                                }
                                _HCUAccount.DisplayName = DisplayName;
                                _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                                if (!string.IsNullOrEmpty(_Request.Name))
                                {
                                    _HCUAccount.Name = _Request.Name;
                                }
                                if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                {
                                    _HCUAccount.MobileNumber = _Request.MobileNumber;
                                    _HCUAccount.ContactNumber = _Request.MobileNumber;
                                }
                                if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                {
                                    _HCUAccount.EmailAddress = _Request.EmailAddress;
                                    _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                                }
                                if (GenderId != 0)
                                {
                                    _HCUAccount.GenderId = GenderId;
                                }
                                if (_Request.DateOfBirth != null)
                                {
                                    _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                                }
                                _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                                _HCUAccount.EmailVerificationStatus = 0;
                                _HCUAccount.EmailVerificationStatusDate = CreateDate;
                                _HCUAccount.NumberVerificationStatus = 0;
                                _HCUAccount.NumberVerificationStatusDate = CreateDate;
                                _Random = new Random();
                                string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                                _HCUAccount.AccountCode = AccountCode;
                                _HCUAccount.ReferralCode = _Request.MobileNumber;
                                _HCUAccount.RegistrationSourceId = RegistrationSource.PartnerImport;
                                if (_Request.UserReference.AppVersionId != 0)
                                {
                                    _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                                }
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                                _HCUAccount.CreateDate = CreateDate;
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.User = _HCUAccountAuth;
                                _HCoreContext.HCUAccount.Add(_HCUAccount);
                                _HCoreContext.SaveChanges();
                                #endregion
                                _AccountResponse = new OAccount.Profile.Response();
                                _AccountResponse.AccountId = UserAccountKey;
                                _AccountResponse.AccountType = "New";
                                _AccountResponse.Pin = AccessPin;
                                _AccountResponse.Status = "Active";
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _AccountResponse, "HC2000", ResponseCode.HC2000);
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ConnectAccount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountBalance(OAccount.Balance.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4011", ResponseCode.HC4011);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var Details = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.Guid == _Request.AccountId).Select(x => x.Id).FirstOrDefault();
                        if (Details > 0)
                        {
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            var BalanceDetails = _ManageCoreTransaction.GetCustomerBalance(Details);
                            _BalanceResponse = new OAccount.Balance.Response();
                            _BalanceResponse.Balance = Convert.ToInt64(BalanceDetails.Balance * 100);
                            _BalanceResponse.BalanceValidity = BalanceDetails.BalanceValidity;
                            _BalanceResponse.TotalEarning = Convert.ToInt64(BalanceDetails.Credit * 100);
                            _BalanceResponse.TotalSpend = Convert.ToInt64(BalanceDetails.Debit * 100);
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _BalanceResponse, "HC2000", ResponseCode.HC2000);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ConnectAccount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Updates the account pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAccountPin(OAccount.UpdatePin.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4011", ResponseCode.HC4011);
                }
                else if (string.IsNullOrEmpty(_Request.OldPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4012", ResponseCode.HC4012);
                }
                else if (_Request.OldPin.Length != 4)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4015", ResponseCode.HC4015);
                }
                else if (string.IsNullOrEmpty(_Request.NewPin))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4013", ResponseCode.HC4013);
                }
                else if (_Request.NewPin.Length != 4)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4016", ResponseCode.HC4016);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser &&
                        x.Guid == _Request.AccountId)
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            string ExistingPin = HCoreEncrypt.DecryptHash(AccountDetails.AccessPin);
                            if (ExistingPin != _Request.OldPin)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4014", ResponseCode.HC4014);
                            }
                            AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.NewPin);
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();
                            _UpdatePinResponse = new OAccount.UpdatePin.Response();
                            _UpdatePinResponse.NewPin = _Request.NewPin;
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _UpdatePinResponse, "HC2000", ResponseCode.HC2000);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                        }
                    }

                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateAccountPin", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Resets the account pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetAccountPin(OAccount.ResetPin.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4011", ResponseCode.HC4011);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser &&
                        x.Guid == _Request.AccountId)
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            string NewPin = HCoreHelper.GenerateRandomNumber(4);
                            AccountDetails.AccessPin = HCoreEncrypt.EncryptHash(NewPin);
                            AccountDetails.ModifyById = _Request.UserReference.AccountId;
                            AccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();
                            _ResetPinResponse = new OAccount.ResetPin.Response();
                            _ResetPinResponse.NewPin = NewPin;
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _ResetPinResponse, "HC2000", ResponseCode.HC2000);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                        }
                    }

                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ResetAccountPin", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Topups the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse TopupAccount(OAccount.Toupup.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4011", ResponseCode.HC4011);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AccountDetails = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser &&
                        x.Guid == _Request.AccountId)
                            .FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateDateString() + "O" + AccountDetails.Id;
                            _CoreTransactionRequest.CustomerId = AccountDetails.Id;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.ParentId = _Request.UserReference.AccountId;
                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount / 100;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount / 100;
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount / 100;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = AccountDetails.Id,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.TUCWalletTopup,
                                SourceId = TransactionSource.TUCVas,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = AccountDetails.Id,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.TUCWalletTopup,
                                SourceId = TransactionSource.TUC,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                _TopupResponse = new OAccount.Toupup.Response();
                                _TopupResponse.Amount = _Request.Amount;
                                _TopupResponse.Charge = 0;
                                _TopupResponse.TotalAmount = _Request.Amount;
                                _TopupResponse.Date = HCoreHelper.GetGMTDateTime();
                                _TopupResponse.Reference = TransactionResponse.GroupKey;
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _TopupResponse, "HC2000", ResponseCode.HC2001);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4017", ResponseCode.HC4017);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                        }
                    }

                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TopupAccount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Transfers the point.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse TransferPoint(OAccount.Transfer.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4011", ResponseCode.HC4011);
                }
                if (_Request.Amount / 100 <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4027", ResponseCode.HC4027);
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var FromAccountDetails = _HCoreContext.HCUAccount.Where(x =>
                        x.AccountTypeId == UserAccountType.Appuser &&
                        x.Guid == _Request.AccountId)
                            .FirstOrDefault();
                        if (FromAccountDetails != null)
                        {
                            if (FromAccountDetails.StatusId != HelperStatus.Default.Active)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4026", ResponseCode.HC4026);
                            }
                            var ToAccountDetails = _HCoreContext.HCUAccount.Where(x =>
                            x.AccountTypeId == UserAccountType.Appuser &&
                            x.Guid == _Request.ToAccountId)
                            .FirstOrDefault();
                            if (ToAccountDetails != null)
                            {
                                if (ToAccountDetails.StatusId != HelperStatus.Default.Active)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4025", ResponseCode.HC4025);
                                }
                                double TransferAmount = _Request.Amount / 100;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                var FromAccountBalance = _ManageCoreTransaction.GetAppUserBalance(FromAccountDetails.Id, true);
                                if (TransferAmount > FromAccountBalance)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4023", ResponseCode.HC4023);
                                }
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateDateString() + "O" + FromAccountDetails.Id;
                                _CoreTransactionRequest.CustomerId = FromAccountDetails.Id;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HCoreConstant.HelperStatus.Transaction.Success;
                                //_CoreTransactionRequest.ParentId = _Request.UserReference.AccountId;
                                _CoreTransactionRequest.InvoiceAmount = TransferAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = TransferAmount;
                                _CoreTransactionRequest.ReferenceAmount = TransferAmount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = FromAccountDetails.Id,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.Loyalty.TUCRedeem.PointTransferRedeem,
                                    SourceId = TransactionSource.TUC,
                                    Amount = _CoreTransactionRequest.InvoiceAmount,
                                    TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = ToAccountDetails.Id,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.PointTransferReward,
                                    SourceId = TransactionSource.TUC,
                                    Amount = _CoreTransactionRequest.InvoiceAmount,
                                    TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    _TopupResponse = new OAccount.Toupup.Response();
                                    _TopupResponse.Amount = _Request.Amount;
                                    _TopupResponse.Charge = 0;
                                    _TopupResponse.TotalAmount = _Request.Amount;
                                    _TopupResponse.Date = HCoreHelper.GetGMTDateTime();
                                    _TopupResponse.Reference = TransactionResponse.GroupKey;
                                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _TopupResponse, "HC2000", ResponseCode.HC2001);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4017", ResponseCode.HC4017);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                            }

                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                        }
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("TransferPoint", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransaction(OAccount.Transaction.List.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Transactions = new List<OAccount.Transaction.List.Response>();
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    int TotalRecords = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                            && x.Account.Guid == _Request.AccountId
                                            && x.SourceId == TransactionSource.TUC
                                            && (x.Type.SubParentId == TransactionTypeCategory.Reward || x.Type.SubParentId == TransactionTypeCategory.Redeem))
                                            .Select(x => new OAccount.Transaction.List.Response
                                            {
                                                ReferenceId = x.Guid,
                                                MerchantId = x.Parent.Guid,
                                                DisplayName = x.Parent.DisplayName,
                                                IconUrl = x.Parent.IconStorage.Path,
                                                TInvoiceAmount = x.PurchaseAmount,
                                                TAmount = x.Amount,
                                                Type = x.Type.Name,
                                                Date = x.TransactionDate,
                                                StatusName = x.Status.Name,
                                            })
                                   .Count();
                    #endregion
                    #region Get Data
                    _Transactions = _HCoreContext.HCUAccountTransaction
                                            .Where(x => x.Account.AccountTypeId == UserAccountType.Appuser
                                            && x.Account.Guid == _Request.AccountId
                                            && x.SourceId == TransactionSource.TUC
                                            && (x.Type.SubParentId == TransactionTypeCategory.Reward || x.Type.SubParentId == TransactionTypeCategory.Redeem))
                                            .Select(x => new OAccount.Transaction.List.Response
                                            {
                                                ReferenceId = x.Guid,
                                                MerchantId = x.Parent.Guid,
                                                DisplayName = x.Parent.DisplayName,
                                                IconUrl = x.Parent.IconStorage.Path,
                                                TInvoiceAmount = x.PurchaseAmount,
                                                TAmount = x.Amount,
                                                Type = x.Type.Name,
                                                Date = x.TransactionDate,
                                                StatusName = x.Status.Name,
                                            })
                                            .OrderByDescending(a => a.Date)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Transactions)
                    {
                        DataItem.InvoiceAmount = Convert.ToInt64(DataItem.TInvoiceAmount * 100);
                        DataItem.Amount = Convert.ToInt64(DataItem.TAmount * 100);

                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        //else
                        //{
                        //    DataItem.IconUrl = _AppConfig.Default_Icon;
                        //}
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _Transactions, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC2000", ResponseCode.HC2000);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetTransaction", _Exception, _Request.UserReference, _OResponse, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Gets the merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchants(OAccount.Merchant.List.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Merchants = new List<OAccount.Merchant.List.Response>();
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = _HCoreContext.HCUAccount
                                            .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                            && x.IconStorageId != null
                                            && x.MDDealAccount.Any(a => a.StatusId == HelperStatus.Deals.Published)
                                            && x.StatusId == HelperStatus.Default.Active)
                                            .Select(x => new OAccount.Merchant.List.Response
                                            {
                                                MerchantId = x.Guid,
                                                DisplayName = x.DisplayName,
                                                IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                                                Deals = x.MDDealAccount.Count(a => a.StatusId == HelperStatus.Deals.Published)

                                            })
                                   .Count();
                    #endregion
                    #region Get Data
                    _Merchants = _HCoreContext.HCUAccount
                                            .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                            && x.IconStorageId != null
                                            && x.MDDealAccount.Any(a => a.StatusId == HelperStatus.Deals.Published)
                                            && x.StatusId == HelperStatus.Default.Active)
                                            .Select(x => new OAccount.Merchant.List.Response
                                            {
                                                MerchantId = x.Guid,
                                                DisplayName = x.DisplayName,
                                                IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                                                Deals = x.MDDealAccount.Count(a => a.StatusId == HelperStatus.Deals.Published)

                                            })
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    //foreach (var DataItem in _Transactions)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _Merchants, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC2000", ResponseCode.HC2000);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, _OResponse, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Gets the deals.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeals(OAccount.Deal.List.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Deals = new List<OAccount.Deal.List.Response>();
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = _HCoreContext.MDDeal
                                            .Where(x =>
                                             x.StatusId == HelperStatus.Deals.Published)
                                            .OrderByDescending(x => x.StartDate)
                                            .Select(x => new OAccount.Deal.List.Response
                                            {
                                                DealId = x.Guid,
                                                Title = x.Title,
                                                ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                Description = x.Description,
                                                MerchantId = x.Guid,
                                                MerchantDisplayName = x.Account.DisplayName,
                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                CategoryId = x.Category.Guid,
                                                CategoryName = x.Category.Name,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                TActualPrice = x.ActualPrice,
                                                TSellingPrice = x.SellingPrice,
                                                TAmount = x.Amount,
                                            })
                                   .Count();
                    #endregion
                    #region Get Data
                    _Deals = _HCoreContext.MDDeal
                                            .Where(x =>
                                             x.StatusId == HelperStatus.Deals.Published)
                                            .OrderByDescending(x => x.StartDate)
                                            .Select(x => new OAccount.Deal.List.Response
                                            {
                                                DealId = x.Guid,
                                                Title = x.Title,
                                                ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                Description = x.Description,
                                                MerchantId = x.Guid,
                                                MerchantDisplayName = x.Account.DisplayName,
                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                CategoryId = x.Category.Guid,
                                                CategoryName = x.Category.Name,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                TActualPrice = x.ActualPrice,
                                                TSellingPrice = x.SellingPrice,
                                                TAmount = x.Amount,
                                            })
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _Deals)
                    {
                        if (DataItem.TActualPrice != null)
                        {
                            DataItem.ActualPrice = Convert.ToInt64(DataItem.TActualPrice * 100);
                        }
                        else
                        {
                            DataItem.ActualPrice = 0;
                        }
                        if (DataItem.TSellingPrice != null)
                        {
                            DataItem.SellingPrice = Convert.ToInt64(DataItem.TSellingPrice * 100);
                        }
                        else
                        {
                            DataItem.ActualPrice = 0;
                        }
                        DataItem.Amount = Convert.ToInt64(DataItem.TAmount * 100);

                        //if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        //{
                        //    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        //}
                        //else
                        //{
                        //    DataItem.IconUrl = _AppConfig.Default_Icon;
                        //}
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _Deals, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC2000", ResponseCode.HC2000);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeals", _Exception, _Request.UserReference, _OResponse, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OAccount.Deal.Details.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealDetails = new OAccount.Deal.Details.Response();
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    _DealDetails = _HCoreContext.MDDeal
                                            .Where(x => x.Guid == _Request.DealId)
                                            .Select(x => new OAccount.Deal.Details.Response
                                            {
                                                DealId = x.Guid,
                                                Title = x.Title,
                                                ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                                Description = x.Description,
                                                MerchantReferenceId = x.AccountId,
                                                MerchantId = x.Guid,
                                                MerchantDisplayName = x.Account.DisplayName,
                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                CategoryId = x.Category.Guid,
                                                CategoryName = x.Category.Name,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                TActualPrice = x.ActualPrice,
                                                TSellingPrice = x.SellingPrice,
                                                TAmount = x.Amount,
                                                Terms = x.Terms,
                                                TotalPurchase = x.MDDealCode.Count,
                                            }).FirstOrDefault();
                    _DealDetails.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _DealDetails.MerchantReferenceId
                   && x.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => new OAccount.Deal.Details.Location
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            Address = x.Address,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude
                        })
                        .ToList();


                    #endregion
                    if (!string.IsNullOrEmpty(_DealDetails.ImageUrl))
                    {
                        _DealDetailImages = new List<OAccount.Deal.Details.ImageUrlItem>();
                        _DealDetailImages.Add(new OAccount.Deal.Details.ImageUrlItem
                        {
                            ImageUrl = _DealDetails.ImageUrl
                        });
                        _DealDetails.Images = _DealDetailImages;
                    }

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    if (_DealDetails.TActualPrice != null)
                    {
                        _DealDetails.ActualPrice = Convert.ToInt64(_DealDetails.TActualPrice * 100);
                    }
                    else
                    {
                        _DealDetails.ActualPrice = 0;
                    }
                    if (_DealDetails.TSellingPrice != null)
                    {
                        _DealDetails.SellingPrice = Convert.ToInt64(_DealDetails.TSellingPrice * 100);
                    }
                    else
                    {
                        _DealDetails.ActualPrice = 0;
                    }
                    _DealDetails.Amount = Convert.ToInt64(_DealDetails.TAmount * 100);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealDetails, "HC2000", ResponseCode.HC2000);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Buys the deal confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyDeal_Confirm(OAccount.Deal.Buy.Request _Request)
        {
            #region Manage Exception
            try
            {
                //if (_Request.ReferenceId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCDACCREF, TUCDealResource.HCDACCREFM);
                //}
                if (string.IsNullOrEmpty(_Request.DealId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4020", ResponseCode.HC4020);
                }
                if (string.IsNullOrEmpty(_Request.AccountId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4021", ResponseCode.HC4021);
                }
                //if (_Request.DealId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCDACCREF, TUCDealResource.HCDACCREFM);
                //}
                //if (string.IsNullOrEmpty(_Request.DealKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCDACCREFKEY, TUCDealResource.HCDACCREFKEYM);
                //}
                //if (string.IsNullOrEmpty(_Request.PaymentSource))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0111, TUCDealResource.HCP0111M);
                //}
                //if (_Request.PaymentSource != "wallet" && _Request.PaymentSource != "online")
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealResource.HCP0112, TUCDealResource.HCP0112M);
                //}
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.AccountId).FirstOrDefault();
                    var _DealDetails = _HCoreContext.MDDeal
                                                 .Where(x =>
                                                  x.Guid == _Request.DealId)
                                                 .Select(x => new
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,


                                                     AccountId = x.AccountId,

                                                     EndDate = x.EndDate,

                                                     Title = x.Title,
                                                     Description = x.Description,
                                                     MerchantDisplayName = x.Account.DisplayName,

                                                     ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,

                                                     UsageTypeId = x.UsageTypeId,
                                                     CodeValidityDays = x.CodeValidtyDays,
                                                     CodeValidityStartDate = x.CodeValidityStartDate,
                                                     CodeValidityEndDate = x.CodeValidityEndDate,
                                                     Terms = x.Terms,
                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     MaximumUnitSale = x.MaximumUnitSale,
                                                     MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,



                                                     MerchantId = x.Guid,
                                                     MerchantIconUrl = _AppConfig.StorageUrl + x.Account.IconStorage.Path,
                                                     CategoryId = x.Category.Guid,
                                                     CategoryName = x.Category.Name,
                                                     StartDate = x.StartDate,
                                                     TActualPrice = x.ActualPrice,
                                                     TSellingPrice = x.SellingPrice,
                                                     TAmount = x.TotalAmount,

                                                     StatusName = x.Status.Name,

                                                 }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.AccountId)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                DisplayName = x.DisplayName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                                AccountStatusId = x.StatusId,
                            }).FirstOrDefault();
                        double AccountBalance = _ManageCoreTransaction.GetAccountBalance(CustomerDetails.ReferenceId, TransactionSource.TUC);
                        if (AccountBalance < _DealDetails.Amount)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4018", ResponseCode.HC4018);
                        }
                        if (CustomerDetails.AccountStatusId != HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4019", ResponseCode.HC4019);
                        }
                        string Reference = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CustomerId = AccountDetails.Id;
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = Reference;
                        _CoreTransactionRequest.ParentId = _DealDetails.AccountId;
                        _CoreTransactionRequest.InvoiceAmount = (double)_DealDetails.Amount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                        //_CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
                        _CoreTransactionRequest.ReferenceNumber = _DealDetails.ReferenceKey;
                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                        _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = AccountDetails.Id,
                            SourceId = TransactionSource.TUC,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.Deal.DealPurchaseRedeem,
                            Amount = _CoreTransactionRequest.InvoiceAmount,
                            Charge = 0,
                            TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _DealDetails.AccountId,
                            SourceId = TransactionSource.Deals,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.Deal.DealPurchase,
                            Amount = _CoreTransactionRequest.InvoiceAmount,
                            Charge = 0,
                            TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            _MDDealCode = new MDDealCode();
                            _MDDealCode.Guid = Reference;
                            _MDDealCode.DealId = _DealDetails.ReferenceId;
                            _MDDealCode.AccountId = AccountDetails.Id;
                            _MDDealCode.ItemCode = "12" + HCoreHelper.GenerateRandomNumber(9);
                            _MDDealCode.ItemPin = HCoreHelper.GenerateRandomNumber(4);
                            _MDDealCode.ItemAmount = _DealDetails.Amount;
                            _MDDealCode.AvailableAmount = _DealDetails.Amount;
                            if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase)
                            {
                                _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
                                _MDDealCode.EndDate = _MDDealCode.StartDate.Value.AddHours((double)_DealDetails.CodeValidityDays);
                            }
                            else
                            {
                                _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
                                _MDDealCode.EndDate = _DealDetails.CodeValidityEndDate;
                            }
                            _MDDealCode.UseCount = 0;
                            _MDDealCode.UseAttempts = 0;
                            _MDDealCode.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDDealCode.CreatedById = _Request.UserReference.AccountId;
                            _MDDealCode.StatusId = HelperStatus.DealCodes.Unused;
                            _HCoreContext.MDDealCode.Add(_MDDealCode);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();

                            //var _Response = new
                            //{
                            //    ReferenceId = _MDDealCode.Id,
                            //    ReferenceKey = _MDDealCode.Guid,
                            //};

                            _DealPurchaseDetails = new OAccount.Deal.Buy.Response();
                            _DealPurchaseDetails.DealRefId = _MDDealCode.Guid;
                            _DealPurchaseDetails.DealCode = _MDDealCode.ItemCode;
                            _DealPurchaseDetails.DealRedeemPin = _MDDealCode.ItemPin;
                            _DealPurchaseDetails.DealId = _DealDetails.ReferenceKey;
                            _DealPurchaseDetails.Title = _DealDetails.Title;
                            _DealPurchaseDetails.Description = _DealDetails.Description;
                            _DealPurchaseDetails.ImageUrl = _DealDetails.ImageUrl;
                            _DealPurchaseDetails.MerchantId = _DealDetails.MerchantId;
                            _DealPurchaseDetails.MerchantDisplayName = _DealDetails.MerchantDisplayName;
                            _DealPurchaseDetails.MerchantIconUrl = _DealDetails.MerchantIconUrl;

                            _DealPurchaseDetails.StartDate = _DealDetails.StartDate;
                            _DealPurchaseDetails.EndDate = _DealDetails.EndDate;
                            _DealPurchaseDetails.Status = _DealDetails.StatusName;
                            _DealPurchaseDetails.Terms = _DealDetails.Terms;
                            if (_DealDetails.TActualPrice != null)
                            {
                                _DealPurchaseDetails.ActualPrice = Convert.ToInt64(_DealDetails.TActualPrice * 100);
                            }
                            else
                            {
                                _DealPurchaseDetails.ActualPrice = 0;
                            }
                            if (_DealDetails.TSellingPrice != null)
                            {
                                _DealPurchaseDetails.SellingPrice = Convert.ToInt64(_DealDetails.TSellingPrice * 100);
                            }
                            else
                            {
                                _DealPurchaseDetails.ActualPrice = 0;
                            }
                            _DealPurchaseDetails.Amount = Convert.ToInt64(_DealDetails.TAmount * 100);

                            //using (_HCoreContext = new HCoreContext())
                            //{

                            //string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                            //if (!string.IsNullOrEmpty(UserNotificationUrl))
                            //{
                            //    HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Purchase Successful", _DealDetails.Title + " deal puchase successful for " + _DealDetails.MerchantDisplayName, "dealpurchasedetails", _Response.ReferenceId, _Response.ReferenceKey, "View details", true, null);
                            //}
                            //if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                            //{
                            //    var _BDetails = new
                            //    {
                            //        DisplayName = CustomerDetails.DisplayName,
                            //        DealCode = _MDDealCode.ItemCode,
                            //        MerchantDisplayName = _DealDetails.MerchantDisplayName,
                            //        StartDate = _MDDealCode.StartDate.Value.ToString("dd-MM-yyyy HH:mm"),
                            //        EndDate = _MDDealCode.EndDate.Value.AddHours(1).ToString("dd-MM-yyyy HH:mm"),
                            //        Title = _DealDetails.Title,
                            //        Description = _DealDetails.Description,
                            //        Terms = _DealDetails.Terms,
                            //        ImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl,
                            //        Amount = _DealDetails.Amount,
                            //    };
                            //    HCoreHelper.BroadCastEmail("d-cfb071d1a1174d6d9b08ce8fdee6b6dd", "ThankUCash", CustomerDetails.EmailAddress, _BDetails, _Request.UserReference);
                            //}
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealPurchaseDetails, "HC2002", ResponseCode.HC2002);
                            //}
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4022", ResponseCode.HC4022);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC4004", ResponseCode.HC4004);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
        /// <summary>
        /// Gets the deal codes.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealCodes(OAccount.Deal.Buy.List.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = _HCoreContext.MDDealCode
                                            .Where(x =>
                                             x.Account.Guid == _Request.AccountId)
                                            .Select(x => new OAccount.Deal.Buy.List.Response
                                            {
                                                DealRefId = x.Guid,
                                                DealId = x.Guid,
                                                DealCode = x.ItemCode,
                                                DealRedeemPin = x.ItemPin,
                                                Title = x.Deal.Title,
                                                Description = x.Deal.Description,
                                                ImageUrl = _AppConfig.StorageUrl + x.Deal.PosterStorage.Path,
                                                TAmount = x.ItemAmount,
                                                MerchantId = x.Guid,
                                                MerchantDisplayName = x.Deal.Account.DisplayName,
                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Deal.Account.IconStorage.Path,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                Status = x.Status.Name,
                                            })
                                   .Count();
                    #endregion
                    #region Get Data
                    _DealPurchaseHistory = _HCoreContext.MDDealCode
                                            .Where(x =>
                                             x.Account.Guid == _Request.AccountId)
                                            .Select(x => new OAccount.Deal.Buy.List.Response
                                            {
                                                DealRefId = x.Guid,
                                                DealId = x.Guid,
                                                DealCode = x.ItemCode,
                                                DealRedeemPin = x.ItemPin,
                                                Title = x.Deal.Title,
                                                Description = x.Deal.Description,
                                                ImageUrl = _AppConfig.StorageUrl + x.Deal.PosterStorage.Path,
                                                TAmount = x.ItemAmount,
                                                MerchantId = x.Guid,
                                                MerchantDisplayName = x.Deal.Account.DisplayName,
                                                MerchantIconUrl = _AppConfig.StorageUrl + x.Deal.Account.IconStorage.Path,
                                                StartDate = x.StartDate,
                                                EndDate = x.EndDate,
                                                Status = x.Status.Name,
                                            })
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _DealPurchaseHistory)
                    {
                        DataItem.Amount = Convert.ToInt64(DataItem.TAmount * 100);
                        //if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        //{
                        //    DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        //}
                        //else
                        //{
                        //    DataItem.IconUrl = _AppConfig.Default_Icon;
                        //}
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _DealPurchaseHistory, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC2000", ResponseCode.HC2000);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealCodes", _Exception, _Request.UserReference, _OResponse, "HC5000", ResponseCode.HC5000);
            }
            #endregion
        }
    }
}
