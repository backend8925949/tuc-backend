﻿using System;
using HCore.Helper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using HCore.ThankUCash.Gateway.Object;
using HCoreX.App.TUC.Ptsp.Framework;

namespace HCoreX.App.TUC.Ptsp.Core
{
    public class HCoreXAuthConnector
    {
        private readonly RequestDelegate _next;
        public HCoreXAuthConnector(RequestDelegate next)
        {
            _next = next;
        }
        public async Task Invoke(HttpContext _HttpContext)
        {
            var _Request = _HttpContext.Request;
            string _RequestBody = new StreamReader(_Request.Body).ReadToEnd();
            var Url = _Request.Path.Value;
            HCoreHelper.LogData(HCoreConstant.LogType.Log, "PTSPTRACKERVGEN", Url, _RequestBody, null);
            try
            {
                DateTime RequestTime = HCoreHelper.GetGMTDateTime();
                if (!string.IsNullOrEmpty(Url) && Url.Count(f => f == '/') == 4)
                {
                    string ApiName = Url.Split("/")[4];
                    string VersionName = Url.Split("/")[2];
                    if (VersionName == "v1")
                    {
                        var _Headers = _Request.Headers;
                        if (ApiName == "notifyscpayment")
                        {
                            string JsonString = null;
                            if (!string.IsNullOrEmpty(_RequestBody))
                            {
                                OThankUGateway.SCNotifyProductPaymentRequest? _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.SCNotifyProductPaymentRequest>(_RequestBody);
                                if (_RequestBodyContent?.eventType == "NEW_PAYMENT")
                                {
                                    JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                    string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_Notify(_Request, SubJsonContent, _RequestBodyContent.till.terminalID, RequestTime, ApiName);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {
                                        JObject _JObject = JObject.Parse(_RequestBody);
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        var Newjson = JsonConvert.SerializeObject(_JObject);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                        var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                                else
                                {
                                    JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                    string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, SubJsonContent, RequestTime, ApiName, null);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {
                                        JObject _JObject = JObject.Parse(_RequestBody);
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        var Newjson = JsonConvert.SerializeObject(_JObject);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                        var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "notifyscproduct")
                        {
                            string JsonString = null;
                            if (!string.IsNullOrEmpty(_RequestBody))
                            {
                                OThankUGateway.SCNotifyProductPaymentRequest _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.SCNotifyProductPaymentRequest>(_RequestBody);
                                JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                var HAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                                OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, SubJsonContent, RequestTime, ApiName, null);
                                if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                {
                                    JObject _JObject = JObject.Parse(_RequestBody);
                                    _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                    var Newjson = JsonConvert.SerializeObject(_JObject);
                                    var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                    Stream stream = await requestContent.ReadAsStreamAsync();
                                    _HttpContext.Request.Body = stream;
                                }
                                else
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "notifygatransaction")
                        {
                            string JsonString = null;
                            if (!string.IsNullOrEmpty(_RequestBody))
                            {
                                OThankUGateway.NotifyPayment.Request _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.NotifyPayment.Request>(_RequestBody);
                                JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                string SubJsonContent = JsonConvert.SerializeObject(_RequestBody);
                                var HAuthorization = _Headers.Where(x => x.Key == "Authorization").FirstOrDefault().Value;
                                if (!string.IsNullOrEmpty(HAuthorization))
                                {
                                }
                                else
                                {
                                }
                                if (_RequestBodyContent.paymentInstrument != null)
                                {
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_Notify(_Request, SubJsonContent, _RequestBodyContent.paymentInstrument.userId, RequestTime, ApiName);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {
                                        JObject _JObject = JObject.Parse(_RequestBody);
                                        _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                        var Newjson = JsonConvert.SerializeObject(_JObject);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                        var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }

                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "claimreward")
                        {
                            string HAppKey = _Headers.Where(x => x.Key == "hcak").FirstOrDefault().Value;
                            if (!string.IsNullOrEmpty(HAppKey))
                            {
                                var _RequestBodyContent = JsonConvert.DeserializeObject<OAuth.Request>(_RequestBody);
                                var ZxContent = HCoreEncrypt.DecodeText(_RequestBodyContent.zx);
                                JObject _JObject = JObject.Parse(ZxContent);
                                #region Validate Request
                                if (!string.IsNullOrEmpty(_RequestBodyContent.zx))
                                {
                                    string JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                                    OAuth.Response _OAuthApp = HCoreAuth.Auth_App(_Request, JsonString, RequestTime, ApiName);
                                    if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                                    {
                                        OAuth.Response _OAuth = HCoreAuth.Auth_Request(_Request, _OAuthApp);
                                        if (_OAuth.Status == HCoreConstant.StatusSuccess)
                                        {
                                            OUserReference _UReference = _OAuth.UserReference;
                                            _JObject.Add("UserReference", JObject.FromObject(_UReference));
                                        }
                                        else
                                        {
                                            var Data = HCoreAuth.Auth_Response(_OAuth.UserResponse, _OAuth);
                                            var json = JsonConvert.SerializeObject(Data);
                                            _HttpContext.Response.StatusCode = 401; //UnAuthorized
                                            await _HttpContext.Response.WriteAsync(json);
                                            return;
                                        }
                                        OAuth.Request _AuthRequest = new OAuth.Request();
                                        _AuthRequest.fx = _RequestBodyContent.fx;
                                        _AuthRequest.vx = _RequestBodyContent.vx;
                                        _AuthRequest.zx = HCoreEncrypt.EncodeText(JsonConvert.SerializeObject(_JObject));

                                        var Newjson = JsonConvert.SerializeObject(_AuthRequest);
                                        var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                        Stream stream = await requestContent.ReadAsStreamAsync();
                                        _HttpContext.Request.Body = stream;
                                    }
                                    else
                                    {
                                        var Data = HCoreAuth.Auth_Response(_OAuthApp.UserResponse, _OAuthApp);
                                        var json = JsonConvert.SerializeObject(Data);
                                        _HttpContext.Response.StatusCode = 401;
                                        await _HttpContext.Response.WriteAsync(json);
                                        return;
                                    }
                                }
                                else
                                {
                                    _HttpContext.Response.StatusCode = 401;
                                    return;
                                }
                                #endregion
                            }
                            else
                            {
                                _HttpContext.Response.StatusCode = 401;
                                return;
                            }
                        }
                        else if (ApiName == "notifysettlements")
                        {
                            string JsonString = null;
                            OThankUGateway.NotifySettlement _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.NotifySettlement>(_RequestBody);
                            JsonString = JsonConvert.SerializeObject(_RequestBodyContent);
                            OAuth.Response _OAuthApp = HCoreAuth.Auth_OuterRequest(_Request, JsonString, RequestTime, ApiName);
                            if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                            {
                                JObject _JObject = JObject.Parse(_RequestBody);
                                _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                var Newjson = JsonConvert.SerializeObject(_JObject);
                                var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                Stream stream = await requestContent.ReadAsStreamAsync();
                                _HttpContext.Request.Body = stream;
                            }
                            else
                            {

                                string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                var json = JsonConvert.SerializeObject(Data);
                                _HttpContext.Response.StatusCode = 401;
                                await _HttpContext.Response.WriteAsync(json);
                                return;
                            }
                        }
                        else
                        {
                            await CMTPush.PostCMTData(_RequestBody, ApiName);
                            var ResponseType = _Headers.Where(x => x.Key == "ResponseType").FirstOrDefault().Value;
                            OAuth.Response _OAuthApp = HCoreAuth.Auth_Key(_Request, _RequestBody, RequestTime, ApiName, ResponseType);
                            if (_OAuthApp.Status == HCoreConstant.StatusSuccess)
                            {
                                JObject _JObject = JObject.Parse(_RequestBody);
                                _JObject.Add("UserReference", JObject.FromObject(_OAuthApp.UserReference));
                                var Newjson = JsonConvert.SerializeObject(_JObject);
                                var requestContent = new StringContent(Newjson, Encoding.UTF8, "application/json");
                                Stream stream = await requestContent.ReadAsStreamAsync();
                                _HttpContext.Request.Body = stream;


                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(ResponseType) && ResponseType == "json")
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefaultObject(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                                else
                                {
                                    string AuthResponse = JsonConvert.SerializeObject(_OAuthApp);
                                    var Data = HCoreAuth.Auth_ResponseDefault(_OAuthApp.UserResponse);
                                    var json = JsonConvert.SerializeObject(Data);
                                    _HttpContext.Response.StatusCode = 401;
                                    await _HttpContext.Response.WriteAsync(json);
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        _HttpContext.Response.StatusCode = 401;
                        return;
                    }
                }
                else
                {
                    _HttpContext.Response.StatusCode = 401;
                    return;
                }

                await _next.Invoke(_HttpContext);
            }
            catch (Exception _Exception)
            {
                if (!string.IsNullOrEmpty(_RequestBody))
                {
                    HCoreHelper.LogData(HCoreConstant.LogType.Exception, "INVOCE-EX-BODY", _RequestBody, String.Empty, null);
                }
                HCoreHelper.LogException("Invoke", _Exception);
                _HttpContext.Response.StatusCode = 401;
                return;
            }
        }
    }
    public static class HCoreXAuthConnectorExtension
    {
        public static IApplicationBuilder HCoreXAuth(this IApplicationBuilder app)
        {
            app.UseMiddleware<HCoreXAuthConnector>();
            return app;
        }
    }
}

