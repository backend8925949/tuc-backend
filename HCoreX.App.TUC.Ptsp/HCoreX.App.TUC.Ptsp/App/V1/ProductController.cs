﻿using HCore.Helper;
using HCore.ThankUCash.Gateway;
using HCore.ThankUCash.Gateway.Object;
using Microsoft.AspNetCore.Mvc;

namespace HCoreX.App.TUC.Ptsp.App.V1
{
    [Produces("application/json")]
    [Route("api/v1/crm/[action]")]
    public class ProductController : ControllerBase
    {
        ManageThankUCashGateway? _ManageThankUCashGateway;
        //[ActionName("notifyproducts")]
        //public object SaveProduct([FromBody] OThankUGateway.Request _Request)
        //{
        //    _ManageThankUCashGateway = new ManageThankUCashGateway();
        //    OResponse _Response = _ManageThankUCashGateway.SaveProduct(_Request);
        //    return HCoreAuth.Auth_ResponseDefault(_Response);
        //}


        [ActionName("syncproducts")]
        public object SyncProducts([FromBody] OThankUGateway.Request _Request)
        {
            _ManageThankUCashGateway = new ManageThankUCashGateway();
            OResponse _Response = _ManageThankUCashGateway.SaveProduct(_Request);
            return HCoreAuth.Auth_ResponseDefault(_Response);
        }
    }
}

