﻿using System;
using HCore.Helper;
using HCore.ThankUCash.Gateway;
using HCore.ThankUCash.Gateway.Object;
using Microsoft.AspNetCore.Mvc;

namespace HCoreX.App.TUC.Ptsp.App.V1
{
    [Produces("application/json")]
    [Route("api/v1/loyalty/[action]")]
    public class POSLoyaltyController : ControllerBase
    {
        ManageThankUCashGateway? _ManageThankUCashGateway;
        ManageGatewayRewardV1? _ManageGatewayRewardV1;
        ManageMerchant? _ManageMerchant;

        [ActionName("syncchange")]
        public object SyncChangeItem([FromBody] OThankUGateway.Request _Request)
        {
            return "Hello with new change";
        }
        [ActionName("getconfiguration")]
        public object GetConfiguration([FromBody] OThankUGateway.Request _Request)
        {
            _ManageThankUCashGateway = new ManageThankUCashGateway();
            OResponse _Response = _ManageThankUCashGateway.GetConfiguration(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [ActionName("reward")]
        public object Reward([FromBody] OThankUGateway.Request _Request)
        {
            _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
            OResponse _Response = _ManageGatewayRewardV1.SettleTransaction(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [ActionName("getbalance")]
        public object GetBalance([FromBody] OThankUGateway.Request _Request)
        {
            _ManageThankUCashGateway = new ManageThankUCashGateway();
            OResponse _Response = _ManageThankUCashGateway.GetBalance(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [ActionName("redeem")]
        public async Task<object> Redeem([FromBody] OThankUGateway.Request _Request)
        {
            _ManageThankUCashGateway = new ManageThankUCashGateway();
            OResponse _Response = await _ManageThankUCashGateway.Redeem(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }


        [ActionName("canceltransaction")]
        public object CancelTransaction([FromBody] OThankUGateway.Request _Request)
        {
            _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
            OResponse _Response = _ManageGatewayRewardV1.CancelTransaction(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }

        [ActionName("notifyfailedtransaction")]
        public object NotifyFailedTransaction([FromBody] OThankUGateway.Request _Request)
        {
            _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
            OResponse _Response = _ManageGatewayRewardV1.NotifyFailedTransaction(_Request);
            return HCoreAuth.Auth_ResponseDefaultObject(_Response);
        }
    }
}

