﻿using HCore.Helper;
using HCore.ThankUCash.Gateway;
using HCore.ThankUCash.Gateway.Object;
using Microsoft.AspNetCore.Mvc;

namespace HCoreX.App.TUC.Ptsp.App.V1
{
    [Produces("application/json")]
    [Route("api/v1/thankuconnect/[action]")]
    public class LoyaltyController : ControllerBase
    {
        ManageThankUCashGateway _ManageThankUCashGateway;
        ManageGatewayRewardV1 _ManageGatewayRewardV1;
        ManageMerchant _ManageMerchant;


        [ActionName("registermerchant")]
        public object RegisterMerchant([FromBody] OThankUGateway.MerchantRegistration.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageMerchant = new ManageMerchant();
                    OResponse _Response = _ManageMerchant.RegisterMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageMerchant = new ManageMerchant();
                    OResponse _Response = _ManageMerchant.RegisterMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageMerchant = new ManageMerchant();
                    OResponse _Response = _ManageMerchant.RegisterMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageMerchant = new ManageMerchant();
                    OResponse _Response = _ManageMerchant.RegisterMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }
        // UPGRADE START V1
        [ActionName("initializetransaction")]
        public object InitializeTransaction([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.InitializeTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.InitializeTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.InitializeTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.InitializeTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
            //object ProcessRequest()
            //{

            //}

            //return Task.FromResult<object>(ProcessRequest());
        }

        [ActionName("settletransaction")]
        public object SettleTransaction([FromBody] OThankUGateway.Request _Request)
        {

            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.SettleTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.SettleTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.SettleTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.SettleTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }

        //[ActionName("notifyscpayment")]
        //public IActionResult NotifySoftComTransaction([FromBody] OThankUGateway.SCNotifyProductPaymentRequest _Request)
        //{
        //    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
        //    OResponse _Response = _ManageGatewayRewardV1.NotifySoftComTransaction(_Request);
        //    if (_Response.Status == "Success")
        //    {
        //        return StatusCode(200, HCoreAuth.Auth_ResponseDefault(_Response));
        //    }
        //    else
        //    {
        //        return StatusCode(400, HCoreAuth.Auth_ResponseDefault(_Response));
        //    }
        //}

        [ActionName("notifygatransaction")]
        public IActionResult NotifyTransaction([FromBody] OThankUGateway.NotifyPayment.Request _Request)
        {
            _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
            OResponse _Response = _ManageGatewayRewardV1.NotifyTransaction(_Request);
            if (_Response.Status == "Success")
            {
                return StatusCode(200, HCoreAuth.Auth_ResponseDefault(_Response));
            }
            else
            {
                return StatusCode(400, HCoreAuth.Auth_ResponseDefault(_Response));
            }
        }

        [ActionName("notifyfailedtransaction")]
        public object NotifyFailedTransaction([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifyFailedTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifyFailedTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifyFailedTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifyFailedTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }

        [ActionName("canceltransaction")]
        public object CancelTransaction([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.CancelTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.CancelTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.CancelTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.CancelTransaction(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }

        [ActionName("notifysettlements")]
        public object NotifySettlements([FromBody] OThankUGateway.NotifySettlement _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifySettlements(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifySettlements(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifySettlements(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageGatewayRewardV1 = new ManageGatewayRewardV1();
                    OResponse _Response = _ManageGatewayRewardV1.NotifySettlements(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }


        // UPGRADE END V1 
        // Operations
        [ActionName("connectaccount")]
        public object ConnectMerchant([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ConnectMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ConnectMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ConnectMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ConnectMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }

        [ActionName("removeaccount")]
        public object RemoveMerchant([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RemoveMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RemoveMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RemoveMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RemoveMerchant(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }

        [ActionName("getconfig")]
        public object GetConfiguration([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetConfiguration(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetConfiguration(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetConfiguration(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetConfiguration(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }


        /// <summary>
        /// Request balance of customer
        /// </summary>
        /// <param name="_Request">Object containing terminal id , customer accountnumber / mobile number</param>
        /// <returns></returns>
        [ActionName("getbalance")]
        public object GetBalance([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetBalance(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetBalance(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetBalance(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.GetBalance(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }

        [ActionName("validateaccount")]
        public object ValidateAccount([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ValidateAccount(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ValidateAccount(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ValidateAccount(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ValidateAccount(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }



        [ActionName("registeruser")]
        public object RegisterUser([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUser(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }

        // USSD
        [ActionName("resetussduserpin")]
        public object ResetUssdUserPin([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ResetUssdUserPin(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ResetUssdUserPin(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ResetUssdUserPin(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.ResetUssdUserPin(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }

        [ActionName("updateussduser")]
        public object UpdateUssdUser([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.UpdateUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.UpdateUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.UpdateUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.UpdateUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }

        [ActionName("registerussduser")]
        public object RegisterUssdUser([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.RegisterUssdUser(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }

        [ActionName("rewardaccount")]
        public object MerchantReward([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.IsRsa)
            {
                _ManageThankUCashGateway = new ManageThankUCashGateway();
                OResponse _Response = _ManageThankUCashGateway.MerchantReward(_Request);
                return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
            }
            else
            {
                _ManageThankUCashGateway = new ManageThankUCashGateway();
                OResponse _Response = _ManageThankUCashGateway.MerchantReward(_Request);
                return HCoreAuth.Auth_ResponseDefaultObject(_Response);
            }
        }

        [ActionName("reward")]
        public object CreditReward([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.IsRsa)
            {
                _ManageThankUCashGateway = new ManageThankUCashGateway();
                OResponse _Response = _ManageThankUCashGateway.CreditReward(_Request);
                return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
            }
            else
            {
                _ManageThankUCashGateway = new ManageThankUCashGateway();
                OResponse _Response = _ManageThankUCashGateway.CreditReward(_Request);
                return HCoreAuth.Auth_ResponseDefaultObject(_Response);
            }
        }

        // Redeem
        [ActionName("redeem")]
        public async Task<object> Redeem([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.InitializeRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.InitializeRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.InitializeRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.InitializeRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }

        [ActionName("confirmredeem")]
        public async Task<object> ConfirmRedeem([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.ConfirmRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.ConfirmRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }
            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.ConfirmRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = await _ManageThankUCashGateway.ConfirmRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }
            }
        }

        [ActionName("redeemaccount")]
        public object MerchantRedeem([FromBody] OThankUGateway.Request _Request)
        {
            if (_Request.UserReference.ResponseType == "json")
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.MerchantRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObjectRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.MerchantRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultObject(_Response);
                }

            }
            else
            {
                if (_Request.UserReference.IsRsa)
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.MerchantRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefaultRsa(_Response);
                }
                else
                {
                    _ManageThankUCashGateway = new ManageThankUCashGateway();
                    OResponse _Response = _ManageThankUCashGateway.MerchantRedeem(_Request);
                    return HCoreAuth.Auth_ResponseDefault(_Response);
                }

            }
        }


    }
}

