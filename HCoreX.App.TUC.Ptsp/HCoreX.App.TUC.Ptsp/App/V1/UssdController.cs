﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace HCoreX.App.TUC.Ptsp.App.V1
{
    [Produces("application/json")]
    [Route("api/v1/ussdconnect/[action]")]
    public class UssdController : ControllerBase
    {
        public class OUssdParameter
        {
            public string? sessionId { get; set; }
            public string? serviceCode { get; set; }
            public string? phoneNumber { get; set; }
            public string? text { get; set; }
        }
        [ActionName("notifyussd")]
        public IActionResult NotifyTransaction([FromBody] OUssdParameter _Request)
        {
            string ResponseContent = "";
            if (_Request.text == "")
            {
                ResponseContent = "CON Welcome To ThankUCash \n";
                ResponseContent += "Enter your name \n";
            }
            else if (_Request.text == "1")
            {
                // Business logic for first level response
                ResponseContent = "CON Select your gender \n";
                ResponseContent += "1. Male \n";
                ResponseContent += "2. Female";
            }
            else if (_Request.text == "2")
            {
                ResponseContent = "CON Enter date of birth \n";
                ResponseContent += "ddmmyy \n";
            }
            else if (_Request.text == "3")
            {
                ResponseContent = "CON Enter email address \n";
                ResponseContent += "ddmmyy \n";
            }
            else if (_Request.text == "4")
            {
                ResponseContent = "END Thank U " + _Request + " for registration with us. Download ThankUCash for crazy deals and offers \n";
            }
            return StatusCode(200, ResponseContent);
        }

    }
}

