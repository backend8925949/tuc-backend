﻿using System;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Pipedrive;
using RestSharp;
using static HCore.Helper.HCoreConstant;
using HCore.ThankUCash.Gateway.Object;

namespace HCoreX.App.TUC.Ptsp.Framework
{
    public static class CMTPush
    {
        public class cmtObject
        {
            public string type { get; set; }
            public string terminalId { get; set; }
            public string cashierId { get; set; }
            public string mobileNumber { get; set; }
            public string referenceNumber { get; set; }
            public string transactionMode { get; set; }
            public string invoiceAmount { get; set; }
            public string binNumber { get; set; }
        }

        public static async Task PostCMTData(string requestData, string Type)
        {
            OThankUGateway.Request _RequestBodyContent = JsonConvert.DeserializeObject<OThankUGateway.Request>(requestData);
            if (_RequestBodyContent != null)
            {
                if (!string.IsNullOrEmpty(_RequestBodyContent.TerminalId))
                {
                    cmtObject _cmtObject = new cmtObject();
                    _cmtObject.type = Type;
                    _cmtObject.terminalId = _RequestBodyContent.TerminalId;
                    _cmtObject.cashierId = _RequestBodyContent.CashierId;
                    _cmtObject.mobileNumber = _RequestBodyContent.MobileNumber;
                    _cmtObject.referenceNumber = _RequestBodyContent.ReferenceNumber;
                    _cmtObject.transactionMode = _RequestBodyContent.TransactionMode;
                    _cmtObject.invoiceAmount = _RequestBodyContent.InvoiceAmount.ToString();
                    if (!string.IsNullOrEmpty(_RequestBodyContent.bin))
                    {
                        _cmtObject.binNumber = _RequestBodyContent.bin;
                    }
                    else if (!string.IsNullOrEmpty(_RequestBodyContent.SixDigitPan))
                    {
                        _cmtObject.binNumber = _RequestBodyContent.SixDigitPan;
                    }
                    else if (!string.IsNullOrEmpty(_RequestBodyContent.ban))
                    {
                        _cmtObject.binNumber = _RequestBodyContent.ban;
                    }
                    string Url = "https://bankandtech.thankucash.dev/api/v1/transaction/downloadpostransaction";
                    var client = new RestSharp.RestClient(Url);
                    var request = new RestSharp.RestRequest();
                    request.Method = Method.Post;
                    request.AddHeader("accept", "application/json");
                    request.AddHeader("content-type", "application/json");
                    request.AddParameter("application/json", JsonConvert.SerializeObject(_cmtObject), RestSharp.ParameterType.RequestBody);
                    var response = await client.ExecuteAsync(request);


                    string Url_Tech = "https://bankandtech.thankucash.tech/api/v1/transaction/downloadpostransaction";
                    var client_Tech = new RestSharp.RestClient(Url_Tech);
                    var request_Tech = new RestSharp.RestRequest();
                    request_Tech.Method = Method.Post;
                    request_Tech.AddHeader("accept", "application/json");
                    request_Tech.AddHeader("content-type", "application/json");
                    request_Tech.AddParameter("application/json", JsonConvert.SerializeObject(_cmtObject), RestSharp.ParameterType.RequestBody);
                    var response_tech = await client_Tech.ExecuteAsync(request_Tech);
                }
            }

        }
    }
}

