//==================================================================================
// FileName: CoreDataStoreOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using HCore.Helper;
using System.Collections.Generic;
using Akka.Actor;

namespace HCore.Data.Store.Core
{
    internal static class CoreDataStoreOperation
    {

        internal static void SyncDataStore()
        {
            try
            {
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    #region Helpers
                    List<OHCoreDataStore.Helper> SystemHelpers = (from x in _HCoreContext.HCCore
                                                                  where x.SyncTime > HCoreDataStore.SyncTime
                                                                  select new OHCoreDataStore.Helper
                                                                  {
                                                                      ReferenceId = x.Id,
                                                                      ReferenceKey = x.SystemName,

                                                                      Name = x.Name,
                                                                      SystemName = x.SystemName,
                                                                      TypeName = x.TypeName,
                                                                      Value = x.Value,

                                                                      Sequence = x.Sequence,

                                                                      ParentId = x.ParentId,
                                                                      ParentName = x.Parent.Name,
                                                                      ParentCode = x.Parent.SystemName,

                                                                      SubParentId = x.SubParentId,
                                                                      SubParentName = x.SubParent.Name,
                                                                      SubParentCode = x.SubParent.SystemName,

                                                                      StatusId = x.StatusId,
                                                                      StatusCode = x.Status.SystemName,
                                                                      StatusName = x.Status.Name,
                                                                  }).ToList();
                    if (SystemHelpers.Count > 0)
                    {
                        foreach (var Item in SystemHelpers)
                        {
                            var ExistingItem = HCoreDataStore.SystemHelpers.Where(x => x.ReferenceId == Item.ReferenceId).FirstOrDefault();
                            if (ExistingItem != null)
                            {
                                ExistingItem = Item;
                            }
                            else
                            {
                                HCoreDataStore.SystemHelpers.Add(Item);
                            }
                        }
                    }
                    #endregion
                    #region Bin Numbers
                    List<OHCoreDataStore.BinParameter> BinNumbers = (from x in _HCoreContext.HCCoreCommon
                                                                     where (x.CreateDate > HCoreDataStore.SyncTime || x.ModifyDate > HCoreDataStore.SyncTime)
                                                                     &&
                                                                     (x.TypeId == HelperType.BinCardBrand
                                                                     || x.TypeId == HelperType.BinCardType
                                                                     || x.TypeId == HelperType.BinCardBank)
                                                                     select new OHCoreDataStore.BinParameter
                                                                     {
                                                                         Name = x.Name,
                                                                         ReferenceId = x.Id,
                                                                         SystemName = x.SystemName,
                                                                         TypeId = x.TypeId,
                                                                         TypeName = x.Type.Name,
                                                                     }).ToList();
                    if (BinNumbers.Count > 0)
                    {
                        foreach (var Item in BinNumbers)
                        {
                            var ExistingItem = HCoreDataStore.SystemBinParameters.Where(x => x.ReferenceId == Item.ReferenceId).FirstOrDefault();
                            if (ExistingItem != null)
                            {
                                ExistingItem = Item;
                            }
                            else
                            {
                                HCoreDataStore.SystemBinParameters.Add(Item);
                            }
                        }
                    }
                    #endregion
                    #region Accounts
                    List<OHCoreDataStore.Account> Accounts = (from x in _HCoreContext.HCUAccount
                                                              where (x.CreateDate > HCoreDataStore.SyncTime || x.ModifyDate > HCoreDataStore.SyncTime)
                                                              &&
                                                            (x.AccountTypeId == Helpers.UserAccountType.Acquirer
                                                             || x.AccountTypeId == Helpers.UserAccountType.PgAccount
                                                             || x.AccountTypeId == Helpers.UserAccountType.PosAccount
                                                             || x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                             || x.AccountTypeId == Helpers.UserAccountType.Merchant
                                                             || x.AccountTypeId == Helpers.UserAccountType.MerchantCashier
                                                             || x.AccountTypeId == Helpers.UserAccountType.AcquirerSubAccount
                                                             || x.AccountTypeId == Helpers.UserAccountType.MerchantSubAccount
                                                             || x.AccountTypeId == Helpers.UserAccountType.Controller
                                                             || x.AccountTypeId == Helpers.UserAccountType.Admin)
                                                              select new OHCoreDataStore.Account
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,

                                                                  UserName = x.User.Username,

                                                                  DisplayName = x.DisplayName,
                                                                  Name = x.Name,
                                                                  FirstName = x.FirstName,
                                                                  LastName = x.LastName,

                                                                  EmailAddress = x.EmailAddress,
                                                                  SecondaryEmailAddress = x.SecondaryEmailAddress,

                                                                  ContactNumber = x.ContactNumber,
                                                                  MobileNumber = x.MobileNumber,

                                                                  AccountCode = x.AccountCode,

                                                                  Address = x.Address,
                                                                  Latitude = x.Latitude,
                                                                  Longitude = x.Longitude,

                                                                  IconUrl = x.IconStorage.Path,
                                                                  PosterUrl = x.PosterStorage.Path,

                                                                  Description = x.Description,


                                                                  AccountTypeId = x.AccountTypeId,
                                                                  AccountTypeCode = x.AccountType.SystemName,
                                                                  AccountTypeName = x.AccountType.Name,

                                                                  RoleId = x.RoleId,
                                                                  RoleKey = x.Role.Guid,
                                                                  RoleName = x.Role.Name,


                                                                  LastLoginDate = x.LastLoginDate,
                                                                  LastActivityDate = null,

                                                                  LastTransactionDate = x.LastTransactionDate,

                                                                  CreateDate = x.CreateDate,
                                                                  CreatedById = x.CreatedById,
                                                                  CreatedByKey = x.CreatedBy.Guid,
                                                                  CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                  CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                                  CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                                  CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                                  ModifyDate = x.ModifyDate,
                                                                  ModifyById = x.ModifyById,
                                                                  ModifyByKey = x.ModifyBy.Guid,
                                                                  ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                  ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                                  ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                                  ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                                  StatusId = x.StatusId,
                                                                  StatusCode = x.Status.SystemName,
                                                                  StatusName = x.Status.Name,

                                                                  OwnerId = x.OwnerId,
                                                                  OwnerKey = x.Owner.Guid,
                                                                  OwnerDisplayName = x.Owner.DisplayName,
                                                                  OwnerIconUrl = x.Owner.IconStorage.Path,
                                                                  OwnerAccountTypeId = x.Owner.AccountTypeId,

                                                                  SubOwnerId = x.SubOwnerId,
                                                                  SubOwnerKey = x.SubOwner.Guid,
                                                                  SubOwnerDisplayName = x.SubOwner.DisplayName,
                                                                  SubOwnerIconUrl = x.SubOwner.IconStorage.Path,

                                                                  BankId = x.BankId,
                                                                  BankKey = x.Bank.Guid,
                                                                  BankDisplayName = x.Bank.DisplayName,
                                                                  BankIconUrl = x.Bank.IconStorage.Path,

                                                                  MerchantsCount = 0,
                                                                  StoresCount = 0,
                                                                  AcquirersCount = 0,
                                                                  TerminalsCount = 0,
                                                                  CashiersCount = 0,
                                                                  PgAccountsCount = 0,
                                                                  PosAccountsCount = 0,
                                                                  SubAccountsCount = 0,
                                                              }).ToList();

                    if (Accounts.Count > 0)
                    {
                        #region Accounts Data Update
                        foreach (var Account in Accounts)
                        {
                            if (Account.AccountTypeId == Helpers.UserAccountType.Merchant)
                            {
                                Account.StoresCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore);
                                Account.CashiersCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantCashier);
                                Account.SubAccountsCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantSubAccount);
                                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                            }
                            if (Account.AccountTypeId == Helpers.UserAccountType.Acquirer)
                            {
                                Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                Account.SubAccountsCount = HCoreDataStore.Accounts.Where(x => x.OwnerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                            }
                            if (Account.AccountTypeId == Helpers.UserAccountType.PosAccount)
                            {
                                Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                            }
                            if (Account.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                            {
                                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                            }
                            if (!string.IsNullOrEmpty(Account.IconUrl))
                            {
                                Account.IconUrl = _AppConfig.StorageUrl + Account.IconUrl;
                            }
                            else
                            {
                                Account.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Account.OwnerIconUrl))
                            {
                                Account.OwnerIconUrl = _AppConfig.StorageUrl + Account.OwnerIconUrl;
                            }
                            else
                            {
                                Account.OwnerIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Account.SubOwnerIconUrl))
                            {
                                Account.SubOwnerIconUrl = _AppConfig.StorageUrl + Account.SubOwnerIconUrl;
                            }
                            else
                            {
                                Account.SubOwnerIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Account.BankIconUrl))
                            {
                                Account.BankIconUrl = _AppConfig.StorageUrl + Account.BankIconUrl;
                            }
                            else
                            {
                                Account.BankIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #endregion
                        foreach (var Item in Accounts)
                        {
                            var ExistingItem = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Item.ReferenceId).FirstOrDefault();
                            if (ExistingItem != null)
                            {
                                ExistingItem = Item;
                            }
                            else
                            {
                                HCoreDataStore.Accounts.Add(Item);
                            }
                        }
                    }
                    #endregion
                    #region Terminals
                    List<OHCoreDataStore.Terminal> Terminals = (from x in _HCoreContext.TUCTerminal
                                                                where (x.CreateDate > HCoreDataStore.SyncTime || x.ModifyDate > HCoreDataStore.SyncTime)
                                                                select new OHCoreDataStore.Terminal
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,
                                                                    TerminalId = x.IdentificationNumber,
                                                                    DisplayName = x.DisplayName,
                                                                    ProviderId = x.ProviderId,
                                                                    MerchantId = x.MerchantId,
                                                                    StoreId = x.StoreId,
                                                                    AcquirerId = x.AcquirerId,

                                                                    ApplicationStatusId = x.ActivityStatusId,
                                                                    LastTransactionDate = x.LastTransactionDate,

                                                                    CreateDate = x.CreateDate,
                                                                    CreatedById = x.CreatedById,
                                                                    CreatedByKey = x.CreatedBy.Guid,
                                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                    CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                                    CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                                    CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                                    ModifyDate = x.ModifyDate,
                                                                    ModifyById = x.ModifyById,
                                                                    ModifyByKey = x.ModifyBy.Guid,
                                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                    ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                                    ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                                    ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                                    StatusId = x.StatusId,
                                                                    StatusCode = x.Status.SystemName,
                                                                    StatusName = x.Status.Name,

                                                                }).ToList();
                    if (Terminals.Count > 0)
                    {
                        #region Terminals Data Update
                        foreach (var Terminal in Terminals)
                        {
                            Terminal.RmId = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == Terminal.StoreId
                            && x.Account.OwnerId == Terminal.AcquirerId && x.StatusId == HelperStatus.Default.Active
                            && x.Account.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager).Select(x => x.AccountId).FirstOrDefault();
                            Terminal.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == Terminal.ReferenceId).OrderByDescending(x => x.Id).Select(x => x.TransactionDate).FirstOrDefault();
                            var ProviderDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.ProviderId).FirstOrDefault();
                            if (ProviderDetails != null)
                            {
                                Terminal.ProviderKey = ProviderDetails.ReferenceKey;
                                if (!string.IsNullOrEmpty(ProviderDetails.IconUrl))
                                {
                                    Terminal.ProviderIconUrl = ProviderDetails.IconUrl;
                                }
                                Terminal.ProviderDisplayName = ProviderDetails.DisplayName;
                                Terminal.ProviderStatusId = ProviderDetails.StatusId;
                            }

                            var MerchantDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                Terminal.MerchantKey = MerchantDetails.ReferenceKey;
                                if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                                {
                                    Terminal.MerchantIconUrl = MerchantDetails.IconUrl;
                                }
                                Terminal.MerchantDisplayName = MerchantDetails.DisplayName;
                                Terminal.MerchantStatusId = MerchantDetails.StatusId;
                            }

                            var StoreDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.StoreId).FirstOrDefault();
                            if (StoreDetails != null)
                            {
                                Terminal.StoreKey = StoreDetails.ReferenceKey;
                                if (!string.IsNullOrEmpty(StoreDetails.IconUrl))
                                {
                                    Terminal.StoreIconUrl = StoreDetails.IconUrl;
                                }
                                Terminal.StoreDisplayName = StoreDetails.DisplayName;
                                Terminal.StoreStatusId = StoreDetails.StatusId;
                            }
                            var AcquirerDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.AcquirerId).FirstOrDefault();
                            if (AcquirerDetails != null)
                            {
                                Terminal.AcquirerKey = AcquirerDetails.ReferenceKey;
                                if (!string.IsNullOrEmpty(AcquirerDetails.IconUrl))
                                {
                                    Terminal.AcquirerIconUrl = AcquirerDetails.IconUrl;
                                }
                                Terminal.AcquirerDisplayName = AcquirerDetails.DisplayName;
                                Terminal.AcquirerStatusId = AcquirerDetails.StatusId;
                            }
                            var RmDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.RmId).FirstOrDefault();
                            if (RmDetails != null)
                            {
                                Terminal.RmKey = RmDetails.ReferenceKey;
                                Terminal.RmDisplayName = RmDetails.DisplayName;
                            }
                        }
                        #endregion
                        foreach (var Item in Terminals)
                        {
                            var ExistingItem = HCoreDataStore.Terminals.Where(x => x.ReferenceId == Item.ReferenceId).FirstOrDefault();
                            if (ExistingItem != null)
                            {
                                ExistingItem = Item;
                            }
                            else
                            {
                                HCoreDataStore.Terminals.Add(Item);
                            }
                        }
                    }
                    #endregion
                    _HCoreContext.Dispose();
                }
                HCoreDataStore.SyncTime = HCoreHelper.GetGMTDateTime();
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SyncDataStore", _Exception);
            }
        }
        internal static void LoadDataStore()
        {
            try
            {


                HCoreDataStore.SyncTime = HCoreHelper.GetGMTDateTime();
                HCoreDataStore.SystemHelpers.Clear();
                HCoreDataStore.Terminals.Clear();
                HCoreDataStore.Accounts.Clear();
                HCoreDataStore.Configurations.Clear();
                HCoreDataStore.AccountConfigurations.Clear();
                HCoreDataStore.SystemBinParameters.Clear();
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    #region Helpers
                    HCoreDataStore.SystemHelpers = (from x in _HCoreContext.HCCore
                                                    select new OHCoreDataStore.Helper
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.SystemName,

                                                        Name = x.Name,
                                                        SystemName = x.SystemName,
                                                        TypeName = x.TypeName,
                                                        Value = x.Value,

                                                        Sequence = x.Sequence,

                                                        ParentId = x.ParentId,
                                                        ParentName = x.Parent.Name,
                                                        ParentCode = x.Parent.SystemName,

                                                        SubParentId = x.SubParentId,
                                                        SubParentName = x.SubParent.Name,
                                                        SubParentCode = x.SubParent.SystemName,

                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    }).ToList();
                    #endregion
                    #region Bin Numbers
                    HCoreDataStore.SystemBinParameters = (from x in _HCoreContext.HCCoreCommon
                                                          where
                                                          x.TypeId == HelperType.BinCardBrand
                                                          || x.TypeId == HelperType.BinCardType
                                                          || x.TypeId == HelperType.BinCardBank
                                                          select new OHCoreDataStore.BinParameter
                                                          {
                                                              Name = x.Name,
                                                              ReferenceId = x.Id,
                                                              SystemName = x.SystemName,
                                                              TypeId = x.TypeId,
                                                              TypeName = x.Type.Name,
                                                          }).ToList();
                    #endregion
                    #region Accounts
                    HCoreDataStore.Accounts = (from x in _HCoreContext.HCUAccount
                                               where
                                              x.AccountTypeId == Helpers.UserAccountType.Acquirer
                                              || x.AccountTypeId == Helpers.UserAccountType.PgAccount
                                              || x.AccountTypeId == Helpers.UserAccountType.PosAccount
                                              || x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                              || x.AccountTypeId == Helpers.UserAccountType.Merchant
                                              || x.AccountTypeId == Helpers.UserAccountType.MerchantCashier
                                              || x.AccountTypeId == Helpers.UserAccountType.AcquirerSubAccount
                                              || x.AccountTypeId == Helpers.UserAccountType.MerchantSubAccount
                                              || x.AccountTypeId == Helpers.UserAccountType.Controller
                                              || x.AccountTypeId == Helpers.UserAccountType.Admin
                                               select new OHCoreDataStore.Account
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,

                                                   UserName = x.User.Username,

                                                   DisplayName = x.DisplayName,
                                                   Name = x.Name,
                                                   FirstName = x.FirstName,
                                                   LastName = x.LastName,

                                                   EmailAddress = x.EmailAddress,
                                                   SecondaryEmailAddress = x.SecondaryEmailAddress,

                                                   ContactNumber = x.ContactNumber,
                                                   MobileNumber = x.MobileNumber,

                                                   AccountCode = x.AccountCode,

                                                   Address = x.Address,
                                                   Latitude = x.Latitude,
                                                   Longitude = x.Longitude,

                                                   IconUrl = x.IconStorage.Path,
                                                   PosterUrl = x.PosterStorage.Path,

                                                   Description = x.Description,


                                                   AccountTypeId = x.AccountTypeId,
                                                   AccountTypeCode = x.AccountType.SystemName,
                                                   AccountTypeName = x.AccountType.Name,

                                                   RoleId = x.RoleId,
                                                   RoleKey = x.Role.Guid,
                                                   RoleName = x.Role.Name,


                                                   LastLoginDate = x.LastLoginDate,
                                                   LastActivityDate = null,

                                                   LastTransactionDate = x.LastTransactionDate,

                                                   CreateDate = x.CreateDate,
                                                   CreatedById = x.CreatedById,
                                                   CreatedByKey = x.CreatedBy.Guid,
                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                   CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                   CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                   CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                   ModifyDate = x.ModifyDate,
                                                   ModifyById = x.ModifyById,
                                                   ModifyByKey = x.ModifyBy.Guid,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                   ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                   ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                   ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                   StatusId = x.StatusId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name,

                                                   OwnerId = x.OwnerId,
                                                   OwnerKey = x.Owner.Guid,
                                                   OwnerDisplayName = x.Owner.DisplayName,
                                                   OwnerIconUrl = x.Owner.IconStorage.Path,
                                                   OwnerAccountTypeId = x.Owner.AccountTypeId,

                                                   SubOwnerId = x.SubOwnerId,
                                                   SubOwnerKey = x.SubOwner.Guid,
                                                   SubOwnerDisplayName = x.SubOwner.DisplayName,
                                                   SubOwnerIconUrl = x.SubOwner.IconStorage.Path,

                                                   BankId = x.BankId,
                                                   BankKey = x.Bank.Guid,
                                                   BankDisplayName = x.Bank.DisplayName,
                                                   BankIconUrl = x.Bank.IconStorage.Path,

                                                   MerchantsCount = 0,
                                                   StoresCount = 0,
                                                   AcquirersCount = 0,
                                                   TerminalsCount = 0,
                                                   CashiersCount = 0,
                                                   PgAccountsCount = 0,
                                                   PosAccountsCount = 0,
                                                   SubAccountsCount = 0,
                                               }).ToList();
                    #endregion
                    #region Terminals
                    HCoreDataStore.Terminals = (from x in _HCoreContext.TUCTerminal
                                                select new OHCoreDataStore.Terminal
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    TerminalId = x.IdentificationNumber,
                                                    DisplayName = x.DisplayName,
                                                    ProviderId = x.ProviderId,
                                                    MerchantId = x.MerchantId,
                                                    StoreId = x.StoreId,
                                                    AcquirerId = x.AcquirerId,

                                                    ApplicationStatusId = x.ActivityStatusId,
                                                    LastTransactionDate = x.LastTransactionDate,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                    CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                    CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                    ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                    ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                }).ToList();
                    #endregion
                    #region Accounts Data Update
                    foreach (var Account in HCoreDataStore.Accounts)
                    {
                        if (Account.AccountTypeId == Helpers.UserAccountType.Merchant)
                        {
                            Account.StoresCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore);
                            Account.CashiersCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantCashier);
                            Account.SubAccountsCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantSubAccount);
                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                        }
                        if (Account.AccountTypeId == Helpers.UserAccountType.Acquirer)
                        {
                            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                            Account.SubAccountsCount = HCoreDataStore.Accounts.Where(x => x.OwnerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                        }
                        if (Account.AccountTypeId == Helpers.UserAccountType.PosAccount)
                        {
                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                        }
                        if (Account.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                        {
                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                        }
                        if (!string.IsNullOrEmpty(Account.IconUrl))
                        {
                            Account.IconUrl = _AppConfig.StorageUrl + Account.IconUrl;
                        }
                        else
                        {
                            Account.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Account.OwnerIconUrl))
                        {
                            Account.OwnerIconUrl = _AppConfig.StorageUrl + Account.OwnerIconUrl;
                        }
                        else
                        {
                            Account.OwnerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Account.SubOwnerIconUrl))
                        {
                            Account.SubOwnerIconUrl = _AppConfig.StorageUrl + Account.SubOwnerIconUrl;
                        }
                        else
                        {
                            Account.SubOwnerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Account.BankIconUrl))
                        {
                            Account.BankIconUrl = _AppConfig.StorageUrl + Account.BankIconUrl;
                        }
                        else
                        {
                            Account.BankIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #endregion
                    #region Terminals Data Update
                    foreach (var Terminal in HCoreDataStore.Terminals)
                    {

                        //Terminal.RmId = _HCoreContext.HCUAccountOwner
                        //.Where(x=> x.OwnerId == Terminal.StoreId 
                        //&& x.Account.BankId == Terminal.AcquirerId
                        //&& x.Account.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager ).Select(x=>x.UserAccountId).FirstOrDefault();

                        Terminal.RmId = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == Terminal.StoreId
                        && x.Account.OwnerId == Terminal.AcquirerId && x.StatusId == HelperStatus.Default.Active
                        && x.Account.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager).Select(x => x.AccountId).FirstOrDefault();


                        //Terminal.RmId = _HCoreContext.HCUAccount
                        //.Where(x=>x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager
                        //&& x.OwnerId == Terminal.AcquirerId
                        //&& x.HCUAccountOwnerAccount.Any(m=> m.Owner.AccountTypeId ==  HCoreConstant.Helpers.UserAccountType.MerchantStore 
                        //&& m.StatusId ==  HelperStatus.Default.Active)).Select(x=>x.Id).FirstOrDefault();




                        // _HCoreContext.HCUAccount
                        //.Where(x=>x.Id == Terminal.ReferenceId
                        //&& x.BankId == Terminal.AcquirerId
                        //&& x.SubOwner.HCUAccountOwnerOwner.Any(m => m.AccountId == Terminal.ReferenceId 
                        //&& m.StatusId == HelperStatus.Default.Active)



                        //(from x in _HCoreContext.HCUAccount
                        //where x.AccountTypeId == UserAccountType.TerminalAccount
                        //&& x.BankId == UserAccountType.BankId
                        //&& x.SubOwner.HCUAccountOwnerOwner.Any(m => m.AccountId == Terminal.ReferenceId && m.StatusId == HelperStatus.Default.Active)



                        Terminal.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == Terminal.ReferenceId).OrderByDescending(x => x.Id).Select(x => x.TransactionDate).FirstOrDefault();
                        //Terminal.RmId  = _HCoreContext.HCUAccountOwner.Where(x=>x.OwnerId == Terminal.StoreId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager && x.Account.BankId == Terminal.AcquirerId).Select(x=>x.UserAccountId).FirstOrDefault();
                        var ProviderDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.ProviderId).FirstOrDefault();
                        if (ProviderDetails != null)
                        {
                            Terminal.ProviderKey = ProviderDetails.ReferenceKey;
                            if (!string.IsNullOrEmpty(ProviderDetails.IconUrl))
                            {
                                Terminal.ProviderIconUrl = ProviderDetails.IconUrl;
                            }
                            Terminal.ProviderDisplayName = ProviderDetails.DisplayName;
                            Terminal.ProviderStatusId = ProviderDetails.StatusId;
                        }

                        var MerchantDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault();
                        if (MerchantDetails != null)
                        {
                            Terminal.MerchantKey = MerchantDetails.ReferenceKey;
                            if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                            {
                                Terminal.MerchantIconUrl = MerchantDetails.IconUrl;
                            }
                            Terminal.MerchantDisplayName = MerchantDetails.DisplayName;
                            Terminal.MerchantStatusId = MerchantDetails.StatusId;
                        }

                        var StoreDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.StoreId).FirstOrDefault();
                        if (StoreDetails != null)
                        {
                            Terminal.StoreKey = StoreDetails.ReferenceKey;
                            if (!string.IsNullOrEmpty(StoreDetails.IconUrl))
                            {
                                Terminal.StoreIconUrl = StoreDetails.IconUrl;
                            }
                            Terminal.StoreDisplayName = StoreDetails.DisplayName;
                            Terminal.StoreStatusId = StoreDetails.StatusId;
                        }
                        var AcquirerDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.AcquirerId).FirstOrDefault();
                        if (AcquirerDetails != null)
                        {
                            Terminal.AcquirerKey = AcquirerDetails.ReferenceKey;
                            if (!string.IsNullOrEmpty(AcquirerDetails.IconUrl))
                            {
                                Terminal.AcquirerIconUrl = AcquirerDetails.IconUrl;
                            }
                            Terminal.AcquirerDisplayName = AcquirerDetails.DisplayName;
                            Terminal.AcquirerStatusId = AcquirerDetails.StatusId;
                        }
                        var RmDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.RmId).FirstOrDefault();
                        if (RmDetails != null)
                        {
                            Terminal.RmKey = RmDetails.ReferenceKey;
                            Terminal.RmDisplayName = RmDetails.DisplayName;
                        }
                    }
                    #endregion
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("LoadDataStore", ex);
            }
        }
        internal static int RefreshAccount(long UserAccountId)
        {
            try
            {
                #region Accounts
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    OHCoreDataStore.Account AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccountId
                    && (x.AccountTypeId == Helpers.UserAccountType.Acquirer
                                              || x.AccountTypeId == Helpers.UserAccountType.PgAccount
                                              || x.AccountTypeId == Helpers.UserAccountType.PosAccount
                                              || x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                              || x.AccountTypeId == Helpers.UserAccountType.Merchant
                                              || x.AccountTypeId == Helpers.UserAccountType.MerchantCashier
                                              || x.AccountTypeId == Helpers.UserAccountType.AcquirerSubAccount
                                              || x.AccountTypeId == Helpers.UserAccountType.MerchantSubAccount))
                                              .Select(x => new OHCoreDataStore.Account
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  UserName = x.User.Username,

                                                  DisplayName = x.DisplayName,
                                                  Name = x.Name,
                                                  FirstName = x.FirstName,
                                                  LastName = x.LastName,

                                                  EmailAddress = x.EmailAddress,
                                                  SecondaryEmailAddress = x.SecondaryEmailAddress,

                                                  ContactNumber = x.ContactNumber,
                                                  MobileNumber = x.MobileNumber,

                                                  Address = x.Address,
                                                  Latitude = x.Latitude,
                                                  Longitude = x.Longitude,

                                                  AccountCode = x.AccountCode,

                                                  IconUrl = x.IconStorage.Path,
                                                  PosterUrl = x.PosterStorage.Path,

                                                  Description = x.Description,


                                                  AccountTypeId = x.AccountTypeId,
                                                  AccountTypeCode = x.AccountType.SystemName,
                                                  AccountTypeName = x.AccountType.Name,

                                                  RoleId = x.RoleId,
                                                  RoleKey = x.Role.Guid,
                                                  RoleName = x.Role.Name,


                                                  LastLoginDate = x.LastLoginDate,
                                                  LastActivityDate = null,

                                                  CreateDate = x.CreateDate,
                                                  CreatedById = x.CreatedById,
                                                  CreatedByKey = x.CreatedBy.Guid,
                                                  CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                  CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                  CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                  CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                  ModifyDate = x.ModifyDate,
                                                  ModifyById = x.ModifyById,
                                                  ModifyByKey = x.ModifyBy.Guid,
                                                  ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                  ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                  ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                  ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,

                                                  OwnerId = x.OwnerId,
                                                  OwnerKey = x.Owner.Guid,
                                                  OwnerDisplayName = x.Owner.DisplayName,
                                                  OwnerIconUrl = x.Owner.IconStorage.Path,
                                                  OwnerAccountTypeId = x.Owner.AccountTypeId,

                                                  SubOwnerId = x.SubOwnerId,
                                                  SubOwnerKey = x.SubOwner.Guid,
                                                  SubOwnerDisplayName = x.SubOwner.DisplayName,
                                                  SubOwnerIconUrl = x.SubOwner.IconStorage.Path,

                                                  BankId = x.BankId,
                                                  BankKey = x.Bank.Guid,
                                                  BankDisplayName = x.Bank.DisplayName,
                                                  BankIconUrl = x.Bank.IconStorage.Path,

                                                  MerchantsCount = 0,
                                                  StoresCount = 0,
                                                  AcquirersCount = 0,
                                                  TerminalsCount = 0,
                                                  CashiersCount = 0,
                                                  PgAccountsCount = 0,
                                                  PosAccountsCount = 0,
                                                  SubAccountsCount = 0,

                                              }).FirstOrDefault();

                    if (AccountDetails != null)
                    {
                        var StoreAccountDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == AccountDetails.ReferenceId).FirstOrDefault();
                        if (StoreAccountDetails != null)
                        {
                            HCoreDataStore.Accounts.Remove(StoreAccountDetails);
                            HCoreDataStore.Accounts.Add(AccountDetails);
                        }
                        else
                        {
                            HCoreDataStore.Accounts.Add(AccountDetails);
                        }
                        UpdateAccountStatistics(AccountDetails.ReferenceId);
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("RefreshAccount", ex);
                return 0;
            }
        }
        internal static int RefreshTerminal(long TerminalId)
        {
            try
            {
                #region Accounts
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    OHCoreDataStore.Terminal TerminalDetails = _HCoreContext.TUCTerminal.Where(x => x.Id == TerminalId)
                                              .Select(x => new OHCoreDataStore.Terminal
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  TerminalId = x.IdentificationNumber,
                                                  DisplayName = x.DisplayName,
                                                  ProviderId = x.ProviderId,
                                                  MerchantId = x.MerchantId,
                                                  StoreId = x.StoreId,
                                                  AcquirerId = x.AcquirerId,

                                                  CreateDate = x.CreateDate,
                                                  CreatedById = x.CreatedById,
                                                  CreatedByKey = x.CreatedBy.Guid,
                                                  CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                  CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                  CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                  CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                  ModifyDate = x.ModifyDate,
                                                  ModifyById = x.ModifyById,
                                                  ModifyByKey = x.ModifyBy.Guid,
                                                  ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                  ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                  ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                  ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,

                                              }).FirstOrDefault();

                    if (TerminalDetails != null)
                    {
                        TerminalDetails.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == TerminalDetails.ReferenceId).OrderByDescending(x => x.Id).Select(x => x.TransactionDate).FirstOrDefault();
                        TerminalDetails.RmId = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == TerminalDetails.StoreId
                       && x.Account.OwnerId == TerminalDetails.AcquirerId && x.StatusId == HelperStatus.Default.Active
                       && x.Account.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager).Select(x => x.AccountId).FirstOrDefault();

                        var StoreTerminalDetails = HCoreDataStore.Terminals.Where(x => x.ReferenceId == TerminalDetails.ReferenceId).FirstOrDefault();
                        if (StoreTerminalDetails != null)
                        {
                            HCoreDataStore.Terminals.Remove(StoreTerminalDetails);
                            HCoreDataStore.Terminals.Add(TerminalDetails);
                        }
                        else
                        {
                            HCoreDataStore.Terminals.Add(TerminalDetails);
                        }
                        UpdateTerminalStatistics(TerminalDetails.ReferenceId);
                        if (TerminalDetails.ProviderId != null)
                        {
                            UpdateAccountStatistics((long)TerminalDetails.ProviderId);
                        }
                        if (TerminalDetails.MerchantId != null)
                        {
                            UpdateAccountStatistics((long)TerminalDetails.StoreId);
                        }
                        if (TerminalDetails.StoreId != null)
                        {
                            UpdateAccountStatistics((long)TerminalDetails.StoreId);
                        }
                        if (TerminalDetails.AcquirerId != null)
                        {
                            UpdateAccountStatistics((long)TerminalDetails.AcquirerId);
                        }
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("RefreshTerminal", ex);
                return 0;
            }
        }
        private static void UpdateTerminalStatistics(long TerminalId)
        {
            var Terminal = HCoreDataStore.Terminals.Where(x => x.ReferenceId == TerminalId).FirstOrDefault();
            #region Terminals Data Update
            var ProviderDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.ProviderId).FirstOrDefault();
            if (ProviderDetails != null)
            {
                Terminal.ProviderKey = ProviderDetails.ReferenceKey;
                if (!string.IsNullOrEmpty(ProviderDetails.IconUrl))
                {
                    Terminal.ProviderIconUrl = ProviderDetails.IconUrl;
                }
                Terminal.ProviderDisplayName = ProviderDetails.DisplayName;
                Terminal.ProviderStatusId = ProviderDetails.StatusId;
            }

            var MerchantDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault();
            if (MerchantDetails != null)
            {
                Terminal.MerchantKey = MerchantDetails.ReferenceKey;
                if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                {
                    Terminal.MerchantIconUrl = MerchantDetails.IconUrl;
                }
                Terminal.MerchantDisplayName = MerchantDetails.DisplayName;
                Terminal.MerchantStatusId = MerchantDetails.StatusId;
            }

            var StoreDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.StoreId).FirstOrDefault();
            if (StoreDetails != null)
            {
                Terminal.StoreKey = StoreDetails.ReferenceKey;
                if (!string.IsNullOrEmpty(StoreDetails.IconUrl))
                {
                    Terminal.StoreIconUrl = StoreDetails.IconUrl;
                }
                Terminal.StoreDisplayName = StoreDetails.DisplayName;
                Terminal.StoreStatusId = StoreDetails.StatusId;
            }
            var AcquirerDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.AcquirerId).FirstOrDefault();
            if (AcquirerDetails != null)
            {
                Terminal.AcquirerKey = AcquirerDetails.ReferenceKey;
                if (!string.IsNullOrEmpty(AcquirerDetails.IconUrl))
                {
                    Terminal.AcquirerIconUrl = AcquirerDetails.IconUrl;
                }
                Terminal.AcquirerDisplayName = AcquirerDetails.DisplayName;
                Terminal.AcquirerStatusId = AcquirerDetails.StatusId;
            }
            var RmDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.RmId).FirstOrDefault();
            if (RmDetails != null)
            {
                Terminal.RmKey = RmDetails.ReferenceKey;
                Terminal.RmDisplayName = RmDetails.DisplayName;
            }
            #endregion

        }
        private static void UpdateAccountStatistics(long AccountId)
        {
            var Account = HCoreDataStore.Accounts.Where(x => x.ReferenceId == AccountId).FirstOrDefault();
            #region Accounts Data Update
            if (Account.AccountTypeId == Helpers.UserAccountType.Merchant)
            {
                Account.StoresCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore);
                Account.CashiersCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantCashier);
                Account.SubAccountsCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantSubAccount);
                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
            }
            if (Account.AccountTypeId == Helpers.UserAccountType.Acquirer)
            {
                Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                Account.SubAccountsCount = HCoreDataStore.Accounts.Where(x => x.OwnerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
            }
            if (Account.AccountTypeId == Helpers.UserAccountType.PosAccount)
            {
                Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
            }
            if (Account.AccountTypeId == Helpers.UserAccountType.MerchantStore)
            {
                Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
            }
            if (!string.IsNullOrEmpty(Account.IconUrl))
            {
                Account.IconUrl = _AppConfig.StorageUrl + Account.IconUrl;
            }
            else
            {
                Account.IconUrl = _AppConfig.Default_Icon;
            }
            if (!string.IsNullOrEmpty(Account.OwnerIconUrl))
            {
                Account.OwnerIconUrl = _AppConfig.StorageUrl + Account.OwnerIconUrl;
            }
            else
            {
                Account.OwnerIconUrl = _AppConfig.Default_Icon;
            }
            if (!string.IsNullOrEmpty(Account.SubOwnerIconUrl))
            {
                Account.SubOwnerIconUrl = _AppConfig.StorageUrl + Account.SubOwnerIconUrl;
            }
            else
            {
                Account.SubOwnerIconUrl = _AppConfig.Default_Icon;
            }
            if (!string.IsNullOrEmpty(Account.BankIconUrl))
            {
                Account.BankIconUrl = _AppConfig.StorageUrl + Account.BankIconUrl;
            }
            else
            {
                Account.BankIconUrl = _AppConfig.Default_Icon;
            }
            #endregion

        }
        internal static void RefreshStore(long StoreId)
        {
            RefreshAccount(StoreId);
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                var StoreTerminals = _HCoreContext.TUCTerminal
                    .Where(x => x.StoreId == StoreId)
                    .Select(x => new
                    {
                        TerminalId = x.Id,
                    }).ToList();
                foreach (var StoreTerminal in StoreTerminals)
                {
                    RefreshTerminal(StoreTerminal.TerminalId);
                }
            }
        }
    }

    public class ActorDataStoreStart : ReceiveActor
    {
        public ActorDataStoreStart()
        {
            Receive<int>(_RequestContent =>
            {
                CoreDataStoreOperation.LoadDataStore();
            });
        }
    }
}
