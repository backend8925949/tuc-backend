//==================================================================================
// FileName: HCoreDataStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Data.Store
{
    public static class HCoreDataStore
    {
        internal static DateTime SyncTime = HCoreHelper.GetGMTDateTime();
        public static List<OHCoreDataStore.Helper> SystemHelpers = new List<OHCoreDataStore.Helper>();
        public static List<OHCoreDataStore.BinParameter> SystemBinParameters = new List<OHCoreDataStore.BinParameter>();
        public static List<OHCoreDataStore.Configuration> Configurations = new List<OHCoreDataStore.Configuration>();
        public static List<OHCoreDataStore.AccountConfiguration> AccountConfigurations = new List<OHCoreDataStore.AccountConfiguration>();
        public static List<OHCoreDataStore.Terminal> Terminals = new List<OHCoreDataStore.Terminal>();
        public static List<OHCoreDataStore.Account> Accounts = new List<OHCoreDataStore.Account>();
    }
}
