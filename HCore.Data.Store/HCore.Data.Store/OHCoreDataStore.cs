//==================================================================================
// FileName: OHCoreDataStore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Data.Store
{
    public class OHCoreDataStore
    {

        public class Helper
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? TypeName { get; set; }

            public long? ParentId { get; set; }
            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }


            public long? SubParentId { get; set; }
            public string? SubParentName { get; set; }
            public string? SubParentCode { get; set; }


            public long HelperId { get; set; }
            public string? HelperCode { get; set; }
            public string? HelperKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? Description { get; set; }
            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class BinParameter
        {
            public long ReferenceId { get; set; }

            public long TypeId { get; set; }
            public string? TypeName { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
        }
        public class Configuration
        {
            public long ReferenceId { get; set; }

            public long? HelperId { get; set; }
            public string? HelperCode { get; set; }
            public string? HelperKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? Description { get; set; }
            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public long? StatusId { get; set; }
        }
        public class AccountConfiguration
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long UserAccountId { get; set; }
            public long ConfigurationId { get; set; }
            public string? Value { get; set; }
        }



        public class Terminal
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? TerminalId { get; set; }


            public long? ApplicationStatusId { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public long? MerchantStatusId { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? StoreIconUrl { get; set; }
            public long? StoreStatusId { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public long? ProviderStatusId { get; set; }

            public long? AcquirerId { get; set; }
            public string? AcquirerKey { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerIconUrl { get; set; }
            public long? AcquirerStatusId { get; set; }

            public long? RmId { get; set; }
            public string? RmKey { get; set; }
            public string? RmDisplayName { get; set; }


            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public string? Address { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public long? CreatedByAccountTypeId { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }
            public string? CreatedByAccountTypeName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public long? ModifyByAccountTypeId { get; set; }
            public string? ModifyByAccountTypeCode { get; set; }
            public string? ModifyByAccountTypeName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }

            public long? TodayTotalTransaction { get; set; }
            public double? TodayTotalInvoiceAmount { get; set; }
            public DateTime? LastTransactionDate { get; set; }
        }



        public class Account
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? UserName { get; set; }

            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? SecondaryEmailAddress { get; set; }
            public string? ContactNumber { get; set; }
            public string? MobileNumber { get; set; }
            public string? AccountCode { get; set; }
            public string? Address { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public string? Description { get; set; }

            public long? AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }

            public long? RoleId { get; set; }
            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }

            public DateTime? LastLoginDate { get; set; }
            public DateTime? LastActivityDate { get; set; }


            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public long? CreatedByAccountTypeId { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }
            public string? CreatedByAccountTypeName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public long? ModifyByAccountTypeId { get; set; }
            public string? ModifyByAccountTypeCode { get; set; }
            public string? ModifyByAccountTypeName { get; set; }


            public long? StatusId { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }


            public long? OwnerId { get; set; }
            public long? OwnerAccountTypeId { get; set; }
            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }
            public string? OwnerIconUrl { get; set; }

            public long? SubOwnerId { get; set; }
            public string? SubOwnerKey { get; set; }
            public string? SubOwnerDisplayName { get; set; }
            public string? SubOwnerIconUrl { get; set; }

            public long? BankId { get; set; }
            public string? BankKey { get; set; }
            public string? BankDisplayName { get; set; }
            public string? BankIconUrl { get; set; }

            public long? MerchantsCount { get; set; }
            public long? StoresCount { get; set; }
            public long? AcquirersCount { get; set; }
            public long? TerminalsCount { get; set; }
            public long? CashiersCount { get; set; }
            public long? PgAccountsCount { get; set; }
            public long? PosAccountsCount { get; set; }
            public long? SubAccountsCount { get; set; }

            public DateTime? LastTransactionDate { get; set; }

        }



    }
}
