//==================================================================================
// FileName: HCoreDataStoreManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.Data.Store.Core;

namespace HCore.Data.Store
{
    public static class HCoreDataStoreManager
    {
        public static void DataStore_Start()
        {
            var system = ActorSystem.Create("ActorDataStoreStart");
            var greeter = system.ActorOf<ActorDataStoreStart>("ActorDataStoreStart");
            greeter.Tell(1);
            //CoreDataStoreOperation.LoadDataStore();
        }
        public static void DataStore_Sync()
        {
            CoreDataStoreOperation.SyncDataStore();
        }
        public static void DataStore_Reset()
        {
            CoreDataStoreOperation.LoadDataStore();
        }
        public static void DataStore_RefreshStore(long StoreId)
        {
            CoreDataStoreOperation.RefreshStore(StoreId);
        }
    }
}
