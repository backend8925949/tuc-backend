//==================================================================================
// FileName: TUCPluginResource.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace HCore.TUC.Plugins.Resource
{
    internal class TUCPluginResource
    {
        internal const string HCP0500 = "HCP0500";
        internal const string HCP0500M = "Internal server error occured. Please try after some time";

        internal const string HCP0200 = "HCP0200";
        internal const string HCP0200M = "Details loaded";

        internal const string HCP0404 = "HCP0404";
        internal const string HCP0404M = "Details not found. It might be removed or not available at the moment";

        internal const string HCP0201 = "HCP0201";
        internal const string HCP0201M = "Details updated successfully";

        internal const string HCPREF = "HCPREF";
        internal const string HCPREFM = "Reference id missing";

        internal const string HCPREFKEY = "HCPREFKEY";
        internal const string HCPREFKEYM = "Reference key missing";

        internal const string HCPACCREF = "ACCREF";
        internal const string HCPACCREFM = "Account reference missing";

        internal const string HCPACCREFKEY = "HCPACCREFKEY";
        internal const string HCPACCREFKEYM = "Account reference key missing";

        internal const string HCPSTATUS = "HCPSTATUS";
        internal const string HCPSTATUSM = "Status code required";
        internal const string HCPINSTATUS = "HCPINSTATUS";
        internal const string HCPINSTATUSM = "Invalid status code";

        internal const string HCP0100 = "HCP0100";
        internal const string HCP0100M = "Amount must be greater than 0";

        internal const string HCP0101 = "HCP0101";
        internal const string HCP0101M = "Source code missing";
        internal const string HCP0102 = "HCP0102";
        internal const string HCP0102M = "Payment reference missing";
        internal const string HCP0103 = "HCP0103";
        internal const string HCP0103M = "Account is not active. Please contact support to activate account";
        internal const string HCP0104 = "HCP0104";
        internal const string HCP0104M = "Feature is not available for account. Please contact support for assistance";
        internal const string HCP0105 = "HCP0105";
        internal const string HCP0105M = "Unable to process your payment request. Please contact support to credit amount";
        internal const string HCP0106 = "HCP0106";
        internal const string HCP0106M = "Amount credited to your account";
        internal const string HCP0107 = "HCP0107";
        internal const string HCP0107M = "Balance source missing";
        internal const string HCP0108 = "HCP0108";
        internal const string HCP0108M = "Invalid balance source";
        internal const string HCP0109 = "HCP0109";
        internal const string HCP0109M = "Balance loaded";
        internal const string HCP0110 = "HCP0110";
        internal const string HCP0110M = "Amount must be less than 100000";

        internal const string HCP0111 = "HCP0111";
        internal const string HCP0111M = "Customer information required for creating gift card";
        internal const string HCP0112 = "HCP0112";
        internal const string HCP0112M = "Customer account required for creating gift card";
        internal const string HCP0113 = "HCP0113";
        internal const string HCP0113M = "Account balance low. Topup your gift card wallet to create gift cards";
        internal const string HCP0114 = "HCP0114";
        internal const string HCP0114M = "Account is not active. Please contact support to activate your account";
        internal const string HCP0115 = "HCP0115";
        internal const string HCP0115M = "Customer not registered. Download app or do transactions at partnered merchant stores";
        internal const string HCP0116 = "HCP0116";
        internal const string HCP0116M = "Customer account is susupended or blocked. Please contact support to activate account";
        internal const string HCP0117 = "HCP0117";
        internal const string HCP0117M = "Unable to generate gift card. ";
        internal const string HCP0118 = "HCP0118";
        internal const string HCP0118M = "Payment reference required";
        internal const string HCP0119 = "HCP0119";
        internal const string HCP0119M = "Operation failed. Payment already processed.";
        internal const string HCP0120 = "HCP0120";
        internal const string HCP0120M = "Bill payment cancelled successfully";

        internal const string HCP0121 = "HCP0121";
        internal const string HCP0121M = "Account number required";
        internal const string HCP0122 = "HCP0122";
        internal const string HCP0122M = "Amount required";
        internal const string HCP0123 = "HCP0123";
        internal const string HCP0123M = "Topup not available for selected service at the moment. Please try after some time";
        internal const string HCP0124 = "HCP0124";
        internal const string HCP0124M = "Purchase Initialized";
        internal const string HCP0125 = "HCP0125";
        internal const string HCP0125M = "Payment reference required";
        internal const string HCP0126 = "HCP0126";
        internal const string HCP0126M = "Payment status loaded";
        internal const string HCP0127 = "HCP0127";
        internal const string HCP0127M = "Details updated successfully";
        internal const string HCP0128 = "HCP0128";
        internal const string HCP0128M = "Your account balance is low. Please topup your account and try again.";
        internal const string HCP0129 = "HCP0129";
        internal const string HCP0129M = "Operation failed. Your wallet balance is low. Please topup your account and try again";
        internal const string HCP0130 = "HCP0130";
        internal const string HCP0130M = "Your account is suspended or blocked. Please contact support to activate account";



        internal const string HCP0131 = "HCP0131";
        internal const string HCP0131M = "Operation failed. ";
        internal const string HCP0132 = "HCP0132";
        internal const string HCP0132M = "Unable to initialize payment process. Please try after some time";
        internal const string HCP0133 = "HCP0133";
        internal const string HCP0133M = "Transaction must be processed for reveral";
        internal const string HCP0134 = "HCP0134";
        internal const string HCP0134M = "Details not found";
        internal const string HCP0135 = "HCP0135";
        internal const string HCP0135M = "Transaction reversed successfully";
        internal const string HCP0136 = "HCP0136";
        internal const string HCP0136M = "Invalid amount. Please enter {{amount}} and proceed to make payment";
        internal const string HCP0137 = "HCP0137";
        internal const string HCP0137M = "Invalid amount. Please enter {{amount}} and proceed to make payment";
        internal const string HCP0138 = "HCP0138";
        internal const string HCP0138M = "Your daily limit for airtime purchase is reached. Please try after sometime";
        internal const string HCP0139 = "HCP0139";
        internal const string HCP0139M = "Your daily limit for bill payments is reached. Please try after sometime";
        internal const string HCP0140 = "HCP0140";
        internal const string HCP0140M = "xxxxxxxxxxxxxx";

        internal const string HCP0141 = "HCP0141";
        internal const string HCP0141M = "Deal id required";
        internal const string HCP0142 = "HCP0142";
        internal const string HCP0142M = "Deal key required";
        internal const string HCP0143 = "HCP0143";
        internal const string HCP0143M = "Invalid deal id";
        internal const string HCP0144 = "HCP0144";
        internal const string HCP0144M = "xxxxxxxxxxxxxx";
        internal const string HCP0145 = "HCP0145";
        internal const string HCP0145M = "xxxxxxxxxxxxxx";
        internal const string HCP0146 = "HCP0146";
        internal const string HCP0146M = "xxxxxxxxxxxxxx";
        internal const string HCP0147 = "HCP0147";
        internal const string HCP0147M = "xxxxxxxxxxxxxx";
        internal const string HCP0148 = "HCP0148";
        internal const string HCP0148M = "xxxxx9xxxxxxxx";
        internal const string HCP0149 = "HCP0149";
        internal const string HCP0149M = "xxxxxxxxxxxxxx";
        internal const string HCP0150 = "HCP0140";
        internal const string HCP0150M = "xxxxxxxxxxxxxx";

        internal const string HCP0151 = "HCP0151";
        internal const string HCP0151M = "xxxxxxxxxxxxxx";
        internal const string HCP0152 = "HCP0152";
        internal const string HCP0152M = "xxxxxxxxxxxxxx";
        internal const string HCP0153 = "HCP0153";
        internal const string HCP0153M = "xxxxxxxxxxxxx";
        internal const string HCP0154 = "HCP0154";
        internal const string HCP0154M = "xxxxxxxxxxxxxx";
        internal const string HCP0155 = "HCP0155";
        internal const string HCP0155M = "xxxxxxxxxxxxxx";
        internal const string HCP0156 = "HCP0156";
        internal const string HCP0156M = "xxxxxxxxxxxxxx";
        internal const string HCP0157 = "HCP0157";
        internal const string HCP0157M = "xxxxxxxxxxxxxx";
        internal const string HCP0158 = "HCP0158";
        internal const string HCP0158M = "xxxxx9xxxxxxxx";
        internal const string HCP0159 = "HCP0159";
        internal const string HCP0159M = "xxxxxxxxxxxxxx";
        internal const string HCP0160 = "HCP0160";
        internal const string HCP0160M = "xxxxxxxxxxxxxx";


        internal const string HCP0161 = "HCP0161";
        internal const string HCP0161M = "xxxxxxxxxxxxxx";
        internal const string HCP0162 = "HCP0162";
        internal const string HCP0162M = "xxxxxxxxxxxxxx";
        internal const string HCP0163 = "HCP0163";
        internal const string HCP0163M = "xxxxxxxxxxxxx";
        internal const string HCP0164 = "HCP0164";
        internal const string HCP0164M = "xxxxxxxxxxxxxx";
        internal const string HCP0165 = "HCP0165";
        internal const string HCP0165M = "xxxxxxxxxxxxxx";
        internal const string HCP0166 = "HCP0166";
        internal const string HCP0166M = "xxxxxxxxxxxxxx";
        internal const string HCP0167 = "HCP0167";
        internal const string HCP0167M = "xxxxxxxxxxxxxx";
        internal const string HCP0168 = "HCP0168";
        internal const string HCP0168M = "xxxxx9xxxxxxxx";
        internal const string HCP0169 = "HCP0169";
        internal const string HCP0169M = "xxxxxxxxxxxxxx";
        internal const string HCP0170 = "HCP0170";
        internal const string HCP0170M = "xxxxxxxxxxxxxx";

        internal const string HCP0171 = "HCP0171";
        internal const string HCP0171M = "xxxxxxxxxxxxxx";
        internal const string HCP0172 = "HCP0172";
        internal const string HCP0172M = "xxxxxxxxxxxxxx";
        internal const string HCP0173 = "HCP0173";
        internal const string HCP0173M = "xxxxxxxxxxxxx";
        internal const string HCP0174 = "HCP0174";
        internal const string HCP0174M = "xxxxxxxxxxxxxx";
        internal const string HCP0175 = "HCP0175";
        internal const string HCP0175M = "xxxxxxxxxxxxxx";
        internal const string HCP0176 = "HCP0176";
        internal const string HCP0176M = "xxxxxxxxxxxxxx";
        internal const string HCP0177 = "HCP0177";
        internal const string HCP0177M = "xxxxxxxxxxxxxx";
        internal const string HCP0178 = "HCP0178";
        internal const string HCP0178M = "xxxxx9xxxxxxxx";
        internal const string HCP0179 = "HCP0179";
        internal const string HCP0179M = "xxxxxxxxxxxxxx";
        internal const string HCP0180 = "HCP0180";
        internal const string HCP0180M = "xxxxxxxxxxxxxx";

        internal const string HCP0181 = "HCP0181";
        internal const string HCP0181M = "xxxxxxxxxxxxxx";
        internal const string HCP0182 = "HCP0182";
        internal const string HCP0182M = "xxxxxxxxxxxxxx";
        internal const string HCP0183 = "HCP0183";
        internal const string HCP0183M = "xxxxxxxxxxxxx";
        internal const string HCP0184 = "HCP0184";
        internal const string HCP0184M = "xxxxxxxxxxxxxx";
        internal const string HCP0185 = "HCP0185";
        internal const string HCP0185M = "xxxxxxxxxxxxxx";
        internal const string HCP0186 = "HCP0186";
        internal const string HCP0186M = "xxxxxxxxxxxxxx";
        internal const string HCP0187 = "HCP0187";
        internal const string HCP0187M = "xxxxxxxxxxxxxx";
        internal const string HCP0188 = "HCP0188";
        internal const string HCP0188M = "xxxxx9xxxxxxxx";
        internal const string HCP0189 = "HCP0189";
        internal const string HCP0189M = "xxxxxxxxxxxxxx";
        internal const string HCP0190 = "HCP0190";
        internal const string HCP0190M = "xxxxxxxxxxxxxx";

        internal const string HCP0191 = "HCP0191";
        internal const string HCP0191M = "xxxxxxxxxxxxxx";
        internal const string HCP0192 = "HCP0192";
        internal const string HCP0192M = "xxxxxxxxxxxxxx";
        internal const string HCP0193 = "HCP0193";
        internal const string HCP0193M = "xxxxxxxxxxxxx";
        internal const string HCP0194 = "HCP0194";
        internal const string HCP0194M = "xxxxxxxxxxxxxx";
        internal const string HCP0195 = "HCP0195";
        internal const string HCP0195M = "xxxxxxxxxxxxxx";
        internal const string HCP0196 = "HCP0196";
        internal const string HCP0196M = "xxxxxxxxxxxxxx";
        internal const string HCP0197 = "HCP0197";
        internal const string HCP0197M = "xxxxxxxxxxxxxx";
        internal const string HCP0198 = "HCP0198";
        internal const string HCP0198M = "xxxxx9xxxxxxxx";
        internal const string HCP0199 = "HCP0199";
        internal const string HCP0199M = "xxxxxxxxxxxxxx";


        internal const string HCP0202 = "HCP0202";
        internal const string HCP0202M = "xxxxxxxxxxxxxx";
        internal const string HCP0203 = "HCP0203";
        internal const string HCP0203M = "xxxxxxxxxxxxxx";
        internal const string HCP0204 = "HCP0204";
        internal const string HCP0204M = "xxxxxxxxxxxxxx";
        internal const string HCP0205 = "HCP0205";
        internal const string HCP0205M = "xxxxxxxxxxxxxx";
        internal const string HCP0206 = "HCP0206";
        internal const string HCP0206M = "xxxxxxxxxxxxxx";
        internal const string HCP0207 = "HCP0207";
        internal const string HCP0207M = "xxxxxxxxxxxxxx";
        internal const string HCP0208 = "HCP0208";
        internal const string HCP0208M = "xxxxxxxxxxxxxx";
        internal const string HCP0209 = "HCP0209";
        internal const string HCP0209M = "xxxxxxxxxxxxxx";
        internal const string HCP0210 = "HCP0210";
        internal const string HCP0210M = "xxxxxxxxxxxxxx";

        internal const string HCP0211 = "HCP0211";
        internal const string HCP0211M = "xxxxxxxxxxxxxx";
        internal const string HCP0212 = "HCP0212";
        internal const string HCP0212M = "xxxxxxxxxxxxxx";
        internal const string HCP0213 = "HCP0213";
        internal const string HCP0213M = "xxxxxxxxxxxxxx";
        internal const string HCP0214 = "HCP0214";
        internal const string HCP0214M = "xxxxxxxxxxxxxx";
        internal const string HCP0215 = "HCP0215";
        internal const string HCP0215M = "xxxxxxxxxxxxxx";
        internal const string HCP0216 = "HCP0216";
        internal const string HCP0216M = "xxxxxxxxxxxxxx";
        internal const string HCP0217 = "HCP0217";
        internal const string HCP0217M = "xxxxxxxxxxxxxx";
        internal const string HCP0218 = "HCP0218";
        internal const string HCP0218M = "xxxxxxxxxxxxxx";
        internal const string HCP0219 = "HCP0219";
        internal const string HCP0219M = "xxxxxxxxxxxxxx";
        internal const string HCP0220 = "HCP0220";
        internal const string HCP0220M = "xxxxxxxxxxxxxx";

        internal const string HCP0221 = "HCP0221";
        internal const string HCP0221M = "xxxxxxxxxxxxxx";
        internal const string HCP0222 = "HCP0222";
        internal const string HCP0222M = "xxxxxxxxxxxxxx";
        internal const string HCP0223 = "HCP0223";
        internal const string HCP0223M = "xxxxxxxxxxxxxx";
        internal const string HCP0224 = "HCP0224";
        internal const string HCP0224M = "xxxxxxxxxxxxxx";
        internal const string HCP0225 = "HCP0225";
        internal const string HCP0225M = "xxxxxxxxxxxxxx";
        internal const string HCP0226 = "HCP0226";
        internal const string HCP0226M = "xxxxxxxxxxxxxx";
        internal const string HCP0227 = "HCP0227";
        internal const string HCP0227M = "xxxxxxxxxxxxxx";
        internal const string HCP0228 = "HCP0228";
        internal const string HCP0228M = "xxxxxxxxxxxxxx";
        internal const string HCP0229 = "HCP0229";
        internal const string HCP0229M = "xxxxxxxxxxxxxx";
        internal const string HCP0230 = "HCP0230";
        internal const string HCP0230M = "xxxxxxxxxxxxxx";

        internal const string HCP0231 = "HCP0231";
        internal const string HCP0231M = "Sender information required for creating gift card";
        internal const string HCP0232 = "HCP0232";
        internal const string HCP0232M = "Receiver information required for creating gift card";
        internal const string HCP0233 = "HCP0233";
        internal const string HCP0233M = "Total sender Amount is not matching with receiver's amount";
        internal const string HCP0234 = "HCP0234";
        internal const string HCP0234M = "Deal count cannot be cancelled as prder has been shipped or delivered!";

    }
}
