//==================================================================================
// FileName: ManageParcel.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManageParcel
    {
        FrameworkParcel _FrameworkParcel;

        /// <summary>
        /// Description: Saves the parcel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveParcel(OParcel.Save _Request)
        {
            _FrameworkParcel = new FrameworkParcel();
            return _FrameworkParcel.SaveParcel(_Request);
        }

        /// <summary>
        /// Description: Gets the parcel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetParcel(OParcel.Details _Request)
        {
            _FrameworkParcel = new FrameworkParcel();
            return _FrameworkParcel.GetParcel(_Request);
        }

        /// <summary>
        /// Description: Gets the parcel list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetParcels(OList.Request _Request)
        {
            _FrameworkParcel = new FrameworkParcel();
            return _FrameworkParcel.GetParcels(_Request);
        }
    }
}

