﻿using System;
using Akka.Actor;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.GiftCards.Object;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManageEmailProcessor
    {
        internal class ActorConfirmOrderNotification : ReceiveActor
        {
            public ActorConfirmOrderNotification()
            {
                Receive<OEmailProcessor.ConfirmOrder>(_Request =>
                {
                    FrameworkEmailProcessor _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                    _FrameworkEmailProcessor.ConfirmOrderEmail(_Request);
                });
            }
        }

        internal class ActorCancelOrderNotification : ReceiveActor
        {
            public ActorCancelOrderNotification()
            {
                Receive<OEmailProcessor.CancelOrder>(_Request =>
                {
                    FrameworkEmailProcessor _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                    _FrameworkEmailProcessor.CancelOrderEmail(_Request);
                });
            }
        }

        internal class ActorProductReviewNotification : ReceiveActor
        {
            public ActorProductReviewNotification()
            {
                Receive<OEmailProcessor.ProductReview>(_Request =>
                {
                    FrameworkEmailProcessor _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                    _FrameworkEmailProcessor.ProductReviewEmail(_Request);
                });
            }
        }

        internal class ActorNewOrderNotification : ReceiveActor
        {
            public ActorNewOrderNotification()
            {
                Receive<OEmailProcessor.OrderNotification>(_Request =>
                {
                    FrameworkEmailProcessor _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                    _FrameworkEmailProcessor.OrderNotificationEmail(_Request);
                });
            }
        }

        internal class ActorDealPurchaseNotification : ReceiveActor
        {
            public ActorDealPurchaseNotification()
            {
                Receive<OEmailProcessor.DealPurchaseNotification>(_Request =>
                {
                    FrameworkEmailProcessor _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                    _FrameworkEmailProcessor.DealPurchaseNotificationEmail(_Request);
                });
            }
        }
    }
}

