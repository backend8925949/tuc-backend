//==================================================================================
// FileName: ManageAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManageAddress
    {
        FrameworkAddress _FrameworkAddress;

        /// <summary>
        /// Description: Saves the address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveAddress(ODeliveryAddress.Save.Request _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.SaveAddress(_Request);
        }

        /// <summary>
        /// Description: Updates the address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateAddress(ODeliveryAddress.Update.Request _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.UpdateAddress(_Request);
        }

        /// <summary>
        /// Description: Gets the address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAddress(OReference _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.GetAddress(_Request);
        }

        /// <summary>
        /// Description: Gets the address list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAddresses(OList.Request _Request)
        {
            _FrameworkAddress = new FrameworkAddress();
            return _FrameworkAddress.GetAddresses(_Request);
        }
    }
}

