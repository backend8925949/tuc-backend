//==================================================================================
// FileName: ManageOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;
using static HCore.TUC.Plugins.Delivery.Objects.OOperations;
using static HCore.TUC.Plugins.Delivery.Objects.OOperations.Pricing;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManageOperations
    {
        FrameworkOperations _FrameworkOperations;
        /// <summary>
        /// Description: Gets the pricing.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPricing(OOperations.Pricing.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetPricing(_Request);
        }
        /// <summary>
        /// Description: Gets the estimated pricing.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetEstimatedPricing(OOperations.Pricing.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetEstimatedPricing(_Request);
        }
        /// <summary>
        /// Description: Gets the shipment pricing.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetShipmentPricing(OOperations.Pricing.Request _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetShipmentPricing(_Request);
        }

        public async Task<OResponse> GetDeliveryPricing(GetDeliveryPricing _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return await _FrameworkOperations.GetDeliveryPricing(_Request);
        }

        public OResponse GetAppDeliveryPricing(GetAppDeliveryPricing _Request)
        {
            _FrameworkOperations = new FrameworkOperations();
            return _FrameworkOperations.GetAppDeliveryPricing(_Request);
        }
    }
}
