﻿using System;
using HCore.Helper;
using HCore.ThankUCash.FrameworkTucPay;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManageOrderProcess
    {
        FrameworkOrderProcess? _FrameworkOrderProcess;

        public async Task<OResponse> ConfirmOrder(OOrderProcess.ConfirmOrder _Request)
        {
            _FrameworkOrderProcess = new FrameworkOrderProcess();
            return await _FrameworkOrderProcess.ConfirmOrders(_Request);
        }

        public async Task<OResponse> CancelOrder(OOrderProcess.CancelOrder _Request)
        {
            _FrameworkOrderProcess = new FrameworkOrderProcess();
            return await _FrameworkOrderProcess.CancelOrder(_Request);
        }

        public async Task<OResponse> UpdateOrderStatus(OOrderProcess.ReadyToPickup _Request)
        {
            _FrameworkOrderProcess = new FrameworkOrderProcess();
            return await _FrameworkOrderProcess.UpdateOrderStatus(_Request);
        }

        public void UpdateOrderStatus(string _Request, string DeliveryPartnerKey)
        {
            _FrameworkOrderProcess = new FrameworkOrderProcess();
            _FrameworkOrderProcess.UpdateOrderStatus(_Request, DeliveryPartnerKey);
        }

        public async Task<OResponse> GetDelvieryInvoice(OOrderProcess.Invoice.Request _Request)
        {
            _FrameworkOrderProcess = new FrameworkOrderProcess();
            return await _FrameworkOrderProcess.GetDelvieryInvoice(_Request);
        }

        public async Task<OResponse> TrackOrder(OOrderProcess.TrackOrder.Request _Request)
        {
            _FrameworkOrderProcess = new FrameworkOrderProcess();
            return await _FrameworkOrderProcess.TrackOrder(_Request);
        }
    }
}

