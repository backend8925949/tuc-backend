//==================================================================================
// FileName: ManageShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManageShipment
    {
        FrameworkShipment? _FrameworkShipment;

        //public OResponse CreateShipment(OShipments.Save.Request _Request)
        //{
        //    _FrameworkShipment = new FrameworkShipment();
        //    return _FrameworkShipment.CreateShipment(_Request);
        //}

        //public OResponse CheckRateForShioment(OShipments.CheckRates _Request)
        //{
        //    _FrameworkShipment = new FrameworkShipment();
        //    return _FrameworkShipment.CheckRateForShipment(_Request);
        //}

        /// <summary>
        /// Description: Gets the rate details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRateDetails(OShipments.CheckRateDetails _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.GetRateDetails(_Request);
        }

        //public OResponse ArrangePickupAndDelivery(OShipments.Update _Request)
        //{
        //    _FrameworkShipment = new FrameworkShipment();
        //    return _FrameworkShipment.ArrangePickupAndDelivery(_Request);
        //}

        /// <summary>
        /// Description: Tracks the shipment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public async Task<OResponse> TrackOrder(OReference _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return await _FrameworkShipment.TrackOrder(_Request);
        }

        /// <summary>
        /// Description: Cancels the shipment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CancelShipment(OReference _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.CancelShipment(_Request);
        }

        /// <summary>
        /// Description: Gets the shipment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetShipment(OReference _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.GetShipment(_Request);
        }

        /// <summary>
        /// Description: Gets the shipment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetShipments(OList.Request _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.GetShipments(_Request);
        }

        /// <summary>
        /// Description: Gets the orders overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOrdersOverview(OReference _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.GetOrdersOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOverview(OReference _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.GetOverview(_Request);
        }

        /// <summary>
        /// Description: Updates the order status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateOrderStatus(OReference _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.UpdateOrderStatus(_Request);
        }

        /// <summary>
        /// Description: Gets the order history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOrderHistory(OShipments.Request _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.GetOrderHistory(_Request);
        }

        /// <summary>
        /// Description: Gets the order list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOrders(OList.Request _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            return _FrameworkShipment.GetOrders(_Request);
        }

        /// <summary>
        /// Description: Updates the orders.
        /// </summary>
        public void UpdateOrders()
        {
            #region Manage Exception
            try
            {
                var system = ActorSystem.Create("ActorManageOrder");
                var greeter = system.ActorOf<ActorManageOrder>("ActorManageOrder");
                greeter.Tell("updateorders");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateOrders", _Exception, null);
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the order status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        public void UpdateOrderStatus(string _Request)
        {
            _FrameworkShipment = new FrameworkShipment();
            _FrameworkShipment.UpdateOrderStatus(_Request);
        }
    }

    internal class ActorManageOrder : ReceiveActor
    {
        public ActorManageOrder()
        {
            Receive<string>(_Request =>
            {
                FrameworkShipment _FrameworkShipment = new FrameworkShipment();
                _FrameworkShipment.UpdateOrders();
            });
        }
    }
}

