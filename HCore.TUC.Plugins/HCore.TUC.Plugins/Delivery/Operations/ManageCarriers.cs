//==================================================================================
// FileName: ManageCarriers.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManageCarriers
    {
        FrameworkCarriers _FrameworkCarriers;

        /// <summary>
        /// Description: Saves the carriers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCarriers(OCarriers.Save.Request _Request)
        {
            _FrameworkCarriers = new FrameworkCarriers();
            return _FrameworkCarriers.SaveCarriers(_Request);
        }

        /// <summary>
        /// Description: Gets the carrirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCarrirer(OReference _Request)
        {
            _FrameworkCarriers = new FrameworkCarriers();
            return _FrameworkCarriers.GetCarrirer(_Request);
        }

        /// <summary>
        /// Description: Gets the carrier list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCarriers(OList.Request _Request)
        {
            _FrameworkCarriers = new FrameworkCarriers();
            return _FrameworkCarriers.GetCarriers(_Request);
        }
    }
}

