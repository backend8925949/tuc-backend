//==================================================================================
// FileName: ManagePackaging.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;

namespace HCore.TUC.Plugins.Delivery.Operations
{
    public class ManagePackaging
    {
        FrameworkPackaging _FrameworkPackaging;

        /// <summary>
        /// Description: Saves the packaging.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SavePackaging(OPackaging.Save.Request _Request)
        {
            _FrameworkPackaging = new FrameworkPackaging();
            return _FrameworkPackaging.SavePackaging(_Request);
        }

        /// <summary>
        /// Description: Gets the packaging.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPackaging(OPackaging.Details _Request)
        {
            _FrameworkPackaging = new FrameworkPackaging();
            return _FrameworkPackaging.GetPackaging(_Request);
        }

        /// <summary>
        /// Description: Gets the packaging list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPackagings(OList.Request _Request)
        {
            _FrameworkPackaging = new FrameworkPackaging();
            return _FrameworkPackaging.GetPackagings(_Request);
        }

        /// <summary>
        /// Description: Gets the system packaging.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSystemPackaging(OList.Request _Request)
        {
            _FrameworkPackaging = new FrameworkPackaging();
            return _FrameworkPackaging.GetSystemPackaging(_Request);
        }
    }
}

