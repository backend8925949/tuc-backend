﻿using System;
using Delivery.Object.Response.Shipments;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.GiftCards.Object;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkEmailProcessor
    {
        HCoreContext _HCoreContext;

        internal void ConfirmOrderEmail(OEmailProcessor.ConfirmOrder _Request)
        {
            try
            {   
                if (!string.IsNullOrEmpty(_Request.UserDisplayName) || !string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail("d-076ec5fd7c1a43c991843aa994337c0a", _Request.UserDisplayName, _Request.EmailAddress, _Request, _Request.UserReference);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string? UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.CustomerId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order details", "Order confirmed", _Request.DealTitle + " Order confirmed for " + _Request.MerchantDisplayName, "orderdetails", (long)_Request.ReferenceId, _Request.ReferenceKey, "View details", true, null);
                    }
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ConfirmOrderEmail", _Exception, _Request.UserReference);
            }
        }

        internal void CancelOrderEmail(OEmailProcessor.CancelOrder _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if(_Request.StatusId == HelperStatus.OrderStatus.CancelledBySeller)
                    {
                        string? UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.CustomerId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                        if (!string.IsNullOrEmpty(UserNotificationUrl))
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "Cancelled order aler!", "Order cancelled", _Request.DealTitle + " Order cancelled for " + _Request.MerchantDisplayName + " by the merchant. Please contact support for more information.", "orderdetails", (long)_Request.ReferenceId, _Request.ReferenceKey, "View details", true, null);
                        }
                    }
                    if (_Request.StatusId == HelperStatus.OrderStatus.CancelledBySystem)
                    {
                        string? UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.CustomerId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                        if (!string.IsNullOrEmpty(UserNotificationUrl))
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "Cancelled order aler!", "Order cancelled", _Request.DealTitle + " Order cancelled for " + _Request.MerchantDisplayName + ". Please contact support for more information.", "orderdetails", (long)_Request.ReferenceId, _Request.ReferenceKey, "View details", true, null);
                        }
                    }
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CancelOrderEmail", _Exception, _Request.UserReference);
            }
        }

        internal void ProductReviewEmail(OEmailProcessor.ProductReview _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail(BuydealEmail.ProductReviewTemplateId, _Request.CustomerDisplayName, _Request.EmailAddress, _Request, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ProductReviewEmail", _Exception, _Request.UserReference);
            }
        }

        internal void OrderNotificationEmail(OEmailProcessor.OrderNotification _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail(BuydealEmail.NewOrderNotificationTemplateId, _Request.MerchantDisplayName, _Request.EmailAddress, _Request, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("OrderNotificationEmail", _Exception, _Request.UserReference);
            }
        }

        internal void DealPurchaseNotificationEmail(OEmailProcessor.DealPurchaseNotification _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail(BuydealEmail.DealPurchaseNotificationTemplateId, _Request.MerchantDisplayName, _Request.EmailAddress, _Request, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DealPurchaseNotificationEmail", _Exception, _Request.UserReference);
            }
        }

        internal void DeliveryInvoice(OOrderProcess.Invoice.Details _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail(BuydealEmail.DeliveryInvoiceTemplateId, _Request.MerchantDetails.Name, _Request.EmailAddress, _Request, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeliveryInvoice", _Exception, _Request.UserReference);
            }
        }

        private class BuydealEmail
        {
            public const string ProductReviewTemplateId = "d-953f2bec813b4f6ab7e863747967e09c";
            public const string NewOrderNotificationTemplateId = "d-96ba73e562bf412e828df480c1a8994c";
            public const string DealPurchaseNotificationTemplateId = "d-2976882495f74a9bb7a6c6b3702147fc";
            public const string DeliveryInvoiceTemplateId = "d-4e894079627e476ebd1861405a65ab6c";
        }
    }
}

