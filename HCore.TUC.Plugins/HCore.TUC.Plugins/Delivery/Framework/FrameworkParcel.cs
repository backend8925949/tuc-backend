//==================================================================================
// FileName: FrameworkParcel.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to parcel
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Delivery.Framework;
using Delivery.Object.Requests.Parcels;
using Delivery.Object.Response.Parcels;
using Delivery.Object.Response.Rates;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkParcel
    {
        HCoreContext _HCoreContext;
        LSParcel _LSParcel;
        LSItems _LSItems;
        FrameworkDelivery _FrameworkDelivery;
        CreateParcelRequest _CreateParcelRequest;
        CreateParcel _CreateParcel;
        List<ParcelItems> _ParcelItems;
        ParcelMetaData _ParcelMetaData;
        OShipments.Save.Request _OShipmentsRequest;
        FrameworkShipment _FrameworkShipment;
        OShipments.CheckRates _RateRequest;

        /// <summary>
        /// Description: Saves the parcel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveParcel(OParcel.Save _Request)
        {
            try
            {
                _FrameworkShipment = new FrameworkShipment();
                _OShipmentsRequest = new OShipments.Save.Request();
                _FrameworkDelivery = new FrameworkDelivery();
                _ParcelItems = new List<ParcelItems>();
                _ParcelMetaData = new ParcelMetaData();
                _RateRequest = new OShipments.CheckRates();

                using (_HCoreContext = new HCoreContext())
                {
                    var Dealdetails = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).FirstOrDefault();
                    var CustomerDetails = _HCoreContext.MDDealAddress.Where(x => x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                    var MerchantDetails = _HCoreContext.MDDealAddress.Where(x => x.AccountId == Dealdetails.AccountId).FirstOrDefault();
                    var Currency = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.UserReference.CountryId).Select(a => a.CurrencyNotation).FirstOrDefault();
                    var PackageDetails = _HCoreContext.LSPackages.Where(x => x.DealId == _Request.DealId).FirstOrDefault();
                    _ParcelItems = new List<ParcelItems>();
                    _ParcelItems.Add(new ParcelItems
                    {
                        description = HCoreHelper.GenerateRandomNumber(42),
                        name = Dealdetails.Title,
                        currency = Currency,
                        value = (long?)Dealdetails.SellingPrice,
                        quantity = 1,
                        weight = PackageDetails.Weight
                    });
                    _CreateParcelRequest = new CreateParcelRequest(PackageDetails.Description, PackageDetails.PackagingId, PackageDetails.WeightUnit, _ParcelItems, _ParcelMetaData);
                    _CreateParcel = _FrameworkDelivery.CreateParcel(_CreateParcelRequest);
                    if(_CreateParcel.status == true)
                    {
                        _LSParcel = new LSParcel();
                        _LSParcel.Guid = HCoreHelper.GenerateGuid();

                        var Packaging = _HCoreContext.LSPackages.Where(x => x.PackagingId == _CreateParcel.data.packaging).Select(a => new
                        {
                            PackagingId = a.Id
                        }).FirstOrDefault();
                        if (Packaging.PackagingId > 0)
                        {
                            _LSParcel.PackagingId = Packaging.PackagingId;
                        }

                        _LSParcel.DealId = _Request.DealId;
                        _LSParcel.AccountId = _Request.UserReference.AccountId;
                        _LSParcel.Description = _CreateParcel.data.description;
                        _LSParcel.TotalWeight = _CreateParcel.data.total_weight;
                        _LSParcel.WeightUnit = _CreateParcel.data.weight_unit;
                        _LSParcel.ParcelId = _CreateParcel.data.parcel_id;
                        _LSParcel.CreateDate = _CreateParcel.data.created_at;
                        _LSParcel.CreatedById = _Request.UserReference.AccountId;
                        _LSParcel.TId = _CreateParcel.data.id;
                        _LSParcel.StatusId = HelperStatus.Default.Active;

                        foreach (var Item in _CreateParcel.data.items)
                        {
                            _LSItems = new LSItems();
                            _LSItems.Guid = HCoreHelper.GenerateGuid();
                            _LSItems.Description = Item.description;
                            _LSItems.Name = Item.name;
                            _LSItems.Currency = Item.currency;
                            _LSItems.Price = Item.value;
                            _LSItems.Quantity = Item.quantity;
                            _LSItems.Weight = Item.weight;
                            _LSItems.Parcel = _LSParcel;
                            _HCoreContext.LSItems.Add(_LSItems);
                        }

                        _HCoreContext.LSParcel.Add(_LSParcel);
                        _HCoreContext.SaveChanges();

                        _RateRequest.ParcelId = _CreateParcel.data.parcel_id;
                        //_RateRequest.DeliveryAddressId = CustomerDetails.AddressReference;
                        //_RateRequest.PickupAddressId = MerchantDetails.AddressReference;
                        //_RateRequest.Currency = Currency;

                        GetRatesForShipments GetRatesforShipment = _FrameworkShipment.CheckRateForShioments(_RateRequest);
                        if (GetRatesforShipment != null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, GetRatesforShipment, "LSSAVE", Resources.Resources.LSSAVE);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveParcel", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the parcel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetParcel(OParcel.Details _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.LSParcel.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                  .Select(x => new OParcel.Details
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,
                                      ParcelDescription = x.Description,
                                      ParcelId = x.ParcelId,
                                      TotalWeight = x.TotalWeight,
                                      WeightUnit = x.WeightUnit,

                                      PackagingId = x.PackagingId,
                                      PackagingCode = x.Packaging.PackagingId,
                                      ProductName = x.Packaging.ProductName,
                                      Height = x.Packaging.Height,
                                      Length = x.Packaging.Length,
                                      Weight = x.Packaging.Weight,
                                      Width = x.Packaging.Width,
                                      AccountId = x.Packaging.AccountId,
                                      AccountKey = x.Packaging.Account.Guid,
                                      AccountDisplayName = x.Packaging.Account.DisplayName,
                                      DealId = x.Packaging.DealId,
                                      DealKey = x.Packaging.Deal.Guid,
                                      DealTitle = x.Packaging.Deal.Title,

                                      TId = x.TId,
                                      CreateDate = x.CreateDate,
                                      CreatedBYId = x.CreatedById,
                                      CreatedByDisplayName = x.CreatedBy.DisplayName,
                                      ModifyDate = x.ModifyDate,
                                      ModifyById = x.ModifyById,
                                      ModifyByDisplayName = x.ModifyBy.DisplayName,
                                      StatusId = x.StatusId,
                                      StatusCode = x.Status.SystemName,
                                      StatusName = x.Status.Name
                                  })
                                  .FirstOrDefault();
                    if (Details != null)
                    {
                        Details.Items = _HCoreContext.LSItems.Where(x => x.ParcelId == Details.ReferenceId)
                            .Select(x => new OParcel.Items
                            {
                                ItemReferenceId = x.Id,
                                ItemDescription = x.Description,
                                ItemName = x.Name,
                                ItemCurrency = x.Currency,
                                ItemValue = x.Price,
                                ItemQuantity = x.Quantity,
                                ItemWeight = x.Weight
                            }).ToList();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "LS0200", Resources.Resources.LS0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetParcel", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the parcel list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetParcels(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.LSParcel
                                                .Select(x => new OParcel.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ParcelDescription = x.Description,
                                                    ParcelId = x.ParcelId,
                                                    TotalWeight = x.TotalWeight,
                                                    WeightUnit = x.WeightUnit,

                                                    PackagingId = x.PackagingId,
                                                    PackagingCode = x.Packaging.PackagingId,
                                                    ProductName = x.Packaging.ProductName,
                                                    AccountId = x.Packaging.AccountId,
                                                    AccountKey = x.Packaging.Account.Guid,
                                                    AccountDisplayName = x.Packaging.Account.DisplayName,
                                                    DealId = x.Packaging.DealId,
                                                    DealKey = x.Packaging.Deal.Guid,
                                                    DealTitle = x.Packaging.Deal.Title,

                                                    TId = x.TId,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    List<OParcel.List> Packagings = _HCoreContext.LSParcel
                                                .Select(x => new OParcel.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ParcelDescription = x.Description,
                                                    ParcelId = x.ParcelId,
                                                    TotalWeight = x.TotalWeight,
                                                    WeightUnit = x.WeightUnit,

                                                    PackagingId = x.PackagingId,
                                                    PackagingCode = x.Packaging.PackagingId,
                                                    ProductName = x.Packaging.ProductName,
                                                    AccountId = x.Packaging.AccountId,
                                                    AccountKey = x.Packaging.Account.Guid,
                                                    AccountDisplayName = x.Packaging.Account.DisplayName,
                                                    DealId = x.Packaging.DealId,
                                                    DealKey = x.Packaging.Deal.Guid,
                                                    DealTitle = x.Packaging.Deal.Title,

                                                    TId = x.TId,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();
                    foreach (var Packaging in Packagings)
                    {
                        Packaging.Items = _HCoreContext.LSItems.Where(x => x.ParcelId == Packaging.ReferenceId)
                            .Select(x => new OParcel.Items
                            {
                                ItemReferenceId = x.Id,
                                ItemDescription = x.Description,
                                ItemName = x.Name,
                                ItemCurrency = x.Currency,
                                ItemValue = x.Price,
                                ItemQuantity = x.Quantity,
                                ItemWeight = x.Weight
                            }).ToList();
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Packagings, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "LS0200", Resources.Resources.LS0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetParcels", _Exception, _Request.UserReference);
            }
        }
    }
}

