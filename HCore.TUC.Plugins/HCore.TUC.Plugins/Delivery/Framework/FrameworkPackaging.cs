//==================================================================================
// FileName: FrameworkPackaging.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to packaging
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Resources;
using static HCore.Helper.HCoreConstant;
using Delivery.Framework;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;
using Delivery.Object.Requests.Packaging;
using Delivery.Object.Response.Packaging;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkPackaging
    {
        HCoreContext _HCoreContext;
        FrameworkDelivery _FrameworkDelivery;
        LSPackages _LSPackages;
        CreatePackagingRequest _CreatePackagingRequest;
        CreatePackaging _CreatePackaging;
        OPackaging.Save.Response _PackagingResponse;
        MDPackaging _MDPackaging;
        // 100 , 100 , 49
        /// <summary>
        /// Description: Creates the package.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="DealId">The deal identifier.</param>
        /// <param name="Title">The title.</param>
        /// <param name="Weight">The weight.</param>
        /// <param name="_UserReference">The user reference.</param>
        internal void CreatePackage(long AccountId, long DealId, string? Title, double Weight, OUserReference _UserReference)
        {
            try
            {
                _FrameworkDelivery = new FrameworkDelivery();

                #region Tedrminal Africa
                //_CreatePackagingRequest = new CreatePackagingRequest(10.0, 40.0, Title, "cm", "box", 25.0, Weight, "kg");
                //_CreatePackaging = _FrameworkDelivery.CreatePackaging(_CreatePackagingRequest);
                //if (_CreatePackaging.status == true && _CreatePackaging.data != null)
                //{
                //    using (_HCoreContext = new HCoreContext())
                //    {
                //        LSPackages PckagingDetails = _HCoreContext.LSPackages.Where(x => x.DealId == DealId && x.AccountId == AccountId).FirstOrDefault();
                //        MDPackaging PDetails =  new MDPackaging();
                //        if (PckagingDetails != null)
                //        {
                //            PDetails = _HCoreContext.MDPackaging.Where(x => x.PackagingId == PckagingDetails.PackagingId && x.MerchantId == AccountId).FirstOrDefault();
                //        }
                //        if (PckagingDetails != null && PDetails != null)
                //        {
                //            PckagingDetails.AccountId = AccountId;
                //            PckagingDetails.DealId = DealId;
                //            PckagingDetails.Height = _CreatePackaging.data.height;
                //            PckagingDetails.Length = _CreatePackaging.data.length;
                //            PckagingDetails.ProductName = _CreatePackaging.data.name;
                //            PckagingDetails.Description = Title;
                //            PckagingDetails.SizeUnit = _CreatePackaging.data.size_unit;
                //            PckagingDetails.Type = _CreatePackaging.data.type;
                //            PckagingDetails.Weight = _CreatePackaging.data.weight;
                //            PckagingDetails.WeightUnit = _CreatePackaging.data.weight_unit;
                //            PckagingDetails.Width = _CreatePackaging.data.width;
                //            PckagingDetails.PackagingId = _CreatePackaging.data.packaging_id;
                //            PckagingDetails.TId = _CreatePackaging.data.id;
                //            PckagingDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                //            PckagingDetails.ModifyById = AccountId;
                //            PckagingDetails.StatusId = HelperStatus.Default.Active;


                //            PDetails.Height = _CreatePackaging.data.height;
                //            PDetails.Length = _CreatePackaging.data.length;
                //            PDetails.SizeUnit = _CreatePackaging.data.size_unit;
                //            PDetails.Weight = _CreatePackaging.data.weight;
                //            PDetails.WeightUnit = _CreatePackaging.data.weight_unit;
                //            PDetails.Width = _CreatePackaging.data.width;
                //            PDetails.Name = _CreatePackaging.data.name;
                //            PDetails.MerchantId = AccountId;
                //            PDetails.PackagingId = _CreatePackaging.data.packaging_id;
                //            PDetails.TId = _CreatePackaging.data.id;
                //            PDetails.Type = _CreatePackaging.data.type;
                //            _HCoreContext.SaveChanges();
                //        }
                //        else
                //        {
                //            _LSPackages = new LSPackages();
                //            _LSPackages.Guid = HCoreHelper.GenerateGuid();
                //            _LSPackages.AccountId = AccountId;
                //            _LSPackages.DealId = DealId;
                //            _LSPackages.Height = _CreatePackaging.data.height;
                //            _LSPackages.Length = _CreatePackaging.data.length;
                //            _LSPackages.Description = Title;
                //            _LSPackages.ProductName = _CreatePackaging.data.name;
                //            _LSPackages.SizeUnit = _CreatePackaging.data.size_unit;
                //            _LSPackages.Type = _CreatePackaging.data.type;
                //            _LSPackages.Weight = _CreatePackaging.data.weight;
                //            _LSPackages.WeightUnit = _CreatePackaging.data.weight_unit;
                //            _LSPackages.Width = _CreatePackaging.data.width;
                //            _LSPackages.PackagingId = _CreatePackaging.data.packaging_id;
                //            _LSPackages.TId = _CreatePackaging.data.id;
                //            _LSPackages.CreateDate = HCoreHelper.GetGMTDateTime();
                //            _LSPackages.CreatedById = AccountId;
                //            _LSPackages.StatusId = HelperStatus.Default.Active;
                //            _HCoreContext.LSPackages.Add(_LSPackages);

                //            _MDPackaging = new MDPackaging();
                //            _MDPackaging.Guid = HCoreHelper.GenerateGuid();
                //            _MDPackaging.Height = _CreatePackaging.data.height;
                //            _MDPackaging.Length = _CreatePackaging.data.length;
                //            _MDPackaging.SizeUnit = _CreatePackaging.data.size_unit;
                //            _MDPackaging.Weight = _CreatePackaging.data.weight;
                //            _MDPackaging.WeightUnit = _CreatePackaging.data.weight_unit;
                //            _MDPackaging.Width = _CreatePackaging.data.width;
                //            _MDPackaging.Name = _CreatePackaging.data.name;
                //            _MDPackaging.MerchantId = AccountId;
                //            _MDPackaging.PackagingId = _CreatePackaging.data.packaging_id;
                //            _MDPackaging.TId = _CreatePackaging.data.id;
                //            _MDPackaging.Type = _CreatePackaging.data.type;
                //            _HCoreContext.MDPackaging.Add(_MDPackaging);
                //            _HCoreContext.SaveChanges();
                //        }

                //    }
                //}
                #endregion

                #region Dellyman
                using (_HCoreContext = new HCoreContext())
                {
                    LSPackages PckagingDetails = _HCoreContext.LSPackages.Where(x => x.DealId == DealId).FirstOrDefault();
                    if (PckagingDetails != null)
                    {
                        PckagingDetails.AccountId = AccountId;
                        PckagingDetails.ProductName = Title;
                        PckagingDetails.Description = Title;
                        PckagingDetails.Weight = Weight;
                        PckagingDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        PckagingDetails.ModifyById = AccountId;
                        PckagingDetails.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                    }
                    else
                    {
                        _LSPackages = new LSPackages();
                        _LSPackages.Guid = HCoreHelper.GenerateGuid();
                        _LSPackages.AccountId = AccountId;
                        _LSPackages.DealId = DealId;
                        _LSPackages.Height = 10.0;
                        _LSPackages.Length = 40.0;
                        _LSPackages.Description = Title;
                        _LSPackages.ProductName = Title;
                        _LSPackages.SizeUnit = "cm";
                        _LSPackages.Type = "TUC Standard Packaging";
                        _LSPackages.Weight = Weight;
                        _LSPackages.WeightUnit = "kg";
                        _LSPackages.Width = 25.0;
                        _LSPackages.PackagingId = "PK-" + HCoreHelper.GenerateRandomNumber(5);
                        _LSPackages.TId = HCoreHelper.GenerateRandomNumber(10);
                        _LSPackages.CreateDate = HCoreHelper.GetGMTDateTime();
                        _LSPackages.CreatedById = AccountId;
                        _LSPackages.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.LSPackages.Add(_LSPackages);

                        _MDPackaging = new MDPackaging();
                        _MDPackaging.Guid = HCoreHelper.GenerateGuid();
                        _MDPackaging.Height = 10.0;
                        _MDPackaging.Length = 40.0;
                        _MDPackaging.SizeUnit = "cm";
                        _MDPackaging.Weight = Weight;
                        _MDPackaging.WeightUnit = "kg";
                        _MDPackaging.Width = 25.0;
                        _MDPackaging.Name = Title;
                        _MDPackaging.MerchantId = AccountId;
                        _MDPackaging.PackagingId = _LSPackages.PackagingId;
                        _MDPackaging.TId = _LSPackages.TId;
                        _MDPackaging.Type = "TUC Standard Packaging";
                        _HCoreContext.MDPackaging.Add(_MDPackaging);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreatePackage", _Exception, _UserReference);
            }
        }

        /// <summary>
        /// Description: Saves the packaging.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SavePackaging(OPackaging.Save.Request _Request)
        {
            try
            {
                _FrameworkDelivery = new FrameworkDelivery();
                if (_Request.Height == null || _Request.Height < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0004);
                }
                if (_Request.Length == null || _Request.Length < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0005);
                }
                if (_Request.Width == null || _Request.Width < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0006);
                }
                if (_Request.Weight == null || _Request.Weight < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0007);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0008);
                }
                if (string.IsNullOrEmpty(_Request.Size_unit))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0009);
                }
                if (string.IsNullOrEmpty(_Request.Type))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0010);
                }
                if (string.IsNullOrEmpty(_Request.Weight_unit))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0011);
                }
                if (_Request.AccountId < 1 || _Request.AccountId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSREF", "Please select merchant before creating new packaging.");
                }


                using (_HCoreContext = new HCoreContext())
                {
                    bool PcakageDetails = _HCoreContext.MDPackaging.Any(x => x.Name == _Request.Name && x.MerchantId == _Request.AccountId);
                    if (PcakageDetails)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSEXISTS", "Packaging already exist with same weight and dimensions.");
                    }
                    _CreatePackagingRequest = new CreatePackagingRequest(_Request.Height, _Request.Length, _Request.Name, _Request.Size_unit, _Request.Type, _Request.Width, _Request.Weight, _Request.Weight_unit);
                    _CreatePackaging = _FrameworkDelivery.CreatePackaging(_CreatePackagingRequest);
                    if (_CreatePackaging.status == true && _CreatePackaging.data != null)
                    {
                        _LSPackages = new LSPackages();
                        _LSPackages.Guid = HCoreHelper.GenerateGuid();
                        _LSPackages.AccountId = _Request.AccountId;
                        _LSPackages.Height = _CreatePackaging.data.height;
                        _LSPackages.Length = _CreatePackaging.data.length;
                        _LSPackages.ProductName = _CreatePackaging.data.name;
                        _LSPackages.SizeUnit = _CreatePackaging.data.size_unit;
                        _LSPackages.Type = _CreatePackaging.data.type;
                        _LSPackages.Weight = _CreatePackaging.data.weight;
                        _LSPackages.WeightUnit = _CreatePackaging.data.weight_unit;
                        _LSPackages.Width = _CreatePackaging.data.width;
                        _LSPackages.PackagingId = _CreatePackaging.data.packaging_id;
                        _LSPackages.TId = _CreatePackaging.data.id;
                        //_LSPackages.Description = _Request.ParcelDescription;
                        _LSPackages.CreateDate = HCoreHelper.GetGMTDateTime();
                        _LSPackages.CreatedById = _Request.UserReference.AccountId;
                        _LSPackages.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.LSPackages.Add(_LSPackages);

                        _MDPackaging = new MDPackaging();
                        _MDPackaging.Guid = HCoreHelper.GenerateGuid();
                        _MDPackaging.Height = _CreatePackaging.data.height;
                        _MDPackaging.Length = _CreatePackaging.data.length;
                        _MDPackaging.SizeUnit = _CreatePackaging.data.size_unit;
                        _MDPackaging.Weight = _CreatePackaging.data.weight;
                        _MDPackaging.WeightUnit = _CreatePackaging.data.weight_unit;
                        _MDPackaging.Width = _CreatePackaging.data.width;
                        _MDPackaging.Name = _CreatePackaging.data.name;
                        _MDPackaging.MerchantId = _Request.AccountId;
                        _MDPackaging.PackagingId = _CreatePackaging.data.packaging_id;
                        _MDPackaging.TId = _CreatePackaging.data.id;
                        _MDPackaging.Type = _CreatePackaging.data.type;
                        _HCoreContext.MDPackaging.Add(_MDPackaging);

                        _HCoreContext.SaveChanges();

                        var Response = new
                        {
                            ReferenceId = _LSPackages.Id,
                            ReferenceKey = _LSPackages.Guid,
                            PackagingId = _LSPackages.PackagingId,
                            Height = _LSPackages.Height,
                            Length = _LSPackages.Length,
                            Weight = _LSPackages.Weight,
                            Width = _LSPackages.Width,
                            Name = _LSPackages.ProductName,
                            Type = _LSPackages.Type,
                            //Description = _LSPackages.Description,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LSSAVE", Resources.Resources.LSSAVE);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SavePackaging", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Saves the packagings.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OPackaging.Save.Response.</returns>
        internal OPackaging.Save.Response SavePackagings(OPackaging.Save.Request _Request)
        {
            try
            {
                _FrameworkDelivery = new FrameworkDelivery();
                if (_Request.AccountId < 1)
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0011);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0012);
                }
                if (_Request.DealId < 1)
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0013);
                }
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0014);
                }
                if (_Request.Height == null)
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0004);
                }
                if (_Request.Length == null)
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0005);
                }
                if (_Request.Width == null)
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0006);
                }
                if (_Request.Weight == null)
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0007);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0008);
                }
                if (string.IsNullOrEmpty(_Request.Size_unit))
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0009);
                }
                if (string.IsNullOrEmpty(_Request.Type))
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0010);
                }
                if (string.IsNullOrEmpty(_Request.Weight_unit))
                {
                    HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0011);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _CreatePackagingRequest = new CreatePackagingRequest(_Request.Height, _Request.Length, _Request.Name, _Request.Size_unit, _Request.Type, _Request.Width, _Request.Weight, _Request.Weight_unit);
                    _CreatePackaging = _FrameworkDelivery.CreatePackaging(_CreatePackagingRequest);
                    if (_CreatePackaging != null)
                    {
                        if (_CreatePackaging.status == true && _CreatePackaging.data != null)
                        {
                            _LSPackages = new LSPackages();
                            _LSPackages.Guid = HCoreHelper.GenerateGuid();

                            var Account = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey)
                                .Select(a => new
                                {
                                    AccountId = a.Id
                                }).FirstOrDefault();
                            if (Account.AccountId > 0)
                            {
                                _LSPackages.AccountId = Account.AccountId;
                            }

                            var Deal = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).Select(a => new
                            {
                                DealId = a.Id
                            }).FirstOrDefault();
                            if (Deal.DealId > 0)
                            {
                                _LSPackages.DealId = Deal.DealId;
                            }

                            _LSPackages.Height = _CreatePackaging.data.height;
                            _LSPackages.Length = _CreatePackaging.data.length;
                            _LSPackages.ProductName = _CreatePackaging.data.name;
                            _LSPackages.SizeUnit = _CreatePackaging.data.size_unit;
                            _LSPackages.Type = _CreatePackaging.data.type;
                            _LSPackages.Weight = _CreatePackaging.data.weight;
                            _LSPackages.WeightUnit = _CreatePackaging.data.weight_unit;
                            _LSPackages.Width = _CreatePackaging.data.width;
                            _LSPackages.PackagingId = _CreatePackaging.data.packaging_id;
                            _LSPackages.TId = _CreatePackaging.data.id;
                            _LSPackages.Description = _Request.ParcelDescription;
                            _LSPackages.CreateDate = HCoreHelper.GetGMTDateTime();
                            _LSPackages.CreatedById = _Request.UserReference.AccountId;
                            _LSPackages.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.LSPackages.Add(_LSPackages);

                            if (_Request.ReferenceId > 0)
                            {
                                var Packaging = _HCoreContext.MDPackaging.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                if (Packaging != null)
                                {
                                    if (_Request.Height > 0)
                                    {
                                        Packaging.Height = (double)_Request.Height;
                                    }
                                    if (_Request.Length > 0)
                                    {
                                        Packaging.Length = (double)_Request.Length;
                                    }
                                    if (_Request.Width > 0)
                                    {
                                        Packaging.Width = (double)_Request.Width;
                                    }
                                    if (_Request.Weight > 0)
                                    {
                                        Packaging.Weight = (double)_Request.Weight;
                                    }
                                    if (Account.AccountId > 0)
                                    {
                                        Packaging.MerchantId = Account.AccountId;
                                    }
                                    if (!string.IsNullOrEmpty(_Request.Type))
                                    {
                                        Packaging.Type = _Request.Type;
                                    }
                                    if (!string.IsNullOrEmpty(_CreatePackaging.data.packaging_id))
                                    {
                                        Packaging.PackagingId = _CreatePackaging.data.packaging_id;
                                    }
                                    if (!string.IsNullOrEmpty(_CreatePackaging.data.id))
                                    {
                                        Packaging.TId = _CreatePackaging.data.id;
                                    }
                                }
                            }
                            else
                            {
                                _MDPackaging = new MDPackaging();
                                _MDPackaging.Guid = HCoreHelper.GenerateGuid();
                                _MDPackaging.Height = _CreatePackaging.data.height;
                                _MDPackaging.Length = _CreatePackaging.data.length;
                                _MDPackaging.SizeUnit = _CreatePackaging.data.size_unit;
                                _MDPackaging.Weight = _CreatePackaging.data.weight;
                                _MDPackaging.WeightUnit = _CreatePackaging.data.weight_unit;
                                _MDPackaging.Width = _CreatePackaging.data.width;
                                _MDPackaging.Name = _CreatePackaging.data.name;
                                _MDPackaging.MerchantId = Account.AccountId;
                                _MDPackaging.PackagingId = _CreatePackaging.data.packaging_id;
                                _MDPackaging.TId = _CreatePackaging.data.id;
                                _MDPackaging.Type = _CreatePackaging.data.type;
                                _HCoreContext.MDPackaging.Add(_MDPackaging);
                            }

                            _HCoreContext.SaveChanges();

                            _PackagingResponse = new OPackaging.Save.Response();
                            _PackagingResponse.ReferenceId = _LSPackages.Id;
                            _PackagingResponse.ReferenceKey = _LSPackages.Guid;
                            _PackagingResponse.PackagingId = _LSPackages.PackagingId;
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PackagingResponse, "LSSAVE", Resources.Resources.LSSAVE);
                            return _PackagingResponse;
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                            _PackagingResponse.Status = "01";
                            _PackagingResponse.Message = "Unable to create packaging.";
                            return _PackagingResponse;
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                        _PackagingResponse.Status = "01";
                        _PackagingResponse.Message = "Unable to create packaging.";
                        return _PackagingResponse;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SavePackaging", _Exception, _Request.UserReference);
                _PackagingResponse.Status = "01";
                _PackagingResponse.Message = _Exception.Message;
                return _PackagingResponse;
            }
        }

        /// <summary>
        /// Description: Updates the packaging.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OPackaging.Save.Response.</returns>
        internal OPackaging.Save.Response UpdatePackaging(OPackaging.Save.Request _Request)
        {
            try
            {
                _CreatePackaging = new CreatePackaging();
                _FrameworkDelivery = new FrameworkDelivery();
                using (_HCoreContext = new HCoreContext())
                {
                    var PckagingDetails = _HCoreContext.LSPackages.Where(x => x.DealId == _Request.DealId && x.Deal.Guid == _Request.DealKey).FirstOrDefault();
                    var PDetails = _HCoreContext.MDPackaging.Where(x => x.PackagingId == _Request.PackagingId && x.MerchantId == _Request.AccountId).FirstOrDefault();
                    if (PckagingDetails != null && PDetails != null)
                    {
                        _CreatePackagingRequest = new CreatePackagingRequest(_Request.Height, _Request.Length, _Request.Name, _Request.Size_unit, _Request.Type, _Request.Width, _Request.Weight, _Request.Weight_unit);
                        _CreatePackaging = _FrameworkDelivery.CreatePackaging(_CreatePackagingRequest);
                        if (_CreatePackaging.status == true && _CreatePackaging.data != null)
                        {
                            if (_Request.AccountId > 0)
                            {
                                PckagingDetails.AccountId = _Request.AccountId;
                            }
                            if (_Request.DealId > 0)
                            {
                                PckagingDetails.DealId = _Request.DealId;
                            }
                            if (_CreatePackaging.data.height > 0)
                            {
                                PckagingDetails.Height = _CreatePackaging.data.height;
                            }
                            if (_CreatePackaging.data.length > 0)
                            {
                                PckagingDetails.Length = _CreatePackaging.data.length;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.name))
                            {
                                PckagingDetails.ProductName = _CreatePackaging.data.name;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.size_unit))
                            {
                                PckagingDetails.SizeUnit = _CreatePackaging.data.size_unit;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.type))
                            {
                                PckagingDetails.Type = _CreatePackaging.data.type;
                            }
                            if (_CreatePackaging.data.weight > 0)
                            {
                                PckagingDetails.Weight = _CreatePackaging.data.weight;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.weight_unit))
                            {
                                PckagingDetails.WeightUnit = _CreatePackaging.data.weight_unit;
                            }
                            if (_CreatePackaging.data.width > 0)
                            {
                                PckagingDetails.Width = _CreatePackaging.data.width;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.packaging_id))
                            {
                                PckagingDetails.PackagingId = _CreatePackaging.data.packaging_id;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.id))
                            {
                                PckagingDetails.TId = _CreatePackaging.data.id;
                            }
                            if (!string.IsNullOrEmpty(_Request.ParcelDescription))
                            {
                                PckagingDetails.Description = _Request.ParcelDescription;
                            }


                            if (_Request.Height > 0)
                            {
                                PDetails.Height = (double)_Request.Height;
                            }
                            if (_Request.Length > 0)
                            {
                                PDetails.Length = (double)_Request.Length;
                            }
                            if (_Request.Width > 0)
                            {
                                PDetails.Width = (double)_Request.Width;
                            }
                            if (_Request.Weight > 0)
                            {
                                PDetails.Weight = (double)_Request.Weight;
                            }
                            if (_Request.AccountId > 0 && PDetails.MerchantId != _Request.AccountId)
                            {
                                PDetails.MerchantId = _Request.AccountId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Type))
                            {
                                PDetails.Type = _Request.Type;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.packaging_id))
                            {
                                PDetails.PackagingId = _CreatePackaging.data.packaging_id;
                            }
                            if (!string.IsNullOrEmpty(_CreatePackaging.data.id))
                            {
                                PDetails.TId = _CreatePackaging.data.id;
                            }

                            PckagingDetails.StatusId = HelperStatus.Default.Active;
                            PckagingDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            PckagingDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();

                            _PackagingResponse = new OPackaging.Save.Response();
                            _PackagingResponse.ReferenceId = PckagingDetails.Id;
                            _PackagingResponse.ReferenceKey = PckagingDetails.Guid;
                            _PackagingResponse.PackagingId = PckagingDetails.PackagingId;
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PackagingResponse, "LSUPDATE", Resources.Resources.LSUPDATE);
                            return _PackagingResponse;
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                            _PackagingResponse.Status = "01";
                            _PackagingResponse.Message = "Unable to create packaging please try after some time.";
                            return _PackagingResponse;
                        }
                    }
                    else
                    {
                        //_HCoreContext.Dispose();
                        //HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                        //_PackagingResponse.Status = "01";
                        //_PackagingResponse.Message = "Packaging details not found.";
                        //return _PackagingResponse;
                        _CreatePackagingRequest = new CreatePackagingRequest(_Request.Height, _Request.Length, _Request.Name, _Request.Size_unit, _Request.Type, _Request.Width, _Request.Weight, _Request.Weight_unit);
                        _CreatePackaging = _FrameworkDelivery.CreatePackaging(_CreatePackagingRequest);
                        if (_CreatePackaging != null)
                        {
                            if (_CreatePackaging.status == true && _CreatePackaging.data != null)
                            {
                                _LSPackages = new LSPackages();
                                _LSPackages.Guid = HCoreHelper.GenerateGuid();

                                var Account = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey)
                                    .Select(a => new
                                    {
                                        AccountId = a.Id
                                    }).FirstOrDefault();
                                if (Account.AccountId > 0)
                                {
                                    _LSPackages.AccountId = Account.AccountId;
                                }

                                var Deal = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).Select(a => new
                                {
                                    DealId = a.Id
                                }).FirstOrDefault();
                                if (Deal.DealId > 0)
                                {
                                    _LSPackages.DealId = Deal.DealId;
                                }

                                _LSPackages.Height = _CreatePackaging.data.height;
                                _LSPackages.Length = _CreatePackaging.data.length;
                                _LSPackages.ProductName = _CreatePackaging.data.name;
                                _LSPackages.SizeUnit = _CreatePackaging.data.size_unit;
                                _LSPackages.Type = _CreatePackaging.data.type;
                                _LSPackages.Weight = _CreatePackaging.data.weight;
                                _LSPackages.WeightUnit = _CreatePackaging.data.weight_unit;
                                _LSPackages.Width = _CreatePackaging.data.width;
                                _LSPackages.PackagingId = _CreatePackaging.data.packaging_id;
                                _LSPackages.TId = _CreatePackaging.data.id;
                                _LSPackages.Description = _Request.ParcelDescription;
                                _LSPackages.CreateDate = HCoreHelper.GetGMTDateTime();
                                _LSPackages.CreatedById = _Request.UserReference.AccountId;
                                _LSPackages.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.LSPackages.Add(_LSPackages);
                                _HCoreContext.SaveChanges();

                                _PackagingResponse = new OPackaging.Save.Response();
                                _PackagingResponse.ReferenceId = _LSPackages.Id;
                                _PackagingResponse.ReferenceKey = _LSPackages.Guid;
                                _PackagingResponse.PackagingId = _LSPackages.PackagingId;
                                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PackagingResponse, "LSSAVE", Resources.Resources.LSSAVE);
                                return _PackagingResponse;
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                                _PackagingResponse.Status = "01";
                                _PackagingResponse.Message = "Unable to create packaging.";
                                return _PackagingResponse;
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                            _PackagingResponse.Status = "01";
                            _PackagingResponse.Message = "Unable to create packaging.";
                            return _PackagingResponse;
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdatePackaging", _Exception, _Request.UserReference);
                _PackagingResponse.Status = "01";
                _PackagingResponse.Message = _Exception.Message;
                return _PackagingResponse;
            }
        }

        /// <summary>
        /// Description: Gets the packaging.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPackaging(OPackaging.Details _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.LSPackages.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                  .Select(x => new OPackaging.Details
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,
                                      Name = x.ProductName,
                                      AccountId = x.AccountId,
                                      AccountKey = x.Account.Guid,
                                      AccountDisplayName = x.Account.DisplayName,
                                      AccountEmailAddress = x.Account.EmailAddress,
                                      AccountMobileNumber = x.Account.MobileNumber,
                                      AccountIconUrl = x.Account.IconStorage.Path,
                                      DealId = x.DealId,
                                      DealKey = x.Deal.Guid,
                                      DealTitle = x.Deal.Title,
                                      DealIconUrl = x.Deal.PosterStorage.Path,
                                      DealActualPrice = x.Deal.ActualPrice,
                                      DealSellingPrice = x.Deal.SellingPrice,
                                      Height = x.Height,
                                      Length = x.Length,
                                      Size_unit = x.SizeUnit,
                                      Type = x.Type,
                                      Weight = x.Weight,
                                      Weight_unit = x.WeightUnit,
                                      Width = x.Width,
                                      Packaging_Id = x.PackagingId,
                                      TId = x.TId,
                                      CreateDate = x.CreateDate,
                                      CreatedBYId = x.CreatedById,
                                      CreatedByDisplayName = x.CreatedBy.DisplayName,
                                      ModifyDate = x.ModifyDate,
                                      ModifyById = x.ModifyById,
                                      ModifyByDisplayName = x.ModifyBy.DisplayName,
                                      StatusId = x.StatusId,
                                      StatusCode = x.Status.SystemName,
                                      StatusName = x.Status.Name
                                  })
                                  .FirstOrDefault();
                    if (Details != null)
                    {
                        if (!string.IsNullOrEmpty(Details.AccountIconUrl))
                        {
                            Details.AccountIconUrl = _AppConfig.StorageUrl + Details.AccountIconUrl;
                        }
                        else
                        {
                            Details.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.DealIconUrl))
                        {
                            Details.DealIconUrl = _AppConfig.StorageUrl + Details.DealIconUrl;
                        }
                        else
                        {
                            Details.DealIconUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "LS0200", Resources.Resources.LS0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPackaging", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the packaging list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPackagings(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.LSPackages
                                                .Select(x => new OPackaging.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.ProductName,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    DealId = x.DealId,
                                                    DealTitle = x.Deal.Title,
                                                    Height = x.Height,
                                                    Length = x.Length,
                                                    Size_unit = x.SizeUnit,
                                                    Type = x.Type,
                                                    Weight = x.Weight,
                                                    Weight_unit = x.WeightUnit,
                                                    Width = x.Width,
                                                    Packaging_Id = x.PackagingId,
                                                    TId = x.TId,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    List<OPackaging.List> Packagings = _HCoreContext.LSPackages
                                                .Select(x => new OPackaging.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.ProductName,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    DealId = x.DealId,
                                                    DealTitle = x.Deal.Title,
                                                    Height = x.Height,
                                                    Length = x.Length,
                                                    Size_unit = x.SizeUnit,
                                                    Type = x.Type,
                                                    Weight = x.Weight,
                                                    Weight_unit = x.WeightUnit,
                                                    Width = x.Width,
                                                    Packaging_Id = x.PackagingId,
                                                    TId = x.TId,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();
                    foreach (var Packaging in Packagings)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Packagings, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "LS0200", Resources.Resources.LS0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPackagings", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the system packaging.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSystemPackaging(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "AccountId", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.MDPackaging
                                                //.Where(x => x.MerchantId == null || x.MerchantId == _Request.AccountId)
                                                .Select(x => new OPackaging.PackagingList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountId = x.MerchantId,
                                                    AccountKey = x.Merchant.Guid,
                                                    AccountDisplayName = x.Merchant.DisplayName,
                                                    Height = x.Height,
                                                    Length = x.Length,
                                                    Name = x.Name,
                                                    SizeUnit = x.SizeUnit,
                                                    Type = x.Type,
                                                    Weight = x.Weight,
                                                    WeightUnit = x.WeightUnit,
                                                    Width = x.Width,
                                                    PackagingId = x.PackagingId,
                                                    TId = x.TId,
                                                    CategoryId = x.CategoryId,
                                                    CategoryName = x.Category.Name,
                                                    ParentCategoryId = x.Category.ParentCategoryId,
                                                    MaxWeight = x.MaximumWeight,
                                                    MinWeight = x.MinimunWeight
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    List<OPackaging.PackagingList> Packagings = _HCoreContext.MDPackaging
                                                //.Where(x => x.MerchantId == null || x.MerchantId == _Request.AccountId)
                                                .Select(x => new OPackaging.PackagingList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountId = x.MerchantId,
                                                    AccountKey = x.Merchant.Guid,
                                                    AccountDisplayName = x.Merchant.DisplayName,
                                                    Height = x.Height,
                                                    Length = x.Length,
                                                    Name = x.Name,
                                                    SizeUnit = x.SizeUnit,
                                                    Type = x.Type,
                                                    Weight = x.Weight,
                                                    WeightUnit = x.WeightUnit,
                                                    Width = x.Width,
                                                    PackagingId = x.PackagingId,
                                                    TId = x.TId,
                                                    CategoryId = x.CategoryId,
                                                    CategoryName = x.Category.Name,
                                                    ParentCategoryId = x.Category.ParentCategoryId,
                                                    MaxWeight = x.MaximumWeight,
                                                    MinWeight = x.MinimunWeight
                                                }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();
                    foreach (var Packaging in Packagings)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Packagings, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "LS0200", Resources.Resources.LS0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSystemPackaging", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Updates the packagings.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdatePackagings(OPackaging.Update _Request)
        {
            try
            {
                if (_Request.DealId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSDEAL", "Deal id required");
                }

                if (_Request.ParcelDescription.Length > 45)
                {
                    _Request.ParcelDescription = _Request.ParcelDescription.Substring(0, 45);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.LSPackages.Where(x => x.PackagingId == _Request.PackagingId).FirstOrDefault();
                    var TDetails = _HCoreContext.MDPackaging.Where(x => x.PackagingId == _Request.PackagingId).FirstOrDefault();
                    long DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).Select(m => m.AccountId).FirstOrDefault();
                    if (Details != null && TDetails != null)
                    {
                        if ((Details.DealId > 1 && Details.AccountId > 1) && (TDetails.MerchantId < 1 || TDetails.MerchantId == null || TDetails.MerchantId != DealDetails))
                        {
                            _LSPackages = new LSPackages();
                            _LSPackages.Guid = HCoreHelper.GenerateGuid();
                            _LSPackages.DealId = _Request.DealId;
                            _LSPackages.AccountId = DealDetails;
                            _LSPackages.Height = Details.Height;
                            _LSPackages.Length = Details.Length;
                            _LSPackages.ProductName = Details.ProductName;
                            _LSPackages.SizeUnit = Details.SizeUnit;
                            _LSPackages.Type = Details.Type;
                            _LSPackages.Weight = Details.Weight;
                            _LSPackages.WeightUnit = Details.WeightUnit;
                            _LSPackages.Width = Details.Width;
                            _LSPackages.PackagingId = Details.PackagingId;
                            _LSPackages.TId = Details.TId;
                            _LSPackages.Description = _Request.ParcelDescription;
                            _LSPackages.CreateDate = HCoreHelper.GetGMTDateTime();
                            _LSPackages.CreatedById = _Request.UserReference.AccountId;
                            _LSPackages.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.LSPackages.Add(_LSPackages);

                            _MDPackaging = new MDPackaging();
                            _MDPackaging.Guid = HCoreHelper.GenerateGuid();
                            _MDPackaging.Height = (double)Details.Height;
                            _MDPackaging.Length = (double)Details.Length;
                            _MDPackaging.SizeUnit = Details.SizeUnit;
                            _MDPackaging.Weight = (double)Details.Weight;
                            _MDPackaging.WeightUnit = Details.WeightUnit;
                            _MDPackaging.Width = (double)Details.Width;
                            _MDPackaging.Name = Details.ProductName;
                            _MDPackaging.MerchantId = DealDetails;
                            _MDPackaging.PackagingId = Details.PackagingId;
                            _MDPackaging.TId = Details.TId;
                            _MDPackaging.Type = Details.Type;
                            _HCoreContext.MDPackaging.Add(_MDPackaging);

                            _HCoreContext.SaveChanges();

                            var Response = new
                            {
                                ReferenceId = _LSPackages.Id,
                                ReferenceKey = _LSPackages.Guid,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LSSAVE", Resources.Resources.LSSAVE);
                        }
                        if ((Details.DealId < 1 || Details.DealId == null) && (TDetails.MerchantId < 1 || TDetails.MerchantId == null || TDetails.MerchantId != DealDetails))
                        {
                            if (Details.DealId < 0 || Details.DealId == null)
                            {
                                Details.DealId = _Request.DealId;
                            }
                            if (Details.AccountId < 0 || Details.AccountId == null)
                            {
                                Details.AccountId = DealDetails;
                            }
                            if (string.IsNullOrEmpty(Details.Description))
                            {
                                Details.Description = _Request.ParcelDescription;
                            }
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.LSPackages.Update(Details);

                            _MDPackaging = new MDPackaging();
                            _MDPackaging.Guid = HCoreHelper.GenerateGuid();
                            _MDPackaging.Height = (double)Details.Height;
                            _MDPackaging.Length = (double)Details.Length;
                            _MDPackaging.SizeUnit = Details.SizeUnit;
                            _MDPackaging.Weight = (double)Details.Weight;
                            _MDPackaging.WeightUnit = Details.WeightUnit;
                            _MDPackaging.Width = (double)Details.Width;
                            _MDPackaging.Name = Details.ProductName;
                            _MDPackaging.MerchantId = DealDetails;
                            _MDPackaging.PackagingId = Details.PackagingId;
                            _MDPackaging.TId = Details.TId;
                            _MDPackaging.Type = Details.Type;
                            _HCoreContext.MDPackaging.Add(_MDPackaging);

                            _HCoreContext.SaveChanges();

                            var Response = new
                            {
                                ReferenceId = _LSPackages.Id,
                                ReferenceKey = _LSPackages.Guid,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LSSAVE", Resources.Resources.LSSAVE);
                        }
                        else
                        {
                            if (Details.DealId < 0 || Details.DealId == null)
                            {
                                Details.DealId = _Request.DealId;
                            }
                            if (Details.AccountId < 0 || Details.AccountId == null)
                            {
                                Details.AccountId = DealDetails;
                            }
                            if (string.IsNullOrEmpty(Details.Description))
                            {
                                Details.Description = _Request.ParcelDescription;
                            }
                            if (TDetails.MerchantId < 0 || TDetails.MerchantId == null)
                            {
                                TDetails.MerchantId = DealDetails;
                            }
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.LSPackages.Update(Details);
                            _HCoreContext.MDPackaging.Update(TDetails);
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "LSUPDATE", "Details updated successfully");
                        }
                    }
                    if ((Details == null) && (TDetails.MerchantId < 1 || TDetails.MerchantId == null || TDetails.MerchantId != DealDetails))
                    {
                        _LSPackages = new LSPackages();
                        _LSPackages.Guid = HCoreHelper.GenerateGuid();
                        _LSPackages.AccountId = DealDetails;
                        _LSPackages.Height = TDetails.Height;
                        _LSPackages.Length = TDetails.Length;
                        _LSPackages.ProductName = TDetails.Name;
                        _LSPackages.SizeUnit = TDetails.SizeUnit;
                        _LSPackages.Type = TDetails.Type;
                        _LSPackages.Weight = TDetails.Weight;
                        _LSPackages.WeightUnit = TDetails.WeightUnit;
                        _LSPackages.Width = TDetails.Width;
                        _LSPackages.PackagingId = TDetails.PackagingId;
                        _LSPackages.TId = TDetails.TId;
                        _LSPackages.Description = _Request.ParcelDescription;
                        _LSPackages.CreateDate = HCoreHelper.GetGMTDateTime();
                        _LSPackages.CreatedById = _Request.UserReference.AccountId;
                        _LSPackages.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.LSPackages.Add(_LSPackages);

                        _MDPackaging = new MDPackaging();
                        _MDPackaging.Guid = HCoreHelper.GenerateGuid();
                        _MDPackaging.Height = (double)TDetails.Height;
                        _MDPackaging.Length = (double)TDetails.Length;
                        _MDPackaging.SizeUnit = TDetails.SizeUnit;
                        _MDPackaging.Weight = (double)TDetails.Weight;
                        _MDPackaging.WeightUnit = TDetails.WeightUnit;
                        _MDPackaging.Width = (double)TDetails.Width;
                        _MDPackaging.Name = TDetails.Name;
                        _MDPackaging.MerchantId = DealDetails;
                        _MDPackaging.PackagingId = TDetails.PackagingId;
                        _MDPackaging.TId = TDetails.TId;
                        _MDPackaging.Type = TDetails.Type;
                        _HCoreContext.MDPackaging.Add(_MDPackaging);

                        _HCoreContext.SaveChanges();

                        var Response = new
                        {
                            ReferenceId = _LSPackages.Id,
                            ReferenceKey = _LSPackages.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LSSAVE", Resources.Resources.LSSAVE);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdatePackagings", _Exception, _Request.UserReference, "LS0500", "Unable to process your request. Please try after some time");
            }
        }
    }
}


