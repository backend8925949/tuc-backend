//==================================================================================
// FileName: FrameworkAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to address
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using Delivery.Framework;
using Delivery.Object.Requests;
using Delivery.Object.Response.Address;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Plugins.Delivery.Objects.ODeliveryAddress;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkAddress
    {
        HCoreContext _HCoreContext;
        HCCoreAddress _HCCoreAddress;
        Create_UpdateAddress _Create_UpdateAddress;
        Create_UpdateAddress _FromAddress;
        Create_UpdateAddress _ToAddress;
        Metadata _Metadata;
        CreateAddress _CreateAddress;
        FrameworkDelivery _FrameworkDelivery;
        UpdateAddress _UpdateAddress;
        MDDealAddress _MDDealAddress;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        Random _Random;

        /// <summary>
        /// Description: Splits to lines.
        /// </summary>
        /// <param name="stringToSplit">The string to split.</param>
        /// <param name="maximumLineLength">Maximum length of the line.</param>
        /// <returns>List&lt;System.String&gt;.</returns>
        List<string> SplitToLines(string stringToSplit, int maximumLineLength)
        {
            var words = stringToSplit.Split(' ').Concat(new[] { "" });
            return
                words
                    .Skip(1)
                    .Aggregate(
                        words.Take(1).ToList(),
                        (a, w) =>
                        {
                            var last = a.Last();
                            while (last.Length > maximumLineLength)
                            {
                                a[a.Count() - 1] = last.Substring(0, maximumLineLength);
                                last = last.Substring(maximumLineLength);
                                a.Add(last);
                            }
                            var test = last + " " + w;
                            if (test.Length > maximumLineLength)
                            {
                                a.Add(w);
                            }
                            else
                            {
                                a[a.Count() - 1] = test;
                            }
                            return a;
                        });
        }

        /// <summary>
        /// Description: Manages the address.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="AddressId">The address identifier.</param>
        /// <returns>ODeliveryAddress.ManageAddress.Response.</returns>
        internal ODeliveryAddress.ManageAddress.Response ManageAddress(long AccountId, long AddressId)
        {
            try
            {
                _Metadata = new Metadata();
                _CreateAddress = new CreateAddress();
                _FrameworkDelivery = new FrameworkDelivery();
                using (_HCoreContext = new HCoreContext())
                {
                    if (AddressId > 0)
                    {
                        //var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == AccountId).FirstOrDefault();
                        var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == AddressId)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                CountryName = x.Country.Iso.ToUpper(),
                                EmailAddress = x.EmailAddress,
                                Name = x.Name,
                                AddressLine1 = x.AddressLine1,  // Minimum 30 Characters // Max 40 Characters
                                AddressLine2 = x.AddressLine2,
                                ContactNumber = x.ContactNumber,
                                CityName = x.City.Name,
                                StateName = x.State.Name,
                                CountryFullName = x.Country.Name,
                                CountryId = x.CountryId,
                                CountryIsd = x.Country.Isd,
                                CountryMobileNumberLength = x.Country.MobileNumberLength,
                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                            }).FirstOrDefault();
                        var MadDealAddres = _HCoreContext.MDDealAddress.Where(x => x.AccountId == AccountId && x.AddressId == AddressId).FirstOrDefault();
                        if (MadDealAddres != null)
                        {
                            string Address = AddressDetails.AddressLine1;
                            if (!string.IsNullOrEmpty(AddressDetails.AddressLine2))
                            {
                                Address += ' ' + AddressDetails.AddressLine2;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.CityName;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.StateName;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.CountryName;
                            }

                            List<string> _AddressComponent = SplitToLines(Address, 40);
                            string AddressLine1 = _AddressComponent[0];
                            string AddressLine2 = "";
                            if (_AddressComponent.Count() > 1)
                            {
                                AddressLine2 = _AddressComponent[1]; ;

                            }
;
                            //string AddressLine1 = AddressDetails.AddressLine1;
                            //if (AddressLine1.Length < 30 || AddressLine1.Length > 45)
                            //{
                            //    AddressLine1 = AddressLine1 + " " + AddressDetails.CityName + " " + AddressDetails.StateName;
                            //    if (AddressLine1.Length > 45)
                            //    {
                            //        AddressLine1 = AddressLine1.Substring(0, 44);
                            //    }
                            //}
                            //string AddressLine2 = "";
                            //if (!string.IsNullOrEmpty(AddressDetails.AddressLine2))
                            //{
                            //    AddressLine2 = AddressDetails.AddressLine2;
                            //    if (AddressLine2.Length < 30 || AddressLine2.Length > 45)
                            //    {
                            //        AddressLine2 = AddressLine2 + " " + AddressDetails.CityName + " " + AddressDetails.StateName;
                            //        if (AddressLine2.Length > 45)
                            //        {
                            //            AddressLine2 = AddressLine2.Substring(0, 44);
                            //        }
                            //    }
                            //}


                            string ContactNumber = HCoreHelper.FormatMobileNumber(AddressDetails.CountryIsd, AddressDetails.ContactNumber, AddressDetails.CountryMobileNumberLength);

                            _Create_UpdateAddress = new Create_UpdateAddress(AddressDetails.CityName,
                                AddressDetails.CountryName,
                                AddressDetails.EmailAddress,
                                AddressDetails.Name,
                                false,
                                 AddressDetails.Name,
                               AddressLine1,
                                AddressDetails.AddressLine2,
                                _Metadata, AddressDetails.Name,
                                 "+" + ContactNumber,
                                AddressDetails.StateName, null);
                            _UpdateAddress = _FrameworkDelivery.UpdateAddress(_Create_UpdateAddress, MadDealAddres.AddressReference);

                            var Response = new ODeliveryAddress.ManageAddress.Response
                            {
                                AccountId = AddressDetails.AccountId,
                                AccountKey = AddressDetails.AccountKey,
                                ReferenceId = MadDealAddres.Id,
                                ReferenceKey = MadDealAddres.Guid,
                                AddressTId = MadDealAddres.TId,
                                AddressReference = MadDealAddres.AddressReference,
                                CoreAddressId = AddressDetails.ReferenceId,
                                CoreAddressKey = AddressDetails.ReferenceKey,
                                Address = AddressDetails.AddressLine1 + " " + AddressDetails.AddressLine2,
                            };
                            return Response;
                        }
                        else
                        {
                            string Address = AddressDetails.AddressLine1;
                            if (!string.IsNullOrEmpty(AddressDetails.AddressLine2))
                            {
                                Address += ' ' + AddressDetails.AddressLine2;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.CityName;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.StateName;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.CountryName;
                            }

                            List<string> _AddressComponent = SplitToLines(Address, 40);
                            string AddressLine1 = _AddressComponent[0];
                            string AddressLine2 = "";
                            if (_AddressComponent.Count() > 1)
                            {
                                AddressLine2 = _AddressComponent[1]; ;

                            }
                            //string AddressLine1 = AddressDetails.AddressLine1;
                            //if (AddressLine1.Length < 30 || AddressLine1.Length > 45)
                            //{
                            //    AddressLine1 = AddressLine1 + " " + AddressDetails.CityName + " " + AddressDetails.StateName;
                            //    if (AddressLine1.Length > 45)
                            //    {
                            //        AddressLine1 = AddressLine1.Substring(0, 44);
                            //    }
                            //}

                            //string AddressLine2 = "";
                            //if (!string.IsNullOrEmpty(AddressDetails.AddressLine2))
                            //{
                            //    AddressLine2 = AddressDetails.AddressLine2;
                            //    if (AddressLine2.Length < 30 || AddressLine2.Length > 45)
                            //    {
                            //        AddressLine2 = AddressLine2 + " " + AddressDetails.CityName + " " + AddressDetails.StateName;
                            //        if (AddressLine2.Length > 45)
                            //        {
                            //            AddressLine2 = AddressLine2.Substring(0, 44);
                            //        }
                            //    }
                            //}

                            // Create Address
                            string ContactNumber = HCoreHelper.FormatMobileNumber(AddressDetails.CountryIsd, AddressDetails.ContactNumber, AddressDetails.CountryMobileNumberLength);
                            _Create_UpdateAddress = new Create_UpdateAddress(AddressDetails.CityName,
                                AddressDetails.CountryName,
                                AddressDetails.EmailAddress,
                                AddressDetails.Name,
                                false,
                               AddressDetails.Name,
                               AddressLine1,
                               AddressLine2,
                                _Metadata, AddressDetails.Name,
                                "+" + ContactNumber,
                                AddressDetails.StateName, "");
                            _CreateAddress = _FrameworkDelivery.CreateAddress(_Create_UpdateAddress);
                            if (_CreateAddress.status == true)
                            {
                                _MDDealAddress = new MDDealAddress();
                                _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                                _MDDealAddress.AccountId = AddressDetails.AccountId;
                                _MDDealAddress.AddressId = AddressId;
                                _MDDealAddress.TId = _CreateAddress.data.id;
                                _MDDealAddress.AddressReference = _CreateAddress.data.address_id;
                                _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                                _HCoreContext.SaveChanges();
                                var Response = new ODeliveryAddress.ManageAddress.Response
                                {
                                    AccountId = AddressDetails.AccountId,
                                    AccountKey = AddressDetails.AccountKey,
                                    ReferenceId = _MDDealAddress.Id,
                                    ReferenceKey = _MDDealAddress.Guid,
                                    AddressTId = _CreateAddress.data.id,
                                    AddressReference = _MDDealAddress.AddressReference,
                                    CoreAddressId = AddressDetails.ReferenceId,
                                    CoreAddressKey = AddressDetails.ReferenceKey,

                                };
                                return Response;

                                //var AddressDetailss = _HCoreContext.MDDealAddress.Where(x => x.AccountId == AccountId).FirstOrDefault();
                                //if (AddressDetailss != null)
                                //{
                                //    if (!string.IsNullOrEmpty(_CreateAddress.data.id))
                                //    {
                                //        AddressDetailss.TId = _CreateAddress.data.id;
                                //    }
                                //    if (!string.IsNullOrEmpty(_CreateAddress.data.address_id))
                                //    {
                                //        AddressDetailss.AddressReference = _CreateAddress.data.address_id;
                                //    }
                                //    AddressDetailss.Address = _HCCoreAddress;
                                //}
                                //else
                                //{
                                //    _MDDealAddress = new MDDealAddress();
                                //    _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                                //    _MDDealAddress.AccountId = AccountId;
                                //    _MDDealAddress.Address = _HCCoreAddress;
                                //    _MDDealAddress.TId = _CreateAddress.data.id;
                                //    _MDDealAddress.AddressReference = _CreateAddress.data.address_id;
                                //    _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                                //}
                                //if (AddressDetailss != null)
                                //{
                                //    var Response = new ODeliveryAddress.ManageAddress.Response
                                //    {
                                //        ReferenceId = AddressDetailss.Id,
                                //        ReferenceKey = AddressDetailss.Guid,
                                //        AddressTId = _CreateAddress.data.id,
                                //        AddressReference = _CreateAddress.data.address_id,
                                //    };
                                //    return Response;
                                //}
                                //else
                                //{
                                //    var Response = new ODeliveryAddress.ManageAddress.Response
                                //    {
                                //        ReferenceId = _MDDealAddress.Id,
                                //        ReferenceKey = _MDDealAddress.Guid,
                                //        AddressTId = _CreateAddress.data.id,
                                //        AddressReference = _MDDealAddress.AddressReference,
                                //    };
                                //    return Response;
                                //}
                            }
                            else
                            {
                                return null;
                            }
                        }
                    }
                    else
                    {
                        var AddressDetails = _HCoreContext.HCUAccount.Where(x => x.Id == AccountId)
                            .Select(x => new
                            {
                                CountryName = x.Country.Iso.ToUpper(),
                                EmailAddress = x.EmailAddress,
                                Name = x.Name,
                                AddressLine1 = x.Address,
                                //AddressLine2 = x.Address,
                                ContactNumber = x.ContactNumber,
                                CityName = x.City.Name,
                                StateName = x.State.Name,
                                CityId = x.CityId,
                                StateId = x.StateId,
                                CountryId = x.CountryId,
                                CountryFullName = x.Country.Name,
                                CountryIsd = x.Country.Isd,
                                CountryMobileNumberLength = x.Country.MobileNumberLength
                            })
                            .FirstOrDefault();
                        if (AddressDetails != null)
                        {
                            string Address = AddressDetails.AddressLine1;
                            //if (!string.IsNullOrEmpty(AddressDetails.AddressLine2))
                            //{
                            //    Address += ' ' + AddressDetails.AddressLine2;
                            //}
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.CityName;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.StateName;
                            }
                            if (Address.Length < 30)
                            {
                                Address += " , " + AddressDetails.CountryName;
                            }

                            List<string> _AddressComponent = SplitToLines(Address, 40);
                            string AddressLine1 = _AddressComponent[0];
                            string AddressLine2 = "";
                            if (_AddressComponent.Count() > 1)
                            {
                                AddressLine2 = _AddressComponent[1]; ;

                            }
                            string ContactNumber = HCoreHelper.FormatMobileNumber(AddressDetails.CountryIsd, AddressDetails.ContactNumber, AddressDetails.CountryMobileNumberLength);
                            _Create_UpdateAddress = new Create_UpdateAddress(AddressDetails.CityName,
                                AddressDetails.CountryName,
                                AddressDetails.EmailAddress,
                                AddressDetails.Name,
                                false,
                                 AddressDetails.Name,
                                 AddressLine1,
                                AddressLine2,
                                _Metadata, AddressDetails.Name,
                                "+" + ContactNumber,
                                AddressDetails.StateName, "");
                            _CreateAddress = _FrameworkDelivery.CreateAddress(_Create_UpdateAddress);
                            if (_CreateAddress.status == true)
                            {

                                _HCCoreAddress = new HCCoreAddress();
                                _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                _HCCoreAddress.AccountId = AccountId;
                                _HCCoreAddress.LocationTypeId = 800;
                                _HCCoreAddress.IsPrimaryAddress = 1;
                                _HCCoreAddress.Name = AddressDetails.Name;
                                _HCCoreAddress.EmailAddress = AddressDetails.EmailAddress;
                                _HCCoreAddress.ContactNumber = AddressDetails.ContactNumber;
                                _HCCoreAddress.AddressLine1 = AddressLine1;
                                //_HCCoreAddress.AddressLine2 = AddressLine1;
                                //_HCCoreAddress.AddressLine2 = _CreateAddress.data.line2;
                                //_HCCoreAddress.ZipCode = _CreateAddress.data.zip;
                                //if (_Request.CityAreaId > 0)
                                //{
                                //    _HCCoreAddress.CityAreaId = _Request.CityAreaId;
                                //}
                                _HCCoreAddress.CityId = AddressDetails.CityId;
                                _HCCoreAddress.StateId = AddressDetails.StateId;
                                _HCCoreAddress.CountryId = (int)AddressDetails.CountryId;
                                _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCCoreAddress.CreatedById = AccountId;
                                _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);

                                _MDDealAddress = new MDDealAddress();
                                _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                                _MDDealAddress.AccountId = AccountId;
                                _MDDealAddress.Address = _HCCoreAddress;
                                _MDDealAddress.TId = _CreateAddress.data.id;
                                _MDDealAddress.AddressReference = _CreateAddress.data.address_id;
                                _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                                _HCoreContext.SaveChanges();
                                var Response = new ODeliveryAddress.ManageAddress.Response
                                {
                                    ReferenceId = _MDDealAddress.Id,
                                    ReferenceKey = _MDDealAddress.Guid,
                                    AddressTId = _CreateAddress.data.id,
                                    AddressReference = _MDDealAddress.AddressReference,
                                    CoreAddressId = _HCCoreAddress.Id,
                                    CoreAddressKey = _HCCoreAddress.Guid,
                                    Address = _HCCoreAddress.AddressLine1 + " " + _HCCoreAddress.AddressLine2,
                                };
                                return Response;

                                //var AddressDetailss = _HCoreContext.MDDealAddress.Where(x => x.AccountId == AccountId).FirstOrDefault();
                                //if (AddressDetailss != null)
                                //{
                                //    if (!string.IsNullOrEmpty(_CreateAddress.data.id))
                                //    {
                                //        AddressDetailss.TId = _CreateAddress.data.id;
                                //    }
                                //    if (!string.IsNullOrEmpty(_CreateAddress.data.address_id))
                                //    {
                                //        AddressDetailss.AddressReference = _CreateAddress.data.address_id;
                                //    }
                                //    AddressDetailss.Address = _HCCoreAddress;
                                //    _HCoreContext.SaveChanges();
                                //    var Response = new ODeliveryAddress.ManageAddress.Response
                                //    {
                                //        ReferenceId = AddressDetailss.Id,
                                //        ReferenceKey = AddressDetailss.Guid,
                                //        AddressTId = _CreateAddress.data.id,
                                //        AddressReference = AddressDetailss.AddressReference,
                                //    };
                                //    return Response;
                                //}
                                //else
                                //{
                                //}
                            }
                            else
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageAddress", _Exception, null);
                return null;
            }
        }

        /// <summary>
        /// Description: Gets the address identifier.
        /// </summary>
        /// <param name="Address">The address.</param>
        /// <returns>OAddress.</returns>
        internal OAddress GetAddressId(OAddress Address)
        {
            try
            {
                _Metadata = new Metadata();
                _CreateAddress = new CreateAddress();
                _FrameworkDelivery = new FrameworkDelivery();
                string FromAddressFull = Address.AddressLine1;
                if (!string.IsNullOrEmpty(Address.AddressLine2))
                {
                    FromAddressFull += ' ' + Address.AddressLine2;
                }
                if (FromAddressFull.Length < 30)
                {
                    FromAddressFull += " , " + Address.CityName;
                }
                if (FromAddressFull.Length < 30)
                {
                    FromAddressFull += " , " + Address.StateName;
                }
                if (FromAddressFull.Length < 30)
                {
                    FromAddressFull += " , " + Address.CountryName;
                }
                List<string> _FromAddressComponent = SplitToLines(FromAddressFull, 40);
                string FromAddressLine1 = _FromAddressComponent[0];
                string FromAddressLine2 = "";
                if (_FromAddressComponent.Count() > 1)
                {
                    FromAddressLine2 = _FromAddressComponent[1]; ;
                }
                // Create Address
                _FromAddress = new Create_UpdateAddress(Address.CityName, Address.CountryIso.ToUpper(), Address.EmailAddress, Address.Name,     false, Address.Name, FromAddressLine1, FromAddressLine2, _Metadata, Address.Name,     "+" + Address.ContactNumber, Address.StateName, "");
                _CreateAddress = _FrameworkDelivery.CreateAddress(_FromAddress);
                if (_CreateAddress.status == true)
                {
                    Address.Reference = _CreateAddress.data.address_id;
                    return Address; 
                }
                else
                {
                    return null;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageAddress", _Exception, null);
                return null;
            }
        }

        /// <summary>
        /// Description: Saves the address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveAddress(ODeliveryAddress.Save.Request _Request)
        {
            try
            {
                _Metadata = new Metadata();
                _CreateAddress = new CreateAddress();
                _FrameworkDelivery = new FrameworkDelivery();
                if (_Request.CountryId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.CountryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (_Request.StateId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.StateKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (_Request.CityId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.CityKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                string LocationType;
                if (_Request.Is_Residetnial == true)
                {
                    LocationType = "locationtype.residential";
                }
                else
                {
                    LocationType = "locationtype.work";
                }
                int? LocationTypeId = HCoreHelper.GetSystemHelperId(LocationType, _Request.UserReference);
                if (LocationTypeId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }


                using (_HCoreContext = new HCoreContext())
                {
                    var CountryName = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(a => a.Iso).FirstOrDefault().ToUpper();
                    var StateName = _HCoreContext.HCCoreCountryState.Where(x => x.Id == _Request.StateId && x.Guid == _Request.StateKey).Select(a => a.Name).FirstOrDefault();
                    var CityName = _HCoreContext.HCCoreCountryStateCity.Where(x => x.Id == _Request.CityId && x.Guid == _Request.CityKey).Select(a => a.Name).FirstOrDefault();
                    _Create_UpdateAddress = new Create_UpdateAddress(CityName, CountryName, _Request.EmailAddress, _Request.FirstName, _Request.Is_Residetnial, _Request.LastName, _Request.AddressLine1, _Request.AddressLine2, _Metadata, _Request.Name, "+" + _Request.MobileNumber, StateName, _Request.Zip);
                    _CreateAddress = _FrameworkDelivery.CreateAddress(_Create_UpdateAddress);
                    if (_CreateAddress.status == true)
                    {
                        if (_Request.ReferenceId > 0)
                        {
                            var MDAddress = _HCoreContext.MDDealAddress.Where(x => x.AddressId == _Request.ReferenceId).FirstOrDefault();
                            if (MDAddress != null)
                            {
                                MDAddress.TId = _CreateAddress.data.id;
                                MDAddress.AddressReference = _CreateAddress.data.address_id;
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _MDDealAddress = new MDDealAddress();
                                _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                                _MDDealAddress.AddressId = _Request.ReferenceId;
                                _MDDealAddress.TId = _CreateAddress.data.id;
                                _MDDealAddress.AddressReference = _CreateAddress.data.address_id;
                                _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                                _HCoreContext.SaveChanges();
                            }

                            var Response = new
                            {
                                ReferenceId = _Request.ReferenceId,
                                ReferenceKey = _Request.ReferenceKey,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LSSAVE", Resources.Resources.LSSAVE);
                        }
                        else
                        {
                            _HCCoreAddress = new HCCoreAddress();
                            _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreAddress.AccountId = _Request.AccountId;
                            _HCCoreAddress.LocationTypeId = LocationTypeId;
                            _HCCoreAddress.Name = _CreateAddress.data.first_name + " " + _CreateAddress.data.last_name;
                            _HCCoreAddress.EmailAddress = _CreateAddress.data.email;
                            _HCCoreAddress.ContactNumber = _CreateAddress.data.phone;
                            _HCCoreAddress.AddressLine1 = _CreateAddress.data.line1;
                            _HCCoreAddress.AddressLine2 = _CreateAddress.data.line2;
                            _HCCoreAddress.ZipCode = _CreateAddress.data.zip;
                            if (_Request.CityAreaId > 0)
                            {
                                _HCCoreAddress.CityAreaId = _Request.CityAreaId;
                            }
                            _HCCoreAddress.CityId = _Request.CityId;
                            _HCCoreAddress.StateId = _Request.StateId;
                            _HCCoreAddress.CountryId = (int)_Request.UserReference.CountryId;
                            _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreAddress.CreatedById = _Request.UserReference.AccountId;
                            _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);

                            _MDDealAddress = new MDDealAddress();
                            _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                            _MDDealAddress.Address = _HCCoreAddress;
                            _MDDealAddress.TId = _CreateAddress.data.id;
                            _MDDealAddress.AddressReference = _CreateAddress.data.address_id;
                            _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                            _HCoreContext.SaveChanges();

                            var Response = new
                            {
                                ReferenceId = _HCCoreAddress.Id,
                                ReferenceKey = _HCCoreAddress.Guid,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LSSAVE", Resources.Resources.LSSAVE);
                        }

                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveAddress", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Updates the address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateAddress(ODeliveryAddress.Update.Request _Request)
        {
            try
            {
                _Metadata = new Metadata();
                _UpdateAddress = new UpdateAddress();
                _FrameworkDelivery = new FrameworkDelivery();
                if (_Request.CountryId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (_Request.StateId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (_Request.CityId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                string LocationType;
                if (_Request.Is_Residetnial == true)
                {
                    LocationType = "locationtype.residential";
                }
                else
                {
                    LocationType = "locationtype.work";
                }
                int? LocationTypeId = HCoreHelper.GetSystemHelperId(LocationType, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    var CountryName = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(a => a.Iso).FirstOrDefault().ToUpper();
                    var StateName = _HCoreContext.HCCoreCountryState.Where(x => x.Id == _Request.StateId && x.Guid == _Request.StateKey).Select(a => a.Name).FirstOrDefault();
                    var CityName = _HCoreContext.HCCoreCountryStateCity.Where(x => x.Id == _Request.CityId && x.Guid == _Request.CityKey).Select(a => a.Name).FirstOrDefault();

                    _Create_UpdateAddress = new Create_UpdateAddress(CityName, CountryName, _Request.EmailAddress, _Request.FirstName, _Request.Is_Residetnial, _Request.LastName, _Request.AddressLine1, _Request.AddressLine2, _Metadata, _Request.Name, _Request.MobileNumber, StateName, _Request.Zip);
                    _UpdateAddress = _FrameworkDelivery.UpdateAddress(_Create_UpdateAddress, _Request.AddressId);
                    if (_UpdateAddress.status == true)
                    {
                        var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (AddressDetails != null)
                        {
                            if (_Request.UserReference.CountryId > 0 && _Request.UserReference.CountryId != AddressDetails.CountryId)
                            {
                                AddressDetails.CountryId = (int)_Request.UserReference.CountryId;
                            }
                            if (_Request.StateId > 0 && _Request.StateId != AddressDetails.StateId)
                            {
                                AddressDetails.StateId = _Request.StateId;
                            }
                            if (_Request.CityId > 0 && _Request.CityId != AddressDetails.CityId)
                            {
                                AddressDetails.CityId = _Request.CityId;
                            }
                            if (_Request.CityAreaId > 0 && _Request.CityAreaId != AddressDetails.CityAreaId)
                            {
                                AddressDetails.CityAreaId = _Request.CityAreaId;
                            }
                            if (!string.IsNullOrEmpty(_Request.FirstName) && !string.IsNullOrEmpty(_Request.LastName))
                            {
                                AddressDetails.Name = _Request.FirstName + " " + _Request.LastName;
                            }
                            if (!string.IsNullOrEmpty(_Request.EmailAddress) && AddressDetails.EmailAddress != _Request.EmailAddress)
                            {
                                AddressDetails.EmailAddress = _Request.EmailAddress;
                            }
                            if (!string.IsNullOrEmpty(_Request.MobileNumber) && AddressDetails.ContactNumber != _Request.MobileNumber)
                            {
                                AddressDetails.ContactNumber = _Request.MobileNumber;
                            }
                            if (LocationTypeId > 0 && AddressDetails.LocationTypeId != LocationTypeId)
                            {
                                AddressDetails.LocationTypeId = LocationTypeId;
                            }

                            AddressDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            AddressDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();

                            var Response = new
                            {
                                ReferenceId = AddressDetails.Id,
                                ReferenceKey = AddressDetails.Guid,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LSUPDATE", Resources.Resources.LSUPDATE);
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateAddress", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the address.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAddress(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREFKEY);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var AddressDetails = _HCoreContext.MDDealAddress.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                         .Select(x => new ODeliveryAddress.Details
                                         {
                                             ReferenceId = x.Id,
                                             ReferenceKey = x.Guid,
                                             AccountId = x.AccountId,
                                             AccountKey = x.Account.Guid,
                                             AccountDisplayName = x.Account.DisplayName,
                                             CountryId = x.Address.CountryId,
                                             CountryKey = x.Address.Country.SystemName,
                                             CountryName = x.Address.Country.Name,
                                             StateId = x.Address.StateId,
                                             StateKey = x.Address.State.SystemName,
                                             StateName = x.Address.State.Name,
                                             CityId = x.Address.CityId,
                                             CityKey = x.Address.City.SystemName,
                                             CityName = x.Address.City.Name,
                                             CityAreaId = x.Address.CityAreaId,
                                             CityAreaKey = x.Address.CityArea.SystemName,
                                             CityAreaName = x.Address.CityArea.Name,
                                             Name = x.Address.Name,
                                             EmailAddress = x.Address.EmailAddress,
                                             MobileNumber = x.Address.ContactNumber,
                                             AdditionalMobileNumber = x.Address.AdditionalContactNumber,
                                             AddressLine1 = x.Address.AddressLine1,
                                             AddressLine2 = x.Address.AddressLine2,
                                             LocationTypeId = x.Address.LocationTypeId,
                                             LocationTypeCode = x.Address.LocationType.SystemName,
                                             LocationTypeName = x.Address.LocationType.Name,
                                             Zip = x.Address.ZipCode,
                                             CreateDate = x.Address.CreateDate,
                                             CreatedBYId = x.Address.CreatedById,
                                             CreatedByDisplayName = x.Address.CreatedBy.DisplayName,
                                             ModifyDate = x.Address.ModifyDate,
                                             ModifyById = x.Address.ModifyById,
                                             ModifyByDisplayName = x.Address.ModifyBy.DisplayName,
                                             StatusId = x.Address.StatusId,
                                             StatusCode = x.Address.Status.SystemName,
                                             StatusName = x.Address.Status.Name,
                                         })
                                         .FirstOrDefault();
                    if (AddressDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, AddressDetails, "LS0200", Resources.Resources.LS0200);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAddress", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the address list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAddresses(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.MDDealAddress.Where(x => x.Address.StatusId == HelperStatus.Default.Active).Select(x => new ODeliveryAddress.List
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            AccountId = x.AccountId,
                            AccountKey = x.Account.Guid,
                            AccountDisplayName = x.Account.DisplayName,
                            CountryId = x.Address.CountryId,
                            CountryKey = x.Address.Country.SystemName,
                            CountryName = x.Address.Country.Name,
                            StateId = x.Address.StateId,
                            StateKey = x.Address.State.SystemName,
                            StateName = x.Address.State.Name,
                            CityId = x.Address.CityId,
                            CityKey = x.Address.City.SystemName,
                            CityName = x.Address.City.Name,
                            CityAreaId = x.Address.CityAreaId,
                            CityAreaKey = x.Address.CityArea.SystemName,
                            CityAreaName = x.Address.CityArea.Name,
                            Name = x.Address.Name,
                            EmailAddress = x.Address.EmailAddress,
                            MobileNumber = x.Address.ContactNumber,
                            AdditionalMobileNumber = x.Address.AdditionalContactNumber,
                            AddressLine1 = x.Address.AddressLine1,
                            AddressLine2 = x.Address.AddressLine2,
                            LocationTypeId = x.Address.LocationTypeId,
                            LocationTypeCode = x.Address.LocationType.SystemName,
                            LocationTypeName = x.Address.LocationType.Name,
                            Zip = x.Address.ZipCode,
                            CreateDate = x.Address.CreateDate,
                            CreatedBYId = x.Address.CreatedById,
                            CreatedByDisplayName = x.Address.CreatedBy.DisplayName,
                            ModifyDate = x.Address.ModifyDate,
                            ModifyById = x.Address.ModifyById,
                            ModifyByDisplayName = x.Address.ModifyBy.DisplayName,
                            StatusId = x.Address.StatusId,
                            StatusCode = x.Address.Status.SystemName,
                            StatusName = x.Address.Status.Name,
                        }).Where(_Request.SearchCondition)
                                             .Count();
                    }

                    List<ODeliveryAddress.List> Addresses = _HCoreContext.MDDealAddress.Where(x => x.Address.StatusId == HelperStatus.Default.Active)
                                                            .Select(x => new ODeliveryAddress.List
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                AccountId = x.AccountId,
                                                                AccountKey = x.Account.Guid,
                                                                AccountDisplayName = x.Account.DisplayName,
                                                                CountryId = x.Address.CountryId,
                                                                CountryKey = x.Address.Country.SystemName,
                                                                CountryName = x.Address.Country.Name,
                                                                StateId = x.Address.StateId,
                                                                StateKey = x.Address.State.SystemName,
                                                                StateName = x.Address.State.Name,
                                                                CityId = x.Address.CityId,
                                                                CityKey = x.Address.City.SystemName,
                                                                CityName = x.Address.City.Name,
                                                                CityAreaId = x.Address.CityAreaId,
                                                                CityAreaKey = x.Address.CityArea.SystemName,
                                                                CityAreaName = x.Address.CityArea.Name,
                                                                Name = x.Address.Name,
                                                                EmailAddress = x.Address.EmailAddress,
                                                                MobileNumber = x.Address.ContactNumber,
                                                                AdditionalMobileNumber = x.Address.AdditionalContactNumber,
                                                                AddressLine1 = x.Address.AddressLine1,
                                                                AddressLine2 = x.Address.AddressLine2,
                                                                LocationTypeId = x.Address.LocationTypeId,
                                                                LocationTypeCode = x.Address.LocationType.SystemName,
                                                                LocationTypeName = x.Address.LocationType.Name,
                                                                Zip = x.Address.ZipCode,
                                                                CreateDate = x.Address.CreateDate,
                                                                CreatedBYId = x.Address.CreatedById,
                                                                CreatedByDisplayName = x.Address.CreatedBy.DisplayName,
                                                                ModifyDate = x.Address.ModifyDate,
                                                                ModifyById = x.Address.ModifyById,
                                                                ModifyByDisplayName = x.Address.ModifyBy.DisplayName,
                                                                StatusId = x.Address.StatusId,
                                                                StatusCode = x.Address.Status.SystemName,
                                                                StatusName = x.Address.Status.Name,
                                                            }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();

                    foreach (var DataItem in Addresses)
                    {

                    }

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Addresses, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "LS0200", Resources.Resources.LS0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAddresses", _Exception, _Request.UserReference);
            }
        }

        internal AddressResponse CreateAddress(AddressRequest _Request)
        {
            try
            {
                using(_HCoreContext = new HCoreContext())
                {
                    string CountryIsd = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.Isd).FirstOrDefault();
                    string VMobileNumber = FormatMobileNumber(_Request.ContactNumber);
                    string MobileNumber = HCoreHelper.FormatMobileNumber(CountryIsd, VMobileNumber, VMobileNumber.Length);
                    string UserName = _AppConfig.AppUserPrefix + MobileNumber;

                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => (x.MobileNumber == MobileNumber || x.User.Username == UserName) && x.AccountTypeId == UserAccountType.Appuser).FirstOrDefault();
                    if(AccountDetails != null)
                    {
                        _HCCoreAddress = new HCCoreAddress();
                        _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreAddress.AccountId = AccountDetails.Id;
                        if (_Request.IsPrimary != null)
                        {
                            if (_Request.IsPrimary == true)
                            {
                                _HCCoreAddress.IsPrimaryAddress = 1;
                                var CustAddressList = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == AccountDetails.Id).ToList();
                                foreach (var CustAdd in CustAddressList)
                                {
                                    CustAdd.IsPrimaryAddress = 0;
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _HCCoreAddress.DisplayName = _Request.Name;
                        }
                        else
                        {
                            _HCCoreAddress.DisplayName = AccountDetails.DisplayName;
                        }
                        if (_Request.LocationTypeId > 0)
                        {
                            _HCCoreAddress.LocationTypeId = _Request.LocationTypeId;
                        }
                        else
                        {
                            _HCCoreAddress.LocationTypeId = 800;
                        }

                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _HCCoreAddress.Name = _Request.Name;
                        }
                        else
                        {
                            _HCCoreAddress.Name = AccountDetails.Name;
                        }

                        if (!string.IsNullOrEmpty(_Request.ContactNumber))
                        {
                            _HCCoreAddress.ContactNumber = _Request.ContactNumber;
                        }
                        else
                        {
                            _HCCoreAddress.ContactNumber = AccountDetails.MobileNumber;
                        }


                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            _HCCoreAddress.EmailAddress = _Request.EmailAddress;
                        }
                        else
                        {
                            _HCCoreAddress.EmailAddress = AccountDetails.EmailAddress;
                        }
                        _HCCoreAddress.AddressLine1 = _Request.AddressLine1;
                        if (!string.IsNullOrEmpty(_Request.AddressLine2))
                        {
                            _HCCoreAddress.AddressLine2 = _Request.AddressLine2;
                        }
                        if (_Request.CityAreaId > 0)
                        {
                            _HCCoreAddress.CityAreaId = _Request.CityAreaId;
                        }
                        if (_Request.CityId > 0)
                        {
                            _HCCoreAddress.CityId = _Request.CityId;
                        }
                        if (_Request.StateId > 0)
                        {
                            _HCCoreAddress.StateId = _Request.StateId;
                        }
                        _HCCoreAddress.CountryId = (int)AccountDetails.CountryId;
                        _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreAddress.CreatedById = AccountDetails.Id;
                        _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);

                        _MDDealAddress = new MDDealAddress();
                        _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                        _MDDealAddress.AccountId = AccountDetails.Id;
                        _MDDealAddress.TId = HCoreHelper.GenerateRandomNumber(11);
                        _MDDealAddress.AddressReference = HCoreHelper.GenerateRandomNumber(11);
                        _MDDealAddress.Address = _HCCoreAddress;
                        _HCoreContext.MDDealAddress.Add(_MDDealAddress);

                        _HCoreContext.SaveChanges();

                        AddressResponse _AddressResponse = new AddressResponse();
                        _AddressResponse.Status = "Success";
                        _AddressResponse.Message = "Address created successfully.";
                        _AddressResponse.Address = _HCCoreAddress.AddressLine1 + ", " + _Request.CityName + ", " + _Request.StateName;
                        _AddressResponse.AddressId = _HCCoreAddress.Id;
                        _AddressResponse.CustomerAddressId = _MDDealAddress.Id;
                        _AddressResponse.AccountId = AccountDetails.Id;
                        _AddressResponse.AccountKey = AccountDetails.Guid;

                        return _AddressResponse;
                    }
                    else
                    {
                        string AccountPin = HCoreHelper.GenerateRandomNumber(4);
                        if (HostEnvironment == HostEnvironmentType.Dev || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Test)
                        {
                            AccountPin = "1234";
                        }
                        string[] FullName = _Request.Name.Split(" ");
                        string FirstName = string.Empty, LastName = string.Empty, MiddleName = string.Empty;
                        if (FullName.Length == 0 || FullName.Length == 1)
                        {
                            FirstName = _Request.Name;
                        }
                        else if (FullName.Length == 2)
                        {
                            FirstName = FullName[0];
                            LastName = FullName[1];
                        }
                        else if (FullName.Length == 3)
                        {
                            FirstName = FullName[0];
                            MiddleName = FullName[1];
                            LastName = FullName[2];
                        }
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = UserName;
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(AccountPin);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        _HCUAccountAuth.CreatedById = 1;
                        _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);

                        // Create user account
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                        _HCUAccount.MobileNumber = MobileNumber;
                        _HCUAccount.DisplayName = _Request.Name;
                        _HCUAccount.Name = _Request.Name;
                        _HCUAccount.FirstName = FirstName;
                        if (!string.IsNullOrEmpty(MiddleName))
                        {
                            _HCUAccount.MiddleName = MiddleName;
                        }
                        _HCUAccount.LastName = LastName;
                        _HCUAccount.EmailAddress = _Request.EmailAddress;
                        _HCUAccount.ReferralCode = HCoreHelper.GenerateSystemName(_HCUAccount.DisplayName);
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccountPin);
                        _HCUAccount.ContactNumber = MobileNumber;
                        _HCUAccount.CountryId = _Request.CountryId;
                        _HCUAccount.EmailVerificationStatus = 1;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.NumberVerificationStatus = 0;

                        _Random = new Random();
                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");

                        _HCUAccount.AccountCode = AccountCode;
                        _HCUAccount.ReferralCode = MobileNumber;
                        _HCUAccount.RegistrationSourceId = RegistrationSource.DealsWebsite;
                        _HCUAccount.CreatedById = 1;
                        _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.User = _HCUAccountAuth;
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();

                        _HCCoreAddress = new HCCoreAddress();
                        _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                        if (_Request.IsPrimary == true)
                        {
                            _HCCoreAddress.IsPrimaryAddress = 1;
                        }
                        _HCCoreAddress.DisplayName = _Request.Name;

                        if (_Request.LocationTypeId > 0)
                        {
                            _HCCoreAddress.LocationTypeId = _Request.LocationTypeId;
                        }
                        else
                        {
                            _HCCoreAddress.LocationTypeId = 800;
                        }

                        _HCCoreAddress.Name = _Request.Name;
                        _HCCoreAddress.ContactNumber = _Request.ContactNumber;
                        _HCCoreAddress.EmailAddress = _Request.EmailAddress;
                        _HCCoreAddress.AddressLine1 = _Request.AddressLine1;
                        if(!string.IsNullOrEmpty(_Request.AddressLine2))
                        {
                            _HCCoreAddress.AddressLine2 = _Request.AddressLine2;
                        }
                        if (_Request.CityAreaId > 0)
                        {
                            _HCCoreAddress.CityAreaId = _Request.CityAreaId;
                        }
                        if (_Request.CityId > 0)
                        {
                            _HCCoreAddress.CityId = _Request.CityId;
                        }
                        if (_Request.StateId > 0)
                        {
                            _HCCoreAddress.StateId = _Request.StateId;
                        }
                        _HCCoreAddress.CountryId = (int)_Request.CountryId;
                        _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreAddress.CreatedBy = _HCUAccount;
                        _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                        _HCCoreAddress.Account = _HCUAccount;
                        _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);

                        _MDDealAddress = new MDDealAddress();
                        _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                        _MDDealAddress.TId = HCoreHelper.GenerateRandomNumber(11);
                        _MDDealAddress.AddressReference = HCoreHelper.GenerateRandomNumber(11);
                        _MDDealAddress.Address = _HCCoreAddress;
                        _MDDealAddress.Account = _HCUAccount;
                        _HCoreContext.MDDealAddress.Add(_MDDealAddress);

                        _HCoreContext.SaveChanges();

                        if (HostEnvironment == HostEnvironmentType.Live)
                        {
                            HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, MobileNumber, "Welcome to Thank U Cash: Your Bal: N0. Your redeeming PIN is: " + AccountPin + ".", _HCUAccount.Id, _HCUAccount.Guid);
                        }

                        AddressResponse _AddressResponse = new AddressResponse();
                        _AddressResponse.Status = "Success";
                        _AddressResponse.Message = "Address created successfully.";
                        _AddressResponse.Address = _HCCoreAddress.AddressLine1 + ", " + _Request.CityName + ", " + _Request.StateName;
                        _AddressResponse.AddressId = _HCCoreAddress.Id;
                        _AddressResponse.CustomerAddressId = _MDDealAddress.Id;
                        _AddressResponse.AccountId = _HCUAccount.Id;
                        _AddressResponse.AccountKey = _HCUAccount.Guid;

                        return _AddressResponse;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateAddress", _Exception, _Request.UserReference);
                AddressResponse _AddressResponse = new AddressResponse();
                _AddressResponse.Status = "Error";
                _AddressResponse.Message = "Unable to create address. Please try after some time.";
                return _AddressResponse;
            }
        }

        private static string FormatMobileNumber(string MobileNumber)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                var _StringBuilder = new StringBuilder(MobileNumber);
                _StringBuilder.Remove(0, 1);
                MobileNumber = _StringBuilder.ToString();
                return MobileNumber;
            }
            else
            {
                return MobileNumber;
            }
        }
    }
}

