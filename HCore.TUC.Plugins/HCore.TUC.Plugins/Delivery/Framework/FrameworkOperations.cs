//==================================================================================
// FileName: FrameworkOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Delivery.Framework;
using Delivery.Object.Requests.Parcels;
using Delivery.Object.Response.Parcels;
using Delivery.Object.Response.Rates;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using static HCore.Helper.HCoreConstant;
using Dellyman;
using Dellyman.DellymanObject.Request;
using Dellyman.DellymanObject.Response;
using Delivery.Object.Response.Shipments;
using System.Text;
using Akka.Actor;
using HCore.Integration.DeliveryIntegration.GoShiip;
using HCore.Integration.DeliveryIntegration.GoShiip.Requests;
using HCore.Integration.DeliveryIntegration.GoShiip.Response;
using Microsoft.EntityFrameworkCore;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkOperations
    {

        OOperations.Pricing.Response? _PricingResponse;
        List<OOperations.Pricing.Partner>? _DeliveryPartners;
        MDDealAddress? _MDDealAddress;
        HCoreContext? _HCoreContext;
        LSParcel? _LSParcel;
        LSItems? _LSItems;
        FrameworkDelivery? _FrameworkDelivery;
        CreateParcelRequest? _CreateParcelRequest;
        CreateParcel? _CreateParcel;
        List<ParcelItems>? _ParcelItems;
        ParcelMetaData? _ParcelMetaData;
        OShipments.Save.Request? _OShipmentsRequest;
        FrameworkShipment? _FrameworkShipment;
        OShipments.CheckRates? _RateRequest;
        FrameworkAddress? _FrameworkAddress;
        LSShipments? _LSShipments;
        DellymanOrders? _DellymanOrders;
        OOperations.Pricing.PricingResponse? _PricingResponses;
        HCCoreAddress? _HCCoreAddress;
        GoShiip? _Shiip;

        /// <summary>
        /// Description: Gets the pricing.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPricing(OOperations.Pricing.Request _Request)
        {
            try
            {
                _FrameworkAddress = new FrameworkAddress();
                _FrameworkShipment = new FrameworkShipment();
                _OShipmentsRequest = new OShipments.Save.Request();
                _FrameworkDelivery = new FrameworkDelivery();
                _ParcelItems = new List<ParcelItems>();
                _ParcelMetaData = new ParcelMetaData();
                _RateRequest = new OShipments.CheckRates();

                using (_HCoreContext = new HCoreContext())
                {
                    var Dealdetails = _HCoreContext.MDDeal
                        .Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey)
                        .Select(x => new
                        {
                            AccountId = x.AccountId,
                            Title = x.Title,
                            SellingPrice = x.SellingPrice,
                            DealId = x.Id,
                            CurrencyNotation = x.Account.Country.CurrencyNotation,
                        })
                        .FirstOrDefault();

                    var PackageDetails = _HCoreContext.LSPackages.Where(x => x.DealId == _Request.DealId)
                        .Select(x => new
                        {
                            PackageId = x.Id,
                            PackageKey = x.Guid,

                            Weight = x.Weight,
                            PackagingId = x.Id,
                            Description = x.Description,
                            WeightUnit = x.WeightUnit,
                            PackagingReference = x.PackagingId,
                        })
                        .FirstOrDefault();
                    if (PackageDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0020", "Delivery options not available");
                    }

                    ODeliveryAddress.ManageAddress.Response CustomerAddressDetails = _FrameworkAddress.ManageAddress(_Request.AccountId, _Request.AddressId);
                    if (CustomerAddressDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0020", "Delivery is not available at your location. Please change address and try again");
                    }
                    ODeliveryAddress.ManageAddress.Response MerchantAddressDetails = _FrameworkAddress.ManageAddress(Dealdetails.AccountId, 0);
                    if (MerchantAddressDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0020", "Delivery is not available at your location. Please change address and try again");
                    }

                    _ParcelItems = new List<ParcelItems>();
                    _ParcelItems.Add(new ParcelItems
                    {
                        description = HCoreHelper.GenerateRandomNumber(42),
                        name = Dealdetails.Title,
                        currency = Dealdetails.CurrencyNotation,
                        value = (long?)Dealdetails.SellingPrice,
                        quantity = 1,
                        weight = PackageDetails.Weight
                    });
                    _CreateParcelRequest = new CreateParcelRequest(PackageDetails.Description, PackageDetails.PackagingReference, PackageDetails.WeightUnit, _ParcelItems, _ParcelMetaData);
                    _CreateParcel = _FrameworkDelivery.CreateParcel(_CreateParcelRequest);
                    if (_CreateParcel.status == true)
                    {
                        _LSParcel = new LSParcel();
                        _LSParcel.Guid = HCoreHelper.GenerateGuid();
                        if (PackageDetails != null)
                        {
                            _LSParcel.PackagingId = PackageDetails.PackagingId;
                        }

                        _LSParcel.Description = _CreateParcel.data.description;
                        _LSParcel.TotalWeight = _CreateParcel.data.total_weight;
                        _LSParcel.WeightUnit = _CreateParcel.data.weight_unit;
                        _LSParcel.ParcelId = _CreateParcel.data.parcel_id;
                        _LSParcel.CreateDate = _CreateParcel.data.created_at;
                        _LSParcel.CreatedById = _Request.UserReference.AccountId;
                        _LSParcel.TId = _CreateParcel.data.id;
                        _LSParcel.StatusId = HelperStatus.Default.Active;

                        foreach (var Item in _CreateParcel.data.items)
                        {
                            _LSItems = new LSItems();
                            _LSItems.Guid = HCoreHelper.GenerateGuid();
                            _LSItems.Description = Item.description;
                            _LSItems.Name = Item.name;
                            _LSItems.Currency = Item.currency;
                            _LSItems.Price = Item.value;
                            _LSItems.Quantity = Item.quantity;
                            _LSItems.Weight = Item.weight;
                            _LSItems.Parcel = _LSParcel;
                            _HCoreContext.LSItems.Add(_LSItems);
                        }

                        _HCoreContext.LSParcel.Add(_LSParcel);

                        _HCoreContext.SaveChanges();

                        var ParcelId = _HCoreContext.LSParcel.Where(x => x.ParcelId == _CreateParcel.data.parcel_id).Select(a => a.Id).FirstOrDefault();
                        _OShipmentsRequest.CustomerId = (long)CustomerAddressDetails.AccountId;
                        _OShipmentsRequest.AccountId = (long)CustomerAddressDetails.AccountId;
                        _OShipmentsRequest.CustomerKey = CustomerAddressDetails.AccountKey;


                        _OShipmentsRequest.CustomerAddressReference = CustomerAddressDetails.AddressReference;
                        _OShipmentsRequest.MerchantAddressReference = MerchantAddressDetails.AddressReference;
                        _OShipmentsRequest.MerchantId = Dealdetails.AccountId;

                        _OShipmentsRequest.ParcelId = _LSParcel.Id;
                        _OShipmentsRequest.ParcelReference = _CreateParcel.data.parcel_id;


                        _OShipmentsRequest.FromAddressId = MerchantAddressDetails.CoreAddressId;
                        _OShipmentsRequest.MerchantAddressReference = MerchantAddressDetails.AddressReference;

                        _OShipmentsRequest.ToAddressId = CustomerAddressDetails.CoreAddressId;


                        _OShipmentsRequest.MerchantId = Dealdetails.AccountId;
                        _OShipmentsRequest.DealId = Dealdetails.DealId;
                        _OShipmentsRequest.DealPrice = Dealdetails.SellingPrice;






                        OShipments.Save.Response CreateShipmentResponse = _FrameworkShipment.CreateShipment(_OShipmentsRequest);
                        if (CreateShipmentResponse != null && CreateShipmentResponse.Status != "01")
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                _RateRequest.ParcelId = _CreateParcel.data.parcel_id;
                                _RateRequest.ShipmentId = CreateShipmentResponse.ShipmentId;


                                _RateRequest.ShipmentId = CreateShipmentResponse.ShipmentId;
                                _RateRequest.ParcelId = _CreateParcel.data.parcel_id;
                                _RateRequest.FromAddressId = MerchantAddressDetails.AddressReference;
                                _RateRequest.ToAddressId = CustomerAddressDetails.AddressReference;
                                GetRatesForShipments GetRatesforShipment = _FrameworkShipment.CheckRateForShioments(_RateRequest);
                                if (GetRatesforShipment != null)
                                {
                                    _PricingResponse = new OOperations.Pricing.Response();
                                    _PricingResponse.TrackingId = CreateShipmentResponse.ReferenceId;
                                    _PricingResponse.TrackingKey = CreateShipmentResponse.ReferenceKey;

                                    _PricingResponse.ShipmentId = CreateShipmentResponse.ReferenceId;
                                    _PricingResponse.ShipmentKey = CreateShipmentResponse.ReferenceKey;
                                    _PricingResponse.ShipmentReference = CreateShipmentResponse.ShipmentId;

                                    _PricingResponse.ParcelId = _LSParcel.Id;
                                    _PricingResponse.ParcelKey = _LSParcel.Guid;

                                    _PricingResponse.PackageId = PackageDetails.PackageId;
                                    _PricingResponse.PackageKey = PackageDetails.PackageKey;


                                    _PricingResponse.FromAddressId = MerchantAddressDetails.ReferenceId;
                                    _PricingResponse.FromAddressKey = MerchantAddressDetails.ReferenceKey;
                                    _PricingResponse.FromAddressReference = MerchantAddressDetails.AddressReference;


                                    _PricingResponse.ToAddressId = CustomerAddressDetails.ReferenceId;
                                    _PricingResponse.ToAddressKey = CustomerAddressDetails.ReferenceKey;
                                    _PricingResponse.ToAddressReference = CustomerAddressDetails.AddressReference;


                                    _DeliveryPartners = new List<OOperations.Pricing.Partner>();
                                    foreach (var Item in GetRatesforShipment.data)
                                    {
                                        _DeliveryPartners.Add(new OOperations.Pricing.Partner
                                        {
                                            ReferenceKey = Item.rate_id,
                                            Name = Item.carrier_name,
                                            IconUrl = Item.carrier_logo,
                                            DeliveryCharge = Math.Round(Item.amount, 2),
                                            DeliveryTime = Item.delivery_time,
                                            DeliveryEta = Item.delivery_eta,
                                            CarrierId = Item.carrier_reference,
                                        });
                                    }
                                    _PricingResponse.DeliveryPartners = _DeliveryPartners;
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PricingResponse, "LSSAVE", Resources.Resources.LSSAVE);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery is not available at your location. Please change address and try agian");
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery is not available at your location. Please change address and try agian");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery is not available at your location. Please change address and try agian");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPricing", _Exception, _Request.UserReference);
            }
        }
        /// <summary>
        /// Description: Gets the shipment pricing.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetShipmentPricing(OOperations.Pricing.Request _Request)
        {
            try
            {
                _FrameworkAddress = new FrameworkAddress();
                _FrameworkShipment = new FrameworkShipment();
                _OShipmentsRequest = new OShipments.Save.Request();
                _FrameworkDelivery = new FrameworkDelivery();
                _ParcelItems = new List<ParcelItems>();
                _ParcelMetaData = new ParcelMetaData();
                _RateRequest = new OShipments.CheckRates();

                using (_HCoreContext = new HCoreContext())
                {
                    var Dealdetails = _HCoreContext.MDDeal
                      .Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey)
                      .Select(x => new
                      {
                          AccountId = x.AccountId,
                          Title = x.Title,
                          SellingPrice = x.SellingPrice,
                          DealId = x.Id,
                          CurrencyNotation = x.Account.Country.CurrencyNotation,
                          CountryIso = x.Account.Country.Iso,
                      })
                      .FirstOrDefault();
                    if (_Request.ToAddressComponent != null)
                    {
                        _Request.ToAddressComponent.ContactNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.ToAddressComponent.ContactNumber, (int)_Request.UserReference.CountryMobileNumberLength);
                    }
                    _Request.ToAddressComponent.CountryIso = Dealdetails.CountryIso;
                    OAddress ToAddress = _FrameworkAddress.GetAddressId(_Request.ToAddressComponent);
                    if (ToAddress == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery is not possible at selected address");
                    }


                    var PackageDetails = _HCoreContext.LSPackages.Where(x => x.DealId == _Request.DealId)
                        .Select(x => new
                        {
                            PackageId = x.Id,
                            PackageKey = x.Guid,

                            Weight = x.Weight,
                            PackagingId = x.Id,
                            Description = x.Description,
                            WeightUnit = x.WeightUnit,
                            PackagingReference = x.PackagingId,
                        })
                        .FirstOrDefault();
                    if (PackageDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0020", "Delivery options not available");
                    }


                    ODeliveryAddress.ManageAddress.Response MerchantAddressDetails = _HCoreContext.MDDealAddress.Where(x => x.AccountId == Dealdetails.AccountId).Select(x => new ODeliveryAddress.ManageAddress.Response
                    {
                        CoreAddressId = x.Address.Id,
                        CoreAddressKey = x.Address.Guid,
                        ReferenceId = x.Id,
                        ReferenceKey = x.Guid,
                        AddressReference = x.AddressReference,
                    }).FirstOrDefault();
                    if (MerchantAddressDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery address error. Please try after some time");
                    }
                    _ParcelItems = new List<ParcelItems>();
                    _ParcelItems.Add(new ParcelItems
                    {
                        description = HCoreHelper.GenerateRandomNumber(42),
                        name = Dealdetails.Title,
                        currency = Dealdetails.CurrencyNotation,
                        value = (long?)Dealdetails.SellingPrice,
                        quantity = 1,
                        weight = PackageDetails.Weight
                    });
                    _CreateParcelRequest = new CreateParcelRequest(PackageDetails.Description, PackageDetails.PackagingReference, PackageDetails.WeightUnit, _ParcelItems, _ParcelMetaData);
                    _CreateParcel = _FrameworkDelivery.CreateParcel(_CreateParcelRequest);
                    if (_CreateParcel.status == true)
                    {
                        _LSParcel = new LSParcel();
                        _LSParcel.Guid = HCoreHelper.GenerateGuid();
                        if (PackageDetails.PackagingId > 0)
                        {
                            _LSParcel.PackagingId = PackageDetails.PackagingId;
                        }
                        _LSParcel.DealId = Dealdetails.DealId;
                        _LSParcel.Description = _CreateParcel.data.description;
                        _LSParcel.TotalWeight = _CreateParcel.data.total_weight;
                        _LSParcel.WeightUnit = _CreateParcel.data.weight_unit;
                        _LSParcel.ParcelId = _CreateParcel.data.parcel_id;
                        _LSParcel.CreateDate = _CreateParcel.data.created_at;
                        if (_Request.UserReference.AccountId > 0)
                        {
                            _LSParcel.CreatedById = _Request.UserReference.AccountId;
                        }
                        _LSParcel.TId = _CreateParcel.data.id;
                        _LSParcel.StatusId = HelperStatus.Default.Active;

                        foreach (var Item in _CreateParcel.data.items)
                        {
                            _LSItems = new LSItems();
                            _LSItems.Guid = HCoreHelper.GenerateGuid();
                            _LSItems.Description = Item.description;
                            _LSItems.Name = Item.name;
                            _LSItems.Currency = Item.currency;
                            _LSItems.Price = Item.value;
                            _LSItems.Quantity = Item.quantity;
                            _LSItems.Weight = Item.weight;
                            _LSItems.Parcel = _LSParcel;
                            _HCoreContext.LSItems.Add(_LSItems);
                        }
                        _HCoreContext.LSParcel.Add(_LSParcel);

                        _LSShipments = new LSShipments();
                        _LSShipments.Guid = HCoreHelper.GenerateGuid();
                        if (Dealdetails.AccountId > 0)
                        {
                            _LSShipments.MerchantId = Dealdetails.AccountId;
                        }
                        _LSShipments.FromAddressId = MerchantAddressDetails.CoreAddressId;
                        _LSShipments.ReturnAddressId = MerchantAddressDetails.CoreAddressId;
                        _LSShipments.Parcel = _LSParcel;
                        _LSShipments.DealId = Dealdetails.DealId;
                        _LSShipments.DealPrice = Dealdetails.SellingPrice;
                        _LSShipments.StatusId = HelperStatus.OrderStatus.New;
                        _LSShipments.CreateDate = HCoreHelper.GetGMTDateTime();
                        if (_Request.UserReference.AccountId > 0)
                        {
                            _LSShipments.CreatedById = _Request.UserReference.AccountId;
                        }
                        _HCoreContext.LSShipments.Add(_LSShipments);
                        _HCoreContext.SaveChanges();

                        _OShipmentsRequest.FromAddressId = MerchantAddressDetails.CoreAddressId;
                        _OShipmentsRequest.MerchantAddressReference = MerchantAddressDetails.AddressReference;
                        _OShipmentsRequest.CustomerAddressReference = ToAddress.Reference;
                        _OShipmentsRequest.ParcelId = _LSParcel.Id;
                        _OShipmentsRequest.ParcelReference = _CreateParcel.data.parcel_id;
                        _OShipmentsRequest.MerchantId = Dealdetails.AccountId;
                        _OShipmentsRequest.DealId = Dealdetails.DealId;
                        _OShipmentsRequest.DealPrice = Dealdetails.SellingPrice;

                        OShipments.Save.Response CreateShipmentResponse = _FrameworkShipment.CreateShipment(_OShipmentsRequest);
                        if (CreateShipmentResponse != null && CreateShipmentResponse.Status != "01")
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                _RateRequest.ShipmentId = CreateShipmentResponse.ShipmentId;
                                _RateRequest.ParcelId = _CreateParcel.data.parcel_id;
                                _RateRequest.FromAddressId = MerchantAddressDetails.AddressReference;
                                _RateRequest.ToAddressId = ToAddress.Reference;
                                GetRatesForShipments GetRatesforShipment = _FrameworkShipment.CheckRateForShioments(_RateRequest);
                                if (GetRatesforShipment != null)
                                {
                                    _PricingResponse = new OOperations.Pricing.Response();
                                    _PricingResponse.ParcelId = _LSParcel.Id;
                                    _PricingResponse.ParcelKey = _LSParcel.Guid;

                                    _PricingResponse.ShipmentId = CreateShipmentResponse.ReferenceId;
                                    _PricingResponse.ShipmentKey = CreateShipmentResponse.ReferenceKey;
                                    _PricingResponse.ShipmentReference = CreateShipmentResponse.ShipmentId;

                                    _PricingResponse.PackageId = PackageDetails.PackageId;
                                    _PricingResponse.PackageKey = PackageDetails.PackageKey;

                                    _PricingResponse.FromAddressId = MerchantAddressDetails.ReferenceId;
                                    _PricingResponse.FromAddressKey = MerchantAddressDetails.ReferenceKey;
                                    _PricingResponse.FromAddressReference = MerchantAddressDetails.AddressReference;

                                    _PricingResponse.ToAddressReference = ToAddress.Reference;
                                    _DeliveryPartners = new List<OOperations.Pricing.Partner>();
                                    foreach (var Item in GetRatesforShipment.data)
                                    {
                                        _DeliveryPartners.Add(new OOperations.Pricing.Partner
                                        {
                                            ReferenceKey = Item.rate_id,
                                            Name = Item.carrier_name,
                                            IconUrl = Item.carrier_logo,
                                            DeliveryCharge = Math.Round(Item.amount, 2),
                                            DeliveryTime = Item.delivery_time,
                                            DeliveryEta = Item.delivery_eta,
                                            CarrierId = Item.carrier_reference,
                                        });
                                    }
                                    _PricingResponse.DeliveryPartners = _DeliveryPartners;
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PricingResponse, "LSSAVE", Resources.Resources.LSSAVE);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery is not available at your location. Please change address and try agian");
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery is not available at your location. Please change address and try agian");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Delivery is not available at your location. Please change address and try agian");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPricing", _Exception, _Request.UserReference);
            }
        }
        /// <summary>
        /// Description: Gets the estimated pricing.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetEstimatedPricing(OOperations.Pricing.Request _Request)
        {
            try
            {
                _FrameworkAddress = new FrameworkAddress();
                _FrameworkShipment = new FrameworkShipment();
                _OShipmentsRequest = new OShipments.Save.Request();
                _FrameworkDelivery = new FrameworkDelivery();
                _ParcelItems = new List<ParcelItems>();
                _ParcelMetaData = new ParcelMetaData();
                _RateRequest = new OShipments.CheckRates();

                using (_HCoreContext = new HCoreContext())
                {
                    var Dealdetails = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).FirstOrDefault();
                    ODeliveryAddress.ManageAddress.Response CustomerAddressDetails = _FrameworkAddress.ManageAddress(_Request.AccountId, _Request.AddressId);
                    ODeliveryAddress.ManageAddress.Response MerchantAddressDetails = _HCoreContext.MDDealAddress.Where(x => x.AccountId == Dealdetails.AccountId).Select(x => new ODeliveryAddress.ManageAddress.Response
                    {
                        ReferenceId = x.Id,
                        ReferenceKey = x.Guid,
                        AddressReference = x.AddressReference,
                    }).FirstOrDefault();
                    if (MerchantAddressDetails != null)
                    {
                        var AddressDetails = _FrameworkAddress.ManageAddress(_Request.AccountId, _Request.AddressId);
                        if (AddressDetails != null)
                        {
                            MerchantAddressDetails.AddressReference = AddressDetails.AddressReference;
                        }
                    }
                    var Currency = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.UserReference.CountryId).Select(a => a.CurrencyNotation).FirstOrDefault();
                    var PackageDetails = _HCoreContext.LSPackages.Where(x => x.DealId == _Request.DealId).FirstOrDefault();
                    var ParcelDescription = _HCoreContext.LSPackages.Where(x => x.DealId == _Request.DealId && x.Deal.Guid == _Request.DealKey).FirstOrDefault();
                    _ParcelItems = new List<ParcelItems>();
                    _ParcelItems.Add(new ParcelItems
                    {
                        description = HCoreHelper.GenerateRandomNumber(42),
                        name = Dealdetails.Title,
                        currency = Currency,
                        value = (long?)Dealdetails.SellingPrice,
                        quantity = 1,
                        weight = PackageDetails.Weight
                    });
                    _CreateParcelRequest = new CreateParcelRequest(ParcelDescription.Description, ParcelDescription.PackagingId, ParcelDescription.WeightUnit, _ParcelItems, _ParcelMetaData);
                    _CreateParcel = _FrameworkDelivery.CreateParcel(_CreateParcelRequest);
                    if (_CreateParcel.status == true)
                    {
                        _LSParcel = new LSParcel();
                        _LSParcel.Guid = HCoreHelper.GenerateGuid();

                        var Packaging = _HCoreContext.LSPackages.Where(x => x.PackagingId == _CreateParcel.data.packaging).Select(a => new
                        {
                            PackagingId = a.Id
                        }).FirstOrDefault();
                        if (Packaging.PackagingId > 0)
                        {
                            _LSParcel.PackagingId = Packaging.PackagingId;
                        }

                        _LSParcel.Description = _CreateParcel.data.description;
                        _LSParcel.TotalWeight = _CreateParcel.data.total_weight;
                        _LSParcel.WeightUnit = _CreateParcel.data.weight_unit;
                        _LSParcel.ParcelId = _CreateParcel.data.parcel_id;
                        _LSParcel.CreateDate = _CreateParcel.data.created_at;
                        _LSParcel.CreatedById = _Request.UserReference.AccountId;
                        _LSParcel.TId = _CreateParcel.data.id;
                        _LSParcel.StatusId = HelperStatus.Default.Active;

                        foreach (var Item in _CreateParcel.data.items)
                        {
                            _LSItems = new LSItems();
                            _LSItems.Guid = HCoreHelper.GenerateGuid();
                            _LSItems.Description = Item.description;
                            _LSItems.Name = Item.name;
                            _LSItems.Currency = Item.currency;
                            _LSItems.Price = Item.value;
                            _LSItems.Quantity = Item.quantity;
                            _LSItems.Weight = Item.weight;
                            _LSItems.Parcel = _LSParcel;
                            _HCoreContext.LSItems.Add(_LSItems);
                        }

                        _HCoreContext.LSParcel.Add(_LSParcel);
                        _HCoreContext.SaveChanges();

                        var ParcelId = _HCoreContext.LSParcel.Where(x => x.ParcelId == _CreateParcel.data.parcel_id).Select(a => a.Id).FirstOrDefault();
                        _OShipmentsRequest.CustomerId = _Request.UserReference.AccountId;
                        _OShipmentsRequest.CustomerKey = _Request.UserReference.AccountKey;
                        _OShipmentsRequest.CustomerAddressReference = CustomerAddressDetails.AddressReference;
                        _OShipmentsRequest.MerchantAddressReference = MerchantAddressDetails.AddressReference;
                        _OShipmentsRequest.ParcelReference = _CreateParcel.data.parcel_id;
                        _OShipmentsRequest.MerchantId = Dealdetails.AccountId;
                        _OShipmentsRequest.DealId = (long)_Request.DealId;
                        using (_HCoreContext = new HCoreContext())
                        {
                            _RateRequest.ParcelId = _CreateParcel.data.parcel_id;

                            GetRatesForShipments GetRatesforShipment = _FrameworkShipment.CheckRateForShioments(_RateRequest);
                            if (GetRatesforShipment != null)
                            {
                                _PricingResponse = new OOperations.Pricing.Response();
                                _PricingResponse.FromAddressId = MerchantAddressDetails.ReferenceId;
                                _PricingResponse.FromAddressKey = MerchantAddressDetails.ReferenceKey;

                                _PricingResponse.ToAddressId = CustomerAddressDetails.ReferenceId;
                                _PricingResponse.ToAddressKey = CustomerAddressDetails.ReferenceKey;

                                _DeliveryPartners = new List<OOperations.Pricing.Partner>();
                                foreach (var Item in GetRatesforShipment.data)
                                {
                                    _DeliveryPartners.Add(new OOperations.Pricing.Partner
                                    {
                                        ReferenceKey = Item.id,
                                        Name = Item.carrier_name,
                                        IconUrl = Item.carrier_logo,
                                        DeliveryCharge = Math.Round(Item.amount, 2),
                                        DeliveryTime = Item.delivery_time,
                                        DeliveryEta = Item.delivery_eta,
                                        CarrierId = Item.carrier_reference,
                                    });
                                }
                                _PricingResponse.DeliveryPartners = _DeliveryPartners;
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PricingResponse, "LSSAVE", Resources.Resources.LSSAVE);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                            }
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveParcel", _Exception, _Request.UserReference);
            }
        }

        internal async Task<OResponse> GetDeliveryPricing(OOperations.Pricing.GetDeliveryPricing request)
        {
            try
            {
                _DellymanOrders = new DellymanOrders();
                _FrameworkAddress = new FrameworkAddress();
                _Shiip = new GoShiip();
                if (request.DealId < 1)
                {
                    return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, "AD002", "Deal id required.");
                }
                if (string.IsNullOrEmpty(request.DealKey))
                {
                    return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, "AD003", "Deal key required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Dealdetails = await _HCoreContext.MDDeal.Where(x => x.Id == request.DealId && x.Guid == request.DealKey)
                        .Select(x => new
                        {
                            AccountId = x.AccountId,
                            Title = x.Title,
                            SellingPrice = x.SellingPrice,
                            DealId = x.Id,
                            CurrencyNotation = x.Account.Country.CurrencyNotation,
                            DealCategory = x.Category.Name + "-" + x.Subcategory.Name
                        }).FirstOrDefaultAsync();

                    if(Dealdetails != null)
                    {
                        OOperations.AddressDetails? _AddressDetails = new OOperations.AddressDetails();
                        var MerchantAddressDetails = await _HCoreContext.HCCoreAddress.Where(x => x.AccountId == Dealdetails.AccountId).Select(x => new
                        {
                            MerchantAddress = x.AddressLine1 + ", " + x.AddressLine2 + ", " + x.City.Name + ", " + x.State.Name,
                            MerchantAddressId = x.Id,
                            MerchantDisplayName = x.Account.DisplayName,
                            MerchantEmailAddress = x.Account.EmailAddress,
                            MerchantMobileNumber = x.Account.MobileNumber,
                            MerchantCountryName = x.Country.Name,
                            MerchantCountryCode = x.Country.Iso,
                            MerchantStateName = x.State.Name,
                            MerchantCityName = x.City.Name,
                            MerchantLatitude = x.Latitude,
                            MerchantLongitude = x.Longitude,
                        }).FirstOrDefaultAsync();
                        if (MerchantAddressDetails != null)
                        {
                            _AddressDetails.MerchantAddressId = MerchantAddressDetails.MerchantAddressId;
                            _AddressDetails.MerchantDisplayName = MerchantAddressDetails.MerchantDisplayName;
                            _AddressDetails.MerchantEmailAddress = MerchantAddressDetails.MerchantEmailAddress;
                            _AddressDetails.MerchantMobileNumber = MerchantAddressDetails.MerchantMobileNumber;
                            _AddressDetails.MerchantCountryName = MerchantAddressDetails.MerchantCountryName;
                            _AddressDetails.MerchantCountryCode = MerchantAddressDetails.MerchantCountryCode;
                            _AddressDetails.MerchantStateName = MerchantAddressDetails.MerchantStateName;
                            _AddressDetails.MerchantCityName = MerchantAddressDetails.MerchantCityName;
                            _AddressDetails.MerchantAddress = MerchantAddressDetails.MerchantAddress;
                            _AddressDetails.MerchantLatitude = MerchantAddressDetails.MerchantLatitude;
                            _AddressDetails.MerchantLongitude = MerchantAddressDetails.MerchantLongitude;
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, "DELL006", "Not able to get merchant address details for this deal.");
                        }

                        var CustomerAddressDetails = await _HCoreContext.HCCoreAddress.Where(x => x.Id == request.AddressId).Select(x => new
                        {
                            CustomerAddress = x.AddressLine1 + ", " + x.AddressLine2 + ", " + x.City.Name + ", " + x.State.Name,
                            AccountId = x.AccountId,
                            CustomerAddressId = x.Id,
                            CustomerDisplayName = x.Account.DisplayName,
                            CustomerEmailAddress = x.Account.EmailAddress,
                            CustomerMobileNumber = x.Account.MobileNumber,
                            CustomerCountryName = x.Country.Name,
                            CustomerCountryCode = x.Country.Iso,
                            CustomerStateName = x.State.Name,
                            CustomerCityName = x.City.Name,
                            CustomerLatitude = x.Latitude,
                            CustomerLongitude = x.Longitude,
                        }).FirstOrDefaultAsync();
                        if(CustomerAddressDetails != null)
                        {
                            _AddressDetails.CustomerAddressId = CustomerAddressDetails.CustomerAddressId;
                            _AddressDetails.CustomerDisplayName = CustomerAddressDetails.CustomerDisplayName;
                            _AddressDetails.CustomerEmailAddress = CustomerAddressDetails.CustomerEmailAddress;
                            _AddressDetails.CustomerMobileNumber = CustomerAddressDetails.CustomerMobileNumber;
                            _AddressDetails.CustomerCountryName = CustomerAddressDetails.CustomerCountryName;
                            _AddressDetails.CustomerCountryCode = CustomerAddressDetails.CustomerCountryCode;
                            _AddressDetails.CustomerStateName = CustomerAddressDetails.CustomerStateName;
                            _AddressDetails.CustomerCityName = CustomerAddressDetails.CustomerCityName;
                            _AddressDetails.CustomerAddress = CustomerAddressDetails.CustomerAddress;
                            _AddressDetails.CustomerLatitude = CustomerAddressDetails.CustomerLatitude;
                            _AddressDetails.CustomerLongitude = CustomerAddressDetails.CustomerLongitude;
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, "DELL006", "Delivery is not available at your location. We are working on getting it available at your location. Please try with another address.");
                        }

                        var OrderDetails = await _HCoreContext.LSPackages.Where(x => x.DealId == request.DealId && x.Deal.Guid == request.DealKey).Select(x => new
                        {
                            Weight = x.Weight,
                            Height = x.Height,
                            Width = x.Width,
                            Length = x.Length,
                            Description = x.Description
                        }).FirstOrDefaultAsync();

                        DateTime PickupDate = HCoreHelper.GetGMTDateTime().AddHours(8);

                        #region Dellyman
                        GetQuotesRequest? getquotesrequest = new GetQuotesRequest();

                        List<string>? customeraddress = new List<string>();
                        customeraddress.Add(_AddressDetails.CustomerAddress);
                        getquotesrequest.DeliveryAddress = customeraddress;

                        getquotesrequest.IsInstantDelivery = 0;
                        getquotesrequest.IsProductOrder = 0;

                        List<int?> ProductWeight = new List<int?>();

                        if(request.ItemCount > 0)
                        {
                            int? ItemWeight = (int?)(OrderDetails.Weight * request.ItemCount);
                            ProductWeight.Add(ItemWeight);
                        }
                        else
                        {
                            ProductWeight.Add((int?)OrderDetails.Weight);
                        }

                        getquotesrequest.PackageWeight = ProductWeight;

                        getquotesrequest.PaymentMode = "online";
                        getquotesrequest.PickupAddress = MerchantAddressDetails.MerchantAddress;

                        getquotesrequest.PickupRequestedDate = PickupDate.ToString("dd/MM/yyyy");
                        getquotesrequest.PickupRequestedTime = _AppConfig.PickUpRequestedTime;

                        if (OrderDetails.Weight <= 10)
                        {
                            getquotesrequest.VehicleID = 1;
                        }
                        else if (OrderDetails.Weight > 10 && OrderDetails.Weight <= 299)
                        {
                            getquotesrequest.VehicleID = 2;
                        }
                        else if (OrderDetails.Weight > 299 && OrderDetails.Weight <= 1000)
                        {
                            getquotesrequest.VehicleID = 3;
                        }
                        else
                        {
                            getquotesrequest.VehicleID = 5;
                        }

                        var getquotesresponse = _DellymanOrders.GetQuotes(getquotesrequest);
                        #endregion

                        #region GoShiip
                        ShipLogInRequest? _LogInRequest = new ShipLogInRequest();
                        _LogInRequest.email_phone = _AppConfig.ShiipUserId;
                        _LogInRequest.password = _AppConfig.ShiipUserKey;

                        ShipLogInResponse? _LogInResponse = await _Shiip.ShipLogIn(_LogInRequest);
                        ShipGetRatesResponse? _GetRateResponse = new ShipGetRatesResponse();
                        int? UserId = 0;
                        if (_LogInResponse.status == true)
                        {
                            UserId = _LogInResponse.data.user.id;

                            ShipGetRatesRequest? _GetRateRequest = new ShipGetRatesRequest();

                            _GetRateRequest.type = "local";
                            _GetRateRequest.items = new List<ShipGetRatesRequest.Item>();
                            _GetRateRequest.items.Add(new ShipGetRatesRequest.Item
                            {
                                category = Dealdetails.DealCategory,
                                name = Dealdetails.Title,
                                quantity = request.ItemCount.ToString(),
                                weight_id = 1,
                                weight = OrderDetails.Weight,
                                height = OrderDetails.Height,
                                width = OrderDetails.Width,
                                length = OrderDetails.Length,
                                amount = Dealdetails.SellingPrice.ToString(),
                                description = OrderDetails.Description,
                                pickup_date = PickupDate.ToString("yyyy-MM-dd HH:mm"),
                            });

                            _GetRateRequest.parcels = new ShipGetRatesRequest.Parcels();
                            _GetRateRequest.parcels.weight = OrderDetails.Weight;
                            _GetRateRequest.parcels.length = OrderDetails.Length;
                            _GetRateRequest.parcels.height = OrderDetails.Height;
                            _GetRateRequest.parcels.width = OrderDetails.Width;
                            _GetRateRequest.parcels.date = PickupDate.ToString("yyyy-MM-dd HH:mm");

                            _GetRateRequest.fromAddress = new ShipGetRatesRequest.FromAddress();
                            _GetRateRequest.fromAddress.name = _AddressDetails.MerchantDisplayName;
                            _GetRateRequest.fromAddress.email = _AddressDetails.MerchantEmailAddress;
                            _GetRateRequest.fromAddress.phone = _AddressDetails.MerchantMobileNumber;
                            _GetRateRequest.fromAddress.country = _AddressDetails.MerchantCountryName;
                            _GetRateRequest.fromAddress.country_code = _AddressDetails.MerchantCountryCode;
                            _GetRateRequest.fromAddress.state = _AddressDetails.MerchantStateName;
                            _GetRateRequest.fromAddress.city = _AddressDetails.MerchantCityName;
                            _GetRateRequest.fromAddress.address = _AddressDetails.MerchantAddress;
                            _GetRateRequest.fromAddress.latitude = _AddressDetails.MerchantLatitude.ToString();
                            _GetRateRequest.fromAddress.longitude = _AddressDetails.MerchantLongitude.ToString();

                            _GetRateRequest.toAddress = new ShipGetRatesRequest.ToAddress();
                            _GetRateRequest.toAddress.name = _AddressDetails.CustomerDisplayName;
                            _GetRateRequest.toAddress.email = _AddressDetails.CustomerEmailAddress;
                            _GetRateRequest.toAddress.phone = _AddressDetails.CustomerMobileNumber;
                            _GetRateRequest.toAddress.country = _AddressDetails.CustomerCountryName;
                            _GetRateRequest.toAddress.country_code = _AddressDetails.CustomerCountryCode;
                            _GetRateRequest.toAddress.state = _AddressDetails.CustomerStateName;
                            _GetRateRequest.toAddress.city = _AddressDetails.CustomerCityName;
                            _GetRateRequest.toAddress.address = _AddressDetails.CustomerAddress;
                            _GetRateRequest.toAddress.latitude = _AddressDetails.CustomerLatitude.ToString();
                            _GetRateRequest.toAddress.longitude = _AddressDetails.CustomerLongitude.ToString();

                            _GetRateResponse = await _Shiip.ShipGetRates(_GetRateRequest, _LogInResponse.data.token);
                        }
                        #endregion

                        List<OOperations.Pricing.DeliveryPartnerss>? _DeliveryPartners = new List<OOperations.Pricing.DeliveryPartnerss>();
                        OOperations.Pricing.GetDeliveryPricingResponse? _GetDeliveryPricingResponse = new OOperations.Pricing.GetDeliveryPricingResponse();
                        if (getquotesresponse.Companies.Count > 0 || _GetRateResponse.data != null)
                        {
                            _LSShipments = new LSShipments();
                            _LSShipments.Guid = HCoreHelper.GenerateGuid();
                            _LSShipments.MerchantId = Dealdetails.AccountId;
                            _LSShipments.CustomerId = CustomerAddressDetails.AccountId;
                            _LSShipments.CreatedById = CustomerAddressDetails.AccountId;
                            _LSShipments.ToAddressId = _AddressDetails.CustomerAddressId;
                            _LSShipments.FromAddressId = _AddressDetails.MerchantAddressId;
                            _LSShipments.ReturnAddressId = _AddressDetails.MerchantAddressId;
                            _LSShipments.DealId = Dealdetails.DealId;
                            _LSShipments.DealPrice = Dealdetails.SellingPrice;
                            _LSShipments.UserId = "USER-" + CustomerAddressDetails.AccountId.ToString();
                            _LSShipments.StatusId = HelperStatus.OrderStatus.New;
                            _LSShipments.CreateDate = HCoreHelper.GetGMTDateTime();
                            _LSShipments.PickUpDate = PickupDate;

                            if(getquotesresponse.Companies.Count > 0)
                            {
                                foreach (var partner in getquotesresponse.Companies)
                                {
                                    _DeliveryPartners.Add(new OOperations.Pricing.DeliveryPartnerss
                                    {
                                        CarrierId = partner.CompanyID,
                                        Name = partner.Name,
                                        DeliveryCharge = (double?)Math.Round((decimal)partner.SameDayPrice, 2),
                                        OriginalPrice = partner.OriginalPrice,
                                        SavedPrice = partner.SavedPrice,
                                        PayablePrice = partner.SameDayPrice,
                                        DeductablePrice = partner.DeductablePrice,
                                        IconUrl = "http://cdn.mcauto-images-production.sendgrid.net/d30d7ad4671305fd/51b18065-0c1d-42ec-a426-a797ae1f1d5d/202x196.png",
                                        DeliveryPartnerKey = HCoreConstant.DeliveryPartners.Dellyman,
                                        ExpectedDeliveryTime = "Estimated Days: Within 1 - 2 days",
                                    });
                                    _DeliveryPartners.Add(new OOperations.Pricing.DeliveryPartnerss
                                    {
                                        CarrierId = partner.CompanyID,
                                        Name = partner.Name,
                                        DeliveryCharge = (double?)Math.Round((decimal)partner.NextDayPrice, 2),
                                        OriginalPrice = partner.OriginalPrice,
                                        SavedPrice = partner.SavedPrice,
                                        PayablePrice = partner.NextDayPrice,
                                        DeductablePrice = partner.DeductablePrice,
                                        IconUrl = "http://cdn.mcauto-images-production.sendgrid.net/d30d7ad4671305fd/51b18065-0c1d-42ec-a426-a797ae1f1d5d/202x196.png",
                                        DeliveryPartnerKey = HCoreConstant.DeliveryPartners.Dellyman,
                                        ExpectedDeliveryTime = "Estimated Days: Within 2 - 3 days",
                                    });
                                    _DeliveryPartners.Add(new OOperations.Pricing.DeliveryPartnerss
                                    {
                                        CarrierId = partner.CompanyID,
                                        Name = partner.Name,
                                        DeliveryCharge = (double?)Math.Round((decimal)partner._4872HoursDeliveryPrice, 2),
                                        OriginalPrice = partner.OriginalPrice,
                                        SavedPrice = partner.SavedPrice,
                                        PayablePrice = partner._4872HoursDeliveryPrice,
                                        DeductablePrice = partner.DeductablePrice,
                                        IconUrl = "http://cdn.mcauto-images-production.sendgrid.net/d30d7ad4671305fd/51b18065-0c1d-42ec-a426-a797ae1f1d5d/202x196.png",
                                        DeliveryPartnerKey = HCoreConstant.DeliveryPartners.Dellyman,
                                        ExpectedDeliveryTime = "Estimated Days: Within 3 - 4 day",
                                    });
                                }
                            }

                            if(_GetRateResponse.data != null && _GetRateResponse.data.rates.Count > 0)
                            {
                                var Rates = _GetRateResponse.data.rates.Where(x => x.status == true).ToList();
                                foreach(var Rate in Rates)
                                {
                                    long? CarrierId = await _HCoreContext.LSCarriers.Where(x => x.Name == Rate.courier.name).Select(x => x.Id).FirstOrDefaultAsync();
                                    _DeliveryPartners.Add(new OOperations.Pricing.DeliveryPartnerss
                                    {
                                        CarrierId = (int?)CarrierId,
                                        Name = Rate.courier.name,
                                        DeliveryCharge = Math.Round((double)Rate.amount, 2),
                                        OriginalPrice = Rate.actual_amount,
                                        SavedPrice = Rate.service_charge,
                                        PayablePrice = (double)Rate.amount,
                                        DeductablePrice = (double)Rate.amount,
                                        IconUrl = Rate.courier.icon,
                                        RateId = Rate.id,
                                        DeliveryPartnerKey = HCoreConstant.DeliveryPartners.GoShiip,
                                        KwikKey = _GetRateResponse.data.kwik_key,
                                        RedisKey = _GetRateResponse.data.redis_key,
                                        ExpectedDeliveryTime = Rate.estimated_days,
                                        DeliveryNote = Rate.delivery_note,
                                    });
                                }
                            }

                            await _HCoreContext.LSShipments.AddAsync(_LSShipments);
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            _GetDeliveryPricingResponse.TrackingId = _LSShipments.Id;
                            _GetDeliveryPricingResponse.TrackingKey = _LSShipments.Guid;
                            _GetDeliveryPricingResponse.ShipmentId = _LSShipments.Id;
                            _GetDeliveryPricingResponse.ShipmentKey = _LSShipments.Guid;
                            _GetDeliveryPricingResponse.FromAddressId = _AddressDetails.MerchantAddressId;
                            _GetDeliveryPricingResponse.ToAddressId = _AddressDetails.CustomerAddressId;
                            _GetDeliveryPricingResponse.DeliveryPartners = _DeliveryPartners.OrderBy(x => x.DeliveryCharge).ToList();

                            return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Success, _GetDeliveryPricingResponse, "DELL001", "Delivery Partners loaded successfully.");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, "DELL003", "Delivery is not available at your location. We are working on getting it available at your location. Please try with another address.");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(request.UserReference, ResponseStatus.Error, null, "DELL004", "Invalid deal deatils.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeliveryPricing", _Exception, request.UserReference, "DELL0500", _Exception.Message);
            }
        }

        internal OResponse GetAppDeliveryPricing(OOperations.GetAppDeliveryPricing _Request)
        {
            try
            {
                _DellymanOrders = new DellymanOrders();
                _FrameworkAddress = new FrameworkAddress();
                if(_Request.DealId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AD002", "Deal id required.");
                }
                if(string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "AD003", "Deal key required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Dealdetails = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey)
                        .Select(x => new
                        {
                            AccountId = x.AccountId,
                            Title = x.Title,
                            SellingPrice = x.SellingPrice,
                            DealId = x.Id,
                            DealKey = x.Guid,
                            CurrencyNotation = x.Account.Country.CurrencyNotation,
                        }).FirstOrDefault();

                    if (Dealdetails != null)
                    {
                        var MerchantAddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.AccountId == Dealdetails.AccountId).Select(x => new
                        {
                            AccountId = x.AccountId,
                            Address = x.AddressLine1 + ", " + x.AddressLine2 + ", " + x.City.Name + ", " + x.State.Name,
                            AddressId = x.Id
                        }).FirstOrDefault();

                        long? FromAddressId = 0;
                        long? DeliveryAddressId = 0;
                        string? Address = string.Empty;
                        if (MerchantAddressDetails != null)
                        {
                            FromAddressId = _HCoreContext.MDDealAddress.Where(x => x.AddressId == MerchantAddressDetails.AddressId).Select(x => x.Id).FirstOrDefault();
                            DeliveryAddressId = MerchantAddressDetails.AddressId;
                        }
                        else
                        {
                            //This code is added due to the we don't have any flow for creating the address for delivery of the merchant.
                            var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == Dealdetails.AccountId)
                                .Select(x => new
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    DisplayName = x.DisplayName,
                                    MobileNumber = x.MobileNumber,
                                    EmailAddress = x.EmailAddress,
                                    Address = x.Address,
                                    CityId = x.CityId,
                                    CityName = x.City.Name,
                                    CityAreaId = x.CityAreaId,
                                    CityAreaName = x.CityArea.Name,
                                    StateId = x.StateId,
                                    StateName = x.State.Name,
                                    CountryId = x.CountryId,
                                    CountryName = x.Country.Name,
                                    Latitude = x.Latitude,
                                    Longitude = x.Longitude,
                                }).FirstOrDefault();
                            if (MerchantDetails != null)
                            {
                                _HCCoreAddress = new HCCoreAddress();
                                _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                _HCCoreAddress.LocationTypeId = 792;
                                _HCCoreAddress.DisplayName = MerchantDetails.DisplayName;
                                _HCCoreAddress.IsPrimaryAddress = 1;
                                _HCCoreAddress.AccountId = MerchantDetails.Id;
                                _HCCoreAddress.Name = MerchantDetails.Name;
                                _HCCoreAddress.ContactNumber = MerchantDetails.MobileNumber;
                                _HCCoreAddress.AdditionalContactNumber = MerchantDetails.MobileNumber;
                                _HCCoreAddress.EmailAddress = MerchantDetails.EmailAddress;
                                _HCCoreAddress.AddressLine1 = MerchantDetails.Address;
                                _HCCoreAddress.CityId = MerchantDetails.CityId;

                                if (MerchantDetails.CityAreaId != null || MerchantDetails.CityAreaId > 0)
                                {
                                    _HCCoreAddress.CityAreaId = MerchantDetails.CityAreaId;
                                }

                                _HCCoreAddress.StateId = MerchantDetails.StateId;
                                _HCCoreAddress.CountryId = (int)MerchantDetails.CountryId;
                                _HCCoreAddress.Latitude = MerchantDetails.Latitude;
                                _HCCoreAddress.Longitude = MerchantDetails.Longitude;
                                _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCCoreAddress.CreatedById = MerchantDetails.Id;
                                _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);

                                _MDDealAddress = new MDDealAddress();
                                _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                                _MDDealAddress.AccountId = MerchantDetails.Id;
                                _MDDealAddress.TId = HCoreHelper.GenerateRandomNumber(11);
                                _MDDealAddress.AddressReference = HCoreHelper.GenerateRandomNumber(11);
                                _MDDealAddress.Address = _HCCoreAddress;
                                _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                                _HCoreContext.SaveChanges();

                                FromAddressId = _MDDealAddress.Id;
                                DeliveryAddressId = _HCCoreAddress.Id;
                                Address = _HCCoreAddress.AddressLine1 + ", " + MerchantDetails.CityName + ", " + MerchantDetails.StateName;
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DELL005", "Delivery is not available at your location. We are working on getting it available at your location. Please try with another address.");
                            }
                        }

                        ODeliveryAddress.AddressRequest? _AddressRequest = new ODeliveryAddress.AddressRequest();
                        _AddressRequest.Name = _Request.ToAddressComponent.Name;
                        _AddressRequest.ContactNumber = _Request.ToAddressComponent.ContactNumber;
                        _AddressRequest.EmailAddress = _Request.ToAddressComponent.EmailAddress;
                        _AddressRequest.AddressLine1 = _Request.ToAddressComponent.AddressLine1;
                        if (!string.IsNullOrEmpty(_Request.ToAddressComponent.AddressLine2))
                        {
                            _AddressRequest.AddressLine2 = _Request.ToAddressComponent.AddressLine2;
                        }
                        _AddressRequest.CountryId = _Request.ToAddressComponent.CountryId;
                        _AddressRequest.CountryKey = _Request.ToAddressComponent.CountryKey;
                        _AddressRequest.CountryName = _Request.ToAddressComponent.CountryName;
                        _AddressRequest.StateId = _Request.ToAddressComponent.StateId;
                        _AddressRequest.StateKey = _Request.ToAddressComponent.StateKey;
                        _AddressRequest.StateName = _Request.ToAddressComponent.StateName;
                        _AddressRequest.CityId = _Request.ToAddressComponent.CityId;
                        _AddressRequest.CityKey = _Request.ToAddressComponent.CityKey;
                        _AddressRequest.CityName = _Request.ToAddressComponent.CityName;
                        _AddressRequest.CityAreaId = _Request.ToAddressComponent.CityAreaId;
                        _AddressRequest.CityAreaKey = _Request.ToAddressComponent.CityAreaKey;
                        _AddressRequest.CityAreaName = _Request.ToAddressComponent.CityAreaName;
                        _AddressRequest.Latitude = _Request.ToAddressComponent.Latitude;
                        _AddressRequest.Longitude = _Request.ToAddressComponent.Longitude;
                        _AddressRequest.IsPrimary = _Request.ToAddressComponent.IsPrimary;
                        _AddressRequest.LocationTypeId = _Request.ToAddressComponent.LocationTypeId;
                        _AddressRequest.UserReference = _Request.UserReference;
                        ODeliveryAddress.AddressResponse? CustomerAddress = _FrameworkAddress.CreateAddress(_AddressRequest);
                        if (CustomerAddress.Status == "Error")
                        {
                            var Response = new
                            {
                                Status = CustomerAddress.Status,
                                Message = CustomerAddress.Message,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, Response, "AD001", Response.Message);
                        }

                        var OrderDetails = _HCoreContext.LSPackages.Where(x => x.DealId == Dealdetails.DealId && x.Deal.Guid == Dealdetails.DealKey).Select(x => new
                        {
                            Weight = x.Weight
                        }).FirstOrDefault();

                        GetQuotesRequest? getquotesrequest = new GetQuotesRequest();

                        List<string>? customeraddress = new List<string>();
                        customeraddress.Add(CustomerAddress.Address);
                        getquotesrequest.DeliveryAddress = customeraddress;

                        getquotesrequest.IsInstantDelivery = 0;
                        getquotesrequest.IsProductOrder = 0;

                        List<int?> ProductWeight = new List<int?>();
                        ProductWeight.Add((int?)OrderDetails.Weight);
                        getquotesrequest.PackageWeight = ProductWeight;

                        getquotesrequest.PaymentMode = "online";
                        if (MerchantAddressDetails != null)
                        {
                            getquotesrequest.PickupAddress = MerchantAddressDetails.Address;
                        }
                        else
                        {
                            getquotesrequest.PickupAddress = Address;
                        }

                        DateTime PickupDate = HCoreHelper.GetGMTDateTime().AddHours(8);

                        getquotesrequest.PickupRequestedDate = PickupDate.ToString("dd/MM/yyyy");
                        getquotesrequest.PickupRequestedTime = _AppConfig.PickUpRequestedTime;

                        if (OrderDetails.Weight <= 10)
                        {
                            getquotesrequest.VehicleID = 1;
                        }
                        else if (OrderDetails.Weight > 10 && OrderDetails.Weight <= 299)
                        {
                            getquotesrequest.VehicleID = 2;
                        }
                        else if (OrderDetails.Weight > 299 && OrderDetails.Weight <= 1000)
                        {
                            getquotesrequest.VehicleID = 3;
                        }
                        else
                        {
                            getquotesrequest.VehicleID = 5;
                        }

                        var getquotesresponse = _DellymanOrders.GetQuotes(getquotesrequest);
                        if (getquotesresponse != null)
                        {
                            if (getquotesresponse.Companies.Count > 0)
                            {
                                _LSShipments = new LSShipments();
                                _LSShipments.Guid = HCoreHelper.GenerateGuid();
                                if (Dealdetails.AccountId > 0)
                                {
                                    _LSShipments.MerchantId = Dealdetails.AccountId;
                                }
                                if (CustomerAddress.AccountId > 0)
                                {
                                    _LSShipments.CustomerId = CustomerAddress.AccountId;
                                    _LSShipments.CreatedById = CustomerAddress.AccountId;
                                }
                                if (CustomerAddress.AddressId > 0)
                                {
                                    _LSShipments.ToAddressId = CustomerAddress.AddressId;
                                }

                                _LSShipments.FromAddressId = DeliveryAddressId;
                                _LSShipments.ReturnAddressId = DeliveryAddressId;

                                if (Dealdetails.DealId > 0)
                                {
                                    _LSShipments.DealId = Dealdetails.DealId;
                                }
                                _LSShipments.DealPrice = Dealdetails.SellingPrice;
                                _LSShipments.OrderReference = "ORDRREF" + HCoreHelper.GenerateRandomNumber(4);
                                _LSShipments.ShipmentId = "ORDRID" + HCoreHelper.GenerateRandomNumber(5);
                                _LSShipments.UserId = "USER-" + CustomerAddress.AccountId.ToString();
                                _LSShipments.TId = HCoreHelper.GenerateRandomNumber(11);
                                _LSShipments.StatusId = HelperStatus.OrderStatus.New;
                                _LSShipments.CreateDate = HCoreHelper.GetGMTDateTime();
                                _LSShipments.PickUpDate = PickupDate;
                                _HCoreContext.LSShipments.Add(_LSShipments);

                                _HCoreContext.SaveChanges();
                                List<OOperations.Pricing.DeliveryPartnerss> deliverypartners = new List<OOperations.Pricing.DeliveryPartnerss>();
                                OOperations.Pricing.GetDeliveryPricingResponse _GetDeliveryPricingResponse = new OOperations.Pricing.GetDeliveryPricingResponse();
                                foreach (var partner in getquotesresponse.Companies)
                                {
                                    deliverypartners.Add(new OOperations.Pricing.DeliveryPartnerss
                                    {
                                        CarrierId = partner.CompanyID,
                                        Name = partner.Name,
                                        DeliveryCharge = (double?)Math.Round((decimal)partner.NextDayPrice, 2),
                                        OriginalPrice = partner.OriginalPrice,
                                        SavedPrice = partner.SavedPrice,
                                        PayablePrice = partner.NextDayPrice,
                                        DeductablePrice = partner.DeductablePrice,
                                        IconUrl = "http://cdn.mcauto-images-production.sendgrid.net/d30d7ad4671305fd/51b18065-0c1d-42ec-a426-a797ae1f1d5d/202x196.png",
                                        DeliveryPartnerKey = HCoreConstant.DeliveryPartners.Dellyman,
                                        ExpectedDeliveryTime = "Estimated Days: Within 2 - 3 days",
                                    });
                                }
                                _GetDeliveryPricingResponse.TrackingId = _LSShipments.Id;
                                _GetDeliveryPricingResponse.TrackingKey = _LSShipments.Guid;
                                _GetDeliveryPricingResponse.ShipmentId = _LSShipments.Id;
                                _GetDeliveryPricingResponse.ShipmentKey = _LSShipments.Guid;
                                _GetDeliveryPricingResponse.AccountId = CustomerAddress.AccountId;
                                _GetDeliveryPricingResponse.AccountKey = CustomerAddress.AccountKey;
                                _GetDeliveryPricingResponse.FromAddressId = FromAddressId;
                                _GetDeliveryPricingResponse.ToAddressId = CustomerAddress.CustomerAddressId;
                                _GetDeliveryPricingResponse.DeliveryPartners = deliverypartners;

                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GetDeliveryPricingResponse, "DELL001", "Delivery Partners loaded successfully.");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DELL002", "Delivery is not available at your location. We are working on getting it available at your location. Please try with another address.");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DELL003", "Delivery is not available at your location. We are working on getting it available at your location. Please try with another address.");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DELL004", "Invalid deal deatils.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetAppDeliveryPricing", _Exception, _Request.UserReference, "DELL0500", _Exception.Message);
            }
        }

        internal async Task<OOperations.Pricing.PricingResponse> ConfirmOrder(OOperations.Pricing.GetDeliveryPricing request)
        {
            _PricingResponses = new OOperations.Pricing.PricingResponse();
            try
            {
                _DellymanOrders = new DellymanOrders();
                _FrameworkAddress = new FrameworkAddress();
                _Shiip = new GoShiip();
                using (_HCoreContext = new HCoreContext())
                {
                    var Dealdetails = await _HCoreContext.MDDeal.Where(x => x.Id == request.DealId)
                        .Select(x => new
                        {
                            AccountId = x.AccountId,
                            Title = x.Title,
                            SellingPrice = x.SellingPrice,
                            DealId = x.Id,
                            CurrencyNotation = x.Account.Country.CurrencyNotation,
                        }).FirstOrDefaultAsync();

                    var MerchantAddressDetails = await _HCoreContext.HCCoreAddress.Where(x => x.AccountId == Dealdetails.AccountId && x.Id == request.MerchantAddressId).Select(x => new
                    {
                        Address = x.AddressLine1+ ", " + x.AddressLine2 + ", " + x.City.Name + ", " + x.State.Name,
                        MerchantName = x.Account.FirstName + " " + x.Account.LastName,
                        MerchantMobileNumber = x.Account.MobileNumber
                    }).FirstOrDefaultAsync();
                    if (MerchantAddressDetails == null)
                    {
                        _PricingResponses.Status = "Error";
                        _PricingResponses.Message = "Not able to get merchant address details for this deal.";
                        return _PricingResponses;
                    }

                    string? MobileNumber = MerchantAddressDetails.MerchantMobileNumber;

                    var CustomerAddressDetails = await _HCoreContext.HCCoreAddress.Where(x => x.AccountId == request.AccountId && x.Id == request.AddressId).Select(x => new
                    {
                        Address = x.AddressLine1 + " " + x.AddressLine2 + ", " + x.City.Name + ", " + x.State.Name,
                        Name = x.Account.FirstName + " " + x.Account.LastName,
                        MobileNumber = x.Account.MobileNumber
                    }).FirstOrDefaultAsync();
                    if (CustomerAddressDetails == null)
                    {
                        _PricingResponses.Status = "Error";
                        _PricingResponses.Message = "Delivery is not available at the customer's location.";
                        return _PricingResponses;
                    }

                    string? ContactNumber = CustomerAddressDetails.MobileNumber;

                    var PckageDetails = await _HCoreContext.LSPackages.Where(x => x.DealId == request.DealId).Select(x => new
                    {
                        Weight = x.Weight,
                        Description = x.Description
                    }).FirstOrDefaultAsync();

                    if (!MobileNumber.StartsWith("0"))
                    {
                        MobileNumber = FormatMobileNumber(MobileNumber);
                    }
                    if (MobileNumber.Length < 11 || MobileNumber.Length > 11)
                    {
                        _PricingResponses.Status = "Error";
                        _PricingResponses.Message = "The selected deal merchant has used an invalid mobile number. Please contact the merchant for updating his profile.";
                        return _PricingResponses;
                    }

                    if (!ContactNumber.StartsWith("0"))
                    {
                        ContactNumber = FormatMobileNumber(ContactNumber);
                    }
                    if (ContactNumber.Length < 11 || ContactNumber.Length > 11)
                    {
                        _PricingResponses.Status = "Error";
                        _PricingResponses.Message = "The customer has used an invalid mobile number. Please inform to the customer to update his profile and use a valid mobile number.";
                        return _PricingResponses;
                    }

                    #region Dellyman
                    BookOrderRequest? bookorderrequest = new BookOrderRequest();
                    bookorderrequest.OrderRef = "ORDER_DELLYMAN" + HCoreHelper.GenerateRandomNumber(5);
                    bookorderrequest.CompanyID = request.CarrierId;
                    bookorderrequest.PaymentMode = "online";

                    if (PckageDetails.Weight <= 10)
                    {
                        bookorderrequest.Vehicle = 1;
                    }
                    else if (PckageDetails.Weight > 10 && PckageDetails.Weight <= 299)
                    {
                        bookorderrequest.Vehicle = 2;
                    }
                    else if (PckageDetails.Weight > 299 && PckageDetails.Weight <= 1000)
                    {
                        bookorderrequest.Vehicle = 3;
                    }
                    else
                    {
                        bookorderrequest.Vehicle = 5;
                    }

                    bookorderrequest.PickUpContactName = MerchantAddressDetails.MerchantName;
                    bookorderrequest.PickUpContactNumber = MobileNumber;
                    bookorderrequest.PickUpGooglePlaceAddress = MerchantAddressDetails.Address;
                    bookorderrequest.PickUpLandmark = MerchantAddressDetails.Address;
                    bookorderrequest.IsInstantDelivery = 0;

                    string? PickupStartTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), request.TimeZoneId).ToString("hh:mm tt");
                    string? PickupEndTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime().AddHours(1), request.TimeZoneId).ToString("hh:mm tt");
                    string? DeliveryStartTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime().AddHours(2), request.TimeZoneId).ToString("hh:mm tt");
                    string? DeliveryEndTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime().AddHours(4), request.TimeZoneId).ToString("hh:mm tt");

                    int? PStartTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), request.TimeZoneId).TimeOfDay.Hours;
                    int? PEndTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime().AddHours(1), request.TimeZoneId).TimeOfDay.Hours;
                    int? DStartTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime().AddHours(2), request.TimeZoneId).TimeOfDay.Hours;
                    int? DEndTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime().AddHours(4), request.TimeZoneId).TimeOfDay.Hours;

                    if (PStartTime > 7 && PEndTime < 19 && DStartTime > 7 && DEndTime < 19)
                    {
                        bookorderrequest.PickUpRequestedDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), request.TimeZoneId).ToString("yyyy/MM/dd");
                        bookorderrequest.PickUpRequestedTime = PickupStartTime + " to " + PickupEndTime;
                        bookorderrequest.DeliveryRequestedTime = DeliveryStartTime + " to " + DeliveryEndTime;
                    }
                    else
                    {
                        bookorderrequest.PickUpRequestedDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime().Date.AddDays(1), request.TimeZoneId).ToString("yyyy/MM/dd");
                        bookorderrequest.PickUpRequestedTime = _AppConfig.PickUpRequestedTime;
                        bookorderrequest.DeliveryRequestedTime = _AppConfig.DeliveryRequestedTime;
                    }

                    List<BookOrderPackage> Packages = new List<BookOrderPackage>();
                    Packages.Add(new BookOrderPackage
                    {
                        PackageDescription = PckageDetails.Description,
                        DeliveryContactName = CustomerAddressDetails.Name,
                        DeliveryContactNumber = ContactNumber,
                        DeliveryGooglePlaceAddress = CustomerAddressDetails.Address,
                        DeliveryLandmark = CustomerAddressDetails.Address
                    });
                    bookorderrequest.Packages = Packages;
                    BookOrderResponse _BookOrderResponse = new BookOrderResponse();

                    if(request.DeliveryPartnerKey == HCoreConstant.DeliveryPartners.Dellyman)
                    {
                        _BookOrderResponse = _DellymanOrders.BookOrder(bookorderrequest);
                    }
                    #endregion

                    #region GoShiip
                    ShipLogInResponse? _LogInResponse = new ShipLogInResponse();
                    ShipBookShipmentResponse? _BookShipmentResponse = new ShipBookShipmentResponse();
                    if (request.DeliveryPartnerKey == HCoreConstant.DeliveryPartners.GoShiip)
                    {
                        ShipLogInRequest? _LogInRequest = new ShipLogInRequest();
                        _LogInRequest.email_phone = _AppConfig.ShiipUserId;
                        _LogInRequest.password = _AppConfig.ShiipUserKey;

                        _LogInResponse = await _Shiip.ShipLogIn(_LogInRequest);
                    }

                    if(_LogInResponse.status == true)
                    {
                        ShipBookShipmentRequest? _BookShipmentRequest = new ShipBookShipmentRequest();
                        _BookShipmentRequest.rate_id = request.RateId;
                        _BookShipmentRequest.redis_key = request.RedisKey;
                        _BookShipmentRequest.user_id = _AppConfig.ShiipUserAccountId;
                        _BookShipmentRequest.platform = "web2";

                        _BookShipmentResponse = await _Shiip.ShipBookShipment(_BookShipmentRequest, _LogInResponse.data.token);
                    }
                    #endregion

                    if (_BookOrderResponse != null || _BookShipmentResponse != null)
                    {
                        if(_BookOrderResponse.ResponseCode != 00 || _BookShipmentResponse.status == true)
                        {
                            if(_BookOrderResponse.ResponseCode != 00 && _BookOrderResponse.ResponseCode != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var OrderDetails = await _HCoreContext.LSShipments.Where(x => x.Id == request.ReferenceId && x.Guid == request.ReferenceKey).FirstOrDefaultAsync();

                                    OrderDetails.ShipmentId = "ORDRID" + _BookOrderResponse.OrderID;
                                    OrderDetails.StatusId = HelperStatus.OrderStatus.Confirmed;
                                    OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    OrderDetails.ModifyById = request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();
                                }

                                _PricingResponses.Status = "Success";
                                _PricingResponses.Message = "Your order has been confirmed successfully. Use OrderId & OrderReference to track your order.";
                                _PricingResponses.OrderId = _BookOrderResponse.OrderID;
                                _PricingResponses.OrderReference = _BookOrderResponse.Reference;
                                _PricingResponses.DeliveryCharge = request.DeliveryCharge;
                                _PricingResponses.TrackingId = _BookOrderResponse.TrackingID;
                                return _PricingResponses;
                            }

                            if (_BookShipmentResponse.data != null)
                            {
                                if (_BookShipmentResponse.data.status == true)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var OrderDetails = await _HCoreContext.LSShipments.Where(x => x.Id == request.ReferenceId && x.Guid == request.ReferenceKey).FirstOrDefaultAsync();

                                        OrderDetails.StatusId = HelperStatus.OrderStatus.Confirmed;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = request.UserReference.AccountId;
                                        _HCoreContext.SaveChanges();
                                    }

                                    _PricingResponses.Status = "Success";
                                    _PricingResponses.Message = "Order has been confirmed successfully. Use OrderId & OrderReference to track the order.";
                                    _PricingResponses.OrderId = _BookShipmentResponse.data.shipmentId;
                                    _PricingResponses.OrderReference = _BookShipmentResponse.data.shipmentId.ToString();
                                    _PricingResponses.DeliveryCharge = request.DeliveryCharge;
                                    return _PricingResponses;
                                }
                                else
                                {
                                    _PricingResponses.Status = "Error";
                                    _PricingResponses.Message = "Not able to confirm order. Please try after some time.";
                                    return _PricingResponses;
                                }
                            }
                            else
                            {
                                _PricingResponses.Status = "Error";
                                _PricingResponses.Message = "Not able to confirm order. Please try after some time.";
                                return _PricingResponses;
                            }
                        }
                        else
                        {
                            _PricingResponses.Status = "Error";
                            _PricingResponses.Message = "We are not able to complete your request. Please try again after some time.";
                            return _PricingResponses;
                        }
                    }
                    else
                    {
                        _PricingResponses.Status = "Error";
                        _PricingResponses.Message = "We are not able to complete your request. Please try again after some time.";
                        return _PricingResponses;
                    }
                }
            }
            catch (Exception _Exception)
            {
                _PricingResponses.Status = "Error";
                _PricingResponses.Message = _Exception.Message;
                HCoreHelper.LogException("ConfirmOrder", _Exception);
                return _PricingResponses;
            }
        }

        private static string FormatMobileNumber(string MobileNumber)
        {
            if (!string.IsNullOrEmpty(MobileNumber))
            {
                if(MobileNumber.StartsWith("2340"))
                {
                    var _StringBuilder = new StringBuilder(MobileNumber);
                    _StringBuilder.Remove(0, 3);
                    MobileNumber = _StringBuilder.ToString();
                    return MobileNumber;
                }
                else
                {
                    var _StringBuilder = new StringBuilder(MobileNumber);
                    _StringBuilder.Remove(0, 3);
                    _StringBuilder.Insert(0, "0");
                    MobileNumber = _StringBuilder.ToString();
                    return MobileNumber;
                }
            }
            else
            {
                return MobileNumber;
            }
        }
    }
}
