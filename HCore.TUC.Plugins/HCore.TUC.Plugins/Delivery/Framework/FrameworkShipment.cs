//==================================================================================
// FileName: FrameworkShipment.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to shippment
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using Delivery.Framework;
using Delivery.Object.Requests;
using Delivery.Object.Requests.Rates;
using Delivery.Object.Requests.Shipments;
using Delivery.Object.Response.Rates;
using Delivery.Object.Response.Shipments;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using HCore.Integration.DellymanIntegrations;
using HCore.Integration.DellymanIntegrations.DellymanObject;
using Dellyman.DellymanObject.Request;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkShipment
    {
        HCoreContext? _HCoreContext;
        Create_UpdateAddress? _Create_UpdateAddress;
        CreateShipmentRequest? _CreateShipmentRequest;
        FrameworkDelivery? _FrameworkDelivery;
        ArrangePick_up_DeliveryForShipmentRequest? _UpdateShipment;
        CreateShipment? _CreateShipment;
        LSShipments? _LSShipments;
        GetRatesForShipments? _GetRatesForShipments;
        GetRatesForShipmentsRequest? _GetRatesForShipmentsRequest;
        GetRatesRequest? _GetRatesRequest;
        GetRate? _GetRate;
        GetRateRequest? _GetRateRequest;
        CancelShipment? _CancelShipment;
        CancelShipmentRequest? _CancelShipmentRequest;
        TrackShipment? _TrackShipment;
        Get_TrackShipment? _Get_TrackShipment;
        ArrangePick_up_DeliveryForShipment? _ArrangePick_up_DeliveryForShipment;
        ArrangePick_up_DeliveryForShipmentRequest? _ArrangePick_up_DeliveryForShipmentRequest;
        HCoreContextLogging? _HCoreContextLogging;
        OShipments.OrdersOverview? _OrdersOverview;
        OShipments.Overview? _Overview;
        OShipments.Save.Response? _ShipmentResponse;
        List<OShipments.ListDownload>? _OrdersDownload;
        OShipments.Save.Request? _OShipmentsRequest;
        OShipments.UpdateResponse? _UpdateResponse;
        OShipments.Update? _UpdateOrder;
        Dellyman.DellymanOrders? _Dellyman;
        Dellyman.DellymanObject.Request.TrackOrderRequest? _TrackOrderRequest;
        

        /// <summary>
        /// Description: Gets the rate details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRateDetails(OShipments.CheckRateDetails _Request)
        {
            try
            {
                _GetRate = new GetRate();
                _FrameworkDelivery = new FrameworkDelivery();
                if (string.IsNullOrEmpty(_Request.RateId))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _GetRateRequest = new GetRateRequest(_Request.RateId);
                    _GetRate = _FrameworkDelivery.GetRate(_GetRateRequest);

                    if (_GetRate.status == true)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _GetRate.data, "LS0200", Resources.Resources.LS0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRateDetails", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Cancels the shipment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CancelShipment(OReference _Request)
        {
            try
            {
                _CancelShipment = new CancelShipment();
                _FrameworkDelivery = new FrameworkDelivery();
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1 || StatusId != HelperStatus.OrderStatus.Confirmed)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSINSTATUS", "Invalid order status");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ShipmentDetails = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();

                    if (ShipmentDetails != null)
                    {
                        _CancelShipmentRequest = new CancelShipmentRequest(ShipmentDetails.ShipmentId);
                        _CancelShipment = _FrameworkDelivery.CancelShipment(_CancelShipmentRequest);
                        if (_CancelShipment != null)
                        {
                            if (_CancelShipment.status == true)
                            {
                                if (_CancelShipment.data.status == "cancelled")
                                {
                                    ShipmentDetails.StatusId = StatusId;
                                    ShipmentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    ShipmentDetails.ModifyById = _Request.UserReference.AccountId;
                                    _HCoreContext.SaveChanges();

                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CancelShipment.data, "LS0200", Resources.Resources.LS0200);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Unable to proceed with your request to cancel the order. Your order is in a new state yet. To cancel the order your order must be arranged by the seller.");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CancelShipment", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Tracks the shipment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal async Task<OResponse> TrackOrder(OReference _Request)
        {
            try
            {
                #region Terminal africa track order flow
                //_TrackShipment = new TrackShipment();
                //_FrameworkDelivery = new FrameworkDelivery();
                #endregion

                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0021);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0022);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ShipmentDetails = await _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();

                    if (ShipmentDetails != null)
                    {
                        #region Terminal africa track order flow
                        //_Get_TrackShipment = new Get_TrackShipment(ShipmentDetails.ShipmentId);
                        //_TrackShipment = _FrameworkDelivery.TrackShipment(_Get_TrackShipment);
                        //if (_TrackShipment.status == true)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TrackShipment.data, "LS0200", Resources.Resources.LS0200);
                        //}
                        //else
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                        //}
                        #endregion

                        _Dellyman = new Dellyman.DellymanOrders();
                        _TrackOrderRequest = new TrackOrderRequest();
                        _TrackOrderRequest.OrderCode = ShipmentDetails.TrackingNumber;
                        var Response = _Dellyman.TrackOrder(_TrackOrderRequest);
                        if(Response != null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "LS0200", Resources.Resources.LS0200);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0023", Resources.Resources.LS0023);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("TrackShipment", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Updates the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OShipments.UpdateResponse.</returns>
        internal OShipments.UpdateResponse UpdateOrder(OShipments.Update _Request)
        {
            try
            {
                _ArrangePick_up_DeliveryForShipment = new ArrangePick_up_DeliveryForShipment();
                _FrameworkDelivery = new FrameworkDelivery();
                _UpdateResponse = new OShipments.UpdateResponse();
                using (_HCoreContext = new HCoreContext())
                {
                    var ShipmentDetails = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();

                    _ArrangePick_up_DeliveryForShipmentRequest = new ArrangePick_up_DeliveryForShipmentRequest(ShipmentDetails.RateId);
                    _ArrangePick_up_DeliveryForShipment = _FrameworkDelivery.ArrangePick_up_DeliveryForShipment(_ArrangePick_up_DeliveryForShipmentRequest);

                    if (_ArrangePick_up_DeliveryForShipment != null)
                    {
                        if (_ArrangePick_up_DeliveryForShipment.status == true)
                        {
                            long? CarrierId = _HCoreContext.LSCarriers.Where(x => x.CarrierId == _ArrangePick_up_DeliveryForShipment.data.carrier).Select(a => a.Id).FirstOrDefault();
                            if (CarrierId > 0)
                            {
                                ShipmentDetails.CarrierId = CarrierId;
                            }
                            ShipmentDetails.DeliveryDate = _ArrangePick_up_DeliveryForShipment.data.delivery_date;
                            ShipmentDetails.PickUpDate = _ArrangePick_up_DeliveryForShipment.data.pickup_date;
                            ShipmentDetails.TrackingNumber = _ArrangePick_up_DeliveryForShipment.data.extras.tracking_number;
                            ShipmentDetails.OrderReference = _ArrangePick_up_DeliveryForShipment.data.extras.reference;
                            ShipmentDetails.CancelPickUpUrl = _ArrangePick_up_DeliveryForShipment.data.metadata.shipment_payload.cancelPickupUrl;
                            ShipmentDetails.TrackingUrl = _ArrangePick_up_DeliveryForShipment.data.extras.carrier_tracking_url;
                            ShipmentDetails.StatusId = HelperStatus.OrderStatus.Preparing;
                            ShipmentDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            ShipmentDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();

                            OStorageContent ImageContent = new OStorageContent();
                            ImageContent.Name = "Shipment Lable";
                            ImageContent.Extension = _ArrangePick_up_DeliveryForShipment.data.extras.shipping_label.imageFormat;
                            ImageContent.Content = _ArrangePick_up_DeliveryForShipment.data.extras.shipping_label.content;

                            long? ShipmentLableId = null;
                            if (ImageContent != null && !string.IsNullOrEmpty(ImageContent.Content))
                            {
                                ShipmentLableId = HCoreHelper.SaveStorage(ImageContent.Name, ImageContent.Extension, ImageContent.Content, null, _Request.UserReference);
                            }
                            if (ShipmentLableId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var _ShipmentDetails = _HCoreContext.LSShipments.Where(x => x.Id == ShipmentDetails.Id && x.Guid == ShipmentDetails.Guid).FirstOrDefault();
                                    if (_ShipmentDetails != null)
                                    {
                                        if (ShipmentLableId != null)
                                        {
                                            _ShipmentDetails.BillId = ShipmentLableId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                    else
                                    {
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            _HCoreContext = new HCoreContext();
                            var Details = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                .Select(x => new
                                {
                                    ShipmentLable = x.Bill.Path,
                                    TrackingNumber = x.TrackingNumber,
                                    PickUpDate = x.PickUpDate,
                                    DeliveryDate = x.DeliveryDate,
                                }).FirstOrDefault();
                            _UpdateResponse.ShipmentLable = _AppConfig.StorageUrl + Details.ShipmentLable;
                            _UpdateResponse.TrackingNumber = Details.TrackingNumber;
                            _UpdateResponse.PickUpDate = Details.PickUpDate;
                            _UpdateResponse.DeliveryDate = Details.DeliveryDate;
                            _HCoreContext.Dispose();
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _UpdateResponse, "LSUPDATE", Resources.Resources.LSUPDATE);
                            return _UpdateResponse;
                        }
                        else
                        {
                            HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                            return null;
                        }
                    }
                    else
                    {
                        HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Unable to process for arrange shipment. Please check rate details and shipment id for your order.");
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ArrangePickupAndDelivery", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
                return null;
            }
        }

        /// <summary>
        /// Description: Gets the shipment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetShipment(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var ShipmentDetails = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                          .Select(x => new OShipments.Details
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,
                                              ToAddressId = x.ToAddressId,
                                              ToAddressKey = x.ToAddress.Guid,
                                              ToName = x.ToAddress.Name,
                                              ToEmailAddress = x.ToAddress.EmailAddress,
                                              ToMobileNumber = x.ToAddress.ContactNumber,
                                              ToAddress = x.ToAddress.AddressLine1 + " " + x.ToAddress.AddressLine2,
                                              ToAddressCity = x.ToAddress.City.Name,
                                              ToAddressState = x.ToAddress.State.Name,
                                              ToAddressCountry = x.ToAddress.Country.Name,
                                              FromAddressId = x.FromAddressId,
                                              FromAddressKey = x.FromAddress.Guid,
                                              FromName = x.FromAddress.Name,
                                              FromEmailAddress = x.FromAddress.EmailAddress,
                                              FromMobileNumber = x.FromAddress.ContactNumber,
                                              FromAddress = x.FromAddress.AddressLine1 + " " + x.FromAddress.AddressLine2,
                                              FromAddressCity = x.FromAddress.City.Name,
                                              FromAddressState = x.FromAddress.State.Name,
                                              FromAddressCountry = x.FromAddress.Country.Name,
                                              ReturnAddressId = x.ReturnAddressId,
                                              ReturnAddressKey = x.ReturnAddress.Guid,
                                              ReturnName = x.ReturnAddress.Name,
                                              ReturnEmailAddress = x.ReturnAddress.EmailAddress,
                                              ReturnMobileNumber = x.ReturnAddress.ContactNumber,
                                              ReturnAddress = x.ReturnAddress.AddressLine1 + " " + x.ReturnAddress.AddressLine2,
                                              ReturnAddressCity = x.ReturnAddress.City.Name,
                                              ReturnAddressState = x.ReturnAddress.State.Name,
                                              ReturnAddressCountry = x.ReturnAddress.Country.Name,
                                              ParcelId = x.ParcelId,
                                              ParcelKey = x.Parcel.Guid,
                                              Parcel = x.Parcel.ParcelId,
                                              ParcelDescription = x.Parcel.Description,
                                              MerchantId = x.MerchantId,
                                              MerchantKey = x.Merchant.Guid,
                                              MerchantDisplayName = x.Merchant.DisplayName,
                                              MerchantMobileNumber = x.Merchant.MobileNumber,
                                              MerchantEmailAddress = x.Merchant.EmailAddress,
                                              MerchantIconUrl = x.Merchant.IconStorage.Path,
                                              MerchantAddress = x.Merchant.Address,
                                              StoreId = x.StoreId,
                                              StoreKey = x.Store.Guid,
                                              StoreDisplayName = x.Store.Name,
                                              StoreMobileNumber = x.Store.MobileNumber,
                                              StoreEmailAddress = x.Store.EmailAddress,
                                              StoreIconUrl = x.Store.IconStorage.Path,
                                              StoreAddress = x.Store.Address,
                                              CustomerId = x.CustomerId,
                                              CustomerKey = x.Customer.Guid,
                                              CustomerDisplayName = x.Customer.DisplayName,
                                              CustomerMobileNumber = x.Customer.MobileNumber,
                                              CustomerEmailAddress = x.Customer.EmailAddress,
                                              CustomerIconUrl = x.Customer.IconStorage.Path,
                                              CustomerAddress = x.Customer.Address,
                                              DealId = x.DealId,
                                              DealKey = x.Deal.Guid,
                                              DealTitle = x.Deal.Title,
                                              DealPrice = x.DealPrice,
                                              DealIconUrl = x.Deal.PosterStorage.Path,
                                              RateId = x.RateId,
                                              RateAmount = x.RateAmount,
                                              ItemCount = x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault(),
                                              TotalAmount = x.TotalAmount,
                                              TrackingNumber = x.TrackingNumber,
                                              Bill = x.Bill.Path,
                                              CarrierId = x.CarrierId,
                                              CarrierKey = x.Carrier.Guid,
                                              CarrierName = x.Carrier.Name,
                                              CarrierMobileNumber = x.Carrier.MobileNumber,
                                              CarrierEmailAddress = x.Carrier.EmailAddress,
                                              CarrierIconUrl = x.Carrier.IconUrl,
                                              PickUpDate = x.PickUpDate,
                                              DeliveryDate = x.DeliveryDate,
                                              ShipmentId = x.ShipmentId,
                                              CreateDate = x.CreateDate,
                                              CreatedBYId = x.CreatedById,
                                              CreatedByDisplayName = x.CreatedBy.DisplayName,
                                              ModifyDate = x.ModifyDate,
                                              ModifyById = x.ModifyById,
                                              ModifyByDisplayName = x.ModifyBy.DisplayName,
                                              StatusId = x.StatusId,
                                              StatusCode = x.Status.SystemName,
                                              StatusName = x.Status.Name,
                                              InvoiceNumber = "#" + x.DealId.ToString() + "" + x.Id.ToString()
                                          }).FirstOrDefault();
                    if (ShipmentDetails != null)
                    {
                        if(ShipmentDetails.ItemCount > 0)
                        {
                            ShipmentDetails.SubTotalAmount = ShipmentDetails.DealPrice * ShipmentDetails.ItemCount;
                        }
                        if (!string.IsNullOrEmpty(ShipmentDetails.MerchantIconUrl))
                        {
                            ShipmentDetails.MerchantIconUrl = _AppConfig.StorageUrl + ShipmentDetails.MerchantIconUrl;
                        }
                        else
                        {
                            ShipmentDetails.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(ShipmentDetails.StoreIconUrl))
                        {
                            ShipmentDetails.StoreIconUrl = _AppConfig.StorageUrl + ShipmentDetails.StoreIconUrl;
                        }
                        else
                        {
                            ShipmentDetails.StoreIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(ShipmentDetails.CustomerIconUrl))
                        {
                            ShipmentDetails.CustomerIconUrl = _AppConfig.StorageUrl + ShipmentDetails.CustomerIconUrl;
                        }
                        else
                        {
                            ShipmentDetails.CustomerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(ShipmentDetails.DealIconUrl))
                        {
                            ShipmentDetails.DealIconUrl = _AppConfig.StorageUrl + ShipmentDetails.DealIconUrl;
                        }
                        else
                        {
                            ShipmentDetails.DealIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(ShipmentDetails.CarrierIconUrl))
                        {
                            ShipmentDetails.CarrierIconUrl = ShipmentDetails.CarrierIconUrl;
                        }
                        else
                        {
                            ShipmentDetails.CarrierIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(ShipmentDetails.Bill))
                        {
                            ShipmentDetails.Bill = _AppConfig.StorageUrl + ShipmentDetails.Bill;
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ShipmentDetails, "LS0200", Resources.Resources.LS0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetShipment", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Gets the shipment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetShipments(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorShipmentsDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorShipmentsDownload>("ActorShipmentsDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.LSShipments
                            .Where(x => x.Customer.CountryId == _Request.UserReference.CountryId && x.CarrierId != null)
                            .Select(x => new OShipments.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                ToAddressId = x.ToAddressId,
                                ToAddressKey = x.ToAddress.Guid,
                                ToName = x.ToAddress.Name,
                                ToEmailAddress = x.ToAddress.EmailAddress,
                                ToMobileNumber = x.ToAddress.ContactNumber,
                                ToAddress = x.ToAddress.AddressLine1 + " " + x.ToAddress.AddressLine2,
                                ToAddressCity = x.ToAddress.City.Name,
                                ToAddressState = x.ToAddress.State.Name,
                                ToAddressCountry = x.ToAddress.Country.Name,
                                FromAddressId = x.FromAddressId,
                                FromAddressKey = x.FromAddress.Guid,
                                FromName = x.FromAddress.Name,
                                FromEmailAddress = x.FromAddress.EmailAddress,
                                FromMobileNumber = x.FromAddress.ContactNumber,
                                FromAddress = x.FromAddress.AddressLine1 + " " + x.FromAddress.AddressLine2,
                                FromAddressCity = x.FromAddress.City.Name,
                                FromAddressState = x.FromAddress.State.Name,
                                FromAddressCountry = x.FromAddress.Country.Name,
                                ReturnAddressId = x.ReturnAddressId,
                                ReturnAddressKey = x.ReturnAddress.Guid,
                                ReturnName = x.ReturnAddress.Name,
                                ReturnEmailAddress = x.ReturnAddress.EmailAddress,
                                ReturnMobileNumber = x.ReturnAddress.ContactNumber,
                                ReturnAddress = x.ReturnAddress.AddressLine1 + " " + x.ReturnAddress.AddressLine2,
                                ReturnAddressCity = x.ReturnAddress.City.Name,
                                ReturnAddressState = x.ReturnAddress.State.Name,
                                ReturnAddressCountry = x.ReturnAddress.Country.Name,
                                ParcelId = x.ParcelId,
                                ParcelKey = x.Parcel.Guid,
                                Parcel = x.Parcel.ParcelId,
                                ParcelDescription = x.Parcel.Description,
                                MerchantId = x.MerchantId,
                                MerchantKey = x.Merchant.Guid,
                                MerchantDisplayName = x.Merchant.DisplayName,
                                MerchantMobileNumber = x.Merchant.MobileNumber,
                                MerchantEmailAddress = x.Merchant.EmailAddress,
                                MerchantIconUrl = x.Merchant.IconStorage.Path,
                                MerchantAddress = x.Merchant.Address,
                                StoreId = x.StoreId,
                                StoreKey = x.Store.Guid,
                                StoreDisplayName = x.Store.Name,
                                StoreMobileNumber = x.Store.MobileNumber,
                                StoreEmailAddress = x.Store.EmailAddress,
                                StoreIconUrl = x.Store.IconStorage.Path,
                                StoreAddress = x.Store.Address,
                                CustomerId = x.CustomerId,
                                CustomerKey = x.Customer.Guid,
                                CustomerDisplayName = x.Customer.DisplayName,
                                CustomerMobileNumber = x.Customer.MobileNumber,
                                CustomerEmailAddress = x.Customer.EmailAddress,
                                CustomerIconUrl = x.Customer.IconStorage.Path,
                                CustomerAddress = x.Customer.Address,
                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,
                                DealPrice = x.DealPrice,
                                DealIconUrl = x.Deal.PosterStorage.Path,
                                RateId = x.RateId,
                                RateAmount = x.RateAmount,
                                ItemCount = x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault(),
                                TotalAmount = x.TotalAmount,
                                TrackingNumber = x.TrackingNumber,
                                Bill = x.Bill.Path,
                                CarrierId = x.CarrierId,
                                CarrierKey = x.Carrier.Guid,
                                CarrierName = x.Carrier.Name,
                                CarrierMobileNumber = x.Carrier.MobileNumber,
                                CarrierEmailAddress = x.Carrier.EmailAddress,
                                CarrierIconUrl = x.Carrier.IconUrl,
                                PickUpDate = x.PickUpDate,
                                DeliveryDate = x.DeliveryDate,
                                ShipmentId = x.ShipmentId,
                                CreateDate = x.CreateDate,
                                CreatedBYId = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                                InvoiceNumber = "#" + x.DealId.ToString() + "" + x.Id.ToString()
                            }).Where(_Request.SearchCondition)
                                             .Count();
                    }

                    List<OShipments.List> Shipments = _HCoreContext.LSShipments
                            .Where(x => x.Customer.CountryId == _Request.UserReference.CountryId && x.CarrierId != null)
                                                            .Select(x => new OShipments.List
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                ToAddressId = x.ToAddressId,
                                                                ToAddressKey = x.ToAddress.Guid,
                                                                ToName = x.ToAddress.Name,
                                                                ToEmailAddress = x.ToAddress.EmailAddress,
                                                                ToMobileNumber = x.ToAddress.ContactNumber,
                                                                ToAddress = x.ToAddress.AddressLine1 + " " + x.ToAddress.AddressLine2,
                                                                ToAddressCity = x.ToAddress.City.Name,
                                                                ToAddressState = x.ToAddress.State.Name,
                                                                ToAddressCountry = x.ToAddress.Country.Name,
                                                                FromAddressId = x.FromAddressId,
                                                                FromAddressKey = x.FromAddress.Guid,
                                                                FromName = x.FromAddress.Name,
                                                                FromEmailAddress = x.FromAddress.EmailAddress,
                                                                FromMobileNumber = x.FromAddress.ContactNumber,
                                                                FromAddress = x.FromAddress.AddressLine1 + " " + x.FromAddress.AddressLine2,
                                                                FromAddressCity = x.FromAddress.City.Name,
                                                                FromAddressState = x.FromAddress.State.Name,
                                                                FromAddressCountry = x.FromAddress.Country.Name,
                                                                ReturnAddressId = x.ReturnAddressId,
                                                                ReturnAddressKey = x.ReturnAddress.Guid,
                                                                ReturnName = x.ReturnAddress.Name,
                                                                ReturnEmailAddress = x.ReturnAddress.EmailAddress,
                                                                ReturnMobileNumber = x.ReturnAddress.ContactNumber,
                                                                ReturnAddress = x.ReturnAddress.AddressLine1 + " " + x.ReturnAddress.AddressLine2,
                                                                ReturnAddressCity = x.ReturnAddress.City.Name,
                                                                ReturnAddressState = x.ReturnAddress.State.Name,
                                                                ReturnAddressCountry = x.ReturnAddress.Country.Name,
                                                                ParcelId = x.ParcelId,
                                                                ParcelKey = x.Parcel.Guid,
                                                                Parcel = x.Parcel.ParcelId,
                                                                ParcelDescription = x.Parcel.Description,
                                                                MerchantId = x.MerchantId,
                                                                MerchantKey = x.Merchant.Guid,
                                                                MerchantDisplayName = x.Merchant.DisplayName,
                                                                MerchantMobileNumber = x.Merchant.MobileNumber,
                                                                MerchantEmailAddress = x.Merchant.EmailAddress,
                                                                MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                                MerchantAddress = x.Merchant.Address,
                                                                StoreId = x.StoreId,
                                                                StoreKey = x.Store.Guid,
                                                                StoreDisplayName = x.Store.Name,
                                                                StoreMobileNumber = x.Store.MobileNumber,
                                                                StoreEmailAddress = x.Store.EmailAddress,
                                                                StoreIconUrl = x.Store.IconStorage.Path,
                                                                StoreAddress = x.Store.Address,
                                                                CustomerId = x.CustomerId,
                                                                CustomerKey = x.Customer.Guid,
                                                                CustomerDisplayName = x.Customer.DisplayName,
                                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                                CustomerEmailAddress = x.Customer.EmailAddress,
                                                                CustomerIconUrl = x.Customer.IconStorage.Path,
                                                                CustomerAddress = x.Customer.Address,
                                                                DealId = x.DealId,
                                                                DealKey = x.Deal.Guid,
                                                                DealTitle = x.Deal.Title,
                                                                DealPrice = x.DealPrice,
                                                                DealIconUrl = x.Deal.PosterStorage.Path,
                                                                RateId = x.RateId,
                                                                RateAmount = x.RateAmount,
                                                                ItemCount = x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault(),
                                                                TotalAmount = x.TotalAmount,
                                                                TrackingNumber = x.TrackingNumber,
                                                                Bill = x.Bill.Path,
                                                                CarrierId = x.CarrierId,
                                                                CarrierKey = x.Carrier.Guid,
                                                                CarrierName = x.Carrier.Name,
                                                                CarrierMobileNumber = x.Carrier.MobileNumber,
                                                                CarrierEmailAddress = x.Carrier.EmailAddress,
                                                                CarrierIconUrl = x.Carrier.IconUrl,
                                                                PickUpDate = x.PickUpDate,
                                                                DeliveryDate = x.DeliveryDate,
                                                                ShipmentId = x.ShipmentId,
                                                                CreateDate = x.CreateDate,
                                                                CreatedBYId = x.CreatedById,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                                ModifyDate = x.ModifyDate,
                                                                ModifyById = x.ModifyById,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,
                                                                InvoiceNumber = "#" + x.DealId.ToString() + "" + x.Id.ToString()
                                                            }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();

                    foreach (var DataItem in Shipments)
                    {
                        if (DataItem.ItemCount > 0)
                        {
                            DataItem.SubTotalAmount = DataItem.DealPrice * DataItem.ItemCount;
                        }
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.StoreIconUrl))
                        {
                            DataItem.StoreIconUrl = _AppConfig.StorageUrl + DataItem.StoreIconUrl;
                        }
                        else
                        {
                            DataItem.StoreIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.CustomerIconUrl))
                        {
                            DataItem.CustomerIconUrl = _AppConfig.StorageUrl + DataItem.CustomerIconUrl;
                        }
                        else
                        {
                            DataItem.CustomerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.DealIconUrl))
                        {
                            DataItem.DealIconUrl = _AppConfig.StorageUrl + DataItem.DealIconUrl;
                        }
                        else
                        {
                            DataItem.DealIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.CarrierIconUrl))
                        {
                            DataItem.CarrierIconUrl = DataItem.CarrierIconUrl;
                        }
                        else
                        {
                            DataItem.CarrierIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.Bill))
                        {
                            DataItem.Bill = _AppConfig.StorageUrl + DataItem.Bill;
                        }
                    }

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Shipments, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "LS0200", Resources.Resources.LS0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetShipments", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Gets the orders overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOrdersOverview(OReference _Request)
        {
            try
            {
                _OrdersOverview = new OShipments.OrdersOverview();
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        _OrdersOverview.TotalOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Count();
                        _OrdersOverview.TotalOrdersAmount = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Sum(x => x.TotalAmount);
                        _OrdersOverview.NewOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && (x.StatusId == HelperStatus.OrderStatus.New || x.StatusId == HelperStatus.OrderStatus.PendingConfirmation) && x.CarrierId != null).Count();
                        _OrdersOverview.ReadyToPickUpOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && (x.StatusId == HelperStatus.OrderStatus.ReadyToPickup || x.StatusId == HelperStatus.OrderStatus.Ready) && x.CarrierId != null).Count();
                        _OrdersOverview.ProcessingOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.OrderStatus.Preparing && x.CarrierId != null).Count();
                        _OrdersOverview.DispatchedOreders = _HCoreContext.LSShipments
                                                            .Where(x => x.MerchantId == _Request.AccountId
                                                            && x.Merchant.Guid == _Request.AccountKey
                                                            && x.Merchant.CountryId == _Request.UserReference.CountryId
                                                            && (x.StatusId == HelperStatus.OrderStatus.OutForDelivery
                                                            || x.StatusId == HelperStatus.OrderStatus.Delivered
                                                            || x.StatusId == HelperStatus.OrderStatus.PickUped
                                                            || x.StatusId == HelperStatus.OrderStatus.ProcessedAtFacility
                                                            || x.StatusId == HelperStatus.OrderStatus.DepartedToFacility
                                                            || x.StatusId == HelperStatus.OrderStatus.DepartedFromFacility
                                                            || x.StatusId == HelperStatus.OrderStatus.ArrivedAtFacility) && x.CarrierId != null)
                                                            .Count();
                        _OrdersOverview.FailedOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.OrderStatus.DeliveryFailed && x.CarrierId != null).Count();
                        _OrdersOverview.TotalCancelledOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && (x.StatusId == HelperStatus.OrderStatus.CancelledByUser || x.StatusId == HelperStatus.OrderStatus.CancelledBySeller || x.StatusId == HelperStatus.OrderStatus.CancelledBySystem) && x.CarrierId != null).Count();
                        _OrdersOverview.TotalDeliveredOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.OrderStatus.Delivered && x.CarrierId != null).Count();
                        _OrdersOverview.CancelledOredersAmount = _HCoreContext.LSShipments.Where(x => (x.StatusId == HelperStatus.OrderStatus.CancelledByUser || x.StatusId == HelperStatus.OrderStatus.CancelledBySeller || x.StatusId == HelperStatus.OrderStatus.CancelledBySystem) && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Sum(a => a.TotalAmount);
                        _OrdersOverview.DeliveredOredersAmount = _HCoreContext.LSShipments.Where(x => (x.StatusId == HelperStatus.OrderStatus.Delivered) && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Sum(a => a.TotalAmount);
                        _OrdersOverview.TotalOrderss = _HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.Deal.Account.CountryId == _Request.UserReference.CountryId && x.Deal.DealTypeId == Helpers.DealType.ProductDeal && (x.Deal.DeliveryTypeId == Helpers.DeliveryType.Delivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStorePickUp)).Count();
                        _OrdersOverview.TotalOrdersAmountt = _HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.Deal.Account.CountryId == _Request.UserReference.CountryId && x.Deal.DealTypeId == Helpers.DealType.ProductDeal && (x.Deal.DeliveryTypeId == Helpers.DeliveryType.Delivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStorePickUp)).Sum(a => a.Deal.SellingPrice);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OrdersOverview, "LS0200", Resources.Resources.LS0200);
                    }
                    else
                    {
                        _OrdersOverview.TotalOrders = _HCoreContext.LSShipments.Where(x => x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Count();
                        _OrdersOverview.TotalOrdersAmount = _HCoreContext.LSShipments.Where(x => x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Sum(x => x.TotalAmount);
                        _OrdersOverview.NewOrders = _HCoreContext.LSShipments.Where(x => (x.StatusId == HelperStatus.OrderStatus.New || x.StatusId == HelperStatus.OrderStatus.PendingConfirmation) && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Count();
                        _OrdersOverview.ReadyToPickUpOrders = _HCoreContext.LSShipments.Where(x => (x.StatusId == HelperStatus.OrderStatus.ReadyToPickup || x.StatusId == HelperStatus.OrderStatus.Ready) && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Count();
                        _OrdersOverview.ProcessingOrders = _HCoreContext.LSShipments.Count(x => x.StatusId == HelperStatus.OrderStatus.Preparing && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null);
                        _OrdersOverview.DispatchedOreders = _HCoreContext.LSShipments
                                                             .Where(x => (x.StatusId == HelperStatus.OrderStatus.OutForDelivery
                                                             && x.Merchant.CountryId == _Request.UserReference.CountryId
                                                             || x.StatusId == HelperStatus.OrderStatus.Delivered
                                                             || x.StatusId == HelperStatus.OrderStatus.PickUped
                                                             || x.StatusId == HelperStatus.OrderStatus.ProcessedAtFacility
                                                             || x.StatusId == HelperStatus.OrderStatus.DepartedToFacility
                                                             || x.StatusId == HelperStatus.OrderStatus.DepartedFromFacility
                                                             || x.StatusId == HelperStatus.OrderStatus.ArrivedAtFacility) && x.CarrierId != null)
                                                             .Count();
                        _OrdersOverview.FailedOrders = _HCoreContext.LSShipments.Count(x => x.StatusId == HelperStatus.OrderStatus.DeliveryFailed && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null);
                        _OrdersOverview.TotalCancelledOrders = _HCoreContext.LSShipments.Count(x => x.Merchant.CountryId == _Request.UserReference.CountryId && (x.StatusId == HelperStatus.OrderStatus.CancelledByUser || x.StatusId == HelperStatus.OrderStatus.CancelledBySeller || x.StatusId == HelperStatus.OrderStatus.CancelledBySystem) && x.CarrierId != null);
                        _OrdersOverview.TotalDeliveredOrders = _HCoreContext.LSShipments.Count(x => x.StatusId == HelperStatus.OrderStatus.Delivered && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null);
                        _OrdersOverview.CancelledOredersAmount = _HCoreContext.LSShipments.Where(x => (x.StatusId == HelperStatus.OrderStatus.CancelledByUser || x.StatusId == HelperStatus.OrderStatus.CancelledBySeller || x.StatusId == HelperStatus.OrderStatus.CancelledBySystem) && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Sum(a => a.TotalAmount);
                        _OrdersOverview.DeliveredOredersAmount = _HCoreContext.LSShipments.Where(x => x.StatusId == HelperStatus.OrderStatus.Delivered && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Sum(a => a.TotalAmount);
                        _OrdersOverview.TotalOrderss = _HCoreContext.MDDealCode.Where(x => x.Account.CountryId == _Request.UserReference.CountryId && x.Deal.DealTypeId == Helpers.DealType.ProductDeal && x.Deal.Account.CountryId == _Request.UserReference.CountryId && (x.Deal.DeliveryTypeId == Helpers.DeliveryType.Delivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStorePickUp)).Count();
                        _OrdersOverview.TotalOrdersAmountt = _HCoreContext.MDDealCode.Where(x => x.Account.CountryId == _Request.UserReference.CountryId && x.Deal.DealTypeId == Helpers.DealType.ProductDeal && x.Deal.Account.CountryId == _Request.UserReference.CountryId && (x.Deal.DeliveryTypeId == Helpers.DeliveryType.Delivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStorePickUp)).Sum(a => a.Deal.SellingPrice);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OrdersOverview, "LS0200", Resources.Resources.LS0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetOrdersOverview", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Gets the overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOverview(OReference _Request)
        {
            try
            {
                _Overview = new OShipments.Overview();
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        _Overview.TotalPickUpOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.OrderStatus.ReadyToPickup && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Count();
                        _Overview.PickUpOredersAmount = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.OrderStatus.ReadyToPickup && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Sum(x => x.TotalAmount);
                        _Overview.TotalOredersAmount = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && x.CarrierId != null).Sum(x => x.TotalAmount);
                        _Overview.TotalDeliveredOrders = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.OrderStatus.Delivered && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Count();
                        _Overview.DeliveredOredersAmount = _HCoreContext.LSShipments.Where(x => x.MerchantId == _Request.AccountId && x.Merchant.Guid == _Request.AccountKey && x.Merchant.CountryId == _Request.UserReference.CountryId && x.StatusId == HelperStatus.OrderStatus.Delivered && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Sum(a => a.TotalAmount);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "LS0200", Resources.Resources.LS0200);
                    }
                    else
                    {
                        _Overview.TotalPickUpOrders = _HCoreContext.LSShipments.Where(x => x.StatusId == HelperStatus.OrderStatus.ReadyToPickup && x.Merchant.CountryId == _Request.UserReference.CountryId && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Count();
                        _Overview.PickUpOredersAmount = _HCoreContext.LSShipments.Where(x => x.StatusId == HelperStatus.OrderStatus.ReadyToPickup && x.Merchant.CountryId == _Request.UserReference.CountryId && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Sum(x => x.TotalAmount);
                        _Overview.TotalOredersAmount = _HCoreContext.LSShipments.Where(x => x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && x.Merchant.CountryId == _Request.UserReference.CountryId && x.CarrierId != null).Sum(x => x.TotalAmount);
                        _Overview.TotalDeliveredOrders = _HCoreContext.LSShipments.Where(x => x.StatusId == HelperStatus.OrderStatus.Delivered && x.Merchant.CountryId == _Request.UserReference.CountryId && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Count();
                        _Overview.DeliveredOredersAmount = _HCoreContext.LSShipments.Where(x => x.StatusId == HelperStatus.OrderStatus.Delivered && x.Merchant.CountryId == _Request.UserReference.CountryId && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate) && x.CarrierId != null).Sum(a => a.TotalAmount);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "LS0200", Resources.Resources.LS0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetOverview", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        /// <summary>
        /// Description: Creates the shipments.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OShipments.Save.Response.</returns>
        internal OShipments.Save.Response CreateShipments(OShipments.Save.Request _Request)
        {
            _ShipmentResponse = new OShipments.Save.Response();
            try
            {
                _FrameworkDelivery = new FrameworkDelivery();
                _CreateShipment = new CreateShipment();


                long? CustomerId = HCoreHelper.GetUserAccountId(_Request.CustomerKey, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    var ParcelId = _HCoreContext.LSParcel.Where(x => x.ParcelId == _Request.ParcelReference).Select(a => a.Id).FirstOrDefault();
                    var CustomerAddress = _HCoreContext.MDDealAddress.Where(x => x.AddressReference == _Request.CustomerAddressReference).FirstOrDefault();
                    var MerchantAddress = _HCoreContext.MDDealAddress.Where(x => x.AddressReference == _Request.MerchantAddressReference).FirstOrDefault();
                    var DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId).Select(a => new
                    {
                        DealId = a.Id,
                        DealPrice = a.SellingPrice
                    }).FirstOrDefault();

                    _CreateShipmentRequest = new CreateShipmentRequest(_Request.MerchantAddressReference, _Request.CustomerAddressReference, _Request.MerchantAddressReference, "", _Request.ParcelReference);
                    _CreateShipment = _FrameworkDelivery.CreateShipment(_CreateShipmentRequest);
                    if (_CreateShipment.status == true)
                    {
                        var ShipmentDetails = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (ShipmentDetails != null)
                        {
                            if (_Request.MerchantId > 0)
                            {
                                ShipmentDetails.MerchantId = _Request.MerchantId;
                            }
                            if (CustomerId > 0)
                            {
                                ShipmentDetails.CustomerId = CustomerId;
                            }
                            ShipmentDetails.ToAddressId = CustomerAddress.AddressId;
                            ShipmentDetails.FromAddressId = MerchantAddress.AddressId;
                            ShipmentDetails.ReturnAddressId = MerchantAddress.AddressId;
                            ShipmentDetails.ParcelId = ParcelId;
                            ShipmentDetails.DealId = DealDetails.DealId;
                            ShipmentDetails.DealPrice = DealDetails.DealPrice;
                            ShipmentDetails.UserId = _CreateShipment.data.user.user_id;
                            ShipmentDetails.ShipmentId = _CreateShipment.data.shipment_id;
                            ShipmentDetails.TId = _CreateShipment.data._id;
                            ShipmentDetails.StatusId = HelperStatus.OrderStatus.Confirmed;
                            ShipmentDetails.CreateDate = HCoreHelper.GetGMTDateTime();
                            ShipmentDetails.CreatedById = CustomerId;

                            _HCoreContext.SaveChanges();
                            _ShipmentResponse.ReferenceId = ShipmentDetails.Id;
                            _ShipmentResponse.ReferenceKey = ShipmentDetails.Guid;
                            _ShipmentResponse.ShipmentId = ShipmentDetails.ShipmentId;
                            _ShipmentResponse.RateAmount = ShipmentDetails.RateAmount;
                            _ShipmentResponse.RateId = ShipmentDetails.RateId;
                            return _ShipmentResponse;
                        }
                        else
                        {
                            _ShipmentResponse.Status = "01";
                            _ShipmentResponse.Message = "Shipment details not found";
                            return _ShipmentResponse;
                        }
                    }
                    else
                    {
                        _ShipmentResponse.Status = "01";
                        _ShipmentResponse.Message = "Failed to create shipment";
                        return _ShipmentResponse;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateShipment", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
                _ShipmentResponse.Status = "01";
                _ShipmentResponse.Message = "Failed to create shipment";
                return _ShipmentResponse;
            }
        }

        /// <summary>
        /// Description: Checks the rate for shioments.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>GetRatesForShipments.</returns>
        internal GetRatesForShipments CheckRateForShioments(OShipments.CheckRates _Request)
        {
            try
            {
                _GetRatesForShipments = new GetRatesForShipments();
                _FrameworkDelivery = new FrameworkDelivery();

                using (_HCoreContext = new HCoreContext())
                {
                    _GetRatesRequest = new GetRatesRequest(_Request.ParcelId, _Request.ShipmentId, _Request.FromAddressId, _Request.ToAddressId);
                    _GetRatesForShipments = _FrameworkDelivery.GetRatesForShipmentCore(_GetRatesRequest);

                    if (_GetRatesForShipments.status == true)
                    {
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_GetRatesForShipments.data.Count, _GetRatesForShipments, 0, _GetRatesForShipments.data.Count);
                        return _GetRatesForShipments;
                    }
                    else
                    {
                        _GetRatesForShipments.status = false;
                        _GetRatesForShipments.message = "Failed to get rates for shipment";
                        return _GetRatesForShipments;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CheckRateForshioment", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
                _GetRatesForShipments.status = false;
                _GetRatesForShipments.message = "Failed to get rates for shipment";
                return _GetRatesForShipments;
            }
        }

        /// <summary>
        /// Description: Updates the order status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateOrderStatus(OReference _Request)
        {
            try
            {
                _OShipmentsRequest = new OShipments.Save.Request();
                _UpdateResponse = new OShipments.UpdateResponse();
                _UpdateOrder = new OShipments.Update();
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSREF", "ReferenceId required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSREFKEY", "Referene key required");
                }
                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSINSTATUS", "Invalid order status");
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    var OrderDetails = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                       .Select(x => new
                                       {
                                           CustomerId = x.CustomerId,
                                           CustomerKey = x.Customer.Guid,
                                           ParcelId = x.Parcel.ParcelId,
                                           MerchantId = x.MerchantId,
                                           DealId = x.DealId,
                                           DealKey = x.Deal.Guid,
                                       })
                                       .FirstOrDefault();
                    string CustomerAddress = _HCoreContext.MDDealAddress.Where(x => x.AddressId == Details.ToAddressId && x.AccountId == OrderDetails.CustomerId).Select(a => a.AddressReference).FirstOrDefault();
                    string MerchantAddress = _HCoreContext.MDDealAddress.Where(x => x.AddressId == Details.FromAddressId && x.AccountId == OrderDetails.MerchantId).Select(a => a.AddressReference).FirstOrDefault();
                    if (Details != null)
                    {
                        if (StatusId > 0)
                        {
                            Details.StatusId = StatusId;
                        }
                        if (StatusId == HelperStatus.OrderStatus.Confirmed)
                        {
                            _UpdateOrder.RateAmount = Details.RateAmount;
                            _UpdateOrder.RateId = Details.RateId;
                            _UpdateOrder.ReferenceId = Details.Id;
                            _UpdateOrder.ReferenceKey = Details.Guid;
                            _UpdateOrder.UserReference = _Request.UserReference;
                            _UpdateResponse = UpdateOrder(_UpdateOrder);
                            if (_UpdateResponse != null)
                            {
                                _UpdateResponse.DeliveryDate = _UpdateResponse.DeliveryDate;
                                _UpdateResponse.PickUpDate = _UpdateResponse.PickUpDate;
                                _UpdateResponse.ShipmentLable = _UpdateResponse.ShipmentLable;
                                _UpdateResponse.TrackingNumber = _UpdateResponse.TrackingNumber;

                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _UpdateResponse, "LSSUCCESS", "Order confirmed. Rider will arrive at " + _UpdateResponse.PickUpDate + ". Please print and paste the shipment lable on shipment box before rider arrive.");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LSUNABLE", "Unable to arrange shipment.");
                            }

                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "LSUPDATE", "Order details updated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", "Details not found. It might be removed");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateOrderStatus", _Exception, _Request.UserReference, "LS0500", "Unable to process your request. Please try after some time");
            }
        }

        /// <summary>
        /// Description: Updates the orders.
        /// </summary>
        public void UpdateOrders()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _FrameworkDelivery = new FrameworkDelivery();
                    GetShipments GetShipments = _FrameworkDelivery.GetShipments();
                    foreach (var Order in GetShipments.data.shipments)
                    {
                        var CheckOrder = _HCoreContext.LSShipments.Where(x => x.ShipmentId == Order.shipment_id).FirstOrDefault();
                        if (CheckOrder != null)
                        {
                            if (Order.status == "confirmed")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.Confirmed;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (Order.status == "PU")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.PickUped;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (Order.status == "DF")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.DepartedToFacility;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (Order.status == "AF")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.ArrivedAtFacility;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (Order.status == "PL")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.ProcessedAtFacility;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (Order.status == "DF")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.DepartedFromFacility;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (Order.status == "WC")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.OutForDelivery;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (Order.status == "OK")
                            {
                                CheckOrder.StatusId = HelperStatus.OrderStatus.Delivered;
                                CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                            }
                        }
                    }
                    _HCoreContext.SaveChanges();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateOrders", _Exception);
            }
        }

        /// <summary>
        /// Description: Updates the order status.
        /// </summary>
        /// <param name="ResponseContent">Content of the response.</param>
        public void UpdateOrderStatus(string ResponseContent)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _FrameworkDelivery = new FrameworkDelivery();
                    GetShipment Response = JsonConvert.DeserializeObject<GetShipment>(ResponseContent);
                    if (Response.data != null)
                    {
                        if (Response.data.events != null)
                        {
                            var CheckOrder = _HCoreContext.LSShipments.Where(x => x.ShipmentId == Response.data.shipment_id).FirstOrDefault();
                            foreach (var shipment in Response.data.events)
                            {
                                if (CheckOrder != null)
                                {
                                    if (shipment.status == "confirmed")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.Confirmed;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (shipment.status == "PU")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.PickUped;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (shipment.status == "DF")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.DepartedToFacility;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (shipment.status == "AF")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.ArrivedAtFacility;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (shipment.status == "PL")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.ProcessedAtFacility;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (shipment.status == "DF")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.DepartedFromFacility;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (shipment.status == "WC")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.OutForDelivery;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                    if (shipment.status == "OK")
                                    {
                                        CheckOrder.StatusId = HelperStatus.OrderStatus.Delivered;
                                        CheckOrder.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    }
                                }
                            }
                            _HCoreContext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateOrderStatus", _Exception);
            }
        }

        /// <summary>
        /// Description: Gets the shipment download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetShipmentDownload(OList.Request _Request)
        {
            try
            {
                _OrdersDownload = new List<OShipments.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.LSShipments
                            .Where(x => x.Customer.CountryId == _Request.UserReference.CountryId && x.CarrierId != null)
                                                        .Select(x => new OShipments.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,
                                                            ToAddressId = x.ToAddressId,
                                                            ToAddressKey = x.ToAddress.Guid,
                                                            ToName = x.ToAddress.Name,
                                                            ToEmailAddress = x.ToAddress.EmailAddress,
                                                            ToMobileNumber = x.ToAddress.ContactNumber,
                                                            FromAddressId = x.FromAddressId,
                                                            FromAddressKey = x.FromAddress.Guid,
                                                            FromName = x.FromAddress.Name,
                                                            FromEmailAddress = x.FromAddress.EmailAddress,
                                                            FromMobileNumber = x.FromAddress.ContactNumber,
                                                            ReturnAddressId = x.ReturnAddressId,
                                                            ReturnAddressKey = x.ReturnAddress.Guid,
                                                            ReturnName = x.ReturnAddress.Name,
                                                            ReturnEmailAddress = x.ReturnAddress.EmailAddress,
                                                            ReturnMobileNumber = x.ReturnAddress.ContactNumber,
                                                            ParcelId = x.ParcelId,
                                                            ParcelKey = x.Parcel.Guid,
                                                            Parcel = x.Parcel.ParcelId,
                                                            ParcelDescription = x.Parcel.Description,
                                                            MerchantId = x.MerchantId,
                                                            MerchantKey = x.Merchant.Guid,
                                                            MerchantDisplayName = x.Merchant.DisplayName,
                                                            MerchantMobileNumber = x.Merchant.MobileNumber,
                                                            MerchantEmailAddress = x.Merchant.EmailAddress,
                                                            MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                            MerchantAddress = x.Merchant.Address,
                                                            StoreId = x.StoreId,
                                                            StoreKey = x.Store.Guid,
                                                            StoreDisplayName = x.Store.Name,
                                                            StoreMobileNumber = x.Store.MobileNumber,
                                                            StoreEmailAddress = x.Store.EmailAddress,
                                                            StoreIconUrl = x.Store.IconStorage.Path,
                                                            StoreAddress = x.Store.Address,
                                                            CustomerId = x.CustomerId,
                                                            CustomerKey = x.Customer.Guid,
                                                            CustomerDisplayName = x.Customer.DisplayName,
                                                            CustomerMobileNumber = x.Customer.MobileNumber,
                                                            CustomerEmailAddress = x.Customer.EmailAddress,
                                                            CustomerIconUrl = x.Customer.IconStorage.Path,
                                                            CustomerAddress = x.Customer.Address,
                                                            DealId = x.DealId,
                                                            DealKey = x.Deal.Guid,
                                                            DealTitle = x.Deal.Title,
                                                            DealPrice = x.DealPrice,
                                                            DealIconUrl = x.Deal.PosterStorage.Path,
                                                            RateId = x.RateId,
                                                            RateAmount = x.RateAmount,
                                                            ItemCount = x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault(),
                                                            SubTotalAmount = (x.DealPrice * x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault()),
                                                            TotalAmount = (x.DealPrice * x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault()) + x.RateAmount,
                                                            TrackingNumber = x.TrackingNumber,
                                                            Bill = x.Bill.Path,
                                                            CarrierId = x.CarrierId,
                                                            CarrierKey = x.Carrier.Guid,
                                                            CarrierName = x.Carrier.Name,
                                                            CarrierMobileNumber = x.Carrier.MobileNumber,
                                                            CarrierEmailAddress = x.Carrier.EmailAddress,
                                                            CarrierIconUrl = x.Carrier.IconUrl,
                                                            PickUpDate = x.PickUpDate,
                                                            DeliveryDate = x.DeliveryDate,
                                                            ShipmentId = x.ShipmentId,
                                                            CreateDate = x.CreateDate,
                                                            CreatedBYId = x.CreatedById,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                            ModifyDate = x.ModifyDate,
                                                            ModifyById = x.ModifyById,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        }).Where(_Request.SearchCondition)
                                             .Count();
                    }
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        var Shipments = _HCoreContext.LSShipments
                            .Where(x => x.Customer.CountryId == _Request.UserReference.CountryId && x.CarrierId != null)
                                                            .Select(x => new OShipments.List
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,
                                                                ToAddressId = x.ToAddressId,
                                                                ToAddressKey = x.ToAddress.Guid,
                                                                ToName = x.ToAddress.Name,
                                                                ToEmailAddress = x.ToAddress.EmailAddress,
                                                                ToMobileNumber = x.ToAddress.ContactNumber,
                                                                FromAddressId = x.FromAddressId,
                                                                FromAddressKey = x.FromAddress.Guid,
                                                                FromName = x.FromAddress.Name,
                                                                FromEmailAddress = x.FromAddress.EmailAddress,
                                                                FromMobileNumber = x.FromAddress.ContactNumber,
                                                                ReturnAddressId = x.ReturnAddressId,
                                                                ReturnAddressKey = x.ReturnAddress.Guid,
                                                                ReturnName = x.ReturnAddress.Name,
                                                                ReturnEmailAddress = x.ReturnAddress.EmailAddress,
                                                                ReturnMobileNumber = x.ReturnAddress.ContactNumber,
                                                                ParcelId = x.ParcelId,
                                                                ParcelKey = x.Parcel.Guid,
                                                                Parcel = x.Parcel.ParcelId,
                                                                ParcelDescription = x.Parcel.Description,
                                                                MerchantId = x.MerchantId,
                                                                MerchantKey = x.Merchant.Guid,
                                                                MerchantDisplayName = x.Merchant.DisplayName,
                                                                MerchantMobileNumber = x.Merchant.MobileNumber,
                                                                MerchantEmailAddress = x.Merchant.EmailAddress,
                                                                MerchantIconUrl = x.Merchant.IconStorage.Path,
                                                                MerchantAddress = x.Merchant.Address,
                                                                StoreId = x.StoreId,
                                                                StoreKey = x.Store.Guid,
                                                                StoreDisplayName = x.Store.Name,
                                                                StoreMobileNumber = x.Store.MobileNumber,
                                                                StoreEmailAddress = x.Store.EmailAddress,
                                                                StoreIconUrl = x.Store.IconStorage.Path,
                                                                StoreAddress = x.Store.Address,
                                                                CustomerId = x.CustomerId,
                                                                CustomerKey = x.Customer.Guid,
                                                                CustomerDisplayName = x.Customer.DisplayName,
                                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                                CustomerEmailAddress = x.Customer.EmailAddress,
                                                                CustomerIconUrl = x.Customer.IconStorage.Path,
                                                                CustomerAddress = x.Customer.Address,
                                                                DealId = x.DealId,
                                                                DealKey = x.Deal.Guid,
                                                                DealTitle = x.Deal.Title,
                                                                DealPrice = x.DealPrice,
                                                                DealIconUrl = x.Deal.PosterStorage.Path,
                                                                RateId = x.RateId,
                                                                RateAmount = x.RateAmount,
                                                                ItemCount = x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault(),
                                                                SubTotalAmount = (x.DealPrice * x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault()),
                                                                TotalAmount = (x.DealPrice * x.MDDealCode.Where(a => a.OrderId == x.Id).Select(a => a.ItemCount).FirstOrDefault() ) + x.RateAmount,
                                                                TrackingNumber = x.TrackingNumber,
                                                                Bill = x.Bill.Path,
                                                                CarrierId = x.CarrierId,
                                                                CarrierKey = x.Carrier.Guid,
                                                                CarrierName = x.Carrier.Name,
                                                                CarrierMobileNumber = x.Carrier.MobileNumber,
                                                                CarrierEmailAddress = x.Carrier.EmailAddress,
                                                                CarrierIconUrl = x.Carrier.IconUrl,
                                                                PickUpDate = x.PickUpDate,
                                                                DeliveryDate = x.DeliveryDate,
                                                                ShipmentId = x.ShipmentId,
                                                                CreateDate = x.CreateDate,
                                                                CreatedBYId = x.CreatedById,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                                ModifyDate = x.ModifyDate,
                                                                ModifyById = x.ModifyById,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name
                                                            }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();

                        foreach (var _DataItem in Shipments)
                        {
                            _OrdersDownload.Add(new OShipments.ListDownload
                            {
                                OrderId = _DataItem.ReferenceId,
                                CarrierName = _DataItem.CarrierName,
                                CarrierMobileNumber = _DataItem.CarrierMobileNumber,
                                CustomerName = _DataItem.CustomerDisplayName,
                                CustomerMobileNumber = _DataItem.CustomerMobileNumber,
                                MerchantName = _DataItem.MerchantDisplayName,
                                MerchantMobileNumber = _DataItem.MerchantMobileNumber,
                                DeliveryCharge = _DataItem.RateAmount,
                                DeliveryDate = _DataItem.DeliveryDate,
                                TotalAmount = _DataItem.TotalAmount,
                                PlacedDate = _DataItem.CreateDate,
                                ProductPrice = _DataItem.DealPrice,
                                OrderStatus = _DataItem.StatusName
                            });
                        }
                        _Request.Offset += 800;
                    }
                }
                _HCoreContext.Dispose();
                HCoreHelper.CreateDownload("OrderHistory", _OrdersDownload, _Request.UserReference);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetShipments", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
            }
        }

        OShipments.DateRangeResponse _DateRangeResponse;
        List<OShipments.DateRange> _DateRanges;
        OShipments.Sales _Sales;
        /// <summary>
        /// Description: Gets the order history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOrderHistory(OShipments.Request _Request)
        {
            _DateRangeResponse = new OShipments.DateRangeResponse();
            _DateRanges = new List<OShipments.DateRange>();
            try
            {
                int Days = (_Request.EndTime - _Request.StartTime).Days;
                DateTime StartTime = _Request.StartTime;
                DateTime EndTime = StartTime.AddDays(1);

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0)
                    {
                        for (int i = 0; i < Days; i++)
                        {
                            DateTime TDayStart = StartTime;
                            DateTime TDayEnd = EndTime.AddHours(24).AddSeconds(-1);
                            DateTime T7DayStart = TDayStart.Date.AddDays(-7);
                            DateTime T7DayEnd = TDayStart.Date.AddSeconds(-1);
                            DateTime TDeadDayEnd = TDayStart.AddDays(-7);
                            _Sales = new OShipments.Sales();
                            _Sales.Amount = _HCoreContext.LSShipments.Where(x => x.DeliveryDate >= _Request.StartTime && x.DeliveryDate <= _Request.EndTime && x.Merchant.CountryId == _Request.UserReference.CountryId).Sum(a => a.TotalAmount);
                            _DateRanges.Add(new OShipments.DateRange
                            {
                                Data = _Sales,
                                Date = StartTime,
                                StartDate = StartTime,
                                EndDate = EndTime,
                            });
                            StartTime = StartTime.AddDays(1);
                            EndTime = EndTime.AddDays(1);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < Days; i++)
                        {
                            DateTime TDayStart = StartTime;
                            DateTime TDayEnd = EndTime.AddHours(24).AddSeconds(-1);
                            DateTime T7DayStart = TDayStart.Date.AddDays(-7);
                            DateTime T7DayEnd = TDayStart.Date.AddSeconds(-1);
                            DateTime TDeadDayEnd = TDayStart.AddDays(-7);
                            _Sales = new OShipments.Sales();
                            _Sales.Amount = _HCoreContext.LSShipments.Where(x => x.DeliveryDate >= _Request.StartTime && x.DeliveryDate <= _Request.EndTime && x.Merchant.CountryId == _Request.UserReference.CountryId).Sum(a => a.TotalAmount);
                            _DateRanges.Add(new OShipments.DateRange
                            {
                                Data = _Sales,
                                Date = StartTime,
                                StartDate = StartTime,
                                EndDate = EndTime,
                            });
                            StartTime = StartTime.AddDays(1);
                            EndTime = EndTime.AddDays(1);
                        }
                    }
                }
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DateRanges, "HC0001", "Details loaded");
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetOrderHistory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }

        /// <summary>
        /// Description: Gets the order list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOrders(OList.Request _Request)
        {
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorOrdersDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorOrdersDownload>("ActorOrdersDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.MDDealCode
                                                .Where(x => x.Deal.DealTypeId == Helpers.DealType.ProductDeal
                                                && x.Deal.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OShipments.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    OrderId = x.OrderId,
                                                    ToAddressId = x.ToAddressId,
                                                    ToAddressKey = x.ToAddress.Guid,
                                                    ToName = x.ToAddress.Name,
                                                    ToEmailAddress = x.ToAddress.EmailAddress,
                                                    ToMobileNumber = x.ToAddress.ContactNumber,
                                                    ToAddress = x.ToAddress.AddressLine1 + " " + x.ToAddress.AddressLine2,
                                                    ToAddressCity = x.ToAddress.City.Name,
                                                    ToAddressState = x.ToAddress.State.Name,
                                                    ToAddressCountry = x.ToAddress.Country.Name,
                                                    FromAddressId = x.FromAddressId,
                                                    FromAddressKey = x.FromAddress.Guid,
                                                    FromName = x.FromAddress.Name,
                                                    FromEmailAddress = x.FromAddress.EmailAddress,
                                                    FromMobileNumber = x.FromAddress.ContactNumber,
                                                    FromAddress = x.FromAddress.AddressLine1 + " " + x.FromAddress.AddressLine2,
                                                    FromAddressCity = x.FromAddress.City.Name,
                                                    FromAddressState = x.FromAddress.State.Name,
                                                    FromAddressCountry = x.FromAddress.Country.Name,
                                                    ReturnAddressId = x.FromAddressId,
                                                    ReturnAddressKey = x.FromAddress.Guid,
                                                    ReturnName = x.FromAddress.Name,
                                                    ReturnEmailAddress = x.FromAddress.EmailAddress,
                                                    ReturnMobileNumber = x.FromAddress.ContactNumber,
                                                    ReturnAddress = x.FromAddress.AddressLine1 + " " + x.FromAddress.AddressLine2,
                                                    ReturnAddressCity = x.FromAddress.City.Name,
                                                    ReturnAddressState = x.FromAddress.State.Name,
                                                    ReturnAddressCountry = x.FromAddress.Country.Name,
                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,
                                                    MerchantMobileNumber = x.Deal.Account.MobileNumber,
                                                    MerchantEmailAddress = x.Account.EmailAddress,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                                    MerchantAddress = x.Deal.Account.Address,
                                                    CustomerId = x.AccountId,
                                                    CustomerKey = x.Account.Guid,
                                                    CustomerDisplayName = x.Account.DisplayName,
                                                    CustomerMobileNumber = x.Account.MobileNumber,
                                                    CustomerEmailAddress = x.Account.EmailAddress,
                                                    CustomerIconUrl = x.Account.IconStorage.Path,
                                                    CustomerAddress = x.Account.Address,
                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,
                                                    DealTitle = x.Deal.Title,
                                                    DealPrice = x.Deal.SellingPrice,
                                                    DealIconUrl = x.Deal.PosterStorage.Path,
                                                    DeliveryDate = x.Order.DeliveryDate,
                                                    CarrierName = x.Order.Carrier.Name,
                                                    CarrierMobileNumber = x.Order.Carrier.MobileNumber,
                                                    ItemCount = x.ItemCount,
                                                    TotalAmount = x.Order.TotalAmount,
                                                    SubTotalAmount = x.Deal.SellingPrice * x.ItemCount,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    InvoiceNumber = x.DealId.ToString() + "" + x.Id.ToString(),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    List<OShipments.List> Shipments = _HCoreContext.MDDealCode
                                                      .Where(x => x.Deal.DealTypeId == Helpers.DealType.ProductDeal
                                                && x.Deal.Account.CountryId == _Request.UserReference.CountryId)
                                                      .Select(x => new OShipments.List
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          OrderId = x.OrderId,
                                                          ToAddressId = x.ToAddressId,
                                                          ToAddressKey = x.ToAddress.Guid,
                                                          ToName = x.ToAddress.Name,
                                                          ToEmailAddress = x.ToAddress.EmailAddress,
                                                          ToMobileNumber = x.ToAddress.ContactNumber,
                                                          ToAddress = x.ToAddress.AddressLine1 + " " + x.ToAddress.AddressLine2,
                                                          ToAddressCity = x.ToAddress.City.Name,
                                                          ToAddressState = x.ToAddress.State.Name,
                                                          ToAddressCountry = x.ToAddress.Country.Name,
                                                          FromAddressId = x.FromAddressId,
                                                          FromAddressKey = x.FromAddress.Guid,
                                                          FromName = x.FromAddress.Name,
                                                          FromEmailAddress = x.FromAddress.EmailAddress,
                                                          FromMobileNumber = x.FromAddress.ContactNumber,
                                                          FromAddress = x.FromAddress.AddressLine1 + " " + x.FromAddress.AddressLine2,
                                                          FromAddressCity = x.FromAddress.City.Name,
                                                          FromAddressState = x.FromAddress.State.Name,
                                                          FromAddressCountry = x.FromAddress.Country.Name,
                                                          ReturnAddressId = x.FromAddressId,
                                                          ReturnAddressKey = x.FromAddress.Guid,
                                                          ReturnName = x.FromAddress.Name,
                                                          ReturnEmailAddress = x.FromAddress.EmailAddress,
                                                          ReturnMobileNumber = x.FromAddress.ContactNumber,
                                                          ReturnAddress = x.FromAddress.AddressLine1 + " " + x.FromAddress.AddressLine2,
                                                          ReturnAddressCity = x.FromAddress.City.Name,
                                                          ReturnAddressState = x.FromAddress.State.Name,
                                                          ReturnAddressCountry = x.FromAddress.Country.Name,
                                                          MerchantId = x.Deal.AccountId,
                                                          MerchantKey = x.Deal.Account.Guid,
                                                          MerchantDisplayName = x.Deal.Account.DisplayName,
                                                          MerchantMobileNumber = x.Deal.Account.MobileNumber,
                                                          MerchantEmailAddress = x.Account.EmailAddress,
                                                          MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                                          MerchantAddress = x.Deal.Account.Address,
                                                          CustomerId = x.AccountId,
                                                          CustomerKey = x.Account.Guid,
                                                          CustomerDisplayName = x.Account.DisplayName,
                                                          CustomerMobileNumber = x.Account.MobileNumber,
                                                          CustomerEmailAddress = x.Account.EmailAddress,
                                                          CustomerIconUrl = x.Account.IconStorage.Path,
                                                          CustomerAddress = x.Account.Address,
                                                          DealId = x.DealId,
                                                          DealKey = x.Deal.Guid,
                                                          DealTitle = x.Deal.Title,
                                                          DealPrice = x.Deal.SellingPrice,
                                                          DealIconUrl = x.Deal.PosterStorage.Path,
                                                          DeliveryDate = x.Order.DeliveryDate,
                                                          CarrierName = x.Order.Carrier.Name,
                                                          CarrierMobileNumber = x.Order.Carrier.MobileNumber,
                                                          ItemCount = x.ItemCount,
                                                          TotalAmount = x.Order.TotalAmount,
                                                          SubTotalAmount = x.Deal.SellingPrice * x.ItemCount,
                                                          CreateDate = x.CreateDate,
                                                          CreatedBYId = x.CreatedById,
                                                          CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                          ModifyDate = x.ModifyDate,
                                                          ModifyById = x.ModifyById,
                                                          ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name,
                                                          InvoiceNumber = x.DealId.ToString() + "" + x.Id.ToString(),
                                                      }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();

                    foreach (var DataItem in Shipments)
                    {
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.StoreIconUrl))
                        {
                            DataItem.StoreIconUrl = _AppConfig.StorageUrl + DataItem.StoreIconUrl;
                        }
                        else
                        {
                            DataItem.StoreIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.CustomerIconUrl))
                        {
                            DataItem.CustomerIconUrl = _AppConfig.StorageUrl + DataItem.CustomerIconUrl;
                        }
                        else
                        {
                            DataItem.CustomerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.DealIconUrl))
                        {
                            DataItem.DealIconUrl = _AppConfig.StorageUrl + DataItem.DealIconUrl;
                        }
                        else
                        {
                            DataItem.DealIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.Bill))
                        {
                            DataItem.Bill = _AppConfig.StorageUrl + DataItem.Bill;
                        }
                    }

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Shipments, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "LS0200", Resources.Resources.LS0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetOrders", _Exception, _Request.UserReference, "LS0500", "Unable to process your request. Please try after some time.");
            }
        }

        /// <summary>
        /// Description: Creates the shipments core.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OShipments.Save.Response.</returns>
        internal OShipments.Save.Response CreateShipmentsCore(OShipments.Save.Request _Request)
        {
            _ShipmentResponse = new OShipments.Save.Response();
            try
            {
                _FrameworkDelivery = new FrameworkDelivery();
                _CreateShipment = new CreateShipment();


                long? CustomerId = HCoreHelper.GetUserAccountId(_Request.CustomerKey, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    var ParcelId = _HCoreContext.LSParcel.Where(x => x.ParcelId == _Request.ParcelReference).Select(a => a.Id).FirstOrDefault();
                    var CustomerAddress = _HCoreContext.MDDealAddress.Where(x => x.AddressReference == _Request.CustomerAddressReference && x.AccountId == CustomerId).FirstOrDefault();
                    var MerchantAddress = _HCoreContext.MDDealAddress.Where(x => x.AddressReference == _Request.MerchantAddressReference && x.AccountId == _Request.MerchantId).FirstOrDefault();
                    var DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId).Select(a => new
                    {
                        DealId = a.Id,
                        DealPrice = a.SellingPrice
                    }).FirstOrDefault();

                    _CreateShipmentRequest = new CreateShipmentRequest(_Request.MerchantAddressReference, _Request.CustomerAddressReference, _Request.MerchantAddressReference, "", _Request.ParcelReference);
                    _CreateShipment = _FrameworkDelivery.CreateShipment(_CreateShipmentRequest);
                    if (_CreateShipment.status == true)
                    {
                        _LSShipments = new LSShipments();
                        _LSShipments.Guid = HCoreHelper.GenerateGuid();
                        if (_Request.MerchantId > 0)
                        {
                            _LSShipments.MerchantId = _Request.MerchantId;
                        }
                        if (CustomerId > 0)
                        {
                            _LSShipments.CustomerId = CustomerId;
                        }
                        if (CustomerAddress.AddressId > 0)
                        {
                            _LSShipments.ToAddressId = CustomerAddress.AddressId;
                        }
                        _LSShipments.FromAddressId = MerchantAddress.AddressId;
                        _LSShipments.ReturnAddressId = MerchantAddress.AddressId;
                        _LSShipments.ParcelId = ParcelId;
                        _LSShipments.DealId = DealDetails.DealId;
                        _LSShipments.DealPrice = DealDetails.DealPrice;
                        _LSShipments.UserId = _CreateShipment.data.user.user_id;
                        _LSShipments.ShipmentId = _CreateShipment.data.shipment_id;
                        _LSShipments.TId = _CreateShipment.data._id;
                        _LSShipments.StatusId = HelperStatus.OrderStatus.New;
                        _LSShipments.CreateDate = HCoreHelper.GetGMTDateTime();
                        _LSShipments.CreatedById = CustomerId;
                        _HCoreContext.LSShipments.Add(_LSShipments);

                        _HCoreContext.SaveChanges();
                        _ShipmentResponse.ReferenceId = _LSShipments.Id;
                        _ShipmentResponse.ReferenceKey = _LSShipments.Guid;
                        _ShipmentResponse.ShipmentId = _LSShipments.ShipmentId;
                        return _ShipmentResponse;
                    }
                    else
                    {
                        _ShipmentResponse.Status = "01";
                        _ShipmentResponse.Message = "Failed to create shipment";
                        return _ShipmentResponse;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateShipment", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
                _ShipmentResponse.Status = "01";
                _ShipmentResponse.Message = "Failed to create shipment";
                return _ShipmentResponse;
            }
        }

        /// <summary>
        /// Description: Creates the shipment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OShipments.Save.Response.</returns>
        internal OShipments.Save.Response CreateShipment(OShipments.Save.Request _Request)
        {
            _ShipmentResponse = new OShipments.Save.Response();
            try
            {
                _FrameworkDelivery = new FrameworkDelivery();
                _CreateShipment = new CreateShipment();
                using (_HCoreContext = new HCoreContext())
                {

                    _CreateShipmentRequest = new CreateShipmentRequest(_Request.MerchantAddressReference, _Request.CustomerAddressReference, _Request.MerchantAddressReference, "", _Request.ParcelReference);
                    _CreateShipment = _FrameworkDelivery.CreateShipment(_CreateShipmentRequest);
                    if (_CreateShipment.status == true)
                    {
                        _LSShipments = new LSShipments();
                        _LSShipments.Guid = HCoreHelper.GenerateGuid();
                        if (_Request.MerchantId > 0)
                        {
                            _LSShipments.MerchantId = _Request.MerchantId;
                        }
                        if (_Request.AccountId > 0)
                        {
                            _LSShipments.CustomerId = _Request.AccountId;
                            _LSShipments.CreatedById = _Request.AccountId;
                        }
                        if (_Request.ToAddressId > 0)
                        {
                            _LSShipments.ToAddressId = _Request.ToAddressId;
                        }
                        _LSShipments.FromAddressId = _Request.FromAddressId;
                        _LSShipments.ReturnAddressId = _Request.FromAddressId;
                        _LSShipments.ParcelId = _Request.ParcelId;
                        if (_Request.DealId > 0)
                        {
                            _LSShipments.DealId = _Request.DealId;
                        }
                        _LSShipments.DealPrice = _Request.DealPrice;
                        _LSShipments.UserId = _CreateShipment.data.user.user_id;
                        _LSShipments.ShipmentId = _CreateShipment.data.shipment_id;
                        _LSShipments.TId = _CreateShipment.data._id;
                        _LSShipments.StatusId = HelperStatus.OrderStatus.New;
                        _LSShipments.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.LSShipments.Add(_LSShipments);

                        _HCoreContext.SaveChanges();
                        _ShipmentResponse.ReferenceId = _LSShipments.Id;
                        _ShipmentResponse.ReferenceKey = _LSShipments.Guid;
                        _ShipmentResponse.ShipmentId = _CreateShipment.data.shipment_id;
                        return _ShipmentResponse;
                    }
                    else
                    {
                        _ShipmentResponse.Status = "01";
                        _ShipmentResponse.Message = "Failed to create shipment";
                        return _ShipmentResponse;
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateShipment", _Exception, _Request.UserReference, "LS0500", Resources.Resources.LS0500);
                _ShipmentResponse.Status = "01";
                _ShipmentResponse.Message = "Failed to create shipment";
                return _ShipmentResponse;
            }
        }

        internal void GetOrdersDownload(OList.Request _Request)
        {
            try
            {
                _OrdersDownload = new List<OShipments.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.MDDealCode
                                                .Where(x => x.Deal.DealTypeId == Helpers.DealType.ProductDeal
                                                && x.Deal.Account.CountryId == _Request.UserReference.CountryId
                                                && (x.Deal.DeliveryTypeId == Helpers.DeliveryType.Delivery
                                                || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery
                                                || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStorePickUp))
                                                .Select(x => new OShipments.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ToAddressId = x.ToAddressId,
                                                    ToAddressKey = x.ToAddress.Guid,
                                                    ToName = x.ToAddress.Name,
                                                    ToEmailAddress = x.ToAddress.EmailAddress,
                                                    ToMobileNumber = x.ToAddress.ContactNumber,
                                                    FromAddressId = x.FromAddressId,
                                                    FromAddressKey = x.FromAddress.Guid,
                                                    FromName = x.FromAddress.Name,
                                                    FromEmailAddress = x.FromAddress.EmailAddress,
                                                    FromMobileNumber = x.FromAddress.ContactNumber,
                                                    ReturnAddressId = x.FromAddressId,
                                                    ReturnAddressKey = x.FromAddress.Guid,
                                                    ReturnName = x.FromAddress.Name,
                                                    ReturnEmailAddress = x.FromAddress.EmailAddress,
                                                    ReturnMobileNumber = x.FromAddress.ContactNumber,
                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,
                                                    MerchantMobileNumber = x.Deal.Account.MobileNumber,
                                                    MerchantEmailAddress = x.Account.EmailAddress,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                                    MerchantAddress = x.Deal.Account.Address,
                                                    CustomerId = x.AccountId,
                                                    CustomerKey = x.Account.Guid,
                                                    CustomerDisplayName = x.Account.DisplayName,
                                                    CustomerMobileNumber = x.Account.MobileNumber,
                                                    CustomerEmailAddress = x.Account.EmailAddress,
                                                    CustomerIconUrl = x.Account.IconStorage.Path,
                                                    CustomerAddress = x.Account.Address,
                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,
                                                    DealTitle = x.Deal.Title,
                                                    DealPrice = x.Deal.SellingPrice,
                                                    DealIconUrl = x.Deal.PosterStorage.Path,
                                                    DeliveryDate = x.Order.DeliveryDate,
                                                    CarrierName = x.Order.Carrier.Name,
                                                    CarrierMobileNumber = x.Order.Carrier.MobileNumber,
                                                    ItemCount = 1,
                                                    TotalAmount = x.Order.TotalAmount,
                                                    SubTotalAmount = x.Deal.SellingPrice,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    InvoiceNumber = x.DealId.ToString() + "" + x.Id.ToString(),
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        var Shipments = _HCoreContext.MDDealCode
                                                      .Where(x => x.Deal.DealTypeId == Helpers.DealType.ProductDeal
                                                && x.Deal.Account.CountryId == _Request.UserReference.CountryId
                                                      && (x.Deal.DeliveryTypeId == Helpers.DeliveryType.Delivery
                                                      || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery
                                                      || x.Deal.DeliveryTypeId == Helpers.DeliveryType.InStorePickUp))
                                                      .Select(x => new OShipments.List
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          ToAddressId = x.ToAddressId,
                                                          ToAddressKey = x.ToAddress.Guid,
                                                          ToName = x.ToAddress.Name,
                                                          ToEmailAddress = x.ToAddress.EmailAddress,
                                                          ToMobileNumber = x.ToAddress.ContactNumber,
                                                          FromAddressId = x.FromAddressId,
                                                          FromAddressKey = x.FromAddress.Guid,
                                                          FromName = x.FromAddress.Name,
                                                          FromEmailAddress = x.FromAddress.EmailAddress,
                                                          FromMobileNumber = x.FromAddress.ContactNumber,
                                                          ReturnAddressId = x.FromAddressId,
                                                          ReturnAddressKey = x.FromAddress.Guid,
                                                          ReturnName = x.FromAddress.Name,
                                                          ReturnEmailAddress = x.FromAddress.EmailAddress,
                                                          ReturnMobileNumber = x.FromAddress.ContactNumber,
                                                          MerchantId = x.Deal.AccountId,
                                                          MerchantKey = x.Deal.Account.Guid,
                                                          MerchantDisplayName = x.Deal.Account.DisplayName,
                                                          MerchantMobileNumber = x.Deal.Account.MobileNumber,
                                                          MerchantEmailAddress = x.Account.EmailAddress,
                                                          MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                                          MerchantAddress = x.Deal.Account.Address,
                                                          CustomerId = x.AccountId,
                                                          CustomerKey = x.Account.Guid,
                                                          CustomerDisplayName = x.Account.DisplayName,
                                                          CustomerMobileNumber = x.Account.MobileNumber,
                                                          CustomerEmailAddress = x.Account.EmailAddress,
                                                          CustomerIconUrl = x.Account.IconStorage.Path,
                                                          CustomerAddress = x.Account.Address,
                                                          DealId = x.DealId,
                                                          DealKey = x.Deal.Guid,
                                                          DealTitle = x.Deal.Title,
                                                          DealPrice = x.Deal.SellingPrice,
                                                          DealIconUrl = x.Deal.PosterStorage.Path,
                                                          DeliveryDate = x.Order.DeliveryDate,
                                                          CarrierName = x.Order.Carrier.Name,
                                                          CarrierMobileNumber = x.Order.Carrier.MobileNumber,
                                                          ItemCount = 1,
                                                          TotalAmount = x.Order.TotalAmount,
                                                          SubTotalAmount = x.Deal.SellingPrice,
                                                          CreateDate = x.CreateDate,
                                                          CreatedBYId = x.CreatedById,
                                                          CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                          ModifyDate = x.ModifyDate,
                                                          ModifyById = x.ModifyById,
                                                          ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name,
                                                          InvoiceNumber = x.DealId.ToString() + "" + x.Id.ToString(),
                                                      }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();

                        foreach (var _DataItem in Shipments)
                        {
                            _OrdersDownload.Add(new OShipments.ListDownload
                            {
                                OrderId = _DataItem.ReferenceId,
                                CarrierName = _DataItem.CarrierName,
                                CarrierMobileNumber = _DataItem.CarrierMobileNumber,
                                CustomerName = _DataItem.CustomerDisplayName,
                                CustomerMobileNumber = _DataItem.CustomerMobileNumber,
                                MerchantName = _DataItem.MerchantDisplayName,
                                MerchantMobileNumber = _DataItem.MerchantMobileNumber,
                                DeliveryCharge = _DataItem.RateAmount,
                                DeliveryDate = _DataItem.DeliveryDate,
                                TotalAmount = _DataItem.TotalAmount,
                                PlacedDate = _DataItem.CreateDate,
                                ProductPrice = _DataItem.DealPrice,
                                OrderStatus = _DataItem.StatusName
                            });
                        }
                        _Request.Offset += 800;
                    }
                }
                _HCoreContext.Dispose();
                HCoreHelper.CreateDownload("OrderHistory", _OrdersDownload, _Request.UserReference);
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetOrdersDownload", _Exception, _Request.UserReference, "LS0500", "Unable to process your request. Please try after some time.");
            }
        }
    }

    internal class ActorShipmentsDownload : ReceiveActor
    {
        public ActorShipmentsDownload()
        {
            FrameworkShipment _FrameworkShipment;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkShipment = new FrameworkShipment();
                _FrameworkShipment.GetShipmentDownload(_Request);
            });
        }
    }

    internal class ActorOrdersDownload : ReceiveActor
    {
        public ActorOrdersDownload()
        {
            FrameworkShipment _FrameworkShipment;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkShipment = new FrameworkShipment();
                _FrameworkShipment.GetOrdersDownload(_Request);
            });
        }
    }
}

