//==================================================================================
// FileName: FrameworkCarriers.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to carriers
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using Delivery.Object.Requests.Carriers;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.TUC.Plugins.Delivery.Resources;
using static HCore.Helper.HCoreConstant;
using Delivery.Framework;
using Delivery.Object.Response.Carriers;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Collections.Generic;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkCarriers
    {
        HCoreContext _HCoreContext;
        GetCarriersRequest _GetCarriersRequest;
        FrameworkDelivery _FrameworkDelivery;
        LSCarriers _LSCarriers;

        /// <summary>
        /// Description: Saves the carriers.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCarriers(OCarriers.Save.Request _Request)
        {
            try
            {
                _FrameworkDelivery = new FrameworkDelivery();
                if (_Request.IsActive == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LS0001);
                }
                if (string.IsNullOrEmpty(_Request.Type))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0002", Resources.Resources.LS0002);
                }
                if (_Request.PerPage == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0003", Resources.Resources.LS0003);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _GetCarriersRequest = new GetCarriersRequest();
                    if(_Request.IsActive == 1)
                    {
                        _GetCarriersRequest.active = true;
                    }
                    else
                    {
                        _GetCarriersRequest.active = false;
                    }
                    _GetCarriersRequest.type = _Request.Type;
                    _GetCarriersRequest.perPage = (long)_Request.PerPage;
                    GetCarriers Carriers = _FrameworkDelivery.GetCarriers(_GetCarriersRequest);
                    if(Carriers.status == true)
                    {
                        foreach(var Carrier in Carriers.data.carriers)
                        {
                            bool Exists = _HCoreContext.LSCarriers.Any(x => x.EmailAddress == Carrier.contact.email);
                            if(Exists)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0003", "Carrier already exists");
                            }
                            _LSCarriers = new LSCarriers();
                            _LSCarriers.Guid = HCoreHelper.GenerateGuid();
                            _LSCarriers.Name = Carrier.name;
                            _LSCarriers.MobileNumber = Carrier.contact.phone;
                            _LSCarriers.EmailAddress = Carrier.contact.email;
                            if(Carrier.active == true)
                            {
                                _LSCarriers.Active = 1;
                            }
                            else
                            {
                                _LSCarriers.Active = 0;
                            }
                            if (Carrier.domestic == true)
                            {
                                _LSCarriers.Domestic = 1;
                            }
                            else
                            {
                                _LSCarriers.Domestic = 0;
                            }
                            if (Carrier.international == true)
                            {
                                _LSCarriers.International = 1;
                            }
                            else
                            {
                                _LSCarriers.International = 0;
                            }
                            _LSCarriers.IconUrl = Carrier.logo;
                            if (Carrier.regional == true)
                            {
                                _LSCarriers.Regional = 1;
                            }
                            else
                            {
                                _LSCarriers.Regional = 0;
                            }
                            if (Carrier.requires_invoice == true)
                            {
                                _LSCarriers.RequiresInvoice = 1;
                            }
                            else
                            {
                                _LSCarriers.RequiresInvoice = 0;
                            }
                            if (Carrier.requires_waybill == true)
                            {
                                _LSCarriers.RequiresWaybill = 1;
                            }
                            else
                            {
                                _LSCarriers.RequiresWaybill = 0;
                            }
                            _LSCarriers.Slug = Carrier.slug;
                            _LSCarriers.CarrierId = Carrier.carrier_id;
                            _LSCarriers.StatusId = HelperStatus.Default.Active;
                            _LSCarriers.CreateDate = HCoreHelper.GetGMTDateTime();
                            _LSCarriers.CreatedById = _Request.UserReference.AccountId;
                            _HCoreContext.LSCarriers.Add(_LSCarriers);
                        }
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "LSSAVE", Resources.Resources.LSSAVE);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0404", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveCarriers", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the carrirer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCarrirer(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "LS0001", Resources.Resources.LSREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.LSCarriers.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                  .Select(x => new OCarriers.Details
                                  {
                                      ReferenceId = x.Id,
                                      ReferenceKey = x.Guid,
                                      Name = x.Name,
                                      EmailAddress = x.EmailAddress,
                                      MobileNummber = x.MobileNumber,
                                      IsActive = x.Active,
                                      IsDomestic = x.Domestic,
                                      IsInternational = x.International,
                                      IsRegional = x.Regional,
                                      IsRequiresInvoice = x.RequiresInvoice,
                                      IsRequiresWaybill = x.RequiresWaybill,
                                      IconUrl = x.IconUrl,
                                      Slug = x.Slug,
                                      CarrierId = x.CarrierId,
                                      CreateDate = x.CreateDate,
                                      CreatedBYId = x.CreatedById,
                                      CreatedByDisplayName = x.CreatedBy.DisplayName,
                                      ModifyDate = x.ModifyDate,
                                      ModifyById = x.ModifyById,
                                      ModifyByDisplayName = x.ModifyBy.DisplayName,
                                      StatusId = x.StatusId,
                                      StatusCode = x.Status.SystemName,
                                      StatusName = x.Status.Name
                                  })
                                  .FirstOrDefault();
                    if(Details != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "", Resources.Resources.LS0404);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "", Resources.Resources.LS0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCarrirer", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the carrier list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCarriers(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.LSCarriers
                                                .Select(x => new OCarriers.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNummber = x.MobileNumber,
                                                    IsActive = x.Active,
                                                    IsDomestic = x.Domestic,
                                                    IsInternational = x.International,
                                                    IsRegional = x.Regional,
                                                    IsRequiresInvoice = x.RequiresInvoice,
                                                    IsRequiresWaybill = x.RequiresWaybill,
                                                    IconUrl = x.IconUrl,
                                                    Slug = x.Slug,
                                                    CarrierId = x.CarrierId,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    List<OCarriers.List> Carriers = _HCoreContext.LSCarriers
                                                .Select(x => new OCarriers.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    EmailAddress = x.EmailAddress,
                                                    MobileNummber = x.MobileNumber,
                                                    IsActive = x.Active,
                                                    IsDomestic = x.Domestic,
                                                    IsInternational = x.International,
                                                    IsRegional = x.Regional,
                                                    IsRequiresInvoice = x.RequiresInvoice,
                                                    IsRequiresWaybill = x.RequiresWaybill,
                                                    IconUrl = x.IconUrl,
                                                    Slug = x.Slug,
                                                    CarrierId = x.CarrierId,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                }).Where(_Request.SearchCondition)
                                                .OrderBy(_Request.SortExpression)
                                                .Skip(_Request.Offset)
                                                .Take(_Request.Limit)
                                                 .ToList();
                    foreach (var Carrier in Carriers)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Carriers, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "LS0200", Resources.Resources.LS0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCarriers", _Exception, _Request.UserReference);
            }
        }
    }
}

