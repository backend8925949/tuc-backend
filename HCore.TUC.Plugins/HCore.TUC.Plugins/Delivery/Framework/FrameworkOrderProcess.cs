﻿using System;
using HCore.Helper;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.TUC.Plugins.Delivery.Objects;
using HCore.Data;
using HCore.Data.Models;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Delivery.Operations;
using Delivery.Framework;
using Newtonsoft.Json;
using HCore.Integration.DellymanIntegrations.DellymanObject.Response;
using Microsoft.EntityFrameworkCore;
using HCore.Integration.DeliveryIntegration.GoShiip;
using HCore.Integration.DeliveryIntegration.GoShiip.Requests;
using HCore.Integration.DeliveryIntegration.GoShiip.Response;
using Dellyman.DellymanObject.Response;
using Dellyman.DellymanObject.Request;
using Dellyman;
using static HCore.TUC.Plugins.Delivery.Objects.OOrderProcess.TrackOrder;
using Dellyman.DellymanObject;

namespace HCore.TUC.Plugins.Delivery.Framework
{
    public class FrameworkOrderProcess
    {
        HCoreContext? _HCoreContext;
        FrameworkOperations? _FrameworkOperations;
        OOperations.Pricing.GetDeliveryPricing? _GetDeliveryPricing;
        GoShiip? _Shiip;
        OOrderProcess.TrackOrder.Response? _TrackOrderResponse;
        OOrderProcess.TrackOrder.CustomerDetails? _CustomerDetails;
        OOrderProcess.TrackOrder.MerchantDetails? _MerchantDetails;
        OOrderProcess.TrackOrder.RiderDetails? _RiderDetails;
        OOrderProcess.TrackOrder.OrderDetails? _OrderDetails;
        DellymanOrders? _DellymanOrders;
        TrackOrderRequest? _TrackOrderRequest;

        internal async Task<OResponse> ConfirmOrders(OOrderProcess.ConfirmOrder _Request)
        {
            try
            {
                if(_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COREF", "Order reference id required.");
                }
                if(string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COREFKEY", "Order reference key required.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var OrderDetails = _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if(OrderDetails != null)
                    {
                        string? TimeZoneId = await _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.UserReference.CountryId && x.Guid == _Request.UserReference.CountryKey).Select(x => x.TimeZoneName).FirstOrDefaultAsync();
                        string? DeliveryPartnerKey = await _HCoreContext.HCUAccount.Where(x => x.Id == OrderDetails.DeliveryPartnerId).Select(x => x.Guid).FirstOrDefaultAsync();
                        _FrameworkOperations = new FrameworkOperations();
                        _GetDeliveryPricing = new OOperations.Pricing.GetDeliveryPricing();
                        _GetDeliveryPricing.AccountId = (long)OrderDetails.CustomerId;
                        _GetDeliveryPricing.AddressId = (long)OrderDetails.ToAddressId;
                        _GetDeliveryPricing.DealId = OrderDetails.DealId;
                        _GetDeliveryPricing.MerchantAddressId = (long)OrderDetails.FromAddressId;
                        _GetDeliveryPricing.CarrierId = (int?)OrderDetails.CarrierId;
                        _GetDeliveryPricing.DeliveryCharge = OrderDetails.RateAmount;
                        _GetDeliveryPricing.ReferenceId = OrderDetails.Id;
                        _GetDeliveryPricing.ReferenceKey = OrderDetails.Guid;
                        _GetDeliveryPricing.TimeZoneId = TimeZoneId;
                        _GetDeliveryPricing.DeliveryPartnerKey = DeliveryPartnerKey;
                        if (!string.IsNullOrEmpty(OrderDetails.RateId))
                        {
                            _GetDeliveryPricing.RateId = OrderDetails.RateId;
                        }
                        _GetDeliveryPricing.RedisKey = OrderDetails.TId;
                        _GetDeliveryPricing.UserReference = _Request.UserReference;
                        OOperations.Pricing.PricingResponse? ConfirmOrder = await _FrameworkOperations.ConfirmOrder(_GetDeliveryPricing);
                        if (ConfirmOrder.Status == "Success")
                        {
                            var CustomerDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == OrderDetails.CustomerId)
                                                  .Select(x => new
                                                  {
                                                      DisplayName = x.DisplayName,
                                                      EmailAddress = x.EmailAddress,
                                                  }).FirstOrDefaultAsync();

                            var DealDetails = await _HCoreContext.MDDeal.Where(x => x.Id == OrderDetails.DealId)
                                               .Select(x => new
                                               {
                                                   Title = x.TitleContent,
                                                   MerchantDisplayName = x.Account.DisplayName,
                                               }).FirstOrDefaultAsync();

                            OrderDetails.DeliveryDate = HCoreHelper.GetGMTDateTime().AddDays(1);
                            OrderDetails.TrackingNumber = ConfirmOrder.OrderReference;
                            OrderDetails.OrderReference = ConfirmOrder.OrderId.ToString();
                            OrderDetails.StatusId = HelperStatus.OrderStatus.Preparing;
                            OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            OrderDetails.ModifyById = _Request.UserReference.AccountId;
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            var Response = new
                            {
                                ReferenceId = OrderDetails.Id,
                                ReferenceKey = OrderDetails.Guid
                            };

                            DateTime DeliveryDate = (DateTime)OrderDetails.DeliveryDate;
                            if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                            {
                                FrameworkEmailProcessor? _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                                OEmailProcessor.ConfirmOrder? _EmailParameters = new OEmailProcessor.ConfirmOrder();
                                _EmailParameters.UserDisplayName = CustomerDetails.DisplayName;
                                _EmailParameters.OrderId = OrderDetails.OrderReference;
                                _EmailParameters.OrderReference = ConfirmOrder.OrderReference;
                                _EmailParameters.DeliveryDate = DeliveryDate.ToString("dd MMMM, yyyy");
                                _EmailParameters.EmailAddress = CustomerDetails.EmailAddress;
                                _EmailParameters.CustomerId = OrderDetails.CustomerId;
                                _EmailParameters.DealTitle = DealDetails.Title;
                                _EmailParameters.MerchantDisplayName = DealDetails.MerchantDisplayName;
                                _EmailParameters.ReferenceId = Response.ReferenceId;
                                _EmailParameters.ReferenceKey = Response.ReferenceKey;
                                _EmailParameters.UserReference = _Request.UserReference;
                                var _Actor = ActorSystem.Create("ActorConfirmOrderNotification");
                                var _ActorNotify = _Actor.ActorOf<ManageEmailProcessor.ActorConfirmOrderNotification>("ActorConfirmOrderNotification");
                                _ActorNotify.Tell(_EmailParameters);
                            }

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "CO0001", "The order is confirmed successfully.");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CO0002", ConfirmOrder.Message);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CO0404", "We are not able to find the order details. It might be removed or not available at the moment.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ConfirmOrders", _Exception, _Request.UserReference, "CO0500", "We are facing some issues while confirming the order. Please try after some time.");
            }
        }

        internal async Task<OResponse> CancelOrder(OOrderProcess.CancelOrder _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COREF", "Order reference id required.");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COREFKEY", "Order reference key required.");
                }
                if(string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COSTATUS", "Status code required");
                }

                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if(StatusId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COINSTATUS", "Invalid status.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var OrderDetails = await _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if(OrderDetails != null)
                    {
                        string? DeliveryPartner = await _HCoreContext.LSShipments.Where(x => x.Id == OrderDetails.Id).Select(x => x.DeliveryPartner.Guid).FirstOrDefaultAsync();
                        if(DeliveryPartner == HCoreConstant.DeliveryPartners.GoShiip && !string.IsNullOrEmpty(OrderDetails.OrderReference))
                        {
                            #region GoShiip
                            _Shiip = new GoShiip();
                            ShipLogInRequest? _LogInRequest = new ShipLogInRequest();
                            _LogInRequest.email_phone = _AppConfig.ShiipUserId;
                            _LogInRequest.password = _AppConfig.ShiipUserKey;

                            ShipLogInResponse? _LogInResponse = await _Shiip.ShipLogIn(_LogInRequest);
                            ShipCancelShipmentResponse? _CancelShipmentResponse = new ShipCancelShipmentResponse();
                            if (_LogInResponse.status == true)
                            {
                                _CancelShipmentResponse = await _Shiip.ShipCancelShipment(OrderDetails.OrderReference, _LogInResponse.data.token);

                                if(_CancelShipmentResponse.data.status == true)
                                {
                                    OrderDetails.StatusId = StatusId;
                                    OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    OrderDetails.ModifyById = _Request.UserReference.AccountId;
                                    await _HCoreContext.SaveChangesAsync();
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CO0500", "We are facing some issues while canceling the order. Please try after some time.");
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            OrderDetails.StatusId = StatusId;
                            OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            OrderDetails.ModifyById = _Request.UserReference.AccountId;
                            await _HCoreContext.SaveChangesAsync();
                        }

                        var DealDetails = await _HCoreContext.MDDeal.Where(x => x.Id == OrderDetails.DealId)
                                           .Select(x => new
                                           {
                                               Title = x.Title,
                                               MerchantDisplayName = x.Account.DisplayName,
                                           }).FirstOrDefaultAsync();

                        var Response = new
                        {
                            ReferenceId = OrderDetails.Id,
                            ReferenceKey = OrderDetails.Guid
                        };

                        FrameworkEmailProcessor? _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                        OEmailProcessor.CancelOrder? _EmailParameters = new OEmailProcessor.CancelOrder();
                        _EmailParameters.CustomerId = OrderDetails.CustomerId;
                        _EmailParameters.ReferenceId = Response.ReferenceId;
                        _EmailParameters.ReferenceKey = Response.ReferenceKey;
                        _EmailParameters.DealTitle = DealDetails.Title;
                        _EmailParameters.MerchantDisplayName = DealDetails.MerchantDisplayName;
                        _EmailParameters.UserReference = _Request.UserReference;
                        var _Actor = ActorSystem.Create("ActorCancelOrderNotification");
                        var _ActorNotify = _Actor.ActorOf<ManageEmailProcessor.ActorCancelOrderNotification>("ActorCancelOrderNotification");
                        _ActorNotify.Tell(_EmailParameters);

                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "CO0003", "The order is cancelled successfully.");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CO0404", "We are not able to find the order details. It might be removed or not available at the moment.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CancelOrder", _Exception, _Request.UserReference, "CO0500", "We are facing some issues while canceling the order. Please try after some time.");
            }
        }

        internal async Task<OResponse> UpdateOrderStatus(OOrderProcess.ReadyToPickup _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COREF", "Order reference id required.");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COREFKEY", "Order reference key required.");
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COSTATUS", "Status code required");
                }

                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "COINSTATUS", "Invalid status.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var OrderDetails = await _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (OrderDetails != null)
                    {
                        var DeliveryDetails = await _HCoreContext.LSShipments.Where(x => x.Id == OrderDetails.Id)
                            .Select(x => new
                            {
                                CarrierName = x.Carrier.Name,
                                PartnerKey = x.DeliveryPartner.Guid,
                                MerchantMobileNumber = x.Merchant.MobileNumber,
                                CustomerMobileNumber = x.Customer.MobileNumber,
                            }).FirstOrDefaultAsync();
                        if (DeliveryDetails.PartnerKey == HCoreConstant.DeliveryPartners.GoShiip)
                        {
                            #region GoShiip
                            _Shiip = new GoShiip();
                            ShipLogInRequest? _LogInRequest = new ShipLogInRequest();
                            _LogInRequest.email_phone = _AppConfig.ShiipUserId;
                            _LogInRequest.password = _AppConfig.ShiipUserKey;

                            ShipLogInResponse? _LogInResponse = await _Shiip.ShipLogIn(_LogInRequest);
                            ShipAssignShipmentResponse? _AssignShipmentResponse = new ShipAssignShipmentResponse();
                            if (_LogInResponse.status == true)
                            {
                                ShipAssignShipmentRequest? _AssignShipmentRequest = new ShipAssignShipmentRequest();
                                _AssignShipmentRequest.type = DeliveryDetails.CarrierName;
                                _AssignShipmentRequest.rate_id = OrderDetails.RateId;
                                _AssignShipmentRequest.redis_key = OrderDetails.TId;
                                _AssignShipmentRequest.user_id = _AppConfig.ShiipUserAccountId;
                                _AssignShipmentRequest.shipment_id = OrderDetails.OrderReference;
                                _AssignShipmentRequest.toPhoneNumber = DeliveryDetails.CustomerMobileNumber;
                                _AssignShipmentRequest.fromPhoneNumber = DeliveryDetails.MerchantMobileNumber;

                                _AssignShipmentResponse = await _Shiip.ShipAssignShipment(_AssignShipmentRequest, _LogInResponse.data.token);

                                if (_AssignShipmentResponse.data != null)
                                {
                                    OrderDetails.StatusId = StatusId;
                                    OrderDetails.TrackingUrl = _AssignShipmentResponse.data.reference;
                                    OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    OrderDetails.ModifyById = _Request.UserReference.AccountId;
                                    await _HCoreContext.SaveChangesAsync();
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CO0500", _AssignShipmentResponse.message);
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            OrderDetails.StatusId = StatusId;
                            OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            OrderDetails.ModifyById = _Request.UserReference.AccountId;
                            await _HCoreContext.SaveChangesAsync();
                        }

                        await _HCoreContext.DisposeAsync();

                        var Response = new
                        {
                            ReferenceId = OrderDetails.Id,
                            ReferenceKey = OrderDetails.Guid
                        };

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "CO0003", "The order status updated successfully.");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CO0404", "We are not able to find the order details. It might be removed or not available at the moment.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateOrderStatus", _Exception, _Request.UserReference, "CO0500", "We are facing some issues while updating status of the order. Please try after some time.");
            }
        }

        public void UpdateOrderStatus(string ResponseContent, string DeliveryPartnerKey)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if(DeliveryPartnerKey ==  HCoreConstant.DeliveryPartners.Dellyman)
                    {
                        WebhookResponse? _RequestBodyContent = JsonConvert.DeserializeObject<WebhookResponse>(ResponseContent);
                        if (_RequestBodyContent != null)
                        {
                            if (_RequestBodyContent.Order != null || (!string.IsNullOrEmpty(_RequestBodyContent.Order.OrderCode)))
                            {
                                var OrderDetails = _HCoreContext.LSShipments.Where(x => x.TrackingNumber == _RequestBodyContent.Order.OrderCode).FirstOrDefault();
                                if (OrderDetails != null)
                                {
                                    if (_RequestBodyContent.Order.OrderStatus == "PENDING")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.ReadyToPickup;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "ASSIGNED")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.PickUped;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "INTRANSIT")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.DepartedToFacility;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "COMPLETED")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.Delivered;
                                        OrderDetails.DeliveryDate = DateTime.Parse(_RequestBodyContent.Order.DeliveredAt);
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "RETURNED")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.DeliveryFailed;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "CANCELLED")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.CancelledByUser;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "ONHOLD")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.Ready;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "CANCEL-REQUEST")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.CancelledByUser;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    if (_RequestBodyContent.Order.OrderStatus == "INVALID")
                                    {
                                        OrderDetails.StatusId = HelperStatus.OrderStatus.New;
                                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OrderDetails.ModifyById = OrderDetails.CustomerId;
                                    }
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                }
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                    }
                    if(DeliveryPartnerKey == HCoreConstant.DeliveryPartners.GoShiip)
                    {
                        ShipWebhookResponse? _RequestBodyContent = JsonConvert.DeserializeObject<ShipWebhookResponse>(ResponseContent);
                        if (_RequestBodyContent != null)
                        {
                            if (_RequestBodyContent.shipment != null)
                            {
                                if(_RequestBodyContent.shipment.data != null && !string.IsNullOrEmpty(_RequestBodyContent.shipment.data.status))
                                {
                                    var OrderDetails = _HCoreContext.LSShipments.Where(x => x.TrackingUrl == _RequestBodyContent.shipment.data.reference).FirstOrDefault();
                                    if (OrderDetails != null)
                                    {
                                        if (_RequestBodyContent.shipment.data.status == "Pending")
                                        {
                                            OrderDetails.StatusId = HelperStatus.OrderStatus.ReadyToPickup;
                                            OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            OrderDetails.ModifyById = OrderDetails.CustomerId;
                                        }
                                        if (_RequestBodyContent.shipment.data.status == "In progress")
                                        {
                                            OrderDetails.StatusId = HelperStatus.OrderStatus.OutForDelivery;
                                            OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            OrderDetails.ModifyById = OrderDetails.CustomerId;
                                        }
                                        if (_RequestBodyContent.shipment.data.status == "Successful")
                                        {
                                            OrderDetails.StatusId = HelperStatus.OrderStatus.Confirmed;
                                            OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            OrderDetails.ModifyById = OrderDetails.CustomerId;
                                        }
                                        _HCoreContext.SaveChanges();
                                        _HCoreContext.Dispose();
                                    }
                                }
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateOrderStatus", _Exception);
            }
        }

        internal async Task<OResponse> GetDelvieryInvoice(OOrderProcess.Invoice.Request _Request)
        {
            try
            {
                if(_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCREF", "Reference id required");
                }
                if(string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCREFKEY", "Reference key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var OrderDetails = await _HCoreContext.LSShipments.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if(OrderDetails != null)
                    {
                        OOrderProcess.Invoice.Details? InvoiceDetails = new OOrderProcess.Invoice.Details();
                        OOrderProcess.Invoice.Customer? CustomerDetails = new OOrderProcess.Invoice.Customer();
                        OOrderProcess.Invoice.Merchant? MerchantDetails = new OOrderProcess.Invoice.Merchant();
                        List<OOrderProcess.Invoice.Products>? Products = new List<OOrderProcess.Invoice.Products>();
                        OOrderProcess.Invoice.AmountSummary? PaymentDetails = new OOrderProcess.Invoice.AmountSummary();

                        InvoiceDetails.CreateDate = OrderDetails.CreateDate;
                        InvoiceDetails.InvoiceNumber = "#" + OrderDetails.DealId.ToString() + "" + OrderDetails.Id.ToString();

                        CustomerDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == OrderDetails.CustomerId)
                                          .Select(x => new OOrderProcess.Invoice.Customer
                                          {
                                              Name = x.DisplayName,
                                              EmailAddress = x.EmailAddress,
                                              Address = x.HCCoreAddressAccount.Where(y => y.Id == OrderDetails.ToAddressId).Select(z => z.AddressLine1).FirstOrDefault(),
                                          }).FirstOrDefaultAsync();
                        InvoiceDetails.CustomerDetails = CustomerDetails;

                        MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == OrderDetails.MerchantId)
                                          .Select(x => new OOrderProcess.Invoice.Merchant
                                          {
                                              Name = x.DisplayName,
                                              EmailAddress = x.EmailAddress,
                                              Address = x.HCCoreAddressAccount.Where(y => y.Id == OrderDetails.FromAddressId).Select(z => z.AddressLine1).FirstOrDefault(),
                                          }).FirstOrDefaultAsync();
                        InvoiceDetails.MerchantDetails = MerchantDetails;

                        Products = await _HCoreContext.MDDealCode.Where(x => x.DealId == OrderDetails.DealId && x.OrderId == OrderDetails.Id)
                                   .Select(x => new OOrderProcess.Invoice.Products
                                   {
                                       ProductTitle = x.Deal.Title,
                                       ItemAmount = x.ItemAmount,
                                       Quantity = x.ItemCount,
                                       TotalItemAmount = x.ItemAmount * x.ItemCount,
                                   }).ToListAsync();
                        InvoiceDetails.Products = Products;

                        PaymentDetails = await _HCoreContext.MDDealCode.Where(x => x.DealId == OrderDetails.DealId)
                                         .Select(x => new OOrderProcess.Invoice.AmountSummary
                                         {
                                             DeliveryCharges = x.DeliveryCharge,
                                             ComissionAmount = x.ComissionAmount,
                                             DiscountAmount = 0,
                                         }).FirstOrDefaultAsync();
                        PaymentDetails.SubTotalAmount = Products.Sum(x => x.TotalItemAmount);
                        PaymentDetails.TotalAmount = PaymentDetails.DeliveryCharges + PaymentDetails.ComissionAmount + PaymentDetails.DiscountAmount + PaymentDetails.SubTotalAmount;
                        InvoiceDetails.PaymentDetails = PaymentDetails;
                        InvoiceDetails.EmailAddress = _Request.EmailAddress;
                        DateTime IssuedDate = (DateTime)InvoiceDetails.CreateDate;
                        InvoiceDetails.IssuedDate = IssuedDate.ToString("yyyy MMM dd");

                        if (_Request.IsEmail == true)
                        {
                            var Actor = ActorSystem.Create("ActorDeliveryInvoice");
                            var ActorNotify = Actor.ActorOf<ActorDeliveryInvoice>("ActorDeliveryInvoice");
                            ActorNotify.Tell(InvoiceDetails);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, InvoiceDetails, "HC0200", "Invoice has been sent to the provided email successfully.");
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, InvoiceDetails, "HC0200", "Invoice details loaded successfully.");
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0404", "Order details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDelvieryInvoice", _Exception, _Request.UserReference, "HC0500", _Exception.Message);
            }
        }

        internal async Task<OResponse> TrackOrder(OOrderProcess.TrackOrder.Request _Request)
        {
            try
            {
                if(string.IsNullOrEmpty(_Request.TrackingNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC001", "Please enter the deal code or order number to track your product");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _TrackOrderResponse = new OOrderProcess.TrackOrder.Response();
                    _CustomerDetails = new OOrderProcess.TrackOrder.CustomerDetails();
                    _MerchantDetails = new OOrderProcess.TrackOrder.MerchantDetails();
                    _RiderDetails = new OOrderProcess.TrackOrder.RiderDetails();
                    _OrderDetails = new OOrderProcess.TrackOrder.OrderDetails();
                    _DellymanOrders = new DellymanOrders();
                    _TrackOrderRequest = new TrackOrderRequest();
                    _Shiip = new GoShiip();

                    var DealCodeDetails = await _HCoreContext.MDDealCode.Where(x => x.ItemCode == _Request.TrackingNumber)
                                          .Select(x => new
                                          {
                                              DealId = x.DealId,
                                              OrderId = x.OrderId,
                                              CustomerDisplayName = x.Account.DisplayName,
                                              CustomerMobileNumber = x.Account.MobileNumber,
                                              CustomerAddress = x.Account.Address,
                                              CustomerLatitude = x.Account.Latitude,
                                              CustomerLongitude = x.Account.Longitude,
                                          }).FirstOrDefaultAsync();
                    if (DealCodeDetails != null)
                    {
                        var DealDetails = await _HCoreContext.MDDeal.Where(x => x.Id == DealCodeDetails.DealId)
                                          .Select(x => new
                                          {
                                              MerchantDisplayName = x.Account.DisplayName,
                                              MerchantMobileNumber = x.Account.MobileNumber,
                                              MerchantAddress = x.Account.Address,
                                              MerchantLatitude = x.Account.Latitude,
                                              MerchantLongitude = x.Account.Longitude,
                                          }).FirstOrDefaultAsync();

                        if(DealCodeDetails.OrderId != null && DealCodeDetails.OrderId > 0)
                        {
                            var OrderDetails = await _HCoreContext.LSShipments.Where(x => x.Id == DealCodeDetails.OrderId)
                                               .Select(x => new
                                               {
                                                   OrderId = x.ShipmentId,
                                                   OrderReference = x.TrackingNumber,
                                                   OrderNumber = x.TrackingUrl,
                                                   DeliveryPartner = x.DeliveryPartner.Guid,
                                                   MerchantDisplayName = x.Merchant.DisplayName,
                                                   MerchantMobileNumber = x.Merchant.MobileNumber,
                                                   MerchantAddress = x.Merchant.Address,
                                                   MerchantLatitude = x.Merchant.Latitude,
                                                   MerchantLongitude = x.Merchant.Longitude,
                                                   CustomerDisplayName = x.Customer.DisplayName,
                                                   CustomerMobileNumber = x.Customer.MobileNumber,
                                                   CustomerAddress = x.Customer.Address,
                                                   CustomerLatitude = x.Customer.Latitude,
                                                   CustomerLongitude = x.Customer.Longitude,
                                                   OrderStatus = x.Status.SystemName,
                                                   ModifyDate = x.ModifyDate,
                                               }).FirstOrDefaultAsync();

                            DateTime OrderDate = (DateTime)OrderDetails.ModifyDate;

                            _TrackOrderResponse.IsDelivery = true;
                            _TrackOrderResponse.OrderUpdateDate = OrderDate.ToString("MMM dd, yyyy | HH:mm");

                            _OrderDetails.OrderId = OrderDetails.OrderId;
                            _OrderDetails.OrderStatus = OrderDetails.OrderStatus;

                            _MerchantDetails.MerchantDisplayName = OrderDetails.MerchantDisplayName;
                            _MerchantDetails.MerchantMobileNumber = OrderDetails.MerchantMobileNumber;
                            _MerchantDetails.MerchantAddress = OrderDetails.MerchantAddress;
                            _MerchantDetails.MerchantLatitude = OrderDetails.MerchantLatitude;
                            _MerchantDetails.MerchantLongitude = OrderDetails.MerchantLongitude;

                            _TrackOrderResponse.MerchantDetails = _MerchantDetails;

                            _CustomerDetails.CustomerDisplayName = OrderDetails.CustomerDisplayName;
                            _CustomerDetails.CustomerMobileNumber = OrderDetails.CustomerMobileNumber;
                            _CustomerDetails.CustomerAddress = OrderDetails.CustomerAddress;
                            _CustomerDetails.CustomerLatitude = OrderDetails.CustomerLatitude;
                            _CustomerDetails.CustomerLongitude = OrderDetails.CustomerLongitude;

                            _TrackOrderResponse.CustomerDetails = _CustomerDetails;

                            if(OrderDetails.DeliveryPartner == HCoreConstant.DeliveryPartners.Dellyman)
                            {
                                _TrackOrderRequest.OrderCode = OrderDetails.OrderReference;
                                TrackOrderResponse _Response = _DellymanOrders.TrackOrder(_TrackOrderRequest);
                                if(_Response.OrderID != null || _Response.OrderID > 0)
                                {
                                    _RiderDetails.RiderName = _Response.RiderName;
                                    _RiderDetails.RiderLatitude = _Response.RiderLatitude;
                                    _RiderDetails.RiderLongitude = _Response.RiderLongitude;

                                    _OrderDetails.RiderDetails = _RiderDetails;
                                }
                            }

                            _TrackOrderResponse.OrderDetails = _OrderDetails;
                        }
                        else
                        {
                            _TrackOrderResponse.IsDelivery = false;

                            _MerchantDetails.MerchantDisplayName = DealDetails.MerchantDisplayName;
                            _MerchantDetails.MerchantMobileNumber = DealDetails.MerchantMobileNumber;
                            _MerchantDetails.MerchantAddress = DealDetails.MerchantAddress;
                            _MerchantDetails.MerchantLatitude = DealDetails.MerchantLatitude;
                            _MerchantDetails.MerchantLongitude = DealDetails.MerchantLongitude;

                            _TrackOrderResponse.MerchantDetails = _MerchantDetails;

                            _CustomerDetails.CustomerDisplayName = DealCodeDetails.CustomerDisplayName;
                            _CustomerDetails.CustomerMobileNumber = DealCodeDetails.CustomerMobileNumber;
                            _CustomerDetails.CustomerAddress = DealCodeDetails.CustomerAddress;
                            _CustomerDetails.CustomerLatitude = DealCodeDetails.CustomerLatitude;
                            _CustomerDetails.CustomerLongitude = DealCodeDetails.CustomerLongitude;

                            _TrackOrderResponse.CustomerDetails = _CustomerDetails;
                        }

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _TrackOrderResponse, "HC0200", "Order details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC001", "We didn't find any package that matches that tracking number");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("TrackOrder", _Exception, _Request.UserReference, "HC0500", _Exception.Message);
            }
        }
    }

    internal class ActorDeliveryInvoice: ReceiveActor
    {
        public ActorDeliveryInvoice()
        {
            FrameworkEmailProcessor? _FrameworkEmailProcessor;
            Receive<OOrderProcess.Invoice.Details>(_Request =>
            {
                _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                _FrameworkEmailProcessor.DeliveryInvoice(_Request);
            });
        }
    }
}

