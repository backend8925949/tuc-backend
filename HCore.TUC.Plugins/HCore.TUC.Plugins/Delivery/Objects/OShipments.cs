//==================================================================================
// FileName: OShipments.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Plugins.Delivery.Objects
{
    public class OShipments
    {
        public class Save
        {
            public class Request
            {
                public long AccountId { get; set; }
                public  long FromAddressId { get; set; }
                public  long ToAddressId { get; set; }
                public long ParcelId { get; set; }
                public string? ParcelReference { get; set; }
                public long DealId { get; set; }
                public double? DealPrice { get; set; }



                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? CustomerAddressReference { get; set; }
                public string? MerchantAddressReference { get; set; }
                public long MerchantId { get; set; }
                public long StoreId { get; set; }
                public string? StoreKey { get; set; }
                public long CustomerId { get; set; }
                public string? CustomerKey { get; set; }
                public string? MetaData { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? ShipmentId { get; set; }
                public double? RateAmount { get; set; }
                public string? RateId { get; set; }
                public string? Status { get; set; }
                public string? Message { get; set; }
            }
        }

        public class CheckRates
        {
            public string? ShipmentId { get; set; }
            public string? ParcelId { get; set; }
            public string? FromAddressId { get; set; }
            public string? ToAddressId { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class CheckRateDetails
        {
            public string? RateId { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Update
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? RateId { get; set; }
            public double? RateAmount { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class UpdateResponse
        {
            public string? ShipmentLable { get; set; }
            public string? TrackingNumber { get; set; }
            public DateTime? PickUpDate { get; set; }
            public DateTime? DeliveryDate { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? ToAddressId { get; set; }
            public string? ToAddressKey { get; set; }
            public string? ToName { get; set; }
            public string? ToEmailAddress { get; set; }
            public string? ToMobileNumber { get; set; }
            public string? ToAddress { get; set; }
            public string? ToAddressCity { get; set; }
            public string? ToAddressState { get; set; }
            public string? ToAddressCountry { get; set; }

            public long? FromAddressId { get; set; }
            public string? FromAddressKey { get; set; }
            public string? FromName { get; set; }
            public string? FromEmailAddress { get; set; }
            public string? FromMobileNumber { get; set; }
            public string? FromAddress { get; set; }
            public string? FromAddressCity { get; set; }
            public string? FromAddressState { get; set; }
            public string? FromAddressCountry { get; set; }

            public long? ReturnAddressId { get; set; }
            public string? ReturnAddressKey { get; set; }
            public string? ReturnName { get; set; }
            public string? ReturnEmailAddress { get; set; }
            public string? ReturnMobileNumber { get; set; }
            public string? ReturnAddress { get; set; }
            public string? ReturnAddressCity { get; set; }
            public string? ReturnAddressState { get; set; }
            public string? ReturnAddressCountry { get; set; }

            public long? ParcelId { get; set; }
            public string? ParcelKey { get; set; }
            public string? Parcel { get; set; }
            public string? ParcelDescription { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantMobileNumber { get; set; }
            public string? MerchantEmailAddress { get; set; }
            public string? MerchantAddress { get; set; }
            public string? MerchantIconUrl { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? StoreMobileNumber { get; set; }
            public string? StoreEmailAddress { get; set; }
            public string? StoreAddress { get; set; }
            public string? StoreIconUrl { get; set; }

            public long? CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerEmailAddress { get; set; }
            public string? CustomerAddress { get; set; }
            public string? CustomerIconUrl { get; set; }

            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public double? DealPrice { get; set; }
            public string? DealIconUrl { get; set; }

            public string? RateId { get; set; }
            public double? RateAmount { get; set; }
            public double? TotalAmount { get; set; }
            public double? SubTotalAmount { get; set; }
            public string? InvoiceNumber { get; set; }

            public int? ItemCount { get; set; }

            public string? Bill { get; set; }
            public string? TrackingNumber { get; set; }

            public long? CarrierId { get; set; }
            public string? CarrierKey { get; set; }
            public string? CarrierName { get; set; }
            public string? CarrierMobileNumber { get; set; }
            public string? CarrierEmailAddress { get; set; }
            public string? CarrierIconUrl { get; set; }

            public string? MetaData { get; set; }
            public DateTime? PickUpDate  { get; set; }
            public DateTime? DeliveryDate { get; set; }
            public string? ShipmentId { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? OrderId { get; set; }

            public long? ToAddressId { get; set; }
            public string? ToAddressKey { get; set; }
            public string? ToName { get; set; }
            public string? ToEmailAddress { get; set; }
            public string? ToMobileNumber { get; set; }
            public string? ToAddress { get; set; }
            public string? ToAddressCity { get; set; }
            public string? ToAddressState { get; set; }
            public string? ToAddressCountry { get; set; }

            public long? FromAddressId { get; set; }
            public string? FromAddressKey { get; set; }
            public string? FromName { get; set; }
            public string? FromEmailAddress { get; set; }
            public string? FromMobileNumber { get; set; }
            public string? FromAddress { get; set; }
            public string? FromAddressCity { get; set; }
            public string? FromAddressState { get; set; }
            public string? FromAddressCountry { get; set; }

            public long? ReturnAddressId { get; set; }
            public string? ReturnAddressKey { get; set; }
            public string? ReturnName { get; set; }
            public string? ReturnEmailAddress { get; set; }
            public string? ReturnMobileNumber { get; set; }
            public string? ReturnAddress { get; set; }
            public string? ReturnAddressCity { get; set; }
            public string? ReturnAddressState { get; set; }
            public string? ReturnAddressCountry { get; set; }

            public long? ParcelId { get; set; }
            public string? ParcelKey { get; set; }
            public string? Parcel { get; set; }
            public string? ParcelDescription { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantMobileNumber { get; set; }
            public string? MerchantEmailAddress { get; set; }
            public string? MerchantAddress { get; set; }
            public string? MerchantIconUrl { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? StoreMobileNumber { get; set; }
            public string? StoreEmailAddress { get; set; }
            public string? StoreAddress { get; set; }
            public string? StoreIconUrl { get; set; }

            public long? CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerEmailAddress { get; set; }
            public string? CustomerAddress { get; set; }
            public string? CustomerIconUrl { get; set; }

            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public double? DealPrice { get; set; }
            public string? DealIconUrl { get; set; }

            public string? RateId { get; set; }
            public double? RateAmount { get; set; }
            public double? TotalAmount { get; set; }
            public double? SubTotalAmount { get; set; }
            public string? InvoiceNumber { get; set; }

            public int? ItemCount { get; set; }

            public string? Bill { get; set; }
            public string? TrackingNumber { get; set; }

            public long? CarrierId { get; set; }
            public string? CarrierKey { get; set; }
            public string? CarrierName { get; set; }
            public string? CarrierMobileNumber { get; set; }
            public string? CarrierEmailAddress { get; set; }
            public string? CarrierIconUrl { get; set; }
            
            public string? MetaData { get; set; }
            public DateTime? PickUpDate { get; set; }
            public DateTime? DeliveryDate { get; set; }
            public string? ShipmentId { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class OrdersOverview
        {
            public long TotalOrders { get; set; }
            public long NewOrders { get; set; }
            public long ReadyToPickUpOrders { get; set; }
            public long ProcessingOrders { get; set; }
            public long DispatchedOreders { get; set; }
            public long FailedOrders { get; set; }
            public long TotalDeliveredOrders { get; set; }
            //public long ReturnedOreders { get; set; }
            public long TotalCancelledOrders { get; set; }
            public double? CancelledOredersAmount { get; set; }
            public double? TotalOrdersAmount { get; set; }
            public double? DeliveredOredersAmount { get; set; }
            //public double? ReturnedOredersAmount { get; set; }

            public long TotalOrderss { get; set; }
            public double? TotalOrdersAmountt { get; set; }
        }

        public class Overview
        {
            public long TotalPickUpOrders { get; set; }
            public double? PickUpOredersAmount { get; set; }
            public double? TotalOredersAmount { get; set; }
            public long TotalDeliveredOrders { get; set; }
            public double? DeliveredOredersAmount { get; set; }
        }
        public class ListDownload
        {
            public long OrderId { get; set; }
            public string? CarrierName { get; set; }
            public string? CarrierMobileNumber { get; set; }
            public string? CustomerName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantMobileNumber { get; set; }
            public double? DeliveryCharge { get; set; }
            public double? ProductPrice { get; set; }
            public double? TotalAmount { get; set; }
            public DateTime? PlacedDate { get; set; }
            public DateTime? DeliveryDate { get; set; }
            public string? OrderStatus { get; set; }
        }

        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public long SubAccountId { get; set; }
            public string? SubAccountKey { get; set; }
            public string? Type { get; set; }
            public DateTime Date { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class DateRangeResponse
        {
            public List<DateRange> DateRange { get; set; }
        }
        public class DateRange
        {
            public DateTime Date { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public object Data { get; set; }
        }

        public class Sales
        {
            public double? Amount { get; set; }
        }
    }
}

