//==================================================================================
// FileName: ODeliveryAddress.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Delivery.Objects
{
    public class ODeliveryAddress
    {
        public class ManageAddress
        {
           public class Request
            {
                public long AccountId { get; set; }
                public long AddressId { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? AddressReference { get; set; }
                public string? AddressTId { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? Address { get; set; }

                public long CoreAddressId { get; set; }
                public   string CoreAddressKey { get; set; }

            }
        }

        
        public class Save
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long CountryId { get; set; }
                public string? CountryKey { get; set; }
                public long CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public long CityId { get; set; }
                public string? CityKey { get; set; }
                public string? EmailAddress { get; set; }
                public string? FirstName { get; set; }
                public bool Is_Residetnial { get; set; }
                public string? LastName { get; set; }
                public string? AddressLine1 { get; set; }
                public string? AddressLine2 { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public long StateId { get; set; }
                public string? StateKey { get; set; }
                public string? Zip { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Update
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? AddressId { get; set; }
                public long CountryId { get; set; }
                public string? CountryKey { get; set; }
                public long CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public long CityId { get; set; }
                public string? CityKey { get; set; }
                public string? EmailAddress { get; set; }
                public string? FirstName { get; set; }
                public bool Is_Residetnial { get; set; }
                public string? LastName { get; set; }
                public string? AddressLine1 { get; set; }
                public string? AddressLine2 { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public long StateId { get; set; }
                public string? StateKey { get; set; }
                public string? Zip { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }

            public long? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }

            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }

            public int? LocationTypeId { get; set; }
            public string? LocationTypeCode { get; set; }
            public string? LocationTypeName { get; set; }

            public string? EmailAddress { get; set; }
            public string? Name { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? MobileNumber { get; set; }
            public string? AdditionalMobileNumber { get; set; }
            public string? Zip { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }

            public long? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }

            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }

            public int? LocationTypeId { get; set; }
            public string? LocationTypeCode { get; set; }
            public string? LocationTypeName { get; set; }

            public string? EmailAddress { get; set; }
            public string? Name { get; set; }
            public bool Is_Residetnial { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? MobileNumber { get; set; }
            public string? AdditionalMobileNumber { get; set; }
            public string? Zip { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class AddressRequest
        {
            public int? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public int? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }
            public int? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public int? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public bool IsPrimary { get; set; }
            public int? LocationTypeId { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class AddressResponse
        {
            public string? Status { get; set; }
            public string? Message { get; set; }
            public long? AddressId { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? Address { get; set; }
            public long? CustomerAddressId { get; set; }
        }
    }
}

