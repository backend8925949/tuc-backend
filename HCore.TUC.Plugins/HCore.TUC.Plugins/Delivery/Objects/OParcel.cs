//==================================================================================
// FileName: OParcel.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Plugins.Delivery.Objects
{
    public class OParcel
    {
        public class Save
        {
            public List<Items> Items { get; set; }
            public MetaData MetaData { get; set; }
            public string? PackagingCode { get; set; }
            //public string? WeightUnit { get; set; }
            public long ToAddressId { get; set; }
            public string? ToAddressKey { get; set; }
            public string? AddressId { get; set; }
            public long MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public long StoreId { get; set; }
            public string? StoreKey { get; set; }
            public long CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? Data { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Items
        {
            public long? ItemReferenceId { get; set; }
            public string? ItemDescription { get; set; }
            public string? ItemName { get; set; }
            public string? ItemCurrency { get; set; }
            public long? ItemValue { get; set; }
            public long? ItemQuantity { get; set; }
            public double? ItemWeight { get; set; }
        }

        public class MetaData
        {

        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ParcelDescription { get; set; }
            public string? WeightUnit { get; set; }
            public double? TotalWeight { get; set; }
            public string? ParcelId { get; set; }

            public long? PackagingId { get; set; }
            public string? PackagingCode { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }

            public double? Height { get; set; }
            public double? Length { get; set; }
            public string? ProductName { get; set; }
            public double? Width { get; set; }
            public double? Weight { get; set; }

            public string? TId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? ItemId { get; set; }
            public List<Items> Items { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ParcelDescription { get; set; }
            public string? WeightUnit { get; set; }
            public double? TotalWeight { get; set; }
            public string? ParcelId { get; set; }

            public long? PackagingId { get; set; }
            public string? PackagingCode { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public string? ProductName { get; set; }

            public string? TId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public long? ItemId { get; set; }
            public List<Items> Items { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}

