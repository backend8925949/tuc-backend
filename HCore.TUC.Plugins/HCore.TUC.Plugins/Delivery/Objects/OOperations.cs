//==================================================================================
// FileName: OOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
using static HCore.TUC.Plugins.Delivery.Objects.OOperations;

namespace HCore.TUC.Plugins.Delivery.Objects
{
    public class OOperations
    {
        public class Pricing
        {
            public class Request
            {
                public long AccountId { get; set; }
                public long AddressId { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public OAddress ToAddressComponent { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public long ParcelId { get; set; }
                public string? ParcelKey { get; set; }

                public long PackageId { get; set; }
                public  string PackageKey { get; set; }

                public long TrackingId { get; set; }
                public string? TrackingKey { get; set; }

                public long ShipmentId { get; set; }
                public string? ShipmentKey { get; set; }
                public string? ShipmentReference { get; set; }

                public long FromAddressId { get; set; }
                public string? FromAddressKey { get; set; }
                public string? FromAddressReference { get; set; }

                public long ToAddressId { get; set; }
                public string? ToAddressKey { get; set; }
                public string? ToAddressReference { get; set; }

                public List<Partner> DeliveryPartners { get; set; }
            }

            public class Partner
            {
                public string? ReferenceKey { get; set; }
                public string? CarrierId{ get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
                public string? DeliveryTime { get; set; }
                public  double  DeliveryEta { get; set; }
                public double DeliveryCharge { get; set; }
                public string? RateId { get; set; }
            }

            public class GetDeliveryPricing
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public int? CarrierId { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public long AccountId { get; set; }
                public long AddressId { get; set; }
                public long MerchantAddressId { get; set; }
                public double? DeliveryCharge { get; set; }
                public string? TimeZoneId { get; set; }
                public int? ItemCount { get; set; }
                public string? RedisKey { get; set; }
                public string? RateId { get; set; }
                public string? DeliveryPartnerKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class DeliveryPartnerss
            {
                public int? CarrierId { get; set; }
                public string? Name { get; set; }
                public double? DeliveryCharge { get; set; }
                public double? OriginalPrice { get; set; }
                public double? SavedPrice { get; set; }
                public double PayablePrice { get; set; }
                public double? DeductablePrice { get; set; }
                public string? IconUrl { get; set; }
                public string? RateId { get; set; }
                public string? DeliveryPartnerKey { get; set; }
                public string? RedisKey { get; set; }
                public string? KwikKey { get; set; }
                public string? ExpectedDeliveryTime { get; set; }
                public string? DeliveryNote { get; set; }
            }

            public class Packages
            {
                public string? PackageDescription { get; set; }
                public string? DeliveryContactName { get; set; }
                public string? DeliveryContactNumber { get; set; }
                public string? DeliveryGooglePlaceAddress { get; set; }
                public string? DeliveryLandmark { get; set; }
            }

            public class GetDeliveryPricingResponse
            {
                public long TrackingId { get; set; }
                public string? TrackingKey { get; set; }

                public long ShipmentId { get; set; }
                public string? ShipmentKey { get; set; }

                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }

                public long? FromAddressId { get; set; }
                public long? ToAddressId { get; set; }

                public List<DeliveryPartnerss> DeliveryPartners { get; set; }
            }

            public class PricingResponse
            {
                public long? OrderId { get; set; }
                public long? TrackingId { get; set; }
                public double? DeliveryCharge { get; set; }
                public string? OrderReference { get; set; }
                public string? Status { get; set; }
                public string? Message { get; set; }
            }
        }

        public class GetAppDeliveryPricing
        {
            public string? Task { get; set; }
            public int? DealId { get; set; }
            public string? DealKey { get; set; }
            public int? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public int? AddressId { get; set; }
            public string? AddressKey { get; set; }
            public AddressComponent ToAddressComponent { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class AddressComponent
        {
            public int? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }
            public string? Name { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public int? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }
            public int? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }
            public int? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public bool IsPrimary { get; set; }
            public int? LocationTypeId { get; set; }
        }

        public class AddressDetails
        {
            public string? MerchantDisplayName { get; set; }
            public string? MerchantEmailAddress { get; set; }
            public string? MerchantMobileNumber { get; set; }
            public string? MerchantCountryName { get; set; }
            public string? MerchantStateName { get; set; }
            public string? MerchantCityName { get; set; }
            public string? MerchantCountryCode { get; set; }
            public string? MerchantAddress { get; set; }
            public double? MerchantLatitude { get; set; }
            public double? MerchantLongitude { get; set; }
            public long? MerchantAddressId { get; set; }

            public string? CustomerDisplayName { get; set; }
            public string? CustomerEmailAddress { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerCountryName { get; set; }
            public string? CustomerStateName { get; set; }
            public string? CustomerCityName { get; set; }
            public string? CustomerCountryCode { get; set; }
            public string? CustomerAddress { get; set; }
            public double? CustomerLatitude { get; set; }
            public double? CustomerLongitude { get; set; }
            public long? CustomerAddressId { get; set; }
        }
    }
}
