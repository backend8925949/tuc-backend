//==================================================================================
// FileName: OPackaging.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Delivery.Objects
{
    public class OPackaging
    {
        public class Save
        {
            public class Request
            {
                public string? PackagingId { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public double? Height { get; set; }
                public double? Length { get; set; }
                public string? Name { get; set; }
                public string? Size_unit { get; set; }
                public string? Type { get; set; }
                public double? Width { get; set; }
                public double? Weight { get; set; }
                public string? Weight_unit { get; set; }
                public string? ParcelDescription { get; set; }
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? PackagingId { get; set; }
                public string? Status { get; set; }
                public string? Message { get; set; }
            }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public long? DealId { get; set; }
            public string? DealTitle { get; set; }
            public double? Height { get; set; }
            public double? Length { get; set; }
            public string? Name { get; set; }
            public string? Size_unit { get; set; }
            public string? Type { get; set; }
            public double? Width { get; set; }
            public double? Weight { get; set; }
            public string? Weight_unit { get; set; }
            public string? Packaging_Id { get; set; }
            public string? TId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class PackagingList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public double? Height { get; set; }
            public double? Length { get; set; }
            public string? Name { get; set; }
            public string? SizeUnit { get; set; }
            public string? Type { get; set; }
            public double? Width { get; set; }
            public double? Weight { get; set; }
            public string? WeightUnit { get; set; }
            public string? PackagingId { get; set; }
            public string? TId { get; set; }
            public int? CategoryId { get; set; }
            public string? CategoryName { get; set; }
            public int? ParentCategoryId { get; set; }
            public double? MinWeight { get; set; }
            public double? MaxWeight { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountEmailAddress { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public double? DealActualPrice { get; set; }
            public double? DealSellingPrice { get; set; }
            public string? DealIconUrl { get; set; }
            public double? Height { get; set; }
            public double? Length { get; set; }
            public string? Name { get; set; }
            public string? Size_unit { get; set; }
            public string? Type { get; set; }
            public double? Width { get; set; }
            public double? Weight { get; set; }
            public string? Weight_unit { get; set; }
            public string? Packaging_Id { get; set; }
            public string? TId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Update
        {
            public string? PackagingId { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ParcelDescription { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}

