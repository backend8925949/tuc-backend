﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Delivery.Objects
{
    public class OOrderProcess
    {
        public class ConfirmOrder
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class CancelOrder
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class ReadyToPickup
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Invoice
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public bool IsEmail { get; set; }
                public string? EmailAddress { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Details
            {
                public string? EmailAddress { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? IssuedDate { get; set; }
                public string? InvoiceNumber { get; set; }
                public Customer? CustomerDetails { get; set; }
                public Merchant? MerchantDetails { get; set; }
                public List<Products>? Products { get; set; }
                public AmountSummary? PaymentDetails { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Products
            {
                public string? ProductTitle { get; set; }
                public int? Quantity { get; set; }
                public double? ItemAmount { get; set; }
                public double? TotalItemAmount { get; set; }
            }

            public class AmountSummary
            {
                public double? DiscountAmount { get; set; }
                public double? DeliveryCharges { get; set; }
                public double? SubTotalAmount { get; set; }
                public double? TotalAmount { get; set; }
                public double? ComissionAmount { get; set; }
            }

            public class Customer
            {
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
            }

            public class Merchant
            {
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? Address { get; set; }
            }
        }

        public class TrackOrder
        {
            public class Request
            {
                public string? TrackingNumber { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public bool IsDelivery { get; set; }
                public string? OrderUpdateDate { get; set; }
                public CustomerDetails? CustomerDetails { get; set; }
                public MerchantDetails? MerchantDetails { get; set; }
                public OrderDetails? OrderDetails { get; set; }
            }

            public class MerchantDetails
            {
                public string? MerchantDisplayName { get; set; }
                public string? MerchantAddress { get; set; }
                public string? MerchantMobileNumber { get; set; }
                public double? MerchantLatitude { get; set; }
                public double? MerchantLongitude { get; set; }
            }

            public class CustomerDetails
            {
                public string? CustomerDisplayName { get; set; }
                public string? CustomerMobileNumber { get; set; }
                public string? CustomerAddress { get; set; }
                public double? CustomerLatitude { get; set; }
                public double? CustomerLongitude { get; set; }
            }

            public class OrderDetails
            {
                public string? OrderId { get; set; }
                public string? OrderStatus { get; set; }
                public RiderDetails? RiderDetails { get; set; }
            }

            public class RiderDetails
            {
                public string? RiderName { get; set; }
                public string? RiderMobileNumber { get; set; }
                public double? RiderLatitude { get; set; }
                public double? RiderLongitude { get; set; }
            }
        }
    }
}

