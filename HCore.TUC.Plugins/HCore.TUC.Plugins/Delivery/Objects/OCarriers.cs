//==================================================================================
// FileName: OCarriers.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Delivery.Objects
{
    public class OCarriers
    {
        public class Save
        {
            public class Request
            {
                public sbyte? IsActive { get; set; }
                public string? Type { get; set; }
                public long? PerPage { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNummber { get; set; }
            public sbyte? IsActive { get; set; }
            public sbyte? IsDomestic { get; set; }
            public sbyte? IsInternational { get; set; }
            public string? IconUrl { get; set; }
            public sbyte? IsRegional { get; set; }
            public sbyte? IsRequiresInvoice { get; set; }
            public sbyte? IsRequiresWaybill { get; set; }
            public string? Slug { get; set; }
            public string? CarrierId { get; set; }
            public DateTime CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? MobileNummber { get; set; }
            public sbyte? IsActive { get; set; }
            public sbyte? IsDomestic { get; set; }
            public sbyte? IsInternational { get; set; }
            public string? IconUrl { get; set; }
            public sbyte? IsRegional { get; set; }
            public sbyte? IsRequiresInvoice { get; set; }
            public sbyte? IsRequiresWaybill { get; set; }
            public string? Slug { get; set; }
            public string? CarrierId { get; set; }
            public DateTime CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}

