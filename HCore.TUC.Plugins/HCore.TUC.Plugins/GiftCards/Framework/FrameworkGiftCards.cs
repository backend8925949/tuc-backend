//==================================================================================
// FileName: FrameworkGiftCards.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to giftcards
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Net;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Plugins.GiftCards.Object;
using HCore.TUC.Plugins.Operations;
using HCore.TUC.Plugins.Resource;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.ThankUCash.Framework.FrameworkApp;

namespace HCore.TUC.Plugins.GiftCards.Framework
{
    public class OGiftCardOverview
    {
        public long Total { get; set; }
        public double? TotalAmount { get; set; }

        public long RedeemTotal { get; set; }
        public double? RedeemAmount { get; set; }

        public long TotalCustomer { get; set; }
    }

    public class OGiftCardAllOverview
    {
        public long TotalSenders { get; set; }
        public long TotalGiftCardSenders { get; set; }
        public double? TotalAmount { get; set; }
        public long TotalReceivers { get; set; }
        public long TotalGiftCardReceivers { get; set; }
        public double CommissionAmount { get; set; }
        public double? RedeemAmount { get; set; }
        public long GiftCardExpire { get; set; }
        public long Unredeemcard { get; set; }
        public double UnredeemcardAmount { get; set; }
        public long Total { get; set; }
        public long Used { get; set; }
        public long UnUsed { get; set; }
    }

    public class FrameworkGiftCards
    {
        HCoreContext _HCoreContext;
        HCore.Operations.Object.OUserAccount.Request _UserAccountCreateRequest;
        CAProduct _CAProduct;
        ManageCoreTransaction _ManageCoreTransaction;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<CAProductCode> _CAProductCodes;
        FrameworkWallet _FrameworkWallet;
        OGiftCardOverview _OGiftCardOverview;
        OGiftCardAllOverview _OGiftCardAllOverview;
        List<OGiftCard.Details> _AllTransactions;
        OOperations.Wallet.Balance.Response _BalanceResponse;
        OGiftCard.Balance.Response _Balance;
        List<OGiftCard.Transactions> _Transactions;
        List<OGiftCard.GiftCards> _GiftCards;
        List<OGiftCard.RedeemHistory> _RedeemHistory;
        OGiftCard.Overview _Overview;
        /// <summary>
        /// Description: Method to get giftcard overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetGiftCardOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0)
                    {
                        _OGiftCardOverview = new OGiftCardOverview();
                        _OGiftCardOverview.Total = _HCoreContext.CAProductCode.Count(x => x.Product.AccountId == _Request.AccountId && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard));
                        _OGiftCardOverview.TotalAmount = _HCoreContext.CAProductCode.Where(x => x.Product.AccountId == _Request.AccountId && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard)).Sum(x => x.ItemAmount);
                        _OGiftCardOverview.RedeemTotal = _HCoreContext.CAProductCode.Count(x => x.Product.AccountId == _Request.AccountId && x.StatusId == HelperStatus.ProdutCode.Used && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard));
                        _OGiftCardOverview.RedeemAmount = _HCoreContext.CAProductCode.Where(x => x.Product.AccountId == _Request.AccountId && x.StatusId == HelperStatus.ProdutCode.Used && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard)).Sum(x => x.ItemAmount);
                        _OGiftCardOverview.TotalCustomer = _HCoreContext.CAProductCode.Where(x => x.Product.AccountId == _Request.AccountId && x.StatusId == HelperStatus.ProdutCode.Used && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard)).Select(x => x.AccountId).Distinct().Count();
                        //_OGiftCardOverview.Draft = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Draft && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        //_OGiftCardOverview.ApprovalPending = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.ApprovalPending && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        //_OGiftCardOverview.Approved = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Approved && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        //_OGiftCardOverview.Published = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Published && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        //_OGiftCardOverview.Paused = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Paused && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        //_OGiftCardOverview.Expired = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Expired && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        //_OGiftCardOverview.Rejected = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Rejected && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OGiftCardOverview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200);
                    }
                    else
                    {
                        _OGiftCardOverview = new OGiftCardOverview();
                        _OGiftCardOverview.Total = _HCoreContext.CAProductCode.Count(x => (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard));
                        _OGiftCardOverview.TotalAmount = _HCoreContext.CAProductCode.Where(x => x.CreateDate < _Request.EndDate && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard)).Sum(x => x.ItemAmount);

                        _OGiftCardOverview.RedeemTotal = _HCoreContext.CAProductCode.Count(x => x.StatusId == HelperStatus.ProdutCode.Used && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard));
                        _OGiftCardOverview.RedeemAmount = _HCoreContext.CAProductCode.Where(x => x.StatusId == HelperStatus.ProdutCode.Used && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard)).Sum(x => x.ItemAmount);

                        _OGiftCardOverview.TotalCustomer = _HCoreContext.CAProductCode.Where(x => x.StatusId == HelperStatus.ProdutCode.Used && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard)).Select(x => x.AccountId).Distinct().Count();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OGiftCardOverview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description: Method to all giftcard overview for console panel 
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetHomeGiftCardOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    _OGiftCardAllOverview = new OGiftCardAllOverview();
                    _OGiftCardAllOverview.TotalSenders = _HCoreContext.CAProduct.Count(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && (x.TypeId == Helpers.Product.GiftCard));
                    _OGiftCardAllOverview.TotalReceivers = _HCoreContext.CAProductCode.Count(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && (x.TypeId == Helpers.Product.GiftCard));
                    _OGiftCardAllOverview.TotalAmount = _HCoreContext.CAProduct.Where(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && (x.TypeId == Helpers.Product.GiftCard)).Sum(x => x.Amount);
                    double commission = Convert.ToDouble(_HCoreContext.CAProduct.Where(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && (x.TypeId == Helpers.Product.GiftCard)).Sum(x => x.CommissionAmount));
                    _OGiftCardAllOverview.CommissionAmount = Convert.ToDouble(commission <= 0 ? _OGiftCardAllOverview.TotalAmount * 0.05 : commission);
                    _OGiftCardAllOverview.RedeemAmount = _HCoreContext.CAProductCode.Where(x => x.Account.CountryId == _Request.UserReference.CountryId && x.ModifyDate.Value >= _Request.StartDate && x.ModifyDate.Value <= _Request.EndDate && x.StatusId == HelperStatus.ProdutCode.Used && (x.TypeId == Helpers.Product.GiftCard)).Sum(x => x.ItemAmount);
                    _OGiftCardAllOverview.UnredeemcardAmount = Convert.ToDouble(_HCoreContext.CAProductCode.Where(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && x.StatusId == HelperStatus.ProdutCode.Unused && (x.TypeId == Helpers.Product.GiftCard)).Sum(x => x.ItemAmount));
                    _OGiftCardAllOverview.Unredeemcard = _HCoreContext.CAProductCode.Count(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && x.StatusId == HelperStatus.ProdutCode.Unused && (x.TypeId == Helpers.Product.GiftCard));
                    _OGiftCardAllOverview.GiftCardExpire = _HCoreContext.CAProductCode.Count(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.ModifyDate.Value <= _Request.EndDate && x.EndDate <= _Request.StartDate && (x.TypeId == Helpers.Product.GiftCard));

                    // NEW PARAMETERS
                    _OGiftCardAllOverview.Total = _HCoreContext.CAProductCode.Count();
                    _OGiftCardAllOverview.Used = _HCoreContext.CAProductCode.Count(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && (x.TypeId == Helpers.Product.GiftCard) && x.StatusId == HelperStatus.ProdutCode.Used);
                    _OGiftCardAllOverview.UnUsed = _HCoreContext.CAProductCode.Count(x => x.Account.CountryId == _Request.UserReference.CountryId && x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate && (x.TypeId == Helpers.Product.GiftCard) && x.StatusId == HelperStatus.ProdutCode.Unused);
                    _OGiftCardAllOverview.TotalGiftCardReceivers = _HCoreContext.HCUAccount.Where(x => x.CountryId == _Request.UserReference.CountryId && x.CAProductAccount.Any(a => a.Account.CountryId == _Request.UserReference.CountryId && a.CreateDate >= _Request.StartDate && a.CreateDate <= _Request.EndDate && a.Account.AccountTypeId == UserAccountType.Appuser && (a.TypeId == Helpers.Product.GiftCard))).Count();
                    _OGiftCardAllOverview.TotalGiftCardSenders = _HCoreContext.HCUAccount.Where(x => x.CountryId == _Request.UserReference.CountryId && x.CAProductCodeAccount.Any(a => a.Account.CountryId == _Request.UserReference.CountryId && a.CreateDate >= _Request.StartDate && a.CreateDate <= _Request.EndDate && a.Account.AccountTypeId == UserAccountType.Appuser && (a.TypeId == Helpers.Product.GiftCard))).Count();

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OGiftCardAllOverview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetGiftCardOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion

        }
        /// <summary>
        /// Description: Method to create quick gift cards
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse CreateGiftCard(OGiftCard.QuickCard _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            #region Manage Exception 
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0100, TUCPluginResource.HCP0100M);
                }
                if (_Request.Amount > 100000)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0110, TUCPluginResource.HCP0110M);
                }
                else if (_Request.Customer == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0111, TUCPluginResource.HCP0111M);
                }
                else if (string.IsNullOrEmpty(_Request.Customer.MobileNumber) && _Request.Customer.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0112, TUCPluginResource.HCP0112M);
                }
                else
                {
                    _FrameworkWallet = new FrameworkWallet();
                    OOperations.Wallet.Balance.Response _WalletBalance = _FrameworkWallet.GetWalletBalance(_Request.AccountId, TransactionSource.GiftCards);
                    if (_WalletBalance.Balance > 0 && (_WalletBalance.Balance + 1) > _Request.Amount)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId)
                                .Select(x => new
                                {
                                    Id = x.Id,
                                    AccountTypeId = x.AccountTypeId,
                                    StatusId = x.StatusId,
                                    DisplayName = x.DisplayName,
                                })
                                .FirstOrDefault();
                            if (AccountDetails != null)
                            {
                                if (AccountDetails.StatusId == HelperStatus.Default.Active)
                                {
                                    long AccountId = 0;
                                    _Request.Customer.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.Customer.MobileNumber);
                                    var CustomerInformation = _HCoreContext.HCUAccount.Where(x => (x.MobileNumber == _Request.Customer.MobileNumber || x.Id == _Request.Customer.AccountId) && x.AccountTypeId == UserAccountType.Appuser)
                                             .Select(x => new
                                             {
                                                 Id = x.Id,
                                                 AccountTypeId = x.AccountTypeId,
                                                 StatusId = x.StatusId,
                                             })
                                             .FirstOrDefault();
                                    if (CustomerInformation != null)
                                    {
                                        AccountId = CustomerInformation.Id;
                                    }
                                    else
                                    {
                                        _UserAccountCreateRequest = new OUserAccount.Request();
                                        _UserAccountCreateRequest.AccountTypeCode = UserAccountType.AppUserS;
                                        _UserAccountCreateRequest.AccountOperationTypeCode = AccountOperationType.OnlineAndOfflineS;
                                        _UserAccountCreateRequest.RegistrationSourceCode = RegistrationSource.SystemS;
                                        _UserAccountCreateRequest.Name = _Request.Customer.FirstName + " " + _Request.Customer.LastName;
                                        _UserAccountCreateRequest.DisplayName = _Request.Customer.FirstName;
                                        _UserAccountCreateRequest.FirstName = _Request.Customer.FirstName;
                                        _UserAccountCreateRequest.LastName = _Request.Customer.LastName;
                                        _UserAccountCreateRequest.EmailAddress = _Request.Customer.EmailAddress;
                                        _UserAccountCreateRequest.MobileNumber = _Request.Customer.MobileNumber;
                                        _UserAccountCreateRequest.GenderCode = _Request.Customer.GenderCode;
                                        _UserAccountCreateRequest.StatusCode = HelperStatus.Default.ActiveS;
                                        _UserAccountCreateRequest.UserReference = _Request.UserReference;
                                        ManageCoreUserOperations _ManageCoreUserOperations = new ManageCoreUserOperations();
                                        OUserAccount.Request _Response = _ManageCoreUserOperations.SaveUserAccountParameter(_UserAccountCreateRequest);
                                        if (_Response != null)
                                        {
                                            AccountId = (long)_Response.ReferenceId;
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000", "Unable to create user account. Please contact support");
                                        }
                                    }
                                    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.MobileNumber == _Request.Customer.MobileNumber && x.AccountTypeId == UserAccountType.Appuser)
                                             .Select(x => new
                                             {
                                                 x.Id,
                                                 x.AccountTypeId,
                                                 x.StatusId,
                                             })
                                             .FirstOrDefault();
                                    if (CustomerDetails != null)
                                    {
                                        if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                                        {
                                            _Request.Amount = HCoreHelper.RoundNumber(_Request.Amount, _AppConfig.SystemEntryRoundDouble);
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = _Request.AccountId;
                                            _CoreTransactionRequest.CustomerId = CustomerDetails.Id;
                                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.ReferenceNumber;
                                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.AccountId,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.GiftCard,
                                                SourceId = TransactionSource.GiftCards,
                                                Comment = _Request.Comment,
                                                Amount = _Request.Amount,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = CustomerDetails.Id,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.GiftCard,
                                                SourceId = TransactionSource.GiftCards,
                                                Amount = _Request.Amount,
                                                Comment = _Request.Comment,
                                                Comission = 0,
                                                TotalAmount = _Request.Amount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                            {
                                                using (_HCoreContext = new HCoreContext())
                                                {
                                                    DateTime CreateDate = HCoreHelper.GetGMTDate();
                                                    DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                                    DateTime EndDate = HCoreHelper.GetGMTDateTime().AddYears(1);
                                                    _CAProductCodes = new List<CAProductCode>();
                                                    _CAProductCodes.Add(new CAProductCode
                                                    {
                                                        Guid = HCoreHelper.GenerateGuid(),
                                                        AccountId = AccountId,
                                                        TypeId = Helpers.Product.QuickGiftCard,
                                                        ItemCode = "314" + HCoreHelper.GenerateRandomNumber(8),
                                                        ItemAmount = _Request.Amount,
                                                        AvailableAmount = _Request.Amount,
                                                        ItemPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4)),
                                                        StartDate = StartDate,
                                                        EndDate = EndDate,
                                                        UseCount = 0,
                                                        UseAttempts = 0,
                                                        CreateDate = CreateDate,
                                                        CreatedById = _Request.UserReference.AccountId,
                                                        StatusId = HelperStatus.ProdutCode.Unused
                                                    });

                                                    _CAProduct = new CAProduct();
                                                    _CAProduct.Guid = HCoreHelper.GenerateGuid();
                                                    _CAProduct.TypeId = Helpers.Product.QuickGiftCard;
                                                    _CAProduct.AccountId = AccountDetails.Id;
                                                    _CAProduct.Title = "Gift Card";
                                                    _CAProduct.Description = "Gift Card";
                                                    _CAProduct.Terms = _Request.Terms;
                                                    _CAProduct.UsageTypeId = HelperType.ProductUsageType.FullValue;
                                                    _CAProduct.UsageInformation = _Request.UsageInformation;
                                                    _CAProduct.IsPinRequired = 0;
                                                    _CAProduct.StartDate = StartDate;
                                                    _CAProduct.EndDate = EndDate;
                                                    _CAProduct.CodeValidityStartDate = _CAProduct.StartDate;
                                                    _CAProduct.CodeValidityEndDate = _CAProduct.EndDate;
                                                    _CAProduct.UnitPrice = _Request.Amount;
                                                    _CAProduct.SellingPrice = _Request.Amount;
                                                    _CAProduct.DiscountAmount = 0;
                                                    _CAProduct.DiscountPercentage = 0;
                                                    _CAProduct.Amount = _Request.Amount;
                                                    _CAProduct.Charge = 0;
                                                    _CAProduct.CommissionAmount = 0;
                                                    _CAProduct.TotalAmount = _Request.Amount;
                                                    _CAProduct.MaximumUnitSale = 1;
                                                    _CAProduct.TotalUnitSale = 0;
                                                    _CAProduct.TotalUsed = 0;
                                                    _CAProduct.TotalUsedAmount = 0;
                                                    _CAProduct.TotalSaleAmount = _Request.Amount;
                                                    _CAProduct.Views = 0;
                                                    _CAProduct.Message = _Request.Message;
                                                    _CAProduct.Comment = _Request.Comment;
                                                    _CAProduct.StatusId = HelperStatus.Product.Active;
                                                    _CAProduct.CreateDate = CreateDate;
                                                    _CAProduct.CreatedById = _Request.UserReference.AccountId;
                                                    _CAProduct.CAProductCodeProduct = _CAProductCodes;
                                                    _HCoreContext.CAProduct.Add(_CAProduct);
                                                    _HCoreContext.SaveChanges();
                                                    if (!string.IsNullOrEmpty(_Request.Message))
                                                    {
                                                        _Request.Message = _Request.Message.Replace("[AMOUNT]", _Request.Amount.ToString());
                                                        _Request.Message = _Request.Message.Replace("[DISPLAYNAME]", AccountDetails.DisplayName);
                                                        if (_Request.Message.Length > 120)
                                                        {
                                                            _Request.Message = _Request.Message.Substring(0, 120);
                                                        }
                                                        _Request.Message = _Request.Message + ". Download TUC App: https://bit.ly/tuc-app";
                                                        HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, _Request.Customer.MobileNumber, _Request.Message, CustomerDetails.Id, TransactionResponse.GroupKey, TransactionResponse.ReferenceId);
                                                    }
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001", "Gift card created for customer worth N" + _Request.Amount.ToString());
                                                }
                                            }
                                            else
                                            {
                                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0117, TUCPluginResource.HCP0117M + " " + TransactionResponse.Message);
                                            }
                                        }
                                        else
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0116, TUCPluginResource.HCP0116M);
                                        }
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0115, TUCPluginResource.HCP0115M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0114, TUCPluginResource.HCP0114M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                            }
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0113, TUCPluginResource.HCP0113M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateGiftCard", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        /// <summary>
        /// Description: Method to all giftcard list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetGiftCards(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.CAProductCode
                                                 .Where(x => x.Product.AccountId == _Request.AccountId && x.Product.Account.Guid == _Request.AccountKey
                                                 && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard))
                                                 .Select(x => new OGiftCard.GiftCard
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     ProductOwnerId = x.Product.AccountId,
                                                     ProductOwnerKey = x.Product.Account.Guid,

                                                     TypeId = x.TypeId,
                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,

                                                     SubTypeId = x.SubTypeId,
                                                     SubTypeCode = x.SubType.SystemName,
                                                     SubTypeName = x.SubType.Name,

                                                     ProductId = x.ProductId,
                                                     ProductKey = x.Product.Guid,

                                                     SubProductId = x.SubProductId,
                                                     SubProductKey = x.SubProduct.Guid,

                                                     AccountId = x.AccountId,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     AccountMobileNumber = x.Account.MobileNumber,
                                                     AccountIconUrl = x.Account.IconStorage.Path,

                                                     MerchantId = x.Account.OwnerId,
                                                     MerchantKey = x.Account.Owner.Guid,
                                                     MerchantDisplayName = x.Account.Owner.DisplayName,

                                                     ItemAmount = x.ItemAmount,
                                                     AvailableAmount = x.AvailableAmount,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     UseCount = x.UseCount,
                                                     LastUseDate = x.LastUseDate,

                                                     LastUseLocationId = x.LastUseLocation.AccountId,
                                                     LastUseLocationName = x.LastUseLocation.Account.DisplayName,
                                                     LastUseLocaionKey = x.LastUseLocation.Account.Guid,

                                                     LastUseSubLocationId = x.LastUseLocation.SubAccountId,
                                                     LastUseSubLocationName = x.LastUseLocation.SubAccount.DisplayName,
                                                     LastUseSubLocationKey = x.LastUseLocation.SubAccount.Guid,

                                                     ProviderId = x.LastUseLocation.Account.OwnerId,
                                                     ProviderKey = x.LastUseLocation.Account.Owner.Guid,
                                                     ProviderName = x.LastUseLocation.Account.Owner.DisplayName,

                                                     UseAttempts = x.UseAttempts,
                                                     Comment = x.Comment,
                                                     Message = x.Message,

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByKey = x.CreatedBy.Guid,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     ModifyByKey = x.ModifyBy.Guid,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OGiftCard.GiftCard> Data = _HCoreContext.CAProductCode
                                                 .Where(x => x.Product.AccountId == _Request.AccountId && x.Product.Account.Guid == _Request.AccountKey
                                                 && (x.TypeId == Helpers.Product.GiftCard || x.TypeId == Helpers.Product.QuickGiftCard))
                                                 .Select(x => new OGiftCard.GiftCard
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     ProductOwnerId = x.Product.AccountId,
                                                     ProductOwnerKey = x.Product.Account.Guid,

                                                     TypeId = x.TypeId,
                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,

                                                     SubTypeId = x.SubTypeId,
                                                     SubTypeCode = x.SubType.SystemName,
                                                     SubTypeName = x.SubType.Name,

                                                     ProductId = x.ProductId,
                                                     ProductKey = x.Product.Guid,

                                                     SubProductId = x.SubProductId,
                                                     SubProductKey = x.SubProduct.Guid,

                                                     AccountId = x.AccountId,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     AccountMobileNumber = x.Account.MobileNumber,
                                                     AccountIconUrl = x.Account.IconStorage.Path,

                                                     MerchantId = x.Account.OwnerId,
                                                     MerchantKey = x.Account.Owner.Guid,
                                                     MerchantDisplayName = x.Account.Owner.DisplayName,

                                                     ItemAmount = x.ItemAmount,
                                                     AvailableAmount = x.AvailableAmount,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     UseCount = x.UseCount,
                                                     LastUseDate = x.LastUseDate,

                                                     LastUseLocationId = x.LastUseLocation.AccountId,
                                                     LastUseLocationName = x.LastUseLocation.Account.DisplayName,
                                                     LastUseLocaionKey = x.LastUseLocation.Account.Guid,

                                                     LastUseSubLocationId = x.LastUseLocation.SubAccountId,
                                                     LastUseSubLocationName = x.LastUseLocation.SubAccount.DisplayName,
                                                     LastUseSubLocationKey = x.LastUseLocation.SubAccount.Guid,

                                                     UseAttempts = x.UseAttempts,
                                                     Comment = x.Comment,
                                                     Message = x.Message,

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByKey = x.CreatedBy.Guid,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     ModifyByKey = x.ModifyBy.Guid,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                        {
                            DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                        }
                        else
                        {
                            DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetGiftCards", _Exception, _Request.UserReference, _OResponse, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }

        List<OAnalytics.OSalesItem> _OSalesItem;
        List<OAnalytics.Sale> _SalesHistory;
        HCoreContextOperations _HCoreContextOperations;
        /// <summary>
        /// Description: Method to purchase history of giftcards
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetGiftCardPurchaseHistory(OAnalytics.Request _Request)
        {
            #region Manage Exception
            _SalesHistory = new List<OAnalytics.Sale>();
            try
            {
                //_Request.StartDate = _Request.StartDate.Value.Date;
                //_Request.EndDate = _Request.EndDate.Value.Date;
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _OSalesItem = new List<OAnalytics.OSalesItem>();
                        _OSalesItem = _HCoreContext.CAProductCode
                                           .Where(x => x.Product.TypeId == 459
                                                    && x.Product.AccountId == _Request.AccountId
                                                 && x.CreateDate.AddHours(1) > _Request.StartDate
                                                 && x.CreateDate.AddHours(1) < _Request.EndDate)
                                           .Select(x => new OAnalytics.OSalesItem
                                           {
                                               UserAccountId = x.AccountId,
                                               TransactionDateT = x.CreateDate.AddHours(1),
                                               InvoiceAmount = x.ItemAmount
                                           }).ToList();
                        _HCoreContext.Dispose();
                        #region Set Default Limit
                        #endregion
                        #region Get Data
                        if (_Request.Type == "hour")
                        {
                            for (int i = 0; i < 23; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Hour = i,
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                });
                            }
                            var _SalesDetails = _OSalesItem
                                                   .GroupBy(x => x.TransactionDateT.Hour)
                                                   .Select(x => new OAnalytics.Sale
                                                   {
                                                       Hour = x.Key,
                                                       TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                       TotalTransaction = x.Count(),
                                                       TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                   })
                                                   .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Hour == _SalesHistoryItem.Hour).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                }
                            }

                        }
                        else if (_Request.Type == "m")
                        {
                        }
                        else if (_Request.Type == "week")
                        {
                            string[] Days = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => x.TransactionDateT.DayOfWeek)
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     WeekDay = x.Key.ToString(),
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 })
                                                 .ToList();
                            foreach (var DayItem in Days)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.WeekDay == DayItem).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "year")
                        {

                            string[] Months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                            int[] MonthIndexes = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => new
                                                 {
                                                     //x.UserAccountId,
                                                     x.TransactionDateT.Month,
                                                 })
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     Month = x.Key.Month,
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 })
                                                 .ToList();

                            foreach (var MonthIndex in MonthIndexes)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Month == MonthIndex).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "month")
                        {


                            var start = _Request.StartDate;
                            var end = _Request.EndDate;

                            // set end-date to end of month
                            end = new DateTime(end.Value.Year, end.Value.Month, DateTime.DaysInMonth(end.Value.Year, end.Value.Month));

                            var diff = Enumerable.Range(0, Int32.MaxValue)
                                                 .Select(e => start.Value.AddMonths(e))
                                                 .TakeWhile(e => e <= end)
                                                 .Select(e => e.ToString("MMMM"));


                            _SalesHistory = _OSalesItem
                                             .GroupBy(x => new
                                             {
                                                 x.TransactionDateT.Year,
                                                 x.TransactionDateT.Month,
                                             })
                                             .Select(x => new OAnalytics.Sale
                                             {
                                                 Month = x.Key.Month,
                                                 Year = x.Key.Year,
                                                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                 TotalTransaction = x.Count(),
                                                 TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                             })
                                             .ToList();
                        }
                        else
                        {
                            int Days = (_Request.EndDate - _Request.StartDate).Value.Days;
                            DateTime? StartDate = _Request.StartDate.Value.Date;
                            for (int i = 0; i < Days; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Date = StartDate.Value.ToString("dd-MM-yyyy"),
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                });
                                StartDate = StartDate.Value.AddDays(1);
                            }
                            var _SalesDetails = _OSalesItem
                                           .GroupBy(x => new
                                           {
                                               x.TransactionDateT.Date,
                                           })
                                           .Select(x => new OAnalytics.Sale
                                           {
                                               Date = x.Key.Date.ToString("dd-MM-yyyy"),
                                               TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                               TotalTransaction = x.Count(),
                                               TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                           })
                                           .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Date == _SalesHistoryItem.Date).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                }
                            }




                            //_SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
                            //                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
                            //                           .GroupBy(x => x.UserAccountId)
                            //                           .Select(x => new OAnalytics.Sale
                            //                           {
                            //                               //UserAccountId = x.Key,
                            //                               //UserAccountTypeId = x.Select(m => m.UserAccountTypeId).FirstOrDefault(),
                            //                               //MerchantId = x.Select(m => m.MerchantId).FirstOrDefault(),
                            //                               //StoreId = x.Select(m => m.StoreId).FirstOrDefault(),
                            //                               //CashierId = x.Select(m => m.CashierId).FirstOrDefault(),
                            //                               //TerminalId = x.Select(m => m.TerminalId).FirstOrDefault(),
                            //                               //BankId = x.Select(m => m.BankId).FirstOrDefault(),
                            //                               //PtspId = x.Select(m => m.PtspId).FirstOrDefault(),
                            //                               //ManagerId = x.Select(m => m.ManagerId).FirstOrDefault(),
                            //                               //RmId = x.Select(m => m.RmId).FirstOrDefault(),
                            //                               //TotalUser = x.Sum(m => m.TotalUser),
                            //                               TotalTransaction = x.Sum(m => m.TotalTransaction),
                            //                               TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
                            //                               //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
                            //                               //CardTransaction = x.Sum(m => m.CardTransaction),
                            //                               //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
                            //                               //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
                            //                               //CashTransaction = x.Sum(m => m.CashTransaction),
                            //                               //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
                            //                               //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
                            //                               //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
                            //                               //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
                            //                               //FailedTransaction = x.Sum(m => m.FailedTransaction),
                            //                               //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
                            //                               //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
                            //                           })
                            //                          //.OrderBy(_Request.SortExpression)
                            //                          //.Skip(_Request.Offset)
                            //                          //.Take(_Request.Limit)
                            //                          .ToList();
                        }
                        #endregion
                        #region Create  Response Object
                        _HCoreContextOperations.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SalesHistory, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GeSalesSummary", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Method to redeem history of giftcards
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetGiftCardRedeemHistory(OAnalytics.Request _Request)
        {
            #region Manage Exception
            _SalesHistory = new List<OAnalytics.Sale>();
            try
            {
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _OSalesItem = new List<OAnalytics.OSalesItem>();
                        _OSalesItem = _HCoreContext.CAProductCode
                                       .Where(x => x.Product.TypeId == 459
                                            && x.StatusId == HelperStatus.ProdutCode.Used
                                                    && x.Product.AccountId == _Request.AccountId
                                            && x.LastUseDate != null
                                            && x.LastUseDate.Value.AddHours(1) > _Request.StartDate
                                            && x.LastUseDate.Value.AddHours(1) < _Request.EndDate)
                                       .Select(x => new OAnalytics.OSalesItem
                                       {
                                           UserAccountId = x.AccountId,
                                           TransactionDateT = x.CreateDate.AddHours(1),
                                           InvoiceAmount = x.ItemAmount
                                       }).ToList();
                        _HCoreContext.Dispose();
                        #region Set Default Limit
                        #endregion
                        #region Get Data
                        if (_Request.Type == "hour")
                        {
                            for (int i = 0; i < 23; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Hour = i,
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                });
                            }
                            var _SalesDetails = _OSalesItem
                                                   .GroupBy(x => x.TransactionDateT.Hour)
                                                   .Select(x => new OAnalytics.Sale
                                                   {
                                                       Hour = x.Key,
                                                       TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                       TotalTransaction = x.Count(),
                                                       TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                   })
                                                   .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Hour == _SalesHistoryItem.Hour).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                }
                            }

                        }
                        else if (_Request.Type == "m")
                        {

                        }
                        else if (_Request.Type == "week")
                        {
                            string[] Days = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => x.TransactionDateT.DayOfWeek)
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     WeekDay = x.Key.ToString(),
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 })
                                                 .ToList();
                            foreach (var DayItem in Days)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.WeekDay == DayItem).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "year")
                        {

                            string[] Months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                            int[] MonthIndexes = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => new
                                                 {
                                                     x.TransactionDateT.Month,
                                                 })
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     Month = x.Key.Month,
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 })
                                                 .ToList();

                            foreach (var MonthIndex in MonthIndexes)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Month == MonthIndex).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "month")
                        {


                            var start = _Request.StartDate;
                            var end = _Request.EndDate;

                            // set end-date to end of month
                            end = new DateTime(end.Value.Year, end.Value.Month, DateTime.DaysInMonth(end.Value.Year, end.Value.Month));

                            var diff = Enumerable.Range(0, Int32.MaxValue)
                                                 .Select(e => start.Value.AddMonths(e))
                                                 .TakeWhile(e => e <= end)
                                                 .Select(e => e.ToString("MMMM"));


                            _SalesHistory = _OSalesItem
                                             .GroupBy(x => new
                                             {
                                                 x.TransactionDateT.Year,
                                                 x.TransactionDateT.Month,
                                             })
                                             .Select(x => new OAnalytics.Sale
                                             {
                                                 Month = x.Key.Month,
                                                 Year = x.Key.Year,
                                                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                 TotalTransaction = x.Count(),
                                                 TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                             })
                                             .ToList();
                        }
                        else
                        {
                            int Days = (_Request.EndDate - _Request.StartDate).Value.Days;
                            DateTime? StartDate = _Request.StartDate.Value.Date;
                            for (int i = 0; i < Days; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Date = StartDate.Value.ToString("dd-MM-yyyy"),
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                });
                                StartDate = StartDate.Value.AddDays(1);
                            }
                            var _SalesDetails = _OSalesItem
                                           .GroupBy(x => new
                                           {
                                               x.TransactionDateT.Date,
                                           })
                                           .Select(x => new OAnalytics.Sale
                                           {
                                               Date = x.Key.Date.ToString("dd-MM-yyyy"),
                                               TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                               TotalTransaction = x.Count(),
                                               TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                           })
                                           .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Date == _SalesHistoryItem.Date).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                }
                            }




                            //_SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
                            //                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
                            //                           .GroupBy(x => x.UserAccountId)
                            //                           .Select(x => new OAnalytics.Sale
                            //                           {
                            //                               //UserAccountId = x.Key,
                            //                               //UserAccountTypeId = x.Select(m => m.UserAccountTypeId).FirstOrDefault(),
                            //                               //MerchantId = x.Select(m => m.MerchantId).FirstOrDefault(),
                            //                               //StoreId = x.Select(m => m.StoreId).FirstOrDefault(),
                            //                               //CashierId = x.Select(m => m.CashierId).FirstOrDefault(),
                            //                               //TerminalId = x.Select(m => m.TerminalId).FirstOrDefault(),
                            //                               //BankId = x.Select(m => m.BankId).FirstOrDefault(),
                            //                               //PtspId = x.Select(m => m.PtspId).FirstOrDefault(),
                            //                               //ManagerId = x.Select(m => m.ManagerId).FirstOrDefault(),
                            //                               //RmId = x.Select(m => m.RmId).FirstOrDefault(),
                            //                               //TotalUser = x.Sum(m => m.TotalUser),
                            //                               TotalTransaction = x.Sum(m => m.TotalTransaction),
                            //                               TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
                            //                               //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
                            //                               //CardTransaction = x.Sum(m => m.CardTransaction),
                            //                               //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
                            //                               //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
                            //                               //CashTransaction = x.Sum(m => m.CashTransaction),
                            //                               //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
                            //                               //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
                            //                               //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
                            //                               //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
                            //                               //FailedTransaction = x.Sum(m => m.FailedTransaction),
                            //                               //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
                            //                               //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
                            //                           })
                            //                          //.OrderBy(_Request.SortExpression)
                            //                          //.Skip(_Request.Offset)
                            //                          //.Take(_Request.Limit)
                            //                          .ToList();
                        }
                        #endregion
                        #region Create  Response Object
                        _HCoreContextOperations.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SalesHistory, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GeSalesSummary", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to create gift cards 
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveGiftCard(OGiftCard.CreateGiftCard _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            #region Manage Exception 
            try
            {
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0100, TUCPluginResource.HCP0100M);
                }
                //if (_Request.Amount > 100000)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0110, TUCPluginResource.HCP0110M);
                //}
                else if (_Request.senderInfo == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0231, TUCPluginResource.HCP0231M);
                }
                else if (_Request.receiverInfo.Count() == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0232, TUCPluginResource.HCP0232M);
                }
                else if (_Request.receiverInfo.Sum(x => x.Amount) != _Request.Amount)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0233, TUCPluginResource.HCP0233M);
                }
                else
                {

                    PayStackResponseData _PayStackResponseData = GetPayStackPaymentStatus(_Request.PaymentReference, _Request.UserReference);
                    if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var countryDetails = _HCoreContext.HCCoreCountry
                           .Where(x => x.Isd == _Request.senderInfo.CountryIsd)
                           .Select(x => new
                           {
                               x.Id,
                               x.Guid,
                               x.MobileNumberLength
                           })
                           .FirstOrDefault();
                            string FormatMobileNo = HCoreHelper.FormatMobileNumber(_Request.senderInfo.CountryIsd, _Request.senderInfo.MobileNo, countryDetails.MobileNumberLength);
                            string TSMobileNumber = _AppConfig.AppUserPrefix + FormatMobileNo;
                            var senderAccountDetails = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.User.Username == TSMobileNumber)
                                    .Select(x => new
                                    {
                                        Id = x.Id,
                                        AccountTypeId = x.AccountTypeId,
                                        StatusId = x.StatusId,
                                        DisplayName = x.DisplayName,
                                    })
                                    .FirstOrDefault();
                            long ParentId = 0;
                            if (senderAccountDetails != null)
                            {
                                ParentId = senderAccountDetails.Id;
                            }
                            else
                            {
                                _UserAccountCreateRequest = new OUserAccount.Request();
                                _UserAccountCreateRequest.AccountTypeCode = UserAccountType.AppUserS;
                                _UserAccountCreateRequest.AccountOperationTypeCode = AccountOperationType.OnlineAndOfflineS;
                                _UserAccountCreateRequest.RegistrationSourceCode = RegistrationSource.SystemS;
                                _UserAccountCreateRequest.Name = _Request.senderInfo.Name;
                                _UserAccountCreateRequest.DisplayName = _Request.senderInfo.Name;
                                _UserAccountCreateRequest.FirstName = _Request.senderInfo.Name;
                                _UserAccountCreateRequest.EmailAddress = _Request.senderInfo.Email;
                                _UserAccountCreateRequest.MobileNumber = _Request.senderInfo.MobileNo;
                                _UserAccountCreateRequest.StatusCode = HelperStatus.Default.ActiveS;
                                _UserAccountCreateRequest.UserReference = _Request.UserReference;
                                _UserAccountCreateRequest.UserReference.CountryIsd = _Request.senderInfo.CountryIsd == null ? _Request.UserReference.CountryIsd : _Request.senderInfo.CountryIsd;
                                _UserAccountCreateRequest.SourceId = TransactionType.GiftCard;
                                _UserAccountCreateRequest.CountryId = countryDetails.Id;
                                _UserAccountCreateRequest.CountryKey = countryDetails.Guid;
                                ManageCoreUserOperations _ManageCoreUserOperations = new ManageCoreUserOperations();
                                OUserAccount.Request _Response = _ManageCoreUserOperations.SaveUserAccountParameter(_UserAccountCreateRequest);
                                if (_Response != null)
                                {
                                    ParentId = (long)_Response.ReferenceId;
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000", "Unable to create user account. Please contact support");
                                }
                            }

                            double receivercount = _Request.receiverInfo.Count();
                            double giftcardamount = _Request.Amount / receivercount;
                            double Commission = _Request.Commission / Convert.ToDouble(100);
                            _Request.Amount = HCoreHelper.RoundNumber(_Request.Amount, _AppConfig.SystemEntryRoundDouble);
                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.UserReference.CountryIsd = _Request.senderInfo.CountryIsd == null ? _Request.UserReference.CountryIsd : _Request.senderInfo.CountryIsd;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = ParentId;
                            //_CoreTransactionRequest.CustomerId = ParentId;
                            _CoreTransactionRequest.InvoiceAmount = (_Request.Amount + (Commission * _Request.Amount));
                            //_CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceNumber = "GCS" + HCoreHelper.GenerateRandomNumber(6);
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = ParentId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.GiftCard,
                                SourceId = TransactionSource.GiftCards,
                                Comment = "Gift Card Amount Credited to Wallet",
                                Amount = _Request.Amount,
                                Comission = 0,
                                Charge = _Request.Commission,
                                TotalAmount = (_Request.Amount + (Commission * _Request.Amount)),
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = ParentId,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.GiftCard,
                                SourceId = TransactionSource.GiftCards,
                                Comment = _Request.senderInfo.Description,
                                Amount = _Request.Amount,
                                Comission = 0,
                                Charge = _Request.Commission,
                                TotalAmount = (_Request.Amount + (Commission * _Request.Amount)),
                            });
                            foreach (var receiver in _Request.receiverInfo)
                            {
                                long AccountId = 0;
                                var receivercountryDetails = _HCoreContext.HCCoreCountry
                                   .Where(x => x.Isd == receiver.CountryIsd)
                                   .Select(x => new
                                   {
                                       x.Id,
                                       x.Guid,
                                       x.MobileNumberLength
                                   })
                                   .FirstOrDefault();
                                string FormatMobileNumber = HCoreHelper.FormatMobileNumber(receiver.CountryIsd, receiver.MobileNo, receivercountryDetails.MobileNumberLength);
                                string TMobileNumber = _AppConfig.AppUserPrefix + FormatMobileNumber;
                                var receiverInformation = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser
                                && x.User.Username == TMobileNumber)
                                                .Select(x => new
                                                {
                                                    Id = x.Id,
                                                    AccountTypeId = x.AccountTypeId,
                                                    StatusId = x.StatusId,
                                                })
                                                .FirstOrDefault();

                                #region Create Account
                                if (receiverInformation != null)
                                {
                                    AccountId = receiverInformation.Id;
                                    receiver.AccountId = AccountId;
                                }
                                else
                                {
                                    _UserAccountCreateRequest = new OUserAccount.Request();
                                    _UserAccountCreateRequest.AccountTypeCode = UserAccountType.AppUserS;
                                    _UserAccountCreateRequest.AccountOperationTypeCode = AccountOperationType.OnlineAndOfflineS;
                                    _UserAccountCreateRequest.RegistrationSourceCode = RegistrationSource.SystemS;
                                    _UserAccountCreateRequest.Name = receiver.Name;
                                    _UserAccountCreateRequest.DisplayName = receiver.Name;
                                    _UserAccountCreateRequest.FirstName = receiver.Name;
                                    //  _UserAccountCreateRequest.LastName = _Request.Customer.LastName;
                                    _UserAccountCreateRequest.EmailAddress = receiver.Email;
                                    _UserAccountCreateRequest.MobileNumber = receiver.MobileNo;
                                    //  _UserAccountCreateRequest.GenderCode = _Request.Customer.GenderCode;
                                    _UserAccountCreateRequest.StatusCode = HelperStatus.Default.ActiveS;
                                    _UserAccountCreateRequest.UserReference = _Request.UserReference;
                                    _UserAccountCreateRequest.UserReference.CountryIsd = receiver.CountryIsd == null ? _Request.UserReference.CountryIsd : receiver.CountryIsd;
                                    _UserAccountCreateRequest.SourceId = TransactionType.GiftCard;
                                    _UserAccountCreateRequest.CountryId = receivercountryDetails.Id;
                                    _UserAccountCreateRequest.CountryKey = receivercountryDetails.Guid;
                                    ManageCoreUserOperations _ManageCoreUserOperations = new ManageCoreUserOperations();
                                    OUserAccount.Request _Response = _ManageCoreUserOperations.SaveUserAccountParameter(_UserAccountCreateRequest);
                                    if (_Response != null)
                                    {
                                        AccountId = (long)_Response.ReferenceId;
                                        receiver.AccountId = AccountId;
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000", "Unable to create user account. Please contact support");
                                    }
                                }
                                #endregion

                                #region Transaction                                                                                                           
                                //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                                //{
                                //    UserAccountId = AccountId,
                                //    ModeId = TransactionMode.Credit,
                                //    TypeId = TransactionType.GiftCard,
                                //    SourceId = TransactionSource.GiftCards,
                                //    Amount = receiver.Amount,
                                //    Comment = receiver.Description,
                                //    Comission = 0,
                                //    TotalAmount = receiver.Amount,
                                //    ReferenceAmount = receiver.Amount
                                //});
                                #endregion
                            }
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    DateTime CreateDate = HCoreHelper.GetGMTDate();
                                    DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                    DateTime EndDate = HCoreHelper.GetGMTDateTime().AddYears(1);
                                    _CAProductCodes = new List<CAProductCode>();
                                    foreach (var receiver in _Request.receiverInfo)
                                    {
                                        string ItemCode = "314" + HCoreHelper.GenerateRandomNumber(8);
                                        string ItemPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                        _CAProductCodes.Add(new CAProductCode
                                        {
                                            Guid = HCoreHelper.GenerateGuid(),
                                            AccountId = receiver.AccountId,
                                            TypeId = Helpers.Product.GiftCard,
                                            ItemCode = ItemCode,
                                            ItemAmount = receiver.Amount,
                                            AvailableAmount = receiver.Amount,
                                            ItemPin = ItemPin,
                                            StartDate = StartDate,
                                            EndDate = EndDate,
                                            UseCount = 0,
                                            UseAttempts = 0,
                                            CreateDate = CreateDate,
                                            CreatedById = ParentId,
                                            Comment = _Request.senderInfo.Description,
                                            StatusId = HelperStatus.ProdutCode.Unused
                                        });
                                        receiver.ItemCode = ItemCode;
                                        receiver.ItemPin = ItemPin;
                                    }
                                    _CAProduct = new CAProduct();
                                    _CAProduct.Guid = HCoreHelper.GenerateGuid();
                                    _CAProduct.TypeId = Helpers.Product.GiftCard;
                                    _CAProduct.AccountId = ParentId;
                                    _CAProduct.Title = "Gift Card";
                                    _CAProduct.Description = _Request.senderInfo.Description;
                                    // _CAProduct.Terms = _Request.Terms;
                                    _CAProduct.UsageTypeId = HelperType.ProductUsageType.FullValue;
                                    // _CAProduct.UsageInformation = _Request.UsageInformation;
                                    _CAProduct.IsPinRequired = 0;
                                    _CAProduct.StartDate = StartDate;
                                    _CAProduct.EndDate = EndDate;
                                    _CAProduct.CodeValidityStartDate = _CAProduct.StartDate;
                                    _CAProduct.CodeValidityEndDate = _CAProduct.EndDate;
                                    _CAProduct.UnitPrice = _Request.Amount;
                                    _CAProduct.SellingPrice = _Request.Amount;
                                    _CAProduct.DiscountAmount = 0;
                                    _CAProduct.DiscountPercentage = 0;
                                    _CAProduct.Amount = _Request.Amount;
                                    _CAProduct.Charge = _Request.Commission;
                                    _CAProduct.CommissionAmount = Commission * _Request.Amount;
                                    _CAProduct.TotalAmount = (_Request.Amount + Commission * _Request.Amount);
                                    _CAProduct.MaximumUnitSale = 1;
                                    _CAProduct.TotalUnitSale = 0;
                                    _CAProduct.TotalUsed = 0;
                                    _CAProduct.TotalUsedAmount = 0;
                                    _CAProduct.TotalSaleAmount = _Request.Amount;
                                    _CAProduct.Views = 0;
                                    _CAProduct.Message = _Request.senderInfo.Description;
                                    _CAProduct.StatusId = HelperStatus.Product.Active;
                                    _CAProduct.CreateDate = CreateDate;
                                    if (ParentId > 0)
                                    {
                                        _CAProduct.CreatedById = ParentId;
                                    }
                                    _CAProduct.PaymentSource = _Request.PaymentSource;
                                    _CAProduct.PaymentReference = _Request.PaymentReference;
                                    _CAProduct.TransactionId = _Request.TransactionId;
                                    _CAProduct.CompanyName = _Request.CompanyName;
                                    _CAProduct.IsIndividual = _Request.isIndividualorCorporate;
                                    _CAProduct.CAProductCodeProduct = _CAProductCodes;
                                    _HCoreContext.CAProduct.Add(_CAProduct);
                                    _HCoreContext.SaveChanges();
                                    if (!string.IsNullOrEmpty(_Request.senderInfo.Email))
                                    {
                                        string tbodystring = string.Empty;
                                        int count = 1;
                                        foreach (var receiver in _Request.receiverInfo)
                                        {
                                            string FormatReceiverMobileNumber = HCoreHelper.FormatMobileNumber(receiver.CountryIsd, receiver.MobileNo);
                                            tbodystring = tbodystring + "<table style='width:100%;border: 1px solid #D3D3D3;border-collapse: collapse;'>";
                                            tbodystring = tbodystring + "<thead>";
                                            tbodystring = tbodystring + "<tr>";
                                            tbodystring = tbodystring + "<td style='border: 1px solid #D3D3D3;border-collapse: collapse;text-align:center' colspan='2'>Receiver's Information</td>";
                                            tbodystring = tbodystring + "</tr>";
                                            tbodystring = tbodystring + "<tr>";
                                            tbodystring = tbodystring + "<td style='border: 1px solid #D3D3D3;border-collapse: collapse;width:100px;'>Receiver's Name</td>";
                                            tbodystring = tbodystring + "<td style='border: 1px solid #D3D3D3;border-collapse: collapse;text-align:center'>" + receiver.Name + "</td>";
                                            tbodystring = tbodystring + "</tr>";
                                            tbodystring = tbodystring + " <tr>";
                                            tbodystring = tbodystring + "<td style='border: 1px solid #D3D3D3;border-collapse: collapse;width:100px;'>Receiver's Email</td>";
                                            tbodystring = tbodystring + "<td style='border: 1px solid #D3D3D3;border-collapse: collapse;text-align:center'>" + receiver.Email + "</td>";
                                            tbodystring = tbodystring + "</tr>";
                                            tbodystring = tbodystring + "<tr>";
                                            tbodystring = tbodystring + "<td style='border: 1px solid #D3D3D3;border-collapse: collapse;width:100px;'>Receiver's Phone no</td>";
                                            tbodystring = tbodystring + "<td style='border: 1px solid #D3D3D3;border-collapse: collapse;text-align:center'>" + FormatReceiverMobileNumber + "</td>";
                                            tbodystring = tbodystring + "</tr>";
                                            tbodystring = tbodystring + "</thead>";
                                            tbodystring = tbodystring + "</table>";
                                            tbodystring = tbodystring + "<br>";
                                        }

                                        var _EmailParameters = new
                                        {

                                            InvoiceNo = _Request.PaymentReference,
                                            Email = _Request.senderInfo.Email,
                                            SenderMobileNo = FormatMobileNo,
                                            SenderName = _Request.senderInfo.Name,
                                            IssueDate = DateTime.Now.ToString("dd-MM-yyyy"),
                                            ExpiryDate = DateTime.Now.AddYears(1).ToString("dd-MM-yyyy"),
                                            UnitPrice = _Request.Amount,
                                            TUC = Math.Round(Commission * _Request.Amount),
                                            Qty = _Request.receiverInfo.Count(),
                                            TotalAmount = Math.Round(_Request.Amount + (Commission * _Request.Amount)),
                                            tbodystring = tbodystring,

                                        };
                                        HCoreHelper.BroadCastEmail("d-15c8d7a41f2e4d048e6e00290157874c", _Request.senderInfo.Name, _Request.senderInfo.Email, _EmailParameters, null);

                                    }
                                    var _Actor = ActorSystem.Create("ActorRecieverEmail");
                                    var _ActorNotify = _Actor.ActorOf<ActorRecieverEmail>("ActorRecieverEmail");
                                    foreach (var receiver in _Request.receiverInfo)
                                    {
                                        string s1 = "";
                                        s1 = s1 + "Dear " + receiver.Name;
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Congratulations! You have received a ThankUCash gift card from:";
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Gift Card Sender";
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Name:" + _Request.senderInfo.Name;
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Phone:" + _Request.senderInfo.MobileNo;
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Email:" + _Request.senderInfo.Email;
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Amount:" + receiver.Amount;
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Issue Date:" + StartDate;
                                        s1 = s1 + System.Environment.NewLine;
                                        s1 = s1 + "Expiry Date:" + EndDate;
                                        s1 = s1 + "Please Click here: https://thankucash.com/download.html to download our app to see how you can use your gift card and the list of merchants' stores where your gift card can be redeemed. ThankU.";
                                        //var ValidateMobileNumber1 = HCoreHelper.ValidateMobileNumber(receiver.MobileNo);
                                        HCoreHelper.SendSMS(SmsType.Transaction, receiver.CountryIsd, receiver.MobileNo, s1.ToString(), 1, null);
                                        if (!string.IsNullOrEmpty(receiver.Email))
                                        {
                                            OGiftCard.SendEmailRequest _SendEmailRequest = new OGiftCard.SendEmailRequest();
                                            _SendEmailRequest.UserReference = _Request.UserReference;
                                            _SendEmailRequest.ReceiversName = receiver.Name;
                                            _SendEmailRequest.InvoiceNo = _Request.PaymentReference;
                                            _SendEmailRequest.Email = _Request.senderInfo.Email;
                                            _SendEmailRequest.SenderMobileNo = FormatMobileNo;
                                            _SendEmailRequest.SenderName = _Request.senderInfo.Name;
                                            _SendEmailRequest.Amount = receiver.Amount;
                                            _SendEmailRequest.Date = DateTime.Now.ToString("dd-MM-yyyy");
                                            _SendEmailRequest.ExpiryDate = EndDate.ToString("dd-MM-yyyy");
                                            _SendEmailRequest.Description = _Request.senderInfo.Description;
                                            _SendEmailRequest.ItemCode = receiver.ItemCode;
                                            _SendEmailRequest.Pin = HCoreEncrypt.DecryptHash(receiver.ItemPin);
                                            _SendEmailRequest.RecieverName = receiver.Name;
                                            _SendEmailRequest.RecieverEmail = receiver.Email;
                                            //SendEmail(_SendEmailRequest);

                                            _ActorNotify.Tell(_SendEmailRequest);
                                            //var _EmailParameters = new
                                            //{
                                            //    ReceiversName = receiver.Name,
                                            //    InvoiceNo = _Request.PaymentReference,
                                            //    Email = _Request.senderInfo.Email,
                                            //    SenderMobileNo = FormatMobileNo,
                                            //    SenderName = _Request.senderInfo.Name,
                                            //    Amount = receiver.Amount,
                                            //    Date = DateTime.Now.ToString("dd-MM-yyyy"),
                                            //    ExpiryDate = EndDate.ToString("dd-MM-yyyy"),
                                            //    Description = _Request.senderInfo.Description,
                                            //    ItemCode = receiver.ItemCode,
                                            //    Pin = HCoreEncrypt.DecryptHash(receiver.ItemPin)
                                            //};
                                        }
                                    }
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001", "Gift card created for customer worth N" + _Request.Amount.ToString());
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                            }
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, PaymentResource.PR2123, PaymentResource.PR2123M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveGiftCard", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
            #endregion
        }
        private static PayStackResponseData GetPayStackPaymentStatus(string PaymentReference, OUserReference UserReference)
        {
            try
            {
                string PayStackUrl = "https://api.paystack.co/transaction/verify/";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PayStackUrl + PaymentReference);
                request.ContentType = "application/json";
                if (HostEnvironment == HostEnvironmentType.Live)
                {
                    request.Headers["Authorization"] = "Bearer sk_live_505bddd9f974f9dcedbfe21c1c31d526b41479e5";
                }
                else
                {
                    request.Headers["Authorization"] = "Bearer sk_test_5766345ff22d84b730a356092d8e54a528dfd814";
                }
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    string _Response = reader.ReadToEnd();
                    PayStackResponse _RequestBodyContent = JsonConvert.DeserializeObject<PayStackResponse>(_Response);
                    if (_RequestBodyContent != null)
                    {
                        return _RequestBodyContent.data;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetPayStackPaymentStatus", _Exception, UserReference);
                #endregion
                return null;
            }
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description: Method to all giftcard purchase history for console panel
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetHomeGiftCardPurchaseHistory(OAnalytics.Request _Request)
        {
            #region Manage Exception
            _SalesHistory = new List<OAnalytics.Sale>();
            try
            {
                //_Request.StartDate = _Request.StartDate.Value.Date;
                //_Request.EndDate = _Request.EndDate.Value.Date;
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        _OSalesItem = new List<OAnalytics.OSalesItem>();
                        _OSalesItem = _HCoreContext.CAProductCode
                                           .Where(x => x.Product.TypeId == Product.GiftCard
                                                && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                 //  && x.Product.AccountId == _Request.AccountId
                                                 && x.CreateDate.AddHours(1) > _Request.StartDate
                                                 && x.CreateDate.AddHours(1) < _Request.EndDate)
                                           .Select(x => new OAnalytics.OSalesItem
                                           {
                                               UserAccountId = x.AccountId,
                                               TransactionDateT = x.CreateDate.AddHours(1),
                                               InvoiceAmount = x.ItemAmount
                                           }).ToList();
                        _HCoreContext.Dispose();
                        #region Set Default Limit
                        #endregion
                        #region Get Data
                        if (_Request.Type == "hour")
                        {
                            for (int i = 0; i < 23; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Hour = i,
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                });
                            }
                            var _SalesDetails = _OSalesItem
                                                   .GroupBy(x => x.TransactionDateT.Hour)
                                                   .Select(x => new OAnalytics.Sale
                                                   {
                                                       Hour = x.Key,
                                                       TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                       TotalTransaction = x.Count(),
                                                       TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                   })
                                                   .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Hour == _SalesHistoryItem.Hour).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                }
                            }

                        }
                        else if (_Request.Type == "m")
                        {

                        }
                        else if (_Request.Type == "week")
                        {
                            string[] Days = { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => x.TransactionDateT.DayOfWeek)
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     WeekDay = x.Key.ToString(),
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 })
                                                 .ToList();
                            foreach (var DayItem in Days)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.WeekDay == DayItem).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        WeekDay = DayItem,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "year")
                        {

                            string[] Months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                            int[] MonthIndexes = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };

                            var _SalesDetails = _OSalesItem
                                                 .GroupBy(x => new
                                                 {
                                                     //x.UserAccountId,
                                                     x.TransactionDateT.Month,
                                                 })
                                                 .Select(x => new OAnalytics.Sale
                                                 {
                                                     Month = x.Key.Month,
                                                     TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                     TotalTransaction = x.Count(),
                                                     TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                                 })
                                                 .ToList();

                            foreach (var MonthIndex in MonthIndexes)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Month == MonthIndex).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = ItemInfo.TotalCustomer,
                                        TotalTransaction = ItemInfo.TotalTransaction,
                                        TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount,
                                    });
                                }
                                else
                                {
                                    _SalesHistory.Add(new OAnalytics.Sale
                                    {
                                        Month = MonthIndex,
                                        TotalCustomer = 0,
                                        TotalTransaction = 0,
                                        TotalInvoiceAmount = 0,
                                    });
                                }
                            }
                        }
                        else if (_Request.Type == "month")
                        {
                            var start = _Request.StartDate;
                            var end = _Request.EndDate;

                            // set end-date to end of month
                            end = new DateTime(end.Value.Year, end.Value.Month, DateTime.DaysInMonth(end.Value.Year, end.Value.Month));

                            var diff = Enumerable.Range(0, Int32.MaxValue)
                                                 .Select(e => start.Value.AddMonths(e))
                                                 .TakeWhile(e => e <= end)
                                                 .Select(e => e.ToString("MMMM"));


                            _SalesHistory = _OSalesItem
                                             .GroupBy(x => new
                                             {
                                                 x.TransactionDateT.Year,
                                                 x.TransactionDateT.Month,
                                             })
                                             .Select(x => new OAnalytics.Sale
                                             {
                                                 Month = x.Key.Month,
                                                 Year = x.Key.Year,
                                                 TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                                 TotalTransaction = x.Count(),
                                                 TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                             })
                                             .ToList();
                        }
                        else
                        {
                            int Days = (_Request.EndDate - _Request.StartDate).Value.Days;
                            DateTime? StartDate = _Request.StartDate.Value.Date;
                            for (int i = 0; i < Days; i++)
                            {
                                _SalesHistory.Add(new OAnalytics.Sale
                                {
                                    Date = StartDate.Value.ToString("dd-MM-yyyy"),
                                    TotalCustomer = 0,
                                    TotalTransaction = 0,
                                    TotalInvoiceAmount = 0,
                                });
                                StartDate = StartDate.Value.AddDays(1);
                            }
                            var _SalesDetails = _OSalesItem
                                           .GroupBy(x => new
                                           {
                                               x.TransactionDateT.Date,
                                           })
                                           .Select(x => new OAnalytics.Sale
                                           {
                                               Date = x.Key.Date.ToString("dd-MM-yyyy"),
                                               TotalCustomer = x.Select(a => a.UserAccountId).Distinct().Count(),
                                               TotalTransaction = x.Count(),
                                               TotalInvoiceAmount = x.Sum(m => m.InvoiceAmount),
                                           })
                                           .ToList();
                            foreach (var _SalesHistoryItem in _SalesHistory)
                            {
                                var ItemInfo = _SalesDetails.Where(x => x.Date == _SalesHistoryItem.Date).FirstOrDefault();
                                if (ItemInfo != null)
                                {
                                    _SalesHistoryItem.TotalCustomer = ItemInfo.TotalCustomer;
                                    _SalesHistoryItem.TotalTransaction = ItemInfo.TotalTransaction;
                                    _SalesHistoryItem.TotalInvoiceAmount = ItemInfo.TotalInvoiceAmount;
                                }
                            }




                            //_SalesHistory = _HCoreContextOperations.HCOAccountSalesHistory
                            //                     .Where(x => x.AccountId == _Request.AccountId && x.Date > _Request.StartDate && x.Date < _Request.EndDate)
                            //                           .GroupBy(x => x.UserAccountId)
                            //                           .Select(x => new OAnalytics.Sale
                            //                           {
                            //                               //UserAccountId = x.Key,
                            //                               //UserAccountTypeId = x.Select(m => m.UserAccountTypeId).FirstOrDefault(),
                            //                               //MerchantId = x.Select(m => m.MerchantId).FirstOrDefault(),
                            //                               //StoreId = x.Select(m => m.StoreId).FirstOrDefault(),
                            //                               //CashierId = x.Select(m => m.CashierId).FirstOrDefault(),
                            //                               //TerminalId = x.Select(m => m.TerminalId).FirstOrDefault(),
                            //                               //BankId = x.Select(m => m.BankId).FirstOrDefault(),
                            //                               //PtspId = x.Select(m => m.PtspId).FirstOrDefault(),
                            //                               //ManagerId = x.Select(m => m.ManagerId).FirstOrDefault(),
                            //                               //RmId = x.Select(m => m.RmId).FirstOrDefault(),
                            //                               //TotalUser = x.Sum(m => m.TotalUser),
                            //                               TotalTransaction = x.Sum(m => m.TotalTransaction),
                            //                               TotalInvoiceAmount = x.Sum(m => m.TotalInvoiceAmount),
                            //                               //CardTransactionUser = x.Sum(m => m.CardTransactionUser),
                            //                               //CardTransaction = x.Sum(m => m.CardTransaction),
                            //                               //CardInvoiceAmount = x.Sum(m => m.CardInvoiceAmount),
                            //                               //CashTransactionUser = x.Sum(m => m.CashTransactionUser),
                            //                               //CashTransaction = x.Sum(m => m.CashTransaction),
                            //                               //CashInvoiceAmount = x.Sum(m => m.CashInvoiceAmount),
                            //                               //SuccessfulTransaction = x.Sum(m => m.SuccessfulTransaction),
                            //                               //SuccessfulTransactionInvoiceAmount = x.Sum(m => m.SuccessfulTransactionInvoiceAmount),
                            //                               //SuccessfulTransactionUser = x.Sum(m => m.SuccessfulTransactionUser),
                            //                               //FailedTransaction = x.Sum(m => m.FailedTransaction),
                            //                               //FailedTransactionInvoiceAmount = x.Sum(m => m.FailedTransactionInvoiceAmount),
                            //                               //FailedTransactionUser = x.Sum(m => m.FailedTransactionUser),
                            //                           })
                            //                          //.OrderBy(_Request.SortExpression)
                            //                          //.Skip(_Request.Offset)
                            //                          //.Take(_Request.Limit)
                            //                          .ToList();
                        }
                        #endregion
                        #region Create  Response Object
                        _HCoreContextOperations.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SalesHistory, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetHomeSalesSummary", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to check customer balance from thankucash website.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetBalance(OGiftCard.Balance.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.accountNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO002", "Mobile number required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.pin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Pin number required");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.countryIsd, _Request.accountNumber);
                        string tAccountCode = _Request.accountNumber;
                        _Request.accountNumber = MobileNumber;
                        string FormatM = _AppConfig.AppUserPrefix + MobileNumber;

                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == FormatM || x.AccountCode == tAccountCode || x.User.Username == tAccountCode))
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    Pin = x.AccessPin,
                                    StatusId = x.StatusId,
                                    DisplayName = x.DisplayName,
                                    Name = x.Name,
                                    FirstName = x.FirstName,
                                    LastName = x.LastName,
                                }).FirstOrDefault();
                        if (UAccount != null)
                        {
                            string DApin = HCoreEncrypt.DecryptHash(UAccount.Pin);
                            if (DApin == _Request.pin)
                            {
                                if (UAccount.StatusId == HelperStatus.Default.Active)
                                {
                                    _Balance = new OGiftCard.Balance.Response();
                                    _Balance.displayName = UAccount.DisplayName;
                                    _Balance.firstName = UAccount.FirstName;
                                    _Balance.lastName = UAccount.LastName;

                                    string AccKey = HCoreEncrypt.EncodeText(HCoreEncrypt.EncryptHash(UAccount.AccountKey));
                                    _Balance.profileCode = AccKey;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    _Balance.balance = (long)HCoreHelper.RoundNumber(_ManageCoreTransaction.GetAppUserBalance(UAccount.AccountId) * 100, 0);
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Balance, "CAO007", "Balance loaded");
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO004", "Account not active. Contact support");
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO006", "Invalid account pin");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO005", "Account not registered");
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException(LogLevel.High, "GetAppUserBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCG500", "Unable to process your request.Please try after some time");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get all the giftcard transaction details according to sender.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetTransactions(OList.Request _Request)
        {
            #region Manage Exception
            _Transactions = new List<OGiftCard.Transactions>();
            try
            {
                if (!string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    if (string.IsNullOrEmpty(_Request.AccessPin))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Pin number required");
                        #endregion
                    }
                    else
                    {
                        string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, _Request.MobileNumber);
                        string tAccountCode = _Request.MobileNumber;
                        _Request.MobileNumber = MobileNumber;
                        string FormatM = _AppConfig.AppUserPrefix + MobileNumber;
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == FormatM || x.AccountCode == tAccountCode || x.User.Username == tAccountCode))
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    Pin = x.AccessPin,
                                    StatusId = x.StatusId,
                                    DisplayName = x.DisplayName,
                                    Name = x.Name,
                                    FirstName = x.FirstName,
                                    LastName = x.LastName,
                                }).FirstOrDefault();
                        if (UAccount != null)
                        {
                            string DApin = HCoreEncrypt.DecryptHash(UAccount.Pin);
                            if (DApin == _Request.AccessPin)
                            {
                                HCoreHelper.GetSearchCondition(_Request, "SenderMobileNo", _Request.MobileNumber, "=");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO006", "Invalid account pin");
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "IssueDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {

                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.CAProduct
                                                     .Where(x => x.TypeId == Helpers.Product.GiftCard
                                                     && x.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.Transactions
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         ExpiryDate = x.EndDate,
                                                         Amount = x.Amount,
                                                         PaidBy = "PayStack",
                                                         PaymentReference = x.PaymentReference,
                                                         SenderName = x.Account.DisplayName,
                                                         SenderMobileNo = x.Account.MobileNumber,
                                                         TotalCards = x.CAProductCodeProduct.Count(y => y.ProductId == x.Id),
                                                         TotalAmount = x.TotalAmount,
                                                         Commission = x.Charge,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                         ReceviersDetails = x.CAProductCodeProduct.Where(y => y.ProductId == x.Id)
                                                                            .Select(z => new OGiftCard.ReceviersDetails
                                                                            {
                                                                                ReferenceId = z.Id,
                                                                                ReferenceKey = z.Guid,
                                                                                CardNo = z.ItemCode,
                                                                                Amount = z.ItemAmount,
                                                                                Name = z.Account.DisplayName,
                                                                                MobileNumber = z.Account.MobileNumber,
                                                                            }).ToList()
                                                     })
                                             .Where(_Request.SearchCondition)
                                     .Count();
                        }
                        #endregion
                        #region Get Data
                        _Transactions = _HCoreContext.CAProduct
                                                     .Where(x => x.TypeId == Helpers.Product.GiftCard
                                                     && x.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.Transactions
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         ExpiryDate = x.EndDate,
                                                         Amount = x.Amount,
                                                         PaidBy = "PayStack",
                                                         PaymentReference = x.PaymentReference,
                                                         SenderName = x.Account.DisplayName,
                                                         SenderMobileNo = x.Account.MobileNumber,
                                                         TotalCards = x.CAProductCodeProduct.Count(y => y.ProductId == x.Id),
                                                         TotalAmount = x.TotalAmount,
                                                         Commission = x.Charge,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                         ReceviersDetails = x.CAProductCodeProduct.Where(y => y.ProductId == x.Id)
                                                                            .Select(z => new OGiftCard.ReceviersDetails
                                                                            {
                                                                                ReferenceId = z.Id,
                                                                                ReferenceKey = z.Guid,
                                                                                CardNo = z.ItemCode,
                                                                                Amount = z.ItemAmount,
                                                                                Name = z.Account.DisplayName,
                                                                                MobileNumber = z.Account.MobileNumber,
                                                                            }).ToList()
                                                     })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();

                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Transactions, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetGiftCardTransactions", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get all issued giftcards history.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetGiftCardsHistory(OList.Request _Request)
        {
            #region Manage Exception
            _GiftCards = new List<OGiftCard.GiftCards>();
            try
            {
                if (!string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    if (string.IsNullOrEmpty(_Request.AccessPin))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO003", "Pin number required");
                        #endregion
                    }
                    else
                    {
                        string MobileNumber = HCoreHelper.FormatMobileNumber(_Request.CountryIsd, _Request.MobileNumber);
                        string tAccountCode = _Request.MobileNumber;
                        _Request.MobileNumber = MobileNumber;
                        string FormatM = _AppConfig.AppUserPrefix + MobileNumber;
                        var UAccount = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && (x.User.Username == FormatM || x.AccountCode == tAccountCode || x.User.Username == tAccountCode))
                                .Select(x => new
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    Pin = x.AccessPin,
                                    StatusId = x.StatusId,
                                    DisplayName = x.DisplayName,
                                    Name = x.Name,
                                    FirstName = x.FirstName,
                                    LastName = x.LastName,
                                }).FirstOrDefault();
                        if (UAccount != null)
                        {
                            string DApin = HCoreEncrypt.DecryptHash(UAccount.Pin);
                            if (DApin == _Request.AccessPin)
                            {
                                HCoreHelper.GetSearchCondition(_Request, "ReceiverMobileNo", _Request.MobileNumber, "=");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "CAO006", "Invalid account pin");
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "IssueDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {

                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                     && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.GiftCards
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         SenderName = x.Product.Account.DisplayName,
                                                         SenderMobileNo = x.Product.Account.MobileNumber,
                                                         ReceiverName = x.Account.DisplayName,
                                                         ReceiverMobileNo = x.Account.MobileNumber,
                                                         Amount = x.ItemAmount,
                                                         Balance = x.AvailableAmount,
                                                         ExpiryDate = x.EndDate,
                                                         AccountId = x.Account.Id,
                                                         AccountKey = x.Account.Guid
                                                     })
                                             .Where(_Request.SearchCondition)
                                     .Count();
                        }
                        #endregion
                        #region Get Data
                        _GiftCards = _HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                      && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.GiftCards
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         SenderName = x.Product.Account.DisplayName,
                                                         SenderMobileNo = x.Product.Account.MobileNumber,
                                                         ReceiverName = x.Account.DisplayName,
                                                         ReceiverMobileNo = x.Account.MobileNumber,
                                                         Amount = x.ItemAmount,
                                                         Balance = x.AvailableAmount,
                                                         ExpiryDate = x.EndDate,
                                                         TransactionId = x.TransactionId,
                                                         AccountId = x.Account.Id,
                                                         AccountKey = x.Account.Guid
                                                     })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var _DataItem in _GiftCards)
                        {
                            _DataItem.GiftCardTransDetails = _HCoreContext.HCUAccountTransaction
                                                .Where(x => (x.TypeId == TransactionType.Loyalty.TUCRedeem.GiftCardRedeem
                                                    && x.AccountId == _DataItem.ReferenceId) ||
                                                     x.Id == _DataItem.TransactionId
                                                     )
                                                    .Select(z => new OGiftCard.GiftCardTransDetails
                                                    {
                                                        ReferenceId = z.Id,
                                                        ReferenceKey = z.Guid,
                                                        MerchantName = z.Parent.DisplayName,
                                                        StoreName = z.SubParent.DisplayName,
                                                        Amount = z.Amount,
                                                        IssueDate = z.TransactionDate,
                                                        //Balance=_DataItem.Balance

                                                    }).ToList();
                            if (_DataItem.GiftCardTransDetails.Count > 0)
                            {
                                foreach (var items in _DataItem.GiftCardTransDetails)
                                {
                                    items.Balance = _DataItem.Balance;
                                }
                            }
                        }
                        _HCoreContext.Dispose();

                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _GiftCards, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetGiftCardsHistory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get list of redeemed giftcards.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetRedeemHistory(OList.Request _Request)
        {
            #region Manage Exception
            _RedeemHistory = new List<OGiftCard.RedeemHistory>();
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "IssueDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    using (_HCoreContext = new HCoreContext())
                    {

                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                    && x.StatusId == HelperStatus.ProdutCode.Used
                                                    && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.RedeemHistory
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         ReceiverName = x.Account.DisplayName,
                                                         Amount = x.ItemAmount,
                                                         MerchantName = x.Transaction.Parent.DisplayName,
                                                         StoreName = x.Transaction.SubParent.DisplayName,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                     })
                                             .Where(_Request.SearchCondition)
                                     .Count();
                        }
                        #endregion
                        #region Get Data
                        _RedeemHistory = _HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                    && x.StatusId == HelperStatus.ProdutCode.Used
                                                     && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.RedeemHistory
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         ReceiverName = x.Account.DisplayName,
                                                         Amount = x.ItemAmount,
                                                         MerchantName = x.Transaction.Parent.DisplayName,
                                                         StoreName = x.Transaction.SubParent.DisplayName,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                     })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();

                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _RedeemHistory, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRedeemHistory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overview of giftcard transaction details according to sender.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetTransactionOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                using (_HCoreContext = new HCoreContext())
                {

                    _Overview = new OGiftCard.Overview();
                    _Overview.Total = _HCoreContext.CAProduct
                                                     .Where(x => x.TypeId == Helpers.Product.GiftCard
                                                     && x.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.Transactions
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         ExpiryDate = x.EndDate,
                                                         Amount = x.Amount,
                                                         PaidBy = "PayStack",
                                                         PaymentReference = x.PaymentReference,
                                                         SenderName = x.Account.DisplayName,
                                                         SenderMobileNo = x.Account.MobileNumber,
                                                         TotalCards = x.CAProductCodeProduct.Count(y => y.ProductId == x.Id),
                                                         TotalAmount = x.TotalAmount,
                                                         Commission = x.Charge,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid
                                                     })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    _Overview.Amount = _HCoreContext.CAProduct
                                                     .Where(x => x.TypeId == Helpers.Product.GiftCard
                                                     && x.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.Transactions
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         ExpiryDate = x.EndDate,
                                                         Amount = x.Amount,
                                                         PaidBy = "PayStack",
                                                         PaymentReference = x.PaymentReference,
                                                         SenderName = x.Account.DisplayName,
                                                         SenderMobileNo = x.Account.MobileNumber,
                                                         TotalCards = x.CAProductCodeProduct.Count(y => y.ProductId == x.Id),
                                                         TotalAmount = x.TotalAmount,
                                                         Commission = x.Charge,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid
                                                     })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.TotalAmount) ?? 0;

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetTransactionOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion

        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overivew of issued giftcards history.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetGiftCardsHistoryOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                using (_HCoreContext = new HCoreContext())
                {

                    _Overview = new OGiftCard.Overview();
                    _Overview.Total = _HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                     && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.GiftCards
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         SenderName = x.Product.Account.DisplayName,
                                                         SenderMobileNo = x.Product.Account.MobileNumber,
                                                         ReceiverName = x.Account.DisplayName,
                                                         ReceiverMobileNo = x.Account.MobileNumber,
                                                         Amount = x.ItemAmount,
                                                         Balance = x.AvailableAmount,
                                                         ExpiryDate = x.EndDate,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid
                                                     })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    _Overview.Amount = _HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                     && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.GiftCards
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         SenderName = x.Product.Account.DisplayName,
                                                         SenderMobileNo = x.Product.Account.MobileNumber,
                                                         ReceiverName = x.Account.DisplayName,
                                                         ReceiverMobileNo = x.Account.MobileNumber,
                                                         Amount = x.ItemAmount,
                                                         Balance = x.AvailableAmount,
                                                         ExpiryDate = x.EndDate,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                     })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.Amount) ?? 0;

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetGiftCardsHistoryOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion

        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overview of redeemed giftcards.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetRedeemHistoryOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                using (_HCoreContext = new HCoreContext())
                {

                    _Overview = new OGiftCard.Overview();
                    _Overview.Total = _HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                    && x.StatusId == HelperStatus.ProdutCode.Used
                                                   && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.RedeemHistory
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         ReceiverName = x.Account.DisplayName,
                                                         Amount = x.ItemAmount,
                                                         MerchantName = x.Transaction.Parent.DisplayName,
                                                         StoreName = x.Transaction.SubParent.DisplayName,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid
                                                     })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    _Overview.Amount = HCoreHelper.RoundNumber(_HCoreContext.CAProductCode
                                                     .Where(x => x.Product.TypeId == Helpers.Product.GiftCard
                                                    && x.StatusId == HelperStatus.ProdutCode.Used
                                                     && x.Product.Account.CountryId == _Request.UserReference.CountryId
                                                     )
                                                     .Select(x => new OGiftCard.RedeemHistory
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         IssueDate = x.StartDate,
                                                         CardNo = x.ItemCode,
                                                         ReceiverName = x.Account.DisplayName,
                                                         Amount = x.ItemAmount,
                                                         MerchantName = x.Transaction.Parent.DisplayName,
                                                         StoreName = x.Transaction.SubParent.DisplayName,
                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                     })
                                             .Where(_Request.SearchCondition)
                                             .Sum(x => x.Amount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetRedeemHistoryOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion

        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get commission for issuing giftcards aacording to user.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveGiftCardUserDrafts(OGiftCard.CreateGiftCard _Request)
        {
            //As of now static data is passed as per discussion with Suraj Sir, later on this functionality will be developed.
            long Commission = 5;
            var details = new
            {
                Commission = Commission,
            };
            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, details, "HC0001", "Details Loaded.");
        }

        /// <summary>
        /// Description: Method to get giftcard wallet balance.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetWalletBalance(OOperations.Wallet.Balance.Request _Request)
        {
            if (string.IsNullOrEmpty(_Request.SourceCode))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0107, TUCPluginResource.HCP0107M);
            }
            _BalanceResponse = new OOperations.Wallet.Balance.Response();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception
            try
            {
                long TransactionSourceId = 0;
                if (_Request.SourceCode == TransactionSource.GiftCardsS)
                {
                    TransactionSourceId = TransactionSource.GiftCards;
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0108, TUCPluginResource.HCP0108M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.SourceId == TransactionSource.GiftCards
                       && x.TransactionDate.Date >= _Request.StartDate.Date
                       && x.TransactionDate.Date <= _Request.EndDate.Date
                      //&& x.TransactionDate.Day == _Request.StartDate.Day
                      //&& x.TransactionDate.Year == _Request.StartDate.Year
                      && x.StatusId == HelperStatus.Transaction.Success
                       && x.ModeId == TransactionMode.Credit)
                       .Sum(x => (double?)x.ReferenceAmount) ?? 0;
                    _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                      .Where(x => x.SourceId == TransactionSource.GiftCards
                             && x.TransactionDate.Date >= _Request.StartDate.Date
                       && x.TransactionDate.Date <= _Request.EndDate.Date
                      //&& x.TransactionDate.Day == _Request.StartDate.Day
                      //&& x.TransactionDate.Year == _Request.StartDate.Year
                      && x.StatusId == HelperStatus.Transaction.Success
                      && x.ModeId == TransactionMode.Debit)
                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, TUCPluginResource.HCP0109, TUCPluginResource.HCP0109M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetWalletBalance", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal void SendEmail(OGiftCard.SendEmailRequest _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.RecieverName))
                {
                    HCoreHelper.BroadCastEmail("d-9f7758b668e143df9f72b5105f19c32e", _Request.RecieverName, _Request.RecieverEmail, _Request, null);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SendEmail", _Exception, _Request.UserReference);
            }
        }
    }

    internal class ActorRecieverEmail : ReceiveActor
    {
        public ActorRecieverEmail()
        {
            Receive<OGiftCard.SendEmailRequest>(_Request =>
            {
                FrameworkGiftCards _FrameworkGiftCards = new FrameworkGiftCards();
                _FrameworkGiftCards.SendEmail(_Request);
            });
        }
    }
}
