//==================================================================================
// FileName: OGiftCard.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Plugins.GiftCards.Object
{
    public class OGiftCard
    {
        public class QuickCard
        {
            public  long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public Customer Customer { get; set; }
            public double Amount { get; set; }
            public string? Comment { get; set; }
            public string? Message { get; set; }
            public string? ReferenceNumber { get; set; }
            //public string? SourceCode { get; set; }
            public string? Terms { get; set; }
            public string? UsageInformation { get; set; }
            //public string? ProductKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Customer
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public string? MobileNumber { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }
            public string? EmailAddress { get; set; }
            public string? GenderCode { get; set; }
        }

        public class GiftCard
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? ProductOwnerId { get; set; }
            public string? ProductOwnerKey { get; set; }

            public int? TypeId { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public int? SubTypeId { get; set; }
            public string? SubTypeCode { get; set; }
            public string? SubTypeName { get; set; }

            public long? ProductId { get; set; }
            public string? ProductKey { get; set; }

            public long? SubProductId { get; set; }
            public string? SubProductKey { get; set; }

            public string? AccountKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }

            public string? MerchantKey { get; set; }
            public long? MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }

            public double? ItemAmount { get; set; }
            public double? AvailableAmount { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public long? UseCount { get; set; }

            public DateTime? LastUseDate { get; set; }
            public long? LastUseLocationId { get; set; }
            public string? LastUseLocaionKey { get; set; }
            public string? LastUseLocationName { get; set; }

            public long? LastUseSubLocationId { get; set; }
            public string? LastUseSubLocationName { get; set; }
            public string? LastUseSubLocationKey { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderName { get; set; }

            public int? UseAttempts { get; set; }
            public string? Comment { get; set; }
            public string? Message { get; set; }

            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class CreateGiftCard
        {
            public double Amount { get; set; }
            public bool isIndividualorCorporate { get; set; }
            public bool type { get; set; }
            public SenderReceiverInfo senderInfo { get; set; }
            public List<SenderReceiverInfo> receiverInfo { get; set; }        
            public long TransactionId { get; set; }
            public string? PaymentReference { get; set; }
            public string? PaymentSource { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string? CompanyName { get; set; }
            public long Commission { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class SendEmailRequest
        {
            public string? ReceiversName { get; set; }
            public string? InvoiceNo { get; set; }
            public string? Email { get; set; }
            public string? SenderMobileNo { get; set; }
            public string? SenderName { get; set; }
            public double? Amount { get; set; }
            public string? Date { get; set; }
            public string?  ExpiryDate { get; set; }
            public string? Description { get; set; }
            public string? ItemCode { get; set; }
            public string? Pin { get; set; }
            public string? RecieverName { get; set; }
            public string? RecieverEmail { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class SenderReceiverInfo { 
            public string? Name { get; set; }
            public string? Email { get; set; }
            public string? MobileNo { get; set; }
            public string? Description { get; set; }
            public long AccountId { get; set; }
            public double Amount { get; set; }
            public string? CountryIsd { get; set; }
            public string? ItemCode { get; set; }
            public string? ItemPin { get; set; }
        }

        public class Details
        {
            #region Parameters
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? InoviceNumber { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountMobileNumber { get; set; }

            public long? ModeId { get; set; }
            public string? ModeKey { get; set; }
            public string? ModeName { get; set; }

            public long? SourceId { get; set; }
            public string? SourceKey { get; set; }
            public string? SourceName { get; set; }

            public long? UserAccountId { get; set; }
            public long? CardBankId { get; set; }
            public long? CardBrandId { get; set; }
            public long? AcquirerId { get; set; }
            public long? ProviderId { get; set; }
            public long? ParentId { get; set; }
            public long? CreatedById { get; set; }

            public long? TypeId { get; set; }
            public string? TypeKey { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCategory { get; set; }

            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public double RedeemAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public double ReferenceInvoiceAmount { get; set; }
            public double ConversionAmount { get; set; }
            public DateTime TransactionDate { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }

            public long? SubParentId { get; set; }
            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }

            public string? CreatedByKey { get; set; }
            public string? CreatedByMobileNumber { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByAccountTypeName { get; set; }
            public string? CreatedByAccountTypeCode { get; set; }

            public string? CreatedByOwnerKey { get; set; }
            public string? CreatedByOwnerDisplayName { get; set; }

            public double? ParentTransactionAmount { get; set; }
            public double? ParentTransactionChargeAmount { get; set; }
            public double? ParentTransactionCommissionAmount { get; set; }
            public double? ParentTransactionTotalAmount { get; set; }

            public long? OwnerId { get; set; }
            public long? InvoiceId { get; set; }
            public long? InvoiceItemId { get; set; }
            public long Status { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? GroupKey { get; set; }

            public string? TUCCardKey { get; set; }
            public string? TUCCardNumber { get; set; }
            public string? TCode { get; set; }

            public string? CardBrandName { get; set; }
            public string? CardSubBrandName { get; set; }
            public string? CardTypeName { get; set; }
            public string? CardBankName { get; set; }
            public string? CardBinNumber { get; set; }

            public double? RewardAmount { get; set; }
            public double? UserAmount { get; set; }
            public double? CommissionAmount { get; set; }
            public double? AcquirerAmount { get; set; }
            public double? PTSAAmount { get; set; }
            public double? PSSPAmount { get; set; }
            public double? ProviderAmount { get; set; }
            public double? IssuerAmount { get; set; }
            public double? ThankUAmount { get; set; }
            public double? MerchantAmount { get; set; }
            public string? AcquirerName { get; set; }
            public string? AcquirerDisplayName { get; set; }
            public string? AcquirerKey { get; set; }

            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }

            public string? AccountNumber { get; set; }

            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierCode { get; set; }
            public string? CashierDisplayName { get; set; }
            public long? TerminalReferenceId { get; set; }
            public string? TerminalReferenceKey { get; set; }
            public string? TerminalId { get; set; }
            #endregion

        }

        public class Balance
        {
            public class Request
            {
                public string? accountNumber { get; set; }
                public string? pin { get; set; }
                public string? countryIsd { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? displayName { get; set; }
                public string? firstName { get; set; }
                public string? lastName { get; set; }
                public long balance { get; set; }
                public string? profileLink { get; set; }
                public string? profileCode { get; set; }
            }
        }
        public class Transactions {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? IssueDate { get; set; }
            public string? SenderName { get; set; }
            public long TotalCards { get; set; }
            public double? Amount { get; set; }
            public DateTime? ExpiryDate { get; set; }
            public string? PaidBy { get; set; }
            public string? PaymentReference { get; set; }
            public string? SenderMobileNo { get; set; }
            public double? TotalAmount { get; set; }
            public double? Commission { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public List<ReceviersDetails> ReceviersDetails { get; set; }
        }
        public class ReceviersDetails {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? CardNo { get; set; }
            public double? Amount { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
        }
        public class GiftCards {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public DateTime? IssueDate { get; set; }
            public string? CardNo { get; set; }
            public string? SenderName { get; set; }
            public string? SenderMobileNo { get; set; }
            public string? ReceiverName { get; set; }
            public string? ReceiverMobileNo { get; set; }
            public double? Amount { get; set; }
            public double? Balance { get; set; }
            public DateTime? ExpiryDate { get; set; }
            public long? TransactionId { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public List<GiftCardTransDetails>? GiftCardTransDetails { get; set; }
        }
        public class GiftCardTransDetails {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime IssueDate { get; set; }
            public string? MerchantName { get; set; }
            public string? StoreName { get; set; }
            public double Amount { get; set; }
            public double? Balance { get; set; }
        }
        public class RedeemHistory {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime? IssueDate { get; set; }
            public string? CardNo { get; set; }
            public string? ReceiverName { get; set; }
            public string? MerchantName { get; set; }
            public string? StoreName { get; set; }
            public double? Amount { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
        }
        public class Overview { 
        public long Total { get; set; }
        public double? Amount { get; set; }
        }
    }
}
