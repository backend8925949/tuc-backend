//==================================================================================
// FileName: OAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.GiftCards.Object
{
    public class OAnalytics
    {

        public class OSalesItem
        {
            public long? UserAccountId { get; set; }
            //public DateTime TransactionDate { get; set; }
            public DateTime TransactionDateT { get; set; }
            public double? InvoiceAmount { get; set; }
            public long? CashierId { get; set; }
            public long TypeId { get; set; }
        }
        public class Overview
        {
            public long Stores { get; set; }
            public long Terminals { get; set; }
            public long Cashiers { get; set; }
            public long Subaccounts { get; set; }
            public bool IsTransactionPresent { get; set; }
            public double? Balance { get; set; }
            public double RewardPercentage { get; set; }

            public string? SubscriptionName { get; set; }
        }

        public class Request
        {
            public string? Type { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public long StoreReferenceId { get; set; }
            public long CustomerReferenceId { get; set; }
            public string? CustomerReferenceIdKey { get; set; }
            public string? StoreReferenceKey { get; set; }

            public int SourceId { get; set; }

            public long CashierReferenceId { get; set; }
            public string? CashierReferenceKey { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public bool AmountDistribution { get; set; } = false;
            public OUserReference? UserReference { get; set; }
        }

        public class SalesOverview
        {
            public long Customers { get; set; }
            public long Transactions { get; set; }
            public double InvoiceAmount { get; set; }
            public long CardTransaction { get; set; }
            public double CardInvoiceAmount { get; set; }
            public long CashTransaction { get; set; }
            public double CashInvoiceAmount { get; set; }
        }
        public class CustomerOverview
        {
            public long Total { get; set; }
            public long New { get; set; }
            public long Repeat { get; set; }
            public double AverageInvoiceAmount { get; set; }
            public double AverageVisit { get; set; }
        }

        public class Loyalty
        {

            public long? NewCustomers { get; set; }
            public long? RepeatingCustomers { get; set; }
            public long? VisitsByRepeatingCustomers { get; set; }

            public double? NewCustomerInvoiceAmount { get; set; }
            public double? RepeatingCustomerInvoiceAmount { get; set; }

            public long? Transaction { get; set; }
            public long? TransactionCustomer { get; set; }
            public double? TransactionInvoiceAmount { get; set; }

            public long? RewardTransaction { get; set; }
            public double? RewardInvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }
            public double? RewardCommissionAmount { get; set; }

            public long? TucRewardTransaction { get; set; }
            public double? TucRewardInvoiceAmount { get; set; }
            public double? TucRewardAmount { get; set; }
            public double? TucRewardCommissionAmount { get; set; }

            public long? TucPlusRewardTransaction { get; set; }
            public double? TucPlusRewardInvoiceAmount { get; set; }
            public double? TucPlusRewardAmount { get; set; }
            public double? TucPlusRewardCommissionAmount { get; set; }

            public long? TucPlusRewardClaimTransaction { get; set; }
            public double? TucPlusRewardClaimInvoiceAmount { get; set; }
            public double? TucPlusRewardClaimAmount { get; set; }


            public long? RedeemTransaction { get; set; }
            public double? RedeemInvoiceAmount { get; set; }
            public double? RedeemAmount { get; set; }
        }

        public class Sale
        {
            public string? TerminalId { get; set; }
            public long? Hour { get; set; }
            public string? WeekDay { get; set; }
            public string? Date { get; set; }
            public string? MonthName { get; set; }
            public long? Year { get; set; }
            public long? Month { get; set; }
            public long? TotalCustomer { get; set; }
            public long? TotalTransaction { get; set; }
            public double? TotalInvoiceAmount { get; set; }

            public string? StoreDisplayName { get; set; }
            public string? StoreAddress { get; set; }

            public DateTime? LastTransactionDate { get; set; }
        }


        public class SalesMetrics
        {
            public string? Title { get; set; }
            public int Hour { get; set; }
            public int Year { get; set; }
            public int Month { get; set; }
            public DateTime? Date { get; set; }
            public long TotalTransaction { get; set; }
            public long TotalCustomer { get; set; }
            public double TotalInvoiceAmount { get; set; }
            public long NewCustomer { get; set; }
            public double NewCustomerInvoiceAmount { get; set; }
            public long RepeatingCustomer { get; set; }
            public long VisitsByRepeatingCustomers { get; set; }
            public double RepeatingCustomerInvoiceAmount { get; set; }
        }
    }
}
