//==================================================================================
// FileName: ManageGiftCard.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 12-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.GiftCards.Framework;
using HCore.TUC.Plugins.GiftCards.Object;
using HCore.TUC.Plugins.Operations;

namespace HCore.TUC.Plugins.GiftCards
{
    public class ManageGiftCard
    {
        FrameworkGiftCards _FrameworkGiftCards;

        /// <summary>
        /// Description: Method to get giftcard overview
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetGiftCardOverview(OReference _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetGiftCardOverview(_Request);
        }
        /// <summary>
        /// Description: Method to create quick gift cards
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse CreateGiftCard(OGiftCard.QuickCard _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.CreateGiftCard(_Request);
        }

        /// <summary>
        /// Description: Method to all giftcard list
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetGiftCards(OList.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetGiftCards(_Request);
        }

        /// <summary>
        ///  Description: Method to purchase history of giftcards
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetGiftCardPurchaseHistory(OAnalytics.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetGiftCardPurchaseHistory(_Request);
        }
        /// <summary>
        /// Description: Method to redeem history of giftcards
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetGiftCardRedeemHistory(OAnalytics.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetGiftCardRedeemHistory(_Request);
        }

        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to create gift cards 
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveGiftCard(OGiftCard.CreateGiftCard _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.SaveGiftCard(_Request);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description: Method to all giftcard overview for console panel
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetHomeGiftCardOverview(OReference _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetHomeGiftCardOverview(_Request);
        }
        /// <summary>
        /// Author:Priya Chavadiya
        /// Description: Method to all giftcard purchase history for console panel
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetHomeGiftCardPurchaseHistory(OAnalytics.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetHomeGiftCardPurchaseHistory(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to check customer balance from thankucash website.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetBalance(OGiftCard.Balance.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetBalance(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get all the giftcard transaction details according to sender.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetTransactions(OList.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetTransactions(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get all issued giftcards history.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetGiftCardsHistory(OList.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetGiftCardsHistory(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get list of redeemed giftcards.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetRedeemHistory(OList.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetRedeemHistory(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overview of giftcard transaction details according to sender.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetTransactionsOverview(OList.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetTransactionOverview(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overivew of issued giftcards history.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetGiftCardsHistoryOverview(OList.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetGiftCardsHistoryOverview(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get overview of redeemed giftcards.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetRedeemHistoryOverview(OList.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetRedeemHistoryOverview(_Request);
        }
        /// <summary>
        /// Author: Priya Chavadiya
        /// Description: Method to get commission for issuing giftcards aacording to user.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SaveGiftCardUserDrafts(OGiftCard.CreateGiftCard _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.SaveGiftCardUserDrafts(_Request);
        }

        /// <summary>
        /// Description: Method to get giftcard wallet balance.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetWalletBalance(OOperations.Wallet.Balance.Request _Request)
        {
            _FrameworkGiftCards = new FrameworkGiftCards();
            return _FrameworkGiftCards.GetWalletBalance(_Request);
        }
    }
}
