//==================================================================================
// FileName: ManageDeals.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Integration.Mailerlite.Requests;
using HCore.Integration.Mailerlite.Responses;
using HCore.TUC.Plugins.Deals.Framework;
using HCore.TUC.Plugins.Deals.Object;

namespace HCore.TUC.Plugins.Deals
{
    public class ManageDeals
    {
        FrameworkDeal _FrameworkDeal;
        FrameworkDealNotification _FrameworkDealNotification;
        /// <summary>
        /// Description: Sends the deal notification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SendDealNotification(OReference _Request)
        {
            _FrameworkDealNotification = new FrameworkDealNotification();
            return _FrameworkDealNotification.SendDealNotification(_Request);
        }
        /// <summary>
        /// Description: Gets the deal notification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealNotification(OList.Request _Request)
        {
            _FrameworkDealNotification = new FrameworkDealNotification();
            return _FrameworkDealNotification.GetDealNotification(_Request);
        }
        /// <summary>
        /// Description: Validates the deal title.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ValidateDealTitle(ODeal.ValidateTitle.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.ValidateDealTitle(_Request);
        }
        /// <summary>
        /// Description: Saves the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveDeal(ODeal.Manage.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.SaveDeal(_Request);
        }
        /// <summary>
        /// Description: Updates the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDeal(ODeal.Manage.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.UpdateDeal(_Request);
        }
        /// <summary>
        /// Description: Extends the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ExtendDeal(ODeal.Manage.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.ExtendDeal(_Request);
        }
        /// <summary>
        /// Description: Updates the deal coupons.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDealCoupons(ODeal.Manage.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.UpdateDealCoupons(_Request);
        }
        /// <summary>
        /// Description: Updates the deal status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDealStatus(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.UpdateDealStatus(_Request);
        }
        /// <summary>
        /// Description: Deletes the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public async Task<OResponse> DeleteDeal(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return await _FrameworkDeal.DeleteDeal(_Request);
        }
        /// <summary>
        /// Description: Deletes the deal image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteDealImage(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.DeleteDealImage(_Request);
        }
        /// <summary>
        /// Description: Duplicates the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DuplicateDeal(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.DuplicateDeal(_Request);
        }
        /// <summary>
        /// Description: Approves the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ApproveDeal(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.ApproveDeal(_Request);
        }
        /// <summary>
        /// Description: Rejects the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RejectDeal(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.RejectDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the deal details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the deal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the flash deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetFlashDeal(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetFlashDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPurchaseHistory(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetPurchaseHistory(_Request);
        }

        /// <summary>
        /// Description: Gets the purchase details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPurchaseDetails(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetPurchaseDetails(_Request);
        }

        /// <summary>
        /// Description: Validates the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ValidateDealCode(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.ValidateDealCode(_Request);
        }

        /// <summary>
        /// Description: Updates the deal code status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDealCodeStatus(DealCodeStatus _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.UpdateDealCodeStatus(_Request);
        }
        /// <summary>
        /// Description: Gets the purchase history overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPurchaseHistoryOverview(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetPurchaseHistoryOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the deal review.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealReview(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDealReview(_Request);
        }

        /// <summary>
        /// Description: Updates the deal rview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDealRview(ODealReview.Update _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.UpdateDealReview(_Request);
        }

        /// <summary>
        /// Description: Gets the deal history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealHistory(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDealHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the deal overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealOverview(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDealOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the deal purchase overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealPurchaseOverview(OReference _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDealPurchaseOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant deal overview list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantDealOverviewList(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetMerchantDealOverviewList(_Request);
        }

        /// <summary>
        /// Description: Gets the customer deal overview list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerDealOverviewList(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetCustomerDealOverviewList(_Request);
        }

        /// <summary>
        /// Description: Saves the flash deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveFlashDeal(OFlashDeal.Manage.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.SaveFlashDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the deal location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealLocation(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.GetDealLocation(_Request);
        }
        /// <summary>
        /// Description: Removes the flash deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RemoveFlashDeal(OFlashDeal.Manage.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return _FrameworkDeal.RemoveFlashDeal(_Request);
        }

        /// <summary>
        /// This manager will manage the API call for the list of the all deal merchants.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public async Task<OResponse> GetDealMerchants(OList.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return await _FrameworkDeal.GetDealMerchants(_Request);
        }

        public async Task<OResponse> SaveDeal(CreateDeal.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return await _FrameworkDeal.SaveDeal(_Request);
        }

        public async Task<OResponse> UpdateDeal(CreateDeal.Request _Request)
        {
            _FrameworkDeal = new FrameworkDeal();
            return await _FrameworkDeal.UpdateDeal(_Request);
        }
    }
}
