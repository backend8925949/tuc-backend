﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Deals.PromoCode.Object
{
    public class OPromoCode
    {
        public class CreatePromoCode
        {
            public class Request
            {
                public string? ValuTypeCode { get; set; }
                public string? TypeCode { get; set; }
                public long? CategoryId { get; set; }
                public string? CategoryKey { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public double? Value { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public bool IsForAllDeals { get; set; }
                public List<Deals>? Deals { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Deals
            {
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class UpdatePromoCode
        {
            public class Request
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class PromoCodeList
        {
            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? PromoCode { get; set; }
                public int? TypeId { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }
                public int? ValueTypeId { get; set; }
                public string? ValueTypeCode { get; set; }
                public string? ValueTypeName { get; set; }
                public double? Value { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public sbyte? IsForAllDeals { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long? CategoryId { get; set; }
                public string? CategoryKey { get; set; }
                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public long? CreatedById { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? CreatedByDisplayName { get; set; }
                public long? ModifyById { get; set; }
                public DateTime? ModifyDate { get; set; }
                public string? ModifyByDisplayName { get; set; }
                public List<Deals>? Deals { get; set; }
                public AccountDetails? AccountDetails { get; set; }
                public CategoryDetails? CategoryDetails { get; set; }
            }

            public class Deals
            {
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public string? DealTitle { get; set; }
                public string? DealIconUrl { get; set; }
            }

            public class AccountDetails
            {
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? DisplayName { get; set; }
                public string? AccountIconUrl { get; set; }
            }

            public class CategoryDetails
            {
                public long? CategoryId { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
                public string? CategoryIconUrl { get; set; }
            }
        }
    }
}

