﻿using System;
using HCore.Helper;
using HCore.Data;
using HCore.Data.Models;
using HCore.TUC.Plugins.Deals.PromoCode.Object;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace HCore.TUC.Plugins.Deals.PromoCode.Framework
{
	public class FrameworkDealPromoCode
	{
        HCoreContext? _HCoreContext;
        MDDealPromoCode? _MDDealPromoCode;
        MDDealPromoCodeAccount? _MDDealPromoCodeAccount;
        List<OPromoCode.PromoCodeList.Response>? _PromoCodeList;
        List<OPromoCode.PromoCodeList.Deals>? _Deals;
        OPromoCode.PromoCodeList.AccountDetails? _AccountDetails;
        OPromoCode.PromoCodeList.CategoryDetails? _CategoryDetails;

        internal async Task<OResponse> CreatePromoCode(OPromoCode.CreatePromoCode.Request _Request)
		{
			try
			{
				if(string.IsNullOrEmpty(_Request.ValuTypeCode))
				{
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0001", "Value type code required");
                }
                if (string.IsNullOrEmpty(_Request.TypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0002", "Type code required");
                }
				if(_Request.Value < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0003", "Value must not be empty");
                }

                if (_Request.AccountId > 0 && _Request.CategoryId > 0 && _Request.Deals.Count > 0 && _Request.IsForAllDeals == true)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0014", "Please note that you can create a promo code only for a specific deal or a group of deals or a specific user, or all deals. You cannot create the promo code for all at a time.");
                }

                if (_Request.AccountId > 0 && _Request.TypeCode != "promocodetype.account")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0015", "Please note that if you are creating a promo code for a specific user type must be an account.");
                }

                if (_Request.CategoryId > 0 && _Request.TypeCode != "promocodetype.category")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0016", "Please note that if you are creating a promo code for a specific deal category type must be a category.");
                }

                if ((_Request.IsForAllDeals == true || _Request.Deals.Count > 0) && _Request.TypeCode != "promocodetype.deal")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0017", "Please note that if you are creating a promo code for a group of deals or for all deals type must be a deal.");
                }

                int? ValueTypeId = 0;
                if (!string.IsNullOrEmpty(_Request.ValuTypeCode))
                {
                    ValueTypeId = HCoreHelper.GetHelperId(_Request.ValuTypeCode);
                    if (ValueTypeId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0005", "Invalid value type");
                    }
                }

                int? TypeId = 0;
                if (!string.IsNullOrEmpty(_Request.TypeCode))
                {
                    TypeId = HCoreHelper.GetHelperId(_Request.TypeCode);
                    if (TypeId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0006", "Invalid type");
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string? PromoCode = "#DEALSBYTUC" + HCoreHelper.GenerateRandomNumber(11);

                    _MDDealPromoCode = new MDDealPromoCode();

                    if (_Request.Deals.Count > 0 && _Request.Deals != null)
                    {
                        foreach(var Deal in _Request.Deals)
                        {
                            bool IsDealExists = await _HCoreContext.MDDeal.AnyAsync(x => x.Id == Deal.DealId && x.Guid == Deal.DealKey && x.StatusId ==  HelperStatus.Deals.Published);
                            if (!IsDealExists)
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0007", "Invalid deal details");
                            }
                            else
                            {
                                _MDDealPromoCode.Guid = HCoreHelper.GenerateGuid();
                                _MDDealPromoCode.TypeId = TypeId;
                                _MDDealPromoCode.ValueTypeId = (int)ValueTypeId;
                                _MDDealPromoCode.DealId = Deal.DealId;
                                _MDDealPromoCode.Value = (double)_Request.Value;
                                _MDDealPromoCode.Code = PromoCode;
                                _MDDealPromoCode.IsAllDeals = 0;
                                _MDDealPromoCode.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                                _MDDealPromoCode.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                                _MDDealPromoCode.StatusId = HelperStatus.DealCodes.Unused;
                                _MDDealPromoCode.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                _MDDealPromoCode.CreatedById = _Request.UserReference.AccountId;
                                await _HCoreContext.MDDealPromoCode.AddAsync(_MDDealPromoCode);
                            }
                        }
                    }
                    else
                    {
                        if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                        {
                            bool IsDealExists = await _HCoreContext.HCUAccount.AnyAsync(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey && x.AccountTypeId == UserAccountType.Appuser);
                            if (!IsDealExists)
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0008", "Invalid account details");
                            }
                            else
                            {
                                _MDDealPromoCode.AccountId = _Request.AccountId;
                            }
                        }

                        if (_Request.CategoryId > 0 && !string.IsNullOrEmpty(_Request.CategoryKey))
                        {
                            bool IsDealExists = await _HCoreContext.MDCategory.AnyAsync(x => x.Id == _Request.CategoryId && x.Guid == _Request.CategoryKey);
                            if (!IsDealExists)
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0009", "Invalid category details");
                            }
                            else
                            {
                                _MDDealPromoCode.CategoryId = (int?)_Request.CategoryId;
                            }
                        }

                        _MDDealPromoCode.Guid = HCoreHelper.GenerateGuid();
                        _MDDealPromoCode.TypeId = TypeId;
                        _MDDealPromoCode.ValueTypeId = (int)ValueTypeId;
                        _MDDealPromoCode.Value = (double)_Request.Value;
                        _MDDealPromoCode.Code = PromoCode;
                        if (_Request.IsForAllDeals)
                        {
                            _MDDealPromoCode.IsAllDeals = 1;
                        }
                        else
                        {
                            _MDDealPromoCode.IsAllDeals = 0;
                        }
                        _MDDealPromoCode.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                        _MDDealPromoCode.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                        _MDDealPromoCode.StatusId = HelperStatus.DealCodes.Unused;
                        _MDDealPromoCode.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                        _MDDealPromoCode.CreatedById = _Request.UserReference.AccountId;
                        await _HCoreContext.MDDealPromoCode.AddAsync(_MDDealPromoCode);
                    }

                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();

                    OPromoCode.CreatePromoCode.Response _Response = new OPromoCode.CreatePromoCode.Response();
                    _Response.ReferenceId = _MDDealPromoCode.Id;
                    _Response.ReferenceKey = _MDDealPromoCode.Guid;

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "DP0010", "Promo code created successfully");
                }
            }
			catch (Exception _Exception)
			{
                return HCoreHelper.LogException("CreatePromoCode", _Exception, _Request.UserReference, "DP0500", "Unable to process your request");
			}
		}

        internal async Task<OResponse> UpdatePromoCodeStatus(OPromoCode.UpdatePromoCode.Request _Request)
        {
            try
            {
                if(_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0011", "Reference id required");
                }
                if(string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0012", "Reference key required");
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DPSTS", "Status code required");
                }

                int? StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DPINVSTS", "Invalid status");
                    }
                }

                using(_HCoreContext = new HCoreContext())
                {
                    var PromoCodeDetails = await _HCoreContext.MDDealPromoCode.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if(PromoCodeDetails != null)
                    {
                        if(PromoCodeDetails.StatusId == StatusId)
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0015", "This promo code status has already been updated.");
                        }
                        if(PromoCodeDetails.StatusId == HelperStatus.DealCodes.Expired)
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0015", "This promo code has already been expired.");
                        }

                        PromoCodeDetails.StatusId = StatusId;
                        PromoCodeDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                        PromoCodeDetails.ModifyById = _Request.UserReference.AccountId;

                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        OPromoCode.UpdatePromoCode.Response _Response = new OPromoCode.UpdatePromoCode.Response();
                        _Response.ReferenceId = PromoCodeDetails.Id;
                        _Response.ReferenceKey = PromoCodeDetails.Guid;

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "DP0013", "Promo code status updated successfully");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DP0404", "Promo code details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdatePromoCodeStatus", _Exception, _Request.UserReference, "DP0500", "Unable to process your request");
            }
        }

        internal async Task<OResponse> GetPromoCodes(OList.Request _Request)
        {
            try
            {
                _PromoCodeList = new List<OPromoCode.PromoCodeList.Response>();
                _Deals = new List<OPromoCode.PromoCodeList.Deals>();
                _AccountDetails = new OPromoCode.PromoCodeList.AccountDetails();
                _CategoryDetails = new OPromoCode.PromoCodeList.CategoryDetails();

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if(string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.MDDealPromoCode
                            .Select(x => new OPromoCode.PromoCodeList.Response
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                PromoCode = x.Code,
                                Value = x.Value,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                TypeId = x.TypeId,
                                TypeCode = x.Type.SystemName,
                                TypeName = x.Type.Name,
                                ValueTypeId = x.ValueTypeId,
                                ValueTypeCode = x.ValueType.SystemName,
                                ValueTypeName = x.ValueType.Name,
                                IsForAllDeals = x.IsAllDeals,
                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                CategoryId = x.CategoryId,
                                CategoryKey = x.Category.Guid,
                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName
                            })
                            .Where(_Request.SearchCondition)
                            .CountAsync();
                    }

                    _PromoCodeList = await _HCoreContext.MDDealPromoCode
                        .Select(x => new OPromoCode.PromoCodeList.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            PromoCode = x.Code,
                            Value = x.Value,
                            StartDate = x.StartDate,
                            EndDate = x.EndDate,
                            TypeId = x.TypeId,
                            TypeCode = x.Type.SystemName,
                            TypeName = x.Type.Name,
                            ValueTypeId = x.ValueTypeId,
                            ValueTypeCode = x.ValueType.SystemName,
                            ValueTypeName = x.ValueType.Name,
                            IsForAllDeals = x.IsAllDeals,
                            DealId = x.DealId,
                            DealKey = x.Deal.Guid,
                            AccountId = x.AccountId,
                            AccountKey = x.Account.Guid,
                            CategoryId = x.CategoryId,
                            CategoryKey = x.Category.Guid,
                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByDisplayName = x.ModifyBy.DisplayName
                        }).Where(_Request.SearchCondition)
                        .OrderBy(_Request.SortExpression)
                        .Skip(_Request.Offset)
                        .Take(_Request.Limit)
                        .ToListAsync();

                    if(_PromoCodeList.Count > 0)
                    {
                        foreach(var PromoCode in _PromoCodeList)
                        {
                            _Deals = await _HCoreContext.MDDeal.Where(x => x.Id == PromoCode.DealId && x.Guid == PromoCode.DealKey)
                                .Select(x => new OPromoCode.PromoCodeList.Deals
                                {
                                    DealId = x.Id,
                                    DealKey = x.Guid,
                                    DealTitle = x.Title,
                                    DealIconUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                }).ToListAsync();
                            PromoCode.Deals = _Deals;

                            _AccountDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == PromoCode.AccountId && x.Guid == PromoCode.AccountKey)
                                .Select(x => new OPromoCode.PromoCodeList.AccountDetails
                                {
                                    AccountId = x.Id,
                                    AccountKey = x.Guid,
                                    DisplayName = x.DisplayName,
                                    AccountIconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                                }).FirstOrDefaultAsync();
                            PromoCode.AccountDetails = _AccountDetails;

                            _CategoryDetails = await _HCoreContext.MDCategory.Where(x => x.Id == PromoCode.CategoryId && x.Guid == PromoCode.CategoryKey)
                                .Select(x => new OPromoCode.PromoCodeList.CategoryDetails
                                {
                                    CategoryId = x.Id,
                                    CategoryKey = x.Guid,
                                    CategoryName = x.RootCategory.Name,
                                    CategoryIconUrl = _AppConfig.StorageUrl + x.RootCategory.PosterStorage.Path,
                                }).FirstOrDefaultAsync();
                            PromoCode.CategoryDetails = _CategoryDetails;
                        }
                    }

                    await _HCoreContext.DisposeAsync();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _PromoCodeList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "DP0200", "Promo code list loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPromoCodes", _Exception, _Request.UserReference, "DP0500", "Unable to process your request");
            }
        }

        public async Task ManagePromoCodeStatus()
        {
            try
            {
                DateTime? CurrentTime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    var _ActivePromoCodes = await _HCoreContext.MDDealPromoCode.Where(x => x.EndDate <= CurrentTime && x.StatusId == HelperStatus.DealCodes.Unused).ToListAsync();
                    if (_ActivePromoCodes.Count > 0)
                    {
                        foreach (var PromoCode in _ActivePromoCodes)
                        {
                            PromoCode.StatusId = HelperStatus.DealCodes.Expired;
                            PromoCode.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }

                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManagePromoCodeStatus", _Exception);
            }
        }
	}
}

