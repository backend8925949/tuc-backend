﻿using System;
using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.PromoCode.Framework;
using HCore.TUC.Plugins.Deals.PromoCode.Object;

namespace HCore.TUC.Plugins.Deals.PromoCode
{
	public class ManageDealPromoCode
	{
		FrameworkDealPromoCode? _FrameworkDealPromoCode;

		public async Task<OResponse> CreatePromoCode(OPromoCode.CreatePromoCode.Request _Request)
		{
			_FrameworkDealPromoCode = new FrameworkDealPromoCode();
			return await _FrameworkDealPromoCode.CreatePromoCode(_Request);
        }

        public async Task<OResponse> UpdatePromoCodeStatus(OPromoCode.UpdatePromoCode.Request _Request)
        {
            _FrameworkDealPromoCode = new FrameworkDealPromoCode();
            return await _FrameworkDealPromoCode.UpdatePromoCodeStatus(_Request);
        }

        public async Task<OResponse> GetPromoCodes(OList.Request _Request)
        {
            _FrameworkDealPromoCode = new FrameworkDealPromoCode();
            return await _FrameworkDealPromoCode.GetPromoCodes(_Request);
        }

        public void ManagePromoCodeStatus()
        {
            try
            {
                var System = ActorSystem.Create("ActorManagePromoCodeStatus");
                var Greeter = System.ActorOf<ActorManagePromoCodeStatus>("ActorManagePromoCodeStatus");
                Greeter.Tell("managepromocodestatus");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManagePromoCodeStatus", _Exception);
            }

        }
    }

    internal class ActorManagePromoCodeStatus : ReceiveActor
    {
        public ActorManagePromoCodeStatus()
        {
            ReceiveAsync<string>(async _Request =>
            {
                FrameworkDealPromoCode _FrameworkDealPromoCode = new FrameworkDealPromoCode();
                await _FrameworkDealPromoCode.ManagePromoCodeStatus();
            });
        }
    }
}

