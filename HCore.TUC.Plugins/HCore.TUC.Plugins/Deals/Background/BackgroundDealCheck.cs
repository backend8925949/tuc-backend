//==================================================================================
// FileName: BackgroundDealCheck.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Deals;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;
namespace HCore.TUC.Plugins.Deals.Background
{
    public class BackgroundDealCheck
    {
        HCoreContext _HCoreContext;
        MDDealGallery _MDDealGallery;

        public void ManageDealStatus()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                    var ActiveDeals = _HCoreContext.MDDeal.Where(x => x.EndDate < CurrentTime && (x.StatusId == HelperStatus.Deals.Published || x.StatusId == HelperStatus.Deals.Approved)).ToList();
                    if (ActiveDeals.Count > 0)
                    {
                        foreach (var ActiveDeal in ActiveDeals)
                        {
                            ActiveDeal.VisiblityStatusId = 0;
                            if(!string.IsNullOrEmpty(ActiveDeal.ApproverComment))
                            {
                                ActiveDeal.ApproverComment = "Deal auto expire";
                            }
                            else
                            {
                                ActiveDeal.ApproverComment += " Deal auto expire";
                            }
                            ActiveDeal.StatusId = HelperStatus.Deals.Expired;
                            ActiveDeal.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                    }

                    DateTime CurrentTimeD = HCoreHelper.GetGMTDateTime();
                    var PublishedDeals = _HCoreContext.MDDeal.Where(x => CurrentTimeD >= x.StartDate && CurrentTimeD <= x.EndDate && x.StatusId == HelperStatus.Deals.Published && x.PosterStorageId != null).ToList();
                    if (PublishedDeals.Count > 0)
                    {
                        foreach (var PublishedDeal in PublishedDeals)
                        {
                            PublishedDeal.VisiblityStatusId = 1;
                        }
                    }

                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageDealStatus", _Exception, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        public void ManageFlashDealStatus()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();

                    var ActiveFlashDeals = _HCoreContext.MDFlashDeal.Where(x => x.EndDate < CurrentTime && x.StatusId == HelperStatus.Default.Active).ToList();
                    if (ActiveFlashDeals.Count > 0)
                    {
                        foreach (var ActiveFlashDeal in ActiveFlashDeals)
                        {
                            ActiveFlashDeal.StatusId = HelperStatus.Default.Blocked;
                            ActiveFlashDeal.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                    }
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageFlashDealStatus", _Exception, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        public void ManageDealCodeStatus()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();

                    var ActiveDealCodes = _HCoreContext.MDDealCode.Where(x => x.EndDate < CurrentTime && x.StatusId == HelperStatus.DealCodes.Unused).ToList();
                    if (ActiveDealCodes.Count > 0)
                    {
                        foreach (var ActiveDealCode in ActiveDealCodes)
                        {
                            ActiveDealCode.StatusId = HelperStatus.DealCodes.Expired;
                            ActiveDealCode.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                    }
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageDealCodeStatus", _Exception, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        public void ManagePromotionStatus()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();

                    var ActiveDealPromotions = _HCoreContext.MDDealPromotion.Where(x => x.EndDate < CurrentTime && x.StatusId == HelperStatus.Default.Active).ToList();
                    if (ActiveDealPromotions.Count > 0)
                    {
                        foreach (var ActiveDealPromotion in ActiveDealPromotions)
                        {
                            ActiveDealPromotion.StatusId = HelperStatus.Default.Blocked;
                            ActiveDealPromotion.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                    }

                    var ActiveAppPromotions = _HCoreContext.TUCAppPromotion.Where(x => x.EndDate < CurrentTime && x.StatusId == HelperStatus.Default.Active).ToList();
                    if (ActiveAppPromotions.Count > 0)
                    {
                        foreach (var ActiveAppPromotion in ActiveAppPromotions)
                        {
                            ActiveAppPromotion.StatusId = HelperStatus.Default.Blocked;
                            ActiveAppPromotion.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                    }

                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManagePromotionStatus", _Exception, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        public void ManagePromoCodeStatus()
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();

                    var ActivePromoCodes = _HCoreContext.TUCPromoCode.Where(x => x.EndDate < CurrentTime && x.StatusId == HelperStatus.Default.Active).ToList();
                    if (ActivePromoCodes.Count > 0)
                    {
                        foreach (var ActivePromoCode in ActivePromoCodes)
                        {
                            ActivePromoCode.StatusId = HelperStatus.Default.Blocked;
                            ActivePromoCode.ModifyDate = HCoreHelper.GetGMTDateTime();
                        }
                    }
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManagePromoCodeStatus", _Exception, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        public async Task UploadDealImage(ODeal.Manage.UploadImageRequest _Request)
        {
            try
            {
                if (_Request.Images != null && _Request.Images.Count > 0)
                {
                    foreach (var Image in _Request.Images)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            long? StorageId = 0;
                            if (Image.ImageContent.Content != null || !(string.IsNullOrEmpty(Image.ImageContent.Content)))
                            {
                                StorageId = HCoreHelper.SaveStorage(Image.ImageContent.Name, Image.ImageContent.Extension, Image.ImageContent.Content, null, _Request.UserReference);
                            }
                            if (StorageId != null && StorageId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (Image.IsDefault == 1)
                                    {
                                        var DealDetails = await _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).FirstOrDefaultAsync();
                                        if (DealDetails != null)
                                        {
                                            DealDetails.PosterStorageId = StorageId;
                                        }
                                    }

                                    _MDDealGallery = new MDDealGallery();
                                    _MDDealGallery.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealGallery.DealId = (long)_Request.DealId;
                                    _MDDealGallery.ImageStorageId = (long)StorageId;
                                    _MDDealGallery.IsDefault = (int)Image.IsDefault;
                                    _MDDealGallery.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _MDDealGallery.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.MDDealGallery.AddAsync(_MDDealGallery);
                                    await _HCoreContext.SaveChangesAsync();
                                    await _HCoreContext.DisposeAsync();
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception _Exception)
            {
                HCoreHelper.LogException("UploadDealImage", _Exception);
            }
        }
    }
}