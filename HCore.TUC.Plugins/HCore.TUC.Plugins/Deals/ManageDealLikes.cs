//==================================================================================
// FileName: ManageDealLikes.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Framework;
using HCore.TUC.Plugins.Deals.Object;

namespace HCore.TUC.Plugins.Deals
{
    public class ManageDealLikes
    {
        FrameworkDealLikes _FrameworkDealLikes;

        /// <summary>
        /// Description: Gets the deallikes.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeallikes(OList.Request _Request)
        {
            _FrameworkDealLikes = new FrameworkDealLikes();
            return _FrameworkDealLikes.GetDeallikes(_Request);
        }
    }
}