//==================================================================================
// FileName: ManageBackgroundDealCheck.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Background;
using HCore.TUC.Plugins.Deals.Notification;
using HCore.TUC.Plugins.Deals.Object;

namespace HCore.TUC.Plugins.Deals
{
    public class ManageBackgroundDealCheck
    {
        public void ManageDealStatus()
        {
            #region Manage Exception
            try
            {
                var system = ActorSystem.Create("ActorManageDealStatus");
                var greeter = system.ActorOf<ActorManageDealStatus>("ActorManageDealStatus");
                greeter.Tell("dealstatuscheck");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageDealStatus", _Exception, null);
            }
            #endregion
        }

        public void ManageFlashDealStatus()
        {
            #region Manage Exception
            try
            {
                var system = ActorSystem.Create("ActorManageFlashDealStatus");
                var greeter = system.ActorOf<ActorManageFlashDealStatus>("ActorManageFlashDealStatus");
                greeter.Tell("flashdealstatuscheck");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageFlashDealStatus", _Exception, null);
            }
            #endregion
        }

        public void ManageDealCodeStatus()
        {
            #region Manage Exception
            try
            {
                var system = ActorSystem.Create("ActorManageDealCodeStatus");
                var greeter = system.ActorOf<ActorManageDealCodeStatus>("ActorManageDealCodeStatus");
                greeter.Tell("dealcodestatuscheck");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManageDealCodeStatus", _Exception, null);
            }
            #endregion
        }

        public void ManagePromotionStatus()
        {
            #region Manage Exception
            try
            {
                var system = ActorSystem.Create("ActorManagePromotionStatus");
                var greeter = system.ActorOf<ActorManagePromotionStatus>("ActorManagePromotionStatus");
                greeter.Tell("promotionstatuscheck");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManagePromotionStatus", _Exception, null);
            }
            #endregion
        }

        public void ManagePromoCodeStatus()
        {
            #region Manage Exception
            try
            {
                var system = ActorSystem.Create("ActorManagePromoCodeStatus");
                var greeter = system.ActorOf<ActorManagePromoCodeStatus>("ActorManagePromoCodeStatus");
                greeter.Tell("promocodestatuscheck");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ManagePromoCodeStatus", _Exception, null);
            }
            #endregion
        }

        public void DealCodeExpireNotification()
        {
            try
            {
                var system = ActorSystem.Create("ActorDealCodeExpireNotification");
                var greeter = system.ActorOf<ActorDealCodeExpireNotification>("ActorDealCodeExpireNotification");
                greeter.Tell("ActorDealCodeExpireNotification");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DealCodeExpireNotification", _Exception, null);
            }
        }
    }

    internal class ActorManageDealStatus : ReceiveActor
    {
        public ActorManageDealStatus()
        {
            Receive<string>(_Request =>
            {
                BackgroundDealCheck _BackgroundDealCheck = new BackgroundDealCheck();
                _BackgroundDealCheck.ManageDealStatus();
            });
        }
    }

    internal class ActorDealCodeExpireNotification : ReceiveActor
    {
        public ActorDealCodeExpireNotification()
        {
            ReceiveAsync<string>(async _Request =>
            {
                DealNotification _DealNotification = new DealNotification();
                await _DealNotification.DealCodeExpireNotification();
                await _DealNotification.DealCodeExpireNotificationAdmin();
            });
        }
    }

    internal class ActorUploadDealImage : ReceiveActor
    {
        public ActorUploadDealImage()
        {
            ReceiveAsync<ODeal.Manage.UploadImageRequest>(async _Request =>
            {
                BackgroundDealCheck _BackgroundDealCheck = new BackgroundDealCheck();
                await _BackgroundDealCheck.UploadDealImage(_Request);
            });
        }
    }

    internal class ActorManageFlashDealStatus : ReceiveActor
    {
        public ActorManageFlashDealStatus()
        {
            Receive<string>(_Request =>
            {
                BackgroundDealCheck _BackgroundDealCheck = new BackgroundDealCheck();
                _BackgroundDealCheck.ManageFlashDealStatus();
            });
        }
    }

    internal class ActorManageDealCodeStatus : ReceiveActor
    {
        public ActorManageDealCodeStatus()
        {
            Receive<string>(_Request =>
            {
                BackgroundDealCheck _BackgroundDealCheck = new BackgroundDealCheck();
                _BackgroundDealCheck.ManageDealCodeStatus();
            });
        }
    }

    internal class ActorManagePromotionStatus : ReceiveActor
    {
        public ActorManagePromotionStatus()
        {
            Receive<string>(_Request =>
            {
                BackgroundDealCheck _BackgroundDealCheck = new BackgroundDealCheck();
                _BackgroundDealCheck.ManagePromotionStatus();
            });
        }
    }

    internal class ActorManagePromoCodeStatus : ReceiveActor
    {
        public ActorManagePromoCodeStatus()
        {
            Receive<string>(_Request =>
            {
                BackgroundDealCheck _BackgroundDealCheck = new BackgroundDealCheck();
                _BackgroundDealCheck.ManagePromoCodeStatus();
            });
        }
    }
}
