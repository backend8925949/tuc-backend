//==================================================================================
// FileName: ManageDealOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Framework;
using HCore.TUC.Plugins.Deals.Object;

namespace HCore.TUC.Plugins.Deals
{
    public class ManageDealOperation
    {
        FrameworkDealOperations _FrameworkDealOperations;
        /// <summary>
        /// Description: Gets the deal merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealMerchants(OList.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetDealMerchants(_Request);
        }
        /// <summary>
        /// Description: Gets the deal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(OList.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the deal details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(OReference _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetDeal(_Request);
        }
        /// <summary>
        /// Description: Saves the deal view.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveDealView(OReference _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.SaveDealView(_Request);
        }
        /// <summary>
        /// Description: Updates the deal comment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDealComment(OReference _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.UpdateDealComment(_Request);
        }

        /// <summary>
        /// Description: Buydeal initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyDeal_Initialize(ODealOperation.DealPayment.Initialize _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.BuyDeal_Initialize(_Request);
        }
        /// <summary>
        /// Description: Buydeal confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyDeal_Confirm(ODealOperation.DealPayment.Confirm _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.BuyDeal_Confirm(_Request);
        }


        /// <summary>
        /// Description: Buydeal initialize v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyDeal_InitializeV2(ODealOperation.DealPurchase.Initialize.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.BuyDeal_InitializeV2(_Request);
        }
        /// <summary>
        /// Description: Buydeal confirm v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse BuyDeal_ConfirmV2(ODealOperation.DealPurchase.Confirm.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.BuyDeal_ConfirmV2(_Request);
        }


        /// <summary>
        /// Description: Gets the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealCode(OList.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetDealCode(_Request);
        }
        /// <summary>
        /// Description: Gets the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealCode(ODealOperation.DealCode.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetDealCode(_Request);
        }
        /// <summary>
        /// Description: Gets the product deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductDeal(OList.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetProductDeal(_Request);
        }

        /// <summary>
        /// Description: Deal redeem initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DealRedeem_Initialize(OReference _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.DealRedeem_Initialize(_Request);
        }
        /// <summary>
        /// Description: Deal redeem confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DealRedeem_Confirm(OReference _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.DealRedeem_Confirm(_Request);
        }
        /// <summary>
        /// Description: Shares the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ShareDealCode(ODealOperation.DealCode.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.ShareDealCode(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant redeem history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantRedeemHistory(OList.Request _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetMerchantRedeemHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant deal code details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantDealCodeDetails(OReference _Request)
        {
            _FrameworkDealOperations = new FrameworkDealOperations();
            return _FrameworkDealOperations.GetMerchantDealCodeDetails(_Request);
        }
    }
}
