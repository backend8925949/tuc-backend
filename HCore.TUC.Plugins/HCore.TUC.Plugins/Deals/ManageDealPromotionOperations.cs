//==================================================================================
// FileName: ManageDealPromotionOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Deals.Framework;
using HCore.TUC.Plugins.Deals.Object;

namespace HCore.TUC.Plugins.Deals
{
    public class ManageDealPromotionOperations
    {
        FrameworkDealPromotion _FrameworkDealPromotion;

        /// <summary>
        /// Description: Saves the deal promotion.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveDealPromotion(ODealPromotion.SaveDealPromotion.Request _Request)
        {
            _FrameworkDealPromotion = new FrameworkDealPromotion();
            return _FrameworkDealPromotion.SaveDealPromotion(_Request);
        }
        /// <summary>
        /// Description: Updates the deal promotion.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDealPromotion(ODealPromotion.UpdateDealPromotion _Request)
        {
            _FrameworkDealPromotion = new FrameworkDealPromotion();
            return _FrameworkDealPromotion.UpdateDealPromotion(_Request);
        }
        /// <summary>
        /// Description: Deletes the deal promotion.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteDealPromotion(OReference _Request)
        {
            _FrameworkDealPromotion = new FrameworkDealPromotion();
            return _FrameworkDealPromotion.DeleteDealPromotion(_Request);
        }
        /// <summary>
        /// Description: Gets the deal promotion details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealPromotionDetails(OReference _Request)
        {
            _FrameworkDealPromotion = new FrameworkDealPromotion();
            return _FrameworkDealPromotion.GetDealPromotionDetails(_Request);
        }
        /// <summary>
        /// Description: Gets the deal promotion list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealPromotionList(OList.Request _Request)
        {
            _FrameworkDealPromotion = new FrameworkDealPromotion();
            return _FrameworkDealPromotion.GetDealPromotionList(_Request);
        }
        /// <summary>
        /// Description: Removes the promotion image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RemovePromotionImage(ODealPromotion.Image _Request)
        {
            _FrameworkDealPromotion = new FrameworkDealPromotion();
            return _FrameworkDealPromotion.RemovePromotionImage(_Request);
        }
    }
}
