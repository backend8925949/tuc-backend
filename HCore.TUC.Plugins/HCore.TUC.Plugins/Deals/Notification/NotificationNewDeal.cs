//==================================================================================
// FileName: NotificationNewDeal.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using Akka.Actor;
using Delivery.Object.Response.Shipments;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Object;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using SendGrid.Helpers.Mail;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Plugins.Deals.Framework.FrameworkDealOperations;
using static HCore.TUC.Plugins.Deals.Object.OEmailProcessor;

namespace HCore.TUC.Plugins.Deals.Notification
{

    public class NotificationNewDealActor : ReceiveActor
    {
        HCoreContext? _HCoreContext;
        public NotificationNewDealActor()
        {
            Receive<long>(_DealId =>
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var DealInformation = _HCoreContext.MDDeal.Where(x => x.Id == _DealId)
                    .Select(x=> new
                    {
                        Id = x.Id,
                        Guid = x.Guid,
                        StatusId = x.StatusId,
                        PosterStorageId = x.PosterStorageId,
                        PosterUrl = x.PosterStorage.Path,
                        Title = x.Title,
                        Description = x.Description,
                    }).FirstOrDefault();
                    if (DealInformation != null)
                    {
                        if (DealInformation.StatusId == HelperStatus.Deals.Published)
                        {
                            //DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                            //if (DealInformation.StartDate < CurrentTime)
                            //{
                                string ImageUrl = null;
                                if (DealInformation.PosterStorageId != null)
                                {
                                    ImageUrl = _AppConfig.StorageUrl + DealInformation.PosterUrl;
                                }
                                if (HostEnvironment ==  HostEnvironmentType.Live)
                                {
                                    HCoreHelper.SendPushToTopic("tuccustomer_test", "deal", DealInformation.Title, DealInformation.Description, "deal", DealInformation.Id, DealInformation.Guid, "View details", ImageUrl);
                                    HCoreHelper.SendPushToTopic("tuccustomer", "deal", DealInformation.Title, DealInformation.Description, "deal", DealInformation.Id, DealInformation.Guid, "View details", ImageUrl);
                                    //HCoreHelper.SendPushToTopic("tuccustomer", "deal", DealInformation.Title, DealInformation.Description, "deal", DealInformation.Id, DealInformation.Guid, "View details", ImageUrl);
                                }
                                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == 7464 && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                if (!string.IsNullOrEmpty(UserNotificationUrl))
                                {
                                    if (HostEnvironment == HostEnvironmentType.Live)
                                    {
                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "deal", DealInformation.Title, DealInformation.Description, "deal", DealInformation.Id, DealInformation.Guid, "View details", true, null, ImageUrl);
                                    }
                                    else
                                    {
                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "deal", "TEST : "+ DealInformation.Title, DealInformation.Description, "deal", DealInformation.Id, DealInformation.Guid, "View details", true, null, ImageUrl);
                                    }
                                }
                            //}
                        }
                    }
                    _HCoreContext.Dispose();
                }
            });
        }
    }

    public class DealNotification
    {
        HCoreContext _HCoreContext;
        long expiringDealsCount = 0;
        MemoryStream memoryStream = new MemoryStream();
        string base64String = "";
        public async Task DealCodeExpireNotification()
        {
            try
            {
                string filePath = "";
                //var filePath = "C:/Projects/ExpiredUploads" + DateTime.Now.ToString();
                DateTime ExpiryDate = HCoreHelper.GetGMTDateTime().AddDays(3);
                DateTime StartDate = ExpiryDate.AddHours(-24);
                DateTime EndDate = StartDate.AddHours(24);

                using (_HCoreContext = new HCoreContext())
                {

                    //var cds = await _HCoreContext.MDDealCode.Where(x => x.StatusId == HelperStatus.DealCodes.Unused && (x.EndDate <= ExpiryDate)).ToListAsync();

                    var Accounts = await _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.MDDealCodeAccount.Any()).ToListAsync();
                    if (Accounts.Count > 0)
                    {
                        foreach (var Account in Accounts)
                        {
                            var DealCodes = await _HCoreContext.MDDealCode.Where(x => x.StatusId == HelperStatus.DealCodes.Unused && (x.EndDate <= ExpiryDate) && x.AccountId == Account.Id).ToListAsync();

                            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                            if (DealCodes.Count > 0)
                            {

                                string? TermsUrl = "";
                                string? PolicyUrl = "";
                                string? PanelUrl = "";
                                if (HostEnvironment == HostEnvironmentType.Dev)
                                {
                                    TermsUrl = "https://deals.thankucash.dev/terms";
                                    PolicyUrl = "https://deals.thankucash.dev/refundpolicy";
                                    PanelUrl = "https://deals.thankucash.dev";
                                }
                                else if (HostEnvironment == HostEnvironmentType.Tech)
                                {
                                    TermsUrl = "https://deals.thankucash.tech/terms";
                                    PolicyUrl = "https://deals.thankucash.tech/refundpolicy";
                                    PanelUrl = "https://deals.thankucash.tech";
                                }
                                else if (HostEnvironment == HostEnvironmentType.Test)
                                {
                                    TermsUrl = "https://deals.thankucash.co/terms";
                                    PolicyUrl = "https://deals.thankucash.co/refundpolicy";
                                    PanelUrl = "https://deals.thankucash.co";
                                }
                                else
                                {
                                    TermsUrl = "https://deals.thankucash.com/terms";
                                    PolicyUrl = "https://deals.thankucash.com/refundpolicy";
                                    PanelUrl = "https://deals.thankucash.com";
                                }

                                OEmailProcessor.DealCodeExpire _Request = new OEmailProcessor.DealCodeExpire();
                                _Request.UserDisplayName = Account.DisplayName;
                                _Request.EmailAddress = Account.EmailAddress;
                                _Request.TermsUrl = TermsUrl;
                                _Request.PolicyUrl = PolicyUrl;
                                _Request.PanelUrl = PanelUrl;

                                string? DealCodesList = "";
                                DealCodesList = DealCodesList + "<!DOCTYPE html>";
                                DealCodesList = DealCodesList + "<html>";
                                DealCodesList = DealCodesList + "<body>";
                                DealCodesList = DealCodesList + "<table style='border:1px solid black; width:100%'>";
                                DealCodesList = DealCodesList + "<tr>";
                                DealCodesList = DealCodesList + "<th style='border:1px solid black;'>Deal code</th>";
                                DealCodesList = DealCodesList + "<th style='border:1px solid black;'>Merchant Number</th>";
                                DealCodesList = DealCodesList + "<th style='border: 1px solid black;'>Merchant’s Store Address</th>";
                                DealCodesList = DealCodesList + "</tr>";

                                List<OEmailProcessor.DealCodeDetails>? _DealCodes = new List<OEmailProcessor.DealCodeDetails>();
                                foreach (var DealCode in DealCodes)
                                {
                                    var UserDetails = await _HCoreContext.MDDealCode.Where(x => x.Id == DealCode.Id && x.Guid == DealCode.Guid).Select(x => new
                                    {
                                        ExpiryDate = x.EndDate,
                                        MerchantDisplayName = x.Deal.Account.DisplayName,
                                        MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                        MerchantMobileNumber = x.Deal.Account.MobileNumber,
                                        MerchantAddress = x.Deal.Account.Address,
                                        DealCode = x.ItemCode
                                    }).FirstOrDefaultAsync();

                                    _DealCodes.Add(new OEmailProcessor.DealCodeDetails
                                    {
                                        MerchantDisplayName = UserDetails.MerchantDisplayName,
                                        MerchantIconUrl = _AppConfig.StorageUrl + UserDetails.MerchantIconUrl,
                                        MerchantMobileNumber = UserDetails.MerchantMobileNumber,
                                        MerchantAddress = UserDetails.MerchantAddress,
                                        DealCode = UserDetails.DealCode
                                    });
                                }
                                foreach (var DealCode in _DealCodes)
                                {
                                    DealCodesList = DealCodesList + "<tr>";
                                    DealCodesList = DealCodesList + "<th style='border:1px;'>" + DealCode.DealCode + "</th>";
                                    DealCodesList = DealCodesList + "<th style='border:1px;'>" + DealCode.MerchantMobileNumber;
                                    DealCodesList = DealCodesList + "<th style='border:1px;'>" + DealCode.MerchantAddress + "</th>";
                                    DealCodesList = DealCodesList + "</tr>";
                                }

                                DealCodesList = DealCodesList + "</table>";
                                DealCodesList = DealCodesList + "</body>";
                                DealCodesList = DealCodesList + "</html>";
                                DealCodesList = DealCodesList + "<br>";
                                DealCodesList = DealCodesList + "<br>";
                                _Request.DealCodes = DealCodesList;

                                var system = ActorSystem.Create("ActorDealCodeExpireEmailNotification");
                                var greeter = system.ActorOf<ActorDealCodeExpireEmailNotification>("ActorDealCodeExpireEmailNotification");
                                greeter.Tell(_Request);
                            }

                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DealCodeExpireNotification", _Exception);
            }
        }

        internal async Task DealCodeExpireEmailNotification(OEmailProcessor.DealCodeExpire _Request)
        {
            try
            {
                if (!string.IsNullOrEmpty(_Request.UserDisplayName) || !string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail("d-b9c2922c5ce04ce283bffc361a819566", _Request.UserDisplayName, _Request.EmailAddress, _Request, null);

                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DealCodeExpireEmailNotification", _Exception, null);
            }
        }



        public async Task DealCodeExpireNotificationAdmin()
        {
            try
            {
                string filePath = "";
                //var filePath = "C:/Projects/ExpiredUploads" + DateTime.Now.ToString();
                DateTime ExpiryDate = HCoreHelper.GetGMTDateTime().AddDays(3);
                DateTime StartDate = ExpiryDate.AddHours(-24);
                DateTime EndDate = StartDate.AddHours(24);

                using (_HCoreContext = new HCoreContext())
                {

                    var DealCodes = await _HCoreContext.MDDealCode.Where(x => x.StatusId == HelperStatus.DealCodes.Unused && (x.EndDate <= ExpiryDate)).ToListAsync();
                    long expiringDealsCount = DealCodes.Count();

                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                    if (DealCodes.Count() > 0)
                    {

                        OEmailProcessor.DealCodeExpire _Request2 = new OEmailProcessor.DealCodeExpire();
                        _Request2.UserDisplayName = "DealDay";
                        _Request2.EmailAddress = "dealday@thankucash.com";
                        _Request2.PanelUrl = base64String;
                        var system2 = ActorSystem.Create("ActorDealCodeExpireAdminEmailNotification");
                        var greeter2 = system2.ActorOf<ActorDealCodeExpireAdminEmailNotification>("ActorDealCodeExpireAdminEmailNotification");
                        greeter2.Tell(_Request2);
                    }




                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DealCodeExpireNotification", _Exception);
            }
        }


        internal async Task DealCodeExpireEmailNotificationAdminSummary(OEmailProcessor.DealCodeExpire _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    long ActiveMerchantCount = await _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant && x.StatusId == 2 && x.HCUAccountParameterAccount.Any(a => a.TypeId == HelperType.ThankUCashDeals)).CountAsync();
                    long ProductDealCount = await _HCoreContext.MDDeal.Where(x => x.DealTypeId == DealType.ProductDeal && x.StatusId.ToString() == "616").CountAsync();
                    long ServiceDealCount = await _HCoreContext.MDDeal.Where(x => x.DealTypeId == DealType.ServiceDeal && x.StatusId.ToString() == "616").CountAsync();
                    if (!string.IsNullOrEmpty(_Request.UserDisplayName) || !string.IsNullOrEmpty(_Request.EmailAddress))
                    {

                        var parameters = new DealSummaryReports()
                        {
                            DateTime = DateTime.Now,
                            ProductDealCount = ProductDealCount,
                            ServiceDealCount = ServiceDealCount,
                            ActiveMerchantCount = ActiveMerchantCount
                        };

                        HCoreHelper.BroadCastEmail("d-ae98094b634543b5a74cb7c93d911d44", "DealDay", "dealday@thankucash.com", parameters, null);

                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DealCodeExpireEmailNotificationAdminSummary", _Exception, null);
            }
        }

        internal async Task DealCodeExpireEmailNotificationAdminAttachment(OEmailProcessor.DealCodeExpire _Request)
        {
            try
            {

                DateTime ExpiryDate = HCoreHelper.GetGMTDateTime().AddDays(3);
                DateTime StartDate = ExpiryDate.AddHours(-24);
                DateTime EndDate = StartDate.AddHours(24);



                using (_HCoreContext = new HCoreContext())
                {
                    var DealCodes = await _HCoreContext.MDDealCode.Where(x => x.StatusId == HelperStatus.DealCodes.Unused && (x.EndDate <= ExpiryDate)).ToListAsync();
                    string filePath = "";
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

                    if (DealCodes.Count() > 0)
                    {
                        //Write data into excelfile
                        using (ExcelPackage excel = new ExcelPackage())
                        {
                            var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                            workSheet.Cells["A1"].LoadFromCollection(DealCodes, true);
                            workSheet.DeleteColumn(2);
                            workSheet.DeleteColumn(1);
                            workSheet.DeleteColumn(12, 53);
                            var columnI = workSheet.Cells["G:G"];
                            columnI.Style.Numberformat.Format = "yyyy-MM-dd HH:mm:ss";
                            var columnJ = workSheet.Cells["H:H"];
                            columnJ.Style.Numberformat.Format = "yyyy-MM-dd HH:mm:ss";
                            var columnL = workSheet.Cells["J:J"];
                            columnL.Style.Numberformat.Format = "yyyy-MM-dd HH:mm:ss";
                            string currentDirectory = Directory.GetCurrentDirectory();
                            string fileName = "ExpiredDeals" + DateTime.Now.Ticks.ToString() + ".csv";
                            filePath = Path.Combine(currentDirectory, fileName);
                            FileInfo file = new FileInfo(filePath);

                            excel.SaveAs(file);
                            Byte[] bytes = File.ReadAllBytes(filePath);
                            base64String = Convert.ToBase64String(bytes);

                        }

                    }

                    if (!string.IsNullOrEmpty(_Request.UserDisplayName) || !string.IsNullOrEmpty(_Request.EmailAddress))
                    {

                        expiringDealsCount = DealCodes.Count();
                        var countParameters = new DealExpiryDetails()
                        {
                            ExpiryDate = DateTime.Now.AddDays(3).ToString("dd-MM-yyyy"),
                            DealCount = expiringDealsCount,
                        };
                        HCoreHelper.BroadCastEmail("d-da0870de7e61419d8573a57d39c98dca", "DealDay", "dealday@thankucash.com", countParameters, _Request.UserReference, AttachmentBase64String: base64String, AttachmentName: "DealExpiryDetails s" + DateTime.Now.ToString());

                    }
                }



            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DealCodeExpireEmailNotificationAdminAttachment", _Exception, null);
            }
        }




    }

    internal class ActorDealCodeExpireNotification : ReceiveActor
    {
        public ActorDealCodeExpireNotification()
        {
            ReceiveAsync<string>(async _Request =>
            {
                DealNotification _DealNotification = new DealNotification();
                await _DealNotification.DealCodeExpireNotification();
                await _DealNotification.DealCodeExpireNotificationAdmin();


            });
        }
    }

    internal class ActorDealCodeExpireEmailNotification : ReceiveActor
    {
        public ActorDealCodeExpireEmailNotification()
        {
            ReceiveAsync<OEmailProcessor.DealCodeExpire>(async _Request =>
            {
                DealNotification _DealNotification = new DealNotification();
                await _DealNotification.DealCodeExpireEmailNotification(_Request);

            });
        }
    }

    internal class ActorDealCodeExpireAdminEmailNotification : ReceiveActor
    {
        public ActorDealCodeExpireAdminEmailNotification()
        {
            ReceiveAsync<OEmailProcessor.DealCodeExpire>(async _Request =>
            {
                DealNotification _DealNotification = new DealNotification();
                await _DealNotification.DealCodeExpireEmailNotificationAdminSummary(_Request);
                await _DealNotification.DealCodeExpireEmailNotificationAdminAttachment(_Request);
            });
        }
    }
}
