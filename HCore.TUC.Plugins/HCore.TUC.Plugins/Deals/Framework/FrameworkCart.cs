//==================================================================================
// FileName: FrameworkCart.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to carts
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using System.Collections.Generic;
using System.Linq;
using HCore.TUC.Plugins.Deals.Resource;
using HCore.TUC.Plugins.Deals.Object;
using static HCore.Helper.HCoreConstant;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;

namespace HCore.TUC.Plugins.Deals.Framework
{
    public class FrameworkCart
    {
        HCoreContext _HCoreContext;
        MDDealCart _MDDealCart;

        /// <summary>
        /// Description: Saves the cart.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal async Task<OResponse> SaveCart(OCart.Save.Request _Request)
        {
            try
            {
                // Check required parameter are in the request or not.
                if (_Request.AccountId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0206", ResponseCode.HCP0206);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0206", ResponseCode.HCP0207);
                }
                if (_Request.DealId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCP0204);
                }
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0205", ResponseCode.HCP0205);
                }
                if (_Request.ItemCount < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCP0212);
                }

                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    // Check if the deal exists and in running state or not.
                    var DealDetails = await _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey && x.StatusId == HelperStatus.Deals.Published).FirstOrDefaultAsync();
                    if(DealDetails != null)
                    {
                        // Check that deal is expired or not.
                        if (DealDetails.StatusId == HelperStatus.Deals.Expired)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0215);
                        }

                        // Check if maximum unit sale per person for the deal is less than item count in the request.
                        if (DealDetails.MaximumUnitSalePerPerson < _Request.ItemCount)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0213", ResponseCode.HCP0213);
                        }

                        long? MinPurchaseCount = 0;

                        // Get the count of the purchase made by the user.
                        long? PurchaseCount = _HCoreContext.MDDealCode.Where(x => x.DealId == DealDetails.Id && x.AccountId == _Request.AccountId).Count();
                        if(PurchaseCount > 0)
                        {
                            // Get the remaining purchase count that user can make.
                            MinPurchaseCount = DealDetails.MaximumUnitSalePerPerson - PurchaseCount;

                            // Check if the remaining purchase count for the user is zero or not.
                            if (MinPurchaseCount == 0)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                            }

                            // Check if remaining purchase count is less than item count in the request.
                            if (MinPurchaseCount < _Request.ItemCount)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0213", ResponseCode.HCP0213);
                            }
                        }

                        // Get the details of the item added to the cart.
                        var CartDetails = await _HCoreContext.MDDealCart.Where(x => x.DealId == DealDetails.Id && x.AccountId == _Request.AccountId).FirstOrDefaultAsync();
                        if (CartDetails != null)
                        {
                            long? AvailableCount = 0;
                            // Check if the item count of the deal added to the cart is greater than or equal to the maximum unit sale per person for the selected deal.
                            if (CartDetails.ItemCount >= DealDetails.MaximumUnitSalePerPerson)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                            }

                            // Get the available purchase count that user can make.
                            AvailableCount = DealDetails.MaximumUnitSalePerPerson - CartDetails.ItemCount;

                            // Check if available count is less than item count in the request or not.
                            if (AvailableCount < _Request.ItemCount)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                            }

                            // Check if the available count for the user is zero or not.
                            if (AvailableCount == 0)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                            }
                            else
                            {
                                // Add the count to the item count and save the changes.
                                if (CartDetails.ItemCount != null)
                                {
                                    CartDetails.ItemCount += (int?)_Request.ItemCount;
                                }
                                if(CartDetails.ItemCount == null)
                                {
                                    CartDetails.ItemCount = (int?)_Request.ItemCount;
                                }
                                CartDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                CartDetails.ModifyById = _Request.UserReference.AccountId;
                                await _HCoreContext.SaveChangesAsync();

                                var Response = new
                                {
                                    ReferenceId = CartDetails.Id,
                                    ReferenceKey = CartDetails.Guid
                                };

                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "HCP0216", ResponseCode.HCP0216);
                            }
                        }
                        else
                        {
                            // Add the item to the cart.
                            _MDDealCart = new MDDealCart();
                            _MDDealCart.Guid = HCoreHelper.GenerateGuid();
                            _MDDealCart.DealId = _Request.DealId;
                            _MDDealCart.AccountId = _Request.AccountId;
                            _MDDealCart.ItemCount = (int?)_Request.ItemCount;
                            _MDDealCart.StatusId = StatusId;
                            _MDDealCart.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDDealCart.CreatedById = _Request.UserReference.AccountId;
                            _HCoreContext.MDDealCart.Add(_MDDealCart);
                            await _HCoreContext.SaveChangesAsync();

                            var Response = new
                            {
                                ReferenceId = _MDDealCart.Id,
                                ReferenceKey = _MDDealCart.Guid
                            };

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "HCPSAVE", ResponseCode.HCPSAVE);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0208", ResponseCode.HCP0208);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveCart", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
        }

        /// <summary>
        /// Description: Updates the cart.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal async Task<OResponse> UpdateCart(OCart.Update.Request _Request)
        {
            try
            {
                // Check required parameter are in the request or not.
                if (_Request.AccountId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0206", ResponseCode.HCP0206);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0206", ResponseCode.HCP0207);
                }
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCDREF);
                }
                if(string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCDREFKEY);
                }
                if (_Request.ItemCount < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCP0212);
                }

                int StatusId = 0;
                if(!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if(StatusId < 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    // Get the details of the item added to the cart.
                    var CartDetails = await _HCoreContext.MDDealCart.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.DealId == _Request.DealId && x.AccountId == _Request.AccountId).FirstOrDefaultAsync();
                    if(CartDetails != null)
                    {

                        // Get the details of the deal.
                        var DealDetails = await _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey && x.StatusId == HelperStatus.Deals.Published)
                                          .Select(x => new
                                          {
                                              MaxPurchaseCount = x.MaximumUnitSalePerPerson,
                                              PurchaseCount = x.MDDealCode.Where(a => a.DealId == x.Id && a.AccountId == _Request.AccountId).Count(),
                                              StatusId = x.StatusId
                                          }).FirstOrDefaultAsync();
                        if(DealDetails != null)
                        {
                            long? MinPurchaseCount = 0;
                            long? AvailableCount = 0;

                            // Check that deal is expired or not.
                            if(DealDetails.StatusId == HelperStatus.Deals.Expired)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0215);
                            }

                            if (DealDetails.PurchaseCount > 0)
                            {
                                // Get the remaining purchase count that user can make.
                                MinPurchaseCount = DealDetails.MaxPurchaseCount - DealDetails.PurchaseCount;

                                // Get the available purchase count that user can make.
                                AvailableCount = MinPurchaseCount - CartDetails.ItemCount;

                                // Check if the remaining purchase count for the user is zero or not.
                                if (MinPurchaseCount == 0)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                                }

                                // Check if remaining purchase count is less than item count in the request.
                                if (MinPurchaseCount < _Request.ItemCount)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0213", ResponseCode.HCP0213);
                                }

                                // Check if max purchase count per person is less than item count in the request
                                if (DealDetails.MaxPurchaseCount < _Request.ItemCount)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0213", ResponseCode.HCP0213);
                                }

                                // Check if max purchase count per person is less than or equal to item count in the request
                                if (DealDetails.MaxPurchaseCount <= CartDetails.ItemCount)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                                }

                                // Check if available count is less than item count in the request or not.
                                if(AvailableCount < _Request.ItemCount)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                                }

                                // Check if the available count for the user is zero or not.
                                if (AvailableCount == 0)
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0214", ResponseCode.HCP0214);
                                }
                            }

                            if (StatusId > 0 && CartDetails.StatusId != StatusId)
                            {
                                CartDetails.StatusId = StatusId;
                            }

                            // Add the count to the item count and save the changes.
                            if(_Request.ItemCount > 0)
                            {
                                CartDetails.ItemCount += (int?)_Request.ItemCount;
                            }
                            CartDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            CartDetails.ModifyById = _Request.UserReference.AccountId;
                            await _HCoreContext.SaveChangesAsync();

                            var Response = new
                            {
                                ReferenceId = CartDetails.Id,
                                ReferenceKey = CartDetails.Guid
                            };

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Response, "HCPUPDATE", ResponseCode.HCPUPDATE);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0208", ResponseCode.HCP0208);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateCart", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
        }

        /// <summary>
        /// Description: Deletes the cart.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal async Task<OResponse> DeleteCart(OReference _Request)
        {
            try
            {
                // Check required parameter are in the request or not.
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCDREFKEY);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    // Get the details of the item added to the cart.
                    var CartDetails = await _HCoreContext.MDDealCart.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();

                    if (CartDetails != null)
                    {
                        // Remove the item fron the cart.
                        _HCoreContext.MDDealCart.Remove(CartDetails);
                        await _HCoreContext.SaveChangesAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCPDELETE", ResponseCode.HCPDELETE);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0209", ResponseCode.HCP0209);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteCart", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
        }

        /// <summary>
        /// Description: Gets the cart details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal async Task<OResponse> GetCart(OReference _Request)
        {
            try
            {
                // Check required parameter are in the request or not.
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0204", ResponseCode.HCDREFKEY);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    // Get the details of the item added to the cart.
                    var CartDetails = await _HCoreContext.MDDealCart.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                      .Select(x => new OCart.Details
                                      {
                                          ReferenceId = x.Id,
                                          ReferenceKey = x.Guid,
                                          AccountId = x.AccountId,
                                          AccountKey = x.Account.Guid,
                                          AccountDisplayName = x.Account.DisplayName,
                                          AccountEmailAddress = x.Account.EmailAddress,
                                          AcountMobileNumber = x.Account.MobileNumber,
                                          AccountAddress = x.Account.Address,
                                          AccountIconUrl = x.Account.IconStorage.Path,
                                          DealId = x.DealId,
                                          DealKey = x.Deal.Guid,
                                          DealTitle = x.Deal.Title,
                                          DealPrice = x.Deal.SellingPrice,
                                          DealIconUrl = x.Deal.PosterStorage.Path,
                                          DealTypeId = x.Deal.DealTypeId,
                                          DealTypeCode = x.Deal.DiscountType.SystemName,
                                          DealTypeName = x.Deal.DealType.Name,
                                          CreateDate = x.CreateDate,
                                          CreatedBYId = x.CreatedById,
                                          CreatedByDisplayName = x.CreatedBy.DisplayName,
                                          ModifyDate = x.ModifyDate,
                                          ModifyById = x.ModifyById,
                                          ModifyByDisplayName = x.ModifyBy.DisplayName,
                                          StatusId = x.StatusId,
                                          StatusCode = x.Status.SystemName,
                                          StatusName = x.Status.Name,
                                          ItemCount = x.ItemCount,
                                          DealStatus = x.Deal.Status.SystemName,
                                      }).FirstOrDefaultAsync();
                    if(CartDetails != null)
                    {
                        if(!string.IsNullOrEmpty(CartDetails.AccountIconUrl))
                        {
                            CartDetails.AccountIconUrl = _AppConfig.StorageUrl + CartDetails.AccountIconUrl;
                        }
                        else
                        {
                            CartDetails.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(CartDetails.DealIconUrl))
                        {
                            CartDetails.DealIconUrl = _AppConfig.StorageUrl + CartDetails.DealIconUrl;
                        }
                        else
                        {
                            CartDetails.DealIconUrl = _AppConfig.Default_Icon;
                        }

                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, CartDetails, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCart", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
        }

        /// <summary>
        /// Description: Gets the cart list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal async Task<OResponse> GetCarts(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    // Get the total records of the items added to the cart.
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.MDDealCart
                                                .Select(x => new OCart.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountEmailAddress = x.Account.EmailAddress,
                                                    AcountMobileNumber = x.Account.MobileNumber,
                                                    AccountAddress = x.Account.Address,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,
                                                    DealTitle = x.Deal.Title,
                                                    DealPrice = x.Deal.SellingPrice,
                                                    DealIconUrl = x.Deal.PosterStorage.Path,
                                                    DealTypeId = x.Deal.DealTypeId,
                                                    DealTypeCode = x.Deal.DiscountType.SystemName,
                                                    DealTypeName = x.Deal.DealType.Name,
                                                    CreateDate = x.CreateDate,
                                                    CreatedBYId = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    ItemCount = x.ItemCount,
                                                    DealStatus = x.Deal.Status.SystemName,
                                                })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }

                    // Get the list of the items added to the cart.
                    List<OCart.List> Data = await _HCoreContext.MDDealCart
                                            .Select(x => new OCart.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                AccountId = x.AccountId,
                                                AccountKey = x.Account.Guid,
                                                AccountDisplayName = x.Account.DisplayName,
                                                AccountEmailAddress = x.Account.EmailAddress,
                                                AcountMobileNumber = x.Account.MobileNumber,
                                                AccountAddress = x.Account.Address,
                                                AccountIconUrl = x.Account.IconStorage.Path,
                                                DealId = x.DealId,
                                                DealKey = x.Deal.Guid,
                                                DealTitle = x.Deal.Title,
                                                DealPrice = x.Deal.SellingPrice,
                                                DealIconUrl = x.Deal.PosterStorage.Path,
                                                DealTypeId = x.Deal.DealTypeId,
                                                DealTypeCode = x.Deal.DiscountType.SystemName,
                                                DealTypeName = x.Deal.DealType.Name,
                                                CreateDate = x.CreateDate,
                                                CreatedBYId = x.CreatedById,
                                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                ModifyDate = x.ModifyDate,
                                                ModifyById = x.ModifyById,
                                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name,
                                                ItemCount = x.ItemCount,
                                                DealStatus = x.Deal.Status.SystemName,
                                            })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToListAsync();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                        {
                            DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                        }
                        else
                        {
                            DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.DealIconUrl))
                        {
                            DataItem.DealIconUrl = _AppConfig.StorageUrl + DataItem.DealIconUrl;
                        }
                        else
                        {
                            DataItem.DealIconUrl = _AppConfig.Default_Icon;
                        }
                    }

                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCDealPromotionResource.MD0200, TUCDealPromotionResource.MD0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetCarts", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
        }
    }
}

