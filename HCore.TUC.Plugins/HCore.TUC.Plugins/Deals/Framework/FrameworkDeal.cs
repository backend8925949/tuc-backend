//==================================================================================
// FileName: FrameworkDeal.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining deals related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//14 Sept 2022     | Umesh Sohaliya    : Deal & customer overview filter issue resolution 
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Mailerlite;
using HCore.Integration.Mailerlite.Requests;
using HCore.Integration.Mailerlite.Responses;
using HCore.Object;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Plugins.Deals.Notification;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.Delivery.Objects;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Plugins.Deals.Object.OEmailProcessor;

namespace HCore.TUC.Plugins.Deals.Framework
{
    public class FrameworkDeal
    {
        HCoreContext? _HCoreContext;
        MDDeal? _MDDeal;
        MDDealLocation? _MDDealLocation;
        MDDealShedule? _MDDealShedule;
        MDDealGallery? _MDDealGallery;
        List<ODeal.List>? _DealsList;
        List<OFlashDeal.List>? _FlashDealsList;
        LSPackages? _LSPackages;
        List<ODealCode.List>? _DealCodeList;
        List<ODealReview.List>? _DealReview;
        ODeal.Details? _DealDetails;
        ODeal.Overview? _DealOverview;
        ODealCode.Overview? _DealCodeOverview;
        MDFlashDeal? _MDFlashDeal;
        ODealCode.ValidateCode.Details? _DealCodeValidate;
        ODealCode.ValidateCode.Merchant? _DealCodeValidateMerchant;
        ODealCode.ValidateCode.Deal? _DealCodeValidateDeal;
        ODealCode.ValidateCode.Customer? _DealCodeValidateCustomer;
        OPackaging.Save.Request? _PackagingRequest;
        FrameworkPackaging? _FrameworkPackaging;
        ManageCoreTransaction? _ManageCoreTransaction;
        OCoreTransaction.Request? _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem>? _TransactionItems;

        /// <summary>
        /// Description: Validates the deal title.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ValidateDealTitle(ODeal.ValidateTitle.Request _Request)
        {
            #region Manage Exception 
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (_Request.Titles == null || _Request.Titles.Count < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0143", ResponseCode.HCP0143);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    foreach (var TitleItem in _Request.Titles)
                    {
                        var DealDetailsCheck = _HCoreContext.MDDeal
                       .Where(x => x.AccountId == _Request.AccountId && x.Title == TitleItem.Title)
                       .Select(x => new
                       {
                           ReferenceId = x.Id,
                           ReferenceKey = x.Guid,
                       }).FirstOrDefault();
                        if (DealDetailsCheck != null)
                        {
                            TitleItem.IsAvailable = false;
                            TitleItem.ReferenceId = DealDetailsCheck.ReferenceId;
                            TitleItem.ReferenceKey = DealDetailsCheck.ReferenceKey;
                        }
                        else
                        {
                            TitleItem.IsAvailable = true;
                        }
                    }
                    var Item = _Request;
                    Item.UserReference = null;
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Item, "HCP0101", ResponseCode.HCP0101);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ValidateDealTitle", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveDeal(ODeal.Manage.Request _Request)
        {
            //_ManageCoreTransaction = new ManageCoreTransaction();
            #region Manage Exception 
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0102", ResponseCode.HCP0102);
                }
                if (string.IsNullOrEmpty(_Request.CodeUsageTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0182", ResponseCode.HCP0182);
                }
                if (string.IsNullOrEmpty(_Request.TitleContent))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0102", ResponseCode.HCP0102);
                }
                if (string.IsNullOrEmpty(_Request.SubCategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", ResponseCode.HCP0183);
                }
                int? UsageTypeId = HCoreHelper.GetHelperId(_Request.CodeUsageTypeCode);
                int? DealTypeId = HCoreHelper.GetHelperId(_Request.DealTypeCode);
                int? DeliveryTypeId = HCoreHelper.GetHelperId(_Request.DeliveryTypeCode);
                int StatusId = HelperStatus.Deals.Draft;
                if (DealTypeId == Helpers.DealType.ProductDeal
                    && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || DeliveryTypeId == Helpers.DeliveryType.Delivery)
                    && _Request.Packaging == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", "Packaging information required");

                }
                if (DealTypeId == Helpers.DealType.ProductDeal
                    && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || DeliveryTypeId == Helpers.DeliveryType.Delivery)
                    && _Request.Packaging.Weight <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", "Packaging weight must be greater than 0");
                }
                if (DealTypeId == Helpers.DealType.ProductDeal
                   && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || DeliveryTypeId == Helpers.DeliveryType.Delivery)
                   && string.IsNullOrEmpty(_Request.Packaging.ParcelDescription))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", "Packaging details required");
                }

                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }
                if ((_Request.Images == null || _Request.Images.Count == 0) && (StatusId == HelperStatus.Deals.ApprovalPending || StatusId == HelperStatus.Deals.Published))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0156", ResponseCode.HCP0156);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    foreach (var item in _Request.Images)
                    {
                        if (!string.IsNullOrEmpty(item.reference))
                        {
                            long ImageId = _HCoreContext.HCCoreStorage.Where(x => x.Guid == item.reference).Select(x => x.Id).FirstOrDefault();
                            if (ImageId > 0)
                            {
                                item.referenceId = ImageId;
                            }
                        }
                    }
                    var CategoryDetails = _HCoreContext.MDCategory
                          .Where(x => x.RootCategory.Guid == _Request.SubCategoryKey)
                          .Select(x => new
                          {
                              SubCategoryId = x.RootCategory.Id,
                              CategoryId = x.RootCategory.ParentCategoryId,
                              Fee = x.Commission
                          })
                          .FirstOrDefault();
                    if (CategoryDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0184", ResponseCode.HCP0184);
                    }

                    var DealDetailsCheck = _HCoreContext.MDDeal
                        .Where(x => x.AccountId == _Request.AccountId && x.TitleContent == _Request.TitleContent)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                        }).FirstOrDefault();
                    if (DealDetailsCheck != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealDetailsCheck, "HCP0142", ResponseCode.HCP0142);
                    }

                    _MDDeal = new MDDeal();
                    _MDDeal.Guid = HCoreHelper.GenerateGuid();
                    _MDDeal.TypeId = 1;
                    _MDDeal.AccountId = _Request.AccountId;
                    _MDDeal.Title = _Request.Title;
                    _MDDeal.TitleContent = _Request.TitleContent;
                    _MDDeal.TitleTypeId = _Request.TitleTypeId;
                    _MDDeal.Description = _Request.Description;
                    _MDDeal.Terms = _Request.Terms;
                    _MDDeal.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                    _MDDeal.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    //_MDDeal.StartDate = _Request.StartDate;
                    //_MDDeal.EndDate = _Request.EndDate;
                    _MDDeal.CommissionAmount = HCoreHelper.GetPercentage(_Request.SellingPrice, CategoryDetails.Fee);
                    _MDDeal.CommissionPercentage = CategoryDetails.Fee;
                    if (_Request.UserReference.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Admin)
                    {
                        if (_Request.TUCPercentage > 0)
                        {
                            _MDDeal.CommissionAmount = HCoreHelper.GetPercentage(_Request.SellingPrice, _Request.TUCPercentage);
                            _MDDeal.CommissionPercentage = _Request.TUCPercentage;
                        }
                    }
                    _MDDeal.CategoryId = CategoryDetails.CategoryId;
                    _MDDeal.SubcategoryId = CategoryDetails.SubCategoryId;
                    _MDDeal.MaximumUnitSale = _Request.MaximumUnitSale;
                    _MDDeal.MaximumUnitSalePerDay = _Request.MaximumUnitSale;
                    _MDDeal.MaximumUnitSalePerPerson = _Request.MaximumUnitSalePerPerson;
                    _MDDeal.UsageInformation = _Request.CodeUsageInformation;
                    _MDDeal.Views = 0;
                    _MDDeal.Like = 0;
                    _MDDeal.DisLike = 0;
                    _MDDeal.TView = 0;
                    _MDDeal.TPurchase = 0;
                    _MDDeal.TLike = 0;
                    _MDDeal.TDislike = 0;
                    _MDDeal.SettlementTypeId = 637; // Postpaid
                    _MDDeal.IsPinRequired = 0;

                    _MDDeal.ActualPrice = _Request.ActualPrice;
                    _MDDeal.SellingPrice = _Request.SellingPrice;
                    _MDDeal.DiscountTypeId = 639; // Percentage 
                    _MDDeal.DiscountAmount = Math.Round((_Request.ActualPrice - _Request.SellingPrice), 2);
                    _MDDeal.DiscountPercentage = HCoreHelper.GetAmountPercentage((double)_MDDeal.DiscountAmount, _Request.ActualPrice);
                    _MDDeal.Amount = _Request.SellingPrice;
                    _MDDeal.Charge = 0;
                    _MDDeal.TotalAmount = _Request.SellingPrice;

                    if (!string.IsNullOrEmpty(_Request.Title))
                    {
                        string TitleSlug = HCoreHelper.GenerateSlug(_Request.Title);
                        bool SlugCheck = _HCoreContext.MDDeal.Any(x => x.Slug == TitleSlug);
                        if (!SlugCheck)
                        {
                            _MDDeal.Slug = TitleSlug;
                        }
                        else
                        {
                            DateTime SlugDate = HCoreHelper.ConvertToUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                            string NewSlug = TitleSlug + "-" + SlugDate.ToString("HH:mm:ss");
                            _MDDeal.Slug = NewSlug;
                        }
                    }

                    if (DealTypeId > 0)
                    {
                        _MDDeal.DealTypeId = DealTypeId;
                    }
                    else
                    {
                        _MDDeal.DealTypeId = Helpers.DealType.ServiceDeal;
                    }

                    if (DealTypeId == Helpers.DealType.ProductDeal)
                    {
                        if (DeliveryTypeId > 0)
                        {
                            _MDDeal.DeliveryTypeId = DeliveryTypeId;
                        }
                        else
                        {
                            _MDDeal.DealTypeId = Helpers.DeliveryType.InStorePickUp;
                        }
                    }

                    _MDDeal.UsageTypeId = (int)UsageTypeId;
                    if (UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase)
                    {
                        _MDDeal.CodeValidtyDays = _Request.CodeValidityHours;
                    }
                    else if (UsageTypeId == Helpers.DealCodeUsageType.DaysAfterPurchase)
                    {
                        _MDDeal.CodeValidtyDays = _Request.CodeValidityDays * 24;
                    }
                    else if (UsageTypeId == Helpers.DealCodeUsageType.DealEndDate)
                    {
                        _MDDeal.CodeValidityEndDate = _Request.EndDate;
                    }
                    else if (UsageTypeId == Helpers.DealCodeUsageType.ExpiresOnDate)
                    {
                        if (_Request.CodeValidityEndDate != null)
                        {
                            _MDDeal.CodeValidityEndDate = _Request.CodeValidityEndDate;
                        }
                        else
                        {
                            _MDDeal.CodeValidityEndDate = _Request.EndDate;
                        }
                    }
                    else
                    {
                        _MDDeal.CodeValidityEndDate = _Request.EndDate;
                    }

                    _MDDeal.CreateDate = HCoreHelper.GetGMTDateTime();
                    _MDDeal.CreatedById = _Request.UserReference.AccountId;
                    if (StatusId > 0)
                    {
                        _MDDeal.StatusId = StatusId;
                    }
                    else
                    {
                        _MDDeal.StatusId = HelperStatus.Deals.Published;
                    }

                    _HCoreContext.MDDeal.Add(_MDDeal);
                    _HCoreContext.SaveChanges();

                    if (_MDDeal.DealTypeId == Helpers.DealType.ProductDeal
                        && (_MDDeal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery
                        || _MDDeal.DeliveryTypeId == Helpers.DeliveryType.Delivery))
                    {
                        if (string.IsNullOrEmpty(_Request.Packaging.Name))
                        {
                            _Request.Packaging.Name = _Request.Title;
                        }
                        _FrameworkPackaging = new FrameworkPackaging();
                        _FrameworkPackaging.CreatePackage(_MDDeal.AccountId, _MDDeal.Id, _Request.Packaging.Name, _Request.Packaging.Weight, _Request.UserReference);
                    }

                    var _Response = new
                    {
                        StatusId = _MDDeal.StatusId,
                        ReferenceId = _MDDeal.Id,
                        ReferenceKey = _MDDeal.Guid
                    };
                    if (_Request.Locations != null && _Request.Locations.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Location in _Request.Locations)
                            {
                                _MDDealLocation = new MDDealLocation();
                                _MDDealLocation.Guid = HCoreHelper.GenerateGuid();
                                _MDDealLocation.DealId = _MDDeal.Id;
                                _MDDealLocation.LocationId = Location.ReferenceId;
                                _MDDealLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                                _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                _HCoreContext.MDDealLocation.Add(_MDDealLocation);
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    if (_Request.Shedule != null && _Request.Shedule.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Location in _Request.Shedule)
                            {
                                _MDDealShedule = new MDDealShedule();
                                _MDDealShedule.Guid = HCoreHelper.GenerateGuid();
                                if (Location.TypeCode == "dealredeemshedule")
                                {
                                    _MDDealShedule.TypeId = 634;
                                }
                                else
                                {
                                    _MDDealShedule.TypeId = 633;
                                }
                                _MDDealShedule.DealId = _MDDeal.Id;
                                _MDDealShedule.StartHour = Location.StartHour;
                                _MDDealShedule.EndHour = Location.EndHour;
                                _MDDealShedule.DayOfWeek = Location.DayOfWeek;
                                _MDDealShedule.CreateDate = HCoreHelper.GetGMTDateTime();
                                _MDDealShedule.CreatedById = _Request.UserReference.AccountId;
                                _MDDealShedule.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.MDDealShedule.Add(_MDDealShedule);
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    if (_Request.Images != null && _Request.Images.Count > 0)
                    {
                        _Request.Images.FirstOrDefault().IsDefault = 1;
                        foreach (var Image in _Request.Images)
                        {
                            if (Image.referenceId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (Image.IsDefault == 1)
                                    {
                                        var DealInfo = _HCoreContext.MDDeal.Where(x => x.Id == _MDDeal.Id).FirstOrDefault();
                                        if (DealInfo != null)
                                        {
                                            DealInfo.PosterStorageId = Image.referenceId;
                                        }
                                    }
                                    _MDDealGallery = new MDDealGallery();
                                    _MDDealGallery.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealGallery.DealId = _MDDeal.Id;
                                    _MDDealGallery.ImageStorageId = Image.referenceId;
                                    _MDDealGallery.IsDefault = Image.IsDefault;
                                    _MDDealGallery.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _MDDealGallery.CreatedById = _Request.UserReference.AccountId;
                                    _HCoreContext.MDDealGallery.Add(_MDDealGallery);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }

                        if (_Request.Images != null && _Request.Images.Count > 0)
                        {
                            ODeal.Manage.UploadImageRequest _UploadImageRequest = new ODeal.Manage.UploadImageRequest();
                            _UploadImageRequest.DealId = _MDDeal.Id;
                            _UploadImageRequest.DealKey = _MDDeal.Guid;
                            _UploadImageRequest.Images = _Request.Images;
                            _UploadImageRequest.UserReference = _Request.UserReference;

                            var _Actor = ActorSystem.Create("ActorUploadDealImage");
                            var _ActorNotify = _Actor.ActorOf<ActorUploadDealImage>("ActorUploadDealImage");
                            _ActorNotify.Tell(_UploadImageRequest);
                        }
                    }
                    if (_MDDeal.StatusId == HelperStatus.Deals.Published)
                    {
                        if (_Request.SendNotification)
                        {
                            #region TransactionPostProcess
                            var _TransactionPostProcessActor = ActorSystem.Create("NotificationNewDealActor");
                            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<NotificationNewDealActor>("NotificationNewDealActor");
                            _TransactionPostProcessActorNotify.Tell(_MDDeal.Id);
                            #endregion
                        }
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0101", ResponseCode.HCP0101);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveDeal", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDeal(ODeal.Manage.Request _Request)
        {
            //_ManageCoreTransaction = new ManageCoreTransaction();
            #region Manage Exception 
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }
                int? UsageTypeId = HCoreHelper.GetHelperId(_Request.CodeUsageTypeCode);
                int? DealTypeId = HCoreHelper.GetHelperId(_Request.DealTypeCode);
                int? DeliveryTypeId = HCoreHelper.GetHelperId(_Request.DeliveryTypeCode);
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Images != null)
                    {
                        foreach (var item in _Request.Images)
                        {
                            if (!string.IsNullOrEmpty(item.reference))
                            {
                                long ImageId = _HCoreContext.HCCoreStorage.Where(x => x.Guid == item.reference).Select(x => x.Id).FirstOrDefault();
                                if (ImageId > 0)
                                {
                                    item.referenceId = ImageId;
                                }
                            }
                        }
                    }

                    MDDeal _Details = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                    if (_Request.TitleTypeId != 0 && _Details.TitleTypeId != _Request.TitleTypeId)
                    {
                        _Details.TitleTypeId = _Request.TitleTypeId;
                    }
                    //if (_Request.TitleTypeId != 0 && _Request.TitleTypeId != _Details.TitleTypeId)
                    //{
                    //    _Details.TitleTypeId = _Request.TitleTypeId;
                    //}
                    if (!string.IsNullOrEmpty(_Request.TitleContent) && _Details.Title != _Request.TitleContent)
                    {
                        _Details.TitleContent = _Request.TitleContent;
                    }
                    if (!string.IsNullOrEmpty(_Request.Title) && _Details.Title != _Request.Title)
                    {
                        _Details.Title = _Request.Title;
                    }
                    if (!string.IsNullOrEmpty(_Request.Description) && _Details.Title != _Request.Description)
                    {
                        _Details.Description = _Request.Description;
                    }
                    if (!string.IsNullOrEmpty(_Request.Terms) && _Details.Title != _Request.Terms)
                    {
                        _Details.Terms = _Request.Terms;
                    }
                    if (!string.IsNullOrEmpty(_Request.CodeUsageInformation) && _Details.Title != _Request.CodeUsageInformation)
                    {
                        _Details.UsageInformation = _Request.CodeUsageInformation;
                    }
                    if (_Request.StartDate != null && _Details.StartDate != _Request.StartDate)
                    {
                        _Details.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                        //_Details.StartDate = _Request.StartDate;
                    }
                    if (_Request.EndDate != null && _Details.EndDate != _Request.EndDate)
                    {
                        _Details.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                        //_Details.EndDate = _Request.EndDate;
                    }
                    if (_Request.ActualPrice > 0 && _Details.ActualPrice != _Request.ActualPrice)
                    {
                        _Details.ActualPrice = _Request.ActualPrice;
                    }
                    if (_Request.SellingPrice > 0 && _Details.SellingPrice != _Request.SellingPrice)
                    {
                        _Details.SellingPrice = _Request.SellingPrice;
                    }

                    //if (!string.IsNullOrEmpty(_Request.SettlementTypeCode))
                    //{
                    //    if (_Request.SettlementTypeCode == "prepaid")
                    //    {
                    //        _Details.SettlementTypeId = 636;
                    //    }
                    //    else
                    //    {
                    //        _Details.SettlementTypeId = 637;
                    //    }
                    //}
                    //if (!string.IsNullOrEmpty(_Request.CodeUsageTypeCode))
                    //{
                    //    if (UsageTypeId != null)
                    //    {
                    //        _Details.UsageTypeId = (int)UsageTypeId;
                    //    }
                    //    if (_Request.CodeUsageTypeCode == "hour")
                    //    {
                    //        if (_Request.CodeValidityDays != null && _Details.CodeValidtyDays != _Request.CodeValidityDays)
                    //        {
                    //            _Details.CodeValidtyDays = _Request.CodeValidityDays;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (_Request.CodeValidityStartDate != null)
                    //        {
                    //            _Details.CodeValidityStartDate = _Request.CodeValidityStartDate;
                    //        }
                    //        else
                    //        {
                    //            _Details.CodeValidityStartDate = _Request.StartDate;
                    //        }
                    //        if (_Request.CodeValidityEndDate != null)
                    //        {
                    //            _Details.CodeValidityEndDate = _Request.CodeValidityEndDate;
                    //        }
                    //        else
                    //        {
                    //            _Details.CodeValidityEndDate = _Request.EndDate;
                    //        }
                    //    }
                    //}
                    //if (_Request.CodeValidityStartDate != null && _Details.CodeValidityStartDate != _Request.CodeValidityStartDate)
                    //{
                    //    _Details.CodeValidityStartDate = _Request.CodeValidityStartDate;
                    //}
                    //if (_Request.CodeValidityEndDate != null && _Details.CodeValidityEndDate != _Request.CodeValidityEndDate)
                    //{
                    //    _Details.CodeValidityEndDate = _Request.CodeValidityEndDate;
                    //}

                    if (!string.IsNullOrEmpty(_Request.Title) && _Request.Title != _Details.Title)
                    {
                        string TitleSlug = HCoreHelper.GenerateSlug(_Request.Title);
                        if (_Details.Slug != TitleSlug || _Details.Slug == null)
                        {
                            bool SlugCheck = _HCoreContext.MDDeal.Any(x => x.Slug == TitleSlug);
                            if (!SlugCheck)
                            {
                                _Details.Slug = TitleSlug;
                            }
                            else
                            {
                                DateTime SlugDate = HCoreHelper.ConvertToUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                string NewSlug = TitleSlug + "-" + SlugDate.ToString("HH:mm:ss");
                                _Details.Slug = NewSlug;
                            }
                        }
                    }


                    if (DealTypeId > 0 || DealTypeId != null)
                    {
                        _Details.DealTypeId = DealTypeId;
                    }

                    if (DealTypeId == Helpers.DealType.ProductDeal)
                    {
                        if (DeliveryTypeId > 0 || DeliveryTypeId != null)
                        {
                            _Details.DeliveryTypeId = DeliveryTypeId;
                        }
                        else
                        {
                            _Details.DeliveryTypeId = Helpers.DeliveryType.InStorePickUp;
                        }
                    }
                    else
                    {
                        _Details.DeliveryTypeId = Helpers.DeliveryType.InStorePickUp;
                    }

                    if (DealTypeId == Helpers.DealType.ProductDeal
                        && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery
                        || DeliveryTypeId == Helpers.DeliveryType.Delivery))
                    {
                        if (string.IsNullOrEmpty(_Request.Packaging.Name))
                        {
                            _Request.Packaging.Name = _Details.Title;
                        }
                        _FrameworkPackaging = new FrameworkPackaging();
                        _FrameworkPackaging.CreatePackage(_Details.AccountId, _Details.Id, _Request.Packaging.Name, _Request.Packaging.Weight, _Request.UserReference);
                    }

                    if (UsageTypeId != null)
                    {
                        if (UsageTypeId > 0)
                        {
                            _Details.UsageTypeId = (int)UsageTypeId;
                            if (UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase)
                            {
                                _Details.CodeValidtyDays = _Request.CodeValidityHours;
                            }
                            else if (UsageTypeId == Helpers.DealCodeUsageType.DaysAfterPurchase)
                            {
                                _Details.CodeValidtyDays = _Request.CodeValidityDays * 24;
                            }
                            else if (UsageTypeId == Helpers.DealCodeUsageType.DealEndDate)
                            {
                                _Details.CodeValidityEndDate = _Request.EndDate;
                            }
                            else if (UsageTypeId == Helpers.DealCodeUsageType.ExpiresOnDate)
                            {
                                if (_Request.CodeValidityEndDate != null)
                                {
                                    _Details.CodeValidityEndDate = HCoreHelper.ConvertToUTC(_Request.CodeValidityEndDate, _Request.UserReference.CountryTimeZone); //_Details.CodeValidityEndDate;
                                }
                                else
                                {
                                    _Details.CodeValidityEndDate = _Details.EndDate;
                                }
                            }
                            else
                            {
                                _Details.CodeValidityEndDate = _Details.EndDate;
                            }
                        }
                    }
                    //if (!string.IsNullOrEmpty(_Request.SubCategoryKey))
                    //{
                    //    var CategoryDetails = _HCoreContext.TUCCategory
                    //        .Where(x => x.Guid == _Request.SubCategoryKey)
                    //        .Select(x => new
                    //        {
                    //            SubCategoryId = x.Id,
                    //            CategoryId = x.ParentCategoryId,
                    //        })
                    //        .FirstOrDefault();
                    //    if (CategoryDetails != null)
                    //    {
                    //        _Details.CategoryId = CategoryDetails.CategoryId;
                    //        _Details.SubcategoryId = CategoryDetails.SubCategoryId;
                    //    }
                    //}
                    double Fee = 5;
                    if (!string.IsNullOrEmpty(_Request.SubCategoryKey))
                    {
                        var CategoryDetails = _HCoreContext.MDCategory
                          .Where(x => x.RootCategory.Guid == _Request.SubCategoryKey)
                          .Select(x => new
                          {
                              SubCategoryId = x.RootCategory.Id,
                              CategoryId = x.RootCategory.ParentCategoryId,
                              Fee = x.Commission
                          })
                          .FirstOrDefault();
                        if (CategoryDetails == null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0184", ResponseCode.HCP0184);
                        }
                        Fee = CategoryDetails.Fee;
                        _Details.CategoryId = CategoryDetails.CategoryId;
                        _Details.SubcategoryId = CategoryDetails.SubCategoryId;
                    }
                    else
                    {
                        if (_Details.SubcategoryId > 0)
                        {
                            var CategoryDetails = _HCoreContext.MDCategory
                                               .Where(x => x.RootCategoryId == _Details.SubcategoryId)
                                               .Select(x => new
                                               {
                                                   Fee = x.Commission
                                               })
                                               .FirstOrDefault();
                            Fee = CategoryDetails.Fee;
                        }
                    }

                    _Details.ActualPrice = _Details.ActualPrice;
                    _Details.SellingPrice = _Details.SellingPrice;
                    _Details.DiscountTypeId = 639; // Percentage
                    _Details.CommissionAmount = HCoreHelper.GetPercentage((double)_Details.SellingPrice, Fee);
                    _Details.CommissionPercentage = Fee;
                    if (_Request.UserReference.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Admin)
                    {
                        if (_Request.TUCPercentage > 0)
                        {
                            _Details.CommissionAmount = HCoreHelper.GetPercentage((double)_Details.SellingPrice, _Request.TUCPercentage);
                            _Details.CommissionPercentage = _Request.TUCPercentage;
                        }
                    }
                    _Details.DiscountAmount = Math.Round(((double)_Details.ActualPrice - (double)_Details.SellingPrice), 2);
                    _Details.DiscountPercentage = HCoreHelper.GetAmountPercentage((double)_Details.DiscountAmount, (double)_Details.ActualPrice);
                    _Details.Amount = _Details.SellingPrice;
                    _Details.Charge = 0;
                    _Details.TotalAmount = _Details.SellingPrice;
                    //if (!string.IsNullOrEmpty(_Request.DiscountTypeCode))
                    //{
                    //    if (_Request.SettlementTypeCode == "percentage")
                    //    {
                    //        _Details.DiscountTypeId = 639;
                    //    }
                    //    else
                    //    {
                    //        _Details.DiscountTypeId = 640;
                    //    }
                    //}
                    //if (_Request.DiscountAmount > 0 && _Details.DiscountAmount != _Request.DiscountAmount)
                    //{
                    //    _Details.DiscountAmount = _Request.DiscountAmount;
                    //}
                    //if (_Request.DiscountPercentage > 0 && _Details.DiscountPercentage != _Request.DiscountPercentage)
                    //{
                    //    _Details.DiscountPercentage = _Request.DiscountPercentage;
                    //}
                    //if (_Request.Amount > 0 && _Details.Amount != _Request.Amount)
                    //{
                    //    _Details.Amount = _Request.Amount;
                    //}
                    //if (_Request.Charge > 0 && _Details.Charge != _Request.Charge)
                    //{
                    //    _Details.Charge = _Request.Charge;
                    //}
                    //if (_Request.CommissionAmount > 0 && _Details.CommissionAmount != _Request.CommissionAmount)
                    //{
                    //    _Details.CommissionAmount = _Request.CommissionAmount;
                    //}
                    //if (_Request.TotalAmount > 0 && _Details.TotalAmount != _Request.TotalAmount)
                    //{
                    //    _Details.TotalAmount = _Request.TotalAmount;
                    //}
                    if (_Request.MaximumUnitSale > 0)
                    {
                        _Details.MaximumUnitSale = _Request.MaximumUnitSale;
                        _Details.MaximumUnitSalePerDay = _Request.MaximumUnitSale;
                    }
                    //if (_Request.MaximumUnitSalePerDay > 0 && _Details.MaximumUnitSalePerDay != _Request.MaximumUnitSalePerDay)
                    //{
                    //    _Details.MaximumUnitSalePerDay = _Request.MaximumUnitSalePerDay;
                    //}
                    if (_Request.MaximumUnitSalePerPerson > 0 && _Details.MaximumUnitSalePerPerson != _Request.MaximumUnitSalePerPerson)
                    {
                        _Details.MaximumUnitSalePerPerson = _Request.MaximumUnitSalePerPerson;
                    }

                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _Details.ModifyById = _Request.UserReference.AccountId;
                    if (StatusId > 0)
                    {
                        _Details.StatusId = StatusId;
                    }
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        StatusCode = _Request.StatusCode,
                        ReferenceId = _Request.ReferenceId,
                        ReferenceKey = _Request.ReferenceKey
                    };
                    if (_Request.Locations != null && _Request.Locations.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var DealLocations = _HCoreContext.MDDealLocation.Where(x => x.DealId == _Request.ReferenceId).ToList();
                            if (DealLocations.Count() > 0)
                            {
                                _HCoreContext.MDDealLocation.RemoveRange(DealLocations);
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Location in _Request.Locations)
                            {
                                _MDDealLocation = new MDDealLocation();
                                _MDDealLocation.Guid = HCoreHelper.GenerateGuid();
                                _MDDealLocation.DealId = _Request.ReferenceId;
                                _MDDealLocation.LocationId = Location.ReferenceId;
                                _MDDealLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                                _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                _HCoreContext.MDDealLocation.Add(_MDDealLocation);
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    if (_Request.Shedule != null && _Request.Shedule.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MDDealShedules = _HCoreContext.MDDealShedule.Where(x => x.DealId == _Request.ReferenceId).ToList();
                            if (MDDealShedules.Count() > 0)
                            {
                                _HCoreContext.MDDealShedule.RemoveRange(MDDealShedules);
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                            }
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Location in _Request.Shedule)
                            {
                                _MDDealShedule = new MDDealShedule();
                                _MDDealShedule.Guid = HCoreHelper.GenerateGuid();
                                if (Location.TypeCode == "dealredeemshedule")
                                {
                                    _MDDealShedule.TypeId = 634;
                                }
                                else
                                {
                                    _MDDealShedule.TypeId = 633;
                                }
                                _MDDealShedule.DealId = _Request.ReferenceId;
                                _MDDealShedule.StartHour = Location.StartHour;
                                _MDDealShedule.EndHour = Location.EndHour;
                                _MDDealShedule.DayOfWeek = Location.DayOfWeek;
                                _MDDealLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                                _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                _HCoreContext.MDDealShedule.Add(_MDDealShedule);
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }
                    if (_Request.Images != null && _Request.Images.Count > 0)
                    {
                        foreach (var Image in _Request.Images)
                        {
                            if (Image.referenceId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (Image.IsDefault == 1)
                                    {
                                        var DealInfo = _HCoreContext.MDDeal.Where(x => x.Id == _Details.Id && x.PosterStorageId == null).FirstOrDefault();
                                        if (DealInfo != null)
                                        {
                                            DealInfo.PosterStorageId = Image.referenceId;
                                        }
                                    }
                                    _MDDealGallery = new MDDealGallery();
                                    _MDDealGallery.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealGallery.DealId = _Details.Id;
                                    _MDDealGallery.ImageStorageId = Image.referenceId;
                                    _MDDealGallery.IsDefault = Image.IsDefault;
                                    _MDDealGallery.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _MDDealGallery.CreatedById = _Request.UserReference.AccountId;
                                    _HCoreContext.MDDealGallery.Add(_MDDealGallery);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }

                    if (_Request.Images != null && _Request.Images.Count > 0)
                    {
                        ODeal.Manage.UploadImageRequest _UploadImageRequest = new ODeal.Manage.UploadImageRequest();
                        _UploadImageRequest.DealId = _Details.Id;
                        _UploadImageRequest.DealKey = _Details.Guid;
                        _UploadImageRequest.Images = _Request.Images;
                        _UploadImageRequest.UserReference = _Request.UserReference;

                        var _Actor = ActorSystem.Create("ActorUploadDealImage");
                        var _ActorNotify = _Actor.ActorOf<ActorUploadDealImage>("ActorUploadDealImage");
                        _ActorNotify.Tell(_UploadImageRequest);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0101", ResponseCode.HCP0101);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateDeal", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the deal coupons.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDealCoupons(ODeal.Manage.Request _Request)
        {
            #region Manage Exception 
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    MDDeal _Details = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                    _Details.MaximumUnitSale = _Request.MaximumUnitSale + _Details.MaximumUnitSale;
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _Request.ReferenceId,
                        ReferenceKey = _Request.ReferenceKey
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0101", ResponseCode.HCP0101);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateDealCoupons", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Extends the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ExtendDeal(ODeal.Manage.Request _Request)
        {
            #region Manage Exception 
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }

                int? UsageTypeId = HCoreHelper.GetHelperId(_Request.UsageTypeCode);
                using (_HCoreContext = new HCoreContext())
                {
                    MDDeal _Details = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                    if (_Details.StatusId == HelperStatus.Deals.Expired)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0196", ResponseCode.HCP0196);
                    }
                    if (_Request.StartDate != null && _Details.StartDate != _Request.StartDate)
                    {

                        _Details.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                    }
                    if (_Request.EndDate != null && _Details.EndDate != _Request.EndDate)
                    {
                        _Details.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    }
                    if (UsageTypeId != null)
                    {
                        if (UsageTypeId > 0)
                        {
                            if (UsageTypeId == Helpers.DealCodeUsageType.DealEndDate)
                            {
                                _Details.CodeValidityEndDate = _Request.EndDate;
                            }
                        }
                    }
                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _Details.ModifyById = _Request.UserReference.AccountId;
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _Request.ReferenceId,
                        ReferenceKey = _Request.ReferenceKey
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0101", ResponseCode.HCP0101);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("ExtendDeal", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the deal status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDealStatus(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0103", ResponseCode.HCP0103);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var DealDetails = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .FirstOrDefault();
                    if (DealDetails != null)
                    {
                        DealDetails.StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        DealDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        DealDetails.ModifyById = _Request.UserReference.AccountId;
                        //if (DealDetails.StatusId == HelperStatus.Deals.Draft && _Request.StatusCode == HelperStatus.Deals.ApprovalPendingS)
                        //{
                        //    DealDetails.StatusId = HelperStatus.Deals.ApprovalPending;
                        //}
                        //else if (DealDetails.StatusId == HelperStatus.Deals.ApprovalPending && _Request.StatusCode == HelperStatus.Deals.ApprovedS)
                        //{
                        //    DealDetails.StatusId = HelperStatus.Deals.Approved;
                        //}
                        //else if (DealDetails.StatusId == HelperStatus.Deals.ApprovalPending && _Request.StatusCode == HelperStatus.Deals.ApprovedS)
                        //{
                        //    DealDetails.StatusId = HelperStatus.Deals.Approved;
                        //}
                        //else if (DealDetails.StatusId == HelperStatus.Deals.Approved && _Request.StatusCode == HelperStatus.Deals.ApprovedS)
                        //{
                        //    DealDetails.StatusId = HelperStatus.Deals.Approved;
                        //}
                        //if (DealStatus != HelperStatus.Deals.Draft)
                        //{
                        //    _HCoreContext.Dispose();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0105", ResponseCode.HCP0105);
                        //}
                        //var DealLocations = _HCoreContext.MDDealLocation.Where(x => x.DealId == _Request.ReferenceId).ToList();
                        //if (DealLocations.Count() > 0)
                        //{
                        //    _HCoreContext.MDDealLocation.RemoveRange(DealLocations);
                        //}
                        //var DealShedule = _HCoreContext.MDDealShedule.Where(x => x.DealId == _Request.ReferenceId).ToList();
                        //if (DealShedule.Count() > 0)
                        //{
                        //    _HCoreContext.MDDealShedule.RemoveRange(DealShedule);
                        //}
                        //var DealGallery = _HCoreContext.MDDealGallery.Where(x => x.DealId == _Request.ReferenceId).ToList();
                        //if (DealGallery.Count() > 0)
                        //{
                        //    _HCoreContext.MDDealGallery.RemoveRange(DealGallery);
                        //    _HCoreContext.SaveChanges();
                        //    var Deal = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                        //    if (Deal != null)
                        //    {
                        //        _HCoreContext.MDDeal.Remove(Deal);
                        //    }
                        //    foreach (var item in DealGallery)
                        //    {
                        //        bool DeleteResponse = HCoreHelper.DeleteStorage(item.ImageStorageId, _Request.UserReference);
                        //    }
                        //}
                        //else
                        //{
                        //    var Deal = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                        //    if (Deal != null)
                        //    {
                        //        _HCoreContext.MDDeal.Remove(Deal);
                        //    }
                        //    _HCoreContext.SaveChanges();
                        //}
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0107", ResponseCode.HCP0107);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateDealStatus", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Duplicates the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DuplicateDeal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var DealDetails = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .FirstOrDefault();
                    if (DealDetails != null)
                    {
                        var DealLocations = _HCoreContext.MDDealLocation.Where(x => x.DealId == DealDetails.Id).ToList();
                        var DealShedule = _HCoreContext.MDDealShedule.Where(x => x.DealId == DealDetails.Id).ToList();
                        var DealImages = _HCoreContext.MDDealGallery.Where(x => x.DealId == DealDetails.Id).ToList();



                        _MDDeal = new MDDeal();
                        _MDDeal.Guid = HCoreHelper.GenerateGuid();
                        _MDDeal.TypeId = 1;
                        _MDDeal.SettlementTypeId = DealDetails.SettlementTypeId;
                        _MDDeal.AccountId = DealDetails.AccountId;
                        _MDDeal.TitleContent = DealDetails.TitleContent;
                        _MDDeal.TitleTypeId = DealDetails.TitleTypeId;
                        _MDDeal.AccountId = DealDetails.AccountId;
                        _MDDeal.Title = DealDetails.Title;
                        _MDDeal.Description = DealDetails.Description;
                        _MDDeal.Terms = DealDetails.Terms;
                        _MDDeal.UsageTypeId = 1;
                        _MDDeal.UsageInformation = DealDetails.UsageInformation;
                        _MDDeal.IsPinRequired = 0;
                        _MDDeal.StartDate = DealDetails.StartDate;
                        _MDDeal.EndDate = DealDetails.EndDate;
                        _MDDeal.UsageTypeId = DealDetails.UsageTypeId;
                        _MDDeal.CodeValidtyDays = DealDetails.CodeValidtyDays;
                        _MDDeal.CodeValidityStartDate = DealDetails.StartDate;
                        _MDDeal.CodeValidityEndDate = DealDetails.CodeValidityEndDate;

                        _MDDeal.ActualPrice = DealDetails.ActualPrice;
                        _MDDeal.SellingPrice = DealDetails.SellingPrice;
                        _MDDeal.DiscountTypeId = DealDetails.DiscountTypeId;

                        if (DealDetails.DealTypeId > 0)
                        {
                            _MDDeal.DealTypeId = DealDetails.DealTypeId;
                        }
                        else
                        {
                            _MDDeal.DealTypeId = Helpers.DealType.ServiceDeal;
                        }

                        if (string.IsNullOrEmpty(DealDetails.Slug))
                        {
                            string TitleSlug = HCoreHelper.GenerateSlug(DealDetails.Title);
                            bool SlugCheck = _HCoreContext.MDDeal.Any(x => x.Slug == TitleSlug);
                            if (!SlugCheck)
                            {
                                _MDDeal.Slug = TitleSlug;
                            }
                            else
                            {
                                DateTime SlugDate = HCoreHelper.ConvertToUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                string NewSlug = TitleSlug + "-" + SlugDate.ToString("HH:mm:ss");
                                _MDDeal.Slug = NewSlug;
                            }
                        }
                        else
                        {
                            DateTime SlugDate = HCoreHelper.ConvertToUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                            string TitleSlug = DealDetails.Slug + "-" + SlugDate.ToString("HH:mm:ss");
                            bool SlugCheck = _HCoreContext.MDDeal.Any(x => x.Slug == DealDetails.Slug || x.Slug == TitleSlug);
                            if (!SlugCheck)
                            {
                                _MDDeal.Slug = TitleSlug;
                            }
                            else
                            {
                                string NewSlug = TitleSlug + "-" + SlugDate.ToString("HH:mm:ss");
                                _MDDeal.Slug = NewSlug;
                            }
                        }

                        _MDDeal.DiscountAmount = DealDetails.DiscountAmount;
                        _MDDeal.DiscountPercentage = DealDetails.DiscountPercentage;
                        _MDDeal.Amount = DealDetails.Amount;
                        _MDDeal.Charge = DealDetails.Charge;
                        _MDDeal.CommissionAmount = DealDetails.CommissionAmount;
                        _MDDeal.TotalAmount = DealDetails.TotalAmount;
                        _MDDeal.MaximumUnitSale = DealDetails.MaximumUnitSale;
                        _MDDeal.TView = 0;
                        _MDDeal.Views = 0;
                        _MDDeal.TPurchase = 0;
                        _MDDeal.TLike = 0;
                        _MDDeal.Like = 0;
                        _MDDeal.TDislike = 0;
                        _MDDeal.DisLike = 0;
                        _MDDeal.PosterStorageId = DealDetails.PosterStorageId;
                        _MDDeal.MaximumUnitSalePerDay = DealDetails.MaximumUnitSalePerDay;
                        _MDDeal.MaximumUnitSalePerPerson = DealDetails.MaximumUnitSalePerPerson;
                        _MDDeal.Views = 0;
                        _MDDeal.CategoryId = DealDetails.CategoryId;
                        _MDDeal.SubcategoryId = DealDetails.SubcategoryId;

                        _MDDeal.CreateDate = HCoreHelper.GetGMTDateTime();
                        _MDDeal.CreatedById = _Request.UserReference.AccountId;
                        _MDDeal.StatusId = HelperStatus.Deals.Draft;
                        _HCoreContext.MDDeal.Add(_MDDeal);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _MDDeal.Id,
                            ReferenceKey = _MDDeal.Guid
                        };
                        if (DealLocations != null && DealLocations.Count > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                foreach (var Location in DealLocations)
                                {
                                    _MDDealLocation = new MDDealLocation();
                                    _MDDealLocation.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealLocation.DealId = _MDDeal.Id;
                                    _MDDealLocation.LocationId = Location.Id;
                                    _MDDealLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                    _HCoreContext.MDDealLocation.Add(_MDDealLocation);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }

                        if (DealShedule != null && DealShedule.Count > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                foreach (var Location in DealShedule)
                                {
                                    _MDDealShedule = new MDDealShedule();
                                    _MDDealShedule.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealShedule.TypeId = Location.TypeId;
                                    _MDDealShedule.DealId = _MDDeal.Id;
                                    _MDDealShedule.StartHour = Location.StartHour;
                                    _MDDealShedule.EndHour = Location.EndHour;
                                    _MDDealShedule.DayOfWeek = Location.DayOfWeek;
                                    _MDDealLocation.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                    _HCoreContext.MDDealShedule.Add(_MDDealShedule);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        if (DealImages != null && DealImages.Count > 0)
                        {
                            foreach (var Image in DealImages)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    _MDDealGallery = new MDDealGallery();
                                    _MDDealGallery.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealGallery.DealId = _MDDeal.Id;
                                    _MDDealGallery.ImageStorageId = Image.ImageStorageId;
                                    _MDDealGallery.IsDefault = Image.IsDefault;
                                    _MDDealGallery.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _MDDealGallery.CreatedById = _Request.UserReference.AccountId;
                                    _HCoreContext.MDDealGallery.Add(_MDDealGallery);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0161", ResponseCode.HCP0161);


                        //_HCoreContext.SaveChanges();
                        //_HCoreContext.Dispose();
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0107", ResponseCode.HCP0107);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DuplicateDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Approves the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ApproveDeal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime? TodayDateTime = HCoreHelper.GetGMTDateTime();
                    var DealDetails = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .FirstOrDefault();
                    if (DealDetails != null)
                    {
                        if (DealDetails.StatusId == HelperStatus.Deals.ApprovalPending && DealDetails.EndDate > TodayDateTime)
                        {
                            DealDetails.StatusId = HelperStatus.Deals.Published;
                            DealDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            DealDetails.ApproverId = _Request.UserReference.AccountId;
                            DealDetails.ApprovalDate = HCoreHelper.GetGMTDateTime();
                            DealDetails.ApproverComment = _Request.Comment;
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0158", ResponseCode.HCP0158);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0157", ResponseCode.HCP0157);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ApproveDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Rejects the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RejectDeal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.Comment))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0159", ResponseCode.HCP0159);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var DealDetails = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .FirstOrDefault();
                    if (DealDetails != null)
                    {
                        if (DealDetails.StatusId == HelperStatus.Deals.ApprovalPending)
                        {
                            DealDetails.StatusId = HelperStatus.Deals.Rejected;
                            DealDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            DealDetails.ApproverId = _Request.UserReference.AccountId;
                            DealDetails.ApprovalDate = HCoreHelper.GetGMTDateTime();
                            DealDetails.ApproverComment = _Request.Comment;
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0160", ResponseCode.HCP0160);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0197", ResponseCode.HCP0197);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("RejectDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal async Task<OResponse> DeleteDeal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long DealStatus = await _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey
                                                && x.AccountId == _Request.AccountId
                                                && x.Account.Guid == _Request.AccountKey)
                                                .Select(x => x.StatusId)
                                                .FirstOrDefaultAsync();
                    if (DealStatus > 0)
                    {
                        var IsFlashDeal = await _HCoreContext.MDFlashDeal.AnyAsync(x => x.DealId == _Request.ReferenceId);
                        if (IsFlashDeal)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ISFLASH", "This deal has been marked as a flash deal. Please delete the flash deal and then try to delete this deal.");
                        }
                        var IsDealPromoted = await _HCoreContext.MDDealPromotion.AnyAsync(x => x.DealId == _Request.ReferenceId);
                        if (IsDealPromoted)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ISDEALPROMOTED", "This deal has been promoted. Please delete the deal promotion and then try to delete this deal.");
                        }
                        var IsDealOrdered = await _HCoreContext.LSShipments.AnyAsync(x => x.DealId == _Request.ReferenceId);
                        if (IsDealOrdered)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ISDEALORDERED", "There are some orders available for this deal. You cannot delete this deal.");
                        }
                        var IsDealBuyed = await _HCoreContext.MDDealCode.AnyAsync(x => x.DealId == _Request.ReferenceId);
                        if (IsDealBuyed)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "ISDEALBUYED", "This deal is baught by some users and there are some dealcodes available for this deal. You cannot delete this deal.");
                        }

                        var DealLocations = await _HCoreContext.MDDealLocation.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealLocations.Count() > 0)
                        {
                            _HCoreContext.MDDealLocation.RemoveRange(DealLocations);
                        }
                        var DealShedule = await _HCoreContext.MDDealShedule.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealShedule.Count() > 0)
                        {
                            _HCoreContext.MDDealShedule.RemoveRange(DealShedule);
                        }
                        var DealPackages = await _HCoreContext.LSPackages.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealPackages != null)
                        {
                            _HCoreContext.LSPackages.RemoveRange(DealPackages);
                        }
                        var DealOperations = await _HCoreContext.LSOperations.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealOperations.Count > 0)
                        {
                            _HCoreContext.LSOperations.RemoveRange(DealOperations);
                        }
                        var DealParcels = await _HCoreContext.LSParcel.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealParcels.Count > 0)
                        {
                            _HCoreContext.LSParcel.RemoveRange(DealParcels);
                        }
                        var DealBookMarks = await _HCoreContext.MDDealBookmark.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealBookMarks.Count > 0)
                        {
                            _HCoreContext.MDDealBookmark.RemoveRange(DealBookMarks);
                        }
                        var DealCarts = await _HCoreContext.MDDealCart.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealCarts.Count > 0)
                        {
                            _HCoreContext.MDDealCart.RemoveRange(DealCarts);
                        }
                        var DealLikes = await _HCoreContext.MDDealLike.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealLikes.Count > 0)
                        {
                            _HCoreContext.MDDealLike.RemoveRange(DealLikes);
                        }
                        var DealNotifications = await _HCoreContext.MDDealNotification.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealNotifications.Count > 0)
                        {
                            _HCoreContext.MDDealNotification.RemoveRange(DealNotifications);
                        }
                        var DealPromoCodes = await _HCoreContext.MDDealPromoCode.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealPromoCodes.Count > 0)
                        {
                            _HCoreContext.MDDealPromoCode.RemoveRange(DealPromoCodes);
                        }
                        var DealReViewes = await _HCoreContext.MDDealReview.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealReViewes.Count > 0)
                        {
                            _HCoreContext.MDDealReview.RemoveRange(DealReViewes);
                        }
                        var DealViewes = await _HCoreContext.MDDealView.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealViewes.Count > 0)
                        {
                            _HCoreContext.MDDealView.RemoveRange(DealViewes);
                        }
                        var DealWishLists = await _HCoreContext.MDDealWishlist.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealWishLists.Count > 0)
                        {
                            _HCoreContext.MDDealWishlist.RemoveRange(DealWishLists);
                        }
                        var DealGallery = await _HCoreContext.MDDealGallery.Where(x => x.DealId == _Request.ReferenceId).ToListAsync();
                        if (DealGallery.Count() > 0)
                        {
                            _HCoreContext.MDDealGallery.RemoveRange(DealGallery);
                            await _HCoreContext.SaveChangesAsync();
                            var Deal = await _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId).FirstOrDefaultAsync();
                            if (Deal != null)
                            {
                                _HCoreContext.MDDeal.Remove(Deal);
                                await _HCoreContext.SaveChangesAsync();
                            }
                            foreach (var item in DealGallery)
                            {
                                bool DeleteResponse = HCoreHelper.DeleteStorage(item.ImageStorageId, _Request.UserReference);
                            }
                        }
                        else
                        {
                            var Deal = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();
                            if (Deal != null)
                            {
                                _HCoreContext.MDDeal.Remove(Deal);
                            }
                            await _HCoreContext.SaveChangesAsync();
                        }

                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0106", ResponseCode.HCP0106);
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _DealDetails = new ODeal.Details();
                    _DealDetails = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODeal.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountEmailAddress = x.Account.EmailAddress,
                                                    AccountIconUrl = x.Account.IconStorage.Path,

                                                    Title = x.Title,

                                                    TitleTypeId = x.TitleTypeId,
                                                    TitleContent = x.TitleContent,

                                                    Description = x.Description,
                                                    Terms = x.Terms,

                                                    UsageTypeId = x.UsageTypeId,
                                                    UsageTypeCode = x.UsageType.SystemName,
                                                    UsageTypeName = x.UsageType.Name,
                                                    UsageInformation = x.UsageInformation,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    DiscountTypeCode = x.DiscountType.SystemName,
                                                    DiscountTypeName = x.DiscountType.Name,

                                                    CodeValidityDays = x.CodeValidtyDays,
                                                    CodeValidityStartDate = x.CodeValidityStartDate,
                                                    CodeValidityEndDate = x.CodeValidityEndDate,

                                                    ActualPrice = x.ActualPrice,
                                                    SellingPrice = x.SellingPrice,

                                                    MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                    DiscountAmount = x.DiscountAmount,
                                                    DiscountPercentage = x.DiscountPercentage,

                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    CommissionAmount = x.CommissionAmount,
                                                    CommissionPercentage = x.CommissionPercentage,
                                                    TotalAmount = x.TotalAmount,

                                                    MaximumUnitSale = x.MaximumUnitSale,
                                                    MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,
                                                    MaximumUnitSalePerPerson = x.MaximumUnitSalePerPerson,

                                                    ImageUrl = x.PosterStorage.Path,

                                                    DealTypeId = x.DealTypeId,
                                                    DealTypeCode = x.DealType.SystemName,
                                                    DealTypeName = x.DealType.Name,

                                                    DeliveryTypeId = x.DeliveryTypeId,
                                                    DeliveryTypeCode = x.DeliveryType.SystemName,
                                                    DeliveryTypeName = x.DeliveryType.Name,

                                                    MerchantAddressId = x.Account.AddressId,
                                                    MerchantAddresskey = x.Account.AddressNavigation.Guid,

                                                    Views = x.Views,
                                                    CategoryId = x.CategoryId,
                                                    CategoryKey = x.Category.Guid,
                                                    CategoryName = x.Category.Name,

                                                    SubCategoryId = x.SubcategoryId,
                                                    SubCategoryKey = x.Subcategory.Guid,
                                                    SubCategoryName = x.Subcategory.Name,

                                                    SettlmentTypeCode = x.SettlementType.SystemName,
                                                    SettlmentTypeName = x.SettlementType.Name,

                                                    TotalPurchase = (long)x.MDDealCode.Sum(y => y.ItemCount),
                                                    Budget = x.Amount * x.MaximumUnitSale,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    ApprovalDate = x.ApprovalDate,
                                                    ApproverComment = x.ApproverComment,
                                                    ApproverId = x.ApproverId,
                                                    ApproverDisplayName = x.Approver.DisplayName,
                                                    ApproverKey = x.Approver.Guid,

                                                    Slug = x.Slug,
                                                }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        _DealDetails.AvailableQuantity = (int?)(_DealDetails.MaximumUnitSale - _DealDetails.TotalPurchase);
                        if (_DealDetails.AvailableQuantity < 0)
                        {
                            _DealDetails.AvailableQuantity = 0;
                        }

                        double? _DealCom = _HCoreContext.MDCategory.Where(x => x.RootCategoryId == _DealDetails.SubCategoryId).Select(x => x.Commission).FirstOrDefault();
                        if (_DealCom != null)
                        {
                            _DealDetails.SubCategoryFee = _DealCom;
                            if (_DealDetails.CommissionPercentage == null)
                            {
                                _DealDetails.CommissionPercentage = _DealCom;
                            }
                        }
                        //SubCategoryPercentage = x.Subcategory.MDCategory.FirstOrDefault().Commission,


                        var FlashDealInfo = _HCoreContext.MDFlashDeal.Where(x => x.DealId == _Request.ReferenceId)
                            .OrderByDescending(x => x.Id)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                            }).FirstOrDefault();
                        if (FlashDealInfo != null)
                        {
                            _DealDetails.IsFlashDeal = true;
                            _DealDetails.FlashDealId = FlashDealInfo.ReferenceId;
                            _DealDetails.FlashDealKey = FlashDealInfo.ReferenceKey;
                            _DealDetails.FlashDealStartDate = FlashDealInfo.StartDate;
                            _DealDetails.FlashDealEndDate = FlashDealInfo.EndDate;
                        }
                        else
                        {
                            _DealDetails.IsFlashDeal = false;
                        }

                        _DealDetails.Ratings = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId)
                                      .Select(x => new ODeal.Rating
                                      {
                                          AverageDealRating = x.MDDealReview.Where(a => a.DealId == _Request.ReferenceId).Average(a => a.Rating),
                                          TotalDealRating = x.MDDealReview.Where(a => a.DealId == _Request.ReferenceId).Sum(a => a.Rating),
                                          Rating1 = x.MDDealReview.Where(a => a.DealId == _Request.ReferenceId && a.Rating > 4).Sum(a => a.Rating),
                                          Rating2 = x.MDDealReview.Where(a => a.DealId == _Request.ReferenceId && (a.Rating == 4 && a.Rating > 3)).Sum(a => a.Rating),
                                          Rating3 = x.MDDealReview.Where(a => a.DealId == _Request.ReferenceId && (a.Rating == 3 && a.Rating > 2)).Sum(a => a.Rating),
                                          Rating4 = x.MDDealReview.Where(a => a.DealId == _Request.ReferenceId && (a.Rating == 2 && a.Rating > 1)).Sum(a => a.Rating),
                                          Rating5 = x.MDDealReview.Where(a => a.DealId == _Request.ReferenceId && a.Rating == 1).Sum(a => a.Rating),
                                      }).FirstOrDefault();

                        var AccountRatings = _HCoreContext.MDDeal.Where(x => x.Id == _DealDetails.ReferenceId)
                                            .Select(x => new
                                            {
                                                AverageAccountRating = x.Account.AverageValue,
                                                TotalAccountRating = x.Account.HCUAccountParameterAccount.Sum(a => Convert.ToInt64(a.Value)),
                                            }).FirstOrDefault();
                        if (AccountRatings != null)
                        {
                            _DealDetails.AverageAccountRating = AccountRatings.AverageAccountRating;
                            _DealDetails.AccountRatingCount = AccountRatings.TotalAccountRating;
                        }
                        else
                        {
                            _DealDetails.AverageAccountRating = 0.0;
                            _DealDetails.AccountRatingCount = 0;
                        }

                        var DealPromotionInfo = _HCoreContext.MDDealPromotion.Where(x => x.DealId == _Request.ReferenceId)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                Status = x.Status.SystemName,
                                IconUrl = x.ImageStorage.Path,
                                DealPromotionLocations = x.Location
                            }).FirstOrDefault();
                        if (DealPromotionInfo != null)
                        {
                            _DealDetails.IsDealPromoted = true;
                            _DealDetails.DealPromotionId = DealPromotionInfo.ReferenceId;
                            _DealDetails.DealPromotionKey = DealPromotionInfo.ReferenceKey;
                            _DealDetails.DealPromotionStartDate = DealPromotionInfo.StartDate;
                            _DealDetails.DealPrmotionEndDate = DealPromotionInfo.EndDate;
                            _DealDetails.DealPromotionStatus = DealPromotionInfo.Status;
                            _DealDetails.DealPromotionLocations = DealPromotionInfo.DealPromotionLocations;
                            //_DealDetails.DealPromotionImageUrl = _AppConfig.StorageUrl + DealPromotionInfo.IconUrl;
                            if (!string.IsNullOrEmpty(DealPromotionInfo.IconUrl))
                            {
                                _DealDetails.DealPromotionImageUrl = _AppConfig.StorageUrl + DealPromotionInfo.IconUrl;
                            }
                            else
                            {
                                _DealDetails.DealPromotionImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                            }
                        }
                        else
                        {
                            _DealDetails.IsDealPromoted = false;
                        }

                        _DealDetails.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == _DealDetails.ReferenceId)
                            .Select(x => new ODeal.Location
                            {
                                AccountId = x.LocationId,
                                AccountKey = x.Location.Guid,
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                DisplayName = x.Location.DisplayName,
                                Address = x.Location.Address
                            }).ToList();

                        _DealDetails.Shedule = _HCoreContext.MDDealShedule.Where(x => x.DealId == _DealDetails.ReferenceId)
                          .Select(x => new ODeal.Shedule
                          {
                              ReferenceId = x.Id,
                              ReferenceKey = x.Guid,
                              TypeCode = x.Type.SystemName,
                              TypeName = x.Type.Name,
                              StartHour = x.StartHour,
                              EndHour = x.EndHour,
                              DayOfWeek = x.DayOfWeek,
                          }).ToList();

                        _DealDetails.Images = _HCoreContext.MDDealGallery.Where(x => x.DealId == _DealDetails.ReferenceId)
                            .Select(x => new ODeal.Gallery
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                ImageUrl = x.ImageStorage.Path,
                                IsDefault = x.IsDefault,
                            }).ToList();

                        if (!string.IsNullOrEmpty(_DealDetails.ImageUrl))
                        {
                            _DealDetails.ImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                        }
                        else
                        {
                            _DealDetails.ImageUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_DealDetails.AccountIconUrl))
                        {
                            _DealDetails.AccountIconUrl = _AppConfig.StorageUrl + _DealDetails.AccountIconUrl;
                        }
                        else
                        {
                            _DealDetails.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (_DealDetails.Images != null && _DealDetails.Images.Count > 0)
                        {
                            foreach (var Image in _DealDetails.Images)
                            {
                                if (!string.IsNullOrEmpty(Image.ImageUrl))
                                {
                                    Image.ImageUrl = _AppConfig.StorageUrl + Image.ImageUrl;
                                }
                                else
                                {
                                    Image.ImageUrl = _AppConfig.Default_Icon;
                                }
                            }
                        }

                        _DealDetails.PackagingCategory = _HCoreContext.LSPackages.Where(x => x.DealId == _DealDetails.ReferenceId && x.Deal.Guid == _DealDetails.ReferenceKey)
                                                 .Select(x => new ODeal.PackagingCategory
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Height = x.Height,
                                                     Length = x.Length,
                                                     Name = x.ProductName,
                                                     Size_unit = x.SizeUnit,
                                                     Type = x.Type,
                                                     Width = x.Width,
                                                     Weight = x.Weight,
                                                     Weight_unit = x.WeightUnit,
                                                     ParcelDescription = x.Description,
                                                     PackagingId = x.PackagingId
                                                 }).FirstOrDefault();
                        if (_DealDetails.PackagingCategory != null)
                        {
                            _DealDetails.Packaging = _HCoreContext.MDPackaging.Where(x => x.PackagingId == _DealDetails.PackagingCategory.PackagingId)
                                                             .Select(x => new ODeal.Packaging
                                                             {
                                                                 ReferenceId = x.Id,
                                                                 ReferenceKey = x.Guid,
                                                                 Height = x.Height,
                                                                 Length = x.Length,
                                                                 Name = x.Name,
                                                                 Size_unit = x.SizeUnit,
                                                                 Type = x.Type,
                                                                 Width = x.Width,
                                                                 Weight = x.Weight,
                                                                 Weight_unit = x.WeightUnit,
                                                                 PackagingId = x.PackagingId
                                                             }).FirstOrDefault();
                        }

                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealDetails, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealsList = new List<ODeal.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "running")
                    {
                        //DateTime TodayDate = HCoreHelper.GetGMTDateTime();
                        DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                        //DateTime ActiveTime = HCoreHelper.GetGMTDate().Date;
                        //if (_Request.UserReference.CountryId ==1 )
                        ////DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                        ////if (_Request.UserReference.CountryId == 1 )
                        //{
                        //    ActiveTime = ActiveTime.AddHours(1);
                        //}
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                                    && x.StatusId == HelperStatus.Deals.Published
                                                    && (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate)
                                                    && x.Account.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODeal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountAddress = x.Account.Address,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,

                                                        Title = x.Title,
                                                        Description = x.Description,

                                                        TitleTypeId = x.TitleTypeId,
                                                        TitleContent = x.TitleContent,

                                                        Views = x.Views,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        MerchantAddressId = x.Account.AddressId,
                                                        MerchantAddresskey = x.Account.AddressNavigation.Guid,

                                                        IsFlashDeal = x.MDFlashDeal.Any(a => a.StatusId == HelperStatus.Default.Active),

                                                        IsDealPromoted = x.MDDealPromotion.Any(),
                                                        IsDealPromotedActive = x.MDDealPromotion.Any(d => d.StatusId == HelperStatus.Default.Active),
                                                        DealPromotionLocations = x.MDDealPromotion.Select(d => d.Location).FirstOrDefault(),

                                                        MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                        Locations = x.MDDealLocation.Count(),
                                                        CategoryKey = x.Category.SystemName,
                                                        CategoryName = x.Category.Name,

                                                        SubCategoryKey = x.Subcategory.Guid,
                                                        SubCategoryName = x.Subcategory.Name,

                                                        MaximumUnitSale = x.MaximumUnitSale,

                                                        Budget = (x.MaximumUnitSale * x.Amount),
                                                        TotalPurchase = (long)x.MDDealCode.Sum(a => a.ItemCount),
                                                        TotalAvailable = (x.MaximumUnitSale - x.MDDealCode.Sum(a => a.ItemCount)),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        ApproverId = x.ApproverId,
                                                        ApprovedDate = x.ApprovalDate,
                                                        ApproverComment = x.ApproverComment,

                                                        RejectedById = x.ApproverId,
                                                        RejectedDate = x.ApprovalDate,
                                                        RejectionComment = x.ApproverComment,

                                                        Slug = x.Slug,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealsList = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                                    && x.StatusId == HelperStatus.Deals.Published
                                                    && (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate)
                                                    && x.Account.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODeal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountAddress = x.Account.Address,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        TitleTypeId = x.TitleTypeId,
                                                        TitleContent = x.TitleContent,


                                                        Title = x.Title,
                                                        Description = x.Description,

                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,
                                                        MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                        Views = x.Views,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        MerchantAddressId = x.Account.AddressId,
                                                        MerchantAddresskey = x.Account.AddressNavigation.Guid,

                                                        IsFlashDeal = x.MDFlashDeal.Any(a => a.StatusId == HelperStatus.Default.Active),

                                                        IsDealPromoted = x.MDDealPromotion.Any(),
                                                        IsDealPromotedActive = x.MDDealPromotion.Any(d => d.StatusId == HelperStatus.Default.Active),
                                                        DealPromotionLocations = x.MDDealPromotion.Select(d => d.Location).FirstOrDefault(),

                                                        Locations = x.MDDealLocation.Count(),
                                                        CategoryKey = x.Category.SystemName,
                                                        CategoryName = x.Category.Name,

                                                        SubCategoryKey = x.Subcategory.Guid,
                                                        SubCategoryName = x.Subcategory.Name,
                                                        MaximumUnitSale = x.MaximumUnitSale,
                                                        Budget = (x.MaximumUnitSale * x.Amount),
                                                        TotalPurchase = (long)x.MDDealCode.Sum(a => a.ItemCount),
                                                        TotalAvailable = (x.MaximumUnitSale - x.MDDealCode.Sum(a => a.ItemCount)),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        ApproverId = x.ApproverId,
                                                        ApprovedDate = x.ApprovalDate,
                                                        ApproverComment = x.ApproverComment,

                                                        RejectedById = x.ApproverId,
                                                        RejectedDate = x.ApprovalDate,
                                                        RejectionComment = x.ApproverComment,

                                                        Slug = x.Slug,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealsList)
                        {
                            if (DataItem.TotalAvailable < 0)
                            {
                                DataItem.TotalAvailable = 0;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Poster;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);

                    }
                    else if (_Request.Type == "upcoming")
                    {
                        DateTime TodayDateTime = HCoreHelper.GetGMTDateTime();
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                                    && x.StatusId == HelperStatus.Deals.Published
                                                    && x.StartDate > TodayDateTime
                                                    && x.Account.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODeal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountAddress = x.Account.Address,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,

                                                        Title = x.Title,
                                                        Description = x.Description,

                                                        TitleTypeId = x.TitleTypeId,
                                                        TitleContent = x.TitleContent,

                                                        Views = x.Views,


                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        IsFlashDeal = x.MDFlashDeal.Any(a => a.StatusId == HelperStatus.Default.Active),
                                                        IsDealPromoted = x.MDDealPromotion.Any(),
                                                        IsDealPromotedActive = x.MDDealPromotion.Any(d => d.StatusId == HelperStatus.Default.Active),
                                                        DealPromotionLocations = x.MDDealPromotion.Select(d => d.Location).FirstOrDefault(),


                                                        MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                        Locations = x.MDDealLocation.Count(),
                                                        CategoryKey = x.Category.SystemName,
                                                        CategoryName = x.Category.Name,

                                                        SubCategoryKey = x.Subcategory.Guid,
                                                        SubCategoryName = x.Subcategory.Name,

                                                        MaximumUnitSale = x.MaximumUnitSale,

                                                        Budget = (x.MaximumUnitSale * x.Amount),
                                                        TotalPurchase = (long)x.MDDealCode.Sum(a => a.ItemCount),
                                                        TotalAvailable = (x.MaximumUnitSale - x.MDDealCode.Sum(a => a.ItemCount)),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        ApproverId = x.ApproverId,
                                                        ApprovedDate = x.ApprovalDate,
                                                        ApproverComment = x.ApproverComment,

                                                        RejectedById = x.ApproverId,
                                                        RejectedDate = x.ApprovalDate,
                                                        RejectionComment = x.ApproverComment,

                                                        Slug = x.Slug,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealsList = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                                    && x.StatusId == HelperStatus.Deals.Published
                                                    && x.StartDate > TodayDateTime
                                                    && x.Account.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODeal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountAddress = x.Account.Address,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        TitleTypeId = x.TitleTypeId,
                                                        TitleContent = x.TitleContent,


                                                        Title = x.Title,
                                                        Description = x.Description,

                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,
                                                        MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                        Views = x.Views,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        IsFlashDeal = x.MDFlashDeal.Any(a => a.StatusId == HelperStatus.Default.Active),
                                                        IsDealPromoted = x.MDDealPromotion.Any(),
                                                        IsDealPromotedActive = x.MDDealPromotion.Any(d => d.StatusId == HelperStatus.Default.Active),
                                                        DealPromotionLocations = x.MDDealPromotion.Select(d => d.Location).FirstOrDefault(),

                                                        Locations = x.MDDealLocation.Count(),
                                                        CategoryKey = x.Category.SystemName,
                                                        CategoryName = x.Category.Name,

                                                        SubCategoryKey = x.Subcategory.Guid,
                                                        SubCategoryName = x.Subcategory.Name,
                                                        MaximumUnitSale = x.MaximumUnitSale,
                                                        Budget = (x.MaximumUnitSale * x.Amount),
                                                        TotalPurchase = (long)x.MDDealCode.Sum(a => a.ItemCount),
                                                        TotalAvailable = (x.MaximumUnitSale - x.MDDealCode.Sum(a => a.ItemCount)),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        ApproverId = x.ApproverId,
                                                        ApprovedDate = x.ApprovalDate,
                                                        ApproverComment = x.ApproverComment,

                                                        RejectedById = x.ApproverId,
                                                        RejectedDate = x.ApprovalDate,
                                                        RejectionComment = x.ApproverComment,

                                                        Slug = x.Slug,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealsList)
                        {
                            if (DataItem.TotalAvailable < 0)
                            {
                                DataItem.TotalAvailable = 0;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Poster;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);

                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Select(x => new ODeal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountAddress = x.Account.Address,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        TitleTypeId = x.TitleTypeId,
                                                        TitleContent = x.TitleContent,

                                                        Views = x.Views,

                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,

                                                        Title = x.Title,
                                                        Description = x.Description,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        IsFlashDeal = x.MDFlashDeal.Any(a => a.StatusId == HelperStatus.Default.Active),
                                                        IsDealPromoted = x.MDDealPromotion.Any(),
                                                        IsDealPromotedActive = x.MDDealPromotion.Any(d => d.StatusId == HelperStatus.Default.Active),
                                                        DealPromotionLocations = x.MDDealPromotion.Select(d => d.Location).FirstOrDefault(),


                                                        MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                        Locations = x.MDDealLocation.Count(),
                                                        CategoryKey = x.Category.SystemName,
                                                        CategoryName = x.Category.Name,

                                                        SubCategoryKey = x.Subcategory.Guid,
                                                        SubCategoryName = x.Subcategory.Name,
                                                        MaximumUnitSale = x.MaximumUnitSale,

                                                        Budget = (x.MaximumUnitSale * x.Amount),
                                                        TotalPurchase = (long)x.MDDealCode.Sum(a => a.ItemCount),
                                                        TotalAvailable = (x.MaximumUnitSale - x.MDDealCode.Sum(a => a.ItemCount)),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        ApproverId = x.ApproverId,
                                                        ApprovedDate = x.ApprovalDate,
                                                        ApproverComment = x.ApproverComment,

                                                        RejectedById = x.ApproverId,
                                                        RejectedDate = x.ApprovalDate,
                                                        RejectionComment = x.ApproverComment,

                                                        Slug = x.Slug,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealsList = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Select(x => new ODeal.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountAddress = x.Account.Address,
                                                        Views = x.Views,

                                                        TypeCode = x.Type.SystemName,
                                                        TypeName = x.Type.Name,

                                                        Title = x.Title,
                                                        Description = x.Description,
                                                        TitleTypeId = x.TitleTypeId,
                                                        TitleContent = x.TitleContent,


                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,
                                                        MerchantAmount = x.SellingPrice - x.CommissionAmount,


                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        IsFlashDeal = x.MDFlashDeal.Any(a => a.StatusId == HelperStatus.Default.Active),
                                                        IsDealPromoted = x.MDDealPromotion.Any(),
                                                        IsDealPromotedActive = x.MDDealPromotion.Any(d => d.StatusId == HelperStatus.Default.Active),
                                                        DealPromotionLocations = x.MDDealPromotion.Select(d => d.Location).FirstOrDefault(),

                                                        Locations = x.MDDealLocation.Count(),
                                                        CategoryKey = x.Category.SystemName,
                                                        CategoryName = x.Category.Name,

                                                        SubCategoryKey = x.Subcategory.Guid,
                                                        SubCategoryName = x.Subcategory.Name,
                                                        MaximumUnitSale = x.MaximumUnitSale,
                                                        Budget = (x.MaximumUnitSale * x.Amount),
                                                        TotalPurchase = (long)x.MDDealCode.Sum(a => a.ItemCount),
                                                        TotalAvailable = (x.MaximumUnitSale - x.MDDealCode.Sum(a => a.ItemCount)),
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        ApproverId = x.ApproverId,
                                                        ApprovedDate = x.ApprovalDate,
                                                        ApproverComment = x.ApproverComment,

                                                        RejectedById = x.ApproverId,
                                                        RejectedDate = x.ApprovalDate,
                                                        RejectionComment = x.ApproverComment,

                                                        Slug = x.Slug,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealsList)
                        {
                            if (DataItem.TotalAvailable < 0)
                            {
                                DataItem.TotalAvailable = 0;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Poster;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);

                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the flash deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetFlashDeal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _FlashDealsList = new List<OFlashDeal.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDFlashDeal
                                                .Where(x => x.Deal.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OFlashDeal.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,

                                                    AccountId = x.Deal.AccountId,
                                                    AccountKey = x.Deal.Account.Guid,
                                                    AccountDisplayName = x.Deal.Account.DisplayName,

                                                    TypeCode = x.Deal.Type.SystemName,
                                                    TypeName = x.Deal.Type.Name,



                                                    Title = x.Deal.Title,
                                                    Description = x.Deal.Description,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,

                                                    MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    DealTypeId = x.Deal.DealTypeId,
                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DealTypeName = x.Deal.DealType.Name,
                                                    TotalPurchase = x.Deal.MDDealCode.Count(),

                                                    Locations = x.Deal.MDDealLocation.Count(),
                                                    CategoryKey = x.Deal.Category.SystemName,
                                                    CategoryName = x.Deal.Category.Name,

                                                    SubCategoryKey = x.Deal.Subcategory.Guid,
                                                    SubCategoryName = x.Deal.Subcategory.Name,
                                                    MaximumUnitSale = x.Deal.MaximumUnitSale,

                                                    Budget = (x.Deal.MaximumUnitSale * x.Deal.Amount),

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _FlashDealsList = _HCoreContext.MDFlashDeal
                                                .Where(x => x.Deal.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OFlashDeal.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,

                                                    AccountId = x.Deal.AccountId,
                                                    AccountKey = x.Deal.Account.Guid,
                                                    AccountDisplayName = x.Deal.Account.DisplayName,

                                                    TypeCode = x.Deal.Type.SystemName,
                                                    TypeName = x.Deal.Type.Name,

                                                    Title = x.Deal.Title,
                                                    Description = x.Deal.Description,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,
                                                    MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,


                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    DealTypeId = x.Deal.DealTypeId,
                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DealTypeName = x.Deal.DealType.Name,
                                                    TotalPurchase = x.Deal.MDDealCode.Count(),


                                                    Locations = x.Deal.MDDealLocation.Count(),
                                                    CategoryKey = x.Deal.Category.SystemName,
                                                    CategoryName = x.Deal.Category.Name,

                                                    SubCategoryKey = x.Deal.Subcategory.Guid,
                                                    SubCategoryName = x.Deal.Subcategory.Name,
                                                    MaximumUnitSale = x.Deal.MaximumUnitSale,

                                                    Budget = (x.Deal.MaximumUnitSale * x.Deal.Amount),

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _FlashDealsList)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                        else
                        {
                            DataItem.ImageUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _FlashDealsList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealCustomer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealsList = new List<ODeal.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDeal
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.StatusId == HelperStatus.Deals.Published)
                                                .Select(x => new ODeal.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,

                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    TitleTypeId = x.TitleTypeId,
                                                    TitleContent = x.TitleContent,


                                                    Title = x.Title,
                                                    Description = x.Description,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                    ImageUrl = x.PosterStorage.Path,

                                                    Locations = x.MDDealLocation.Count(),
                                                    CategoryKey = x.Category.SystemName,
                                                    CategoryName = x.Category.Name,


                                                    SubCategoryKey = x.Subcategory.Guid,
                                                    SubCategoryName = x.Subcategory.Name,

                                                    MaximumUnitSale = x.MaximumUnitSale,

                                                    Budget = (x.MaximumUnitSale * x.Amount),

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _DealsList = _HCoreContext.MDDeal
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new ODeal.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,

                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    TitleTypeId = x.TitleTypeId,
                                                    TitleContent = x.TitleContent,



                                                    Title = x.Title,
                                                    Description = x.Description,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    MerchantAmount = x.SellingPrice - x.CommissionAmount,

                                                    ImageUrl = x.PosterStorage.Path,

                                                    Locations = x.MDDealLocation.Count(),
                                                    CategoryKey = x.Category.SystemName,
                                                    CategoryName = x.Category.Name,

                                                    SubCategoryKey = x.Subcategory.Guid,
                                                    SubCategoryName = x.Subcategory.Name,
                                                    MaximumUnitSale = x.MaximumUnitSale,

                                                    Budget = (x.MaximumUnitSale * x.Amount),

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _DealsList)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                        else
                        {
                            DataItem.ImageUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the purchase history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPurchaseHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealCodeList = new List<ODealCode.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ReferenceId != 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.DealId == _Request.ReferenceId
                                                    && x.Deal.Guid == _Request.ReferenceKey)
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,

                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        ToAccountId = x.SecondaryAccountId,
                                                        ToAccountKey = x.SecondaryAccount.Guid,
                                                        //ToAccountDisplayName = x.SecondaryAccount.DisplayName,
                                                        //ToAccountMobileNumber = x.SecondaryAccount.MobileNumber,
                                                        //ToAccountIconUrl = x.SecondaryAccount.IconStorage.Path,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,
                                                        DealTypeId = x.Deal.DealType.Id,
                                                        DealTypeCode = x.Deal.DealType.SystemName,
                                                        DealTypeName = x.Deal.DealType.Name,
                                                        DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                        DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,
                                                        LastUseLocationSourceCode = x.LastUseSourceNavigation.SystemName,
                                                        LastUseLocationSourceName = x.LastUseSourceNavigation.Name,

                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealCodeList = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.DealId == _Request.ReferenceId
                                                    && x.Deal.Guid == _Request.ReferenceKey)
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        ToAccountId = x.SecondaryAccountId,
                                                        ToAccountKey = x.SecondaryAccount.Guid,
                                                        //ToAccountDisplayName = x.SecondaryAccount.DisplayName,
                                                        //ToAccountMobileNumber = x.SecondaryAccount.MobileNumber,
                                                        //ToAccountIconUrl = x.SecondaryAccount.IconStorage.Path,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,
                                                        DealTypeId = x.Deal.DealType.Id,
                                                        DealTypeCode = x.Deal.DealType.SystemName,
                                                        DealTypeName = x.Deal.DealType.Name,
                                                        DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                        DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,
                                                        LastUseLocationSourceCode = x.LastUseSourceNavigation.SystemName,
                                                        LastUseLocationSourceName = x.LastUseSourceNavigation.Name,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealCodeList)
                        {
                            if (!string.IsNullOrEmpty(DataItem.DealCodeEnd))
                            {
                                DataItem.DealCodeEnd = "XXXXXXXXXXX";
                            }
                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealCodeList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        if (_Request.AccountId != 0)
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                        .Where(x => x.AccountId == _Request.AccountId)
                                                        .Select(x => new ODealCode.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            ToAccountId = x.SecondaryAccountId,
                                                            ToAccountKey = x.SecondaryAccount.Guid,
                                                            MerchantId = x.Deal.AccountId,
                                                            MerchantKey = x.Deal.Account.Guid,
                                                            MerchantDisplayName = x.Deal.Account.DisplayName,

                                                            Title = x.Deal.Title,
                                                            Description = x.Deal.Description,
                                                            ImageUrl = x.Deal.PosterStorage.Path,

                                                            MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                            Quantity = x.ItemCount,
                                                            DealCodeAmount = x.Amount,
                                                            PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                            DealPurchaseDate = x.CreateDate,

                                                            DealCodeEnd = x.ItemCode,
                                                            Amount = x.ItemAmount,

                                                            DealId = x.DealId,
                                                            DealKey = x.Deal.Guid,
                                                            DealTypeId = x.Deal.DealType.Id,
                                                            DealTypeCode = x.Deal.DealType.SystemName,
                                                            DealTypeName = x.Deal.DealType.Name,
                                                            DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                            DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                            DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                            PartnerId = x.PartnerId,
                                                            PartnerKey = x.Partner.Guid,
                                                            PartnerDisplayName = x.Partner.DisplayName,
                                                            PartnerMobileNumber = x.Partner.MobileNumber,
                                                            PartnerComissionAmount = x.PartnerComissionAmount,

                                                            StartDate = x.StartDate,
                                                            EndDate = x.EndDate,

                                                            LastUseDate = x.LastUseDate,
                                                            LastUseLocationId = x.LastUseLocationId,
                                                            LastUseLocationKey = x.LastUseLocation.Guid,
                                                            LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                            LastUseLocationAddress = x.LastUseLocation.Address,
                                                            LastUseLocationSourceCode = x.LastUseSourceNavigation.SystemName,
                                                            LastUseLocationSourceName = x.LastUseSourceNavigation.Name,
                                                            CashierId = x.CashierId,
                                                            CashierKey = x.Cashier.Guid,
                                                            CashierDisplayName = x.Cashier.DisplayName,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                            DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                            DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                            DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                            DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                            DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                            CarrierId = x.Order.CarrierId,
                                                            CarrierKey = x.Order.Carrier.Guid,
                                                            CarrierName = x.Order.Carrier.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _DealCodeList = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                        .Where(x => x.AccountId == _Request.AccountId)
                                                        .Select(x => new ODealCode.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            ToAccountId = x.SecondaryAccountId,
                                                            ToAccountKey = x.SecondaryAccount.Guid,
                                                            MerchantId = x.Deal.AccountId,
                                                            MerchantKey = x.Deal.Account.Guid,
                                                            MerchantDisplayName = x.Deal.Account.DisplayName,

                                                            Quantity = x.ItemCount,
                                                            DealCodeAmount = x.Amount,
                                                            PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                            DealPurchaseDate = x.CreateDate,

                                                            Title = x.Deal.Title,
                                                            Description = x.Deal.Description,
                                                            ImageUrl = x.Deal.PosterStorage.Path,

                                                            DealCodeEnd = x.ItemCode,
                                                            Amount = x.ItemAmount,

                                                            DealId = x.DealId,
                                                            DealKey = x.Deal.Guid,
                                                            MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,
                                                            DealTypeId = x.Deal.DealType.Id,
                                                            DealTypeCode = x.Deal.DealType.SystemName,
                                                            DealTypeName = x.Deal.DealType.Name,
                                                            DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                            DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                            DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                            PartnerId = x.PartnerId,
                                                            PartnerKey = x.Partner.Guid,
                                                            PartnerDisplayName = x.Partner.DisplayName,
                                                            PartnerMobileNumber = x.Partner.MobileNumber,
                                                            PartnerComissionAmount = x.PartnerComissionAmount,

                                                            StartDate = x.StartDate,
                                                            EndDate = x.EndDate,

                                                            LastUseDate = x.LastUseDate,
                                                            LastUseLocationId = x.LastUseLocationId,
                                                            LastUseLocationKey = x.LastUseLocation.Guid,
                                                            LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                            LastUseLocationAddress = x.LastUseLocation.Address,
                                                            LastUseLocationSourceCode = x.LastUseSourceNavigation.SystemName,
                                                            LastUseLocationSourceName = x.LastUseSourceNavigation.Name,
                                                            CashierId = x.CashierId,
                                                            CashierKey = x.Cashier.Guid,
                                                            CashierDisplayName = x.Cashier.DisplayName,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                            DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                            DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                            DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                            DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                            DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                            CarrierId = x.Order.CarrierId,
                                                            CarrierKey = x.Order.Carrier.Guid,
                                                            CarrierName = x.Order.Carrier.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                        }
                        else
                        {
                            if (_Request.RefreshCount)
                            {
                                #region Total Records
                                _Request.TotalRecords = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                        .Select(x => new ODealCode.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            ToAccountId = x.SecondaryAccountId,
                                                            ToAccountKey = x.SecondaryAccount.Guid,
                                                            MerchantId = x.Deal.AccountId,
                                                            MerchantKey = x.Deal.Account.Guid,
                                                            MerchantDisplayName = x.Deal.Account.DisplayName,

                                                            Title = x.Deal.Title,
                                                            Description = x.Deal.Description,
                                                            ImageUrl = x.Deal.PosterStorage.Path,

                                                            AccountId = x.AccountId,
                                                            AccountKey = x.Account.Guid,
                                                            AccountDisplayName = x.Account.DisplayName,
                                                            AccountMobileNumber = x.Account.MobileNumber,
                                                            AccountIconUrl = x.Account.IconStorage.Path,

                                                            Quantity = x.ItemCount,
                                                            DealCodeAmount = x.Amount,
                                                            PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                            DealPurchaseDate = x.CreateDate,

                                                            DealCodeEnd = x.ItemCode,
                                                            Amount = x.ItemAmount,
                                                            MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                            DealId = x.DealId,
                                                            DealKey = x.Deal.Guid,
                                                            DealTypeId = x.Deal.DealType.Id,
                                                            DealTypeCode = x.Deal.DealType.SystemName,
                                                            DealTypeName = x.Deal.DealType.Name,
                                                            DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                            DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                            DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                            PartnerId = x.PartnerId,
                                                            PartnerKey = x.Partner.Guid,
                                                            PartnerDisplayName = x.Partner.DisplayName,
                                                            PartnerMobileNumber = x.Partner.MobileNumber,
                                                            PartnerComissionAmount = x.PartnerComissionAmount,

                                                            StartDate = x.StartDate,
                                                            EndDate = x.EndDate,

                                                            LastUseDate = x.LastUseDate,
                                                            LastUseLocationId = x.LastUseLocationId,
                                                            LastUseLocationKey = x.LastUseLocation.Guid,
                                                            LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                            LastUseLocationAddress = x.LastUseLocation.Address,
                                                            LastUseLocationSourceCode = x.LastUseSourceNavigation.SystemName,
                                                            LastUseLocationSourceName = x.LastUseSourceNavigation.Name,
                                                            CashierId = x.CashierId,
                                                            CashierKey = x.Cashier.Guid,
                                                            CashierDisplayName = x.Cashier.DisplayName,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                            DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                            DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                            DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                            DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                            DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                            CarrierId = x.Order.CarrierId,
                                                            CarrierKey = x.Order.Carrier.Guid,
                                                            CarrierName = x.Order.Carrier.Name,
                                                        })
                                                       .Where(_Request.SearchCondition)
                                               .Count();
                                #endregion
                            }
                            #region Get Data
                            _DealCodeList = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                        .Select(x => new ODealCode.List
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            ToAccountId = x.SecondaryAccountId,
                                                            ToAccountKey = x.SecondaryAccount.Guid,
                                                            MerchantId = x.Deal.AccountId,
                                                            MerchantKey = x.Deal.Account.Guid,
                                                            MerchantDisplayName = x.Deal.Account.DisplayName,

                                                            Title = x.Deal.Title,
                                                            Description = x.Deal.Description,
                                                            ImageUrl = x.Deal.PosterStorage.Path,

                                                            AccountId = x.AccountId,
                                                            AccountKey = x.Account.Guid,
                                                            AccountDisplayName = x.Account.DisplayName,
                                                            AccountMobileNumber = x.Account.MobileNumber,
                                                            AccountIconUrl = x.Account.IconStorage.Path,

                                                            Quantity = x.ItemCount,
                                                            DealCodeAmount = x.Amount,
                                                            PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                            DealPurchaseDate = x.CreateDate,

                                                            DealCodeEnd = x.ItemCode,
                                                            Amount = x.ItemAmount,
                                                            MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                            DealId = x.DealId,
                                                            DealKey = x.Deal.Guid,
                                                            DealTypeId = x.Deal.DealType.Id,
                                                            DealTypeCode = x.Deal.DealType.SystemName,
                                                            DealTypeName = x.Deal.DealType.Name,
                                                            DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                            DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                            DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                            PartnerId = x.PartnerId,
                                                            PartnerKey = x.Partner.Guid,
                                                            PartnerDisplayName = x.Partner.DisplayName,
                                                            PartnerMobileNumber = x.Partner.MobileNumber,
                                                            PartnerComissionAmount = x.PartnerComissionAmount,

                                                            StartDate = x.StartDate,
                                                            EndDate = x.EndDate,

                                                            LastUseDate = x.LastUseDate,
                                                            LastUseLocationId = x.LastUseLocationId,
                                                            LastUseLocationKey = x.LastUseLocation.Guid,
                                                            LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                            LastUseLocationAddress = x.LastUseLocation.Address,
                                                            LastUseLocationSourceCode = x.LastUseSourceNavigation.SystemName,
                                                            LastUseLocationSourceName = x.LastUseSourceNavigation.Name,
                                                            CashierId = x.CashierId,
                                                            CashierKey = x.Cashier.Guid,
                                                            CashierDisplayName = x.Cashier.DisplayName,

                                                            CreateDate = x.CreateDate,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                            DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                            DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                            DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                            DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                            DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                            CarrierId = x.Order.CarrierId,
                                                            CarrierKey = x.Order.Carrier.Guid,
                                                            CarrierName = x.Order.Carrier.Name,
                                                        })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                            #endregion
                        }
                        foreach (var DataItem in _DealCodeList)
                        {
                            if (!string.IsNullOrEmpty(DataItem.DealCodeEnd))
                            {
                                DataItem.DealCodeEnd = "XXXXXXXXXXX";
                            }

                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealCodeList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPurchaseHistory", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the purchase details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPurchaseDetails(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealCodeDetails = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                   .Where(x => x.Id == _Request.ReferenceId
                                                   && x.Guid == _Request.ReferenceKey)
                                                   .Select(x => new ODealCode.List
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,

                                                       AccountId = x.AccountId,
                                                       AccountKey = x.Account.Guid,
                                                       AccountDisplayName = x.Account.DisplayName,
                                                       AccountMobileNumber = x.Account.MobileNumber,
                                                       AccountIconUrl = x.Account.IconStorage.Path,
                                                       AccountAddress = x.Account.Address,
                                                       MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,
                                                       MerchantDisplayName = x.Deal.Account.DisplayName,
                                                       MerchantId = x.Deal.AccountId,
                                                       MerchantKey = x.Deal.Account.Guid,

                                                       TerminalReferenceId = x.Terminal.Id,
                                                       TerminalReferenceKey = x.Terminal.Guid,
                                                       TerminalId = x.Terminal.DisplayName,

                                                       ToAccountId = x.SecondaryAccountId,
                                                       ToAccountKey = x.SecondaryAccount.Guid,
                                                       ToAccountDisplayName = x.SecondaryAccount.DisplayName,
                                                       ToAccountMobileNumber = x.SecondaryAccount.MobileNumber,
                                                       ToAccountIconUrl = x.SecondaryAccount.IconStorage.Path,

                                                       Quantity = x.ItemCount,
                                                       DealCodeAmount = x.Amount,
                                                       PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                       DealPurchaseDate = x.CreateDate,

                                                       DealCodeEnd = x.ItemCode,
                                                       Amount = x.ItemAmount,

                                                       StartDate = x.StartDate,
                                                       EndDate = x.EndDate,

                                                       LastUseDate = x.LastUseDate,
                                                       LastUseLocationId = x.LastUseLocationId,
                                                       LastUseLocationKey = x.LastUseLocation.Guid,
                                                       LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                       LastUseLocationAddress = x.LastUseLocation.Address,
                                                       LastUseLocationSourceCode = x.LastUseSourceNavigation.SystemName,
                                                       LastUseLocationSourceName = x.LastUseSourceNavigation.Name,
                                                       CreateDate = x.CreateDate,
                                                       StatusCode = x.Status.SystemName,
                                                       StatusName = x.Status.Name,

                                                       CashierDisplayName = x.Cashier.DisplayName,
                                                       CashierId = x.CashierId,
                                                       CashierKey = x.Cashier.Guid,

                                                       PartnerId = x.PartnerId,
                                                       PartnerKey = x.Partner.Guid,
                                                       PartnerDisplayName = x.Partner.DisplayName,
                                                       PartnerMobileNumber = x.Partner.MobileNumber,
                                                       PartnerComissionAmount = x.PartnerComissionAmount,

                                                       DealId = x.DealId,
                                                       DealKey = x.Deal.Guid,
                                                       Title = x.Deal.Title,
                                                       Description = x.Deal.Description,
                                                       PaymentReference = x.TransactionId,
                                                       DealTypeId = x.Deal.DealType.Id,
                                                       DealTypeCode = x.Deal.DealType.SystemName,
                                                       DealTypeName = x.Deal.DealType.Name,
                                                       DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                       DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                       DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                       DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                       DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                       DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                       DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                       DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                       CarrierId = x.Order.CarrierId,
                                                       CarrierKey = x.Order.Carrier.Guid,
                                                       CarrierName = x.Order.Carrier.Name,
                                                   }).FirstOrDefault();
                    if (_DealCodeDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_DealCodeDetails.DealCodeEnd))
                        {
                            _DealCodeDetails.DealCodeEnd = "XXXXXXXXXXX";
                        }

                        if (!string.IsNullOrEmpty(_DealCodeDetails.ImageUrl))
                        {
                            _DealCodeDetails.ImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                        }
                        else
                        {
                            _DealCodeDetails.ImageUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(_DealCodeDetails.AccountIconUrl))
                        {
                            _DealCodeDetails.AccountIconUrl = _AppConfig.StorageUrl + _DealCodeDetails.AccountIconUrl;
                        }
                        else
                        {
                            _DealCodeDetails.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_DealCodeDetails.ToAccountIconUrl))
                        {
                            _DealCodeDetails.ToAccountIconUrl = _AppConfig.StorageUrl + _DealCodeDetails.ToAccountIconUrl;
                        }
                        else
                        {
                            _DealCodeDetails.ToAccountIconUrl = _AppConfig.Default_Icon;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCodeDetails, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetPurchaseDetails", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion 
        }
        /// <summary>
        /// Description: Gets the purchase history overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPurchaseHistoryOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealCodeList = new List<ODealCode.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ReferenceId != 0)
                    {
                        #region Total Records
                        long? Transactions = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.DealId == _Request.ReferenceId
                                                && x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODealCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                    DealCodeEnd = x.ItemCode,
                                                    Amount = x.ItemAmount,

                                                    Title = x.Deal.Title,
                                                    Quantity = x.ItemCount,
                                                    DealCodeAmount = x.Amount,
                                                    PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                    DealPurchaseDate = x.CreateDate,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    DealTypeId = x.Deal.DealType.Id,
                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DealTypeName = x.Deal.DealType.Name,
                                                    DeliveryTypeId = x.Deal.DeliveryType.Id,
                                                    DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                    DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                    LastUseDate = x.LastUseDate,
                                                    LastUseLocationId = x.LastUseLocationId,
                                                    LastUseLocationKey = x.LastUseLocation.Guid,
                                                    LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                    LastUseLocationAddress = x.LastUseLocation.Address,

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                    DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                    DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                    DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                    DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                    CarrierId = x.Order.CarrierId,
                                                    CarrierKey = x.Order.Carrier.Guid,
                                                    CarrierName = x.Order.Carrier.Name,

                                                    PartnerId = x.PartnerId,
                                                    PartnerKey = x.Partner.Guid,
                                                    PartnerDisplayName = x.Partner.DisplayName,
                                                    PartnerMobileNumber = x.Partner.MobileNumber,
                                                    PartnerComissionAmount = x.PartnerComissionAmount,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Sum(x => x.Quantity);
                        double? Amount = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.DealId == _Request.ReferenceId
                                                && x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODealCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                    Title = x.Deal.Title,
                                                    Quantity = x.ItemCount,
                                                    DealCodeAmount = x.Amount,
                                                    PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                    DealPurchaseDate = x.CreateDate,

                                                    DealCodeEnd = x.ItemCode,
                                                    Amount = x.ItemAmount,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    LastUseDate = x.LastUseDate,
                                                    LastUseLocationId = x.LastUseLocationId,
                                                    LastUseLocationKey = x.LastUseLocation.Guid,
                                                    LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                    LastUseLocationAddress = x.LastUseLocation.Address,

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                    DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                    DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                    DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                    DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                    CarrierId = x.Order.CarrierId,
                                                    CarrierKey = x.Order.Carrier.Guid,
                                                    CarrierName = x.Order.Carrier.Name,

                                                    PartnerId = x.PartnerId,
                                                    PartnerKey = x.Partner.Guid,
                                                    PartnerDisplayName = x.Partner.DisplayName,
                                                    PartnerMobileNumber = x.Partner.MobileNumber,
                                                    PartnerComissionAmount = x.PartnerComissionAmount,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Sum(x => x.DealCodeAmount);

                        double? SuccessAmount = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.DealId == _Request.ReferenceId
                                                && x.Deal.Guid == _Request.ReferenceKey
                                                && x.StatusId == HelperStatus.DealCodes.Used)
                                                .Select(x => new ODealCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                    Title = x.Deal.Title,
                                                    Quantity = x.ItemCount,
                                                    DealCodeAmount = x.Amount,
                                                    PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                    DealPurchaseDate = x.CreateDate,

                                                    DealCodeEnd = x.ItemCode,
                                                    Amount = x.ItemAmount,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    LastUseDate = x.LastUseDate,
                                                    LastUseLocationId = x.LastUseLocationId,
                                                    LastUseLocationKey = x.LastUseLocation.Guid,
                                                    LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                    LastUseLocationAddress = x.LastUseLocation.Address,

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                    DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                    DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                    DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                    DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                    CarrierId = x.Order.CarrierId,
                                                    CarrierKey = x.Order.Carrier.Guid,
                                                    CarrierName = x.Order.Carrier.Name,

                                                    PartnerId = x.PartnerId,
                                                    PartnerKey = x.Partner.Guid,
                                                    PartnerDisplayName = x.Partner.DisplayName,
                                                    PartnerMobileNumber = x.Partner.MobileNumber,
                                                    PartnerComissionAmount = x.PartnerComissionAmount,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Sum(x => x.DealCodeAmount);

                        double? FailedAmount = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.DealId == _Request.ReferenceId
                                                && x.Deal.Guid == _Request.ReferenceKey
                                                && (x.StatusId == HelperStatus.DealCodes.Expired || x.StatusId == HelperStatus.DealCodes.Blocked))
                                                .Select(x => new ODealCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                    Title = x.Deal.Title,
                                                    Quantity = x.ItemCount,
                                                    DealCodeAmount = x.Amount,
                                                    PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                    DealPurchaseDate = x.CreateDate,

                                                    DealCodeEnd = x.ItemCode,
                                                    Amount = x.ItemAmount,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,

                                                    LastUseDate = x.LastUseDate,
                                                    LastUseLocationId = x.LastUseLocationId,
                                                    LastUseLocationKey = x.LastUseLocation.Guid,
                                                    LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                    LastUseLocationAddress = x.LastUseLocation.Address,

                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,

                                                    DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                    DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                    DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                    DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                    DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                    CarrierId = x.Order.CarrierId,
                                                    CarrierKey = x.Order.Carrier.Guid,
                                                    CarrierName = x.Order.Carrier.Name,

                                                    PartnerId = x.PartnerId,
                                                    PartnerKey = x.Partner.Guid,
                                                    PartnerDisplayName = x.Partner.DisplayName,
                                                    PartnerMobileNumber = x.Partner.MobileNumber,
                                                    PartnerComissionAmount = x.PartnerComissionAmount,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Sum(x => x.DealCodeAmount);
                        #endregion
                        var _Response = new
                        {
                            Transactions = Transactions,
                            Amount = Amount,
                            SuccessfulTransactionsAmount = SuccessAmount,
                            FailedTransactionsAmount = FailedAmount,
                        };
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        if (_Request.AccountId != 0)
                        {
                            #region Total Records
                            long? Transactions = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.AccountId == _Request.AccountId)
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        Title = x.Deal.Title,
                                                        Description = x.Deal.Description,
                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,

                                                        CashierId = x.CashierId,
                                                        CashierKey = x.Cashier.Guid,
                                                        CashierDisplayName = x.Cashier.DisplayName,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,

                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                       .Sum(x => x.Quantity);
                            double? Amount = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.AccountId == _Request.AccountId)
                                                     .Select(x => new ODealCode.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         MerchantId = x.Deal.AccountId,
                                                         MerchantKey = x.Deal.Account.Guid,
                                                         MerchantDisplayName = x.Deal.Account.DisplayName,

                                                         Title = x.Deal.Title,
                                                         Description = x.Deal.Description,
                                                         ImageUrl = x.Deal.PosterStorage.Path,
                                                         MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                         Quantity = x.ItemCount,
                                                         DealCodeAmount = x.Amount,
                                                         PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                         DealPurchaseDate = x.CreateDate,

                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                         AccountDisplayName = x.Account.DisplayName,
                                                         AccountMobileNumber = x.Account.MobileNumber,
                                                         AccountIconUrl = x.Account.IconStorage.Path,

                                                         DealCodeEnd = x.ItemCode,
                                                         Amount = x.ItemAmount,

                                                         StartDate = x.StartDate,
                                                         EndDate = x.EndDate,

                                                         LastUseDate = x.LastUseDate,
                                                         LastUseLocationId = x.LastUseLocationId,
                                                         LastUseLocationKey = x.LastUseLocation.Guid,
                                                         LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                         LastUseLocationAddress = x.LastUseLocation.Address,

                                                         CashierId = x.CashierId,
                                                         CashierKey = x.Cashier.Guid,
                                                         CashierDisplayName = x.Cashier.DisplayName,

                                                         DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                         DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                         DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                         DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                         DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                         CarrierId = x.Order.CarrierId,
                                                         CarrierKey = x.Order.Carrier.Guid,
                                                         CarrierName = x.Order.Carrier.Name,

                                                         CreateDate = x.CreateDate,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,

                                                         PartnerId = x.PartnerId,
                                                         PartnerKey = x.Partner.Guid,
                                                         PartnerDisplayName = x.Partner.DisplayName,
                                                         PartnerMobileNumber = x.Partner.MobileNumber,
                                                         PartnerComissionAmount = x.PartnerComissionAmount,
                                                     })
                                                    .Where(_Request.SearchCondition)
                                            .Sum(x => x.DealCodeAmount);

                            double? SuccessAmount = _HCoreContext.MDDealCode
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.DealId == _Request.ReferenceId
                                                    && x.Deal.Guid == _Request.ReferenceKey
                                                    && x.StatusId == HelperStatus.DealCodes.Used)
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        Title = x.Deal.Title,
                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,

                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Sum(x => x.DealCodeAmount);

                            double? FailedAmount = _HCoreContext.MDDealCode
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.DealId == _Request.ReferenceId
                                                    && x.Deal.Guid == _Request.ReferenceKey
                                                    && (x.StatusId == HelperStatus.DealCodes.Expired || x.StatusId == HelperStatus.DealCodes.Blocked))
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        Title = x.Deal.Title,
                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,

                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Sum(x => x.DealCodeAmount);
                            #endregion
                            var _Response = new
                            {
                                Transactions = Transactions,
                                Amount = Amount,
                                SuccessfulTransactionsAmount = SuccessAmount,
                                FailedTransactionsAmount = FailedAmount,
                            };
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCD0200", ResponseCode.HCD0200);

                        }
                        else
                        {
                            #region Total Records
                            long? Transactions = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        Title = x.Deal.Title,
                                                        Description = x.Deal.Description,
                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,

                                                        CashierId = x.CashierId,
                                                        CashierKey = x.Cashier.Guid,
                                                        CashierDisplayName = x.Cashier.DisplayName,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                       .Sum(x => x.Quantity);
                            double? Amount = _HCoreContext.MDDealCode
                                                .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                     .Select(x => new ODealCode.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         MerchantId = x.Deal.AccountId,
                                                         MerchantKey = x.Deal.Account.Guid,
                                                         MerchantDisplayName = x.Deal.Account.DisplayName,

                                                         Title = x.Deal.Title,
                                                         Description = x.Deal.Description,
                                                         ImageUrl = x.Deal.PosterStorage.Path,

                                                         AccountId = x.AccountId,
                                                         AccountKey = x.Account.Guid,
                                                         AccountDisplayName = x.Account.DisplayName,
                                                         AccountMobileNumber = x.Account.MobileNumber,
                                                         AccountIconUrl = x.Account.IconStorage.Path,
                                                         MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                         Quantity = x.ItemCount,
                                                         DealCodeAmount = x.Amount,
                                                         PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                         DealPurchaseDate = x.CreateDate,

                                                         DealCodeEnd = x.ItemCode,
                                                         Amount = x.ItemAmount,

                                                         StartDate = x.StartDate,
                                                         EndDate = x.EndDate,

                                                         LastUseDate = x.LastUseDate,
                                                         LastUseLocationId = x.LastUseLocationId,
                                                         LastUseLocationKey = x.LastUseLocation.Guid,
                                                         LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                         LastUseLocationAddress = x.LastUseLocation.Address,

                                                         CashierId = x.CashierId,
                                                         CashierKey = x.Cashier.Guid,
                                                         CashierDisplayName = x.Cashier.DisplayName,

                                                         DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                         DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                         DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                         DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                         DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                         CarrierId = x.Order.CarrierId,
                                                         CarrierKey = x.Order.Carrier.Guid,
                                                         CarrierName = x.Order.Carrier.Name,

                                                         CreateDate = x.CreateDate,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,

                                                         PartnerId = x.PartnerId,
                                                         PartnerKey = x.Partner.Guid,
                                                         PartnerDisplayName = x.Partner.DisplayName,
                                                         PartnerMobileNumber = x.Partner.MobileNumber,
                                                         PartnerComissionAmount = x.PartnerComissionAmount,
                                                     })
                                                    .Where(_Request.SearchCondition)
                                            .Sum(x => x.DealCodeAmount);

                            double? SuccessAmount = _HCoreContext.MDDealCode
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry && x.StatusId == HelperStatus.DealCodes.Used)
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        Title = x.Deal.Title,
                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,

                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Sum(x => x.MerchantAmount);

                            double? FailedAmount = _HCoreContext.MDDealCode
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry && (x.StatusId == HelperStatus.DealCodes.Expired || x.StatusId == HelperStatus.DealCodes.Blocked))
                                                    .Select(x => new ODealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantKey = x.Deal.Account.Guid,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,

                                                        AccountId = x.AccountId,
                                                        AccountKey = x.Account.Guid,
                                                        AccountDisplayName = x.Account.DisplayName,
                                                        AccountMobileNumber = x.Account.MobileNumber,
                                                        AccountIconUrl = x.Account.IconStorage.Path,
                                                        MerchantAmount = x.Deal.SellingPrice - x.Deal.CommissionAmount,

                                                        Title = x.Deal.Title,
                                                        Quantity = x.ItemCount,
                                                        DealCodeAmount = x.Amount,
                                                        PaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,
                                                        DealPurchaseDate = x.CreateDate,

                                                        DealCodeEnd = x.ItemCode,
                                                        Amount = x.ItemAmount,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        LastUseDate = x.LastUseDate,
                                                        LastUseLocationId = x.LastUseLocationId,
                                                        LastUseLocationKey = x.LastUseLocation.Guid,
                                                        LastUseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                        LastUseLocationAddress = x.LastUseLocation.Address,

                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        DeliveryPartnerId = x.Order.DeliveryPartnerId,
                                                        DeliveryPartnerKey = x.Order.DeliveryPartner.Guid,
                                                        DeliveryPartnerDisplayName = x.Order.DeliveryPartner.DisplayName,
                                                        DeliveryPartnerMobileNumber = x.Order.DeliveryPartner.MobileNumber,
                                                        DeliveryPartnerEmailAddress = x.Order.DeliveryPartner.MobileNumber,

                                                        CarrierId = x.Order.CarrierId,
                                                        CarrierKey = x.Order.Carrier.Guid,
                                                        CarrierName = x.Order.Carrier.Name,

                                                        PartnerId = x.PartnerId,
                                                        PartnerKey = x.Partner.Guid,
                                                        PartnerDisplayName = x.Partner.DisplayName,
                                                        PartnerMobileNumber = x.Partner.MobileNumber,
                                                        PartnerComissionAmount = x.PartnerComissionAmount,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Sum(x => x.MerchantAmount);
                            #endregion
                            var _Response = new
                            {
                                Transactions = Transactions,
                                Amount = Amount,
                                SuccessfulTransactionsAmount = SuccessAmount,
                                FailedTransactionsAmount = FailedAmount,
                            };
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCD0200", ResponseCode.HCD0200);

                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPurchaseHistoryOverview", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal review.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealReview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealReview = new List<ODealReview.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDealReview
                                                .Where(x => x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODealReview.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.CreatedById,
                                                    AccountKey = x.CreatedBy.Guid,
                                                    AccountDisplayName = x.CreatedBy.DisplayName,
                                                    AccountMobileNumber = x.CreatedBy.MobileNumber,
                                                    AccountIconUrl = x.CreatedBy.IconStorage.Path,
                                                    Review = x.Review,
                                                    Rating = x.Rating,
                                                    IsWorking = x.IsWorking,
                                                    Comment = x.Comment,
                                                    StatusId = x.StatusId,
                                                    CreateDate = x.CreateDate,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _DealReview = _HCoreContext.MDDealReview
                                                .Where(x => x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODealReview.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.CreatedById,
                                                    AccountKey = x.CreatedBy.Guid,
                                                    AccountDisplayName = x.CreatedBy.DisplayName,
                                                    AccountMobileNumber = x.CreatedBy.MobileNumber,
                                                    AccountIconUrl = x.CreatedBy.IconStorage.Path,
                                                    Review = x.Review,
                                                    Rating = x.Rating,
                                                    IsWorking = x.IsWorking,
                                                    Comment = x.Comment,
                                                    StatusId = x.StatusId,
                                                    CreateDate = x.CreateDate,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _DealReview)
                    {
                        if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                        {
                            DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                        }
                        else
                        {
                            DataItem.AccountIconUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealReview, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealReview", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the deal review.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDealReview(ODealReview.Update _Request)
        {
            #region
            try
            {
                #region Validation
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }
                #endregion

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.MDDealReview
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_Details == null)
                    {
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                    else
                    {
                        if (_Request.SystemComment != null && _Details.SystemComment != _Request.SystemComment)
                        {
                            _Details.SystemComment = _Request.SystemComment;
                        }
                        if (StatusId > 0)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();

                        #region Rsponse
                        var _Response = new
                        {
                            ReferenceId = _Details.Id,
                            ReferenceKey = _Details.Guid,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0101", ResponseCode.HCP0101);
                        #endregion
                    }
                }
                #endregion

            }
            #region Exception
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateDealReview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
            #endregion
        }
        /// <summary>
        /// Description: Deletes the deal image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteDealImage(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var DealGallaryImage = _HCoreContext.MDDealGallery.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (DealGallaryImage != null)
                    {
                        bool CheckPriImage = _HCoreContext.MDDeal.Any(x => x.Id == DealGallaryImage.DealId && x.PosterStorageId == DealGallaryImage.ImageStorageId);
                        if (CheckPriImage)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0185", ResponseCode.HCP0185);
                        }
                        _HCoreContext.MDDealGallery.Remove(DealGallaryImage);
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0104", ResponseCode.HCP0104);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteDealImage", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the flash deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveFlashDeal(OFlashDeal.Manage.Request _Request)
        {
            #region Manage Exception 
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {

                    var DealDeails = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (DealDeails != null)
                    {
                        if (DealDeails.StatusId == HelperStatus.Deals.Published)
                        {
                            var FlashDealCheck = _HCoreContext.MDFlashDeal.Any(x => x.DealId == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.StatusId == HelperStatus.Default.Active);
                            if (FlashDealCheck)
                            {
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0118", ResponseCode.HCP0118);
                            }

                            string? TimeZoneId = _HCoreContext.MDDeal.Where(x => x.Id == DealDeails.Id && x.Guid == DealDeails.Guid).Select(x => x.Account.Country.TimeZoneName).FirstOrDefault();
                            DateTime? DealEndDate = HCoreHelper.ConvertFromUTC(DealDeails.EndDate, TimeZoneId);
                            if (_Request.EndDate > DealEndDate)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPEND", "Flash deal end date must be less than or equal to the deals end date");
                            }
                            if (DealDeails.StatusId != HelperStatus.Deals.Published)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCPSTS", "This deal cannot be marked as flash deal as this deal is not available at the moment.");
                            }

                            _MDFlashDeal = new MDFlashDeal();
                            _MDFlashDeal.Guid = HCoreHelper.GenerateGuid();
                            _MDFlashDeal.DealId = _Request.ReferenceId;
                            if (_Request.StartDate != null)
                            {
                                _MDFlashDeal.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                            }
                            else
                            {
                                _MDFlashDeal.StartDate = HCoreHelper.GetGMTDateTime();
                            }
                            if (_Request.EndDate != null)
                            {
                                _MDFlashDeal.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                            }
                            else
                            {
                                _MDFlashDeal.EndDate = HCoreHelper.GetGMTDateTime().AddDays(1);
                            }
                            _MDFlashDeal.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDFlashDeal.CreatedById = _Request.UserReference.AccountId;
                            _MDFlashDeal.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.MDFlashDeal.Add(_MDFlashDeal);
                            _HCoreContext.SaveChanges();
                            var _Response = new
                            {
                                ReferenceId = _MDFlashDeal.Id,
                                ReferenceKey = _MDFlashDeal.Guid,
                            };
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0116", ResponseCode.HCP0116);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0115", ResponseCode.HCP0115);
                        }
                        //_Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        //_Details.ModifyById = _Request.UserReference.AccountId;
                        //_HCoreContext.SaveChanges();
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                    //if (_Request.StartDate != null && DealDeails.StartDate != _Request.StartDate)
                    //{
                    //    DealDeails.StartDate = DealDeails.StartDate;
                    //}
                    //if (_Request.EndDate != null && DealDeails.EndDate != _Request.EndDate)
                    //{
                    //    DealDeails.EndDate = _Request.EndDate;
                    //}
                    _MDFlashDeal.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _MDFlashDeal.ModifyById = _Request.UserReference.AccountId;
                    _HCoreContext.MDFlashDeal.Add(_MDFlashDeal);
                    _HCoreContext.SaveChanges();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0181", ResponseCode.HCP0181);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateFlashDeal", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Removes the flash deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RemoveFlashDeal(OFlashDeal.Manage.Request _Request)
        {
            #region Manage Exception 
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {

                    var FlashDealDetails = _HCoreContext.MDFlashDeal.Where(x => x.DealId == _Request.ReferenceId && x.Deal.Guid == _Request.ReferenceKey && x.StatusId == HelperStatus.Default.Active).FirstOrDefault();
                    if (FlashDealDetails != null)
                    {
                        if (FlashDealDetails.StatusId == HelperStatus.Default.Active)
                        {
                            FlashDealDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            FlashDealDetails.ModifyById = _Request.UserReference.AccountId;
                            FlashDealDetails.StatusId = HelperStatus.Default.Inactive;
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0118", ResponseCode.HCP0118);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0117", ResponseCode.HCP0117);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0120", ResponseCode.HCP0120);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RemoveFlashDeal", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealLocation(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDealLocation
                                                .Where(x => x.DealId == _Request.ReferenceId
                                                && x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODeal.DealLocation
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,

                                                    Title = x.Deal.Title,

                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantName = x.Deal.Account.Name,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    CategoryKey = x.Deal.Category.Guid,
                                                    CategoryName = x.Deal.Category.Name,

                                                    DealCreateDate = x.Deal.CreateDate,
                                                    DealEndDate = x.Deal.EndDate,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,
                                                    Address = x.Deal.Account.Address,

                                                    LocationId = x.LocationId,
                                                    LocationKey = x.Location.Guid,
                                                    LocationDisplayName = x.Location.Name,
                                                    LocationAddress = x.Location.Address,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<ODeal.DealLocation> Locations = _HCoreContext.MDDealLocation
                                                .Where(x => x.DealId == _Request.ReferenceId
                                                && x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODeal.DealLocation
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DealId = x.DealId,
                                                    DealKey = x.Deal.Guid,

                                                    Title = x.Deal.Title,

                                                    MerchantKey = x.Deal.Account.Guid,
                                                    MerchantName = x.Deal.Account.Name,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    CategoryKey = x.Deal.Category.Guid,
                                                    CategoryName = x.Deal.Category.Name,

                                                    DealCreateDate = x.Deal.CreateDate,
                                                    DealEndDate = x.Deal.EndDate,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,
                                                    Address = x.Deal.Account.Address,

                                                    LocationId = x.LocationId,
                                                    LocationKey = x.Location.Guid,
                                                    LocationDisplayName = x.Location.Name,
                                                    LocationAddress = x.Location.Address,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealCodeList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealLocation", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
        }

        public class T
        {
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealImageUrl { get; set; }
            public string? DealTitle { get; set; }
            public long? MerchantReferenceId { get; set; }
            public string? MerchantReferenceKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public long? CustomerReferenceId { get; set; }
            public string? CustomerReferenceKey { get; set; }
            public string? CustomerIconUrl { get; set; }
            public string? CustomerName { get; set; }
            public string? CustomerEmailAddress { get; set; }
            public DateTime? PurchaseDate { get; set; }
            public DateTime? ExpiaryDate { get; set; }
            public DateTime? RedeemDate { get; set; }
            public long? RedeemLocationId { get; set; }
            public string? RedeemLocationKey { get; set; }
            public string? RedeemLocationName { get; set; }
            public string? RedeemLocationAddress { get; set; }
            public string? ItemCode { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public double? Amount { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public int? Quantity { get; set; }
            public double? DealCodeAmount { get; set; }
            public double? PaybleAmount { get; set; }
        }

        /// <summary>
        /// Description: Validates the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ValidateDealCode(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0153", ResponseCode.HCP0153);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var DealDetails = _HCoreContext.MDDealCode
                                                .Where(x => x.ItemCode == _Request.ReferenceCode)
                                                .Select(x => new T
                                                {
                                                    DealId = x.Deal.Id,
                                                    DealKey = x.Deal.Guid,
                                                    DealTitle = x.Deal.Title,
                                                    DealImageUrl = x.Deal.PosterStorage.Path,

                                                    Quantity = x.ItemCount,
                                                    DealCodeAmount = x.Amount,
                                                    PaybleAmount = x.Amount * x.ItemCount,

                                                    MerchantReferenceId = x.Deal.Account.Id,
                                                    MerchantReferenceKey = x.Deal.Account.Guid,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    CustomerReferenceId = x.Account.Id,
                                                    CustomerReferenceKey = x.Account.Guid,
                                                    CustomerIconUrl = x.Account.IconStorage.Path,
                                                    CustomerName = x.Account.Name,
                                                    CustomerMobileNumber = x.Account.MobileNumber,
                                                    CustomerEmailAddress = x.Account.EmailAddress,

                                                    PurchaseDate = x.StartDate,
                                                    ExpiaryDate = x.EndDate,
                                                    RedeemDate = x.LastUseDate,
                                                    RedeemLocationId = x.LastUseLocation.Id,
                                                    RedeemLocationKey = x.LastUseLocation.Guid,
                                                    RedeemLocationName = x.LastUseLocation.DisplayName,
                                                    RedeemLocationAddress = x.LastUseLocation.Address,
                                                    ItemCode = x.ItemCode,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    Amount = x.ItemAmount,


                                                })
                                                .FirstOrDefault();
                    if (DealDetails != null)
                    {
                        _DealCodeValidate = new ODealCode.ValidateCode.Details();
                        _DealCodeValidate.Amount = DealDetails.Amount;
                        _DealCodeValidate.PurchaseDate = DealDetails.PurchaseDate;
                        _DealCodeValidate.ExpiaryDate = DealDetails.ExpiaryDate;
                        _DealCodeValidate.RedeemDate = DealDetails.RedeemDate;
                        _DealCodeValidate.RedeemLocationId = DealDetails.RedeemLocationId;
                        _DealCodeValidate.RedeemLocationKey = DealDetails.RedeemLocationKey;
                        _DealCodeValidate.RedeemLocationName = DealDetails.RedeemLocationName;
                        _DealCodeValidate.RedeemLocationAddress = DealDetails.RedeemLocationAddress;
                        _DealCodeValidate.ItemCode = DealDetails.ItemCode;
                        _DealCodeValidate.StatusCode = DealDetails.StatusCode;
                        _DealCodeValidate.StatusName = DealDetails.StatusName;
                        _DealCodeValidate.Quantity = DealDetails.Quantity;
                        _DealCodeValidate.PaybleAmount = DealDetails.PaybleAmount;
                        _DealCodeValidate.DealCodeAmount = DealDetails.DealCodeAmount;

                        _DealCodeValidateMerchant = new ODealCode.ValidateCode.Merchant();
                        _DealCodeValidateMerchant.ReferenceId = DealDetails.MerchantReferenceId;
                        _DealCodeValidateMerchant.ReferenceKey = DealDetails.MerchantReferenceKey;
                        _DealCodeValidateMerchant.DisplayName = DealDetails.MerchantDisplayName;
                        _DealCodeValidateMerchant.IconUrl = DealDetails.MerchantIconUrl;
                        if (!string.IsNullOrEmpty(_DealCodeValidateMerchant.IconUrl))
                        {
                            _DealCodeValidateMerchant.IconUrl = _AppConfig.StorageUrl + _DealCodeValidateMerchant.IconUrl;
                        }
                        else
                        {
                            _DealCodeValidateMerchant.IconUrl = _AppConfig.Default_Icon;
                        }
                        _DealCodeValidate.Merchant = _DealCodeValidateMerchant;

                        _DealCodeValidateDeal = new ODealCode.ValidateCode.Deal();
                        _DealCodeValidateDeal.ReferenceId = DealDetails.DealId;
                        _DealCodeValidateDeal.ReferenceKey = DealDetails.DealKey;
                        _DealCodeValidateDeal.Title = DealDetails.DealTitle;
                        _DealCodeValidateDeal.ImageUrl = DealDetails.DealImageUrl;
                        if (!string.IsNullOrEmpty(_DealCodeValidateDeal.ImageUrl))
                        {
                            _DealCodeValidateDeal.ImageUrl = _AppConfig.StorageUrl + _DealCodeValidateDeal.ImageUrl;
                        }
                        else
                        {
                            _DealCodeValidateDeal.ImageUrl = _AppConfig.Default_Icon;
                        }
                        _DealCodeValidate.Deal = _DealCodeValidateDeal;

                        _DealCodeValidateCustomer = new ODealCode.ValidateCode.Customer();
                        _DealCodeValidateCustomer.ReferenceId = DealDetails.CustomerReferenceId;
                        _DealCodeValidateCustomer.ReferenceKey = DealDetails.CustomerReferenceKey;
                        _DealCodeValidateCustomer.Name = DealDetails.CustomerName;
                        _DealCodeValidateCustomer.MobileNumber = DealDetails.CustomerMobileNumber;
                        _DealCodeValidateCustomer.EmailAddress = DealDetails.CustomerEmailAddress;
                        _DealCodeValidateCustomer.IconUrl = DealDetails.CustomerIconUrl;
                        if (!string.IsNullOrEmpty(_DealCodeValidateCustomer.IconUrl))
                        {
                            _DealCodeValidateCustomer.IconUrl = _AppConfig.StorageUrl + _DealCodeValidateCustomer.IconUrl;
                        }
                        else
                        {
                            _DealCodeValidateCustomer.IconUrl = _AppConfig.Default_Icon;
                        }
                        _DealCodeValidate.Customer = _DealCodeValidateCustomer;
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCodeValidate, "HCP0154", ResponseCode.HCP0154);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ValidateDealCode", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        public class TDPurchase
        {
            public DateTime CreateDate { get; set; }
            public DateTime? UseDate { get; set; }
            public long? AccountId { get; set; }
            public int StatusId { get; set; }
            public double? Amount { get; set; }
            public double? CommissionAmount { get; set; }
        }
        public class OSalesMetrics
        {
            public string? Title { get; set; }
            public int Hour { get; set; }
            public int Year { get; set; }
            public int Month { get; set; }
            public DateTime? Date { get; set; }
            public long Customer { get; set; }
            public long TotalDeals { get; set; }
            public double? TotalDealAmount { get; set; }
            public long Total { get; set; }
            public double? Amount { get; set; }
            public long Unused { get; set; }
            public double? UnusedAmount { get; set; }
            public long Used { get; set; }
            public double? UsedAmount { get; set; }
            public long Expired { get; set; }
            public double? ExpiredAmount { get; set; }
            public double? CommissionAmount { get; set; }
        }
        public class OTDeals
        {
            public DateTime? StartDate { get; set; }
            public double? SellingPrice { get; set; }
        }
        List<TDPurchase> _TDPurchase;
        List<OTDeals> _OTDeals;
        /// <summary>
        /// Description: Gets the deal overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                    if (_Request.AccountId > 0)
                    {
                        _DealOverview = new ODeal.Overview();
                        _DealOverview.Running = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Published && x.Account.StatusId == HelperStatus.Default.Active && (x.MaximumUnitSale - x.MDDealCode.Sum(y => y.ItemCount) > 0) && (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Upcoming = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Published && x.Account.StatusId == HelperStatus.Default.Active && x.Account.CountryId == _Request.UserReference.SystemCountry && x.StartDate > ActiveTime);
                        _DealOverview.Total = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId);
                        _DealOverview.Draft = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Draft && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.ApprovalPending = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.ApprovalPending && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Approved = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Approved && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Published = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Published && (x.MaximumUnitSale - x.MDDealCode.Sum(y => y.ItemCount) > 0) && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Paused = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Paused && (x.MaximumUnitSale - x.MDDealCode.Sum(y => y.ItemCount) > 0) && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Expired = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Expired && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Rejected = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Rejected && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.AppDownloaded = _HCoreContext.HCUAccountSession.Count(x => x.AccountId == _Request.AccountId && x.Account.AccountTypeId == Helpers.UserAccountType.Merchant) > 0 ? true : false;
                        _DealOverview.SoldOut = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Published && x.Account.StatusId == HelperStatus.Default.Active && (x.MaximumUnitSale - x.MDDealCode.Sum(y => y.ItemCount) == 0) && (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.BestSelling = _HCoreContext.MDDeal.Count(x => x.AccountId == _Request.AccountId && x.StatusId == HelperStatus.Deals.Published && x.Account.StatusId == HelperStatus.Default.Active && ((x.MaximumUnitSale - x.MDDealCode.Sum(y => y.ItemCount) > 0) && (x.MaximumUnitSale - x.MDDealCode.Sum(y => y.ItemCount) < x.MaximumUnitSale)) && (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.Account.CountryId == _Request.UserReference.SystemCountry);

                        //AppDownloaded
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealOverview, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        _DealOverview = new ODeal.Overview();
                        _DealOverview.Running = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Published && x.Account.StatusId == HelperStatus.Default.Active && (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Upcoming = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Published && x.Account.StatusId == HelperStatus.Default.Active && x.Account.CountryId == _Request.UserReference.SystemCountry && x.StartDate > ActiveTime);
                        _DealOverview.Total = _HCoreContext.MDDeal.Count(x => x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Draft = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Draft && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.ApprovalPending = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.ApprovalPending && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Approved = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Approved && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Published = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Published && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Paused = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Paused && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Expired = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Expired && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _DealOverview.Rejected = _HCoreContext.MDDeal.Count(x => x.StatusId == HelperStatus.Deals.Rejected && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealOverview, "HCD0200", ResponseCode.HCD0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealOverview", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal purchase overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealPurchaseOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ReferenceId != 0)
                    {
                        _DealCodeOverview = new ODealCode.Overview();
                        _DealCodeOverview.MaxLimit = _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId && x.Account.CountryId == _Request.UserReference.SystemCountry).Select(x => x.MaximumUnitSale).FirstOrDefault();
                        _DealCodeOverview.Total = (long)_HCoreContext.MDDealCode.Where(x => x.DealId == _Request.ReferenceId && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemCount);
                        _DealCodeOverview.TotalAmount = _HCoreContext.MDDealCode.Where(x => x.DealId == _Request.ReferenceId && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.Amount);
                        _DealCodeOverview.Unused = (long)_HCoreContext.MDDealCode.Where(x => x.DealId == _Request.ReferenceId && x.Account.CountryId == _Request.UserReference.SystemCountry && x.StatusId == HelperStatus.DealCodes.Unused).Sum(x => x.ItemCount);
                        _DealCodeOverview.Used = (long)_HCoreContext.MDDealCode.Where(x => x.DealId == _Request.ReferenceId && x.Account.CountryId == _Request.UserReference.SystemCountry && x.StatusId == HelperStatus.DealCodes.Used).Sum(x => x.ItemCount);
                        _DealCodeOverview.UsedAmount = _HCoreContext.MDDealCode.Where(x => x.DealId == _Request.ReferenceId && x.Account.CountryId == _Request.UserReference.SystemCountry && x.StatusId == HelperStatus.DealCodes.Used).Sum(x => x.Amount);
                        _DealCodeOverview.Expired = (long)_HCoreContext.MDDealCode.Where(x => x.DealId == _Request.ReferenceId && x.Account.CountryId == _Request.UserReference.SystemCountry && x.StatusId == HelperStatus.DealCodes.Expired).Sum(x => x.ItemCount);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCodeOverview, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        if (_Request.AccountId > 0)
                        {
                            _DealCodeOverview = new ODealCode.Overview();
                            _DealCodeOverview.Total = (long)_HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId).Sum(x => x.ItemCount);
                            _DealCodeOverview.TotalAmount = _HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemAmount);
                            _DealCodeOverview.Customer = _HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.Account.CountryId == _Request.UserReference.SystemCountry).Select(x => x.AccountId).Distinct().Count();
                            _DealCodeOverview.Unused = (long)_HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.StatusId == HelperStatus.DealCodes.Unused && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemCount);
                            _DealCodeOverview.Used = (long)_HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.StatusId == HelperStatus.DealCodes.Used && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemCount);
                            _DealCodeOverview.UsedAmount = _HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.Account.CountryId == _Request.UserReference.SystemCountry && x.StatusId == HelperStatus.DealCodes.Used).Sum(x => x.ItemAmount);
                            _DealCodeOverview.Expired = (long)_HCoreContext.MDDealCode.Where(x => x.Deal.AccountId == _Request.AccountId && x.StatusId == HelperStatus.DealCodes.Expired && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemCount);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCodeOverview, "HCD0200", ResponseCode.HCD0200);
                        }
                        else
                        {
                            _DealCodeOverview = new ODealCode.Overview();
                            _DealCodeOverview.Total = (long)_HCoreContext.MDDealCode.Sum(x => x.ItemCount);
                            _DealCodeOverview.TotalAmount = _HCoreContext.MDDealCode.Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemAmount);
                            _DealCodeOverview.Customer = _HCoreContext.MDDealCode.Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry).Select(x => x.AccountId).Distinct().Count();
                            _DealCodeOverview.Unused = (long)_HCoreContext.MDDealCode.Where(x => x.StatusId == HelperStatus.DealCodes.Unused && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemCount);
                            _DealCodeOverview.Used = (long)_HCoreContext.MDDealCode.Where(x => x.StatusId == HelperStatus.DealCodes.Used && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemCount);
                            _DealCodeOverview.UsedAmount = _HCoreContext.MDDealCode.Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry && x.StatusId == HelperStatus.DealCodes.Used).Sum(x => x.ItemAmount);
                            _DealCodeOverview.Expired = (long)_HCoreContext.MDDealCode.Where(x => x.StatusId == HelperStatus.DealCodes.Expired && x.Account.CountryId == _Request.UserReference.SystemCountry).Sum(x => x.ItemCount);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCodeOverview, "HCD0200", ResponseCode.HCD0200);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealPurchaseOverview", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _OTDeals = new List<OTDeals>();
                    long DaysDifference = (_Request.EndDate.Value.Date - _Request.StartDate.Value.Date).Days;
                    _TDPurchase = new List<TDPurchase>();
                    if (_Request.ReferenceId > 0)
                    {
                        _TDPurchase = _HCoreContext.MDDealCode
                                        .Where(x =>
                                        x.DealId == _Request.ReferenceId
                                        && x.Account.CountryId == _Request.UserReference.SystemCountry
                                        && x.CreateDate >= _Request.StartDate
                                        && x.CreateDate < _Request.EndDate)
                                       .Select(x => new TDPurchase
                                       {
                                           CreateDate = x.CreateDate.AddHours(1),
                                           AccountId = x.AccountId,
                                           UseDate = x.LastUseDate,
                                           StatusId = x.StatusId,
                                           Amount = x.ItemAmount,
                                           //CommissionAmount = 0
                                           CommissionAmount = x.Deal.CommissionAmount
                                       }).ToList();
                    }
                    else
                    {
                        if (_Request.AccountId != 0)
                        {
                            _OTDeals = _HCoreContext.MDDeal
                                .Where(x => x.AccountId == _Request.AccountId
                                 && x.Account.CountryId == _Request.UserReference.SystemCountry
                                       && x.CreateDate >= _Request.StartDate
                                       && x.CreateDate < _Request.EndDate)
                                .Select(x => new OTDeals
                                {
                                    StartDate = x.StartDate,
                                    SellingPrice = x.SellingPrice,
                                }).ToList();
                            _TDPurchase = _HCoreContext.MDDealCode
                                       .Where(x =>
                                       x.Deal.AccountId == _Request.AccountId
                                        && x.Account.CountryId == _Request.UserReference.SystemCountry
                                       && x.CreateDate >= _Request.StartDate
                                       && x.CreateDate < _Request.EndDate)
                                      .Select(x => new TDPurchase
                                      {
                                          CreateDate = x.CreateDate.AddHours(1),
                                          AccountId = x.AccountId,
                                          UseDate = x.LastUseDate,
                                          StatusId = x.StatusId,
                                          Amount = x.ItemAmount,
                                          //CommissionAmount = 0
                                          CommissionAmount = x.Deal.CommissionAmount
                                      }).ToList();
                        }
                        else
                        {
                            _OTDeals = _HCoreContext.MDDeal
                               .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry
                                      && x.CreateDate >= _Request.StartDate
                                      && x.CreateDate < _Request.EndDate)
                               .Select(x => new OTDeals
                               {
                                   StartDate = x.StartDate,
                                   SellingPrice = x.SellingPrice,
                               }).ToList();
                            _TDPurchase = _HCoreContext.MDDealCode
                                       .Where(x =>
                                        x.CreateDate >= _Request.StartDate
                                        && x.Account.CountryId == _Request.UserReference.SystemCountry
                                       && x.CreateDate < _Request.EndDate)
                                      .Select(x => new TDPurchase
                                      {
                                          CreateDate = x.CreateDate.AddHours(1),
                                          AccountId = x.AccountId,
                                          UseDate = x.LastUseDate,
                                          StatusId = x.StatusId,
                                          Amount = x.ItemAmount,
                                          //CommissionAmount = 0
                                          CommissionAmount = x.Deal.CommissionAmount
                                      }).ToList();
                        }
                    }
                    if (_TDPurchase.Count() > 0)
                    {
                        foreach (var item in _TDPurchase)
                        {
                            if (item.UseDate != null)
                            {
                                item.UseDate.Value.AddHours(1);
                            }
                        }
                    }
                    if (DaysDifference > 365)
                    {
                        var TM = _TDPurchase.GroupBy(x => x.CreateDate.Year)
                       .Select(x => new OSalesMetrics
                       {
                           Title = x.Key.ToString(),
                           Year = x.Key,

                           TotalDeals = _OTDeals.Count(a => a.StartDate.Value.Year == x.Key),
                           TotalDealAmount = _OTDeals.Where(a => a.StartDate.Value.Year == x.Key).Sum(a => a.SellingPrice),

                           Total = x.Count(),
                           Amount = x.Sum(a => a.Amount),

                           Unused = x.Count(a => a.StatusId == HelperStatus.DealCodes.Unused),
                           UnusedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Unused).Sum(a => a.Amount),

                           Used = x.Count(a => a.StatusId == HelperStatus.DealCodes.Used),
                           UsedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Used).Sum(a => a.Amount),

                           Expired = x.Count(a => a.StatusId == HelperStatus.DealCodes.Expired),
                           ExpiredAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Expired).Sum(a => a.Amount),

                           Customer = x.Where(a => a.AccountId != null).Select(a => a.AccountId).Distinct().Count(),

                           CommissionAmount = x.Sum(a => a.CommissionAmount)
                       }).ToList();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 366 && DaysDifference > 31)
                    {
                        var TM = _TDPurchase.GroupBy(x => new
                        {
                            x.CreateDate.Date.Year,
                            x.CreateDate.Date.Month,
                        })
                     .Select(x => new OSalesMetrics
                     {
                         Title = x.Key.Year.ToString() + "-" + x.Key.Month.ToString(),
                         Year = x.Key.Year,
                         Month = x.Key.Month,
                         Total = x.Count(),
                         TotalDeals = _OTDeals.Count(a => a.StartDate.Value.Year == x.Key.Year && a.StartDate.Value.Month == x.Key.Month),
                         TotalDealAmount = _OTDeals.Where(a => a.StartDate.Value.Year == x.Key.Year && a.StartDate.Value.Month == x.Key.Month).Sum(a => a.SellingPrice),
                         Amount = x.Sum(a => a.Amount),
                         Unused = x.Count(a => a.StatusId == HelperStatus.DealCodes.Unused),
                         UnusedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Unused).Sum(a => a.Amount),
                         Used = x.Count(a => a.StatusId == HelperStatus.DealCodes.Used),
                         UsedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Used).Sum(a => a.Amount),
                         Expired = x.Count(a => a.StatusId == HelperStatus.DealCodes.Expired),
                         ExpiredAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Expired).Sum(a => a.Amount),
                         Customer = x.Where(a => a.AccountId != null).Select(a => a.AccountId).Distinct().Count(),
                         CommissionAmount = x.Sum(a => a.CommissionAmount)
                     }).ToList();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                        #endregion
                    }
                    else if (DaysDifference < 2)
                    {
                        var TM = _TDPurchase.GroupBy(x => x.CreateDate.Hour)
                         .Select(x => new OSalesMetrics
                         {
                             Title = x.Key.ToString(),
                             Hour = x.Key,
                             TotalDeals = _OTDeals.Count(a => a.StartDate.Value.Hour == x.Key),
                             TotalDealAmount = _OTDeals.Where(a => a.StartDate.Value.Hour == x.Key).Sum(a => a.SellingPrice),
                             Total = x.Count(),
                             Amount = x.Sum(a => a.Amount),
                             Unused = x.Count(a => a.StatusId == HelperStatus.DealCodes.Unused),
                             UnusedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Unused).Sum(a => a.Amount),
                             Used = x.Count(a => a.StatusId == HelperStatus.DealCodes.Used),
                             UsedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Used).Sum(a => a.Amount),
                             Expired = x.Count(a => a.StatusId == HelperStatus.DealCodes.Expired),
                             ExpiredAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Expired).Sum(a => a.Amount),
                             Customer = x.Where(a => a.AccountId != null).Select(a => a.AccountId).Distinct().Count(),
                             CommissionAmount = x.Sum(a => a.CommissionAmount)
                         }).ToList();
                        foreach (var item in TM)
                        {
                            var hours = Convert.ToInt32(item.Title);
                            int minutes = 0;
                            var amPmDesignator = "AM";
                            if (hours == 0)
                                hours = 12;
                            else if (hours == 12)
                                amPmDesignator = "PM";
                            else if (hours > 12)
                            {
                                hours -= 12;
                                amPmDesignator = "PM";
                            }
                            item.Title = String.Format("{0}:{1:00} {2}", hours, minutes, amPmDesignator);
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                    else
                    {
                        var TM = _TDPurchase.GroupBy(x => x.CreateDate.Date)
                         .Select(x => new OSalesMetrics
                         {
                             Title = x.Key.ToString("dd-MM-yyyy"),
                             Date = x.Key,
                             Total = x.Count(),
                             TotalDeals = _OTDeals.Count(a => a.StartDate.Value.Date == x.Key),
                             TotalDealAmount = _OTDeals.Where(a => a.StartDate.Value.Date == x.Key).Sum(a => a.SellingPrice),
                             Amount = x.Sum(a => a.Amount),
                             Unused = x.Count(a => a.StatusId == HelperStatus.DealCodes.Unused),
                             UnusedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Unused).Sum(a => a.Amount),
                             Used = x.Count(a => a.StatusId == HelperStatus.DealCodes.Used),
                             UsedAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Used).Sum(a => a.Amount),
                             Expired = x.Count(a => a.StatusId == HelperStatus.DealCodes.Expired),
                             ExpiredAmount = x.Where(a => a.StatusId == HelperStatus.DealCodes.Expired).Sum(a => a.Amount),
                             Customer = x.Where(a => a.AccountId != null).Select(a => a.AccountId).Distinct().Count(),
                             CommissionAmount = x.Sum(a => a.CommissionAmount)
                         }).ToList();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TM, "HC0001");
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetDealHistory", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
            }
            #endregion
        }

        #region Update deal code status
        /// <summary>
        /// Description: Updates the deal code status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDealCodeStatus(DealCodeStatus _Request)
        {
            try
            {
                if (_Request.ReferenceId > 0 && !string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    OCoreTransaction.Response? TransactionResponse = new OCoreTransaction.Response();
                    int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);

                    using (_HCoreContext = new HCoreContext())
                    {
                        var CodeDetails = _HCoreContext.MDDealCode.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (CodeDetails != null)
                        {
                            if (CodeDetails.StatusId == StatusId)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0303", "Order status already updated.");
                            }
                            else
                            {
                                CodeDetails.StatusId = (int)StatusId;
                                CodeDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                CodeDetails.ModifyById = _Request.UserReference.AccountId;
                                long? StoreId = 0;
                                if (StatusId == HelperStatus.DealCodes.Used)
                                {
                                    var DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == CodeDetails.DealId)
                                                      .Select(x => new
                                                      {
                                                          MerchantId = x.AccountId,
                                                          ReferenceKey = x.Guid,
                                                          MerchantAmount = (x.SellingPrice - x.CommissionAmount),
                                                      }).FirstOrDefault();

                                    if (StatusId == HelperStatus.DealCodes.Used)
                                    {
                                        if (DealDetails.MerchantId > 0)
                                        {
                                            StoreId = _HCoreContext.HCUAccount.Where(x => x.OwnerId == DealDetails.MerchantId && x.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.Id).FirstOrDefault();
                                            if (StoreId > 0)
                                            {
                                                CodeDetails.LastUseLocationId = StoreId;
                                            }
                                        }

                                        CodeDetails.LastUseDate = HCoreHelper.GetGMTDateTime();
                                        CodeDetails.LastUseSource = Helpers.DealRedeemSource.App;
                                    }

                                    double? PaybleAmount = 0.0;
                                    if (CodeDetails.ItemCount > 0)
                                    {
                                        PaybleAmount = DealDetails.MerchantAmount * CodeDetails.ItemCount;
                                    }
                                    else
                                    {
                                        PaybleAmount = DealDetails.MerchantAmount * 1;
                                    }

                                    string? GroupKey = "TPP" + CodeDetails.AccountId + "O" + HCoreHelper.GenerateDateString();

                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    _CoreTransactionRequest = new OCoreTransaction.Request();

                                    _CoreTransactionRequest.CustomerId = (long)CodeDetails.AccountId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = GroupKey;
                                    _CoreTransactionRequest.ParentId = DealDetails.MerchantId;
                                    _CoreTransactionRequest.InvoiceAmount = (double)PaybleAmount;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceNumber = DealDetails.ReferenceKey;
                                    _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                    _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;

                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();

                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = DealDetails.MerchantId,
                                        SourceId = TransactionSource.Deals,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.Deal.DealPurchase,
                                        Amount = _CoreTransactionRequest.InvoiceAmount,
                                        Charge = 0,
                                        TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = DealDetails.MerchantId,
                                        SourceId = TransactionSource.Merchant,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.Deal.DealPurchase,
                                        Amount = _CoreTransactionRequest.InvoiceAmount,
                                        Charge = 0,
                                        TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                                    });

                                    _CoreTransactionRequest.Transactions = _TransactionItems;

                                    TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                }

                                if (StatusId == HelperStatus.DealCodes.Blocked)
                                {
                                    var DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == CodeDetails.DealId)
                                                      .Select(x => new
                                                      {
                                                          MerchantId = x.AccountId,
                                                          ReferenceKey = x.Guid,
                                                          MerchantAmount = (x.SellingPrice - x.CommissionAmount),
                                                          DealPrice = x.SellingPrice,
                                                      }).FirstOrDefault();

                                    double? RefundAmount = 0.0;
                                    if (CodeDetails.ItemCount > 0)
                                    {
                                        RefundAmount = DealDetails.DealPrice * CodeDetails.ItemCount;
                                    }
                                    else
                                    {
                                        RefundAmount = DealDetails.DealPrice * 1;
                                    }

                                    string? GroupKey = "TPP" + CodeDetails.AccountId + "O" + HCoreHelper.GenerateDateString();

                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    _CoreTransactionRequest = new OCoreTransaction.Request();

                                    _CoreTransactionRequest.CustomerId = (long)CodeDetails.AccountId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = GroupKey;
                                    _CoreTransactionRequest.ParentId = (long)CodeDetails.AccountId;
                                    _CoreTransactionRequest.InvoiceAmount = (double)RefundAmount;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceNumber = DealDetails.ReferenceKey;
                                    _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                    _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;

                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();

                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = SystemAccounts.TUCVas,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.TUCRefund.Refund,
                                        SourceId = TransactionSource.TUC,
                                        Amount = (double)RefundAmount,
                                        Charge = 0,
                                        TotalAmount = (double)RefundAmount,
                                        Comment = "TUC deal purchase refund",
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = (long)CodeDetails.AccountId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.TUCRefund.Refund,
                                        SourceId = TransactionSource.TUC,
                                        Amount = (double)RefundAmount,
                                        Charge = 0,
                                        TotalAmount = (double)RefundAmount,
                                        Comment = "TUC deal purchase refund",
                                    });

                                    _CoreTransactionRequest.Transactions = _TransactionItems;

                                    TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                }

                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0200", "Payabale amount credited to the user's wallet");
                                }
                                else
                                {
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP00003", TransactionResponse.Message);
                                }
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0404", "Dealcode details not found. Please use valid details.");
                        }
                    }
                }
                else
                {
                    OCoreTransaction.Response? TransactionResponse = new OCoreTransaction.Response();
                    if (string.IsNullOrEmpty(_Request.ReferenceCode))
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0199", ResponseCode.HCP0199);
                    }

                    int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId > 0)
                    {
                        if (StatusId != HelperStatus.DealCodes.Used && StatusId != HelperStatus.DealCodes.Blocked && StatusId != HelperStatus.DealCodes.Expired)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                        }
                    }

                    using (_HCoreContext = new HCoreContext())
                    {
                        DateTime? TodaysDateTime = HCoreHelper.GetGMTDateTime();
                        var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.ItemCode == _Request.ReferenceCode).FirstOrDefault();

                        if (DealCodeDetails == null)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0122", ResponseCode.HCP0122);
                        }
                        if (DealCodeDetails.StatusId == StatusId)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", "Deal code status already updated.");
                        }
                        if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Used)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0202", ResponseCode.HCP0202);
                        }

                        DealCodeDetails.StatusId = (int)StatusId;


                        DealCodeDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        DealCodeDetails.ModifyById = _Request.UserReference.AccountId;
                        long? StoreId = 0;
                        if (StatusId == HelperStatus.DealCodes.Used)
                        {
                            var DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == DealCodeDetails.DealId)
                                              .Select(x => new
                                              {
                                                  MerchantId = x.AccountId,
                                                  ReferenceKey = x.Guid,
                                                  MerchantAmount = (x.SellingPrice - x.CommissionAmount),
                                              }).FirstOrDefault();

                            if (StatusId == HelperStatus.DealCodes.Used)
                            {
                                if (DealDetails.MerchantId > 0)
                                {
                                    StoreId = _HCoreContext.HCUAccount.Where(x => x.OwnerId == DealDetails.MerchantId && x.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.Id).FirstOrDefault();
                                    if (StoreId > 0)
                                    {
                                        DealCodeDetails.LastUseLocationId = StoreId;
                                    }
                                }

                                DealCodeDetails.LastUseDate = HCoreHelper.GetGMTDateTime();
                                DealCodeDetails.LastUseSource = Helpers.DealRedeemSource.App;
                            }

                            double? PaybleAmount = 0.0;
                            if (DealCodeDetails.ItemCount > 0)
                            {
                                PaybleAmount = DealDetails.MerchantAmount * DealCodeDetails.ItemCount;
                            }
                            else
                            {
                                PaybleAmount = DealDetails.MerchantAmount * 1;
                            }

                            string? GroupKey = "TPP" + DealCodeDetails.AccountId + "O" + HCoreHelper.GenerateDateString();

                            _ManageCoreTransaction = new ManageCoreTransaction();
                            _CoreTransactionRequest = new OCoreTransaction.Request();

                            _CoreTransactionRequest.CustomerId = (long)DealCodeDetails.AccountId;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = GroupKey;
                            _CoreTransactionRequest.ParentId = DealDetails.MerchantId;
                            _CoreTransactionRequest.InvoiceAmount = (double)PaybleAmount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                            _CoreTransactionRequest.ReferenceNumber = DealDetails.ReferenceKey;
                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;

                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();

                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = DealDetails.MerchantId,
                                SourceId = TransactionSource.Deals,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.Deal.DealPurchase,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                Charge = 0,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = DealDetails.MerchantId,
                                SourceId = TransactionSource.Merchant,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.Deal.DealPurchase,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                Charge = 0,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });

                            _CoreTransactionRequest.Transactions = _TransactionItems;

                            TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        }

                        if (StatusId == HelperStatus.DealCodes.Blocked)
                        {
                            var DealDetails = _HCoreContext.MDDeal.Where(x => x.Id == DealCodeDetails.DealId)
                                              .Select(x => new
                                              {
                                                  MerchantId = x.AccountId,
                                                  ReferenceKey = x.Guid,
                                                  MerchantAmount = (x.SellingPrice - x.CommissionAmount),
                                                  DealPrice = x.SellingPrice,
                                              }).FirstOrDefault();

                            double? RefundAmount = 0.0;
                            if (DealCodeDetails.ItemCount > 0)
                            {
                                RefundAmount = DealDetails.DealPrice * DealCodeDetails.ItemCount;
                            }
                            else
                            {
                                RefundAmount = DealDetails.DealPrice * 1;
                            }

                            string? GroupKey = "TPP" + DealCodeDetails.AccountId + "O" + HCoreHelper.GenerateDateString();

                            _ManageCoreTransaction = new ManageCoreTransaction();
                            _CoreTransactionRequest = new OCoreTransaction.Request();

                            _CoreTransactionRequest.CustomerId = (long)DealCodeDetails.AccountId;
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = GroupKey;
                            _CoreTransactionRequest.ParentId = (long)DealCodeDetails.AccountId;
                            _CoreTransactionRequest.InvoiceAmount = (double)RefundAmount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                            _CoreTransactionRequest.ReferenceNumber = DealDetails.ReferenceKey;
                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;

                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();

                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = SystemAccounts.TUCVas,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.TUCRefund.Refund,
                                SourceId = TransactionSource.TUC,
                                Amount = (double)RefundAmount,
                                Charge = 0,
                                TotalAmount = (double)RefundAmount,
                                Comment = "TUC deal purchase refund",
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = (long)DealCodeDetails.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.TUCRefund.Refund,
                                SourceId = TransactionSource.TUC,
                                Amount = (double)RefundAmount,
                                Charge = 0,
                                TotalAmount = (double)RefundAmount,
                                Comment = "TUC deal purchase refund",
                            });

                            _CoreTransactionRequest.Transactions = _TransactionItems;

                            TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        }

                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0200", "Payabale amount credited to the user's wallet");
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP00003", TransactionResponse.Message);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateDealCodeStatus", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
        }
        #endregion

        /// <summary>
        /// Description: Gets the merchant deal overview list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantDealOverviewList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                    if (_Request.RefreshCount)
                    {
                        #region Total Records

                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                               .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                               && x.CountryId == _Request.UserReference.CountryId)
                                               .Where(x => x.HCUAccountParameterAccount.Any(a => a.TypeId == HelperType.ThankUCashDeals))
                                               .Select(x => new ODeal.MerchantList
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   DisplayName = x.DisplayName,
                                                   IconUrl = x.IconStorage.Path,

                                                   TotalDeals = x.MDDealAccount.Count(d => d.StatusId == HelperStatus.Deals.Published),
                                                   RunningDeals = x.MDDealAccount.Count(d => d.StatusId == HelperStatus.Deals.Published && (ActiveTime >= d.StartDate && ActiveTime <= d.EndDate)),
                                                   UpcomingDeals = x.MDDealAccount.Count(d => d.StatusId == HelperStatus.Deals.Published && d.StartDate > ActiveTime),
                                                   SoldDeals = x.MDDealAccount.Sum(d => d.MDDealCode.Count(c => c.DealId == d.Id)),
                                                   RedeemedDeals = x.MDDealAccount.Sum(d => d.MDDealCode.Count(c => c.DealId == d.Id && c.StatusId == HelperStatus.DealCodes.Used)),
                                                   StatusCode = x.Status.SystemName,
                                               })
                                              .Where(_Request.SearchCondition)
                                      .Count();


                        #endregion
                    }
                    #region Get Data
                    List<ODeal.MerchantList> _List = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                && x.CountryId == _Request.UserReference.CountryId).
                                                Where(x => x.HCUAccountParameterAccount.Any(a => a.TypeId == HelperType.ThankUCashDeals))
                                                .Select(x => new ODeal.MerchantList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    IconUrl = x.IconStorage.Path,
                                                    TotalDeals = x.MDDealAccount.Count(d => d.StatusId == HelperStatus.Deals.Published),
                                                    RunningDeals = x.MDDealAccount.Count(d => d.StatusId == HelperStatus.Deals.Published && (ActiveTime >= d.StartDate && ActiveTime <= d.EndDate)),
                                                    UpcomingDeals = x.MDDealAccount.Count(d => d.StatusId == HelperStatus.Deals.Published && d.StartDate > ActiveTime),
                                                    SoldDeals = x.MDDealAccount.Sum(d => d.MDDealCode.Count(c => c.DealId == d.Id)),
                                                    RedeemedDeals = x.MDDealAccount.Sum(d => d.MDDealCode.Count(c => c.DealId == d.Id && c.StatusId == HelperStatus.DealCodes.Used)),
                                                    StatusCode = x.Status.SystemName,
                                                })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();


                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: For getting a customers deal purchase overview
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerDealOverviewList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {

                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                    if (_Request.RefreshCount)
                    {
                        #region Total Records

                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                               .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                               && x.MDDealCodeAccount.Any()
                                               && x.CountryId == _Request.UserReference.CountryId
                                               )
                                               .Select(x => new ODeal.CustomerList
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   Name = x.DisplayName,
                                                   MobileNo = x.MobileNumber,
                                                   PurchasedDeals = x.MDDealCodeAccount.Count(),
                                                   Used = x.MDDealCodeAccount.Count(c => c.StatusId == HelperStatus.DealCodes.Used),
                                                   Unused = x.MDDealCodeAccount.Count(c => c.StatusId == HelperStatus.DealCodes.Unused),
                                                   Expired = x.MDDealCodeAccount.Count(c => c.StatusId == HelperStatus.DealCodes.Expired),
                                                   LastPurchasedDate = x.MDDealCodeAccount.OrderBy(o => o.CreateDate).Select(s => s.CreateDate.ToString()).FirstOrDefault(),
                                                   StatusCode = x.Status.SystemName,
                                               })
                                              .Where(_Request.SearchCondition)
                                      .Count();

                        #endregion
                    }
                    #region Get Data
                    List<ODeal.CustomerList> _List = _HCoreContext.HCUAccount
                                               .Where(x => x.AccountTypeId == UserAccountType.Appuser
                                               && x.MDDealCodeAccount.Any()
                                               && x.CountryId == _Request.UserReference.CountryId
                                               )
                                               .Select(x => new ODeal.CustomerList
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   Name = x.DisplayName,
                                                   MobileNo = x.MobileNumber,
                                                   PurchasedDeals = x.MDDealCodeAccount.Count(),
                                                   Used = x.MDDealCodeAccount.Count(c => c.StatusId == HelperStatus.DealCodes.Used),
                                                   Unused = x.MDDealCodeAccount.Count(c => c.StatusId == HelperStatus.DealCodes.Unused),
                                                   Expired = x.MDDealCodeAccount.Count(c => c.StatusId == HelperStatus.DealCodes.Expired),
                                                   LastPurchasedDate = x.MDDealCodeAccount.OrderByDescending(o => o.CreateDate).Select(s => s.CreateDate.ToString()).FirstOrDefault(),
                                                   IconUrl = x.IconStorage.Path,
                                                   StatusCode = x.Status.SystemName,
                                               })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();


                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    #endregion

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCustomerDealOverviewList", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// This API will return list of the all deal merchants with required parameter's using the codition to check that the merchant has registered as a deal merchant or not.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal async Task<OResponse> GetDealMerchants(OList.Request _Request)
        {
            try
            {
                List<ODealOperation.DealMerchantList> _DealMerchants = new List<ODealOperation.DealMerchantList>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.CountryId == _Request.UserReference.CountryId
                                                && x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new ODealOperation.DealMerchantList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    DisplayName = x.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName
                                                }).Where(_Request.SearchCondition)
                                                .Count();
                    }

                    _DealMerchants = await _HCoreContext.HCUAccount
                                     .Where(x => x.CountryId == _Request.UserReference.CountryId
                                     && x.StatusId == HelperStatus.Default.Active)
                                     .Select(x => new ODealOperation.DealMerchantList
                                     {
                                         ReferenceId = x.Id,
                                         ReferenceKey = x.Guid,
                                         DisplayName = x.DisplayName,
                                         StatusId = x.StatusId,
                                         StatusCode = x.Status.SystemName
                                     }).Where(_Request.SearchCondition)
                                     .OrderBy(_Request.SortExpression)
                                     .Skip(_Request.Offset)
                                     .Take(_Request.Limit)
                                     .ToListAsync();

                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealMerchants, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "MD0200", "Deal merchants list loaded successfully.");
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealMerchants", _Exception, _Request.UserReference, _OResponse, "MD0200", "Deal merchants list loaded successfully.");
            }
        }

        internal async Task<OResponse> SaveDeal(CreateDeal.Request _Request)
        {
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.Title))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0102", ResponseCode.HCP0102);
                }
                if (string.IsNullOrEmpty(_Request.CodeUsageTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0182", ResponseCode.HCP0182);
                }
                if (string.IsNullOrEmpty(_Request.TitleContent))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0102", ResponseCode.HCP0102);
                }
                if (string.IsNullOrEmpty(_Request.SubCategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", ResponseCode.HCP0183);
                }

                int? UsageTypeId = HCoreHelper.GetHelperId(_Request.CodeUsageTypeCode);
                int? DealTypeId = HCoreHelper.GetHelperId(_Request.DealTypeCode);
                int? StatusId = HelperStatus.Deals.Draft;
                int? SettlementTypeId = HCoreHelper.GetHelperId(_Request.SettlementTypeCode);
                int? DeliveryTypeId = HCoreHelper.GetHelperId(_Request.DeliveryTypeCode);

                if (DealTypeId == Helpers.DealType.ProductDeal && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || DeliveryTypeId == Helpers.DeliveryType.Delivery) && _Request.Packaging == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", "Packaging information required");
                }
                if (DealTypeId == Helpers.DealType.ProductDeal && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || DeliveryTypeId == Helpers.DeliveryType.Delivery) && _Request.Packaging.Weight <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", "Packaging weight must be greater than 0");
                }
                if (DealTypeId == Helpers.DealType.ProductDeal && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || DeliveryTypeId == Helpers.DeliveryType.Delivery) && string.IsNullOrEmpty(_Request.Packaging.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0183", "Packaging details required");
                }

                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }

                if ((_Request.Images == null || _Request.Images.Count == 0) && (StatusId == HelperStatus.Deals.ApprovalPending || StatusId == HelperStatus.Deals.Published))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0156", ResponseCode.HCP0156);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    foreach (var item in _Request.Images)
                    {
                        if (!string.IsNullOrEmpty(item.reference))
                        {
                            long ImageId = await _HCoreContext.HCCoreStorage.Where(x => x.Guid == item.reference).Select(x => x.Id).FirstOrDefaultAsync();
                            if (ImageId > 0)
                            {
                                item.referenceId = ImageId;
                            }
                        }
                    }

                    var CategoryDetails = await _HCoreContext.MDCategory
                          .Where(x => x.RootCategoryId == _Request.CategoryId && x.RootCategory.Guid == _Request.CategoryKey)
                          .Select(x => new
                          {
                              CategoryId = x.RootCategoryId,
                              Fee = x.Commission
                          }).FirstOrDefaultAsync();
                    if (CategoryDetails == null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0184", ResponseCode.HCP0184);
                    }

                    var SubCategoryDetails = await _HCoreContext.MDCategory
                          .Where(x => x.RootCategoryId == _Request.SubCategoryId && x.RootCategory.Guid == _Request.SubCategoryKey)
                          .Select(x => new
                          {
                              SubCategoryId = x.RootCategoryId,
                              Fee = x.Commission
                          }).FirstOrDefaultAsync();
                    if (SubCategoryDetails == null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0184", ResponseCode.HCP0184);
                    }

                    var DealDetailsCheck = await _HCoreContext.MDDeal
                        .Where(x => x.AccountId == _Request.AccountId && x.TitleContent == _Request.TitleContent)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                        }).FirstOrDefaultAsync();
                    if (DealDetailsCheck != null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealDetailsCheck, "HCP0142", ResponseCode.HCP0142);
                    }

                    _MDDeal = new MDDeal();
                    _MDDeal.Guid = HCoreHelper.GenerateGuid();
                    _MDDeal.TypeId = 1;
                    _MDDeal.AccountId = _Request.AccountId;
                    _MDDeal.Title = _Request.Title;
                    _MDDeal.TitleContent = _Request.TitleContent;
                    _MDDeal.TitleTypeId = _Request.TitleTypeId;
                    _MDDeal.Description = _Request.Description;
                    _MDDeal.Terms = _Request.Terms;
                    _MDDeal.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                    _MDDeal.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    _MDDeal.CommissionAmount = HCoreHelper.GetPercentage((double)_Request.SellingPrice, CategoryDetails.Fee);
                    _MDDeal.CommissionPercentage = CategoryDetails.Fee;
                    if (_Request.UserReference.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Admin)
                    {
                        if (_Request.TUCPercentage > 0)
                        {
                            _MDDeal.CommissionAmount = HCoreHelper.GetPercentage((double)_Request.SellingPrice, (double)_Request.TUCPercentage);
                            _MDDeal.CommissionPercentage = _Request.TUCPercentage;
                        }
                    }
                    _MDDeal.CategoryId = CategoryDetails.CategoryId;
                    _MDDeal.SubcategoryId = SubCategoryDetails.SubCategoryId;
                    _MDDeal.MaximumUnitSale = _Request.MaximumUnitSale;
                    _MDDeal.MaximumUnitSalePerDay = _Request.MaximumUnitSale;
                    _MDDeal.MaximumUnitSalePerPerson = _Request.MaximumUnitSalePerPerson;
                    _MDDeal.UsageInformation = _Request.CodeUsageInformation;
                    _MDDeal.Views = 0;
                    _MDDeal.Like = 0;
                    _MDDeal.DisLike = 0;
                    _MDDeal.TView = 0;
                    _MDDeal.TPurchase = 0;
                    _MDDeal.TLike = 0;
                    _MDDeal.TDislike = 0;
                    _MDDeal.SettlementTypeId = SettlementTypeId;
                    _MDDeal.IsPinRequired = 0;

                    _MDDeal.ActualPrice = _Request.ActualPrice;
                    _MDDeal.SellingPrice = _Request.SellingPrice;
                    _MDDeal.DiscountTypeId = 639; // Percentage 
                    _MDDeal.DiscountAmount = Math.Round(((double)_Request.ActualPrice - (double)_Request.SellingPrice), 2);
                    _MDDeal.DiscountPercentage = HCoreHelper.GetAmountPercentage((double)_MDDeal.DiscountAmount, (double)_Request.ActualPrice);
                    _MDDeal.Amount = _Request.SellingPrice;
                    _MDDeal.Charge = 0;
                    _MDDeal.TotalAmount = _Request.SellingPrice;

                    if (!string.IsNullOrEmpty(_Request.Title))
                    {
                        string TitleSlug = HCoreHelper.GenerateSlug(_Request.Title);
                        bool SlugCheck = await _HCoreContext.MDDeal.AnyAsync(x => x.Slug == TitleSlug);
                        if (!SlugCheck)
                        {
                            _MDDeal.Slug = TitleSlug;
                        }
                        else
                        {
                            DateTime SlugDate = HCoreHelper.ConvertToUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                            string NewSlug = TitleSlug + "-" + SlugDate.ToString("HH:mm:ss");
                            _MDDeal.Slug = NewSlug;
                        }
                    }

                    if (DealTypeId > 0)
                    {
                        _MDDeal.DealTypeId = DealTypeId;
                    }
                    else
                    {
                        _MDDeal.DealTypeId = Helpers.DealType.ServiceDeal;
                    }

                    if (DealTypeId == Helpers.DealType.ProductDeal)
                    {
                        if (DeliveryTypeId > 0)
                        {
                            _MDDeal.DeliveryTypeId = DeliveryTypeId;
                        }
                        else
                        {
                            _MDDeal.DealTypeId = Helpers.DeliveryType.InStorePickUp;
                        }
                    }

                    _MDDeal.UsageTypeId = (int)UsageTypeId;
                    if (UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase)
                    {
                        _MDDeal.CodeValidtyDays = _Request.CodeValidityHours;
                    }
                    else if (UsageTypeId == Helpers.DealCodeUsageType.DaysAfterPurchase)
                    {
                        _MDDeal.CodeValidtyDays = _Request.CodeValidityDays * 24;
                    }
                    else if (UsageTypeId == Helpers.DealCodeUsageType.DealEndDate)
                    {
                        _MDDeal.CodeValidityEndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    }
                    else if (UsageTypeId == Helpers.DealCodeUsageType.ExpiresOnDate)
                    {
                        if (_Request.CodeValidityEndDate != null)
                        {
                            _MDDeal.CodeValidityEndDate = HCoreHelper.ConvertToUTC(_Request.CodeValidityEndDate, _Request.UserReference.CountryTimeZone);
                        }
                        else
                        {
                            _MDDeal.CodeValidityEndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                        }
                    }
                    else
                    {
                        _MDDeal.CodeValidityEndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    }

                    _MDDeal.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                    _MDDeal.CreatedById = _Request.UserReference.AccountId;
                    if (StatusId > 0)
                    {
                        _MDDeal.StatusId = (int)StatusId;
                    }
                    else
                    {
                        _MDDeal.StatusId = HelperStatus.Deals.Published;
                    }

                    await _HCoreContext.MDDeal.AddAsync(_MDDeal);
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();

                    if (_MDDeal.DealTypeId == Helpers.DealType.ProductDeal && (_MDDeal.DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || _MDDeal.DeliveryTypeId == Helpers.DeliveryType.Delivery))
                    {
                        if (string.IsNullOrEmpty(_Request.Packaging.Description))
                        {
                            _Request.Packaging.Description = _Request.Title;
                        }
                        _FrameworkPackaging = new FrameworkPackaging();
                        _FrameworkPackaging.CreatePackage(_MDDeal.AccountId, _MDDeal.Id, _Request.Packaging.Description, _Request.Packaging.Weight, _Request.UserReference);
                    }

                    var _Response = new
                    {
                        StatusId = _MDDeal.StatusId,
                        ReferenceId = _MDDeal.Id,
                        ReferenceKey = _MDDeal.Guid
                    };

                    if (_Request.Locations != null && _Request.Locations.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Location in _Request.Locations)
                            {
                                _MDDealLocation = new MDDealLocation();
                                _MDDealLocation.Guid = HCoreHelper.GenerateGuid();
                                _MDDealLocation.DealId = _MDDeal.Id;
                                _MDDealLocation.LocationId = Location.ReferenceId;
                                _MDDealLocation.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                await _HCoreContext.MDDealLocation.AddAsync(_MDDealLocation);
                                await _HCoreContext.SaveChangesAsync();
                            }
                            await _HCoreContext.DisposeAsync();
                        }
                    }

                    if (_Request.IsAllLocations == true)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantLocations = await _HCoreContext.HCUAccount.Where(x => x.OwnerId == _MDDeal.AccountId && x.AccountTypeId == UserAccountType.MerchantStore).ToListAsync();
                            if (MerchantLocations.Count > 0)
                            {
                                foreach (var MerchantLocation in MerchantLocations)
                                {
                                    _MDDealLocation = new MDDealLocation();
                                    _MDDealLocation.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealLocation.DealId = _MDDeal.Id;
                                    _MDDealLocation.LocationId = MerchantLocation.Id;
                                    _MDDealLocation.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                    _MDDealLocation.CreatedById = _MDDeal.AccountId;
                                    await _HCoreContext.MDDealLocation.AddAsync(_MDDealLocation);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                                await _HCoreContext.DisposeAsync();
                            }
                        }
                    }

                    if (_Request.Shedule != null && _Request.Shedule.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Shedule in _Request.Shedule)
                            {
                                _MDDealShedule = new MDDealShedule();
                                _MDDealShedule.Guid = HCoreHelper.GenerateGuid();
                                if (Shedule.TypeCode == "dealredeemshedule")
                                {
                                    _MDDealShedule.TypeId = 634;
                                }
                                else
                                {
                                    _MDDealShedule.TypeId = 633;
                                }
                                _MDDealShedule.DealId = _MDDeal.Id;
                                _MDDealShedule.StartHour = Shedule.StartHour;
                                _MDDealShedule.EndHour = Shedule.EndHour;
                                _MDDealShedule.DayOfWeek = Shedule.DayOfWeek;
                                _MDDealShedule.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                _MDDealShedule.StatusId = HelperStatus.Default.Active;
                                _MDDealShedule.CreatedById = _MDDeal.AccountId;
                                _HCoreContext.MDDealShedule.Add(_MDDealShedule);
                                await _HCoreContext.SaveChangesAsync();
                            }
                            await _HCoreContext.DisposeAsync();
                        }
                    }

                    if (_Request.Images != null && _Request.Images.Count > 0)
                    {
                        foreach (var Image in _Request.Images)
                        {
                            if (Image.referenceId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (Image.IsDefault == 1)
                                    {
                                        var DealInfo = await _HCoreContext.MDDeal.Where(x => x.Id == _MDDeal.Id).FirstOrDefaultAsync();
                                        if (DealInfo != null)
                                        {
                                            DealInfo.PosterStorageId = Image.referenceId;
                                        }
                                    }
                                    _MDDealGallery = new MDDealGallery();
                                    _MDDealGallery.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealGallery.DealId = _MDDeal.Id;
                                    _MDDealGallery.ImageStorageId = Image.referenceId;
                                    _MDDealGallery.IsDefault = Image.IsDefault;
                                    _MDDealGallery.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                    _MDDealGallery.CreatedById = _MDDeal.AccountId;
                                    await _HCoreContext.MDDealGallery.AddAsync(_MDDealGallery);
                                    await _HCoreContext.SaveChangesAsync();
                                    await _HCoreContext.DisposeAsync();
                                }
                            }
                        }

                        if (_Request.Images != null && _Request.Images.Count > 0)
                        {
                            ODeal.Manage.UploadImageRequest _UploadImageRequest = new ODeal.Manage.UploadImageRequest();
                            _UploadImageRequest.DealId = _MDDeal.Id;
                            _UploadImageRequest.DealKey = _MDDeal.Guid;
                            _UploadImageRequest.Images = _Request.Images;
                            _UploadImageRequest.UserReference = _Request.UserReference;

                            var _Actor = ActorSystem.Create("ActorUploadDealImage");
                            var _ActorNotify = _Actor.ActorOf<ActorUploadDealImage>("ActorUploadDealImage");
                            _ActorNotify.Tell(_UploadImageRequest);
                        }
                    }
                    if (_MDDeal.StatusId == HelperStatus.Deals.Published)
                    {
                        if (_Request.SendNotification)
                        {
                            var _TransactionPostProcessActor = ActorSystem.Create("NotificationNewDealActor");
                            var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<NotificationNewDealActor>("NotificationNewDealActor");
                            _TransactionPostProcessActorNotify.Tell(_MDDeal.Id);
                        }
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0101", ResponseCode.HCP0101);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveDeal", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
        }


        internal async Task<OResponse> UpdateDeal(CreateDeal.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }

                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }

                int? UsageTypeId = HCoreHelper.GetHelperId(_Request.CodeUsageTypeCode);
                int? DealTypeId = HCoreHelper.GetHelperId(_Request.DealTypeCode);
                int? SettlementTypeId = HCoreHelper.GetHelperId(_Request.SettlementTypeCode);
                int? DeliveryTypeId = HCoreHelper.GetHelperId(_Request.DeliveryTypeCode);

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Images.Count > 0)
                    {
                        foreach (var item in _Request.Images)
                        {
                            if (!string.IsNullOrEmpty(item.reference))
                            {
                                long ImageId = await _HCoreContext.HCCoreStorage.Where(x => x.Guid == item.reference).Select(x => x.Id).FirstOrDefaultAsync();
                                if (ImageId > 0)
                                {
                                    item.referenceId = ImageId;
                                }
                            }
                        }
                    }

                    MDDeal? _Details = await _HCoreContext.MDDeal.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (_Details == null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                    if (_Request.TitleTypeId != 0 && _Details.TitleTypeId != _Request.TitleTypeId)
                    {
                        _Details.TitleTypeId = _Request.TitleTypeId;
                    }
                    if (!string.IsNullOrEmpty(_Request.TitleContent) && _Details.TitleContent != _Request.TitleContent)
                    {
                        _Details.TitleContent = _Request.TitleContent;
                    }
                    if (!string.IsNullOrEmpty(_Request.Title) && _Details.Title != _Request.Title)
                    {
                        _Details.Title = _Request.Title;
                    }
                    if (!string.IsNullOrEmpty(_Request.Description) && _Details.Title != _Request.Description)
                    {
                        _Details.Description = _Request.Description;
                    }
                    if (!string.IsNullOrEmpty(_Request.Terms) && _Details.Title != _Request.Terms)
                    {
                        _Details.Terms = _Request.Terms;
                    }
                    if (!string.IsNullOrEmpty(_Request.CodeUsageInformation) && _Details.Title != _Request.CodeUsageInformation)
                    {
                        _Details.UsageInformation = _Request.CodeUsageInformation;
                    }
                    if (_Request.StartDate != null && _Details.StartDate != _Request.StartDate)
                    {
                        _Details.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                    }
                    if (_Request.EndDate != null && _Details.EndDate != _Request.EndDate)
                    {
                        _Details.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    }
                    if (_Request.ActualPrice > 0 && _Details.ActualPrice != _Request.ActualPrice)
                    {
                        _Details.ActualPrice = _Request.ActualPrice;
                    }
                    if (_Request.SellingPrice > 0 && _Details.SellingPrice != _Request.SellingPrice)
                    {
                        _Details.SellingPrice = _Request.SellingPrice;
                    }

                    if (!string.IsNullOrEmpty(_Request.Title) && _Request.Title != _Details.Title)
                    {
                        string TitleSlug = HCoreHelper.GenerateSlug(_Request.Title);
                        if (_Details.Slug != TitleSlug || _Details.Slug == null)
                        {
                            bool SlugCheck = await _HCoreContext.MDDeal.AnyAsync(x => x.Slug == TitleSlug);
                            if (!SlugCheck)
                            {
                                _Details.Slug = TitleSlug;
                            }
                            else
                            {
                                DateTime SlugDate = HCoreHelper.ConvertToUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                string NewSlug = TitleSlug + "-" + SlugDate.ToString("HH:mm:ss");
                                _Details.Slug = NewSlug;
                            }
                        }
                    }


                    if (DealTypeId > 0 || DealTypeId != null)
                    {
                        _Details.DealTypeId = DealTypeId;
                    }


                    if (DealTypeId == Helpers.DealType.ProductDeal)
                    {
                        if (DeliveryTypeId > 0 || DeliveryTypeId != null)
                        {
                            _Details.DeliveryTypeId = DeliveryTypeId;
                        }
                        else
                        {
                            _Details.DeliveryTypeId = Helpers.DeliveryType.InStorePickUp;
                        }
                    }
                    else
                    {
                        _Details.DeliveryTypeId = Helpers.DeliveryType.InStorePickUp;
                    }

                    if (DealTypeId == Helpers.DealType.ProductDeal && (DeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery || DeliveryTypeId == Helpers.DeliveryType.Delivery))
                    {
                        if (string.IsNullOrEmpty(_Request.Packaging.Description))
                        {
                            _Request.Packaging.Description = _Details.Title;
                        }
                        _FrameworkPackaging = new FrameworkPackaging();
                        _FrameworkPackaging.CreatePackage(_Details.AccountId, _Details.Id, _Request.Packaging.Description, _Request.Packaging.Weight, _Request.UserReference);
                    }

                    if (UsageTypeId != null)
                    {
                        if (UsageTypeId > 0)
                        {
                            _Details.UsageTypeId = (int)UsageTypeId;
                            if (UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase)
                            {
                                _Details.CodeValidtyDays = _Request.CodeValidityHours;
                            }
                            else if (UsageTypeId == Helpers.DealCodeUsageType.DaysAfterPurchase)
                            {
                                _Details.CodeValidtyDays = _Request.CodeValidityDays * 24;
                            }
                            else if (UsageTypeId == Helpers.DealCodeUsageType.DealEndDate)
                            {
                                _Details.CodeValidityEndDate = _Request.EndDate;
                            }
                            else if (UsageTypeId == Helpers.DealCodeUsageType.ExpiresOnDate)
                            {
                                if (_Request.CodeValidityEndDate != null)
                                {
                                    _Details.CodeValidityEndDate = HCoreHelper.ConvertToUTC(_Request.CodeValidityEndDate, _Request.UserReference.CountryTimeZone); //_Details.CodeValidityEndDate;
                                }
                                else
                                {
                                    _Details.CodeValidityEndDate = _Details.EndDate;
                                }
                            }
                            else
                            {
                                _Details.CodeValidityEndDate = _Details.EndDate;
                            }
                        }
                    }

                    double Fee = 5;
                    if(!string.IsNullOrEmpty(_Request.SubCategoryKey) && !string.IsNullOrEmpty(_Request.CategoryKey))
                    {
                        var CategoryDetails = await _HCoreContext.MDCategory.Where(x => x.RootCategoryId == _Request.CategoryId && x.RootCategory.Guid == _Request.CategoryKey)
                              .Select(x => new
                              {
                                  CategoryId = x.RootCategoryId,
                              }).FirstOrDefaultAsync();
                        if (CategoryDetails == null)
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0184", ResponseCode.HCP0184);
                        }

                        var SubCategoryDetails = await _HCoreContext.MDCategory.Where(x => x.RootCategoryId == _Request.SubCategoryId && x.RootCategory.Guid == _Request.SubCategoryKey)
                              .Select(x => new
                              {
                                  SubCategoryId = x.RootCategoryId,
                                  Fee = x.Commission
                              }).FirstOrDefaultAsync();
                        if (SubCategoryDetails == null)
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0184", ResponseCode.HCP0184);
                        }

                        Fee = SubCategoryDetails.Fee;
                        if(CategoryDetails.CategoryId > 0 && _Details.CategoryId != CategoryDetails.CategoryId)
                        {
                            _Details.CategoryId = CategoryDetails.CategoryId;
                        }
                        if (SubCategoryDetails.SubCategoryId > 0 && _Details.SubcategoryId != SubCategoryDetails.SubCategoryId)
                        {
                            _Details.SubcategoryId = SubCategoryDetails.SubCategoryId;
                        }
                    }
                    else
                    {
                        if (_Details.SubcategoryId > 0)
                        {
                            Fee = await _HCoreContext.MDCategory.Where(x => x.RootCategoryId == _Details.SubcategoryId).Select(x => x.Commission).FirstOrDefaultAsync();
                        }
                    }

                    if(_Request.ActualPrice > 0 && _Details.ActualPrice != _Request.ActualPrice)
                    {
                        _Details.ActualPrice = _Request.ActualPrice;
                    }
                    if (_Request.SellingPrice > 0 && _Details.ActualPrice != _Request.SellingPrice)
                    {
                        _Details.TotalAmount = _Request.SellingPrice;
                        _Details.Amount = _Request.SellingPrice;
                        _Details.SellingPrice = _Request.SellingPrice;
                        _Details.CommissionAmount = HCoreHelper.GetPercentage((double)_Request.SellingPrice, Fee);
                    }
                    if (Fee > 0 && _Details.ActualPrice != Fee)
                    {
                        _Details.CommissionPercentage = Fee;
                    }

                    _Details.DiscountTypeId = 639; // Percentage

                    if (_Request.UserReference.AccountTypeId == UserAccountType.Admin)
                    {
                        if (_Request.TUCPercentage > 0)
                        {
                            _Details.CommissionAmount = HCoreHelper.GetPercentage((double)_Details.SellingPrice, (double)_Request.TUCPercentage);
                            _Details.CommissionPercentage = _Request.TUCPercentage;
                        }
                    }

                    if (_Request.ActualPrice > 0 && _Request.SellingPrice > 0)
                    {
                        _Details.DiscountAmount = Math.Round(((double)_Request.ActualPrice - (double)_Request.SellingPrice), 2);
                        _Details.DiscountPercentage = HCoreHelper.GetAmountPercentage((double)_Details.DiscountAmount, (double)_Request.ActualPrice);
                    }

                    _Details.Charge = 0;

                    if (_Request.MaximumUnitSale > 0)
                    {
                        _Details.MaximumUnitSale = _Request.MaximumUnitSale;
                        _Details.MaximumUnitSalePerDay = _Request.MaximumUnitSale;
                    }
                    if (_Request.MaximumUnitSalePerPerson > 0 && _Details.MaximumUnitSalePerPerson != _Request.MaximumUnitSalePerPerson)
                    {
                        _Details.MaximumUnitSalePerPerson = _Request.MaximumUnitSalePerPerson;
                    }

                    _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                    _Details.ModifyById = _Request.UserReference.AccountId;

                    if (StatusId > 0)
                    {
                        _Details.StatusId = StatusId;
                    }

                    await _HCoreContext.SaveChangesAsync();

                    var _Response = new
                    {
                        StatusCode = _Request.StatusCode,
                        ReferenceId = _Request.ReferenceId,
                        ReferenceKey = _Request.ReferenceKey
                    };

                    if (_Request.Locations != null && _Request.Locations.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var DealLocations = await _HCoreContext.MDDealLocation.Where(x => x.DealId == _Details.Id).ToListAsync();
                            if (DealLocations.Count() > 0)
                            {
                                _HCoreContext.MDDealLocation.RemoveRange(DealLocations);
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                            }
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Location in _Request.Locations)
                            {
                                _MDDealLocation = new MDDealLocation();
                                _MDDealLocation.Guid = HCoreHelper.GenerateGuid();
                                _MDDealLocation.DealId = _Details.Id;
                                _MDDealLocation.LocationId = Location.ReferenceId;
                                _MDDealLocation.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                await _HCoreContext.MDDealLocation.AddAsync(_MDDealLocation);
                                await _HCoreContext.SaveChangesAsync();
                            }
                            await _HCoreContext.DisposeAsync();
                        }
                    }

                    if (_Request.IsAllLocations == true)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var DealLocations = await _HCoreContext.MDDealLocation.Where(x => x.DealId == _Details.Id).ToListAsync();
                            if (DealLocations.Count() > 0)
                            {
                                _HCoreContext.MDDealLocation.RemoveRange(DealLocations);
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                            }
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MerchantLocations = await _HCoreContext.HCUAccount.Where(x => x.OwnerId == _Details.AccountId && x.AccountTypeId == UserAccountType.MerchantStore).ToListAsync();
                            if (MerchantLocations.Count > 0)
                            {
                                foreach (var MerchantLocation in MerchantLocations)
                                {
                                    _MDDealLocation = new MDDealLocation();
                                    _MDDealLocation.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealLocation.DealId = _Details.Id;
                                    _MDDealLocation.LocationId = MerchantLocation.Id;
                                    _MDDealLocation.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                    _MDDealLocation.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.MDDealLocation.AddAsync(_MDDealLocation);
                                    await _HCoreContext.SaveChangesAsync();
                                }
                                await _HCoreContext.DisposeAsync();
                            }
                        }
                    }

                    if (_Request.Shedule != null && _Request.Shedule.Count > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var MDDealShedules = await _HCoreContext.MDDealShedule.Where(x => x.DealId == _Details.Id).ToListAsync();
                            if (MDDealShedules.Count() > 0)
                            {
                                _HCoreContext.MDDealShedule.RemoveRange(MDDealShedules);
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                            }
                        }
                        using (_HCoreContext = new HCoreContext())
                        {
                            foreach (var Shedule in _Request.Shedule)
                            {
                                _MDDealShedule = new MDDealShedule();
                                _MDDealShedule.Guid = HCoreHelper.GenerateGuid();
                                if (Shedule.TypeCode == "dealredeemshedule")
                                {
                                    _MDDealShedule.TypeId = 634;
                                }
                                else
                                {
                                    _MDDealShedule.TypeId = 633;
                                }
                                _MDDealShedule.DealId = _Details.Id;
                                _MDDealShedule.StartHour = Shedule.StartHour;
                                _MDDealShedule.EndHour = Shedule.EndHour;
                                _MDDealShedule.DayOfWeek = Shedule.DayOfWeek;
                                _MDDealShedule.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                _MDDealShedule.StatusId = HelperStatus.Default.Active;
                                _MDDealShedule.CreatedById = _Request.UserReference.AccountId;
                                _HCoreContext.MDDealShedule.Add(_MDDealShedule);
                                await _HCoreContext.SaveChangesAsync();
                            }
                            await _HCoreContext.DisposeAsync();
                        }
                    }

                    if (_Request.Images != null && _Request.Images.Count > 0)
                    {
                        foreach (var Image in _Request.Images)
                        {
                            if (Image.referenceId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (Image.IsDefault == 1)
                                    {
                                        var DealInfo = await _HCoreContext.MDDeal.Where(x => x.Id == _Details.Id).FirstOrDefaultAsync();
                                        if (DealInfo != null)
                                        {
                                            DealInfo.PosterStorageId = Image.referenceId;
                                        }
                                    }
                                    _MDDealGallery = new MDDealGallery();
                                    _MDDealGallery.Guid = HCoreHelper.GenerateGuid();
                                    _MDDealGallery.DealId = _Details.Id;
                                    _MDDealGallery.ImageStorageId = Image.referenceId;
                                    _MDDealGallery.IsDefault = Image.IsDefault;
                                    _MDDealGallery.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), _Request.UserReference.CountryTimeZone);
                                    _MDDealGallery.CreatedById = _Request.UserReference.AccountId;
                                    await _HCoreContext.MDDealGallery.AddAsync(_MDDealGallery);
                                    await _HCoreContext.SaveChangesAsync();
                                    await _HCoreContext.DisposeAsync();
                                }
                            }
                        }

                        if (_Request.Images != null && _Request.Images.Count > 0)
                        {
                            ODeal.Manage.UploadImageRequest _UploadImageRequest = new ODeal.Manage.UploadImageRequest();
                            _UploadImageRequest.DealId = _Details.Id;
                            _UploadImageRequest.DealKey = _Details.Guid;
                            _UploadImageRequest.Images = _Request.Images;
                            _UploadImageRequest.UserReference = _Request.UserReference;

                            var _Actor = ActorSystem.Create("ActorUploadDealImage");
                            var _ActorNotify = _Actor.ActorOf<ActorUploadDealImage>("ActorUploadDealImage");
                            _ActorNotify.Tell(_UploadImageRequest);
                        }
                    }

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0101", "Deal details updated successfully.");
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateDeal", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
        }
    }
}
