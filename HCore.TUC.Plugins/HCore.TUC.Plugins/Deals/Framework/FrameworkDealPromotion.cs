//==================================================================================
// FileName: FrameworkDealPromotion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deal promotion
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.Deals.Framework
{
    public class FrameworkDealPromotion
    {
        HCoreContext _HCoreContext;
        MDDealPromotion _MDDealPromotion;
        /// <summary>
        /// Description: Saves the deal promotion.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveDealPromotion(ODealPromotion.SaveDealPromotion.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.TypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDTYPECODE, TUCDealPromotionResource.MDTYPECODEM);
                }
                if (_Request.DealId < 1 && _Request.ImageContent == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0207, TUCDealPromotionResource.MD0207M);
                }
                if (_Request.StartDate == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0208, TUCDealPromotionResource.MD0208M);
                }
                if (_Request.EndDate == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0209, TUCDealPromotionResource.MD0209M);
                }
                int StatusId = HelperStatus.Default.Inactive;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDINSTATUS, TUCDealPromotionResource.MDINSTATUSM);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    long referenceId = _HCoreContext.HCCoreStorage.Where(x => x.Guid == _Request.ImageReference).Select(x => x.Id).FirstOrDefault();
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    if (TypeId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDTYPEID, TUCDealPromotionResource.MDTYPEIDM);
                    }
                    long DealImageId = 0;
                    long DealId = 0;
                    if (_Request.DealId > 0)
                    {
                        var Details = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey)
                       .Select(x => new
                       {
                           x.PosterStorageId,
                           x.Id,
                           x.EndDate,
                           x.StatusId,
                           x.Account.Country.TimeZoneName
                       }).FirstOrDefault();
                        if (Details != null)
                        {
                            DealImageId = (long)Details.PosterStorageId;
                            DealId = Details.Id;
                        }

                        DateTime? DealEndDate = HCoreHelper.ConvertFromUTC(Details.EndDate, Details.TimeZoneName);
                        if(_Request.EndDate > DealEndDate)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0213, TUCDealPromotionResource.MD0213M);
                        }

                        if (Details.StatusId != HelperStatus.Deals.Published)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0214, TUCDealPromotionResource.MD0214M);
                        }
                    }
                    _MDDealPromotion = new MDDealPromotion();
                    _MDDealPromotion.Guid = HCoreHelper.GenerateGuid();
                    _MDDealPromotion.TypeId = (int)TypeId;
                    _MDDealPromotion.CountryId = _Request.UserReference.CountryId;
                    if (_Request.DealId > 0)
                    {
                        _MDDealPromotion.DealId = DealId;
                        _MDDealPromotion.ImageStorageId = DealImageId;
                    }

                    _MDDealPromotion.Url = _Request.Url;
                    _MDDealPromotion.Location = _Request.Locations;
                    _MDDealPromotion.StartDate = HCoreHelper.ConvertTimeToUtc(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                    _MDDealPromotion.EndDate = HCoreHelper.ConvertTimeToUtc(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                    _MDDealPromotion.CreateDate = HCoreHelper.GetGMTDateTime();
                    _MDDealPromotion.CreatedById = _Request.UserReference.AccountId;
                    _MDDealPromotion.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.MDDealPromotion.Add(_MDDealPromotion);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    if (referenceId > 0)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var SaveDetails = _HCoreContext.MDDealPromotion.Where(x => x.Id == _MDDealPromotion.Id && x.Guid == _MDDealPromotion.Guid).FirstOrDefault();
                            if (SaveDetails != null)
                            {
                                SaveDetails.ImageStorageId = referenceId;
                                _HCoreContext.SaveChanges();
                                _HCoreContext.Dispose();
                            }
                            else
                            {
                                _HCoreContext.SaveChanges();
                                _HCoreContext.Dispose();
                            }
                        }
                    }
                    else if (_Request.ImageContent != null)
                    {
                        long? ImageStorageId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                        }
                        if (ImageStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var SaveDetails = _HCoreContext.MDDealPromotion.Where(x => x.Id == _MDDealPromotion.Id && x.Guid == _MDDealPromotion.Guid).FirstOrDefault();
                                if (SaveDetails != null)
                                {
                                    if (ImageStorageId != null)
                                    {
                                        SaveDetails.ImageStorageId = (long)ImageStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                    _HCoreContext.Dispose();
                                }
                            }
                        }
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCDealPromotionResource.MD0202, TUCDealPromotionResource.MD0202M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDealPromotionDetails", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Updates the deal promotion.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDealPromotion(ODealPromotion.UpdateDealPromotion _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREFKEY, TUCDealPromotionResource.MDREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDINSTATUS, TUCDealPromotionResource.MDINSTATUSM);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long? referenceId = _HCoreContext.HCCoreStorage.Where(x => x.Guid == _Request.ImageReference).Select(x => x.Id).FirstOrDefault();
                    MDDealPromotion DealDetails = _HCoreContext.MDDealPromotion.Where(x => x.Guid == _Request.ReferenceKey && x.Id == _Request.ReferenceId).FirstOrDefault();
                    if (DealDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Url) && DealDetails.Url != _Request.Url)
                        {
                            DealDetails.Url = _Request.Url;
                        }
                        if (_Request.DealId > 0)
                        {
                            bool DealCheck = _HCoreContext.MDDeal.Any(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey);
                            if (!DealCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0204, TUCDealPromotionResource.MD0204M);
                            }
                            DealDetails.DealId = _Request.DealId;
                        }

                        if (_Request.StartDate != null && DealDetails.StartDate != _Request.StartDate)
                        {
                            DealDetails.StartDate = HCoreHelper.ConvertToUTC(_Request.StartDate, _Request.UserReference.CountryTimeZone);
                        }
                        if (_Request.EndDate != null && DealDetails.EndDate != _Request.EndDate)
                        {
                            DealDetails.EndDate = HCoreHelper.ConvertToUTC(_Request.EndDate, _Request.UserReference.CountryTimeZone);
                        }

                        if (StatusId > 0)
                        {
                            DealDetails.StatusId = StatusId;
                        }
                        DealDetails.Location = _Request.Locations;
                        DealDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        DealDetails.ModifyById = _Request.UserReference.AccountId;
                        long? ActiveImageId = DealDetails.ImageStorageId;
                        long? DealId = DealDetails.DealId;
                        _HCoreContext.SaveChanges();
                        if (referenceId > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.MDDealPromotion.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (referenceId > 0)
                                    {
                                        UserAccDetails.ImageStorageId = referenceId;
                                    }
                                    else if (referenceId == null)
                                    {
                                        UserAccDetails.ImageStorageId = null;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        else if (_Request.ImageContent != null)
                        {
                            long? ImageStorageId = null;
                            if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                            {
                                ImageStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, ActiveImageId, _Request.UserReference);
                            }
                            if (ImageStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UserAccDetails = _HCoreContext.MDDealPromotion.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                    if (UserAccDetails != null)
                                    {
                                        if (ImageStorageId != null)
                                        {
                                            UserAccDetails.ImageStorageId = ImageStorageId;
                                        }
                                        else if (ImageStorageId == null)
                                        {
                                            UserAccDetails.ImageStorageId = null;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request.ReferenceKey, TUCDealPromotionResource.MD0201, TUCDealPromotionResource.MD0201M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateDealPromotionDetails", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Deletes the deal promotion.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteDealPromotion(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                }

                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var DealPromotionDetails = _HCoreContext.MDDealPromotion
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (DealPromotionDetails != null)
                    {
                        _HCoreContext.MDDealPromotion.Remove(DealPromotionDetails);
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCDealPromotionResource.MD0203, TUCDealPromotionResource.MD0203M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0206, TUCDealPromotionResource.MD0206M);
                    }
                }

            }

            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteDealPromotionDetails", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
        }

        /// <summary>
        /// Description: Gets the deal promotion details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealPromotionDetails(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _Details = _HCoreContext.MDDealPromotion.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new ODealPromotion.DealPromotionDetails
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            CountryId = x.CountryId,
                            CountryKey = x.Country.Guid,
                            CountryCode = x.Country.SystemName,
                            CountryName = x.Country.Name,

                            DealId = x.Deal.Id,
                            DealKey = x.Deal.Guid,
                            DealTitle = x.Deal.Title,

                            DealTypeId = x.Deal.DealTypeId,
                            DealTypeCode = x.Deal.DealType.SystemName,
                            DealTypeName = x.Deal.DealType.Name,

                            DealStartDate = x.Deal.StartDate,
                            DealEndDate = x.Deal.EndDate,
                            SellingPrice = x.Deal.SellingPrice,
                            ActualPrice = x.Deal.ActualPrice,

                            Url = x.Url,
                            ImageUrl = x.ImageStorage.Path,
                            Locations = x.Location,
                            TypeId = x.TypeId,
                            TypeCode = x.Type.Guid,
                            TypeName = x.Type.Name,
                            TypeSystemName = x.Type.SystemName,

                            StartDate = x.StartDate,
                            EndDate = x.EndDate,
                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedBy.Id,
                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyBy.Id,

                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        }).FirstOrDefault();

                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                        {
                            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                        }
                        else
                        {
                            _Details.ImageUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCDealPromotionResource.MD0200, TUCDealPromotionResource.MD0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MD0404, TUCDealPromotionResource.MD0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealPromotiondetails", _Exception, _Request.UserReference);
            }
        }

        /// <summary>
        /// Description: Gets the deal promotion list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealPromotionList(OList.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.MDDealPromotion
                            .Where(x => x.CountryId == _Request.UserReference.SystemCountry)//x.Url != null || 
                            .Select(x => new ODealPromotion.DealPromotionList
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                CountryId = x.CountryId,
                                CountryCode = x.Country.SystemName,
                                CountryName = x.Country.Name,

                                DealId = x.Deal.Id,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,

                                DealTypeId = x.Deal.DealTypeId,
                                DealTypeCode = x.Deal.DealType.SystemName,
                                DealTypeName = x.Deal.DealType.Name,

                                Url = x.Url,
                                ImageUrl = x.ImageStorage.Path,
                                ImageStorageId = x.ImageStorageId,
                                TypeId = x.TypeId,
                                TypeCode = x.Type.Guid,
                                TypeName = x.Type.Name,
                                Locations = x.Location,
                                TypeSystemName = x.Type.SystemName,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                CreateDate = x.CreateDate,

                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }

                    List<ODealPromotion.DealPromotionList> Data = _HCoreContext.MDDealPromotion
                            .Where(x => x.CountryId == _Request.UserReference.SystemCountry)//x.Url != null || 
                                                .Select(x => new ODealPromotion.DealPromotionList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CountryId = x.CountryId,
                                                    CountryCode = x.Country.SystemName,
                                                    CountryName = x.Country.Name,

                                                    DealId = x.Deal.Id,
                                                    DealKey = x.Deal.Guid,
                                                    DealTitle = x.Deal.Title,

                                                    DealTypeId = x.Deal.DealTypeId,
                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DealTypeName = x.Deal.DealType.Name,

                                                    Url = x.Url,
                                                    ImageUrl = x.ImageStorage.Path,
                                                    ImageStorageId = x.ImageStorageId,
                                                    TypeId = x.TypeId,
                                                    TypeCode = x.Type.Guid,
                                                    TypeName = x.Type.Name,
                                                    TypeSystemName = x.Type.SystemName,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    CreateDate = x.CreateDate,
                                                    Locations = x.Location,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    AccountId = x.Deal.Account.Id,
                                                    AccountKey = x.Deal.Account.Guid,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                    }

                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCDealPromotionResource.MD0200, TUCDealPromotionResource.MD0200M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetDealPromotionList", _Exception, _Request.UserReference);
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, TUCDealPromotionResource.MD0500, TUCDealPromotionResource.MD0500M);
            }
        }

        /// <summary>
        /// Description: Removes the promotion image.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RemovePromotionImage(ODealPromotion.Image _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCDealPromotionResource.MDREF, TUCDealPromotionResource.MDREFM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var Promotion = _HCoreContext.MDDealPromotion.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Promotion != null)
                    {
                        if (Promotion != null && Promotion.ImageStorageId > 0)
                        {
                            long? IconStorageId = Promotion.ImageStorageId;
                            Promotion.ImageStorageId = null;
                            Promotion.ModifyById = _Request.UserReference.AccountId;
                            Promotion.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();

                            bool DeleteStatus = HCoreHelper.DeleteStorage(IconStorageId, _Request.UserReference);
                            if (DeleteStatus == false)
                            {
                                MDDealPromotion _DetailUpdates = _HCoreContext.MDDealPromotion.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                _DetailUpdates.ImageStorageId = IconStorageId;
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCDealPromotionResource.MD0210, TUCDealPromotionResource.MD0210M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, null, TUCDealPromotionResource.MD0211, TUCDealPromotionResource.MD0211M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCDealPromotionResource.MD0212, TUCDealPromotionResource.MD0212M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, TUCDealPromotionResource.MD0404, TUCDealPromotionResource.MD0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RemovePromotionImage", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
            }
        }
    }
}



