//==================================================================================
// FileName: FrameworkDealNotification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deal notification
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Notification;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.Deals.Framework
{
    internal class FrameworkDealNotification
    {
        MDDealNotification _MDDealNotification;
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Sends the deal notification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SendDealNotification(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealDetails = _HCoreContext.MDDeal
                                                  .Where(x => x.Id == _Request.ReferenceId
                                                  && x.Guid == _Request.ReferenceKey)
                                                    .Select(x => new
                                                    {
                                                        Id = x.Id,
                                                        Guid  = x.Guid,
                                                        StatusId = x.StatusId,
                                                        PosterStorageId = x.PosterStorageId,
                                                        PosterUrl = x.PosterStorage.Path,
                                                        Title = x.TitleContent,
                                                        Description = x.Description,
                                                    }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        if (_DealDetails.StatusId ==  HelperStatus.Deals.Published)
                        {
                            _MDDealNotification = new MDDealNotification();
                            _MDDealNotification.Guid = HCoreHelper.GenerateGuid();
                            _MDDealNotification.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDDealNotification.DealId = _Request.ReferenceId;
                            _MDDealNotification.CreatedById = _Request.UserReference.AccountId;
                            _HCoreContext.MDDealNotification.Add(_MDDealNotification);
                            _HCoreContext.SaveChanges();
                            string ImageUrl = null;
                            if (_DealDetails.PosterStorageId != null)
                            {
                                ImageUrl = _AppConfig.StorageUrl + _DealDetails.PosterUrl;
                            }
                            if (HostEnvironment == HostEnvironmentType.Live)
                            {
                                HCoreHelper.SendPushToTopic("tuccustomer_test", "deal", _DealDetails.Title, _DealDetails.Description, "deal", _DealDetails.Id, _DealDetails.Guid, "View details", ImageUrl);
                                HCoreHelper.SendPushToTopic("tuccustomer", "deal", _DealDetails.Title, _DealDetails.Description, "deal", _DealDetails.Id, _DealDetails.Guid, "View details", ImageUrl);
                                //HCoreHelper.SendPushToTopic("tuccustomer", "deal", DealInformation.Title, DealInformation.Description, "deal", DealInformation.Id, DealInformation.Guid, "View details", ImageUrl);
                            }
                            string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == 7464 && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                            if (!string.IsNullOrEmpty(UserNotificationUrl))
                            {
                                if (HostEnvironment == HostEnvironmentType.Live)
                                {
                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "deal", _DealDetails.Title, _DealDetails.Description, "deal", _DealDetails.Id, _DealDetails.Guid, "View details", true, null, ImageUrl);
                                }
                                else
                                {
                                    HCoreHelper.SendPushToDevice(UserNotificationUrl, "deal", "TEST : " + _DealDetails.Title, _DealDetails.Description, "deal", _DealDetails.Id, _DealDetails.Guid, "View details", true, null, ImageUrl);
                                }
                            }
                            //_DealDetails.Views += 1;
                            //_MDDealView = new MDDealView();
                            //_MDDealView.DealId = _DealDetails.Id;
                            //_MDDealView.CustomerId = _Request.UserReference.AccountId;
                            //_MDDealView.Latitude = _Request.UserReference.RequestLatitude;
                            //_MDDealView.Longitude = _Request.UserReference.RequestLongitude;
                            //_MDDealView.CreateDate = HCoreHelper.GetGMTDateTime();
                            //_HCoreContext.MDDealView.Add(_MDDealView);
                            //_HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0141", ResponseCode.HCP0141);
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0140", ResponseCode.HCP0140);
                        }
                      
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDealView", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal notification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealNotification(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDealNotification
                                               .Where(x=>x.DealId == _Request.ReferenceId
                                               && x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new 
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Date = x.CreateDate,

                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                  
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                   var _List = _HCoreContext.MDDealNotification
                                               .Where(x => x.DealId == _Request.ReferenceId
                                               && x.Deal.Guid == _Request.ReferenceKey)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Date = x.CreateDate,

                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                   
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealNotification", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

    }
}
