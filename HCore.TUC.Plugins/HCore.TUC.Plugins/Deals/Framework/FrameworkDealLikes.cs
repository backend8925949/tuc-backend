//==================================================================================
// FileName: FrameworkDealLikes.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deals likes
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.Deals.Framework
{
    public class FrameworkDealLikes
    {
        HCoreContext _HCoreContext;

        /// <summary>
        /// Description: Gets the deallikes.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeallikes(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    #region Deal Likes
                    if (_Request.Type == "Likes")
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = _HCoreContext.MDDealLike
                                .Where(x => x.DealId == _Request.ReferenceId
                                && x.Deal.Guid == _Request.ReferenceKey
                                && x.TypeId == 1)
                            .Select(x => new DealLikes.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                TypeId = x.TypeId,

                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,
                                Description = x.Deal.Description,
                                ActualPrice = x.Deal.ActualPrice,
                                SellingPrice = x.Deal.SellingPrice,
                                Amount = x.Deal.Amount,
                                DealStartDate = x.Deal.StartDate,
                                DealEndDate = x.Deal.EndDate,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountName = x.Account.Name,
                                AccountDisplayName = x.Account.DisplayName,
                                MobileNumber = x.Account.MobileNumber,
                                EmailAddress = x.Account.EmailAddress,
                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,
                                AccountTypeDisplayName = x.Account.AccountType.SystemName,

                                CreateDate = x.CreateDate,
                            }).Where(_Request.SearchCondition)
                              .Count();
                        }
                        #endregion

                        #region Data
                        List<DealLikes.List> Data = _HCoreContext.MDDealLike
                                .Where(x => x.Id == _Request.ReferenceId
                                && x.Guid == _Request.ReferenceKey
                                && x.TypeId == 1)
                            .Select(x => new DealLikes.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                TypeId = x.TypeId,

                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,
                                Description = x.Deal.Description,
                                ActualPrice = x.Deal.ActualPrice,
                                SellingPrice = x.Deal.SellingPrice,
                                Amount = x.Deal.Amount,
                                DealStartDate = x.Deal.StartDate,
                                DealEndDate = x.Deal.EndDate,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountName = x.Account.Name,
                                AccountDisplayName = x.Account.DisplayName,
                                MobileNumber = x.Account.MobileNumber,
                                EmailAddress = x.Account.EmailAddress,
                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,
                                AccountTypeDisplayName = x.Account.AccountType.SystemName,

                                CreateDate = x.CreateDate,
                            }).Where(_Request.SearchCondition)
                              .OrderBy(_Request.SortExpression)
                              .Skip(_Request.Offset)
                              .Take(_Request.Limit)
                              .ToList();
                        #endregion

                        #region Response
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                        #endregion
                    }
                    #endregion

                    #region DisLikes
                    else if(_Request.Type == "Dislikes")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDealLike
                                .Where(x => x.DealId == _Request.ReferenceId
                                && x.Deal.Guid == _Request.ReferenceKey
                                && x.TypeId == 2)
                            .Select(x => new DealLikes.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                TypeId = x.TypeId,

                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,
                                Description = x.Deal.Description,
                                ActualPrice = x.Deal.ActualPrice,
                                SellingPrice = x.Deal.SellingPrice,
                                Amount = x.Deal.Amount,
                                DealStartDate = x.Deal.StartDate,
                                DealEndDate = x.Deal.EndDate,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountName = x.Account.Name,
                                AccountDisplayName = x.Account.DisplayName,
                                MobileNumber = x.Account.MobileNumber,
                                EmailAddress = x.Account.EmailAddress,
                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,
                                AccountTypeDisplayName = x.Account.AccountType.SystemName,

                                CreateDate = x.CreateDate,
                            }).Where(_Request.SearchCondition)
                              .Count();
                        }
                        #endregion

                        #region Data
                        List<DealLikes.List> Data = _HCoreContext.MDDealLike
                                .Where(x => x.DealId == _Request.ReferenceId
                                && x.Deal.Guid == _Request.ReferenceKey
                                && x.TypeId == 2)
                            .Select(x => new DealLikes.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                TypeId = x.TypeId,

                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,
                                Description = x.Deal.Description,
                                ActualPrice = x.Deal.ActualPrice,
                                SellingPrice = x.Deal.SellingPrice,
                                Amount = x.Deal.Amount,
                                DealStartDate = x.Deal.StartDate,
                                DealEndDate = x.Deal.EndDate,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountName = x.Account.Name,
                                AccountDisplayName = x.Account.DisplayName,
                                MobileNumber = x.Account.MobileNumber,
                                EmailAddress = x.Account.EmailAddress,
                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,
                                AccountTypeDisplayName = x.Account.AccountType.SystemName,

                                CreateDate = x.CreateDate,
                            }).Where(_Request.SearchCondition)
                              .OrderBy(_Request.SortExpression)
                              .Skip(_Request.Offset)
                              .Take(_Request.Limit)
                              .ToList();
                        #endregion

                        #region Response
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                        #endregion
                    }
                    #endregion

                    #region All
                    else 
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDealLike
                                .Where(x => x.DealId == _Request.ReferenceId
                                && x.Deal.Guid == _Request.ReferenceKey)
                            .Select(x => new DealLikes.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                TypeId = x.TypeId,

                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,
                                Description = x.Deal.Description,
                                ActualPrice = x.Deal.ActualPrice,
                                SellingPrice = x.Deal.SellingPrice,
                                Amount = x.Deal.Amount,
                                DealStartDate = x.Deal.StartDate,
                                DealEndDate = x.Deal.EndDate,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountName = x.Account.Name,
                                AccountDisplayName = x.Account.DisplayName,
                                MobileNumber = x.Account.MobileNumber,
                                EmailAddress = x.Account.EmailAddress,
                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,
                                AccountTypeDisplayName = x.Account.AccountType.SystemName,

                                CreateDate = x.CreateDate,
                            }).Where(_Request.SearchCondition)
                              .Count();
                        }
                        #endregion

                        #region Data
                        List<DealLikes.List> Data = _HCoreContext.MDDealLike
                                .Where(x => x.DealId == _Request.ReferenceId
                                && x.Deal.Guid == _Request.ReferenceKey)
                            .Select(x => new DealLikes.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                TypeId = x.TypeId,

                                DealId = x.DealId,
                                DealKey = x.Deal.Guid,
                                DealTitle = x.Deal.Title,
                                Description = x.Deal.Description,
                                ActualPrice = x.Deal.ActualPrice,
                                SellingPrice = x.Deal.SellingPrice,
                                Amount = x.Deal.Amount,
                                DealStartDate = x.Deal.StartDate,
                                DealEndDate = x.Deal.EndDate,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountName = x.Account.Name,
                                AccountDisplayName = x.Account.DisplayName,
                                MobileNumber = x.Account.MobileNumber,
                                EmailAddress = x.Account.EmailAddress,
                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,
                                AccountTypeDisplayName = x.Account.AccountType.SystemName,

                                CreateDate = x.CreateDate,
                            }).Where(_Request.SearchCondition)
                              .OrderBy(_Request.SortExpression)
                              .Skip(_Request.Offset)
                              .Take(_Request.Limit)
                              .ToList();
                        #endregion

                        #region Response
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            #region Exception
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealLikes", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
            #endregion
        }
    }
}
