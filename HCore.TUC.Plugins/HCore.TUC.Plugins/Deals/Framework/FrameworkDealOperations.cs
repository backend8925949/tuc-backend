//==================================================================================
// FileName: FrameworkDealOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deals
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Transactions;
using Akka.Actor;
using Delivery.Object.Response.Shipments;
using Dellyman.DellymanObject.Response;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Plugins.Deals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using HCore.TUC.Plugins.Delivery.Framework;
using HCore.TUC.Plugins.GiftCards.Framework;
using HCore.TUC.Plugins.Operations;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Plugins.Deals.Framework.FrameworkDealOperations;
using static HCore.TUC.Plugins.Deals.Object.ODeal;
using static HCore.TUC.Plugins.Deals.Object.OEmailProcessor;
using static HCore.TUC.Plugins.Delivery.Objects.OOperations.Pricing;
using static HCore.TUC.Plugins.Delivery.Operations.ManageEmailProcessor;
using static HCore.TUC.Plugins.GiftCards.Object.OGiftCard;

namespace HCore.TUC.Plugins.Deals.Framework
{
    internal class FrameworkDealOperations
    {
        HCoreContext? _HCoreContext;
        List<ODealOperation.DealList>? _DealsList;
        List<ODealOperation.DealMerchantList>? _DealMerchants;
        List<ODealOperation.DealCode.List>? _DealCodesList;
        ODealOperation.Details? _DealDetails;
        MDDealCode? _MDDealCode;
        MDDealView? _MDDealView;
        ManageCoreTransaction? _ManageCoreTransaction;
        OCoreTransaction.Request? _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem>? _TransactionItems;
        ODealRedeem.Details? _DealRedeemDetails;
        LSOperations? _LSOperations;
        OAppProfile.Request? _AppUserRequest;
        ManageCoreUserAccess? _ManageCoreUserAccess;
        FrameworkOperations? _FrameworkOperations;
        List<MDDealCode>? _MDDealCodes;

        /// <summary>
        /// Description: Gets the deal list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealsList = new List<ODealOperation.DealList>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "flash")
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDFlashDeal
                                                    .Where(x => x.Deal.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.StatusId == HelperStatus.Default.Active
                                                    && x.Deal.StatusId == HelperStatus.Deals.Published)
                                                    .Select(x => new ODealOperation.DealList
                                                    {
                                                        ReferenceId = x.Deal.Id,
                                                        ReferenceKey = x.Deal.Guid,

                                                        Title = x.Deal.TitleContent,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,
                                                        MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                        CategoryId = x.Deal.CategoryId,
                                                        CategoryName = x.Deal.Category.Name,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        DealTypeId = x.Deal.DealTypeId,
                                                        DealTypeCode = x.Deal.DealType.SystemName,
                                                        DealTypeName = x.Deal.DealType.Name,

                                                        DeliveryTypeId = x.Deal.DeliveryTypeId,
                                                        DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                        MerchantAddressId = x.Deal.Account.AddressId,
                                                        MerchantAddresskey = x.Deal.Account.AddressNavigation.Guid,


                                                        ActualPrice = x.Deal.ActualPrice,
                                                        SellingPrice = x.Deal.SellingPrice,

                                                        Amount = x.Deal.Amount,

                                                        Purchase = x.Deal.MDDealCode.Count,
                                                        Likes = x.Deal.Like,
                                                        DisLikes = x.Deal.DisLike,
                                                        Views = x.Deal.Views,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealsList = _HCoreContext.MDFlashDeal
                                                    .Where(x => x.Deal.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.StatusId == HelperStatus.Default.Active
                                                    && x.Deal.StatusId == HelperStatus.Deals.Published)
                                                    .Select(x => new ODealOperation.DealList
                                                    {
                                                        ReferenceId = x.Deal.Id,
                                                        ReferenceKey = x.Deal.Guid,

                                                        Title = x.Deal.TitleContent,

                                                        MerchantId = x.Deal.AccountId,
                                                        MerchantDisplayName = x.Deal.Account.DisplayName,
                                                        MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                        CategoryId = x.Deal.CategoryId,
                                                        CategoryName = x.Deal.Category.Name,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        DealTypeId = x.Deal.DealTypeId,
                                                        DealTypeCode = x.Deal.DealType.SystemName,
                                                        DealTypeName = x.Deal.DealType.Name,

                                                        DeliveryTypeId = x.Deal.DeliveryTypeId,
                                                        DeliveryTypeCode = x.Deal.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.Deal.DeliveryType.Name,

                                                        MerchantAddressId = x.Deal.Account.AddressId,
                                                        MerchantAddresskey = x.Deal.Account.AddressNavigation.Guid,


                                                        ActualPrice = x.Deal.ActualPrice,
                                                        SellingPrice = x.Deal.SellingPrice,

                                                        //Purchase = x.Deal.MDDealCode.Count(),

                                                        Amount = x.Deal.Amount,

                                                        Purchase = x.Deal.MDDealCode.Count,
                                                        Likes = x.Deal.Like,
                                                        DisLikes = x.Deal.DisLike,
                                                        Views = x.Deal.Views,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealsList)
                        {
                            if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                            {
                                DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                            }
                            else
                            {
                                DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Poster;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);

                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.StatusId == HelperStatus.Deals.Published)
                                                    .Select(x => new ODealOperation.DealList
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.TitleContent,

                                                        MerchantId = x.AccountId,
                                                        MerchantDisplayName = x.Account.DisplayName,
                                                        MerchantIconUrl = x.Account.IconStorage.Path,

                                                        CategoryId = x.CategoryId,
                                                        CategoryName = x.Category.Name,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        MerchantAddressId = x.Account.AddressId,
                                                        MerchantAddresskey = x.Account.AddressNavigation.Guid,


                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,

                                                        //Purchase = x.MDDealCode.Count(),

                                                        Amount = x.Amount,

                                                        Purchase = x.MDDealCode.Count,
                                                        Likes = x.Like,
                                                        DisLikes = x.DisLike,
                                                        Views = x.Views,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealsList = _HCoreContext.MDDeal
                                                    .Where(x => x.Account.CountryId == _Request.UserReference.CountryId)
                                                    .Where(x => x.StatusId == HelperStatus.Deals.Published)
                                                    .Select(x => new ODealOperation.DealList
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.TitleContent,

                                                        MerchantId = x.AccountId,
                                                        MerchantDisplayName = x.Account.DisplayName,
                                                        MerchantIconUrl = x.Account.IconStorage.Path,

                                                        CategoryId = x.CategoryId,
                                                        CategoryName = x.Category.Name,
                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,

                                                        ImageUrl = x.PosterStorage.Path,

                                                        MerchantAddressId = x.Account.AddressId,
                                                        MerchantAddresskey = x.Account.AddressNavigation.Guid,

                                                        DealTypeId = x.DealTypeId,
                                                        DealTypeCode = x.DealType.SystemName,
                                                        DealTypeName = x.DealType.Name,

                                                        DeliveryTypeId = x.DeliveryTypeId,
                                                        DeliveryTypeCode = x.DeliveryType.SystemName,
                                                        DeliveryTypeName = x.DeliveryType.Name,

                                                        ActualPrice = x.ActualPrice,
                                                        SellingPrice = x.SellingPrice,
                                                        Amount = x.Amount,

                                                        Purchase = x.MDDealCode.Count,
                                                        Likes = x.Like,
                                                        DisLikes = x.DisLike,
                                                        Views = x.Views,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealsList)
                        {
                            if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                            {
                                DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                            }
                            else
                            {
                                DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Poster;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);

                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealMerchants(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealMerchants = new List<ODealOperation.DealMerchantList>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                    .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == Helpers.UserAccountType.Merchant
                                                && x.StatusId == HelperStatus.Default.Active
                                                && x.MDDealAccount.Any(a => a.StatusId == HelperStatus.Deals.Published))
                                                .Select(x => new ODealOperation.DealMerchantList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,
                                                    IconUrl = x.IconStorage.Path,
                                                    Rating = x.AverageValue,

                                                    Deals = x.MDDealAccount.Count(a => a.StatusId == HelperStatus.Deals.Published),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _DealMerchants = _HCoreContext.HCUAccount
                                                    .Where(x => x.CountryId == _Request.UserReference.CountryId)
                                                .Where(x => x.AccountTypeId == Helpers.UserAccountType.Merchant
                                                && x.StatusId == HelperStatus.Default.Active
                                                && x.MDDealAccount.Any(a => a.StatusId == HelperStatus.Deals.Published))
                                                .Select(x => new ODealOperation.DealMerchantList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    DisplayName = x.DisplayName,
                                                    IconUrl = x.IconStorage.Path,
                                                    Rating = x.AverageValue,

                                                    Deals = x.MDDealAccount.Count(a => a.StatusId == HelperStatus.Deals.Published),
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _DealMerchants)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealMerchants", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _DealDetails = new ODealOperation.Details();
                    _DealDetails = _HCoreContext.MDDeal
                                                .Where(x =>
                                                 x.Id == _Request.ReferenceId
                                                && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new ODealOperation.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountIconUrl = x.Account.IconStorage.Path,
                                                    AccountRating = x.Account.AverageValue,

                                                    Title = x.TitleContent,
                                                    Description = x.Description,
                                                    Terms = x.Terms,

                                                    UsageInformation = x.UsageInformation,

                                                    EndDate = x.EndDate,

                                                    CodeValidityDays = x.CodeValidtyDays,
                                                    CodeValidityStartDate = x.CodeValidityStartDate,
                                                    CodeValidityEndDate = x.CodeValidityEndDate,

                                                    ActualPrice = x.ActualPrice,
                                                    SellingPrice = x.SellingPrice,
                                                    DiscountPercentage = x.DiscountPercentage,

                                                    Amount = x.Amount,
                                                    Charge = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    MaximumUnitSale = x.MaximumUnitSale,
                                                    MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,


                                                    ImageUrl = x.PosterStorage.Path,

                                                    DealTypeId = x.DealTypeId,
                                                    DealTypeCode = x.DealType.SystemName,
                                                    DealTypeName = x.DealType.Name,

                                                    DeliveryTypeId = x.DeliveryTypeId,
                                                    DeliveryTypeCode = x.DeliveryType.SystemName,
                                                    DeliveryTypeName = x.DeliveryType.Name,

                                                    MerchantAddressId = x.Account.AddressId,
                                                    MerchantAddresskey = x.Account.AddressNavigation.Guid,

                                                    Views = x.MDDealView.Count,
                                                    Likes = x.Like,
                                                    DisLikes = x.DisLike,
                                                    TotalPurchase = x.MDDealCode.Count,
                                                    CategoryName = x.Category.Name,
                                                    StatusId = x.StatusId
                                                }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        if (_DealDetails.StatusId == HelperStatus.Deals.Published)
                        {
                            _DealDetails.TotalPurchaseRemaining = _DealDetails.MaximumUnitSale - _DealDetails.TotalPurchase;
                            var FlashDealInfo = _HCoreContext.MDFlashDeal.Where(x => x.DealId == _Request.ReferenceId)
                               .Select(x => new
                               {
                                   ReferenceId = x.Id,
                                   ReferenceKey = x.Guid,
                                   StartDate = x.StartDate,
                                   EndDate = x.EndDate,
                                   Amount = x.Amount,
                               }).FirstOrDefault();
                            if (FlashDealInfo != null)
                            {
                                _DealDetails.IsFlashDeal = true;
                                _DealDetails.FlashDealAmount = FlashDealInfo.Amount;
                                _DealDetails.FlashDealStartDate = FlashDealInfo.StartDate;
                                _DealDetails.FlashDealEndDate = FlashDealInfo.EndDate;
                            }
                            else
                            {
                                _DealDetails.IsFlashDeal = false;
                            }
                            _DealDetails.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == _DealDetails.ReferenceId)
                                .Select(x => new ODealOperation.Location
                                {

                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    DisplayName = x.Location.DisplayName,
                                    Address = x.Location.Address,
                                    Latitude = x.Location.Latitude,
                                    Longitude = x.Location.Longitude,

                                }).ToList();
                            if (_DealDetails.Locations.Count() == 0)
                            {
                                _DealDetails.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _DealDetails.AccountId && x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new ODealOperation.Location
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    DisplayName = x.DisplayName,
                                    Address = x.Address,
                                    Latitude = x.Latitude,
                                    Longitude = x.Longitude,
                                    ContactNumber = x.ContactNumber
                                }).ToList();
                            }
                            string StoreContent = " <div><br></div> <div> <b> Deal can redeemed at below store locations :  </b> <div>  <div><br></div>";
                            foreach (var Location in _DealDetails.Locations)
                            {
                                StoreContent = StoreContent + "<div style='font-weight : 600 !important ; color : #b11a83 !important; font-size : 14px  !important;'> <b>" + Location.DisplayName + "</b></div> <div style='font-size : 11px  !important; padding-bottom : 8px; '>" + Location.Address + "</div>\n<br>  ";
                            }
                            if (!string.IsNullOrEmpty(_DealDetails.Terms))
                            {
                                _DealDetails.Terms = _DealDetails.Terms + StoreContent;
                            }
                            else
                            {
                                _DealDetails.Terms = _DealDetails.Terms;
                            }
                            _DealDetails.Images = _HCoreContext.MDDealGallery.Where(x => x.DealId == _DealDetails.ReferenceId)
                                .Select(x => new ODealOperation.Gallery
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    ImageUrl = x.ImageStorage.Path,
                                    IsDefault = x.IsDefault,
                                }).ToList();
                            if (!string.IsNullOrEmpty(_DealDetails.ImageUrl))
                            {
                                _DealDetails.ImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                            }
                            else
                            {
                                _DealDetails.ImageUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_DealDetails.AccountIconUrl))
                            {
                                _DealDetails.AccountIconUrl = _AppConfig.StorageUrl + _DealDetails.AccountIconUrl;
                            }
                            else
                            {
                                _DealDetails.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (_DealDetails.Images != null && _DealDetails.Images.Count > 0)
                            {
                                foreach (var Image in _DealDetails.Images)
                                {
                                    if (!string.IsNullOrEmpty(Image.ImageUrl))
                                    {
                                        Image.ImageUrl = _AppConfig.StorageUrl + Image.ImageUrl;
                                    }
                                    else
                                    {
                                        Image.ImageUrl = _AppConfig.Default_Icon;
                                    }
                                }
                            }
                            _DealDetails.RedeemInstruction = "- You can redeem deal code through Pos machine or ask merchant to scan QR code. \n - POS Redeem : On pos select ThankUCash Redeem Option. Enter deal code and your pin. Pos will validate your code and print receipt for redeem. \n - Other Option  : Ask merchant to scan qr code to redeem. /n - Merchant will scan code and redeem your coupon to avail deal.";
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealDetails, "HCD0200", ResponseCode.HCD0200);

                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0198", ResponseCode.HCP0198);
                        }



                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the deal view.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveDealView(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealDetails = _HCoreContext.MDDeal
                                                  .Where(x => x.Id == _Request.ReferenceId
                                                  && x.Guid == _Request.ReferenceKey)
                                                  .FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        _DealDetails.Views += 1;
                        _DealDetails.TView = _DealDetails.Views * 3;
                        _DealDetails.TPurchase = _DealDetails.Views * 2;
                        _DealDetails.TLike = _DealDetails.Views * 2;
                        _MDDealView = new MDDealView();
                        _MDDealView.DealId = _DealDetails.Id;
                        _MDDealView.CustomerId = _Request.UserReference.AccountId;
                        _MDDealView.Latitude = _Request.UserReference.RequestLatitude;
                        _MDDealView.Longitude = _Request.UserReference.RequestLongitude;
                        _MDDealView.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.MDDealView.Add(_MDDealView);
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0108", ResponseCode.HCP0108);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDealView", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the deal comment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDealComment(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.Comment))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0144", ResponseCode.HCP0144);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealDetails = _HCoreContext.MDDealCode
                                                  .Where(x => x.Id == _Request.ReferenceId
                                                  && x.Guid == _Request.ReferenceKey)
                                                  .FirstOrDefault();
                    if (_DealDetails != null)
                    {

                        _DealDetails.Comment = _Request.Comment;
                        _DealDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _DealDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0145", ResponseCode.HCP0145);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDealView", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Buydeal initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyDeal_Initialize(ODealOperation.DealPayment.Initialize _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.UserReference.AccountId > 0)
                {
                    _Request.AccountId = _Request.UserReference.AccountId;
                    _Request.AccountKey = _Request.UserReference.AccountKey;
                }
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                //if (_Request.ReferenceId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                //}
                //if (string.IsNullOrEmpty(_Request.ReferenceKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                //}
                if (string.IsNullOrEmpty(_Request.PaymentSource))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0111", ResponseCode.HCP0111);
                }
                if (_Request.PaymentSource != "wallet" && _Request.PaymentSource != "online")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0112", ResponseCode.HCP0112);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealDetails = _HCoreContext.MDDeal
                                                 .Where(x =>
                                                  x.Id == _Request.ReferenceId
                                                 && x.Guid == _Request.ReferenceKey)
                                                 .Select(x => new
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     MerchantStatusId = x.Account.StatusId,

                                                     EndDate = x.EndDate,

                                                     UsageTypeId = x.UsageTypeId,
                                                     CodeValidityDays = x.CodeValidtyDays,
                                                     CodeValidityStartDate = x.CodeValidityStartDate,
                                                     CodeValidityEndDate = x.CodeValidityEndDate,

                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     MaximumUnitSale = x.MaximumUnitSale,
                                                     MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,
                                                     MaximumUnitSalePerPerson = x.MaximumUnitSalePerPerson,
                                                     StatusId = x.StatusId,
                                                     BuyerTypeId = x.BuyerTypeId,
                                                 }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        long? SoldDealsCount = _HCoreContext.MDDealCode.Where(x => x.DealId == _DealDetails.ReferenceId).Sum(a => a.ItemCount);
                        long? AvailableDealsCount = _DealDetails.MaximumUnitSale - SoldDealsCount;
                        if (AvailableDealsCount <= 0)
                        {
                            AvailableDealsCount = 0;
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0217", ResponseCode.HCP0217);
                        }
                        if (_DealDetails.StatusId != HelperStatus.Deals.Published)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0189", ResponseCode.HCP0189);
                        }
                        DateTime currentdatetime = HCoreHelper.GetGMTDateTime();
                        if (_DealDetails.StatusId != HelperStatus.Deals.Published || currentdatetime > _DealDetails.EndDate)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0198", ResponseCode.HCP0198);
                        }
                        if (_DealDetails.MerchantStatusId != HelperStatus.Default.Active)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", ResponseCode.HCP0137);
                        }
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                DisplayName = x.DisplayName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                                AccountStatusId = x.StatusId,
                            }).FirstOrDefault();
                        if (CustomerDetails.AccountStatusId != HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0139", ResponseCode.HCP0139);
                        }
                        long TotalPurchase = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId);
                        if (TotalPurchase <= _DealDetails.MaximumUnitSale)
                        {
                            DateTime TodaysDate = HCoreHelper.GetGMTDate();
                            long TotalPurchaseForDay = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId && x.CreateDate.Date == TodaysDate);
                            if (TotalPurchaseForDay < _DealDetails.MaximumUnitSalePerDay)
                            {
                                long? MaxUnitSale = _DealDetails.MaximumUnitSalePerPerson;
                                if (MaxUnitSale == null)
                                {
                                    MaxUnitSale = 0;
                                }
                                long? UserPurchase = _HCoreContext.MDDealCode.Where(x => x.DealId == _DealDetails.ReferenceId && x.AccountId == _Request.AccountId).Sum(x => x.ItemCount);
                                if (UserPurchase < MaxUnitSale)
                                {
                                    long? AvailableQuantity = MaxUnitSale - UserPurchase;
                                    if (_Request.ItemCount > MaxUnitSale)
                                    {
                                        _HCoreContext.Dispose();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0134", "You have selected quantity more than maximum purchase limit per user for this deal.");
                                    }
                                    if (_Request.ItemCount > AvailableQuantity)
                                    {
                                        _HCoreContext.Dispose();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0134", "You have selected quantity more than your available purchase limit for this deal. Your available limit is: " + AvailableQuantity);
                                    }
                                    if (_DealDetails.BuyerTypeId == 1) // All Customers
                                    {

                                    }
                                    else if (_DealDetails.BuyerTypeId == 2) // New Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.AccountId);
                                        if (UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for new customers only");
                                        }
                                    }
                                    else if (_DealDetails.BuyerTypeId == 3) // Existing Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.AccountId);
                                        if (!UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for existing customers only");
                                        }
                                    }
                                    else
                                    {

                                    }

                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    double AccountBalance = _ManageCoreTransaction.GetAccountBalance(CustomerDetails.ReferenceId, TransactionSource.TUC);
                                    if (_Request.PaymentSource == "wallet")
                                    {
                                        if (AccountBalance < _DealDetails.Amount)
                                        {
                                            _HCoreContext.Dispose();
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0138", ResponseCode.HCP0138);
                                        }
                                    }
                                    double DeliveryCharge = Math.Round(_Request.DeliveryCharge, 2);
                                    double? DealAmount = 0;
                                    if (_Request.ItemCount > 0)
                                    {
                                        DealAmount = _Request.ItemCount * _DealDetails.Amount;
                                    }
                                    else
                                    {
                                        DealAmount = _DealDetails.Amount;
                                    }
                                    var _Response = new
                                    {
                                        ReferenceId = HCoreHelper.GenerateRandomNumber(4),
                                        ReferenceKey = HCoreHelper.GenerateGuid(),
                                        Amount = Math.Round((double)DealAmount, 2),
                                        Fee = 0,
                                        PaymentSource = _Request.PaymentSource,
                                        TotalAmount = Math.Round((double)DealAmount + (double)DeliveryCharge, 2),
                                        Balance = Math.Round(AccountBalance, 2),
                                        DeliveryCharge = Math.Round(DeliveryCharge, 2)
                                    };
                                    _HCoreContext.Dispose();

                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0110", ResponseCode.HCP0110);
                                }
                                else
                                {
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0134", ResponseCode.HCP0134);
                                }
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0135", ResponseCode.HCP0135);
                            }
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", ResponseCode.HCP0133);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Buydeal confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyDeal_Confirm(ODealOperation.DealPayment.Confirm _Request)
        {
            long accountIdResult = 0;
            #region Manage Exception
            try
            {
                int newUserCheck = 0;

                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (_Request.DealId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.PaymentSource))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0111", ResponseCode.HCP0111);
                }
                if (_Request.PaymentSource != "wallet" && _Request.PaymentSource != "online")
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0112", ResponseCode.HCP0112);
                }
                using (_HCoreContext = new HCoreContext())
                {


                    var _DealDetails = _HCoreContext.MDDeal
                                                 .Where(x =>
                                                  x.Id == _Request.DealId
                                                 && x.Guid == _Request.DealKey)
                                                 .Select(x => new
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountId = x.AccountId,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,

                                                     Title = x.TitleContent,
                                                     DealTitle = x.Title,
                                                     Description = x.Description,
                                                     MerchantId = x.AccountId,
                                                     MerchantDisplayName = x.Account.DisplayName,
                                                     MerchantIconUrl = x.Account.IconStorage.Path,
                                                     MerchantContactNumber = x.Account.ContactNumber,
                                                     MerchantAddress = x.Account.Address,
                                                     MerchantEmail = x.Account.EmailAddress,

                                                     ImageUrl = x.PosterStorage.Path,

                                                     UsageTypeId = x.UsageTypeId,
                                                     CodeValidityDays = x.CodeValidtyDays,
                                                     CodeValidityStartDate = x.CodeValidityStartDate,
                                                     CodeValidityEndDate = x.CodeValidityEndDate,
                                                     Terms = x.Terms,
                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     MaximumUnitSale = x.MaximumUnitSale,
                                                     MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,
                                                     MaximumUnitSalePerPerson = x.MaximumUnitSalePerPerson,
                                                     BuyerTypeId = x.BuyerTypeId,
                                                 }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        accountIdResult = _Request.AccountId;
                        newUserCheck = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == accountIdResult).Count();
                        int? TotalPurchase = _HCoreContext.MDDealCode.Where(x => x.DealId == _DealDetails.ReferenceId).Sum(a => a.ItemCount);
                        if (TotalPurchase <= _DealDetails.MaximumUnitSale)
                        {
                            DateTime TodaysDate = HCoreHelper.GetGMTDate();
                            long TotalPurchaseForDay = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId && x.CreateDate.Date == TodaysDate);
                            if (TotalPurchaseForDay < _DealDetails.MaximumUnitSalePerDay)
                            {
                                long? MaxUnitSale = _DealDetails.MaximumUnitSalePerPerson;
                                if (MaxUnitSale == null)
                                {
                                    MaxUnitSale = 0;
                                }
                                long? UserPurchase = _HCoreContext.MDDealCode.Where(x => x.DealId == _DealDetails.ReferenceId && x.AccountId == _Request.UserReference.AccountId).Sum(x => x.ItemCount);
                                if (UserPurchase < MaxUnitSale)
                                {
                                    long? AvailableQuantity = MaxUnitSale - UserPurchase;
                                    if (_Request.ItemCount > MaxUnitSale)
                                    {
                                        _HCoreContext.Dispose();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0134", "You have selected quantity more than maximum purchase limit per user for this deal.");
                                    }
                                    if (_Request.ItemCount > AvailableQuantity)
                                    {
                                        _HCoreContext.Dispose();
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0134", "You have selected quantity more than your available purchase limit for this deal. Your available limit is: " + AvailableQuantity);
                                    }
                                    if (_DealDetails.BuyerTypeId == 1) // All Customers
                                    {

                                    }
                                    else if (_DealDetails.BuyerTypeId == 2) // New Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.UserReference.AccountId);
                                        if (UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for new customers only");
                                        }
                                    }
                                    else if (_DealDetails.BuyerTypeId == 3) // Existing Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.UserReference.AccountId);
                                        if (!UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for existing customers only");
                                        }
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", "Deal sold. Your amount is credited back to your ThankUCash Wallet.");
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", "Deal sold. Your amount is credited back to your ThankUCash Wallet.");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", "Deal sold. Your amount is credited back to your ThankUCash Wallet.");
                        }

                        if (_Request.TotalAmount == 0)
                        {
                            _Request.TotalAmount = (double)_DealDetails.TotalAmount;
                        }

                        _ManageCoreTransaction = new ManageCoreTransaction();
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                DisplayName = x.DisplayName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                                AccountStatusId = x.StatusId,
                                Address = x.Address,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                            }).FirstOrDefault();
                        if (_Request.PaymentSource == "wallet")
                        {
                            double AccountBalance = _ManageCoreTransaction.GetAccountBalance(CustomerDetails.ReferenceId, TransactionSource.TUC);
                            if (AccountBalance < _Request.TotalAmount)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0138", ResponseCode.HCP0138);
                            }
                        }
                        if (CustomerDetails.AccountStatusId != HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0139", ResponseCode.HCP0139);
                        }
                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CustomerId = _Request.UserReference.AccountId;
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = _Request.ReferenceKey;
                        _CoreTransactionRequest.ParentId = _DealDetails.AccountId;
                        _CoreTransactionRequest.InvoiceAmount = (double)_Request.TotalAmount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                        _CoreTransactionRequest.ReferenceNumber = _DealDetails.ReferenceKey;
                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                        _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        if (_Request.PaymentSource == "wallet")
                        {
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.UserReference.AccountId,
                                SourceId = TransactionSource.TUC,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.Deal.DealPurchaseRedeem,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                Charge = 0,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _DealDetails.AccountId,
                                SourceId = TransactionSource.Deals,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.Deal.DealPurchase,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                Charge = 0,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                        }
                        else
                        {
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.UserReference.AccountId,
                                SourceId = TransactionSource.TUC,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.Deal.DealPurchase,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                Charge = 0,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.UserReference.AccountId,
                                SourceId = TransactionSource.TUC,
                                ModeId = TransactionMode.Debit,
                                TypeId = TransactionType.Deal.DealPurchaseRedeem,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                Charge = 0,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _DealDetails.AccountId,
                                SourceId = TransactionSource.Deals,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionType.Deal.DealPurchase,
                                Amount = _CoreTransactionRequest.InvoiceAmount,
                                Charge = 0,
                                TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                            });
                        }
                        _CoreTransactionRequest.Transactions = _TransactionItems;

                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            _MDDealCode = new MDDealCode();
                            _MDDealCode.Guid = _Request.ReferenceKey;
                            _MDDealCode.DealId = _DealDetails.ReferenceId;
                            _MDDealCode.AccountId = _Request.UserReference.AccountId;
                            _MDDealCode.ItemCode = "12" + HCoreHelper.GenerateRandomNumber(9);
                            _MDDealCode.ItemPin = HCoreHelper.GenerateRandomNumber(4);
                            _MDDealCode.ItemAmount = _DealDetails.Amount;
                            _MDDealCode.AvailableAmount = _DealDetails.Amount * _Request.ItemCount;
                            _MDDealCode.TransactionId = TransactionResponse.ReferenceId;
                            _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.ItemCount > 0)
                            {
                                _MDDealCode.ItemCount = _Request.ItemCount;
                            }
                            else
                            {
                                _MDDealCode.ItemCount = 1;
                            }
                            if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase || _DealDetails.UsageTypeId == Helpers.DealCodeUsageType.DaysAfterPurchase)
                            {
                                if (_DealDetails.CodeValidityDays != null && _DealDetails.CodeValidityDays > 0)
                                {
                                    _MDDealCode.EndDate = _MDDealCode.StartDate.Value.AddHours((double)_DealDetails.CodeValidityDays);
                                }
                                else
                                {
                                    _MDDealCode.EndDate = _MDDealCode.StartDate.Value.AddHours(168);
                                }
                            }
                            else if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.DealEndDate)
                            {
                                _MDDealCode.EndDate = _DealDetails.EndDate;
                            }
                            else if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.ExpiresOnDate)
                            {
                                if (_DealDetails.CodeValidityEndDate != null)
                                {
                                    _MDDealCode.EndDate = _DealDetails.CodeValidityEndDate;
                                }
                                else
                                {
                                    _MDDealCode.EndDate = _DealDetails.EndDate;
                                }
                            }
                            else
                            {
                                _MDDealCode.EndDate = _DealDetails.EndDate;
                            }

                            _MDDealCode.Amount = _Request.Amount;
                            _MDDealCode.Charge = _Request.Charge;
                            _MDDealCode.DeliveryCharge = _Request.DeliveryCharge;
                            _MDDealCode.TotalAmount = _Request.TotalAmount;
                            if (_Request.FromAddressId > 0)
                            {
                                var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.FromAddressId).Select(x => new { x.Id }).FirstOrDefault();
                                if (AddressDetails != null)
                                {
                                    _MDDealCode.FromAddressId = AddressDetails.Id;
                                }
                            }
                            if (_Request.ToAddressId > 0)
                            {
                                var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.ToAddressId).Select(x => new { x.Id }).FirstOrDefault();
                                if (AddressDetails != null)
                                {
                                    _MDDealCode.ToAddressId = AddressDetails.Id;
                                }
                            }
                            if (_Request.FromAddressId > 0)
                            {
                                _MDDealCode.Comment = _Request.FromAddressId + "|";
                            }
                            else
                            {
                                _MDDealCode.Comment = "0|";
                            }
                            if (_Request.ToAddressId > 0)
                            {
                                _MDDealCode.Comment += _Request.ToAddressId;
                            }
                            else
                            {
                                _MDDealCode.Comment += "0";
                            }

                            if (HostEnvironment == HostEnvironmentType.Dev)
                            {
                                _MDDealCode.PartnerId = 20688;
                            }
                            else if (HostEnvironment == HostEnvironmentType.Tech)
                            {
                                _MDDealCode.PartnerId = 1326826;
                            }
                            else if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                _MDDealCode.PartnerId = 336897;
                            }
                            else
                            {
                                _MDDealCode.PartnerId = 743666;
                            }

                            _MDDealCode.UseCount = 0;
                            _MDDealCode.UseAttempts = 0;
                            _MDDealCode.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDDealCode.CreatedById = _Request.UserReference.AccountId;
                            _MDDealCode.StatusId = HelperStatus.DealCodes.Unused;
                            _HCoreContext.MDDealCode.Add(_MDDealCode);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();

                            Panelurls _Panelurls = new Panelurls();
                            if (HostEnvironment == HostEnvironmentType.Dev)
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.dev/deal/";
                                _Panelurls.MerchantUrl = "https://merchant-dealday.thankucash.dev/";
                            }
                            else if (HostEnvironment == HostEnvironmentType.Tech)
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.tech/deal/";
                                _Panelurls.MerchantUrl = "https://merchant-dealday.thankucash.tech/";
                            }
                            else if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.co/deal/";
                                _Panelurls.MerchantUrl = "https://merchant.dealday.africa/";
                            }
                            else
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.com/deal/";
                                _Panelurls.MerchantUrl = "https://merchant.dealday.africa/";
                            }

                            if (_Request.CarrierId != null || _Request.CarrierId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var OrderDetails = _HCoreContext.LSShipments.Where(x => x.DealId == _DealDetails.ReferenceId
                                                       && x.CustomerId == CustomerDetails.ReferenceId
                                                       && x.Id == _Request.TrackingId && x.Guid == _Request.TrackingKey).FirstOrDefault();
                                    if (OrderDetails != null)
                                    {
                                        var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Guid == _MDDealCode.Guid).FirstOrDefault();
                                        var DeliveryPartner = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.DeliveryPartnerKey).Select(x => new
                                        {
                                            x.Id,
                                            x.Guid

                                        }).FirstOrDefault();

                                        OrderDetails.RateId = _Request.RateId;
                                        OrderDetails.RateAmount = _Request.DeliveryCharge;
                                        OrderDetails.TransactionReference = _MDDealCode.TransactionId.ToString();
                                        OrderDetails.CarrierId = _Request.CarrierId;
                                        OrderDetails.DeliveryPartnerId = DeliveryPartner.Id;
                                        double? TotalIemAmount = OrderDetails.DealPrice * _Request.ItemCount;
                                        OrderDetails.TotalAmount = TotalIemAmount + _Request.DeliveryCharge;
                                        if (DeliveryPartner.Guid == HCoreConstant.DeliveryPartners.Dellyman)
                                        {
                                            OrderDetails.TId = HCoreHelper.GenerateRandomNumber(11);
                                        }
                                        if (DeliveryPartner.Guid == HCoreConstant.DeliveryPartners.GoShiip)
                                        {
                                            OrderDetails.TId = _Request.RedisKey;
                                            OrderDetails.ShipmentId = _Request.KwikKey;
                                        }

                                        DealCodeDetails.OrderId = OrderDetails.Id;

                                        _HCoreContext.SaveChanges();

                                        var DeliveryAddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == OrderDetails.ToAddressId).Select(x => new
                                        {
                                            Address = x.AddressLine1 + ", " + x.AddressLine2 + ", " + x.City.Name + ", " + x.State.Name,
                                        }).FirstOrDefault();

                                        OrderNotification _OrderNotification = new OrderNotification();
                                        _OrderNotification.MerchantDisplayName = _DealDetails.MerchantDisplayName;
                                        _OrderNotification.EmailAddress = _DealDetails.MerchantEmail;
                                        _OrderNotification.OrderNumber = _MDDealCode.Id.ToString();
                                        _OrderNotification.DealCode = _MDDealCode.ItemCode;
                                        _OrderNotification.DeliveryAddress = DeliveryAddressDetails.Address;
                                        _OrderNotification.DealTitle = _DealDetails.DealTitle;
                                        _OrderNotification.DealImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                                        _OrderNotification.PanelUrl = _Panelurls.MerchantUrl;
                                        _OrderNotification.Quantity = _MDDealCode.ItemCount;
                                        _OrderNotification.UserReference = _Request.UserReference;

                                        var _Actor = ActorSystem.Create("ActorNewOrderNotification");
                                        var _ActorNotify = _Actor.ActorOf<ActorNewOrderNotification>("ActorNewOrderNotification");
                                        _ActorNotify.Tell(_OrderNotification);
                                    }
                                }
                            }

                            if (_Request.PromoCodeId > 0 && !string.IsNullOrEmpty(_Request.PromoCodeKey))
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var PromoCodeDetails = _HCoreContext.MDDealPromoCode.Where(x => x.Id == _Request.PromoCodeId && x.Guid == _Request.PromoCodeKey).FirstOrDefault();
                                    if (PromoCodeDetails != null)
                                    {
                                        var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Guid == _MDDealCode.Guid).FirstOrDefault();

                                        PromoCodeDetails.StatusId = HelperStatus.DealCodes.Used;
                                        PromoCodeDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        PromoCodeDetails.ModifyById = _Request.UserReference.AccountId;

                                        MDDealPromoCodeAccount? _MDDealPromoCodeAccount = new MDDealPromoCodeAccount();

                                        _MDDealPromoCodeAccount.Guid = HCoreHelper.GenerateGuid();
                                        _MDDealPromoCodeAccount.PromoCodeId = PromoCodeDetails.Id;
                                        _MDDealPromoCodeAccount.AccountId = _Request.UserReference.AccountId;
                                        _MDDealPromoCodeAccount.UseDate = HCoreHelper.GetGMTDateTime();
                                        _HCoreContext.MDDealPromoCodeAccount.Add(_MDDealPromoCodeAccount);
                                        _HCoreContext.SaveChanges();

                                        DealCodeDetails.PromoCodeId = _MDDealPromoCodeAccount.Id;
                                        _HCoreContext.SaveChanges();

                                        var _Deals = _HCoreContext.MDDealPromoCode.Where(x => x.Code == PromoCodeDetails.Code && x.Id != PromoCodeDetails.Id).ToList();
                                        if (_Deals.Count > 0)
                                        {
                                            foreach (var Deal in _Deals)
                                            {
                                                List<MDDealPromoCodeAccount>? _MDDealPromoCodeAccounts = new List<MDDealPromoCodeAccount>();
                                                Deal.StatusId = HelperStatus.DealCodes.Used;
                                                Deal.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                Deal.ModifyById = _Request.UserReference.AccountId;

                                                _MDDealPromoCodeAccount.Guid = HCoreHelper.GenerateGuid();
                                                _MDDealPromoCodeAccount.PromoCodeId = Deal.Id;
                                                _MDDealPromoCodeAccount.AccountId = _Request.UserReference.AccountId;
                                                _MDDealPromoCodeAccount.UseDate = HCoreHelper.GetGMTDateTime();
                                                _MDDealPromoCodeAccounts.Add(_MDDealPromoCodeAccount);
                                            }
                                            _HCoreContext.SaveChanges();
                                        }

                                        _HCoreContext.Dispose();
                                    }
                                }
                            }

                            var _Response = new
                            {
                                ReferenceId = _MDDealCode.Id,
                                ReferenceKey = _MDDealCode.Guid,
                            };
                            try
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string? UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.UserReference.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                                    {
                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Purchase Successful", _DealDetails.Title + " deal puchase successful for " + _DealDetails.MerchantDisplayName, "dealpurchasedetails", _Response.ReferenceId, _Response.ReferenceKey, "View details", true, null);
                                    }

                                    if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                                    {
                                        var Stores = _HCoreContext.HCUAccount
                                            .Where(x => x.OwnerId == _DealDetails.MerchantId && x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Active)
                                            .Select(x => new
                                            {
                                                Name = x.Name,
                                                ContactNumber = x.ContactNumber,
                                                Address = x.Address,
                                            })
                                            .Skip(0)
                                            .Take(5)
                                            .ToList();
                                        string MerchantIcon = _DealDetails.MerchantIconUrl;
                                        if (!string.IsNullOrEmpty(MerchantIcon))
                                        {
                                            MerchantIcon = _AppConfig.StorageUrl + _DealDetails.MerchantIconUrl;
                                        }
                                        else
                                        {
                                            MerchantIcon = _AppConfig.Default_Icon;
                                        }
                                        var _BDetails = new
                                        {
                                            DisplayName = CustomerDetails.DisplayName,
                                            DealCode = _MDDealCode.ItemCode,
                                            StartDate = _MDDealCode.StartDate.Value.ToString("dd-MM-yyyy HH:mm"),
                                            EndDate = _MDDealCode.EndDate.Value.AddHours(1).ToString("dd-MM-yyyy HH:mm"),
                                            Title = _DealDetails.Title,
                                            Description = _DealDetails.Description,
                                            Terms = _DealDetails.Terms,
                                            ImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl,
                                            Amount = _DealDetails.Amount,
                                            MerchantEmailAddress = _DealDetails.MerchantEmail,
                                            MerchantDisplayName = _DealDetails.MerchantDisplayName,
                                            MerchantLogo = MerchantIcon,
                                            MerchantContactNumber = _DealDetails.MerchantContactNumber,
                                            MerchantAddress = _DealDetails.MerchantAddress,
                                            StoreLocations = Stores
                                        };

                                        var _BDetailsForMerchant = new
                                        {
                                            merchantname = _DealDetails.MerchantDisplayName,
                                            noofdealsold = 1,
                                            customername = CustomerDetails.DisplayName,
                                            customeraddress = CustomerDetails.Address,
                                            trackingnumber = _MDDealCode.Id.ToString(),
                                            ordernumber = _MDDealCode.Id.ToString(),
                                            payableamount = _DealDetails.TotalAmount.ToString(),
                                            firstname = CustomerDetails.FirstName,
                                            lastname = CustomerDetails.LastName,
                                            productname = _DealDetails.Title,
                                            sellersku = "10001",
                                            shopsku = _DealDetails.MerchantId.ToString(),
                                            itemprice = _MDDealCode.ItemAmount.ToString(),
                                            total = _DealDetails.TotalAmount.ToString()

                                        };

                                        if (_Request.CarrierId == null || _Request.CarrierId <= 0)
                                        {
                                            DealPurchaseNotification _DealPurchaseNotification = new DealPurchaseNotification();
                                            _DealPurchaseNotification.MerchantDisplayName = _DealDetails.MerchantDisplayName;
                                            _DealPurchaseNotification.EmailAddress = _DealDetails.MerchantEmail;
                                            _DealPurchaseNotification.OrderNumber = _MDDealCode.Id.ToString();
                                            _DealPurchaseNotification.DealTitle = _DealDetails.DealTitle;
                                            _DealPurchaseNotification.DealImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                                            _DealPurchaseNotification.PanelUrl = _Panelurls.MerchantUrl;
                                            _DealPurchaseNotification.Quantity = _MDDealCode.ItemCount;
                                            _DealPurchaseNotification.UserReference = _Request.UserReference;

                                            var _Actors = ActorSystem.Create("ActorDealPurchaseNotification");
                                            var _ActorNotifys = _Actors.ActorOf<ActorDealPurchaseNotification>("ActorDealPurchaseNotification");
                                            _ActorNotifys.Tell(_DealPurchaseNotification);
                                        }

                                        ProductReview _ProductReview = new ProductReview();
                                        _ProductReview.CustomerDisplayName = CustomerDetails.DisplayName;
                                        _ProductReview.EmailAddress = CustomerDetails.EmailAddress;
                                        _ProductReview.DealTitle = _DealDetails.DealTitle;
                                        _ProductReview.DealImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                                        _ProductReview.DealId = _DealDetails.ReferenceId;
                                        _ProductReview.DealKey = _DealDetails.ReferenceKey;
                                        _ProductReview.PanelUrl = _Panelurls.DealsWebUrl;
                                        _ProductReview.UserReference = _Request.UserReference;

                                        var _Actor = ActorSystem.Create("ActorProductReviewNotification");
                                        var _ActorNotify = _Actor.ActorOf<ActorProductReviewNotification>("ActorProductReviewNotification");
                                        _ActorNotify.Tell(_ProductReview);

                                        //send email to customer
                                        HCoreHelper.BroadCastEmail("d-cfb071d1a1174d6d9b08ce8fdee6b6dd", "ThankUCash", CustomerDetails.EmailAddress, _BDetails, _Request.UserReference);

                                        try
                                        {

                                            StringBuilder Html = new StringBuilder(System.IO.File.ReadAllText("./templates/" + "invoice.html"));
                                            Html.Replace("{{customername}}", CustomerDetails.DisplayName);
                                            Html.Replace("{{customeraddress}}", CustomerDetails.Address);
                                            Html.Replace("{{trackingnumber}}", _MDDealCode.Id.ToString());
                                            Html.Replace("{{ordernumber}}", _MDDealCode.Id.ToString());
                                            Html.Replace("{{payableamount}}", _DealDetails.TotalAmount.ToString());

                                            Html.Replace("{{firstname}}", CustomerDetails.FirstName);
                                            Html.Replace("{{lastname}}", CustomerDetails.LastName);

                                            Html.Replace("{{productname}}", _DealDetails.Title);
                                            Html.Replace("{{sellersku}}", "10001");
                                            Html.Replace("{{shopsku}}", _DealDetails.MerchantId.ToString());
                                            Html.Replace("{{itemprice}}", _MDDealCode.ItemAmount.ToString());
                                            Html.Replace("{{total}}", _DealDetails.TotalAmount.ToString());

                                            //Html.Replace("{{returnqrcode}}", );

                                            string AttachmentName = "Invoice_" + _MDDealCode.Id.ToString() + ".pdf";
                                            string pdfBase64String = HCoreHelper.HtmlToPdfBase64(Html.ToString());


                                            //Send email to merchant
                                            HCoreHelper.BroadCastEmail("d-112d272ca74f42d1bd1c8f0e7043eaa9", "ThankUCash", _DealDetails.MerchantEmail, _BDetailsForMerchant, _Request.UserReference, AttachmentBase64String: pdfBase64String, AttachmentName: AttachmentName);

                                            //Send SMS to customer
                                            string CustomerMessage = "Hi " + CustomerDetails.DisplayName + "ThankU for placing an order with ThankUcash. Your order " + _DealDetails.Title + " with the serial number " + _MDDealCode.Id.ToString() + " has been confirmed. Here is the product Coupon code " + _MDDealCode.ItemCode + ". ThankU for shopping with DealDay";

                                            HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, CustomerDetails.MobileNumber, CustomerMessage, CustomerDetails.ReferenceId, null);

                                            //Send SMS to Merchant
                                            string MerchantMessage = "Hi " + _DealDetails.MerchantDisplayName + " You have received an order for " + _DealDetails.Title + " with the serial number" + _MDDealCode.Id.ToString() + ". Here is the product Coupon code " + _MDDealCode.ItemCode + ". ThankU from DealDay.";
                                            HCoreHelper.SendSMS(SmsType.Transaction, _Request.UserReference.CountryIsd, _DealDetails.MerchantContactNumber, MerchantMessage, _DealDetails.MerchantId, null);
                                        }
                                        catch (Exception ex)
                                        {
                                            HCoreHelper.LogException("BuyDeal_Confirm_sendemail", ex, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
                                        }
                                    }
                                }
                            }
                            catch (Exception _Exception)
                            {
                                HCoreHelper.LogException("BuyDeal_Confirm", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
                            }

                            //Validate if first time buyer and credit referrer
                            if (newUserCheck <= 1)
                            {
                                //This is a new user. Get Referral Details
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var ReferralDetails = _HCoreContext.ReferralSettings.Where(x => x.Status == HelperStatus.Default.Active)
                                       .Select(x => new OReferralAmountResponse
                                       {
                                           Amount = x.Amount

                                       })
                                       .FirstOrDefault();
                                    //Get Referrer
                                    var ReferrerId = _HCoreContext.HCUAccount.Where(x => x.Id == accountIdResult).FirstOrDefault();

                                    //Credit Referrer's Wallet
                                    if (ReferrerId.ReferrerId != null || ReferrerId.ReferrerId != "")
                                    {
                                        //Get Account Id of Referrer
                                        var ReferrerAccountId = _HCoreContext.HCUAccount.Where(x => x.ReferralCode == ReferrerId.ReferrerId).Select(x => new
                                        {

                                            id = x.Id
                                        }).FirstOrDefault();

                                        ManageWallet _ManageWallet = new ManageWallet();
                                        var creditReferralRequest = new OOperations.Wallet.Credit.Request
                                        {
                                            AccountId = ReferrerAccountId.id,
                                            AccountKey = _Request.ReferenceKey,
                                            Amount = Convert.ToDouble(ReferralDetails.Amount),
                                            Comment = "",
                                            PaymentReference = _Request.PaymentReference,
                                            PaymentType = "WalletFunding",
                                            SourceCode = TransactionSource.Payments.ToString(),
                                            TransactionReference = _Request.TransactionId.ToString(),
                                            UserReference = _Request.UserReference

                                        };
                                        var resp = _ManageWallet.FundWallet(creditReferralRequest);
                                        if (resp.Status != "Success")
                                        {

                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP02178", ResponseCode.HCP02178);
                                        }
                                    }

                                }





                            }



                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0109", ResponseCode.HCP0109);

                            //var _Response = new
                            //{
                            //    ReferenceId = TransactionResponse.ReferenceId,
                            //    ReferenceKey = TransactionResponse.ReferenceKey,
                            //    Amount = _DealDetails.Amount,
                            //};
                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0110", ResponseCode.HCP0110);
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP0113", ResponseCode.HCP0113);
                            #endregion
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        //internal OResponse BuyDeal_Confirm(ODealOperation.DealPayment.Confirm _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (_Request.ReferenceId == 0)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
        //        }
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
        //        }
        //        if (_Request.DealId == 0)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
        //        }
        //        if (string.IsNullOrEmpty(_Request.DealKey))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
        //        }
        //        if (!string.IsNullOrEmpty(_Request.PaymentSource))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0111", ResponseCode.HCP0111);
        //        }
        //        if (_Request.PaymentSource != "wallet" && _Request.PaymentSource != "online")
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0112", ResponseCode.HCP0112);
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            var TransactionDetails = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.ReferenceNumber == _Request.ReferenceKey).FirstOrDefault();
        //            if (TransactionDetails != null)
        //            {
        //                if (TransactionDetails.StatusId == HelperStatus.Transaction.Initialized)
        //                {

        //                }
        //                else
        //                {
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0114", ResponseCode.HCP0114);
        //                }
        //            }
        //            else
        //            {
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
        //            }

        //            var _DealDetails = _HCoreContext.MDDeal
        //                                        .Where(x =>
        //                                         x.Id == _Request.DealId
        //                                        && x.Guid == _Request.DealKey)
        //                                        .Select(x => new
        //                                        {
        //                                            ReferenceId = x.Id,
        //                                            ReferenceKey = x.Guid,


        //                                            EndDate = x.EndDate,

        //                                            UsageTypeId = x.UsageTypeId,
        //                                            CodeValidityDays = x.CodeValidtyDays,
        //                                            CodeValidityStartDate = x.CodeValidityStartDate,
        //                                            CodeValidityEndDate = x.CodeValidityEndDate,

        //                                            Amount = x.Amount,
        //                                            Charge = x.Charge,
        //                                            TotalAmount = x.TotalAmount,
        //                                            MaximumUnitSale = x.MaximumUnitSale,
        //                                            MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,

        //                                        }).FirstOrDefault();
        //            if (_DealDetails != null)
        //            {
        //                _CoreTransactionRequest = new OCoreTransaction.Request();
        //                _CoreTransactionRequest.ParentTransactionKey = TransactionDetails.Guid;
        //                _CoreTransactionRequest.CustomerId = _Request.UserReference.AccountId;
        //                _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
        //                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                _CoreTransactionRequest.ParentId = 1;
        //                _CoreTransactionRequest.InvoiceAmount = (double)_DealDetails.Amount;
        //                _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
        //                //_CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
        //                _CoreTransactionRequest.ReferenceNumber = _DealDetails.ReferenceKey;
        //                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
        //                _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;
        //                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                if (_Request.PaymentSource == "wallet")
        //                {
        //                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                    {
        //                        UserAccountId = _Request.UserReference.AccountId,
        //                        SourceId = TransactionSource.TUC,
        //                        ModeId = TransactionMode.Debit,
        //                        TypeId = TransactionType.Deal.DealPurchaseRedeem,
        //                        Amount = _CoreTransactionRequest.InvoiceAmount,
        //                        Charge = 0,
        //                        TotalAmount = _CoreTransactionRequest.InvoiceAmount,
        //                    });
        //                }
        //                else
        //                {
        //                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                    {
        //                        UserAccountId = _Request.UserReference.AccountId,
        //                        SourceId = TransactionSource.TUC,
        //                        ModeId = TransactionMode.Credit,
        //                        TypeId = TransactionType.Deal.DealPurchase,
        //                        Amount = _CoreTransactionRequest.InvoiceAmount,
        //                        Charge = 0,
        //                        TotalAmount = _CoreTransactionRequest.InvoiceAmount,
        //                    });
        //                }
        //                _CoreTransactionRequest.Transactions = _TransactionItems;
        //                _ManageCoreTransaction = new ManageCoreTransaction();
        //                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                {
        //                    //var _Response = new
        //                    //{
        //                    //    ReferenceId = TransactionResponse.ReferenceId,
        //                    //    ReferenceKey = TransactionResponse.ReferenceKey,
        //                    //    Amount = _DealDetails.Amount,
        //                    //};

        //                    _MDDealCode = new MDDealCode();
        //                    _MDDealCode.Guid = HCoreHelper.GenerateGuid();
        //                    _MDDealCode.DealId = _DealDetails.ReferenceId;
        //                    _MDDealCode.AccountId = _Request.UserReference.AccountId;
        //                    _MDDealCode.ItemCode = "12" + HCoreHelper.GenerateRandomNumber(9);
        //                    _MDDealCode.ItemPin = HCoreHelper.GenerateRandomNumber(4);
        //                    _MDDealCode.ItemAmount = _DealDetails.Amount;
        //                    _MDDealCode.AvailableAmount = _DealDetails.Amount;
        //                    if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.Hour)
        //                    {
        //                        _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
        //                        _MDDealCode.EndDate = _MDDealCode.StartDate.Value.AddHours((double)_DealDetails.CodeValidityDays);
        //                    }
        //                    else
        //                    {
        //                        _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
        //                        _MDDealCode.EndDate = _DealDetails.CodeValidityEndDate;
        //                    }
        //                    _MDDealCode.UseCount = 0;
        //                    _MDDealCode.UseAttempts = 0;
        //                    _MDDealCode.CreateDate = HCoreHelper.GetGMTDateTime();
        //                    _MDDealCode.CreatedById = _Request.UserReference.AccountId;
        //                    _MDDealCode.StatusId = HelperStatus.DealCodes.Unused;
        //                    _HCoreContext.MDDealCode.Add(_MDDealCode);
        //                    _HCoreContext.SaveChanges();
        //                    _HCoreContext.Dispose();

        //                    var _Response = new
        //                    {
        //                        ReferenceId = _MDDealCode.Id,
        //                        ReferenceKey = _MDDealCode.Guid,
        //                    };
        //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0109", ResponseCode.HCP0109);

        //                    //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0110", ResponseCode.HCP0110);
        //                }
        //                else
        //                {
        //                    #region Send Response
        //                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP0113", ResponseCode.HCP0113);
        //                    #endregion
        //                }




        //            }
        //            else
        //            {
        //                _HCoreContext.Dispose();
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
        //            }


        //            //var _DealCode = _HCoreContext.MDDealCode
        //            //                              .Where(x =>
        //            //                               x.Id == _Request.ReferenceId
        //            //                              && x.Guid == _Request.ReferenceKey
        //            //                              && x.DealId == _Request.DealId
        //            //                              && x.Deal.Guid == _Request.DealKey)
        //            //                              .FirstOrDefault();
        //            //if (_DealCode != null)
        //            //{
        //            //    _DealCode.ItemCode = "12" + HCoreHelper.GenerateRandomNumber(9);
        //            //    _DealCode.ItemPin = HCoreHelper.GenerateRandomNumber(4);
        //            //    _DealCode.UseCount = 0;
        //            //    _DealCode.UseAttempts = 0;
        //            //    _DealCode.StatusId = HelperStatus.DealCodes.Unused;
        //            //    _DealCode.ModifyDate = HCoreHelper.GetGMTDateTime();
        //            //    _DealCode.ModifyById = _Request.UserReference.AccountId;
        //            //    _HCoreContext.SaveChanges();
        //            //    _HCoreContext.Dispose();
        //            //    var _Response = new
        //            //    {
        //            //        ReferenceId = _DealCode.Id,
        //            //        ReferenceKey = _DealCode.Guid,
        //            //    };
        //            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0109", ResponseCode.HCP0109);
        //            //}
        //            //else
        //            //{
        //            //    _HCoreContext.Dispose();
        //            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
        //            //}
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        return HCoreHelper.LogException("BuyDeal_Confirm", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
        //    }
        //    #endregion
        //}

        /// <summary>
        /// Description: Gets the deal code details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealCode(ODealOperation.DealCode.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealCode = _HCoreContext.MDDealCode
                                              .Where(x =>
                                               x.Id == _Request.ReferenceId
                                              && x.Guid == _Request.ReferenceKey)
                                              .Select(x => new ODealOperation.DealCode.Response
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  DealReferenceId = x.DealId,
                                                  DealReferenceKey = x.Deal.Guid,
                                                  ItemCode = x.ItemCode,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,
                                                  AccountIconUrl = x.Account.IconStorage.Path,

                                                  Quantity = x.ItemCount,

                                                  StartDate = x.StartDate,
                                                  EndDate = x.EndDate,

                                                  Amount = x.ItemAmount,
                                                  Charge = x.Charge,
                                                  CommissionAmount = x.Charge,
                                                  DeliveryCharge = x.DeliveryCharge,
                                                  TotalAmount = x.TotalAmount,

                                                  Title = x.Deal.TitleContent,
                                                  Description = x.Deal.Description,
                                                  UsageInformation = x.Deal.UsageInformation,
                                                  Terms = x.Deal.Terms,
                                                  ImageUrl = x.Deal.PosterStorage.Path,

                                                  MerchantReferenceId = x.Deal.AccountId,
                                                  MerchantReferenceKey = x.Deal.Account.Guid,
                                                  MerchantDisplayName = x.Deal.Account.DisplayName,
                                                  MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                                  MerchantContactNumber = x.Deal.Account.ContactNumber,
                                                  MerchantEmail = x.Deal.Account.EmailAddress,

                                                  UseDate = x.LastUseDate,
                                                  UseLocationId = x.LastUseLocationId,
                                                  UseLocationKey = x.LastUseLocation.Guid,
                                                  UseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                  UseLocationAddress = x.LastUseLocation.Address,

                                                  SharedCustomerId = x.SecondaryAccount.Id,
                                                  SharedCustomerKey = x.SecondaryAccount.Guid,
                                                  SharedCustomerDisplayName = x.SecondaryAccount.DisplayName,
                                                  SharedCustomerMobileNumber = x.SecondaryAccount.MobileNumber,
                                                  SharedCustomerIconUrl = x.SecondaryAccount.IconStorage.Path,

                                                  SecondaryCode = x.SecondaryCode,
                                                  SecondaryCodeMessage = x.SecondaryMessage,


                                                  CreateDate = x.CreateDate,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,

                                                  DealTypeId = x.Deal.DealTypeId,

                                                  DealTypeCode = x.Deal.DealType.SystemName,
                                                  DeliveryTypeCode = x.Deal.DeliveryType.SystemName,

                                                  FromAddressId = x.FromAddressId,
                                                  ToAddressId = x.ToAddressId,
                                                  ShipmentId = x.OrderId,

                                                  TransactionId = x.TransactionId,
                                              })
                                              .FirstOrDefault();
                    if (_DealCode != null)
                    {
                        if (!string.IsNullOrEmpty(_DealCode.SharedCustomerIconUrl))
                        {
                            _DealCode.SharedCustomerIconUrl = _AppConfig.StorageUrl + _DealCode.SharedCustomerIconUrl;
                        }
                        else
                        {
                            _DealCode.SharedCustomerIconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(_DealCode.AccountIconUrl))
                        {
                            _DealCode.AccountIconUrl = _AppConfig.StorageUrl + _DealCode.AccountIconUrl;
                        }
                        else
                        {
                            _DealCode.AccountIconUrl = _AppConfig.Default_Icon;
                        }

                        _DealCode.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == _DealCode.ReferenceId)
                            .Select(x => new ODealOperation.DealCodeLocation
                            {
                                Address = "M: " + x.Location.ContactNumber + " | Address : " + x.Location.Address,
                                DisplayName = x.Location.DisplayName,
                                Latitude = x.Location.Latitude,
                                Longitude = x.Location.Longitude,
                            }).ToList();
                        if (_DealCode.Locations.Count == 0)
                        {
                            _DealCode.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _DealCode.MerchantReferenceId
                            && x.StatusId == HelperStatus.Default.Active
                            && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore)
                           .Select(x => new ODealOperation.DealCodeLocation
                           {
                               Address = "M: " + x.ContactNumber + " | Address : " + x.Address,
                               DisplayName = x.DisplayName,
                               ContactNumber = x.ContactNumber,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                           }).ToList();
                        }

                        if (!string.IsNullOrEmpty(_DealCode.ImageUrl))
                        {
                            _DealCode.ImageUrl = _AppConfig.StorageUrl + _DealCode.ImageUrl;
                        }
                        else
                        {
                            _DealCode.ImageUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_DealCode.MerchantIconUrl))
                        {
                            _DealCode.MerchantIconUrl = _AppConfig.StorageUrl + _DealCode.MerchantIconUrl;
                        }
                        else
                        {
                            _DealCode.MerchantIconUrl = _AppConfig.Default_Icon;
                        }

                        string StoreContent = " <div><br></div> <div> <b> Deal can redeemed at below store locations :  </b> <div>  <div><br></div>";
                        foreach (var Location in _DealCode.Locations)
                        {
                            StoreContent = StoreContent + "<div style='font-weight : 600 !important ; color : #b11a83 !important; font-size : 14px  !important;'> <b>" + Location.DisplayName + "</b></div> <div style='font-size : 11px  !important; padding-bottom : 8px; '>m: " + Location.ContactNumber + "</div> <div style='font-size : 11px  !important; padding-bottom : 8px; '>" + Location.Address + "</div>\n<br>  ";
                        }
                        if (!string.IsNullOrEmpty(_DealCode.Terms))
                        {
                            _DealCode.Terms = _DealCode.Terms + StoreContent;
                        }
                        else
                        {
                            _DealCode.Terms = _DealCode.Terms;
                        }
                        ODealOperation.DealCode.Delivery _DeliverDetails;
                        ODealOperation.DealCode.Carrier _Carrier;
                        _DeliverDetails = new ODealOperation.DealCode.Delivery();
                        if (_DealCode.DealTypeId == HCoreConstant.Helpers.DealType.ProductDeal)
                        {
                            var ShipmentDetails = _HCoreContext.LSShipments
                                .Where(x => x.Id == _DealCode.ShipmentId)
                                .Select(x => new
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    ShipmentId = x.ShipmentId,
                                    OrderReference = x.OrderReference,
                                    TrackingUrl = x.TrackingUrl,
                                    TrackingNumber = x.TrackingNumber,

                                    CarrierName = x.Carrier.Name,
                                    CarrierEmailAddress = x.Carrier.EmailAddress,
                                    CarrierContactNumber = x.Carrier.MobileNumber,
                                    CarrierIconUrl = x.Carrier.IconUrl,
                                    DeliveryDate = x.DeliveryDate,
                                    StatusId = x.StatusId,
                                    StatusCode = x.Status.SystemName,
                                    StatusName = x.Status.Name,
                                })
                                .FirstOrDefault();
                            if (ShipmentDetails != null)
                            {
                                _DeliverDetails = new ODealOperation.DealCode.Delivery();
                                _DeliverDetails.ShipmentId = ShipmentDetails.ShipmentId;
                                _DeliverDetails.OrderReference = ShipmentDetails.OrderReference;
                                _DeliverDetails.TrackingUrl = ShipmentDetails.TrackingUrl;
                                _DeliverDetails.TrackingNumber = ShipmentDetails.TrackingNumber;

                                _DeliverDetails.StatusId = ShipmentDetails.StatusId;
                                _DeliverDetails.StatusCode = ShipmentDetails.StatusCode;
                                _DeliverDetails.StatusName = ShipmentDetails.StatusName;

                                _DeliverDetails.DeliveryDate = ShipmentDetails.DeliveryDate;

                                _Carrier = new ODealOperation.DealCode.Carrier();
                                _Carrier.Name = ShipmentDetails.CarrierName;
                                _Carrier.EmailAddress = ShipmentDetails.CarrierEmailAddress;
                                _Carrier.ContactNumber = ShipmentDetails.CarrierContactNumber;
                                _Carrier.IconUrl = ShipmentDetails.CarrierIconUrl;
                                _DeliverDetails.Carrier = _Carrier;
                            }
                            if (_DealCode.FromAddressId != null && _DealCode.FromAddressId > 0)
                            {
                                _DeliverDetails.FromAddress = _HCoreContext.MDDealAddress
                                    .Where(x => x.AddressId == _DealCode.FromAddressId)
                                    .Select(x => new ODealOperation.DealCode.Address
                                    {
                                        Name = x.Address.Name,
                                        ContactNumber = x.Address.ContactNumber,
                                        EmailAddress = x.Address.EmailAddress,
                                        AddressLine1 = x.Address.AddressLine1,
                                        AddressLine2 = x.Address.AddressLine2,
                                        CityName = x.Address.City.Name,
                                        StateName = x.Address.State.Name,
                                        CountryName = x.Address.Country.Name
                                    }).FirstOrDefault();
                            }
                            if (_DealCode.ToAddressId != null && _DealCode.ToAddressId > 0)
                            {
                                _DeliverDetails.ToAddress = _HCoreContext.MDDealAddress
                                  .Where(x => x.AddressId == _DealCode.ToAddressId)
                                  .Select(x => new ODealOperation.DealCode.Address
                                  {
                                      Name = x.Address.Name,
                                      ContactNumber = x.Address.ContactNumber,
                                      EmailAddress = x.Address.EmailAddress,
                                      AddressLine1 = x.Address.AddressLine1,
                                      AddressLine2 = x.Address.AddressLine2,
                                      CityName = x.Address.City.Name,
                                      StateName = x.Address.State.Name,
                                      CountryName = x.Address.Country.Name
                                  }).FirstOrDefault();
                            }
                        }
                        ODealOperation.DealCode.PaymentDetails? _PaymenDetails = new ODealOperation.DealCode.PaymentDetails();
                        var PaymentType = _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _DealCode.TransactionId)
                                          .Select(x => new
                                          {
                                              PaymentTypeId = x.TypeId,
                                              PaymentTypeCode = x.Type.SystemName,
                                              PaymentTypeName = x.Type.Name,
                                          }).FirstOrDefault();
                        if (PaymentType != null)
                        {
                            _PaymenDetails.PaymentTypeId = PaymentType.PaymentTypeId;
                            _PaymenDetails.PaymentTypeCode = PaymentType.PaymentTypeCode;
                            _PaymenDetails.PaymentTypeName = PaymentType.PaymentTypeName;
                        }
                        var CardDetails = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == _DealCode.AccountId && x.TypeId == 465)
                                          .Select(x => new
                                          {
                                              CardNumber = x.SystemName + x.Value,
                                          }).FirstOrDefault();
                        if (CardDetails != null)
                        {
                            _PaymenDetails.CardNumber = CardDetails.CardNumber;
                        }

                        _PaymenDetails.ItemAmount = _DealCode.Amount;
                        _PaymenDetails.ItemCount = _DealCode.Quantity;
                        _PaymenDetails.DeliveryCharges = _DealCode.DeliveryCharge;
                        _PaymenDetails.TotalAmount = (_DealCode.Amount * _DealCode.Quantity) + _DealCode.DeliveryCharge;

                        _DealCode.PaymentDetails = _PaymenDetails;

                        _DealCode.DeliveryDetails = _DeliverDetails;

                        _DealCode.RedeemInstruction = "- You can redeem deal code through Pos machine or ask merchant to scan QR code. \n - POS Redeem : On pos select ThankUCash Redeem Option. Enter deal code and your pin. Pos will validate your code and print receipt for redeem. \n - Other Option  : Ask merchant to scan qr code to redeem. /n - Merchant will scan code and redeem your coupon to avail deal. Please call on " + _DealCode.MerchantContactNumber + " for any assitance";
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCode, "HCP0109", ResponseCode.HCP0109);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealCode", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealCode(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealCodesList = new List<ODealOperation.DealCode.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDealCode
                                                .Where(x =>
                                                (x.AccountId == _Request.UserReference.AccountId || x.SecondaryAccountId == _Request.UserReference.AccountId)
                                                && x.Deal.DealTypeId == Helpers.DealType.ServiceDeal
                                                && x.StatusId != HelperStatus.Default.Inactive)
                                                .Select(x => new ODealOperation.DealCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Title = x.Deal.TitleContent,

                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    CategoryName = x.Deal.Category.Name,
                                                    EndDate = x.EndDate,
                                                    ItemCode = x.ItemCode,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,

                                                    Amount = x.Deal.Amount,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    DealTypeId = x.Deal.DealTypeId,


                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DeliveryTypeCode = x.Deal.DeliveryType.SystemName,

                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    _DealCodesList = _HCoreContext.MDDealCode
                                                 .Where(x =>
                                                (x.AccountId == _Request.UserReference.AccountId || x.SecondaryAccountId == _Request.UserReference.AccountId)
                                                 && x.Deal.DealTypeId == Helpers.DealType.ServiceDeal
                                                && x.StatusId != HelperStatus.Default.Inactive)
                                                .Select(x => new ODealOperation.DealCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Title = x.Deal.TitleContent,

                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    CategoryName = x.Deal.Category.Name,
                                                    EndDate = x.EndDate,

                                                    ItemCode = x.ItemCode,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    ActualPrice = x.Deal.ActualPrice,
                                                    SellingPrice = x.Deal.SellingPrice,

                                                    Amount = x.Deal.Amount,
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    DealTypeId = x.Deal.DealTypeId,


                                                    DealTypeCode = x.Deal.DealType.SystemName,
                                                    DeliveryTypeCode = x.Deal.DeliveryType.SystemName,

                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _DealCodesList)
                    {
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                        else
                        {
                            DataItem.ImageUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealCodesList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealCode", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductDeal(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealCodesList = new List<ODealOperation.DealCode.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDealCode
                                                .Where(x =>
                                               (x.AccountId == _Request.UserReference.AccountId || x.AccountId == _Request.UserReference.AccountId)
                                                )
                                                .Select(x => new ODealOperation.DealCode.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    ShipmentId = x.Order.Id,
                                                    ShipmentKey = x.Order.Guid,

                                                    Title = x.Deal.TitleContent,

                                                    MerchantId = x.Deal.AccountId,
                                                    MerchantDisplayName = x.Deal.Account.DisplayName,
                                                    MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                                    DeliveryDate = x.Order.DeliveryDate,

                                                    ImageUrl = x.Deal.PosterStorage.Path,

                                                    TotalAmount = x.TotalAmount,
                                                    CreateDate = x.CreateDate,
                                                    StatusId = x.Order.StatusId,
                                                    StatusCode = x.Order.Status.SystemName,
                                                    StatusName = x.Order.Status.Name,

                                                    ItemCode = x.ItemCode,
                                                    DealStatusId = x.Deal.StatusId,
                                                    DealStatusCode = x.Deal.Status.SystemName,
                                                    DealStatusName = x.Deal.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                               .GroupBy(m => m.Title).Select(x => x.First())
                                       .Count();
                        #endregion
                    }
                    #region Get Data

                    _DealCodesList = _HCoreContext.MDDealCode
                                        .Where(x =>
                                       (x.AccountId == _Request.UserReference.AccountId || x.AccountId == _Request.UserReference.AccountId)
                                        )
                                        .Select(x => new ODealOperation.DealCode.List
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            Count = 0,
                                            ShipmentId = x.Order.Id,
                                            ShipmentKey = x.Order.Guid,
                                            DealId = x.DealId,
                                            Title = x.Deal.TitleContent,

                                            MerchantId = x.Deal.AccountId,
                                            MerchantDisplayName = x.Deal.Account.DisplayName,
                                            MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                                            DeliveryDate = x.Order.DeliveryDate,

                                            ImageUrl = x.Deal.PosterStorage.Path,

                                            TotalAmount = x.TotalAmount,
                                            CreateDate = x.CreateDate,
                                            StatusId = x.Order.StatusId,
                                            StatusCode = x.Order.Status.SystemName,
                                            StatusName = x.Order.Status.Name,

                                            ItemCode = x.ItemCode,
                                            DealStatusId = x.Deal.StatusId,
                                            DealStatusCode = x.Deal.Status.SystemName,
                                            DealStatusName = x.Deal.Status.Name,
                                        })
                                     .Where(_Request.SearchCondition)
                                     .OrderBy(_Request.SortExpression)
                                     .GroupBy(m => m.Title).Select(x => x.First())
                                     .Skip(_Request.Offset)
                                     .Take(_Request.Limit)
                                     .ToList();

                    #endregion
                    foreach (var DataItem in _DealCodesList)
                    {
                        if (DataItem.StatusId == HelperStatus.OrderStatus.New)
                        {
                            DataItem.StatusContent = "Order placed waiting for confirmation";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.PendingConfirmation)
                        {
                            DataItem.StatusContent = "Order placed waiting for confirmation";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.Confirmed)
                        {
                            DataItem.StatusContent = "Order confirmed. Preparing for delivery";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.Preparing)
                        {
                            DataItem.StatusContent = "Preparing for delivery";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.Ready)
                        {
                            DataItem.StatusContent = "Item ready for pickup";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.ReadyToPickup)
                        {
                            DataItem.StatusContent = "Item ready for pickup";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.OutForDelivery)
                        {
                            DataItem.StatusContent = "Your package out for delivery";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.DeliveryFailed)
                        {
                            DataItem.StatusContent = "Delivery failed";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.CancelledByUser)
                        {
                            DataItem.StatusContent = "Order cancelled by user";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.CancelledBySeller)
                        {
                            DataItem.StatusContent = "Order cancelled by seller";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.CancelledBySystem)
                        {
                            DataItem.StatusContent = "Order cancelled by system";
                        }
                        if (DataItem.StatusId == HelperStatus.OrderStatus.Delivered)
                        {
                            DataItem.StatusContent = "Order delivered";
                        }


                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                        else
                        {
                            DataItem.ImageUrl = _AppConfig.Default_Poster;
                        }

                        //Get Count
                        DataItem.Count = _HCoreContext.MDDealCode
                                                .Where(x =>
                                               (x.DealId == DataItem.DealId && x.AccountId == _Request.UserReference.AccountId)
                                                ).Count();
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealCodesList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetProductDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Shares the deal code.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ShareDealCode(ODealOperation.DealCode.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0146", ResponseCode.HCP0146);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealCode = _HCoreContext.MDDealCode
                                                  .Where(x =>
                                                   x.Id == _Request.ReferenceId
                                                  && x.Guid == _Request.ReferenceKey)
                                                  .FirstOrDefault();
                    if (_DealCode != null)
                    {
                        if (_DealCode.AccountId == _Request.UserReference.AccountId)
                        {
                            if (_DealCode.StatusId == HelperStatus.DealCodes.Unused)
                            {

                                string FormattedMobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber);
                                long AccountId = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Appuser && x.CountryId == _Request.UserReference.SystemCountry && x.User.Username == FormattedMobileNumber).Select(x => x.Id).FirstOrDefault();
                                if (AccountId > 0)
                                {
                                    if (AccountId == _Request.UserReference.AccountId)
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0151", ResponseCode.HCP0151);
                                    }
                                    _DealCode.SecondaryAccountId = AccountId;
                                    _DealCode.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    _HCoreContext.SaveChanges();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0149", ResponseCode.HCP0149);
                                }
                                else
                                {
                                    ManageCoreUserAccess _ManageCoreUserAccess;
                                    OAppProfile.Request _AppProfileRequest;
                                    _AppProfileRequest = new OAppProfile.Request();
                                    _AppProfileRequest.OwnerId = _Request.UserReference.AccountId;
                                    _AppProfileRequest.CreatedById = _Request.UserReference.AccountId;
                                    _AppProfileRequest.MobileNumber = _Request.MobileNumber;
                                    //_AppProfileRequest.EmailAddress = _Request.EmailAddress;
                                    _AppProfileRequest.DisplayName = _Request.MobileNumber;
                                    //if (!string.IsNullOrEmpty(_Request.Gender))
                                    //{
                                    //    _Request.Gender = _Request.Gender.ToLower();
                                    //    if (_Request.Gender == "male")
                                    //    {
                                    //        _AppProfileRequest.GenderCode = "gender.male";
                                    //    }
                                    //    else if (_Request.Gender == "female")
                                    //    {
                                    //        _AppProfileRequest.GenderCode = "gender.female";
                                    //    }
                                    //}
                                    //_AppProfileRequest.CardNumber = _Request.CardNumber;
                                    //_AppProfileRequest.CardSerialNumber = _Request.TagNumber;
                                    _AppProfileRequest.UserReference = _Request.UserReference;
                                    _ManageCoreUserAccess = new ManageCoreUserAccess();
                                    OAppProfile.Response _AppUserCreateResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppProfileRequest);
                                    if (_AppUserCreateResponse.Status == ResponseStatus.Success)
                                    {

                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var DealCode = _HCoreContext.MDDealCode
                                                 .Where(x =>
                                                  x.Id == _Request.ReferenceId
                                                 && x.Guid == _Request.ReferenceKey)
                                                 .FirstOrDefault();
                                            DealCode.SecondaryAccountId = _AppUserCreateResponse.AccountId;
                                            DealCode.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            _HCoreContext.SaveChanges();
                                        }

                                        //_GatewayResponse.Name = _Request.Name;
                                        //_GatewayResponse.FirstName = _Request.FirstName;
                                        //_GatewayResponse.LastName = _Request.LastName;
                                        //_GatewayResponse.Name = _Request.Name;
                                        //_GatewayResponse.Gender = _Request.Gender;
                                        //_GatewayResponse.EmailAddress = _Request.EmailAddress;
                                        //_GatewayResponse.MobileNumber = _Request.MobileNumber;
                                        //_GatewayResponse.DateOfBirth = _Request.DateOfBirth;
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0149", ResponseCode.HCP0149);
                                    }
                                    else
                                    {
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0150", ResponseCode.HCP0150);
                                    }

                                }

                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0147", ResponseCode.HCP0147);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0148", ResponseCode.HCP0148);
                        }
                        //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DealCode, "HCP0109", ResponseCode.HCP0109);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDealCode", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deal redeem initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DealRedeem_Initialize(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0121", ResponseCode.HCP0121);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _Request.ReferenceKey = _Request.ReferenceKey.Trim();
                    //HCoreHelper.LogData(HCoreConstant.LogType.Log, "DEALCODEINI", _Request.ReferenceKey, _Request.AccountId.ToString(), _Request.UserReference);
                    var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.ItemCode == _Request.ReferenceKey)
                        .Select(x => new ODealOperation.DealCode.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            DealReferenceId = x.DealId,
                            DealReferenceKey = x.Deal.Guid,
                            ItemCode = x.ItemCode,

                            StartDate = x.StartDate,
                            EndDate = x.EndDate,

                            ActualPrice = x.Deal.ActualPrice,
                            SellingPrice = x.Deal.SellingPrice,
                            CommissionAmount = x.Deal.CommissionAmount,
                            Amount = x.ItemAmount,
                            TotalAmount = x.Deal.TotalAmount,

                            DealStartDate = x.Deal.StartDate,
                            DealEndDate = x.Deal.EndDate,

                            Title = x.Deal.TitleContent,
                            Description = x.Deal.Description,
                            UsageInformation = x.Deal.UsageInformation,
                            Terms = x.Deal.Terms,
                            ImageUrl = x.Deal.PosterStorage.Path,

                            MerchantReferenceId = x.Deal.AccountId,
                            MerchantReferenceKey = x.Deal.Account.Guid,
                            MerchantDisplayName = x.Deal.Account.DisplayName,
                            MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                            UseDate = x.LastUseDate,
                            UseLocationId = x.LastUseLocationId,
                            UseLocationKey = x.LastUseLocation.Guid,
                            UseLocationDisplayName = x.LastUseLocation.DisplayName,
                            UseLocationAddress = x.LastUseLocation.Address,

                            CustomerId = x.Account.Id,
                            CustomerKey = x.Account.Guid,
                            CustomerDisplayName = x.Account.DisplayName,
                            CustomerMobileNumber = x.Account.MobileNumber,
                            CustomerIconUrl = x.Account.IconStorage.Path,

                            CreateDate = x.CreateDate,
                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();
                    if (DealCodeDetails != null)
                    {
                        //long AccountId = _Request.AccountId; 
                        //if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                        //{
                        //    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Owner.Owner.AccountTypeId == UserAccountType.Merchant
                        //    && x.Owner.Id == _Request.AccountId
                        //    && x.Owner.Guid == _Request.AccountKey)
                        //        .Select(x => new
                        //        {
                        //            MerchantId = x.Owner.Owner.Id,
                        //        }).FirstOrDefault();
                        //    if (MerchantDetails != null)
                        //    {
                        //        AccountId = MerchantDetails.MerchantId;
                        //    }
                        //}
                        //if (DealCodeDetails.MerchantReferenceId == AccountId)
                        //{
                        //if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Unused)
                        //{
                        DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                        //if (CurrentTime < DealCodeDetails.StartDate)
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0127", ResponseCode.HCP0127);
                        //}
                        //else if (CurrentTime > DealCodeDetails.EndDate)
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0128", ResponseCode.HCP0128);
                        //}
                        //else if (CurrentTime > DealCodeDetails.StartDate && CurrentTime < DealCodeDetails.EndDate)
                        //{
                        DealCodeDetails.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == DealCodeDetails.ReferenceId)
                                   .Select(x => new ODealOperation.DealCodeLocation
                                   {
                                       Address = x.Location.Address,
                                       DisplayName = x.Location.DisplayName,
                                       Latitude = x.Location.Latitude,
                                       Longitude = x.Location.Longitude,
                                   }).ToList();
                        if (DealCodeDetails.Locations.Count == 0)
                        {
                            DealCodeDetails.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == DealCodeDetails.MerchantReferenceId
                            && x.StatusId == HelperStatus.Default.Active
                            && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore)
                           .Select(x => new ODealOperation.DealCodeLocation
                           {
                               Address = x.Address,
                               DisplayName = x.DisplayName,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                           }).ToList();
                        }
                        if (!string.IsNullOrEmpty(DealCodeDetails.ImageUrl))
                        {
                            DealCodeDetails.ImageUrl = _AppConfig.StorageUrl + DealCodeDetails.ImageUrl;
                        }
                        else
                        {
                            DealCodeDetails.ImageUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DealCodeDetails.MerchantIconUrl))
                        {
                            DealCodeDetails.MerchantIconUrl = _AppConfig.StorageUrl + DealCodeDetails.MerchantIconUrl;
                        }
                        else
                        {
                            DealCodeDetails.MerchantIconUrl = _AppConfig.Default_Icon;
                        }


                        if (!string.IsNullOrEmpty(DealCodeDetails.CustomerIconUrl))
                        {
                            DealCodeDetails.CustomerIconUrl = _AppConfig.StorageUrl + DealCodeDetails.CustomerIconUrl;
                        }
                        else
                        {
                            DealCodeDetails.CustomerIconUrl = _AppConfig.Default_Icon;
                        }
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, DealCodeDetails, "HCP0130", ResponseCode.HCP0130);
                        //}
                        //else
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0129", ResponseCode.HCP0129);
                        //}
                        //}
                        //else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Used)
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0123", ResponseCode.HCP0123);
                        //}
                        //else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Expired)
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0124", ResponseCode.HCP0124);
                        //}
                        //else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Blocked)
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0125", ResponseCode.HCP0125);
                        //}
                        //else
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0126", ResponseCode.HCP0126);
                        //}
                        //}
                        //else
                        //{
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0136", ResponseCode.HCP0136);
                        //}
                    }
                    else
                    {
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0122", ResponseCode.HCP0122);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DealRedeem_Initialize", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deal redeem confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DealRedeem_Confirm(OReference _Request)
        {
            #region Manage Exception
            try
            {
                _ManageCoreTransaction = new ManageCoreTransaction();
                _CoreTransactionRequest = new OCoreTransaction.Request();
                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                OCoreTransaction.Response? TransactionResponse = new OCoreTransaction.Response();
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0121", ResponseCode.HCP0121);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                    {
                        var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Owner.Owner.AccountTypeId == UserAccountType.Merchant
                        && x.Owner.Id == _Request.AccountId
                        && x.Owner.Guid == _Request.AccountKey)
                            .Select(x => new
                            {
                                MerchantId = x.Owner.Owner.Id,
                                MerchantKey = x.Owner.Owner.Guid,
                            }).FirstOrDefault();
                        if (MerchantDetails != null)
                        {
                            _Request.AccountId = MerchantDetails.MerchantId;
                            _Request.AccountKey = MerchantDetails.MerchantKey;
                        }
                    }
                    _Request.ReferenceKey = _Request.ReferenceKey.Trim();
                    //HCoreHelper.LogData(HCoreConstant.LogType.Log, "DEALCODECON", _Request.ReferenceKey, _Request.AccountId.ToString(), _Request.UserReference);
                    var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.ItemCode == _Request.ReferenceKey
                     && x.Deal.AccountId == _Request.AccountId
                     && x.Deal.Account.Guid == _Request.AccountKey)
                                                  .Select(x => new ODealOperation.DealCode.Response
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      DealReferenceId = x.DealId,
                                                      DealReferenceKey = x.Deal.Guid,
                                                      ItemCode = x.ItemCode,


                                                      StartDate = x.StartDate,
                                                      EndDate = x.EndDate,

                                                      ActualPrice = x.Deal.ActualPrice,
                                                      SellingPrice = x.Deal.SellingPrice,
                                                      CommissionAmount = x.Deal.CommissionAmount,
                                                      Amount = x.ItemAmount,
                                                      TotalAmount = x.Deal.TotalAmount,

                                                      DealStartDate = x.Deal.StartDate,
                                                      DealEndDate = x.Deal.EndDate,

                                                      Title = x.Deal.TitleContent,
                                                      Description = x.Deal.Description,
                                                      UsageInformation = x.Deal.UsageInformation,
                                                      Terms = x.Deal.Terms,
                                                      ImageUrl = x.Deal.PosterStorage.Path,

                                                      MerchantReferenceId = x.Deal.AccountId,
                                                      MerchantReferenceKey = x.Deal.Account.Guid,
                                                      MerchantDisplayName = x.Deal.Account.DisplayName,
                                                      MerchantIconUrl = x.Deal.Account.IconStorage.Path,
                                                      MerchantPaybleAmount = (x.Deal.SellingPrice - x.Deal.CommissionAmount) * x.ItemCount,

                                                      UseDate = x.LastUseDate,
                                                      UseLocationId = x.LastUseLocationId,
                                                      UseLocationKey = x.LastUseLocation.Guid,
                                                      UseLocationDisplayName = x.LastUseLocation.DisplayName,
                                                      UseLocationAddress = x.LastUseLocation.Address,

                                                      CustomerId = x.Account.Id,
                                                      CustomerKey = x.Account.Guid,
                                                      CustomerDisplayName = x.Account.DisplayName,
                                                      CustomerMobileNumber = x.Account.MobileNumber,
                                                      CustomerIconUrl = x.Account.IconStorage.Path,

                                                      CreateDate = x.CreateDate,
                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name,
                                                      ItemCount = x.ItemCount,
                                                  })
                                                  .FirstOrDefault();
                    if (DealCodeDetails != null)
                    {
                        if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Unused)
                        {
                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                            if (CurrentTime < DealCodeDetails.StartDate)
                            {
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0127", ResponseCode.HCP0127);
                            }
                            else if (CurrentTime > DealCodeDetails.EndDate)
                            {
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0128", ResponseCode.HCP0128);
                            }
                            else if (CurrentTime > DealCodeDetails.StartDate && CurrentTime < DealCodeDetails.EndDate)
                            {
                                var DealInfo = _HCoreContext.MDDealCode.Where(x => x.Id == DealCodeDetails.ReferenceId).FirstOrDefault();

                                if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                                {
                                    var CashierStoreInfo = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.OwnerId).FirstOrDefault();
                                    if (CashierStoreInfo > 0)
                                    {
                                        DealInfo.LastUseLocationId = CashierStoreInfo;
                                    }
                                    DealInfo.CashierId = _Request.UserReference.AccountId;
                                }
                                DealInfo.LastUseSource = Helpers.DealRedeemSource.App;
                                DealInfo.UseAttempts = 1;
                                DealInfo.UseCount = 1;
                                DealInfo.LastUseDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.ReferenceId != 0)
                                {
                                    DealInfo.LastUseLocationId = _Request.ReferenceId;
                                }
                                else
                                {
                                    long? StoreId = _HCoreContext.HCUAccount.Where(x => x.OwnerId == DealCodeDetails.MerchantReferenceId && x.AccountTypeId == UserAccountType.MerchantStore).Select(x => x.Id).FirstOrDefault();
                                    if (StoreId > 0)
                                    {
                                        DealInfo.LastUseLocationId = StoreId;
                                    }
                                }
                                DealInfo.StatusId = HelperStatus.DealCodes.Used;
                                DealInfo.ModifyById = _Request.UserReference.AccountId;
                                _HCoreContext.SaveChanges();

                                double? PaybleAmount = 0.0;
                                if (DealCodeDetails.ItemCount > 0)
                                {
                                    PaybleAmount = DealCodeDetails.MerchantPaybleAmount * DealCodeDetails.ItemCount;
                                }
                                else
                                {
                                    PaybleAmount = DealCodeDetails.MerchantPaybleAmount * 1;
                                }

                                string? GroupKey = "TPP" + DealCodeDetails.AccountId + "O" + HCoreHelper.GenerateDateString();

                                _ManageCoreTransaction = new ManageCoreTransaction();
                                _CoreTransactionRequest = new OCoreTransaction.Request();

                                _CoreTransactionRequest.CustomerId = (long)DealCodeDetails.CustomerId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = GroupKey;
                                _CoreTransactionRequest.ParentId = (long)DealCodeDetails.MerchantReferenceId;
                                _CoreTransactionRequest.InvoiceAmount = (double)PaybleAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                                _CoreTransactionRequest.ReferenceNumber = DealInfo.Guid;
                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;

                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();

                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = (long)DealCodeDetails.MerchantReferenceId,
                                    SourceId = TransactionSource.Deals,
                                    ModeId = TransactionMode.Debit,
                                    TypeId = TransactionType.Deal.DealPurchase,
                                    Amount = _CoreTransactionRequest.InvoiceAmount,
                                    Charge = 0,
                                    TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                                });
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = (long)DealCodeDetails.MerchantReferenceId,
                                    SourceId = TransactionSource.Merchant,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.Deal.DealPurchase,
                                    Amount = _CoreTransactionRequest.InvoiceAmount,
                                    Charge = 0,
                                    TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                                });

                                _CoreTransactionRequest.Transactions = _TransactionItems;

                                TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);

                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        string? UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == DealInfo.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(UserNotificationUrl))
                                        {
                                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Redeemed", DealCodeDetails.Title + " deal redeemed at " + DealCodeDetails.MerchantDisplayName, "dealpurchasedetails", DealInfo.Id, DealInfo.Guid, "View details", true, null);
                                        }
                                    }

                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, DealCodeDetails, "HCP0131", ResponseCode.HCP0131);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP00003", TransactionResponse.Message);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0129", ResponseCode.HCP0129);
                            }
                        }
                        else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Used)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0123", ResponseCode.HCP0123);
                        }
                        else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Expired)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0124", ResponseCode.HCP0124);
                        }
                        else if (DealCodeDetails.StatusId == HelperStatus.DealCodes.Blocked)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0125", ResponseCode.HCP0125);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, DealCodeDetails, "HCP0126", ResponseCode.HCP0126);
                        }
                    }
                    else
                    {
                        HCoreHelper.LogData(HCoreConstant.LogType.Log, "DEALCODECONINV", _Request.ReferenceKey + "|" + _Request.AccountId + "|" + _Request.AccountKey, _Request.AccountId.ToString(), _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0122", ResponseCode.HCP0122);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DealRedeem_Confirm", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant redeem history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantRedeemHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _DealCodesList = new List<ODealOperation.DealCode.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDealCode
                                                    .Where(x => x.ModifyById == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.DealCodes.Used)
                                                    .Select(x => new ODealOperation.DealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.Deal.TitleContent,

                                                        CategoryName = x.Deal.Category.Name,
                                                        EndDate = x.EndDate,
                                                        ItemCode = x.ItemCode,
                                                        UseDate = x.LastUseDate,
                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        ActualPrice = x.Deal.ActualPrice,
                                                        SellingPrice = x.Deal.SellingPrice,

                                                        Amount = x.Deal.Amount,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,


                                                        CustomerId = x.AccountId,
                                                        CustomerKey = x.Account.Guid,
                                                        CustomerDisplayName = x.Account.DisplayName,
                                                        CustomerMobileNumber = x.Account.MobileNumber,
                                                        CustomerIconUrl = x.Account.IconStorage.Path,

                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealCodesList = _HCoreContext.MDDealCode
                                                    .Where(x => x.ModifyById == _Request.UserReference.AccountId
                                                    && x.StatusId == HelperStatus.DealCodes.Used)
                                                    .Select(x => new ODealOperation.DealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.Deal.TitleContent,

                                                        CategoryName = x.Deal.Category.Name,
                                                        EndDate = x.EndDate,
                                                        ItemCode = x.ItemCode,
                                                        UseDate = x.LastUseDate,
                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        ActualPrice = x.Deal.ActualPrice,
                                                        SellingPrice = x.Deal.SellingPrice,

                                                        Amount = x.Deal.Amount,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        CustomerId = x.AccountId,
                                                        CustomerKey = x.Account.Guid,
                                                        CustomerDisplayName = x.Account.DisplayName,
                                                        CustomerMobileNumber = x.Account.MobileNumber,
                                                        CustomerIconUrl = x.Account.IconStorage.Path,

                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealCodesList)
                        {
                            if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                            {
                                DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                            }
                            else
                            {
                                DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Poster;
                            }


                            if (!string.IsNullOrEmpty(DataItem.CustomerIconUrl))
                            {
                                DataItem.CustomerIconUrl = _AppConfig.StorageUrl + DataItem.CustomerIconUrl;
                            }
                            else
                            {
                                DataItem.CustomerIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealCodesList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDealCode
                                                    .Where(x => x.Deal.AccountId == _Request.AccountId
                                                    && x.Deal.Account.Guid == _Request.AccountKey
                                                    && x.StatusId == HelperStatus.DealCodes.Used)
                                                    .Select(x => new ODealOperation.DealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.Deal.TitleContent,

                                                        CategoryName = x.Deal.Category.Name,
                                                        EndDate = x.EndDate,
                                                        ItemCode = x.ItemCode,
                                                        UseDate = x.LastUseDate,
                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        ActualPrice = x.Deal.ActualPrice,
                                                        SellingPrice = x.Deal.SellingPrice,

                                                        Amount = x.Deal.Amount,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,


                                                        CustomerId = x.AccountId,
                                                        CustomerKey = x.Account.Guid,
                                                        CustomerDisplayName = x.Account.DisplayName,
                                                        CustomerMobileNumber = x.Account.MobileNumber,
                                                        CustomerIconUrl = x.Account.IconStorage.Path,

                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        _DealCodesList = _HCoreContext.MDDealCode
                                                    .Where(x => x.Deal.AccountId == _Request.AccountId
                                                    && x.Deal.Account.Guid == _Request.AccountKey
                                                    && x.StatusId == HelperStatus.DealCodes.Used)
                                                    .Select(x => new ODealOperation.DealCode.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Title = x.Deal.TitleContent,

                                                        CategoryName = x.Deal.Category.Name,
                                                        EndDate = x.EndDate,
                                                        ItemCode = x.ItemCode,
                                                        UseDate = x.LastUseDate,
                                                        ImageUrl = x.Deal.PosterStorage.Path,

                                                        ActualPrice = x.Deal.ActualPrice,
                                                        SellingPrice = x.Deal.SellingPrice,

                                                        Amount = x.Deal.Amount,
                                                        CreateDate = x.CreateDate,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,

                                                        CustomerId = x.AccountId,
                                                        CustomerKey = x.Account.Guid,
                                                        CustomerDisplayName = x.Account.DisplayName,
                                                        CustomerMobileNumber = x.Account.MobileNumber,
                                                        CustomerIconUrl = x.Account.IconStorage.Path,

                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in _DealCodesList)
                        {
                            if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                            {
                                DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                            }
                            else
                            {
                                DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                            {
                                DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                            }
                            else
                            {
                                DataItem.ImageUrl = _AppConfig.Default_Poster;
                            }


                            if (!string.IsNullOrEmpty(DataItem.CustomerIconUrl))
                            {
                                DataItem.CustomerIconUrl = _AppConfig.StorageUrl + DataItem.CustomerIconUrl;
                            }
                            else
                            {
                                DataItem.CustomerIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealCodesList, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);

                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant deal code details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantDealCodeDetails(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0121", ResponseCode.HCP0121);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.UserReference.AccountTypeId == UserAccountType.MerchantCashier)
                    {
                        var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Id == _Request.ReferenceId
                  && x.Guid == _Request.ReferenceKey)
                      .Select(x => new ODealOperation.DealCode.Response
                      {
                          ReferenceId = x.Id,
                          ReferenceKey = x.Guid,

                          DealReferenceId = x.DealId,
                          DealReferenceKey = x.Deal.Guid,
                          ItemCode = x.ItemCode,

                          StartDate = x.StartDate,
                          EndDate = x.EndDate,

                          ActualPrice = x.Deal.ActualPrice,
                          SellingPrice = x.Deal.SellingPrice,
                          CommissionAmount = x.Deal.CommissionAmount,
                          Amount = x.ItemAmount,
                          TotalAmount = x.Deal.TotalAmount,

                          DealStartDate = x.Deal.StartDate,
                          DealEndDate = x.Deal.EndDate,

                          Title = x.Deal.TitleContent,
                          Description = x.Deal.Description,
                          UsageInformation = x.Deal.UsageInformation,
                          Terms = x.Deal.Terms,
                          ImageUrl = x.Deal.PosterStorage.Path,

                          MerchantReferenceId = x.Deal.AccountId,
                          MerchantReferenceKey = x.Deal.Account.Guid,
                          MerchantDisplayName = x.Deal.Account.DisplayName,
                          MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                          UseDate = x.LastUseDate,
                          UseLocationId = x.LastUseLocationId,
                          UseLocationKey = x.LastUseLocation.Guid,
                          UseLocationDisplayName = x.LastUseLocation.DisplayName,
                          UseLocationAddress = x.LastUseLocation.Address,

                          CustomerId = x.Account.Id,
                          CustomerKey = x.Account.Guid,
                          CustomerDisplayName = x.Account.DisplayName,
                          CustomerMobileNumber = x.Account.MobileNumber,
                          CustomerIconUrl = x.Account.IconStorage.Path,

                          CreateDate = x.CreateDate,
                          StatusId = x.StatusId,
                          StatusCode = x.Status.SystemName,
                          StatusName = x.Status.Name,
                      }).FirstOrDefault();
                        if (DealCodeDetails != null)
                        {
                            DealCodeDetails.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == DealCodeDetails.ReferenceId)
                          .Select(x => new ODealOperation.DealCodeLocation
                          {
                              Address = x.Location.Address,
                              DisplayName = x.Location.DisplayName,
                              Latitude = x.Location.Latitude,
                              Longitude = x.Location.Longitude,
                          }).ToList();
                            if (DealCodeDetails.Locations.Count == 0)
                            {
                                DealCodeDetails.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == DealCodeDetails.MerchantReferenceId
                                && x.StatusId == HelperStatus.Default.Active
                                && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore)
                               .Select(x => new ODealOperation.DealCodeLocation
                               {
                                   Address = x.Address,
                                   DisplayName = x.DisplayName,
                                   Latitude = x.Latitude,
                                   Longitude = x.Longitude,
                               }).ToList();
                            }

                            if (!string.IsNullOrEmpty(DealCodeDetails.ImageUrl))
                            {
                                DealCodeDetails.ImageUrl = _AppConfig.StorageUrl + DealCodeDetails.ImageUrl;
                            }
                            else
                            {
                                DealCodeDetails.ImageUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(DealCodeDetails.MerchantIconUrl))
                            {
                                DealCodeDetails.MerchantIconUrl = _AppConfig.StorageUrl + DealCodeDetails.MerchantIconUrl;
                            }
                            else
                            {
                                DealCodeDetails.MerchantIconUrl = _AppConfig.Default_Icon;
                            }


                            if (!string.IsNullOrEmpty(DealCodeDetails.CustomerIconUrl))
                            {
                                DealCodeDetails.CustomerIconUrl = _AppConfig.StorageUrl + DealCodeDetails.CustomerIconUrl;
                            }
                            else
                            {
                                DealCodeDetails.CustomerIconUrl = _AppConfig.Default_Icon;
                            }
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, DealCodeDetails, "HCP0132", ResponseCode.HCP0132);

                        }
                        else
                        {
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0122", ResponseCode.HCP0122);
                        }

                    }
                    else
                    {
                        var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Id == _Request.ReferenceId
                  && x.Guid == _Request.ReferenceKey
                  && x.Deal.AccountId == _Request.AccountId
                  && x.Deal.Account.Guid == _Request.AccountKey)
                      .Select(x => new ODealOperation.DealCode.Response
                      {
                          ReferenceId = x.Id,
                          ReferenceKey = x.Guid,

                          DealReferenceId = x.DealId,
                          DealReferenceKey = x.Deal.Guid,
                          ItemCode = x.ItemCode,

                          StartDate = x.StartDate,
                          EndDate = x.EndDate,

                          ActualPrice = x.Deal.ActualPrice,
                          SellingPrice = x.Deal.SellingPrice,
                          CommissionAmount = x.Deal.CommissionAmount,
                          Amount = x.ItemAmount,
                          TotalAmount = x.Deal.TotalAmount,

                          DealStartDate = x.Deal.StartDate,
                          DealEndDate = x.Deal.EndDate,

                          Title = x.Deal.TitleContent,
                          Description = x.Deal.Description,
                          UsageInformation = x.Deal.UsageInformation,
                          Terms = x.Deal.Terms,
                          ImageUrl = x.Deal.PosterStorage.Path,

                          MerchantReferenceId = x.Deal.AccountId,
                          MerchantReferenceKey = x.Deal.Account.Guid,
                          MerchantDisplayName = x.Deal.Account.DisplayName,
                          MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                          UseDate = x.LastUseDate,
                          UseLocationId = x.LastUseLocationId,
                          UseLocationKey = x.LastUseLocation.Guid,
                          UseLocationDisplayName = x.LastUseLocation.DisplayName,
                          UseLocationAddress = x.LastUseLocation.Address,

                          CustomerId = x.Account.Id,
                          CustomerKey = x.Account.Guid,
                          CustomerDisplayName = x.Account.DisplayName,
                          CustomerMobileNumber = x.Account.MobileNumber,
                          CustomerIconUrl = x.Account.IconStorage.Path,

                          CreateDate = x.CreateDate,
                          StatusId = x.StatusId,
                          StatusCode = x.Status.SystemName,
                          StatusName = x.Status.Name,
                      }).FirstOrDefault();
                        if (DealCodeDetails != null)
                        {
                            DealCodeDetails.Locations = _HCoreContext.MDDealLocation.Where(x => x.DealId == DealCodeDetails.ReferenceId)
                          .Select(x => new ODealOperation.DealCodeLocation
                          {
                              Address = x.Location.Address,
                              DisplayName = x.Location.DisplayName,
                              Latitude = x.Location.Latitude,
                              Longitude = x.Location.Longitude,
                          }).ToList();
                            if (DealCodeDetails.Locations.Count == 0)
                            {
                                DealCodeDetails.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == DealCodeDetails.MerchantReferenceId
                                && x.StatusId == HelperStatus.Default.Active
                                && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore)
                               .Select(x => new ODealOperation.DealCodeLocation
                               {
                                   Address = x.Address,
                                   DisplayName = x.DisplayName,
                                   Latitude = x.Latitude,
                                   Longitude = x.Longitude,
                               }).ToList();
                            }

                            if (!string.IsNullOrEmpty(DealCodeDetails.ImageUrl))
                            {
                                DealCodeDetails.ImageUrl = _AppConfig.StorageUrl + DealCodeDetails.ImageUrl;
                            }
                            else
                            {
                                DealCodeDetails.ImageUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(DealCodeDetails.MerchantIconUrl))
                            {
                                DealCodeDetails.MerchantIconUrl = _AppConfig.StorageUrl + DealCodeDetails.MerchantIconUrl;
                            }
                            else
                            {
                                DealCodeDetails.MerchantIconUrl = _AppConfig.Default_Icon;
                            }


                            if (!string.IsNullOrEmpty(DealCodeDetails.CustomerIconUrl))
                            {
                                DealCodeDetails.CustomerIconUrl = _AppConfig.StorageUrl + DealCodeDetails.CustomerIconUrl;
                            }
                            else
                            {
                                DealCodeDetails.CustomerIconUrl = _AppConfig.Default_Icon;
                            }
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, DealCodeDetails, "HCP0132", ResponseCode.HCP0132);

                        }
                        else
                        {
                            _HCoreContext.SaveChanges();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0122", ResponseCode.HCP0122);
                        }

                    }

                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DealRedeem_Initialize", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        MDDealAddress _MDDealAddress;


        /// <summary>
        /// Description: Buydeal initialize v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyDeal_InitializeV2(ODealOperation.DealPurchase.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.DealId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Deal id required");
                }
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Deal key required");
                }
                if (_Request.Customer == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Customer information required");
                }
                if (_Request.Customer.IsGuestUser == true && string.IsNullOrEmpty(_Request.Customer.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Customer information required");
                }
                if (_Request.Customer.IsGuestUser == true && HCoreHelper.ValidateEmailAddress(_Request.Customer.EmailAddress) == false)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Invalid email address");
                }
                if (_Request.AddressComponent == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Address information required");
                }
                if (_Request.AddressComponent.CountryId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Country information required");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.AddressComponent.CountryId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            Isd = x.Isd,
                            MobileNumberLength = x.MobileNumberLength,
                        }).FirstOrDefault();
                    if (CountryDetails == null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Country country details");
                    }
                    string MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, _Request.AddressComponent.ContactNumber, CountryDetails.MobileNumberLength);
                    if (_Request.Customer.IsGuestUser)
                    {
                        var MobileNumberValidate = HCoreHelper.ValidateMobileNumber(MobileNumber);
                        if (MobileNumberValidate.IsNumberValid)
                        {
                            MobileNumber = HCoreHelper.FormatMobileNumber(MobileNumberValidate.Isd, _Request.Customer.MobileNumber);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Invalid mobile number");
                        }
                    }
                    var _DealDetails = _HCoreContext.MDDeal
                                                 .Where(x => x.Id == _Request.DealId)
                                                 .Select(x => new
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     MerchantStatusId = x.Account.StatusId,

                                                     EndDate = x.EndDate,

                                                     UsageTypeId = x.UsageTypeId,
                                                     CodeValidityDays = x.CodeValidtyDays,
                                                     CodeValidityStartDate = x.CodeValidityStartDate,
                                                     CodeValidityEndDate = x.CodeValidityEndDate,

                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     MaximumUnitSale = x.MaximumUnitSale,
                                                     //MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,
                                                     MaximumUnitSalePerPerson = x.MaximumUnitSalePerPerson,
                                                     StatusId = x.StatusId,
                                                     BuyerTypeId = x.BuyerTypeId,
                                                     TotalPurchase = x.MDDealCode.Count(),
                                                 }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        if (_DealDetails.StatusId != HelperStatus.Deals.Published)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0198", ResponseCode.HCP0198);
                        }
                        if (_DealDetails.MerchantStatusId != HelperStatus.Default.Active)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", ResponseCode.HCP0137);
                        }

                        if (_DealDetails.TotalPurchase <= _DealDetails.MaximumUnitSale)
                        {

                            if (_Request.Customer.IsGuestUser == true && _Request.Customer.AccountId == 0)
                            {

                                _AppUserRequest = new OAppProfile.Request();
                                _AppUserRequest.Name = _Request.Customer.Name;
                                _AppUserRequest.MobileNumber = MobileNumber;
                                _AppUserRequest.EmailAddress = _Request.Customer.EmailAddress;
                                _AppUserRequest.CountryId = CountryDetails.Id;
                                _AppUserRequest.AddressComponent = _Request.AddressComponent;
                                _AppUserRequest.UserReference = _Request.UserReference;
                                _ManageCoreUserAccess = new ManageCoreUserAccess();
                                var _CreateUserResponse = _ManageCoreUserAccess.CreateAppUserAccount(_AppUserRequest);
                                if (_CreateUserResponse.Status == ResponseStatus.Success)
                                {
                                    _Request.Customer.AccountId = _CreateUserResponse.AccountId;
                                    _Request.Customer.AccountKey = _CreateUserResponse.AccountKey;

                                    _Request.AddressComponent.ReferenceId = _CreateUserResponse.AddressReferenceId;
                                    _Request.AddressComponent.ReferenceKey = _CreateUserResponse.AddressReferenceKey;

                                    long ToAddressId = 0;
                                    string ToAddressKey = null;

                                    if (_CreateUserResponse.AddressReferenceId > 0)
                                    {
                                        if (!string.IsNullOrEmpty(_Request.AddressComponent.Reference))
                                        {
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                _MDDealAddress = new MDDealAddress();
                                                _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                                                _MDDealAddress.AddressId = _CreateUserResponse.AddressReferenceId;
                                                _MDDealAddress.AddressReference = _Request.AddressComponent.Reference;
                                                _MDDealAddress.AccountId = _CreateUserResponse.AccountId;
                                                _HCoreContext.MDDealAddress.Add(_MDDealAddress);
                                                _HCoreContext.SaveChanges();
                                                ToAddressId = _MDDealAddress.Id;
                                                ToAddressKey = _MDDealAddress.Guid;
                                            }
                                        }
                                    }
                                    double DeliveryCharge = 0;
                                    if (_Request.Delivery != null && _Request.Delivery.DeliveryFee > 0)
                                    {
                                        DeliveryCharge = Math.Round(_Request.Delivery.DeliveryFee, 2);
                                    }
                                    var _Response = new ODealOperation.DealPurchase.Initialize.Response
                                    {
                                        DealId = _Request.DealId,
                                        DealKey = _Request.DealKey,
                                        Customer = _Request.Customer,
                                        AddressComponent = _Request.AddressComponent,
                                        ToAddressId = ToAddressId,
                                        ToAddressKey = ToAddressKey,
                                        PaymentDetails = new ODealOperation.DealPurchase.PaymentDetails
                                        {
                                            ReferenceId = HCoreHelper.GenerateRandomNumber(4),
                                            ReferenceKey = HCoreHelper.GenerateGuid(),
                                            Amount = Math.Round((double)_DealDetails.Amount, 2),
                                            Fee = 0,
                                            DeliveryFee = Math.Round(DeliveryCharge, 2),
                                            TotalAmount = Math.Round((double)_DealDetails.Amount + (double)DeliveryCharge, 2),
                                        }
                                    };
                                    _HCoreContext.Dispose();

                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0110", ResponseCode.HCP0110);
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Unable to process your request." + _CreateUserResponse.StatusMessage);
                                }
                            }
                            else
                            {
                                var CustomerAccountStatusId = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.Customer.AccountId)
                                    .Select(x => x.StatusId).FirstOrDefault();
                                if (CustomerAccountStatusId != HelperStatus.Default.Active)
                                {
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0139", ResponseCode.HCP0139);
                                }
                                long? MaxUnitSale = _DealDetails.MaximumUnitSalePerPerson;
                                if (MaxUnitSale == null)
                                {
                                    MaxUnitSale = 0;
                                }
                                long UserPurchase = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId && x.AccountId == _Request.Customer.AccountId);
                                if (UserPurchase < MaxUnitSale)
                                {
                                    if (_DealDetails.BuyerTypeId == 2) // New Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.Customer.AccountId);
                                        if (UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for new customers only");
                                        }
                                    }
                                    else if (_DealDetails.BuyerTypeId == 3) // Existing Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.Customer.AccountId);
                                        if (!UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for existing customers only");
                                        }
                                    }
                                    double DeliveryCharge = 0;
                                    if (_Request.Delivery != null && _Request.Delivery.DeliveryFee > 0)
                                    {
                                        DeliveryCharge = Math.Round(_Request.Delivery.DeliveryFee, 2);
                                    }
                                    //var _Response = new ODealOperation.DealPurchase.Initialize.Response
                                    //{
                                    //    ReferenceId = HCoreHelper.GenerateRandomNumber(4),
                                    //    ReferenceKey = HCoreHelper.GenerateGuid(),
                                    //    Amount = Math.Round((double)_DealDetails.Amount, 2),
                                    //    Fee = 0,
                                    //    DeliveryCharge = Math.Round(DeliveryCharge, 2),
                                    //    TotalAmount = Math.Round((double)_DealDetails.Amount + (double)DeliveryCharge, 2),
                                    //    Customer = _Request.Customer,
                                    //    AddressComponent = _Request.AddressComponent,
                                    //};
                                    var _Response = new ODealOperation.DealPurchase.Initialize.Response
                                    {
                                        DealId = _Request.DealId,
                                        DealKey = _Request.DealKey,
                                        Customer = _Request.Customer,
                                        AddressComponent = _Request.AddressComponent,
                                        PaymentDetails = new ODealOperation.DealPurchase.PaymentDetails
                                        {
                                            ReferenceId = HCoreHelper.GenerateRandomNumber(4),
                                            ReferenceKey = HCoreHelper.GenerateGuid(),
                                            Amount = Math.Round((double)_DealDetails.Amount, 2),
                                            Fee = 0,
                                            DeliveryFee = Math.Round(DeliveryCharge, 2),
                                            TotalAmount = Math.Round((double)_DealDetails.Amount + (double)DeliveryCharge, 2),
                                        }
                                    };
                                    _HCoreContext.Dispose();

                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0110", ResponseCode.HCP0110);
                                }
                                else
                                {
                                    _HCoreContext.Dispose();
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0134", ResponseCode.HCP0134);
                                }
                            }
                            //long UserPurchase = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId && x.AccountId == _Request.AccountId);
                            //if (UserPurchase < MaxUnitSale)
                            //{
                            //    if (_DealDetails.BuyerTypeId == 2) // New Customers
                            //    {
                            //        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.AccountId);
                            //        if (UserAllPurchase)
                            //        {
                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for new customers only");
                            //        }
                            //    }
                            //    else if (_DealDetails.BuyerTypeId == 3) // Existing Customers
                            //    {
                            //        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.AccountId);
                            //        if (!UserAllPurchase)
                            //        {
                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for existing customers only");
                            //        }
                            //    }
                            //    _ManageCoreTransaction = new ManageCoreTransaction();
                            //    double AccountBalance = _ManageCoreTransaction.GetAccountBalance(CustomerDetails.ReferenceId, TransactionSource.App);
                            //    if (_Request.PaymentSource == "wallet")
                            //    {
                            //        if (AccountBalance < _DealDetails.Amount)
                            //        {
                            //            _HCoreContext.Dispose();
                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0138", ResponseCode.HCP0138);
                            //        }
                            //    }
                            //    double DeliveryCharge = 0;
                            //    if (_Request.Delivery != null && _Request.Delivery.DeliveryCharge > 0)
                            //    {
                            //        DeliveryCharge = Math.Round(DeliveryCharge, 2);
                            //    }
                            //    var _Response = new
                            //    {
                            //        ReferenceId = HCoreHelper.GenerateRandomNumber(4),
                            //        ReferenceKey = HCoreHelper.GenerateGuid(),
                            //        Amount = Math.Round((double)_DealDetails.Amount, 2),
                            //        Fee = 0,
                            //        DeliveryCharge = Math.Round(DeliveryCharge, 2),
                            //        TotalAmount = Math.Round((double)_DealDetails.Amount + (double)DeliveryCharge, 2),
                            //    };
                            //    _HCoreContext.Dispose();
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0110", ResponseCode.HCP0110);
                            //}
                            //else
                            //{
                            //    _HCoreContext.Dispose();
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0134", ResponseCode.HCP0134);
                            //}
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", ResponseCode.HCP0133);
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("BuyDeal_InitializeV2", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Buydeal confirm v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse BuyDeal_ConfirmV2(ODealOperation.DealPurchase.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.DealId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Deal id required");
                }
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Deal key required");
                }
                if (_Request.Customer == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Customer information required");
                }
                if (_Request.Customer.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Invalid customer id");
                }
                if (string.IsNullOrEmpty(_Request.Customer.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Invalid customer key");
                }
                if (_Request.AddressComponent == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Address information required");
                }
                if (_Request.AddressComponent.CountryId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCX4500", "Country information required");
                }
                //if (_Request.ReferenceId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                //}
                //if (string.IsNullOrEmpty(_Request.ReferenceKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                //}
                //if (_Request.DealId == 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREF", ResponseCode.HCDACCREF);
                //}
                //if (string.IsNullOrEmpty(_Request.DealKey))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDACCREFKEY", ResponseCode.HCDACCREFKEY);
                //}
                //if (string.IsNullOrEmpty(_Request.PaymentSource))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0111", ResponseCode.HCP0111);
                //}
                //if (_Request.PaymentSource != "wallet" && _Request.PaymentSource != "online")
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0112", ResponseCode.HCP0112);
                //}
                using (_HCoreContext = new HCoreContext())
                {
                    var _DealDetails = _HCoreContext.MDDeal
                                                 .Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey)
                                                 .Select(x => new
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountId = x.AccountId,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,

                                                     Title = x.TitleContent,
                                                     DealTitle = x.Title,
                                                     Description = x.Description,
                                                     MerchantId = x.AccountId,
                                                     MerchantDisplayName = x.Account.DisplayName,
                                                     MerchantIconUrl = x.Account.IconStorage.Path,
                                                     MerchantContactNumber = x.Account.ContactNumber,
                                                     MerchantAddress = x.Account.Address,
                                                     MerchantEmail = x.Account.EmailAddress,

                                                     ImageUrl = x.PosterStorage.Path,

                                                     UsageTypeId = x.UsageTypeId,
                                                     CodeValidityDays = x.CodeValidtyDays,
                                                     CodeValidityStartDate = x.CodeValidityStartDate,
                                                     CodeValidityEndDate = x.CodeValidityEndDate,
                                                     Terms = x.Terms,
                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     MaximumUnitSale = x.MaximumUnitSale,
                                                     MaximumUnitSalePerDay = x.MaximumUnitSalePerDay,
                                                     MaximumUnitSalePerPerson = x.MaximumUnitSalePerPerson,
                                                     BuyerTypeId = x.BuyerTypeId,
                                                     DealTypeId = x.DealTypeId,
                                                     DealDeliveryTypeId = x.DeliveryTypeId,
                                                 }).FirstOrDefault();
                    if (_DealDetails != null)
                    {
                        long TotalPurchase = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId);
                        if (TotalPurchase <= _DealDetails.MaximumUnitSale)
                        {
                            DateTime TodaysDate = HCoreHelper.GetGMTDate();
                            long TotalPurchaseForDay = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId && x.CreateDate.Date == TodaysDate);
                            if (TotalPurchaseForDay < _DealDetails.MaximumUnitSalePerDay)
                            {
                                long? MaxUnitSale = _DealDetails.MaximumUnitSalePerPerson;
                                if (MaxUnitSale == null)
                                {
                                    MaxUnitSale = 0;
                                }
                                long UserPurchase = _HCoreContext.MDDealCode.Count(x => x.DealId == _DealDetails.ReferenceId && x.AccountId == _Request.Customer.AccountId);
                                if (UserPurchase < MaxUnitSale)
                                {
                                    if (_DealDetails.BuyerTypeId == 1) // All Customers
                                    {

                                    }
                                    else if (_DealDetails.BuyerTypeId == 2) // New Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.UserReference.AccountId);
                                        if (UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for new customers only");
                                        }
                                    }
                                    else if (_DealDetails.BuyerTypeId == 3) // Existing Customers
                                    {
                                        bool UserAllPurchase = _HCoreContext.MDDealCode.Any(x => x.AccountId == _Request.UserReference.AccountId);
                                        if (!UserAllPurchase)
                                        {
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0137", "Deal purchase is for existing customers only");
                                        }
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", "Deal sold. Your amount is credited back to your ThankUCash Wallet.");
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", "Deal sold. Your amount is credited back to your ThankUCash Wallet.");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0133", "Deal sold. Your amount is credited back to your ThankUCash Wallet.");
                        }
                        //if (_Request.TotalAmount == 0)
                        //{
                        //    _Request.TotalAmount = (double)_DealDetails.TotalAmount;
                        //}
                        //else
                        //{
                        //    _CoreTransactionRequest.InvoiceAmount = (double)_DealDetails.TotalAmount;
                        //}

                        _ManageCoreTransaction = new ManageCoreTransaction();
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.Customer.AccountId && x.Guid == _Request.Customer.AccountKey)
                            .Select(x => new
                            {
                                ReferenceId = x.Id,
                                DisplayName = x.DisplayName,
                                MobileNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                                AccountStatusId = x.StatusId,
                                Address = x.Address,
                                FirstName = x.FirstName,
                                LastName = x.LastName,

                            }).FirstOrDefault();
                        if (CustomerDetails.AccountStatusId != HelperStatus.Default.Active)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0139", ResponseCode.HCP0139);
                        }

                        double AccountBalance = _ManageCoreTransaction.GetAccountBalance(CustomerDetails.ReferenceId, TransactionSource.TUC);
                        if (AccountBalance < _Request.PaymentDetails.TotalAmount)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0138", ResponseCode.HCP0138);
                        }

                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CustomerId = _Request.Customer.AccountId;
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = _Request.PaymentDetails.ReferenceKey;
                        _CoreTransactionRequest.ParentId = _DealDetails.AccountId;
                        _CoreTransactionRequest.InvoiceAmount = (double)_Request.PaymentDetails.TotalAmount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.InvoiceAmount;
                        //_CoreTransactionRequest.AccountNumber = _PayStackResponseData.authorization.bin;
                        _CoreTransactionRequest.ReferenceNumber = _DealDetails.ReferenceKey;
                        _CoreTransactionRequest.CreatedById = _Request.Customer.AccountId;
                        _CoreTransactionRequest.ReferenceAmount = _CoreTransactionRequest.InvoiceAmount;
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.Customer.AccountId,
                            SourceId = TransactionSource.TUC,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.Deal.DealPurchaseRedeem,
                            Amount = _CoreTransactionRequest.InvoiceAmount,
                            Charge = 0,
                            TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _DealDetails.AccountId,
                            SourceId = TransactionSource.Deals,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.Deal.DealPurchase,
                            Amount = _CoreTransactionRequest.InvoiceAmount,
                            Charge = 0,
                            TotalAmount = _CoreTransactionRequest.InvoiceAmount,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;

                        OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            _MDDealCode = new MDDealCode();
                            _MDDealCode.Guid = _Request.PaymentDetails.ReferenceKey;
                            _MDDealCode.DealId = _DealDetails.ReferenceId;
                            _MDDealCode.AccountId = _Request.Customer.AccountId;
                            _MDDealCode.ItemCode = "12" + HCoreHelper.GenerateRandomNumber(9);
                            _MDDealCode.ItemPin = HCoreHelper.GenerateRandomNumber(4);
                            _MDDealCode.ItemAmount = _DealDetails.Amount;
                            _MDDealCode.AvailableAmount = _DealDetails.Amount;
                            _MDDealCode.TransactionId = TransactionResponse.ReferenceId;
                            _MDDealCode.StartDate = HCoreHelper.GetGMTDateTime();
                            if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.HoursAfterPurchase || _DealDetails.UsageTypeId == Helpers.DealCodeUsageType.DaysAfterPurchase)
                            {
                                if (_DealDetails.CodeValidityDays != null && _DealDetails.CodeValidityDays > 0)
                                {
                                    _MDDealCode.EndDate = _MDDealCode.StartDate.Value.AddHours((double)_DealDetails.CodeValidityDays);
                                }
                                else
                                {
                                    _MDDealCode.EndDate = _MDDealCode.StartDate.Value.AddHours(168);
                                }
                            }
                            else if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.DealEndDate)
                            {
                                _MDDealCode.EndDate = _DealDetails.EndDate;
                            }
                            else if (_DealDetails.UsageTypeId == Helpers.DealCodeUsageType.ExpiresOnDate)
                            {

                                if (_DealDetails.CodeValidityEndDate != null)
                                {
                                    _MDDealCode.EndDate = _DealDetails.CodeValidityEndDate;
                                }
                                else
                                {
                                    _MDDealCode.EndDate = _DealDetails.EndDate;
                                }
                            }
                            else
                            {
                                _MDDealCode.EndDate = _DealDetails.EndDate;
                            }

                            _MDDealCode.Amount = _Request.PaymentDetails.Amount;
                            _MDDealCode.ItemCount = 1;
                            _MDDealCode.Charge = _Request.PaymentDetails.Fee;
                            _MDDealCode.DeliveryCharge = _Request.PaymentDetails.DeliveryFee;
                            _MDDealCode.TotalAmount = _Request.PaymentDetails.TotalAmount;
                            if (_DealDetails.DealTypeId == Helpers.DealType.ProductDeal && (_DealDetails.DealDeliveryTypeId == Helpers.DeliveryType.Delivery || _DealDetails.DealDeliveryTypeId == Helpers.DeliveryType.InStoreAndDelivery) && _Request.Delivery != null)
                            {
                                if (_Request.Delivery.FromAddressId > 0)
                                {
                                    var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.Delivery.FromAddressId).Select(x => new { x.Id }).FirstOrDefault();
                                    if (AddressDetails != null)
                                    {
                                        _MDDealCode.FromAddressId = AddressDetails.Id;
                                    }
                                }
                                if (_Request.Delivery.ToAddressId > 0)
                                {
                                    var AddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == _Request.Delivery.ToAddressId).Select(x => new { x.Id }).FirstOrDefault();
                                    if (AddressDetails != null)
                                    {
                                        _MDDealCode.ToAddressId = AddressDetails.Id;
                                    }
                                }
                                if (_Request.Delivery.FromAddressId > 0)
                                {
                                    _MDDealCode.Comment = _Request.Delivery.FromAddressId + "|";
                                }
                                else
                                {
                                    _MDDealCode.Comment = "0|";
                                }
                                if (_Request.Delivery.ToAddressId > 0)
                                {
                                    _MDDealCode.Comment += _Request.Delivery.ToAddressId;
                                }
                                else
                                {
                                    _MDDealCode.Comment += "0";
                                }
                            }

                            if (HostEnvironment == HostEnvironmentType.Dev)
                            {
                                _MDDealCode.PartnerId = 20689;
                            }
                            else if (HostEnvironment == HostEnvironmentType.Tech)
                            {
                                _MDDealCode.PartnerId = 1326827;
                            }
                            else if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                _MDDealCode.PartnerId = 336898;
                            }
                            else
                            {
                                _MDDealCode.PartnerId = 743663;
                            }

                            _MDDealCode.UseCount = 0;
                            _MDDealCode.UseAttempts = 0;
                            _MDDealCode.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDDealCode.CreatedById = _Request.Customer.AccountId;
                            _MDDealCode.StatusId = HelperStatus.DealCodes.Unused;
                            _HCoreContext.MDDealCode.Add(_MDDealCode);
                            _HCoreContext.SaveChanges();
                            _HCoreContext.Dispose();

                            Panelurls _Panelurls = new Panelurls();
                            if (HostEnvironment == HostEnvironmentType.Dev)
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.dev/";
                                _Panelurls.MerchantUrl = "https://merchant-dealday.thankucash.dev/";
                            }
                            else if (HostEnvironment == HostEnvironmentType.Tech)
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.tech/";
                                _Panelurls.MerchantUrl = "https://merchant-dealday.thankucash.tech/";
                            }
                            else if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.co/";
                                _Panelurls.MerchantUrl = "https://merchant.dealday.africa/";
                            }
                            else
                            {
                                _Panelurls.DealsWebUrl = "https://deals.thankucash.com/";
                                _Panelurls.MerchantUrl = "https://merchant.dealday.africa/";
                            }

                            if (_Request.Delivery.ShipmentId != null || _Request.Delivery.ShipmentId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var OrderDetails = _HCoreContext.LSShipments.Where(x => x.DealId == _DealDetails.ReferenceId && x.Deal.Guid == _DealDetails.ReferenceKey
                                                       && x.CustomerId == CustomerDetails.ReferenceId
                                                       && x.Id == _Request.Delivery.ShipmentId && x.Guid == _Request.Delivery.ShipmentKey).FirstOrDefault();
                                    if (OrderDetails != null)
                                    {
                                        var DealCodeDetails = _HCoreContext.MDDealCode.Where(x => x.Guid == _MDDealCode.Guid).FirstOrDefault();

                                        OrderDetails.RateAmount = _Request.Delivery.DeliveryFee;
                                        OrderDetails.TransactionReference = _MDDealCode.TransactionId.ToString();
                                        OrderDetails.CarrierId = _Request.Delivery.DeliveryPartner.CarrierId;
                                        OrderDetails.TotalAmount = OrderDetails.DealPrice + _Request.Delivery.DeliveryFee;

                                        if (_Request.Delivery.DeliveryPartner.CarrierId == 762)
                                        {
                                            OrderDetails.DeliveryPartnerId = 745009;
                                        }
                                        else
                                        {
                                            OrderDetails.DeliveryPartnerId = 745006;
                                        }

                                        DealCodeDetails.OrderId = OrderDetails.Id;

                                        _HCoreContext.SaveChanges();

                                        var DeliveryAddressDetails = _HCoreContext.HCCoreAddress.Where(x => x.Id == OrderDetails.ToAddressId).Select(x => new
                                        {
                                            Address = x.AddressLine1 + ", " + x.City.Name + ", " + x.State.Name,

                                        }).FirstOrDefault();

                                        OrderNotification _OrderNotification = new OrderNotification();
                                        _OrderNotification.MerchantDisplayName = _DealDetails.MerchantDisplayName;
                                        _OrderNotification.EmailAddress = _DealDetails.MerchantEmail;
                                        _OrderNotification.OrderNumber = OrderDetails.OrderReference;
                                        _OrderNotification.DealCode = _MDDealCode.ItemCode;
                                        _OrderNotification.DeliveryAddress = DeliveryAddressDetails.Address;
                                        _OrderNotification.DealTitle = _DealDetails.DealTitle;
                                        _OrderNotification.DealImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                                        _OrderNotification.PanelUrl = _Panelurls.MerchantUrl;
                                        _OrderNotification.UserReference = _Request.UserReference;

                                        var _Actor = ActorSystem.Create("ActorNewOrderNotification");
                                        var _ActorNotify = _Actor.ActorOf<ActorNewOrderNotification>("ActorNewOrderNotification");
                                        _ActorNotify.Tell(_OrderNotification);
                                    }
                                }
                            }

                            var _Response = new
                            {
                                ReferenceId = _MDDealCode.Id,
                                ReferenceKey = _MDDealCode.Guid,
                            };
                            try
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.Customer.AccountId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                                    {
                                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "dealpurchasedetails", "Deal Purchase Successful", _DealDetails.Title + " deal puchase successful for " + _DealDetails.MerchantDisplayName, "dealpurchasedetails", _Response.ReferenceId, _Response.ReferenceKey, "View details", true, null);
                                    }

                                    if (!string.IsNullOrEmpty(CustomerDetails.EmailAddress))
                                    {
                                        var Stores = _HCoreContext.HCUAccount
                                            .Where(x => x.OwnerId == _DealDetails.MerchantId && x.AccountTypeId == UserAccountType.MerchantStore && x.StatusId == HelperStatus.Default.Active)
                                            .Select(x => new
                                            {
                                                Name = x.Name,
                                                ContactNumber = x.ContactNumber,
                                                Address = x.Address,
                                            })
                                            .Skip(0)
                                            .Take(5)
                                            .ToList();
                                        string MerchantIcon = _DealDetails.MerchantIconUrl;
                                        if (!string.IsNullOrEmpty(MerchantIcon))
                                        {
                                            MerchantIcon = _AppConfig.StorageUrl + _DealDetails.MerchantIconUrl;
                                        }
                                        else
                                        {
                                            MerchantIcon = _AppConfig.Default_Icon;
                                        }
                                        var _BDetails = new
                                        {
                                            DisplayName = CustomerDetails.DisplayName,
                                            DealCode = _MDDealCode.ItemCode,
                                            StartDate = _MDDealCode.StartDate.Value.ToString("dd-MM-yyyy HH:mm"),
                                            EndDate = _MDDealCode.EndDate.Value.AddHours(1).ToString("dd-MM-yyyy HH:mm"),
                                            Title = _DealDetails.Title,
                                            Description = _DealDetails.Description,
                                            Terms = _DealDetails.Terms,
                                            ImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl,
                                            Amount = _DealDetails.Amount,

                                            MerchantDisplayName = _DealDetails.MerchantDisplayName,
                                            MerchantLogo = MerchantIcon,
                                            MerchantContactNumber = _DealDetails.MerchantContactNumber,
                                            MerchantAddress = _DealDetails.MerchantAddress,
                                            StoreLocations = Stores
                                        };

                                        var _BDetailsForMerchant = new
                                        {
                                            merchantname = _DealDetails.MerchantDisplayName,
                                            noofdealsold = 1,
                                            customername = CustomerDetails.DisplayName,
                                            customeraddress = CustomerDetails.Address,
                                            trackingnumber = _MDDealCode.Id.ToString(),
                                            ordernumber = _MDDealCode.Id.ToString(),
                                            payableamount = _DealDetails.TotalAmount.ToString(),
                                            firstname = CustomerDetails.FirstName,
                                            lastname = CustomerDetails.LastName,
                                            productname = _DealDetails.Title,
                                            sellersku = "10001",
                                            shopsku = _DealDetails.MerchantId.ToString(),
                                            itemprice = _MDDealCode.ItemAmount.ToString(),
                                            total = _DealDetails.TotalAmount.ToString()

                                        };

                                        if (_Request.Delivery.ShipmentId == null || _Request.Delivery.ShipmentId <= 0)
                                        {

                                            DealPurchaseNotification _DealPurchaseNotification = new DealPurchaseNotification();
                                            _DealPurchaseNotification.MerchantDisplayName = _DealDetails.MerchantDisplayName;
                                            _DealPurchaseNotification.EmailAddress = _DealDetails.MerchantEmail;
                                            _DealPurchaseNotification.OrderNumber = _MDDealCode.Id.ToString();
                                            _DealPurchaseNotification.DealTitle = _DealDetails.DealTitle;
                                            _DealPurchaseNotification.DealImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                                            _DealPurchaseNotification.PanelUrl = _Panelurls.MerchantUrl;
                                            _DealPurchaseNotification.UserReference = _Request.UserReference;

                                            var _Actors = ActorSystem.Create("ActorDealPurchaseNotification");
                                            var _ActorNotifys = _Actors.ActorOf<ActorDealPurchaseNotification>("ActorDealPurchaseNotification");
                                            _ActorNotifys.Tell(_DealPurchaseNotification);
                                        }

                                        ProductReview _ProductReview = new ProductReview();
                                        _ProductReview.CustomerDisplayName = CustomerDetails.DisplayName;
                                        _ProductReview.EmailAddress = CustomerDetails.EmailAddress;
                                        _ProductReview.DealTitle = _DealDetails.DealTitle;
                                        _ProductReview.DealImageUrl = _AppConfig.StorageUrl + _DealDetails.ImageUrl;
                                        _ProductReview.DealId = _DealDetails.ReferenceId;
                                        _ProductReview.DealKey = _DealDetails.ReferenceKey;
                                        _ProductReview.PanelUrl = _Panelurls.DealsWebUrl;
                                        _ProductReview.UserReference = _Request.UserReference;

                                        var _Actor = ActorSystem.Create("ActorProductReviewNotification");
                                        var _ActorNotify = _Actor.ActorOf<ActorProductReviewNotification>("ActorProductReviewNotification");
                                        _ActorNotify.Tell(_ProductReview);

                                        //StringBuilder Html = new StringBuilder(System.IO.File.ReadAllText("./templates/" + "invoice.html"));
                                        //Html.Replace("{{customername}}", CustomerDetails.DisplayName);
                                        //Html.Replace("{{customeraddress}}", CustomerDetails.Address);
                                        //Html.Replace("{{trackingnumber}}", _MDDealCode.Id.ToString());
                                        //Html.Replace("{{ordernumber}}", _MDDealCode.Id.ToString());
                                        //Html.Replace("{{payableamount}}", _DealDetails.TotalAmount.ToString());

                                        //Html.Replace("{{firstname}}", CustomerDetails.FirstName);
                                        //Html.Replace("{{lastname}}", CustomerDetails.LastName);

                                        //Html.Replace("{{productname}}", _DealDetails.Title);
                                        //Html.Replace("{{sellersku}}","10001" );
                                        //Html.Replace("{{shopsku}}", _DealDetails.MerchantId.ToString());
                                        //Html.Replace("{{itemprice}}", _MDDealCode.ItemAmount.ToString());
                                        //Html.Replace("{{total}}", _DealDetails.TotalAmount.ToString());

                                        ////Html.Replace("{{returnqrcode}}", );

                                        //string AttachmentName = "Invoice_" + _MDDealCode.Id.ToString() + ".pdf";
                                        //string pdfBase64String = HCoreHelper.HtmlToPdfBase64(Html.ToString());


                                        //send email to customer
                                        HCoreHelper.BroadCastEmail("d-cfb071d1a1174d6d9b08ce8fdee6b6dd", "ThankUCash", CustomerDetails.EmailAddress, _BDetails, _Request.UserReference);
                                        //Send email to merchant
                                        //HCoreHelper.BroadCastEmail("d-112d272ca74f42d1bd1c8f0e7043eaa9", "ThankUCash", _DealDetails.MerchantEmail, _BDetailsForMerchant, _Request.UserReference, AttachmentBase64String: pdfBase64String, AttachmentName: AttachmentName);
                                    }
                                }
                            }
                            catch (Exception _Exception)
                            {
                                HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
                            }

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0109", ResponseCode.HCP0109);

                            //var _Response = new
                            //{
                            //    ReferenceId = TransactionResponse.ReferenceId,
                            //    ReferenceKey = TransactionResponse.ReferenceKey,
                            //    Amount = _DealDetails.Amount,
                            //};
                            //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0110", ResponseCode.HCP0110);
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP0113", ResponseCode.HCP0113);
                            #endregion
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("BuyDeal_ConfirmV2", _Exception, _Request.UserReference, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        public class Panelurls
        {
            public string? DealsWebUrl { get; set; }
            public string? MerchantUrl { get; set; }
        }
    }
}
