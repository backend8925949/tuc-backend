//==================================================================================
// FileName: FrameworkCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deals category
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using System.Linq.Dynamic.Core;
using HCore.TUC.Plugins.Deals.Resource;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Plugins.Deals.Object;
using System.Collections.Generic;

namespace HCore.TUC.Plugins.Deals.Framework
{
    public class FrameworkCategory
    {
        HCoreContext _HCoreContext;
        MDCategory _MDCategory;

        /// <summary>
        /// Description: Gets all category list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAllCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDCategory
                                               .Where(x => x.StatusId == HelperStatus.Default.Active
                                                   && x.RootCategory.ParentCategoryId != null
                                                   && x.RootCategory.StatusId == HelperStatus.Default.Active)
                                               .OrderBy(x => x.RootCategory.ParentCategory.Name)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = _AppConfig.StorageUrl + x.RootCategory.ParentCategory.IconStorage.Path,
                                                    Fees = x.Commission,
                                                    ParentCategoryId = x.RootCategory.ParentCategory.Id,
                                                    ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                    ParentCategoryName = x.RootCategory.ParentCategory.Name,
                                                })
                                                .OrderBy(x => x.Name)
                                               .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                    }
                    #region Get Data
                    var _List = _HCoreContext.MDCategory
                                               .Where(x => x.StatusId == HelperStatus.Default.Active
                                                   && x.RootCategory.ParentCategoryId != null
                                                   && x.RootCategory.StatusId == HelperStatus.Default.Active)
                                               .OrderBy(x => x.RootCategory.ParentCategory.Name)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = _AppConfig.StorageUrl + x.RootCategory.ParentCategory.IconStorage.Path,
                                                    Fees = x.Commission,
                                                    ParentCategoryId = x.RootCategory.ParentCategory.Id,
                                                    ParentCategoryKey = x.RootCategory.ParentCategory.Guid,
                                                    ParentCategoryName = x.RootCategory.ParentCategory.Name,
                                                })
                                                .OrderBy(x => x.Name)
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealCategory", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the category list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDCategory
                                               .Where(x => x.StatusId == HelperStatus.Default.Active
                                                   && x.RootCategory.ParentCategoryId == null
                                                   && x.RootCategory.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,
                                                    Fees = x.Commission,
                                                })
                                                .OrderBy(x => x.Name)
                                               .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                    }
                    #region Get Data
                    var _List = _HCoreContext.MDCategory
                                               .Where(x => x.StatusId == HelperStatus.Default.Active
                                                   && x.RootCategory.ParentCategoryId == null
                                                   && x.RootCategory.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,
                                                    Fees = x.Commission,
                                                })
                                                .OrderBy(_Request.SortExpression)
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the sub category list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDCategory
                                               .Where(x => x.StatusId == HelperStatus.Default.Active
                                                   && x.RootCategory.ParentCategoryId == _Request.ReferenceId
                                                   && x.RootCategory.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,
                                                    Fees = x.Commission,
                                                })
                                                .OrderBy(x => x.Name)
                                               .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                    }
                    #region Get Data
                    var _List = _HCoreContext.MDCategory
                                               .Where(x => x.StatusId == HelperStatus.Default.Active
                                                   && x.RootCategory.ParentCategoryId == _Request.ReferenceId
                                                   && x.RootCategory.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new
                                                {
                                                    ReferenceId = x.RootCategory.Id,
                                                    ReferenceKey = x.RootCategory.Guid,
                                                    Name = x.RootCategory.Name,
                                                    IconUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,
                                                    Fees = x.Commission,
                                                })
                                                .OrderBy(_Request.SortExpression)
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSubCategory", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCategory(OCategory.Save.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Commission < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0178", ResponseCode.HCP0178);
                }
                if (_Request.RootCategoryId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0168", ResponseCode.HCP0168);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDSTATUS", ResponseCode.HCDSTATUS);
                }
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    bool CheckDetails = _HCoreContext.TUCCategory.Any(x => x.Id == _Request.RootCategoryId && x.Guid == _Request.RootCategoryKey);
                    if (CheckDetails)
                    {
                        bool categoryDetails = _HCoreContext.MDCategory.Any(x => x.RootCategoryId == _Request.RootCategoryId && x.Commission == _Request.Commission);
                        if (categoryDetails)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0164", ResponseCode.HCP0164);
                        }
                        else
                        {
                            _MDCategory = new MDCategory();
                            _MDCategory.Guid = HCoreHelper.GenerateGuid();
                            _MDCategory.RootCategoryId = _Request.RootCategoryId;
                            _MDCategory.Commission = _Request.Commission;
                            _MDCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                            _MDCategory.CreatedById = _Request.UserReference.AccountId;
                            _MDCategory.StatusId = StatusId;
                            _HCoreContext.MDCategory.Add(_MDCategory);
                            _HCoreContext.SaveChanges();
                        }
                        var _Response = new
                        {
                            ReferenceId = _MDCategory.Id,
                            ReferenceKey = _MDCategory.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0163", ResponseCode.HCP0163);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0179", ResponseCode.HCP0179);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCategory(OCategory.Update _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDINSTATUS", ResponseCode.HCDINSTATUS);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _CategoryDetails = _HCoreContext.MDCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_CategoryDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.RootCategoryKey))
                        {
                            bool categoryDetails = _HCoreContext.MDCategory.Any(x => x.Commission == _Request.Commission && x.Id != _CategoryDetails.Id && _CategoryDetails.RootCategoryId == _Request.RootCategoryId);
                            if (categoryDetails)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0164", ResponseCode.HCP0164);
                            }
                            _CategoryDetails.Commission = _Request.Commission;
                        }
                        if (StatusId > 0)
                        {
                            _CategoryDetails.StatusId = StatusId;
                        }
                        _CategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _CategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CategoryDetails, "HCP0165", ResponseCode.HCP0165);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCategory(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var CategoryDetails = _HCoreContext.MDCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (CategoryDetails != null)
                    {
                        _HCoreContext.MDCategory.Remove(CategoryDetails);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        var _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCP0167", ResponseCode.HCP0167);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the category details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategory(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREF", ResponseCode.HCDREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCDREFKEY", ResponseCode.HCDREFKEY);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OCategory.Details _CategoryDetails = (from x in _HCoreContext.MDCategory
                                                          where x.Guid == _Request.ReferenceKey
                                                          && x.Id == _Request.ReferenceId
                                                          select new OCategory.Details
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              CategoryId = x.RootCategory.Id,
                                                              CategoryKey = x.RootCategory.Guid,
                                                              CategoryName = x.RootCategory.Name,
                                                              CategoryStatusId = x.RootCategory.Status.Id,
                                                              CategoryStatusCode = x.RootCategory.Status.SystemName,
                                                              CategoryStatusName = x.RootCategory.Status.Name,
                                                              CategoryCreateDate = x.RootCategory.CreateDate,
                                                              IconStorageUrl = x.RootCategory.IconStorage.Path,
                                                              PosterStorageUrl = x.RootCategory.PosterStorage.Path,

                                                              Commission = x.Commission,

                                                              CreateDate = x.CreateDate,
                                                              CreatedById = x.CreatedById,
                                                              CreatedByKey = x.CreatedBy.Guid,
                                                              CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                              ModifyDate = x.ModifyDate,
                                                              ModifyById = x.ModifyById,
                                                              ModifyByKey = x.ModifyBy.Guid,
                                                              ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name,
                                                          })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_CategoryDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_CategoryDetails.IconStorageUrl))
                        {
                            _CategoryDetails.IconStorageUrl = _AppConfig.StorageUrl + _CategoryDetails.IconStorageUrl;
                        }
                        else
                        {
                            _CategoryDetails.IconStorageUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_CategoryDetails.PosterStorageUrl))
                        {
                            _CategoryDetails.PosterStorageUrl = _AppConfig.StorageUrl + _CategoryDetails.PosterStorageUrl;
                        }
                        else
                        {
                            _CategoryDetails.PosterStorageUrl = _AppConfig.Default_Poster;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CategoryDetails, "HCD0200", ResponseCode.HCD0200);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the category list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategories(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDCategory
                                               .Where(x => x.Id == _Request.ReferenceId
                                                   && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCategory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CategoryId = x.RootCategory.Id,
                                                    CategoryKey = x.RootCategory.Guid,
                                                    CategoryName = x.RootCategory.Name,
                                                    CategoryStatusId = x.RootCategory.StatusId,
                                                    IconStorageUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,
                                                    CategoryImageUrl=_AppConfig.StorageUrl+x.RootCategory.IconStorage.Path,
                                                    Commission = x.Commission,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                                .OrderBy(x => x.CategoryId)
                                               .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                    }
                    #region Get Data
                    var _List = _HCoreContext.MDCategory
                                               .Where(x => x.Id == _Request.ReferenceId
                                                   && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OCategory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CategoryId = x.RootCategory.Id,
                                                    CategoryKey = x.RootCategory.Guid,
                                                    CategoryName = x.RootCategory.Name,
                                                    CategoryStatusId = x.RootCategory.StatusId,
                                                    IconStorageUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,
                                                    CategoryImageUrl = _AppConfig.StorageUrl + x.RootCategory.IconStorage.Path,

                                                    Commission = x.Commission,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                                .OrderBy(_Request.SortExpression)
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }


        List<OCategory.AllCats> _AllCats;
        /// <summary>
        /// Description: Gets the deals category list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealsCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    var TCategories = _HCoreContext.MDCategory
                           .Select(x => new OCategory.Details
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,

                               ParentCategoryId = x.RootCategory.ParentCategoryId,

                               Name = x.RootCategory.Name,
                               Description = x.RootCategory.Description,

                               IconUrl = x.RootCategory.IconStorage.Path,
                               PosterUrl = x.RootCategory.PosterStorage.Path,
                               CategoryImageUrl=x.RootCategory.IconStorage.Path,
                               Fee  = x.Commission,
                               StatusId = x.StatusId,
                               StatusCode = x.Status.SystemName,
                               StatusName = x.Status.Name,

                               ModifyDate  = x.ModifyDate,
                               ModifyByDisplayName = x.ModifyBy.DisplayName,

                           }).OrderBy(x => x.Name).ToList();
                    foreach (var TCategoryItem in TCategories)
                    {
                        if (!string.IsNullOrEmpty(TCategoryItem.IconUrl))
                        {
                            TCategoryItem.IconUrl = _AppConfig.StorageUrl + TCategoryItem.IconUrl;
                        }
                        else
                        {
                            TCategoryItem.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(TCategoryItem.PosterUrl))
                        {
                            TCategoryItem.PosterUrl = _AppConfig.StorageUrl + TCategoryItem.PosterUrl;
                        }
                        else
                        {
                            TCategoryItem.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _AllCats = new List<OCategory.AllCats>();
                    var RootCategories = TCategories.Where(x => x.ParentCategoryId == null).ToList();
                    var SubCategories = TCategories.Where(x => x.ParentCategoryId != null).ToList();
                    foreach (var RootCategory in RootCategories)
                    {
                        var SubCats = SubCategories.Where(x => x.ParentCategoryId == RootCategory.ReferenceId)
                            .Select(x => new OCategory.Item
                            {
                                ReferenceId = x.ReferenceId,
                                ReferenceKey = x.ReferenceKey,
                                Name = x.Name,
                                IconUrl = x.IconUrl,
                                Fee = x.Fee,
                                StatusId = x.StatusId,
                                ModifyDate = x.ModifyDate,
                                ModifyByDisplayName = x.ModifyByDisplayName,
                            }).ToList();
                        _AllCats.Add(new OCategory.AllCats
                        {
                            ReferenceId = RootCategory.ReferenceId,
                            ReferenceKey = RootCategory.ReferenceKey,
                            Name = RootCategory.Name,
                            IconUrl = RootCategory.IconUrl,
                            
                            Fee   = RootCategory.Fee,
                            StatusId = RootCategory.StatusId,
                            ModifyDate = RootCategory.ModifyDate,
                            ModifyByDisplayName = RootCategory.ModifyByDisplayName,
                            Items = SubCats
                        });
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AllCats, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetDealsCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }


    }
}
