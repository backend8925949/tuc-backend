//==================================================================================
// FileName: TUCDealPromotionResource.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿
namespace HCore.TUC.Plugins.Deals.Resource
{
    public class TUCDealPromotionResource
    {
        internal const string MD0500 = "MD0500";
        internal const string MD0500M = "Internal server error occured. Please try after some time";

        internal const string MD0200 = "MD0200";
        internal const string MD0200M = "Results loaded";

        internal const string MD0404 = "MD0404";
        internal const string MD0404M = "Details not found. It might be removed or not available at the moment";

        internal const string MD0201 = "MD0201";
        internal const string MD0201M = "Deal Promotion updated successfully";

        internal const string MD0202 = "MD0202";
        internal const string MD0202M = "Deal Promotion Saved successfully";

        internal const string MD0203 = "MD0203";
        internal const string MD0203M = "Deal promotion Removed successfully";

        internal const string MDREF = "MDREF";
        internal const string MDREFM = "Reference id missing";

        internal const string MDREFKEY = "MDREFKEY";
        internal const string MDREFKEYM = "Reference key missing";

        internal const string MDINSTATUS = "MDINSTATUS";
        internal const string MDINSTATUSM = "Invalid Status Code";

        internal const string MDPROMOIMG = "MDPROMOIMG";
        internal const string MDPROMOIMGM = "Image required";

        internal const string MDEXIST = "MDEXIST";
        internal const string MDEXISTM = "Url Already Exists";

        internal const string MD0204 = "MD0204";
        internal const string MD0204M = "Invalid deal details";

        internal const string MDTYPECODE = "MDTYPE";
        internal const string MDTYPECODEM = "Type Id Required";

        internal const string MDTYPEID = "MDTYPEID";
        internal const string MDTYPEIDM = "Invalid Type Id";

        internal const string MD0205 = "MD0205";
        internal const string MD0205M = "Deal promotion already removed";

        internal const string MD0206 = "MD0206";
        internal const string MD0206M = "Deal already removed from promotion";

        internal const string MD0207 = "MD0207";
        internal const string MD0207M = "Image required for adding promotion";

        internal const string MD0208 = "MD0208";
        internal const string MD0208M = "Start date required";

        internal const string MD0209 = "MD0209";
        internal const string MD0209M = "End date required";

        internal const string MD0210 = "MD0210";
        internal const string MD0210M = "Image not found";

        internal const string MD0211 = "MD0211";
        internal const string MD0211M = "Promotion image removed successfully";

        internal const string MD0212 = "MD0212";
        internal const string MD0212M = "Promotion Image not found";

        internal const string MD0213 = "MD0213";
        internal const string MD0213M = "Deal promotion end date must be less than or equal to the end date of the deal";

        internal const string MD0214 = "MD0214";
        internal const string MD0214M = "You can not promote this deal as this deal is not available at the moment.";
    }
}
