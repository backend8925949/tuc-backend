//==================================================================================
// FileName: TUCDealResource.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
namespace HCore.TUC.Plugins.Deals.Resource
{
    internal class TUCDealResource
    {
        //internal const string HCD0500 = "HCD0500";
        //internal const string HCD0500M = "Internal server error occured. Please try after some time";

        //internal const string HCD0200 = "HCD0200";
        //internal const string HCD0200M = "Results loaded";

        //internal const string HCD0404 = "HCD0404";
        //internal const string HCD0404M = "Details not found. It might be removed or not available at the moment";

        //internal const string HCD0201 = "HCD0201";
        //internal const string HCD0201M = "Details updated successfully";

        //internal const string HCDREF = "HCDREF";
        //internal const string HCDREFM = "Reference id missing";

        //internal const string HCDREFKEY = "HCDREFKEY";
        //internal const string HCDREFKEYM = "Reference key missing";

        //internal const string HCDACCREF = "ACCREF";
        //internal const string HCDACCREFM = "Account reference missing";

        //internal const string HCDACCREFKEY = "HCDACCREFKEY";
        //internal const string HCDACCREFKEYM = "Account reference key missing";

        //internal const string HCDSTATUS = "HCDSTATUS";
        //internal const string HCDSTATUSM = "Status code required";
        //internal const string HCDINSTATUS = "HCDINSTATUS";
        //internal const string HCDINSTATUSM = "Invalid status code";

        //internal const string HCP0101 = "HCP0101";
        //internal const string HCP0101M = "Deal saved successfully";
        //internal const string HCP0102 = "HCP0102";
        //internal const string HCP0102M = "Deal title required";
        //internal const string HCP0103 = "HCP0103";
        //internal const string HCP0103M = "Deal status  required";
        //internal const string HCP0104 = "HCP0104";
        //internal const string HCP0104M = "Image deleted successfully";
        //internal const string HCP0105 = "HCP0105";
        //internal const string HCP0105M = "Operation failed. Delete operation only allowed in draft mode";
        //internal const string HCP0106 = "HCP0106";
        //internal const string HCP0106M = "Deal deleted successfully";
        //internal const string HCP0107 = "HCP0107";
        //internal const string HCP0107M = "Deal status updated successfully";
        //internal const string HCP0108 = "HCP0108";
        //internal const string HCP0108M = "logged";
        //internal const string HCP0109 = "HCP0109";
        //internal const string HCP0109M = "Deal code generated";
        //internal const string HCP0110 = "HCP0110";
        //internal const string HCP0110M = "Deal payment initialized";

        //internal const string HCP0111 = "HCP0111";
        //internal const string HCP0111M = "Payment source required";
        //internal const string HCP0112 = "HCP0112";
        //internal const string HCP0112M = "Invalid payment source";
        //internal const string HCP0113 = "HCP0113";
        //internal const string HCP0113M = "Unable to start deal purchase. Please try after some time";
        //internal const string HCP0114 = "HCP0114";
        //internal const string HCP0114M = "Transaction already processed";
        //internal const string HCP0115 = "HCP0115";
        //internal const string HCP0115M = "Deal must be published to set as flash deal";
        //internal const string HCP0116 = "HCP0116";
        //internal const string HCP0116M = "Deal Marked as flash deal";
        //internal const string HCP0117 = "HCP0117";
        //internal const string HCP0117M = "Flash deal already disabled";
        //internal const string HCP0118 = "HCP0118";
        //internal const string HCP0118M = "Flash deal removed";
        //internal const string HCP0119 = "HCP0118";
        //internal const string HCP0119M = "Deal already marked as flash deal";
        //internal const string HCP0120 = "HCP0120";
        //internal const string HCP0120M = "Deal already removed from flash deal";

        //internal const string HCP0121 = "HCP0121";
        //internal const string HCP0121M = "Deal code missing";
        //internal const string HCP0122 = "HCP0122";
        //internal const string HCP0122M = "Invalid deal code";
        //internal const string HCP0123 = "HCP0123";
        //internal const string HCP0123M = "Deal code already used";
        //internal const string HCP0124 = "HCP0124";
        //internal const string HCP0124M = "Deal code expired";
        //internal const string HCP0125 = "HCP0125";
        //internal const string HCP0125M = "Deal code blocked";
        //internal const string HCP0126 = "HCP0126";
        //internal const string HCP0126M = "Invalid deal code";
        //internal const string HCP0127 = "HCP0127";
        //internal const string HCP0127M = "Deal cannot be redeemed at current time. Check for deal redeem shedule";
        //internal const string HCP0128 = "HCP0128";
        //internal const string HCP0128M = "Deal cannot be redeemed at current time. Check for deal redeem shedule";
        //internal const string HCP0129 = "HCP0128";
        //internal const string HCP0129M = "Deal cannot be redeemed at current time. Check for deal redeem shedule";
        //internal const string HCP0130 = "HCP0130";
        //internal const string HCP0130M = "Deal validated";

        //internal const string HCP0131 = "HCP0131";
        //internal const string HCP0131M = "Deal code redeemed";
        //internal const string HCP0132 = "HCP0132";
        //internal const string HCP0132M = "Details loaded";
        //internal const string HCP0133 = "HCP0133";
        //internal const string HCP0133M = "You have reached maximum limit for purchase of this code";
        //internal const string HCP0134 = "HCP0134";
        //internal const string HCP0134M = "Deal reached maximum limit for purchase. Please check after some time";
        //internal const string HCP0135 = "HCP0135";
        //internal const string HCP0135M = "Deal maximum purchase   limit reached for day. Please try tommorow ";
        //internal const string HCP0136 = "HCP0136";
        //internal const string HCP0136M = "Deal code is not valid for this merchant";
        //internal const string HCP0137 = "HCP0137";
        //internal const string HCP0137M = "Merchant account is not active. Please contact support";
        //internal const string HCP0138 = "HCP0138";
        //internal const string HCP0138M = "Your account balance is low. Buy more points to purchase this deal";
        //internal const string HCP0139 = "HCP0138";
        //internal const string HCP0139M = "Your account is not active. Please contact support to activate account";
        //internal const string HCP0140 = "HCP0140";
        //internal const string HCP0140M = "Deal must be published for sending push notifications";

        //internal const string HCP0141 = "HCP0141";
        //internal const string HCP0141M = "Notification sent successfully";

        //internal const string HCP0142 = "HCP0142";
        //internal const string HCP0142M = "Deal title already exists. Please update existing deal or use new title for deal";

        //internal const string HCP0143 = "HCP0143";
        //internal const string HCP0143M = "Deal titles required";
        //internal const string HCP0144 = "HCP0144";
        //internal const string HCP0144M = "Deal comment required";
        //internal const string HCP0145 = "HCP0145";
        //internal const string HCP0145M = "Comment added successfully";
        //internal const string HCP0146 = "HCP0146";
        //internal const string HCP0146M = "Mobile number required";
        //internal const string HCP0147 = "HCP0147";
        //internal const string HCP0147M = "Operation failed. Deal code used or expired";
        //internal const string HCP0148 = "HCP0148";
        //internal const string HCP0148M = "Operation failed. Invalid account";
        //internal const string HCP0149 = "HCP0149";
        //internal const string HCP0149M = "Deal code shared successfully";

        //internal const string HCP0150 = "HCP0150";
        //internal const string HCP0150M = "Unable to create customer profile. Please try after some time";
        //internal const string HCP0151 = "HCP0151";
        //internal const string HCP0151M = "Deal cannot be shared to same mobile number";

        //internal const string HCP0152 = "HCP0152";
        //internal const string HCP0152M = "Minimum 1 deal image is required for saving deal";

        //internal const string HCP0153 = "HCP0153";
        //internal const string HCP0153M = "Deal code required";
        //internal const string HCP0154 = "HCP0154";
        //internal const string HCP0154M = "Deal code validated successfully";
        //internal const string HCP0155 = "HCP0155";
        //internal const string HCP0155M = "Deal code validated successfully";
        //internal const string HCP0156 = "HCP0156";
        //internal const string HCP0156M = "Deal image required";
        //internal const string HCP0157 = "HCP0157";
        //internal const string HCP0157M = "Invalid deal status";
        //internal const string HCP0158 = "HCP0158";
        //internal const string HCP0158M = "Deal approved successfully";
        //internal const string HCP0159 = "HCP0159";
        //internal const string HCP0159M = "Comment required";

        //internal const string HCP0160 = "HCP0160";
        //internal const string HCP0160M = "Deal rejected successfully";
        //internal const string HCP0161 = "HCP0161";
        //internal const string HCP0161M = "Deal duplicated successfully";



        //internal const string HCP0162 = "HCP0162";
        //internal const string HCP0162M = "Caetgory name required";
        //internal const string HCP0163 = "HCP0163";
        //internal const string HCP0163M = "Category saved successfully";
        //internal const string HCP0164 = "HCP0164";
        //internal const string HCP0164M = "Category id already exists";
        //internal const string HCP0165 = "HCP0165";
        //internal const string HCP0165M = "Category updated successfully";
        //internal const string HCP0166 = "HCP0166";
        //internal const string HCP0166M = "Operation failed. Deals associated with category";
        //internal const string HCP0167 = "HCP0167";
        //internal const string HCP0167M = "Category deleted successfully";
        //internal const string HCP0168 = "HCP0168";
        //internal const string HCP0168M = "Category id required";
        //internal const string HCP0169 = "HCP0169";
        //internal const string HCP0169M = "Category key required";
        //internal const string HCP0170 = "HCP0170";
        //internal const string HCP0170M = "Name required";
        //internal const string HCP0171 = "HCP0171";
        //internal const string HCP0171M = "Invalid category";
        //internal const string HCP0172 = "HCP0172";
        //internal const string HCP0172M = "Subcategory updated successfully";
        //internal const string HCP0173 = "HCP0173";
        //internal const string HCP0173M = "Name already exists";
        //internal const string HCP0174 = "HCP0174";
        //internal const string HCP0174M = "Subcategory updated successfully";
        //internal const string HCP0175 = "HCP0175";
        //internal const string HCP0175M = "Subcategory deleted succssfully";
        //internal const string HCP0176 = "HCP0176";
        //internal const string HCP0176M = "Subcategories required";
        //internal const string HCP0177 = "HCP0177";
        //internal const string HCP0177M = "Subcategories saved successfully";
        //internal const string HCP0178 = "HCP0178";
        //internal const string HCP0178M = "Commission required";
        //internal const string HCP0179 = "HCP0179";
        //internal const string HCP0179M = "Invalid category id";
        //internal const string HCP0180 = "HCP0180";
        //internal const string HCP0180M = "XXXXXXXXX";


        //internal const string HCP0181 = "HCP0181";
        //internal const string HCP0181M = "Flash Deal Updated successfully";
        //internal const string HCP0182 = "HCP0182";
        //internal const string HCP0182M = "Usage type code required";
        //internal const string HCP0183 = "HCP0183";
        //internal const string HCP0183M = "Subcategory required";
        //internal const string HCP0184 = "HCP0184";
        //internal const string HCP0184M = "Invalid category";


        //internal const string HCP0185 = "HCP0185";
        //internal const string HCP0185M = "Operation failed. Image used as primary image for deal.";

        //internal const string HCP0186 = "HCP0186";
        //internal const string HCP0186M = "Bookmark saved successfully.";
        //internal const string HCP0187 = "HCP0187";
        //internal const string HCP0187M = "Bookmark removed successfully.";
        //internal const string HCP0188 = "HCP0188";
        //internal const string HCP0188M = "Deal key required.";
        //internal const string HCP0189 = "HCP0189";
        //internal const string HCP0189M = "Invalid deal id.";
        //internal const string HCP0190 = "HCP0190";
        //internal const string HCP0190M = "Invalid account id.";
    }
}
