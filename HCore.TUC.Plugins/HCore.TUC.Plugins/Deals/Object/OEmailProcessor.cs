﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Deals.Object
{
    public class OEmailProcessor
    {
        public class ConfirmOrder
        {
            public long? ReferenceId { get; set; }
            public long? CustomerId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? OrderId { get; set; }
            public string? OrderReference { get; set; }
            public string? UserDisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? DeliveryDate { get; set; }
            public string? DealTitle { get; set; }
            public string? MerchantDisplayName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class CancelOrder
        {
            public long? ReferenceId { get; set; }
            public long? CustomerId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DealTitle { get; set; }
            public string? MerchantDisplayName { get; set; }
            public int? StatusId { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class DealCodeExpire
        {
            public string? UserDisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? TermsUrl { get; set; }
            public string? PolicyUrl { get; set; }
            public string? PanelUrl { get; set; }
            public string? DealCodes { get; set; }
            public long? AccountId { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class DealSummaryReports
        {
            public DateTime? DateTime { get; set; }
            public long? ProductDealCount { get; set; }
            public long? ServiceDealCount { get; set; }
            public long? ActiveMerchantCount { get; set; }
        }

        public class DealExpiryDetails
        {
            public long? DealCount { get; set; }
            public string? ExpiryDate { get; set; }
            public Stream? Attachment { get; set; }
        }


        public class DealCodeDetails
        {
            public string? ExpiryDate { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? MerchantAddress { get; set; }
            public string? MerchantMobileNumber { get; set; }
            public string? DealCode { get; set; }
        }

        public class ProductReview
        {
            public string? CustomerDisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? DealTitle { get; set; }
            public string? DealImageUrl { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? PanelUrl { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class OrderNotification
        {
            public string? MerchantDisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? OrderNumber { get; set; }
            public string? DealCode { get; set; }
            public string? DeliveryAddress { get; set; }
            public string? DealTitle { get; set; }
            public string? DealImageUrl { get; set; }
            public string? PanelUrl { get; set; }
            public int? Quantity { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class DealPurchaseNotification
        {
            public string? MerchantDisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? OrderNumber { get; set; }
            public string? DealTitle { get; set; }
            public string? DealImageUrl { get; set; }
            public string? PanelUrl { get; set; }
            public int? Quantity { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}

