//==================================================================================
// FileName: ODealPromotion.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Deals.Object
{
    public class ODealPromotion
    {
        public class Image
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class SaveDealPromotion
        {
            public class Request
            {
                public long DealId { get; set; }
                public string? DealKey { get; set; }
                public string? TypeCode { get; set; }
                public string? Url { get; set; }
                public string? Locations { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string? StatusCode { get; set; }
                public long SyncTime { get; set; }
                public string? ImageReference { get; set; }
                public OStorageContent ImageContent { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public int ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class UpdateDealPromotion
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long DealId { get; set; }
            public string? DealKey { get; set; }
            public string? TypeCode { get; set; }
            public string? ImageStorageUrl { get; set; }
            public string? Url { get; set; }
            public string? Locations { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string? StatusCode { get; set; }
            public string? ImageReference { get; set; }
            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class DealPromotionList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? CountryId { get; set; }
            public string? CountryCode { get; set; }
            public string? CountryName { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }

            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }
            public string? ImageUrl { get; set; }
            public long? ImageStorageId { get; set; }
            public string? Url { get; set; }
            public int? TypeId { get; set; }
            public string? Locations { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeSystemName { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
        }

        public class DealPromotionDetails
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryCode { get; set; }
            public string? CountryName { get; set; }

            public long DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public double? SellingPrice { get; set; }
            public double? ActualPrice { get; set; }
            public DateTime? DealStartDate { get; set; }
            public DateTime? DealEndDate { get; set; }

            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }

            public string? Locations { get; set; }
            public string? Url { get; set; }
            public string? ImageUrl { get; set; }

            public int? TypeId { get; set; }
            public string? TypeName { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeSystemName { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }


            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long ModifyById { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }

}
