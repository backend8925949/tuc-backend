//==================================================================================
// FileName: ODeal.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Plugins.Deals.Object
{
    public class ODealOperation
    {
        public class DealMerchantList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }

            public long? Deals { get; set; }
            public double? Rating { get; set; }

            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
        }
        public class DealList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Title { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }


            public long? CategoryId { get; set; }
            public string? CategoryName { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }

            public double? Amount { get; set; }

            public string? ImageUrl { get; set; }

            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }

            public int? DeliveryTypeId { get; set; }
            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public long? MerchantAddressId { get; set; }
            public string? MerchantAddresskey { get; set; }

            public long? Likes { get; set; }
            public long? DisLikes { get; set; }
            public long? Views { get; set; }
            public long? Purchase { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountIconUrl { get; set; }

            public double? AccountRating { get; set; }

            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? Terms { get; set; }

            public string? UsageInformation { get; set; }
            public string? RedeemInstruction { get; set; }

            public double? DiscountPercentage { get; set; }

            public DateTime? EndDate { get; set; }


            public long? CodeValidityDays { get; set; }
            public DateTime? CodeValidityStartDate { get; set; }
            public DateTime? CodeValidityEndDate { get; set; }

            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }


            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? TotalAmount { get; set; }
            public long? MaximumUnitSale { get; set; }
            public long? MaximumUnitSalePerDay { get; set; }

            public long? TotalPurchaseRemaining { get; set; }

            public string? ImageUrl { get; set; }

            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }

            public int? DeliveryTypeId { get; set; }
            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public long? MerchantAddressId { get; set; }
            public string? MerchantAddresskey { get; set; }

            public long? Views { get; set; }
            public long? Likes { get; set; }
            public long? DisLikes { get; set; }
            public string? CategoryName { get; set; }

            public long? TotalPurchase { get; set; }

            public bool IsFlashDeal { get; set; }
            public double? FlashDealAmount { get; set; }
            public DateTime? FlashDealStartDate { get; set; }
            public DateTime? FlashDealEndDate { get; set; }

            public List<Gallery>? Images { get; set; }
            public List<Location>? Locations { get; set; }
            public int StatusId { get; set; }
        }

        public class Gallery
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ImageUrl { get; set; }
            public int IsDefault { get; set; }
        }
        public class Location
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
            public string? ContactNumber { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class DealCode
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? MobileNumber { get; set; }
                public List<DealCodes>? DealCodes { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class DealCodes
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }

            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long DealReferenceId { get; set; }
                public string? DealReferenceKey { get; set; }

                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountDisplayName { get; set; }
                public string? AccountMobileNumber { get; set; }
                public string? AccountIconUrl { get; set; }


                public DateTime? UseDate { get; set; }
                public long? UseLocationId { get; set; }
                public string? UseLocationKey { get; set; }
                public string? UseLocationDisplayName { get; set; }
                public string? UseLocationAddress { get; set; }

                public string? ItemCode { get; set; }
                public int? ItemCount { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public double? ActualPrice { get; set; }
                public double? SellingPrice { get; set; }

                public int? Quantity { get; set; }

                public double? Amount { get; set; }
                public double? CommissionAmount { get; set; }
                public double? Charge { get; set; }
                public double? DeliveryCharge { get; set; }
                public double? TotalAmount { get; set; }


                public string? Title { get; set; }
                public string? Description { get; set; }
                public string? ImageUrl { get; set; }
                public string? UsageInformation { get; set; }
                public string? Terms { get; set; }
                public long? MerchantReferenceId { get; set; }
                public string? MerchantReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public string? MerchantContactNumber { get; set; }
                public string? MerchantEmail { get; set; }
                public string? RedeemInstruction { get; set; }
                public double? MerchantPaybleAmount { get; set; }
                public DateTime? CreateDate { get; set; }
                public DateTime? DealStartDate { get; set; }
                public DateTime? DealEndDate { get; set; }

                public long? CustomerId { get; set; }
                public string? CustomerKey { get; set; }
                public string? CustomerDisplayName { get; set; }
                public string? CustomerMobileNumber { get; set; }
                public string? CustomerIconUrl { get; set; }


                public long? SharedCustomerId { get; set; }
                public string? SharedCustomerKey { get; set; }
                public string? SharedCustomerDisplayName { get; set; }
                public string? SharedCustomerMobileNumber { get; set; }
                public string? SharedCustomerIconUrl { get; set; }

                public string? SecondaryCode { get; set; }
                public string? SecondaryCodeMessage { get; set; }

                public int? DealTypeId { get; set; }
                public List<DealCodeLocation>? Locations { get; set; }

                public long? ShipmentId { get; set; }
                public string? ShipmenKey { get; set; }

                internal long? FromAddressId { get; set; }
                internal long? ToAddressId { get; set; }

                public string? DealTypeCode { get; set; }
                public string? DeliveryTypeCode { get; set; }

                public Delivery? DeliveryDetails { get; set; }
                public PaymentDetails? PaymentDetails { get; set; }

                public long? TransactionId { get; set; }
            }

            public class DealCodeResponse
            {
                public Response? Response { get; set; }
            }


            public class Address
            {
                public string? Name { get; set; }
                public  string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }
                public  string? AddressLine1 { get; set; }
                public string? AddressLine2 { get; set; }
                public string? CityName { get; set; }
                public  string? StateName { get; set; }
                public string? CountryName { get; set; }
            }

            public class Delivery
            {
                public string? ShipmentId { get; set; }
                public string? OrderReference { get; set; }
                public string? TrackingUrl { get; set; }
                public string? TrackingNumber { get; set; }
                public DateTime? DeliveryDate { get; set; }
                public Carrier? Carrier { get; set; }
                public Address? FromAddress { get; set; }
                public Address? ToAddress { get; set; }

                public  int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }


            public class Carrier
            {
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? ContactNumber { get; set; }
                public string? IconUrl { get; set; }
            }

            public class PaymentDetails
            {
                public int? PaymentTypeId { get; set; }
                public string? PaymentTypeName { get; set; }
                public string? PaymentTypeCode { get; set; }
                public string? CardNumber { get; set; }
                public double? ItemAmount { get; set; }
                public double? DeliveryCharges { get; set; }
                public double? TotalAmount { get; set; }
                public int? ItemCount { get; set; }
            }


            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long Count { get; set; }
                public long DealId { get; set; }
                public long? ShipmentId { get; set; }
                public string? ShipmentKey { get; set; }

                public string? Title { get; set; }

                public long? MerchantId { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }

                public string? ItemCode { get; set; }

                public string? CategoryName { get; set; }

                public DateTime? EndDate { get; set; }

                public DateTime? UseDate { get; set; }

                public string? UseLocationDisplayName { get; set; }
                public string? UseLocationAddress { get; set; }

                public double? ActualPrice { get; set; }
                public double? SellingPrice { get; set; }

                public double? Amount { get; set; }
                public double? TotalAmount { get; set; }

                public string? ImageUrl { get; set; }
                public DateTime? DeliveryDate { get; set; }

                public DateTime? CreateDate { get; set; }

                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public string? StatusContent { get; set; }


                public long? CustomerId { get; set; }
                public string? CustomerKey { get; set; }
                public string? CustomerDisplayName { get; set; }
                public string? CustomerMobileNumber { get; set; }
                public string? CustomerIconUrl { get; set; }

                public int? DealTypeId { get; set; }

                public long? DealCodeId { get; set; }
                public string? DealCodeKey { get; set; }

                public string? DealTypeCode { get; set; }
                public string? DeliveryTypeCode { get; set; }

                public int? DealStatusId { get; set; }
                public string? DealStatusCode { get; set; }
                public string? DealStatusName { get; set; }
            }
        }
        public class DealCodeLocation
        {
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
            public string? ContactNumber { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
        }
        public class DealPayment
        {
            public class Initialize
            {
                public bool IsGuestUser { get; set; } = false;
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                public long TrackingId { get; set; }
                public string? TrackingKey { get; set; }

                public double DeliveryCharge { get; set; }
                public string? PaymentSource { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long DealId { get; set; }
                public string? DealKey { get; set; }
                public string? PaymentReference { get; set; }
                public long TransactionId { get; set; }

                public int? ItemCount { get; set; }

                public OAddress? AddressComponent { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Confirm
            {

                public bool IsGuestUser { get; set; } = false;
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }


                public long TrackingId { get; set; }
                public string? TrackingKey { get; set; }

                public string? PaymentSource { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long DealId { get; set; }
                public string? DealKey { get; set; }
                public string? PaymentReference { get; set; }
                public long TransactionId { get; set; }
                public string? DeliveryPartnerKey { get; set; }
                public int? CarrierId { get; set; }

                public double DeliveryEta { get; set; }

                public double Amount { get; set; }
                public double Charge { get; set; }
                public double DeliveryCharge { get; set; }
                public double TotalAmount { get; set; }

                public long? FromAddressId { get; set; }
                public string? FromAddressKey { get; set; }
                public long? ToAddressId { get; set; }
                public string? ToAddressKey { get; set; }

                public long? PromoCodeId { get; set; }
                public string? PromoCodeKey { get; set; }

                public int? ItemCount { get; set; }
                public string? RateId { get; set; }
                public string? RedisKey { get; set; }
                public string? KwikKey { get; set; }

                public OAddress? AddressComponent { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }
        public class DealPurchase
        {
            public class Initialize
            {
                public class Request
                {
                    public long DealId { get; set; }
                    public string? DealKey { get; set; }
                    public Customer? Customer { get; set; }
                    public OAddress? AddressComponent { get; set; }
                    public DeliveryOptions? Delivery { get; set; }
                    
                    //public double DeliveryCharge { get; set; }
                    //public string? PaymentSource { get; set; }
                    //public long ReferenceId { get; set; }
                    //public string? ReferenceKey { get; set; }
                    //public string? PaymentReference { get; set; }
                    //public long TransactionId { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public long DealId { get; set; }
                    public string? DealKey { get; set; }
                    public Customer? Customer { get; set; }
                    public OAddress? AddressComponent { get; set; }
                    public PaymentDetails? PaymentDetails { get; set; }
                    public long ToAddressId { get; set; }
                    public string? ToAddressKey { get; set; }
                }
            }
            public class DeliveryOptions
            {
                    public double DeliveryFee { get; set; } = 0;
                    public long? ShipmentId { get; set; }
                    public string? ShipmentKey { get; set; }
                    public long? ParcelId { get; set; }
                    public string? ParcelKey { get; set; }
                    public long? PackageId { get; set; }
                    public string? PackageKey { get; set; }
                    public Carrier? DeliveryPartner { get; set; }
                    public long? FromAddressId { get; set; }
                    public string? FromAddressKey { get; set; }
                    public string? FromAddressReference { get; set; }
                    public long? ToAddressId { get; set; }
                    public string? ToAddressKey { get; set; }
                    public string? ToAddressReference { get; set; }
            }
            public class Carrier
            {
                public string? ReferenceKey { get; set; }
                public int? CarrierId { get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
                public string? DeliveryTime { get; set; }
                public double DeliveryEta { get; set; }
                public double DeliveryCharge { get; set; }
                public string? RateId { get; set; }
            }
            public class PaymentDetails
            {
                public string? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public double Amount { get; set; }
                public double Fee { get; set; }
                public double DeliveryFee { get; set; }
                public double TotalAmount { get; set; }
            }
            public class Customer
            {
               public bool IsGuestUser { get; set; } = false;
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? Name { get; set; }
                public string? EmailAddress { get; set; }
                public string? MobileNumber { get; set; }
            }
            public class Confirm
            {
               public class Request
                {
                    public long DealId { get; set; }
                    public string? DealKey { get; set; }
                    public Customer? Customer { get; set; }
                    public OAddress? AddressComponent { get; set; }
                    public DeliveryOptions? Delivery { get; set; }
                    public PaymentDetails? PaymentDetails { get; set; }
                    //public long TrackingId { get; set; }
                    //public string? TrackingKey { get; set; }
                    //public double DeliveryCharge { get; set; }
                    //public string? PaymentSource { get; set; }
                    //public long ReferenceId { get; set; }
                    //public string? ReferenceKey { get; set; }
                    //public string? PaymentReference { get; set; }
                    //public long TransactionId { get; set; }
                    public OUserReference? UserReference { get; set; }
                    //public bool IsGuestUser { get; set; } = false;
                    //public long AccountId { get; set; }
                    //public string? AccountKey { get; set; }
                    //public long TrackingId { get; set; }
                    //public string? TrackingKey { get; set; }
                    //public string? PaymentSource { get; set; }
                    //public long ReferenceId { get; set; }
                    //public string? ReferenceKey { get; set; }
                    //public long DealId { get; set; }
                    //public string? DealKey { get; set; }
                    //public string? PaymentReference { get; set; }
                    //public long TransactionId { get; set; }
                    //public string? DeliveryPartnerKey { get; set; }
                    //public string? CarrierId { get; set; }
                    //public double DeliveryEta { get; set; }
                    //public double Amount { get; set; }
                    //public double Charge { get; set; }
                    //public double DeliveryCharge { get; set; }
                    //public double TotalAmount { get; set; }
                    //public long? FromAddressId { get; set; }
                    //public string? FromAddressKey { get; set; }
                    //public long? ToAddressId { get; set; }
                    //public string? ToAddressKey { get; set; }
                    //public OAddress AddressComponent { get; set; }
                    //public OUserReference? UserReference { get; set; }
                }
            }
        }

    }


    public class OFlashDeal
    {
        public class Manage
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long DealId { get; set; }
            public string? DealKey { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? Title { get; set; }
            public string? Description { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public long? Locations { get; set; }

            public long? MaximumUnitSale { get; set; }
            public double? Budget { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }

            public double? MerchantAmount { get; set; }

            public long TotalPurchase { get; set; }

            public string? ImageUrl { get; set; }

            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }


            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName { get; set; }


            public DateTime CreateDate { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }

    public class ODeal
    {

        public class ValidateTitle
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public List<DealTitle>? Titles { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }
        public class DealTitle
        {
            public string? Title { get; set; }
            public bool IsAvailable { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
        public class Overview
        {
            public long Running { get; set; } = 0;
            public long Upcoming { get; set; } = 0;
            public long Total { get; set; } = 0;
            public long Draft { get; set; } = 0;
            public long ApprovalPending { get; set; } = 0;
            public long Approved { get; set; } = 0;
            public long Published { get; set; } = 0;
            public long Paused { get; set; } = 0;
            public long Rejected { get; set; } = 0;
            public long Expired { get; set; } = 0;
            public bool AppDownloaded { get; set; } = false;
            public long SoldOut { get; set; } = 0;
            public long BestSelling { get; set; } = 0;
        }
        public class Manage
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long AccountId { get; set; }
                public string? AccountKey { get; set; }

                //public string? TypeCode { get; set; }
                public string? Title { get; set; }
                public string? TitleContent { get; set; }
                public int TitleTypeId { get; set; } = 1;
                public string? Description { get; set; }
                public string? Terms { get; set; }

                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }

                public string? CodeUsageTypeCode { get; set; }
                public string? UsageTypeCode { get; set; }
                public string? CodeUsageInformation { get; set; }
                public int? CodeValidityDays { get; set; }
                public int? CodeValidityHours { get; set; }
                //public DateTime? CodeValidityStartDate { get; set; }
                public DateTime? CodeValidityEndDate { get; set; }
                public double TUCPercentage { get; set; }
                public double ActualPrice { get; set; }
                public double SellingPrice { get; set; }

                //public string? DiscountTypeCode { get; set; }
                //public double DiscountAmount { get; set; }
                //public double DiscountPercentage { get; set; }

                //public double Amount { get; set; }
                //public double Charge { get; set; }
                //public double CommissionAmount { get; set; }
                //public double TotalAmount { get; set; }

                public long MaximumUnitSale { get; set; }
                //public long MaximumUnitSalePerDay { get; set; }
                public long MaximumUnitSalePerPerson { get; set; }
                //public string? CategoryKey { get; set; }
                public string? SubCategoryKey { get; set; }
                public string? StatusCode { get; set; }
                public string? DealTypeCode { get; set; }
                public string? DeliveryTypeCode { get; set; }
                //public string? SettlementTypeCode { get; set; }
                public bool SendNotification { get; set; }
                public Packaging? Packaging { get; set; }
                public OUserReference? UserReference { get; set; }
                public List<Gallary>? Images { get; set; }
                public List<Location>? Locations { get; set; }
                public List<Shedule>? Shedule { get; set; }
            }

            public class UploadImageRequest
            {
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public List<Gallary>? Images { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Packaging
            {
                public double? Height { get; set; }
                public double? Length { get; set; }
                public string? Name { get; set; }
                public string? Size_unit { get; set; }
                public string? Type { get; set; }
                public double? Width { get; set; }
                public double Weight { get; set; }
                public string? Weight_unit { get; set; }
                public string? ParcelDescription { get; set; }
                public string? PackagingId { get; set; }
                public string? Reference { get; set; }
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }

            public class Gallary
            {
                public string? reference { get; set; }
                public long referenceId { get; set; }
                public OStorageContent? ImageContent { get; set; }
                public int IsDefault { get; set; }
            }
            public class Location
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
            public class Shedule
            {
                public string? TypeCode { get; set; }
                public int? StartHour { get; set; }
                public int? EndHour { get; set; }
                public int? DayOfWeek { get; set; }
            }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountAddress { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? Title { get; set; }
            public string? Description { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public long? Locations { get; set; }

            public string? TitleContent { get; set; }
            public int? TitleTypeId { get; set; }

            public long? Views { get; set; }

            public bool? IsFlashDeal { get; set; }
            public bool? IsDealPromoted { get; set; }
            public string? DealPromotionLocations { get; set; }
            public bool? IsDealPromotedActive { get; set; }

            //public bool? IsDealPromotionStopped { get; set; }

            public double? MerchantAmount { get; set; }

            public long? MaximumUnitSale { get; set; }
            public double? Budget { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }

            public long TotalPurchase { get; set; }
            public long? TotalAvailable { get; set; }

            public string? ImageUrl { get; set; }

            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }

            public int? DeliveryTypeId { get; set; }
            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public long? MerchantAddressId { get; set; }
            public string? MerchantAddresskey { get; set; }


            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName { get; set; }


            public DateTime CreateDate { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? ApproverId { get; set; }
            public DateTime? ApprovedDate { get; set; }
            public string? ApproverComment { get; set; }

            public long? RejectedById { get; set; }
            public DateTime? RejectedDate { get; set; }
            public string? RejectionComment { get; set; }

            public string? Slug { get; set; }
        }

        public class MerchantList
        {


            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }
            public int TotalDeals { get; set; }
            public int RunningDeals { get; set; }
            public int UpcomingDeals { get; set; }
            public int SoldDeals { get; set; }
            public int RedeemedDeals { get; set; }
            public string? StatusCode { get; set; }

        }
        public class CustomerList
        {

            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? MobileNo { get; set; }
            public int PurchasedDeals { get; set; }
            public int Used { get; set; }
            public int Unused { get; set; }
            public int Expired { get; set; }
            public string? LastPurchasedDate { get; set; }
            public string? IconUrl { get; set; }
            public string? StatusCode { get; set; }

        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? TitleContent { get; set; }
            public int? TitleTypeId { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountEmailAddress { get; set; }
            public string? AccountIconUrl { get; set; }


            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? Terms { get; set; }


            public string? SettlmentTypeCode { get; set; }
            public string? SettlmentTypeName { get; set; }

            public string? DiscountTypeCode { get; set; }
            public string? DiscountTypeName { get; set; }

            public int? UsageTypeId { get; set; }
            public string? UsageTypeCode { get; set; }
            public string? UsageTypeName { get; set; }
            public string? UsageInformation { get; set; }

            public double? MerchantAmount { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }


            public long? CodeValidityDays { get; set; }
            public DateTime? CodeValidityStartDate { get; set; }
            public DateTime? CodeValidityEndDate { get; set; }

            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }

            public double? DiscountAmount { get; set; }
            public double? DiscountPercentage { get; set; }

            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? CommissionAmount { get; set; }
            public double? CommissionPercentage { get; set; }
            public double? TotalAmount { get; set; }
            public long? MaximumUnitSale { get; set; }
            public long? MaximumUnitSalePerDay { get; set; }

            public string? ImageUrl { get; set; }

            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }

            public int? DeliveryTypeId { get; set; }
            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public long? Views { get; set; }
            public int? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public int? SubCategoryId { get; set; }
            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName { get; set; }
            public double? SubCategoryFee { get; set; }


            public long? MaximumUnitSalePerPerson { get; set; }


            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public double? Budget { get; set; }
            public long TotalPurchase { get; set; }
            public int? AvailableQuantity { get; set; }

            public long? MerchantAddressId { get; set; }
            public string? MerchantAddresskey { get; set; }

            public bool IsFlashDeal { get; set; }
            public long FlashDealId { get; set; }
            public string? FlashDealKey { get; set; }
            public DateTime? FlashDealStartDate { get; set; }
            public DateTime? FlashDealEndDate { get; set; }

            public bool IsDealPromoted { get; set; }
            public long DealPromotionId { get; set; }
            public string? DealPromotionKey { get; set; }
            public DateTime? DealPromotionStartDate { get; set; }
            public DateTime? DealPrmotionEndDate { get; set; }
            public string? DealPromotionStatus { get; set; }
            public string? DealPromotionImageUrl { get; set; }
            public string? DealPromotionLocations { get; set; }

            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public DateTime? ApprovalDate { get; set; }
            public string? ApproverComment { get; set; }
            public long? ApproverId { get; set; }
            public string? ApproverKey { get; set; }
            public string? ApproverDisplayName { get; set; }

            public double? AverageAccountRating { get; set; }
            public long AccountRatingCount { get; set; }

            public Packaging? Packaging { get; set; }
            public PackagingCategory? PackagingCategory { get; set; }
            public Rating? Ratings { get; set; }
            public List<Gallery>? Images { get; set; }
            public List<Location>? Locations { get; set; }
            public List<Shedule>? Shedule { get; set; }

            public string? Slug { get; set; }
        }
        public class PackagingCategory
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public double? Height { get; set; }
            public double? Length { get; set; }
            public string? Name { get; set; }
            public string? Size_unit { get; set; }
            public string? Type { get; set; }
            public double? Width { get; set; }
            public double? Weight { get; set; }
            public string? Weight_unit { get; set; }
            public string? PackagingId { get; set; }
            public string? ParcelDescription { get; set; }
        }
        public class Packaging
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public double? Height { get; set; }
            public double? Length { get; set; }
            public string? Name { get; set; }
            public string? Size_unit { get; set; }
            public string? Type { get; set; }
            public double? Width { get; set; }
            public double? Weight { get; set; }
            public string? Weight_unit { get; set; }
            public string? PackagingId { get; set; }
            //public int? CategoryId { get; set; }
            //public string? CategoryName { get; set; }
            //public int? ParentCategoryId { get; set; }
            //public string? ParentCategoryName { get; set; }
        }
        public class Shedule
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }
            public int? StartHour { get; set; }
            public int? EndHour { get; set; }
            public int? DayOfWeek { get; set; }
        }
        public class Rating
        {
            public double? AverageDealRating { get; set; }
            public long? TotalDealRating { get; set; }
            public long? Rating1 { get; set; }
            public long? Rating2 { get; set; }
            public long? Rating3 { get; set; }
            public long? Rating4 { get; set; }
            public long? Rating5 { get; set; }
        }
        public class Gallery
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ImageUrl { get; set; }
            public int IsDefault { get; set; }
        }
        public class Location
        {
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
        }

        public class DealLocation
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long DealId { get; set; }
            public string? DealKey { get; set; }
            public string? Title { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }
            public string? ImageUrl { get; set; }
            public DateTime? DealEndDate { get; set; }
            public DateTime? DealCreateDate { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }
            public string? Address { get; set; }

            public long? LocationId { get; set; }
            public string? LocationKey { get; set; }
            public string? LocationDisplayName { get; set; }
            public string? LocationAddress { get; set; }

            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
        }
    }
    public class ODealReview
    {
        public class Update
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? SystemComment { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }
            public int? Rating { get; set; }
            public int? IsWorking { get; set; }
            public string? Review { get; set; }
            public string? Comment { get; set; }
            public long? StatusId { get; set; }
            public DateTime? CreateDate { get; set; }
        }
    }
    public class ODealCode
    {

        public class ValidateCode
        {
            public class Details
            {
                public double? Amount { get; set; }
                public DateTime? PurchaseDate { get; set; }
                public DateTime? ExpiaryDate { get; set; }
                public DateTime? RedeemDate { get; set; }

                public long? RedeemLocationId { get; set; }
                public string? RedeemLocationKey { get; set; }
                public string? RedeemLocationName { get; set; }
                public string? RedeemLocationAddress { get; set; }

                public string? ItemCode { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public int? Quantity { get; set; }
                public double? DealCodeAmount { get; set; }
                public double? PaybleAmount { get; set; }

                public Merchant? Merchant { get; set; }
                public Customer? Customer { get; set; }
                public Deal? Deal { get; set; }
            }

            public class Merchant
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? IconUrl { get; set; }
            }

            public class Customer
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }
                public string? IconUrl { get; set; }
            }

            public class Deal
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Title { get; set; }
                public string? ImageUrl { get; set; }
            }
        }

        public class Overview
        {
            public long? MaxLimit { get; set; } = 0;
            public long Total { get; set; } = 0;
            public long Customer { get; set; } = 0;
            public long Unused { get; set; } = 0;
            public long Used { get; set; } = 0;
            public long Expired { get; set; } = 0;
            public double? TotalAmount { get; set; } = 0;
            public double? UsedAmount { get; set; } = 0;
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }

            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? ImageUrl { get; set; }

            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public long? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }
            public long? DeliveryTypeId { get; set; }
            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public long? PaymentReference { get; set; }

            public long? ToAccountId { get; set; }
            public string? ToAccountKey { get; set; }
            public string? ToAccountDisplayName { get; set; }
            public string? ToAccountMobileNumber { get; set; }
            public string? ToAccountIconUrl { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountAddress { get; set; }
            public string? AccountIconUrl { get; set; }
            public double? MerchantAmount { get; set; }
            public string? DealCodeEnd { get; set; }
            public double? Amount { get; set; }

            public DateTime? DealPurchaseDate { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public DateTime? LastUseDate { get; set; }
            public long? LastUseLocationId { get; set; }
            public string? LastUseLocationKey { get; set; }
            public string? LastUseLocationDisplayName { get; set; }
            public string? LastUseLocationAddress { get; set; }
            public string? LastUseLocationSourceName { get; set; }
            public string? LastUseLocationSourceCode { get; set; }

            public long? TerminalReferenceId { get; set; }
            public string? TerminalReferenceKey { get; set; }
            public string? TerminalId { get; set; }
            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierDisplayName { get; set; }

            public int? Quantity { get; set; }
            public double? DealCodeAmount { get; set; }
            public double? PaybleAmount { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? PartnerId { get; set; }
            public string? PartnerKey { get; set; }
            public string? PartnerDisplayName { get; set; }
            public string? PartnerMobileNumber { get; set; }
            public double? PartnerComissionAmount { get; set; }

            public long? DeliveryPartnerId { get; set; }
            public string? DeliveryPartnerKey { get; set; }
            public string? DeliveryPartnerDisplayName { get; set; }
            public string? DeliveryPartnerMobileNumber { get; set; }
            public string? DeliveryPartnerEmailAddress { get; set; }

            public long? CarrierId { get; set; }
            public string? CarrierKey { get; set; }
            public string? CarrierName { get; set; }
        }


        public class ListPanel
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountIconUrl { get; set; }

            public string? DealCodeEnd { get; set; }
            public double? Amount { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public DateTime? LastUseDate { get; set; }
            public long? LastUseLocationId { get; set; }
            public string? LastUseLocationKey { get; set; }
            public string? LastUseLocationDisplayName { get; set; }
            public string? LastUseLocationAddress { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }


    public class ODealRedeem
    {
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountIconUrl { get; set; }


            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? Terms { get; set; }


            public string? UsageTypeCode { get; set; }
            public string? UsageTypeName { get; set; }
            public string? UsageInformation { get; set; }


            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }


            public long? CodeValidityDays { get; set; }
            public DateTime? CodeValidityStartDate { get; set; }
            public DateTime? CodeValidityEndDate { get; set; }

            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }

            public double? DiscountAmount { get; set; }
            public double? DiscountPercentage { get; set; }

            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? CommissionAmount { get; set; }
            public double? TotalAmount { get; set; }
            public long? MaximumUnitSale { get; set; }
            public long? MaximumUnitSalePerDay { get; set; }

            public string? ImageUrl { get; set; }

            public long? Views { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName { get; set; }


            public string? MaximumUnitSalePerPerson { get; set; }


            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public double? Budget { get; set; }
            public long TotalPurchase { get; set; }

            public bool IsFlashDeal { get; set; }
            public long FlashDealId { get; set; }
            public string? FlashDealKey { get; set; }
            public DateTime? FlashDealStartDate { get; set; }
            public DateTime? FlashDealEndDate { get; set; }


            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public List<Gallery>? Images { get; set; }
            public List<Location>? Locations { get; set; }
        }
        public class Gallery
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ImageUrl { get; set; }
            public int IsDefault { get; set; }
        }
        public class Location
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? Address { get; set; }
        }
    }

    public class DealCodeStatus
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? StatusCode { get; set; }
        public string? ReferenceCode { get; set; }
        public OUserReference? UserReference { get; set; }
    }

    public class CreateDeal
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }

            public string? Title { get; set; }
            public string? TitleContent { get; set; }
            public int? TitleTypeId { get; set; } = 1;
            public string? Description { get; set; }
            public string? Terms { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }

            public string? CodeUsageTypeCode { get; set; }
            public string? UsageTypeCode { get; set; }
            public string? CodeUsageInformation { get; set; }
            public int? CodeValidityDays { get; set; }
            public int? CodeValidityHours { get; set; }
            public DateTime? CodeValidityEndDate { get; set; }
            public double? TUCPercentage { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }
            public string? SettlementTypeCode { get; set; }
            public long? MaximumUnitSale { get; set; }
            public long? MaximumUnitSalePerPerson { get; set; }
            public long? SubCategoryId { get; set; }
            public string? SubCategoryKey { get; set; }
            public long? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? StatusCode { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DeliveryTypeCode { get; set; }
            public bool SendNotification { get; set; }
            public bool IsAllLocations { get; set; }
            public Packaging? Packaging { get; set; }
            public List<ODeal.Manage.Gallary>? Images { get; set; }
            public List<Location>? Locations { get; set; }
            public List<Shedule>? Shedule { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Packaging
        {
            public double Weight { get; set; }
            public string? Description { get; set; }
        }
        public class Location
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
        public class Shedule
        {
            public string? TypeCode { get; set; }
            public int? StartHour { get; set; }
            public int? EndHour { get; set; }
            public int? DayOfWeek { get; set; }
        }
    }
}
