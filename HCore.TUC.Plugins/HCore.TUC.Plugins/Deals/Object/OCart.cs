//==================================================================================
// FileName: OCart.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Deals.Object
{
    public class OCart
    {
        public class Save
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long DealId { get; set; }
                public string? DealKey { get; set; }
                public string? StatusCode { get; set; }
                public long? ItemCount { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Update
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long DealId { get; set; }
                public string? DealKey { get; set; }
                public long? ItemCount { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AcountMobileNumber { get; set; }
            public string? AccountEmailAddress { get; set; }
            public string? AccountIconUrl { get; set; }
            public string? AccountAddress { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public string? DealStatus { get; set; }
            public string? DealIconUrl { get; set; }
            public double? DealPrice { get; set; }
            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public long? ItemCount { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AcountMobileNumber { get; set; }
            public string? AccountEmailAddress { get; set; }
            public string? AccountIconUrl { get; set; }
            public string? AccountAddress { get; set; }
            public long? DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public string? DealIconUrl { get; set; }
            public double? DealPrice { get; set; }
            public int? DealTypeId { get; set; }
            public string? DealTypeCode { get; set; }
            public string? DealTypeName { get; set; }
            public DateTime? CreateDate { get; set; }
            public long? CreatedBYId { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? DealStatus { get; set; }
            public long? ItemCount { get; set; }
        }
    }
}

