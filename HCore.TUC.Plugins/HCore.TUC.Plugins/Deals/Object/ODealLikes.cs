//==================================================================================
// FileName: ODealLikes.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Plugins.Deals.Object
{
    public class DealLikes
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public int TypeId { get; set; }

            public long DealId { get; set; }
            public string? DealKey { get; set; }
            public string? DealTitle { get; set; }
            public string? Description { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }
            public double? Amount { get; set; }
            public DateTime? DealStartDate { get; set; }
            public DateTime? DealEndDate { get; set; }

            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountName { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public int AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }
            public string? AccountTypeDisplayName { get; set; }

            public DateTime CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }
}