//==================================================================================
// FileName: ManageDealCart.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Framework;
using HCore.TUC.Plugins.Deals.Object;

namespace HCore.TUC.Plugins.Deals
{
    public class ManageDealCart
    {
        FrameworkCart _FrameworkCart;

        /// <summary>
        /// Description: Saves the cart.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public async Task<OResponse> SaveCart(OCart.Save.Request _Request)
        {
            _FrameworkCart = new FrameworkCart();
            return await _FrameworkCart.SaveCart(_Request);
        }
        /// <summary>
        /// Description: Updates the cart.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public async Task<OResponse> UpdateCart(OCart.Update.Request _Request)
        {
            _FrameworkCart = new FrameworkCart();
            return await _FrameworkCart.UpdateCart(_Request);
        }
        /// <summary>
        /// Description: Deletes the cart.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public async Task<OResponse> DeleteCart(OReference _Request)
        {
            _FrameworkCart = new FrameworkCart();
            return await _FrameworkCart.DeleteCart(_Request);
        }
        /// <summary>
        /// Description: Gets the cart details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public async Task<OResponse> GetCart(OReference _Request)
        {
            _FrameworkCart = new FrameworkCart();
            return await _FrameworkCart.GetCart(_Request);
        }
        /// <summary>
        /// Description: Gets the cart list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public async Task<OResponse> GetCarts(OList.Request _Request)
        {
            _FrameworkCart = new FrameworkCart();
            return await _FrameworkCart.GetCarts(_Request);
        }
    }
}

