//==================================================================================
// FileName: OOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Operations
{
    public class OOperations
    {
        public class Wallet
        {
            public class Balance
            {
                public class Request
                {
                    public string? SourceCode { get; set; }
                    public long AccountId { get; set; }
                    public string? AccountKey { get; set; }
                    public DateTime StartDate { get; set; }
                    public DateTime EndDate { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public string? SourceCode { get; set; }
                    public double Credit { get; set; }
                    public double Debit { get; set; }
                    public double Balance { get; set; }
                }
            }

            public class Credit
            {
                public class Request
                {
                    public string? SourceCode { get; set; }
                    public long AccountId { get; set; }
                    public string? AccountKey { get; set; }
                    public double Amount { get; set; }
                    public string? PaymentReference { get; set; }
                    public string? TransactionReference { get; set; }
                    public string? Comment { get; set; }
                    public string? PaymentType { get; set; }

                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public string? ProductCode { get; set; }
                    public double Credit { get; set; }
                    public double Debit { get; set; }
                    public double Balance { get; set; }
                }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public DateTime TransactionDate { get; set; }
                public double Amount { get; set; }
                public string? PaymentReference { get; set; }

                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
    }
}
