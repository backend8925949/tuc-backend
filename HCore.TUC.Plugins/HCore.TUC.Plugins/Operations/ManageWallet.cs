//==================================================================================
// FileName: ManageWallet.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Operations
{
    public class ManageWallet
    {
        FrameworkWallet _FrameworkWallet;
        /// <summary>
        /// Description: Gets the wallet balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetWalletBalance(OOperations.Wallet.Balance.Request _Request)
        {
            _FrameworkWallet = new FrameworkWallet();
            return _FrameworkWallet.GetWalletBalance(_Request);
        }
        /// <summary>
        /// Description: Credits the wallet.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CreditWallet(OOperations.Wallet.Credit.Request _Request)
        {
            _FrameworkWallet = new FrameworkWallet();
            return _FrameworkWallet.CreditWallet(_Request);
        }
        /// <summary>
        /// Description: Gets the wallet credit history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetWalletCreditHistory(OList.Request _Request)
        {
            _FrameworkWallet = new FrameworkWallet();
            return _FrameworkWallet.GetWalletCreditHistory(_Request);
        }

        /// <summary>
        /// Description: Credits the wallet.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse FundWallet(OOperations.Wallet.Credit.Request _Request)
        {
            _FrameworkWallet = new FrameworkWallet();
            return _FrameworkWallet.FundWallet(_Request);
        }
    }
}
