//==================================================================================
// FileName: FrameworkWallet.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to wallet
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Plugins.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Plugins.Operations
{

    public class FrameworkWallet
    {
        #region Objects
        HCoreContext _HCoreContext;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        OOperations.Wallet.Credit.Response _CreditResponse;
        OOperations.Wallet.Balance.Response _BalanceResponse;
        List<OOperations.Wallet.List> _CreditHistoryDownload;

        #endregion
        /// <summary>
        /// Description: Gets the wallet balance.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>OOperations.Wallet.Balance.Response.</returns>
        internal OOperations.Wallet.Balance.Response GetWalletBalance(long AccountId, long SourceId)
        {
            _BalanceResponse = new OOperations.Wallet.Balance.Response();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception
            try
            {
                long TransactionSourceId = 0;
                if (SourceId == TransactionSource.GiftCards)
                {
                    TransactionSourceId = TransactionSource.GiftCards;
                }
                else if (SourceId == TransactionSource.GiftCards)
                {
                    TransactionSourceId = TransactionSource.GiftPoints;
                }
                else
                {
                    return _BalanceResponse;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.AccountId == AccountId
                       && x.SourceId == TransactionSourceId
                      && x.StatusId == HelperStatus.Transaction.Success
                       && x.ModeId == TransactionMode.Credit)
                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                      .Where(x => x.AccountId == AccountId
                      && x.SourceId == TransactionSourceId
                      && x.StatusId == HelperStatus.Transaction.Success
                      && x.ModeId == TransactionMode.Debit)
                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                    return _BalanceResponse;
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetWalletBalance", _Exception, null);
                return _BalanceResponse;
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the wallet balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetWalletBalance(OOperations.Wallet.Balance.Request _Request)
        {
            if (_Request.AccountId < 1)
            {
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
            }
            if (string.IsNullOrEmpty(_Request.AccountKey))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
            }
            if (string.IsNullOrEmpty(_Request.SourceCode))
            {
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0107, TUCPluginResource.HCP0107M);
            }
            _BalanceResponse = new OOperations.Wallet.Balance.Response();
            _BalanceResponse.Credit = 0;
            _BalanceResponse.Debit = 0;
            _BalanceResponse.Balance = 0;
            #region Manage Exception
            try
            {
                long TransactionSourceId = 0;
                if (_Request.SourceCode == TransactionSource.GiftCardsS)
                {
                    TransactionSourceId = TransactionSource.GiftCards;
                }
                else if (_Request.SourceCode == TransactionSource.GiftPointS)
                {
                    TransactionSourceId = TransactionSource.GiftPoints;
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0108, TUCPluginResource.HCP0108M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _BalanceResponse.Credit = _HCoreContext.HCUAccountTransaction
                       .Where(x => x.AccountId == _Request.AccountId
                       && x.SourceId == TransactionSourceId
                      && x.StatusId == HelperStatus.Transaction.Success
                       && x.ModeId == TransactionMode.Credit)
                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Debit = _HCoreContext.HCUAccountTransaction
                      .Where(x => x.AccountId == _Request.AccountId
                      && x.SourceId == TransactionSourceId
                      && x.StatusId == HelperStatus.Transaction.Success
                      && x.ModeId == TransactionMode.Debit)
                      .Sum(x => (double?)x.TotalAmount) ?? 0;
                    _BalanceResponse.Balance = _BalanceResponse.Credit - _BalanceResponse.Debit;
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, TUCPluginResource.HCP0109, TUCPluginResource.HCP0109M);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetWalletBalance", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Credits the wallet.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CreditWallet(OOperations.Wallet.Credit.Request _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _CreditResponse = new OOperations.Wallet.Credit.Response();
            _CreditResponse.Credit = 0;
            _CreditResponse.Debit = 0;
            _CreditResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0100, TUCPluginResource.HCP0100M);
                }
                if (string.IsNullOrEmpty(_Request.SourceCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0101, TUCPluginResource.HCP0101M);
                }
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0102, TUCPluginResource.HCP0102M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey)
                        .Select(x => new
                        {
                            Id = x.Id,
                            AccountTypeId = x.AccountTypeId,
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            if (AccountDetails.AccountTypeId == UserAccountType.Merchant)
                            {
                                double CommissionPercentage = 0;
                                double SystemAmount = 0;
                                double UserAmount = 0;
                                int TransactionSourceId = 0;
                                int TransactionTypeId = 0;
                                string ConvinenceChargeConfiguration = "giftcardsconvinencecharges";
                                if (_Request.SourceCode == TransactionSource.GiftCardsS)
                                {
                                    TransactionSourceId = TransactionSource.GiftCards;
                                    TransactionTypeId = TransactionType.GiftCard;
                                    ConvinenceChargeConfiguration = "giftcardsconvinencecharges";
                                }
                                else if (_Request.SourceCode == TransactionSource.GiftPointS)
                                {
                                    TransactionSourceId = TransactionSource.GiftPoints;
                                    TransactionTypeId = TransactionType.OnlineGiftPoints;
                                    ConvinenceChargeConfiguration = "giftpointsconvinencecharges";
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001", "Invalid transaction source");
                                }
                                string TConvincenceCharges = HCoreHelper.GetConfiguration(ConvinenceChargeConfiguration, AccountDetails.Id);
                                if (!string.IsNullOrEmpty(TConvincenceCharges))
                                {
                                    CommissionPercentage = Convert.ToDouble(TConvincenceCharges);
                                    SystemAmount = HCoreHelper.GetPercentage(_Request.Amount, CommissionPercentage);
                                    UserAmount = _Request.Amount - SystemAmount;
                                }
                                else
                                {
                                    SystemAmount = 0;
                                    UserAmount = _Request.Amount;
                                }
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = _Request.AccountId;
                                _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                _CoreTransactionRequest.InvoiceNumber = _Request.TransactionReference;
                                _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = _Request.AccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionTypeId,
                                    SourceId = TransactionSourceId,
                                    Comment = _Request.Comment,
                                    Amount = _Request.Amount,
                                    Comission = SystemAmount,
                                    TotalAmount = _Request.Amount,
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        _CreditResponse.Credit = _HCoreContext.HCUAccountTransaction
                                           .Where(x => x.AccountId == _Request.AccountId
                                           && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                           && x.ModeId == TransactionMode.Credit)
                                           .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.AccountId
                                          && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Balance = _CreditResponse.Credit - _CreditResponse.Debit;
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CreditResponse, TUCPluginResource.HCP0106, TUCPluginResource.HCP0106M);
                                    }
                                }
                                else
                                {
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0105, TUCPluginResource.HCP0105M);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0104, TUCPluginResource.HCP0104M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0103, TUCPluginResource.HCP0103M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditWallet", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Fund user wallet.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse FundWallet(OOperations.Wallet.Credit.Request _Request)
        {
            _ManageCoreTransaction = new ManageCoreTransaction();
            _CreditResponse = new OOperations.Wallet.Credit.Response();
            _CreditResponse.Credit = 0;
            _CreditResponse.Debit = 0;
            _CreditResponse.Balance = 0;
            #region Manage Exception 
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0100, TUCPluginResource.HCP0100M);
                }

                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0102, TUCPluginResource.HCP0102M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId)
                        .Select(x => new
                        {
                            Id = x.Id,
                            AccountTypeId = x.AccountTypeId,
                            StatusId = x.StatusId,
                        })
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId == HelperStatus.Default.Active)
                        {
                            int? PaymentMethodId = HCoreHelper.GetSystemHelperId(PaymentMethod.ReferralBonus, _Request.UserReference);

                            double SystemAmount = 0;
                            int TransactionSourceId = 0;
                            int TransactionTypeId = 0;

                            if(_Request.PaymentType == "WalletFunding")
                            {
                                TransactionTypeId = TransactionType.Deal.DealPurchase;
                                TransactionSourceId = TransactionSource.Payments;
                            }

                            _CoreTransactionRequest = new OCoreTransaction.Request();
                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            _CoreTransactionRequest.ParentId = _Request.AccountId;
                            _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            _CoreTransactionRequest.InvoiceNumber = _Request.TransactionReference;
                            _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                            _CoreTransactionRequest.PaymentMethodId = (int)PaymentMethodId;
                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            {
                                UserAccountId = _Request.AccountId,
                                ModeId = TransactionMode.Credit,
                                TypeId = TransactionTypeId,
                                SourceId = TransactionSourceId,
                                Comment = _Request.Comment,
                                Amount = _Request.Amount,
                                Comission = SystemAmount,
                                TotalAmount = _Request.Amount,
                            });
                            _CoreTransactionRequest.Transactions = _TransactionItems;
                            _ManageCoreTransaction = new ManageCoreTransaction();
                            OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            if (TransactionResponse.Status == HelperStatus.Transaction.Success)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if(_Request.PaymentType == "WalletFunding")
                                    {
                                        _CreditResponse.Credit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.AccountId == _Request.AccountId
                                       && x.SourceId == TransactionSource.Payments
                                          && x.StatusId == HelperStatus.Transaction.Success
                                       && x.ModeId == TransactionMode.Credit)
                                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.AccountId
                                          && x.SourceId == TransactionSource.Payments
                                          && x.StatusId == HelperStatus.Transaction.Success
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Balance = _CreditResponse.Credit - _CreditResponse.Debit;
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CreditResponse, TUCPluginResource.HCP0106, TUCPluginResource.HCP0106M);
                                    }
                                    else
                                    {
                                        _CreditResponse.Credit = _HCoreContext.HCUAccountTransaction
                                       .Where(x => x.AccountId == _Request.AccountId
                                       && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                       && x.ModeId == TransactionMode.Credit)
                                       .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Debit = _HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == _Request.AccountId
                                          && x.Source.SystemName == _Request.SourceCode
                                          && x.StatusId == HelperStatus.Transaction.Success
                                          && x.ModeId == TransactionMode.Debit)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0;
                                        _CreditResponse.Balance = _CreditResponse.Credit - _CreditResponse.Debit;
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CreditResponse, TUCPluginResource.HCP0106, TUCPluginResource.HCP0106M);
                                    }
                                    
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0105, TUCPluginResource.HCP0105M);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0104, TUCPluginResource.HCP0104M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0103, TUCPluginResource.HCP0103M);
                    }


                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditWallet", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }


        /// <summary>
        /// Description: Gets the wallet credit history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetWalletCreditHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetWalletCreditHistoryDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetWalletCreditHistoryDownload>("ActorGetWalletCreditHistoryDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                                 where x.AccountId == _Request.AccountId
                                                 && x.Source.SystemName == _Request.ReferenceKey
                                                 && x.ModeId == TransactionMode.Credit
                                                 select new OOperations.Wallet.List
                                                 {

                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     TransactionDate = x.TransactionDate,
                                                     Amount = x.Amount,
                                                     PaymentReference = x.ReferenceNumber,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OOperations.Wallet.List> Data = (from x in _HCoreContext.HCUAccountTransaction
                                                          where x.AccountId == _Request.AccountId
                                                          && x.Source.SystemName == _Request.ReferenceKey
                                                          && x.ModeId == TransactionMode.Credit
                                                          select new OOperations.Wallet.List
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,
                                                              TransactionDate = x.TransactionDate,
                                                              Amount = x.Amount,
                                                              PaymentReference = x.ReferenceNumber,
                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name
                                                          })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetWalletCreditHistory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the wallet credit history download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetWalletCreditHistoryDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _CreditHistoryDownload = new List<OOperations.Wallet.List>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                             where x.AccountId == _Request.AccountId
                                             && x.Source.SystemName == _Request.ReferenceKey
                                             && x.ModeId == TransactionMode.Credit
                                             select new OOperations.Wallet.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 TransactionDate = x.TransactionDate,
                                                 Amount = x.Amount,
                                                 PaymentReference = x.ReferenceNumber,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name
                                             })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = (from x in _HCoreContext.HCUAccountTransaction
                                     where x.AccountId == _Request.AccountId
                                     && x.Source.SystemName == _Request.ReferenceKey
                                     && x.ModeId == TransactionMode.Credit
                                     select new OOperations.Wallet.List
                                     {
                                         ReferenceId = x.Id,
                                         ReferenceKey = x.Guid,
                                         TransactionDate = x.TransactionDate,
                                         Amount = x.Amount,
                                         PaymentReference = x.ReferenceNumber,
                                         StatusId = x.StatusId,
                                         StatusCode = x.Status.SystemName,
                                         StatusName = x.Status.Name
                                     })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _CreditHistoryDownload.Add(new OOperations.Wallet.List
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                ReferenceKey = _DataItem.ReferenceKey,
                                TransactionDate = _DataItem.TransactionDate,
                                Amount = _DataItem.Amount,
                                PaymentReference = _DataItem.PaymentReference,
                                StatusId = _DataItem.StatusId
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("WalletCreditHistory", _CreditHistoryDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetWalletCreditHistoryDownload", _Exception);
            }
            #endregion
        }
    }

    internal class ActorGetWalletCreditHistoryDownload : ReceiveActor
    {
        public ActorGetWalletCreditHistoryDownload()
        {
            FrameworkWallet FrameworkWallet;
            Receive<OList.Request>(_Request =>
            {
                FrameworkWallet = new FrameworkWallet();
                FrameworkWallet.GetWalletCreditHistoryDownload(_Request);
            });
        }
    }
}
