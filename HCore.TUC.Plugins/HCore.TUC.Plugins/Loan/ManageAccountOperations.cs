//==================================================================================
// FileName: ManageAccountOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan.Framework;
using HCore.TUC.Plugins.Loan.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan
{
    public class ManageAccountOperations
    {
        FrameworkAccountOperations _FrameworkAccountOperation;

        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccount(OAccountOperation.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperations();
            return _FrameworkAccountOperation.GetAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the account list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccounts(OList.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperations();
            return _FrameworkAccountOperation.GetAccounts(_Request);
        }
        /// <summary>
        /// Description: Gets the account loan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountLoan(OAccountLoanOperation.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperations();
            return _FrameworkAccountOperation.GetAccountLoan(_Request);
        }
        /// <summary>
        /// Description: Gets the account loan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountLoans(OList.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperations();
            return _FrameworkAccountOperation.GetAccountLoans(_Request);
        }
        /// <summary>
        /// Description: Gets the account loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountLoansOverview(OAccountLoanOperation.Overview.Request _Request)
        {
            _FrameworkAccountOperation = new FrameworkAccountOperations();
            return _FrameworkAccountOperation.GetAccountLoansOverview(_Request);
        }
    }
}
