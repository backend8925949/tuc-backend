//==================================================================================
// FileName: ManageEligibleCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan.Framework;
using HCore.TUC.Plugins.Loan.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace HCore.TUC.Plugins.Loan
{
    public class ManageEligibleCustomer
    {
        FrameworkEligibleCustomer _FrameworkEligibleCustomer;
        /// <summary>
        /// Description: Gets the eligible customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetEligibleCustomer(OList.Request _Request)
        {
            _FrameworkEligibleCustomer = new FrameworkEligibleCustomer();
            return _FrameworkEligibleCustomer.GetEligibleCustomer(_Request);
        }
        /// <summary>
        /// Description: Gets the eligible customer overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetEligibleCustomerOverview(OSummaryOverview.Request _Request)
        {
            _FrameworkEligibleCustomer = new FrameworkEligibleCustomer();
            return _FrameworkEligibleCustomer.GetEligibleCustomerOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the eligible customers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetEligibleCustomersOverview(OReference _Request)
        {
            _FrameworkEligibleCustomer = new FrameworkEligibleCustomer();
            return _FrameworkEligibleCustomer.GetEligibleCustomersOverview(_Request);
        }
    }
}
