//==================================================================================
// FileName: ManageLoanOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan.Framework;
using HCore.TUC.Plugins.Loan.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan
{
    public class ManageLoanOperation
    {
        FrameWorkLoanOperation _FrameWorkLoanOperation;

        /// <summary>
        /// Description: Gets the loan request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoanRequest(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetLoanRequest(_Request);
        }

        /// <summary>
        /// Description: Gets all loan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAllLoans(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetAllLoans(_Request);
        }

        /// <summary>
        /// Description: Gets the repayment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRepayments(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetRepayments(_Request);
        }
        /// <summary>
        /// Description: Gets the loan request overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoanRequestOverview(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetLoanRequestOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the repayments overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRepaymentsOverview(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetRepaymentsOverview(_Request);
        }
        /// <summary>
        /// Description: Gets all loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAllLoansOverview(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetAllLoansOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the loan details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoanDetails(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetLoanDetails(_Request);
        }
        /// <summary>
        /// Description: Gets the loan activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoanActivity(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetLoanActivity(_Request);
        }
        /// <summary>
        /// Description: Gets the reconcilation report.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetReconcilationReport(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetReconcilationReport(_Request);
        }
        /// <summary>
        /// Description: Gets the reconcilation report overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetReconcilationReportOverview(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetReconcilationReportOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the BNPL overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBNPLOverview(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetBNPLOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the disbursment distribution overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDisbursmentDistributionOverview(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetDisbursmentDistributionOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the repayment status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRepaymentStatusOverview(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetRepaymentStatusOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the loan disbursement.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoanDisbursement(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetLoanDisbursement(_Request);
        }

        /// <summary>
        /// Description: Gets the disbursement by month.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDisbursementByMonth(OSummaryOverview.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetDisbursementByMonth(_Request);
        }
        /// <summary>
        /// Description: Gets the account loan history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountLoanHistory(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetAccountLoanHistory(_Request);
        }
        /// <summary>
        /// Description: Gets the account repayment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountRepayments(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetAccountRepayments(_Request);
        }
        /// <summary>
        /// Description: Gets the loan provider list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoanProviders(OList.Request _Request)
        {
            _FrameWorkLoanOperation = new FrameWorkLoanOperation();
            return _FrameWorkLoanOperation.GetLoanProviders(_Request);
        }
    }
}
