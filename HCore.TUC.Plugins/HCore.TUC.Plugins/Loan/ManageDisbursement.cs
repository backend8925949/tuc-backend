//==================================================================================
// FileName: ManageDisbursement.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Plugins.Loan.Framework;

namespace HCore.TUC.Plugins.Loan
{
    public class ManageDisbursement
    {
        /// <summary>
        /// Credits the loan amount.
        /// </summary>
        public void CreditLoanAmount()
        {
            try
            {
                var system = ActorSystem.Create("ActoreCreditLoanAmount");
                var greeter = system.ActorOf<ActoreCreditLoanAmount>("ActoreCreditLoanAmount");
                greeter.Tell("creditloanamount");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditLoanAmount", _Exception, null);
            }
        }
        /// <summary>
        /// Updates the repayment status.
        /// </summary>
        // public void UpdateRepaymentStatus()
        // {
        //     try
        //     {
        //         var system = ActorSystem.Create("ActorUpdateRepaymentStatus");
        //         var greeter = system.ActorOf<ActorUpdateRepaymentStatus>("ActorUpdateRepaymentStatus");
        //         greeter.Tell("updaterepaymentstatus");
        //     }
        //     catch (Exception _Exception)
        //     {
        //         HCoreHelper.LogException("UpdateRepaymentStatus", _Exception, null);
        //     }
        // }
        /// <summary>
        /// Updates the repayment status1.
        /// </summary>
        public void UpdateRepaymentStatus1()
        {
            try
            {
                var system = ActorSystem.Create("ActorUpdateRepaymentStatuss");
                var greeter = system.ActorOf<ActorUpdateRepaymentStatuss>("ActorUpdateRepaymentStatuss");
                greeter.Tell("updaterepaymentstatuss");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateRepaymentStatus1", _Exception, null);
            }
        }
        /// <summary>
        /// Checks the overdue repayments.
        /// </summary>
        public void CheckOverdueRepayments()
        {
            try
            {
                var system = ActorSystem.Create("ActorCheckOverdueRepayments");
                var greeter = system.ActorOf<ActorCheckOverdueRepayments>("ActorCheckOverdueRepayments");
                greeter.Tell("checkoverduerepayments");
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CheckOverdueRepayments", _Exception, null);
            }
        }
    }

    internal class ActoreCreditLoanAmount : ReceiveActor
    {
        public ActoreCreditLoanAmount()
        {
            Receive<string>(Request =>
            {
                FrameworkDisburseLoan DisburseLoan = new FrameworkDisburseLoan();
                DisburseLoan.CreditLoanAmount();
            });
        }
    }

    // internal class ActorUpdateRepaymentStatus : ReceiveActor
    // {
    //     public ActorUpdateRepaymentStatus()
    //     {
    //         Receive<string>(Request =>
    //         {
    //             FrameworkDisburseLoan DisburseLoan = new FrameworkDisburseLoan();
    //             DisburseLoan.UpdateRepaymentStatus();
    //         });
    //     }
    // }

    internal class ActorUpdateRepaymentStatuss : ReceiveActor
    {
        public ActorUpdateRepaymentStatuss()
        {
            Receive<string>(Request =>
            {
                FrameworkDisburseLoan DisburseLoan = new FrameworkDisburseLoan();
                DisburseLoan.UpdateRepaymentStatus1();
            });
        }
    }

    internal class ActorCheckOverdueRepayments : ReceiveActor
    {
        public ActorCheckOverdueRepayments()
        {
            Receive<string>(Request =>
            {
                FrameworkDisburseLoan DisburseLoan = new FrameworkDisburseLoan();
                DisburseLoan.CheckOverdueRepayments();
            });
        }
    }
}
