//==================================================================================
// FileName: ManageMerchantOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan.Framework;
using HCore.TUC.Plugins.Loan.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan
{
    public class ManageMerchantOperation
    {
        FrameworkMerchantOperation _FrameworkMerchantOperation;

        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantList(OList.Request _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetMerchantList(_Request);
        }

        /// <summary>
        /// Description: Gets the pending loans.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPendingLoans(OList.Request _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetPendingLoans(_Request);
        }
        /// <summary>
        /// Description: Gets the redeemed loans.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemedLoans(OList.Request _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetRedeemedLoans(_Request);
        }

        /// <summary>
        /// Description: Gets the pending loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPendingLoansOverview(OReference _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetPendingLoansOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the redeemed loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRedeemedLoansOverview(OReference _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetRedeemedLoansOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the loan overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoanOverview(OReference _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetLoanOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the merchant list overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantListOverview(OSummaryOverview.Request _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetMerchantListOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the settlement list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSettlementList(OList.Request _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetSettlementList(_Request);
        }

        /// <summary>
        /// Description: Gets the settlement overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSettlementOverview(OReference _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetSettlementOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the merchant settlement.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantSettlement(OReference _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetMerchantSettlement(_Request);
        }

        /// <summary>
        /// Description: Gets the settlements lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSettlementsLite(OList.Request _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetSettlementsLite(_Request);
        }

        /// <summary>
        /// Description: Gets the settlement overviewlite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSettlementOverviewlite(OReference _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.GetSettlementOverviewlite(_Request);
        }

        /// <summary>
        /// Description: Settlements the invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SettlementInvoice(OMerchant.InvoiceEmail _Request)
        {
            _FrameworkMerchantOperation = new FrameworkMerchantOperation();
            return _FrameworkMerchantOperation.SettlementInvoice(_Request);
        }
    }
}
