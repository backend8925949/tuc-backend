//==================================================================================
// FileName: OLoanOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan.Object
{
    internal class OLoanOperation
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerNumber { get; set; }
            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public long? MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconURL { get; set; }
            public short MerchantRedeem { get; set; }
            public double? LoanAmount { get; set; }
            public double? InstallmentAmount { get; set; }
            public string? PaidInstallmentStatus { get; set; }
            public string? Status { get; set; }
            public long? StatusId { get; set; }
            public long? PaidInstallmentStatusId { get; set; }
            public DateTime? CreatedDate { get; set; }
            public DateTime? ApprovedDate { get; set; }
            public long? TotalInstallments { get; set; }
            public long? PaidInstallments { get; set; }

        }
        public class AllLoanList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerNumber { get; set; }
            public long MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }
            public string? ProviderReference { get; set; }
            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public string? Status { get; set; }
            public string? StatusCode { get; set; }
            public double? LoanAmount { get; set; }
            public long? TotalInstallments { get; set; }
            public double? TotalInterest { get; set; }
            public double? TotalAmount { get; set; }
            public double? TotalPaidAmount { get; set; }
            public double? InterestRate { get; set; }
            public long? PaidInstallments { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? NextRepayment { get; set; }
            public double? Repayment { get; set; }
            public long? OverDueInstallment { get; set; }
            public DateTime? DueDate { get; set; }
            public double? TUCFees { get; set; }
            public double? MerchantSettlement { get; set; }
            public string? MerchantSettleStatus { get; set; }
            public double? PrincipalAmount { get; set; }
            public string? TUCStatus { get; set; }

        }
        public class Repayments
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerNumber { get; set; }
            public long MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }
            public string? ProviderReference { get; set; }
            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public long LoanId { get; set; }
            public string? LoanKey { get; set; }
            public double LoanAmount { get; set; }

            public int StatusId { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }

            public double InterestAmount { get; set; }
            public double? TotalPaidAmount { get; set; }
            public double? Repayment { get; set; }
            public double? InterestRate { get; set; }
            public long? PaidInstallment { get; set; }
            public long? OverDueInstallment { get; set; }
            public long? TotalInstallment { get; set; }
            public DateTime? NextRepayment { get; set; }
            public DateTime? LastPayment { get; set; }
            public DateTime? DueDate { get; set; }
        }
        public class ListDownload
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerNumber { get; set; }
            public long MerchantId { get; set; }
            public string? MerchantDisplayName { get; set; }
            public long LoanId { get; set; }
            public string? LoanKey { get; set; }
            public double LoanAmount { get; set; }
            public double InterestAmount { get; set; }
            public double? TotalPaidAmount { get; set; }
            public long? TotalInstallment { get; set; }
            public double? Repayment { get; set; }
            public DateTime? NextRepayment { get; set; }
            public DateTime? LastPayment { get; set; }
            public DateTime? DueDate { get; set; }
        }
        public class LoanDetails
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime RequestedOn { get; set; }
            public DateTime? ApprovedOn { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public LoanProviderDetail ProviderDetail { get; set; }
            public LoanPlanDetail PlanDetail { get; set; }
            public LoanCustomerDetail CustomerDetail { get; set; }
            public List<LoanRepaymentTimeLine> RepaymentTimeLine { get; set; }
            public LoanDisbursmentTimeLine LoanDisbursmentTimeLine { get; set; }
        }
        public class LoanProviderDetail
        {
            public string? ProviderReference { get; set; }
            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public double PrincipalAmount { get; set; }
            public double InterestRate { get; set; }
            public double InterestAmount { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantAddress { get; set; }
            public string? MerchantIconURL { get; set; }
        }
        public class LoanPlanDetail
        {
            public long TotalInstallments { get; set; }
            public double IntialAmount { get; set; }
            public double FinalAmount { get; set; }
            public double PaidAmount { get; set; }
            public long PaidInstallments { get; set; }
            public double RepaymentAmount { get; set; }
            public long Duration { get; set; }
        }
        public class LoanCustomerDetail
        {
            public string? CustomerName { get; set; }
            public long CustomerAccountId { get; set; }
            public string? CustomerAccountKey { get; set; }
            public string? IconURL { get; set; }
            public string? MobileNumber { get; set; }
            public double? CreditLimit { get; set; }
            public DateTime? ApprovedOn { get; set; }
            public string? ContactNumber { get; set; }
            public string? ContactEmail { get; set; }
            public List<CustomerOtherLoans> CustomerOtherLoans { get; set; }
        }
        public class LoanRepaymentTimeLine
        {
            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public DateTime? DueDate { get; set; }
            public DateTime? PaymentDate { get; set; }
            public double PrincipalAmount { get; set; }
            public double Interest { get; set; }
            public double Total { get; set; }
            public double ToTUC { get; set; }
            public double ToProvider { get; set; }
            public string? Status { get; set; }
            public double ProcessingFees { get; set; }
        }
        public class CustomerOtherLoans
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
        }
        public class LoanActivityTimeLine
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long LoanId { get; set; }
            public string? LoanKey { get; set; }
            public DateTime? LoanAmountCreditDate { get; set; }
            public double? SettelmentAmount { get; set; }
            public DateTime? SettelmentDate { get; set; }
            public string? Description { get; set; }
            public double? Amount { get; set; }
            public DateTime? StatusUpdatedTime { get; set; }
            public sbyte IsCompleted { get; set; }
            public double? FirstPartCreditAmount { get; set; }
            public DateTime? FirstPartCreditDate { get; set; }
            public double? LoanAmountCredited { get; set; }
            public DateTime? LoanRedeemDate { get; set; }
            public DateTime? CreateDate { get; set; }
        }
        public class LTARepayment
        {
            public string? Description { get; set; }
            public double Amount { get; set; }
            public DateTime? PaymentDate { get; set; }
        }
        public class LoanDisbursmentTimeLine
        {
            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public DateTime? Date { get; set; }
            public string? Status { get; set; }
            public double ProviderToTUC { get; set; }
            public double TUCFees { get; set; }
            public double TUCToMerchant { get; set; }
            public DateTime? TUCToMerchantDate { get; set; }
        }

        public class DisbursementbyMonth
        {
            public DateTime? Date { get; set; }
            public string? Month { get; set; }
            public double? LoanAmount { get; set; }
            public long? LoanCount { get; set; }
            public int MonthNo { get; set; }
        }

        public class DisbursementbyMonthResponse
        {
            public double Difference { get; set; }
            public double Differencepercent { get; set; }
            public List<Data> DataSummary { get; set; }
            public List<Data> DataByDate { get; set; }
        }
        public class Data
        {
            public string? Month { get; set; }
            public string? Title { get; set; }
            public long? TotalLoanCount { get; set; }
            public double? TotalLoanAmount { get; set; }
            public int MonthNo { get; set; }
        }

        public class Overview
        {
            public long TotalLoanRequests { get; set; }
            public long TotalActiveLoans { get; set; }
            public double? PendingMerchantSettlements { get; set; }
            public double? TUCFees { get; set; }
            public double? Interest { get; set; }
            public double? Revenue { get; set; }
        }

        public class DisbursementOverview
        {
            public long TotalDisbursedLoans { get; set; }
            public double TotalDisbursedAmount { get; set; }
            public double TotalInterestAmount { get; set; }
            public long TotalDisbursedLoansByEC { get; set; }
            public long TotalDisbursedLoansByTUC { get; set; }
            public double TotalDisbursedAmountByEC { get; set; }
            public double TotalDisbursedAmountByTUC { get; set; }
            public double TotalInterestByEC { get; set; }
            public double TotalInterestByTUC { get; set; }
        }

        public class Providers
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ProviderName { get; set; }
        }
    }
}
