//==================================================================================
// FileName: OPlanOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan.Object
{
    public class OPlanOperation
    {
        public class Save
        {
            public class Request
            {
                public string? Name { get; set; }
                public string? Description { get; set; }
                public string? Policy { get; set; }
                public int Tenture { get; set; }
                public double MinimumLoanAmount { get; set; }
                public double CustomerInterestRate { get; set; }
                public double ProviderInterestRate { get; set; }
                public double SystemInterestRate { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public int ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? Policy { get; set; }
            public int Tenture { get; set; }
            public double MinimumLoanAmount { get; set; }
            public double CustomerInterestRate { get; set; }
            public double ProviderInterestRate { get; set; }
            public double SystemInterestRate { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? Policy { get; set; }
            public int Tenture { get; set; }
            public double MinimumLoanAmount { get; set; }
            public double CustomerInterestRate { get; set; }
            public double ProviderInterestRate { get; set; }
            public double SystemInterestRate { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        
        public class List
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? Policy { get; set; }
            public int Tenture { get; set; }
            public double MinimumLoanAmount { get; set; }
            public double CustomerInterestRate { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }


    public class OMerchantOperation
    {
        public class Save
        {
            public class Request
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? SettelmentTypeCode { get; set; }
                public string? TransactionSourceCode { get; set; }
                public long? SettelmentDays { get; set; }
                public DateTime StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public string? StatusCode { get; set; }
                public sbyte IsMerchantSettelment { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class Update
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? SettelmentTypeCode { get; set; }
            public string? TransactionSourceCode { get; set; }
            public long? SettelmentDays { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public long AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }

            public short? IsMerchantSettelment { get; set; }

            public int SettelmentTypeId { get; set; }
            public string? SettelmentTypeCode { get; set; }
            public string? SettelmentTypeName { get; set; }

            public long? SettelmentDays { get; set; }

            public int TransactionSourceId { get; set; }
            public string? TransactionSourceName { get; set; }
            public string? TransactionSourceCode { get; set; }

            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public long AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }

            public int SettelmentTypeId { get; set; }
            public string? SettelmentTypeCode { get; set; }
            public string? SettelmentTypeName { get; set; }

            public DateTime StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class SettelmentRequest
        {
            public long MerchantId { get; set; }
            public string? StatusCode { get; set; }
            public sbyte IsMerchantSettelment { get; set; }
            public OUserReference? UserReference { get; set; }
        }


        public class CreateSettelmentRequest
        {
            public long MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
