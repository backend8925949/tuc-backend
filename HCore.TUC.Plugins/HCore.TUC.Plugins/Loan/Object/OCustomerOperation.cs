//==================================================================================
// FileName: OCustomerOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.TUC.Plugins.Loan.Object
{
    public class OCustomerOperation
    {
        public class Customer
        {
            public class Configuration
            {
                public class SystemConfig
                {
                    public string? monoKey { get; set; }
                }

                public class Request
                {
                    public long AccountId { get; set; }
                    public string? AccountKey { get; set; }
                    public Mode Mode { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
                public class Mode
                {
                    public string? Mono { get; set; }
                    public string? Evolve { get; set; }
                }
                public class Response
                {
                    public bool IsFeatureEnabled { get; set; }
                    public string? FeatureEnabledMessage { get; set; }
                    public bool IsBnplEnable { get; set; }

                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }

                    public bool IsLoanActive { get; set; }
                    public long? ActiveLoan { get; set; }
                    public long? TotalLoan { get; set; }
                    public double? CreditLimit { get; set; }
                    public double? AvailableCredit { get; set; }
                    public double? MaximumLoanAmount { get; set; }
                    public double? MinimumLoanAmount { get; set; }
                    
                    public List<Plan.List> Plans { get; set; }
                    public SystemConfig SystemConfig { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
            }
            public class Profile
            {
                public class Request
                {
                    public string? FirstName { get; set; }
                    public string? LastName { get; set; }
                    public string? EmailAddress { get; set; }
                    public string? WorkEmailAddress { get; set; }
                    public string? MobileNumber { get; set; }
                    public DateTime? DateOfBirth { get; set; }
                    public string? BvnNumber { get; set; }
                    public double Salary { get; set; }
                    public int SalaryDay { get; set; }
                    public string? Address { get; set; }
                    public string? StateName { get; set; }
                    public double Latitude { get; set; }
                    public double Longitude { get; set; }
                    public string? LgaName { get; set; }
                    public OUserReference? UserReference { get; set; }
                }
            }
        }

        public class Loan
        {
            public class Initialize
            {
                public long AccountId { get; set; }
                public string? AccountKey { get; set; }
                public long LoanRequestId { get; set; }
                public long PlanId { get; set; }
                public long MerchantId { get; set; }
                public double Amount { get; set; }
                public string? PaymentReference { get; set; }
                public OUserReference? UserReference { get; set; }
            }
        }

        public class Merchant
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? DisplayName { get; set; }
                public string? Address { get; set; }
                public long ActiveLoans { get; set; }
                public long ClosedLoans { get; set; }
                public double ActiveLoansAmount { get; set; }
                public double ClosedLoansAmount { get; set; }
                public double TotalInterest { get; set; }
                public double TUCFees { get; set; }
                public double MerchantSettlement { get; set; }
                public int MerchantSettlementStatusId { get; set; }
                public string? MerchantSettlementStatusCode { get; set; }
                public string? MerchantSettlementStatusName { get; set; }
                public string? IconUrl { get; set; }
                public double? RewardPercentage { get; set; }
                public long Locations { get; set; }
                public int StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class Plan
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? Description { get; set; }
                public string? Policy { get; set; }
                public int Tenture { get; set; }
                public double MinimumLoanAmount { get; set; }
                public double CustomerInterestRate { get; set; }
                public double ProviderInterestRate { get; set; }
                public double SystemInterestRate { get; set; }
            }
        }
        public class LoanPurchase
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? CustomerName { get; set; }
                public string? CustomerMobileNumber { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public string? MerchantAddress { get; set; }
                public string? ProviderReference { get; set; }
                public double? LoanAmount { get; set; }
                public double? InstallmentAmount { get; set; }
                public double? InterestRate { get; set; }
                public double? TotalInterest { get; set; }
                public long? TotalInstallment { get; set; }
                public long? PaidInstallment { get; set; }
                public double? TotalPaidAmount { get; set; }
                public double? TotalAmount { get; set; }
                public double? TUCFeesPercentage { get; set; }
                public double? TUCFees { get; set; }
                public DateTime? NextInstallmentDate { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? MerchantDisplayName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public double? LoanAmount { get; set; }
                public double? InterestRate { get; set; }
                public double? InstallmentAmount { get; set; }
                public long? TotalInstallment { get; set; }
                public long? PaidInstallment { get; set; }
                public string? LoanCode { get; set; }

                public DateTime RequestDate { get; set; }
                public DateTime? ClosedOn { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public List<LoanPayment> Payments { get; set; }
            }
            public class LoanPayment
            {
                public long ReferenceId { get; set; }
                public double Amount { get; set; }
                public  DateTime? DueDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

            }
        }

        public class EligibleCustomer {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? CustomerName { get; set; } 
            public string? MobileNumber { get; set; }
            public double? CreditLimit { get; set; }
            public DateTime? ApprovedOn { get; set; }
            public long ActiveLoans { get; set; }
            public string? IconURL { get; set; }
            public DateTime? LastActivity { get; set; }
        }

        public class Overview
        {
            public long TotalLoans { get; set; }
            public double? TotalLoansAmount { get; set; }
            public double? TotalCreditLimit { get; set; }
            public long TotalCustomers { get; set; }
            public long TotalLoanCustomers { get; set; }
            public long? UsedCredit { get; set; }
            public long? UnUsedCredit { get; set; }
        }

        public class Configuration
        {
            public OUserReference? UserReference { get; set; }
        }
    }
}
