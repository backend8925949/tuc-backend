//==================================================================================
// FileName: OMerchant.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan.Object
{
    public class OMerchant
    {
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantAddress { get; set; }
            public string? MerchantLogoURL { get; set; }
            public long ActiveLoans { get; set; }
            public double TotalActiveAmount { get; set; }
            public double ClosedLoans { get; set; }
            public double TotalClosedAmount { get; set; }
            public double TotalInterest { get; set; }
            public double? TUCFees { get; set; }
            public double MerchantSettlement { get; set; }
            public string? MerchantSettleStatus { get; set; }
            public string? PrimaryCategoryKey { get; set; }
            public string? PrimaryCategoryName { get; set; }
        }

        public class ListDownload
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantAddress { get; set; }
            public string? MerchantLogoURL { get; set; }
            public long ActiveLoans { get; set; }
            public double TotalActiveAmount { get; set; }
            public double ClosedLoans { get; set; }
            public double TotalClosedAmount { get; set; }
            public double TotalInterest { get; set; }
            public double MerchantSettlement { get; set; }
        }

        public class PendingLoans
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long CustomerAccountId { get; set; }
            public string? CustomerAccountKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerNumber { get; set; }
            public string? IconURL { get; set; }
            public double? TUCFees { get; set; }
            public double? TUCAmount { get; set; }
            public double? LoanAmount { get; set; }
            public long? TotalInstallments { get; set; }
            public DateTime? ApprovedOn { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByName { get; set; }
        }
        public class RedeemedLoans
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long CustomerAccountId { get; set; }
            public string? CustomerAccountKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerNumber { get; set; }
            public double? TUCFees { get; set; }
            public int? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public double? TUCFeesPercentage { get; set; }
            public double? TotalAmount { get; set; }
            public double? LoanAmount { get; set; }
            public long? TotalInstallments { get; set; }
            public double? SettlementAmount { get; set; }
            public DateTime? ApprovedOn { get; set; }
            public DateTime? RedeemedOn { get; set; }
            public DateTime? SettlementDate { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByName { get; set; }
        }

        public class MerchantRequest
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long MerchantId { get; set; }
            public long LoanId { get; set; }
            public long ProviderId { get; set; }
            public DateTime? LoanRedeemDate { get; set; }
            public DateTime? SettlementDate { get; set; }
            public double? LoanAmount { get; set; }
            public double? TucFees { get; set; }
            public double? SettlementAmount { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class SettlementList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? TransactionId { get; set; }
            public string? TransactionReference { get; set; }

            public long LoanId { get; set; }
            public string? LoanKey { get; set; }
            public double? LoanAmount { get; set; }
            public DateTime? LoanRedeemDate { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }

            public long MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public long MerchantAccountId { get; set; }
            public string? MerchantAccountKey { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? MerchantAccountName { get; set; }
            public string? MerchantAccountAddress { get; set; }

            public string? CustomerName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerEmailAddress { get; set; }

            public int StatusId { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }
            public DateTime? SettlementDate { get; set; }
            public double? SettlementAmount { get; set; }
            public double? TUCFees { get; set; }

            public string? MerchantName { get; set; }
            public string? MobileNumber { get; set; }
            public DateTime? RedeemedOn { get; set; }
            public string? MerchantLogoURL { get; set; }
            public string? ProviderLogoURL { get; set; }
            public string? MerchantAddress { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByName { get; set; }
        }

        public class Settlementslite
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public double? Amount { get; set; }
            public string? ReferenceNumber { get; set; }
            public DateTime? TransactionDate { get; set; }

            public long? MerchantAccountId { get; set; }
            public string? MerchantAccountKey { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? MerchantAccountName { get; set; }
            public string? MerchantAccountAddress { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByName { get; set; }

            public int StatusId { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }
        }

        public class OverviewLite
        {
            public long? TotalTransactions { get; set; }
            public double? TotalTransactionAmount { get; set; }
        }


        public class SettlementDetails
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? TransactionId { get; set; }
            public string? TransactionReference { get; set; }
            public string? InvoiceNumber { get; set; }

            public long LoanId { get; set; }
            public string? LoanKey { get; set; }
            public double? LoanAmount { get; set; }
            public DateTime? LoanRedeemDate { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }
            public string? ProviderMobileNumber { get; set; }
            public string? ProviderEmailAddress { get; set; }
            public string? ProviderAddress { get; set; }

            public long MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public long MerchantAccountId { get; set; }
            public string? MerchantAccountKey { get; set; }
            public string? MerchantAccountName { get; set; }
            public string? MerchantAccountAddress { get; set; }
            public string? MerchantAccountEmailAddress { get; set; }
            public string? MerchantAccountMobileNumber { get; set; }

            public double? SettlementAmount { get; set; }
            public double? TUCFees { get; set; }
            public DateTime? SettlementDate { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class SettlementOverview
        {
            public long TotalSettlements { get; set; }
            public double? TotalSettlementAmount { get; set; }
            public double? TotalTUCFees { get; set; }
            public long TotalPendingSettlements { get; set; }
            public double? TotalPendingSettlementAmount { get; set; }
            public long TotalClearSettlements { get; set; }
            public double? TotalClearSettlementAmount { get; set; }
        }

        public class Overview
        {
            public long PendingLoans { get; set; }
            public double? PendingLoansAmount { get; set; }
            public double? TotalPendingSettlementAmount { get; set; }
            public long RedeemedLoans { get; set; }
            public double? RedeemedLoansAmount { get; set; }
            public long TotalLoans { get; set; }
            public double? TotalLoansAmount { get; set; }
            public long TotalRedeemedLoans { get; set; }
            public double? TotalRedeemedLoansAmount { get; set; }
        }

        public class MerchantSettlementListDownload
        {
            public long ReferenceId { get; set; }
            public long LoanId { get; set; }
            public double? LoanAmount { get; set; }
            public string? StatusName { get; set; }
            public DateTime? SettlementDate { get; set; }
            public double? SettlementAmount { get; set; }
            public double? TUCFees { get; set; }
            public string? MerchantName { get; set; }
            public string? ProviderName { get; set; }
            public string? CustomerName { get; set; }
            public string? MobileNumber { get; set; }
            public DateTime? RedeemedOn { get; set; }
            public string? MerchantLogoURL { get; set; }
            public string? ProviderLogoURL { get; set; }
            public string? MerchantAddress { get; set; }
        }
        public class RedeemedLoansDownload
        {
            public long LoanId { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerNumber { get; set; }
            public DateTime? ApprovedOn { get; set; }
            public DateTime? RedeemedOn { get; set; }
            public double? LoanAmount { get; set; }
            public long? TotalInstallments { get; set; }
            public double? TUCFees { get; set; }
            public double? TUCFeesPercentage { get; set; }
            public string? StatusName { get; set; }
            public double? SettlementAmount { get; set; }
            public DateTime? SettlementDate { get; set; }
        }

        public class InvoiceEmail
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? MerchantEmailAddress { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }
}
