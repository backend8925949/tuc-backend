//==================================================================================
// FileName: OAccountOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan.Object
{
    public class OAccountOperation
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public long ActiveLoans { get; set; }
            public long CreditLimit { get; set; }
            public string? ApprovedDate { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountIconUrl { get; set; }
            public int AccountTypeId { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }
            public int AccountStatusId { get; set; }
            public string? AccountStatusCode { get; set; }
            public string? AccountStatusName { get; set; }
            public string? ProviderReference { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? EmailAddress { get; set; }
            public string? WorkEmailAddress { get; set; }
            public string? MobileNumber { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public string? BvnNumber { get; set; }
            public double? Salary { get; set; }
            public int? SalaryDay { get; set; }
            public string? Address { get; set; }
            public string? StateName { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }
            public string? LgaName { get; set; }
            public DateTime? LastActivityDate { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }



    public class OAccountLoanOperation
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountName { get; set; }
            public string? AccountMobileNumber { get; set; }
            public string? AccountEmailAddress { get; set; }
            public int PlanId { get; set; }
            public string? PlanKey { get; set; }
            public string? PlanName { get; set; }
            public string? PlanPolicy { get; set; }
            public int Tenture { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double TotalAmount { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string? LoanCode { get; set; }
            public string? LoanPin { get; set; }
            public sbyte IsRedeemed { get; set; }
            public DateTime? RedeemDate { get; set; }
            public long? RedeemLocationId { get; set; }
            public long? TerminalId { get; set; }
            public long? CashierId { get; set; }
            public long MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }
            public long? ProviderId { get; set; }
            public string? ProviderName { get; set; }
            public string? ProviderIconUrl { get; set; }

            public double? TotalInterest { get; set; }
            public double? InterestRate { get; set; }
            public double? PaidInstallments { get; set; }
            public double? TotalInstallments { get; set; }
            public double? TotalPaidAmount { get; set; }

            public long? CreditTransactionId { get; set; }
            public long? RedeemTransactionId { get; set; }
            public DateTime? CreateDate { get; set; }
            public long CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Overview
        {
            public class Request
            {
                public long AccountId { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long AllLoans { get; set; }
                public long ActveLoans { get; set; }
                public double ActiveLoanAmount { get; set; }
                public long ClosedLoans { get; set; }
                public double ClosedLoanAmount { get; set; }
            }
        }
    }
}
