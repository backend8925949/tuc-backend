//==================================================================================
// FileName: OLoanOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.Plugins.Loan.Object
{
    public class OLoanOperations
    {
        public class Repayments
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? CustomerName { get; set; }
            public string? CustomerMobileNumber { get; set; }

            public string? MerchantDisplayName { get; set; }
            public string? MerchantAddress { get; set; }

            public long LoanId { get; set; }
            public string? LoanKey { get; set; }
            public string? ProviderReference { get; set; }
            public double LoanAmount { get; set; }
            public double InstallmentAmount { get; set; }
            public double InterestRate { get; set; }
            public double TotalInterest { get; set; }
            public long TotalInstallment { get; set; }
            public long PaidInstallment { get; set; }
            public long PendingInstallments { get; set; }
            public long OverdueInstallments { get; set; }
            public double TotalPaidAmount { get; set; }
            public double TotalAmount { get; set; }
            public double RepaymentAmount { get; set; }
            public DateTime? DueDate { get; set; }
            public DateTime? LastPaid { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
