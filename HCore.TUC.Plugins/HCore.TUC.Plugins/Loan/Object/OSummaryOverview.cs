//==================================================================================
// FileName: OSummaryOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.TUC.Plugins.Loan.Object
{
    public class OSummaryOverview
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public DateTime Date { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class LoanOverview
        {
            public int TotalLoansTaken { get; set; }
            public double TotalLoanAmount { get; set; }
            public long CardsRedeemed { get; set; }
            public double TotalAmountRedeemed { get; set; }
        }
        public class Overview
        {
            public long Total { get; set; }
            public double TotalAmount { get; set; }
        }
        public class SettlementOverview
        {
            public Overview PendingSettlements { get; set; }
            public Overview ClearSettlements { get; set; }
        }
        public class RepaymentOverview
        {
            public Overview OverDue { get; set; }
            public Overview Paid { get; set; }
            public Overview Pending { get; set; }
        }
        public class MerchantOverview {
            public long TotalMerchants { get; set; }
            public long ActiveLoans { get; set; }
            public long ClosedLoans { get; set; }
            public double? TUCFees { get; set; }
        }

        public class AllLoansSubOverview
        {
            public long? Loans { get; set; }
            public double? PrincipalAmount { get; set; }
            public double? InterestAmount { get; set; }
            public double? TUCAmount { get; set; }
        }
        public class DefaultLoansOverView { 
            public long TotalLoans { get; set; }
            public long Repay { get; set; }
        }

        public class AllLoansOverview { 
            public AllLoansSubOverview ActiveLoans { get; set; }
            public DefaultLoansOverView DefaultLoansOverView{ get; set; }
            public AllLoansSubOverview ClosedLoansOverView { get; set; }
            public AllLoansSubOverview AllLoansOverView { get; set; }
        }
        public class ReconcilationReportOverview
        {
            public double TotalPrincipalAmount { get; set; }
            public double TotalInterest { get; set; }
            public double TotalAmount { get; set; }
            public double TUCFees { get; set; }
        }
        public class BNPLOverview
        {
            public long TotalLoanRequest { get; set; }
            public long ActiveLoans { get; set; }
            public double? PendingSettlements { get; set; }
            public double? TUCRevenue { get; set; }
        }
        public class DisbursmentDistributionOverview
        {
            public double? TotalDisbursed { get; set; }
            public double? MerchantSettlement { get; set; }
            public double? TUCFees { get; set; }
        }

        public class RepaymentStatusOverview
        {
            public double OverDueAmount { get; set; }
            public long OverDueCount { get; set; }
            public double PaidAmount { get; set; }
            public long PaidCount { get; set; }
            public double UpComingAmount { get; set; }
            public long UpComingCount { get; set; }
        }
        public class LoanDisbursementStaticByProviderOverview
        {                    
            public long TotalDisbursedLoans { get; set; }
            public double TotalDisburedAmount { get; set; }          
            public double TotalInterestAmount { get; set; }
            public List<ProviderReferenceStatistic> providerReferenceStatistics { get; set; }   
           
        }
        public class ProviderReferenceStatistic {
            public string? ProviderName{ get; set; }
            public long DisbursedLoan { get; set; }
            public double PrincipalAmount { get; set; }
            public double InterestAmount { get; set; }
            public double Amt { get; set; }
            public long RefrenceId { get; set; }
        }

        public class EligibleCustomerByDateOverview {
            public double TotalCustomer { get; set; }
            public double UsedCredits { get; set; }
            public double NotUsedCredit { get; set; }
        }
    }
}
