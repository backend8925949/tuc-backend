//==================================================================================
// FileName: FrameworkAccountOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to account operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Loan.Object;
using HCore.TUC.Plugins.Loan.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.Loan.Framework
{
    public class FrameworkAccountOperations
    {
        HCoreContext _HCoreContext;
        OAccountLoanOperation.Overview.Response _Overview;

        /// <summary>
        /// Description: Gets the account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccount(OAccountOperation.Request _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _AccountDetails = _HCoreContext.BNPLAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OAccountOperation.Request
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AccountId = x.Account.Id,
                            AccountKey = x.Account.Guid,
                            AccountDisplayName = x.Account.DisplayName,
                            AccountTypeId = x.Account.AccountType.Id,
                            AccountTypeCode = x.Account.AccountType.SystemName,
                            AccountTypeName = x.Account.AccountType.Name,

                            ProviderReference = x.ProviderReference,
                            FirstName = x.FirstName,
                            LastName = x.LastName,
                            EmailAddress = x.EmailAddress,
                            WorkEmailAddress = x.WorkEmailAddress,
                            MobileNumber = x.MobileNumber,
                            DateOfBirth = x.DateOfBirth,
                            BvnNumber = x.BvnNumber,
                            Salary = x.Salary,
                            SalaryDay = x.SalaryDay,
                            Address = x.Address,
                            StateName = x.StateName,
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,
                            LgaName = x.LgaName,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();

                    if (_AccountDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AccountDetails, ResponseCode.BNPL0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccount", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccounts(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.BNPLAccount
                                                 .Select(x => new OAccountOperation.Request
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountId = x.Account.Id,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     AccountIconUrl = x.Account.IconStorage.Path,
                                                     AccountTypeId = x.Account.AccountType.Id,
                                                     AccountTypeCode = x.Account.AccountType.SystemName,
                                                     AccountTypeName = x.Account.AccountType.Name,
                                                     AccountStatusId = x.Account.StatusId,
                                                     AccountStatusCode = x.Account.Status.SystemName,
                                                     AccountStatusName = x.Account.Status.Name,
                                                     LastActivityDate = x.Account.LastActivityDate,
                                                     ActiveLoans = x.BNPLAccountLoan.Count(a => a.AccountId == x.Id && a.StatusId == BNPLHelper.StatusHelper.Loan.Running),
                                                     CreditLimit = 700000,
                                                     ApprovedDate = "2021-03-07T16:30:02",

                                                     Name = x.FirstName + " " + x.LastName,
                                                     EmailAddress = x.EmailAddress,
                                                     WorkEmailAddress = x.WorkEmailAddress,
                                                     MobileNumber = x.MobileNumber,
                                                     DateOfBirth = x.DateOfBirth,
                                                     BvnNumber = x.BvnNumber,
                                                     Salary = x.Salary,
                                                     SalaryDay = x.SalaryDay,
                                                     Address = x.Address,
                                                     StateName = x.StateName,

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();

                    }
                    List<OAccountOperation.Request> Data = _HCoreContext.BNPLAccount
                                                 .Select(x => new OAccountOperation.Request
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountId = x.Account.Id,
                                                     AccountKey = x.Account.Guid,
                                                     AccountDisplayName = x.Account.DisplayName,
                                                     AccountIconUrl = x.Account.IconStorage.Path,
                                                     AccountTypeId = x.Account.AccountType.Id,
                                                     AccountTypeCode = x.Account.AccountType.SystemName,
                                                     AccountTypeName = x.Account.AccountType.Name,
                                                     AccountStatusId = x.Account.StatusId,
                                                     AccountStatusCode = x.Account.Status.SystemName,
                                                     AccountStatusName = x.Account.Status.Name,
                                                     LastActivityDate = x.Account.LastActivityDate,
                                                     ActiveLoans = x.BNPLAccountLoan.Count(a => a.AccountId == x.Id && a.StatusId == BNPLHelper.StatusHelper.Loan.Running),
                                                     CreditLimit = 700000,
                                                     ApprovedDate = "2021-03-07T16:30:02",

                                                     Name = x.FirstName + " " + x.LastName,
                                                     EmailAddress = x.EmailAddress,
                                                     WorkEmailAddress = x.WorkEmailAddress,
                                                     MobileNumber = x.MobileNumber,
                                                     DateOfBirth = x.DateOfBirth,
                                                     BvnNumber = x.BvnNumber,
                                                     Salary = x.Salary,
                                                     SalaryDay = x.SalaryDay,
                                                     Address = x.Address,
                                                     StateName = x.StateName,

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();

                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                        {
                            DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                        }
                        else
                        {
                            DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccounts", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account loan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountLoan(OAccountLoanOperation.Request _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _AccountLoanDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OAccountLoanOperation.Request
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AccountId = x.Account.Id,
                            AccountKey = x.Account.Guid,
                            AccountName = x.Account.FirstName + " " + x.Account.LastName,
                            AccountMobileNumber = x.Account.MobileNumber,
                            AccountEmailAddress = x.Account.EmailAddress,

                            PlanId = x.PlanId,
                            PlanKey = x.Plan.Guid,
                            PlanName = x.Plan.Name,
                            PlanPolicy = x.Plan.Policy,
                            Tenture = x.Plan.Tenture,

                            Amount = x.Amount,
                            Charge = x.Charge,
                            TotalAmount = x.TotalAmount,
                            StartDate = x.StartDate,
                            EndDate = x.EndDate,
                            LoanCode = x.LoanCode,
                            LoanPin = x.LoanPin,
                            IsRedeemed = x.IsRedeemed,
                            RedeemDate = x.RedeemDate,
                            RedeemLocationId = x.RedeemLocationId,
                            RedeemTransactionId = x.RedeemTransactionId,
                            TerminalId = x.TerminalId,
                            CashierId = x.CashierId,
                            CreditTransactionId = x.CreditTransactionId,

                            MerchantId = x.MerchantId,
                            MerchantKey = x.Merchant.Guid,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();

                    if (_AccountLoanDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AccountLoanDetails, ResponseCode.BNPL0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountLoan", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account loan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountLoans(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                 .Where(x => x.Account.AccountId == _Request.AccountId)
                                                 .Select(x => new OAccountLoanOperation.Request
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountId = x.Account.Id,
                                                     AccountKey = x.Account.Guid,
                                                     AccountName = x.Account.FirstName + " " + x.Account.LastName,
                                                     AccountMobileNumber = x.Account.MobileNumber,

                                                     PlanId = x.PlanId,
                                                     PlanKey = x.Plan.Guid,
                                                     PlanName = x.Plan.Name,

                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     TerminalId = x.TerminalId,
                                                     CashierId = x.CashierId,

                                                     MerchantId = x.MerchantId,
                                                     MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                     MerchantAddress = x.Merchant.Account.Address,

                                                     ProviderId = x.ProviderId,
                                                     ProviderName = x.Provider.Name,
                                                     ProviderIconUrl = x.Provider.IconStorage.Path,

                                                     TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(a => a.InterestAmount),
                                                     InterestRate = x.Plan.ProviderInterestRate,
                                                     PaidInstallments = x.BNPLAccountLoanPayment.Count(c => c.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                     TotalInstallments = x.BNPLAccountLoanPayment.Count(),
                                                     TotalPaidAmount = x.BNPLAccountLoanPayment.Where(b => b.LoanId == x.Id && b.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(b => b.Amount),

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                                 .Where(_Request.SearchCondition)
                                                 .Count();

                    }
                    List<OAccountLoanOperation.Request> Data = _HCoreContext.BNPLAccountLoan
                                                 .Where(x => x.Account.AccountId == _Request.AccountId)
                                                 .Select(x => new OAccountLoanOperation.Request
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountId = x.Account.Id,
                                                     AccountKey = x.Account.Guid,
                                                     AccountName = x.Account.FirstName + " " + x.Account.LastName,
                                                     AccountMobileNumber = x.Account.MobileNumber,

                                                     PlanId = x.PlanId,
                                                     PlanKey = x.Plan.Guid,
                                                     PlanName = x.Plan.Name,

                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     TotalAmount = x.TotalAmount,
                                                     StartDate = x.StartDate,
                                                     EndDate = x.EndDate,
                                                     TerminalId = x.TerminalId,
                                                     CashierId = x.CashierId,

                                                     MerchantId = x.MerchantId,
                                                     MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                     MerchantAddress = x.Merchant.Account.Address,

                                                     ProviderId = x.ProviderId,
                                                     ProviderName = x.Provider.Name,
                                                     ProviderIconUrl = x.Provider.IconStorage.Path,

                                                     TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(a => a.InterestAmount),
                                                     InterestRate = x.Plan.ProviderInterestRate,
                                                     PaidInstallments = x.BNPLAccountLoanPayment.Count(c => c.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                     TotalInstallments = x.BNPLAccountLoanPayment.Count(),
                                                     TotalPaidAmount = x.BNPLAccountLoanPayment.Where(b => b.LoanId == x.Id && b.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(b => b.Amount),

                                                     CreateDate = x.CreateDate,
                                                     CreatedById = x.CreatedById,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyById = x.ModifyById,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountLoans", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountLoansOverview(OAccountLoanOperation.Overview.Request _Request)
        {
            try
            {
                _Overview = new OAccountLoanOperation.Overview.Response();
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _Overview.AllLoans = _HCoreContext.BNPLAccountLoan.Count(x => x.Account.AccountId == _Request.AccountId);
                    _Overview.ActveLoans = _HCoreContext.BNPLAccountLoan.Count(x => x.Account.AccountId == _Request.AccountId && x.StatusId == BNPLHelper.StatusHelper.Loan.Running);
                    _Overview.ActiveLoanAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.AccountId == _Request.AccountId && x.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(a => a.Amount);
                    _Overview.ClosedLoans = _HCoreContext.BNPLAccountLoan.Count(x => x.Account.AccountId == _Request.AccountId && x.StatusId == BNPLHelper.StatusHelper.Loan.Closed);
                    _Overview.ClosedLoanAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.AccountId == _Request.AccountId && x.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Sum(a => a.Amount);

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "BNPL0200", "details Loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAccountLoansOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
        }
    }
}
