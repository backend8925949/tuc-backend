//==================================================================================
// FileName: FrameworkMerchantOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to merchant operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Loan.Object;
using HCore.TUC.Plugins.Loan.Resource;
using System;
using Akka.Actor;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Plugins.Loan.Framework
{
    public class FrameworkMerchantOperation
    {
        HCoreContext _HCoreContext;
        List<OMerchant.ListDownload> _MerchantsDownload;
        List<OMerchant.MerchantSettlementListDownload> _MerchantSettlementListDownloads;
        List<OMerchant.RedeemedLoansDownload> _RedeemedLoansDownloads;
        BNPLMerchantSettlement _BNPLMerchantSettlement;
        OMerchant.SettlementOverview _SettlementOverview;
        OMerchant.SettlementDetails _SettlementDetails;
        OMerchant.Overview _Overview;
        OMerchant.OverviewLite _OverviewLite;

        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetMerchantDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetMerchantDownload>("ActorGetMerchantDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLMerchant
                            .Where(x => x.Account.StatusId == HelperStatus.Default.Active
                                        && x.Account.CountryId == _Request.UserReference.CountryId
                                                && x.IsMerchantSettelment == 1)
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OMerchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantName = x.Account.DisplayName,
                                                    MerchantLogoURL = x.Account.IconStorage.Path,
                                                    MerchantAddress = x.Account.Address,
                                                    ActiveLoans = x.BNPLAccountLoan.Where(a => a.MerchantId == x.Id && a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Count(),
                                                    TotalActiveAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Amount),

                                                    ClosedLoans = x.BNPLAccountLoan.Where(m => m.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Count(),
                                                    TotalClosedAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Sum(y => y.Amount),

                                                    TotalInterest = x.BNPLAccountLoan.Where(q => q.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Charge),
                                                    TUCFees = x.BNPLAccountLoan.Where(x => x.Merchant.StatusId == HelperStatus.Default.Active && x.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.BNPLMerchantSettlement.Sum(w => w.TucFees)),
                                                    MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                    .Sum(z => z.BNPLAccountLoanPayment.Sum(a => a.ProviderAmount)),
                                                    MerchantSettleStatus = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                    .Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                    PrimaryCategoryKey = x.Account.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.Account.PrimaryCategory.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OMerchant.List> _List = _HCoreContext.BNPLMerchant
                                                .Where(x => x.Account.StatusId == HelperStatus.Default.Active
                                                && x.Account.CountryId == _Request.UserReference.CountryId
                                                && x.IsMerchantSettelment == 1)
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OMerchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantName = x.Account.DisplayName,
                                                    MerchantLogoURL = x.Account.IconStorage.Path,
                                                    MerchantAddress = x.Account.Address,
                                                    ActiveLoans = x.BNPLAccountLoan.Where(a => a.MerchantId == x.Id && a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Count(),
                                                    TotalActiveAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Amount),

                                                    ClosedLoans = x.BNPLAccountLoan.Where(m => m.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Count(),
                                                    TotalClosedAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Sum(y => y.Amount),

                                                    TotalInterest = x.BNPLAccountLoan.Where(q => q.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Charge),
                                                    TUCFees = x.BNPLAccountLoan.Where(x => x.Merchant.StatusId == HelperStatus.Default.Active && x.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.BNPLMerchantSettlement.Sum(w => w.TucFees)),
                                                    MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                    .Sum(z => z.BNPLAccountLoanPayment.Sum(a => a.ProviderAmount)),
                                                    MerchantSettleStatus = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                    .Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                    PrimaryCategoryKey = x.Account.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.Account.PrimaryCategory.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.MerchantLogoURL))
                        {
                            DataItem.MerchantLogoURL = _AppConfig.StorageUrl + DataItem.MerchantLogoURL;
                        }
                        else
                        {
                            DataItem.MerchantLogoURL = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchantList", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant list overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantListOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OSummaryOverview.MerchantOverview data = new OSummaryOverview.MerchantOverview();
                    data.ActiveLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.StatusId == HelperStatus.Default.Active && x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Account.Account.CountryId == _Request.UserReference.CountryId).Count();
                    data.ClosedLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.StatusId == HelperStatus.Default.Active && x.StatusId == BNPLHelper.StatusHelper.Loan.Closed && x.Account.Account.CountryId == _Request.UserReference.CountryId).Count();
                    data.TotalMerchants = _HCoreContext.BNPLMerchant.Where(x => x.Account.StatusId == HelperStatus.Default.Active && x.IsMerchantSettelment == 1 && x.Account.CountryId == _Request.UserReference.CountryId).Distinct().Count();
                    data.TUCFees = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.StatusId == HelperStatus.Default.Active && x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Account.Account.CountryId == _Request.UserReference.CountryId).Sum(z => z.BNPLMerchantSettlement.Sum(w => w.TucFees));
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, data, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchantListOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetMerchantDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _MerchantsDownload = new List<OMerchant.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.BNPLMerchant
                            .Where(x => x.Account.StatusId == HelperStatus.Default.Active 
                                            && x.StatusId == HelperStatus.Default.Active
                                            && x.Account.CountryId == _Request.UserReference.CountryId)
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OMerchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantName = x.Account.DisplayName,
                                                    MerchantLogoURL = x.Account.IconStorage.Path,
                                                    MerchantAddress = x.Account.Address,
                                                    ActiveLoans = x.BNPLAccountLoan.Where(a => a.MerchantId == x.Id && a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Count(),
                                                    TotalActiveAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Amount),

                                                    ClosedLoans = x.BNPLAccountLoan.Where(m => m.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Count(),
                                                    TotalClosedAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Sum(y => y.Amount),

                                                    TotalInterest = x.BNPLAccountLoan.Where(q => q.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Charge),
                                                    TUCFees = x.BNPLAccountLoan.Where(r => r.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.BNPLAccountLoanPayment.Sum(a => a.SystemAmount)),
                                                    MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.BNPLAccountLoanPayment.Sum(a => a.ProviderAmount)),
                                                    MerchantSettleStatus = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                    PrimaryCategoryKey = x.Account.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.Account.PrimaryCategory.Name
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.BNPLMerchant
                                                .Where(x => x.Account.StatusId == HelperStatus.Default.Active 
                                                        && x.StatusId == HelperStatus.Default.Active
                                                        && x.Account.CountryId == _Request.UserReference.CountryId)
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OMerchant.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantName = x.Account.DisplayName,
                                                    MerchantLogoURL = x.Account.IconStorage.Path,
                                                    MerchantAddress = x.Account.Address,
                                                    ActiveLoans = x.BNPLAccountLoan.Where(a => a.MerchantId == x.Id && a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Count(),
                                                    TotalActiveAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Amount),
                                                    ClosedLoans = x.BNPLAccountLoan.Where(m => m.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Count(),
                                                    TotalClosedAmount = x.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed).Sum(y => y.Amount),
                                                    TotalInterest = x.BNPLAccountLoan.Where(q => q.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.Charge),
                                                    TUCFees = x.BNPLAccountLoan.Where(r => r.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.BNPLAccountLoanPayment.Sum(a => a.SystemAmount)),
                                                    MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(z => z.BNPLAccountLoanPayment.Sum(a => a.ProviderAmount)),
                                                    MerchantSettleStatus = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                    PrimaryCategoryKey = x.Account.PrimaryCategory.SystemName,
                                                    PrimaryCategoryName = x.Account.PrimaryCategory.Name
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _MerchantsDownload.Add(new OMerchant.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                ReferenceKey = _DataItem.ReferenceKey,
                                MerchantName = _DataItem.MerchantName,
                                MerchantAddress = _DataItem.MerchantAddress,
                                MerchantLogoURL = _DataItem.MerchantLogoURL,
                                MerchantSettlement = _DataItem.MerchantSettlement,
                                ActiveLoans = _DataItem.ActiveLoans,
                                TotalActiveAmount = _DataItem.TotalActiveAmount,
                                ClosedLoans = _DataItem.ClosedLoans,
                                TotalClosedAmount = _DataItem.TotalClosedAmount
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("BNPLMerchants_List", _MerchantsDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchantDownload", _Exception);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the pending loans.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPendingLoans(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                .Where(x => x.Merchant.AccountId == _Request.ReferenceId
                                                && x.Merchant.Account.Guid == _Request.ReferenceKey
                                                && x.IsRedeemed == 0
                                                && x.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                .Select(x => new OMerchant.PendingLoans
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CustomerAccountId = x.Account.AccountId,
                                                    CustomerAccountKey = x.Account.Account.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,
                                                    IconURL = x.Account.Account.IconStorage.Path,

                                                    LoanAmount = x.Amount,
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Count(),
                                                    TUCFees = x.Charge,
                                                    ApprovedOn = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(a => a.Loan_Approved_Date).FirstOrDefault(),
                                                    TUCAmount = 0,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByName = x.CreatedBy.DisplayName
                                                })
                                               .Where(_Request.SearchCondition)
                                               .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OMerchant.PendingLoans> _List = _HCoreContext.BNPLAccountLoan
                                                         .Where(x => x.Merchant.AccountId == _Request.ReferenceId
                                                         && x.Merchant.Account.Guid == _Request.ReferenceKey
                                                         && x.IsRedeemed == 0
                                                         && x.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                         .Select(x => new OMerchant.PendingLoans
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             CustomerAccountId = x.Account.AccountId,
                                                             CustomerAccountKey = x.Account.Account.Guid,
                                                             CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                             CustomerNumber = x.Account.MobileNumber,
                                                             IconURL = x.Account.Account.IconStorage.Path,

                                                             LoanAmount = x.Amount,
                                                             TotalInstallments = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Count(),
                                                             TUCFees = x.Charge,
                                                             ApprovedOn = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(a => a.Loan_Approved_Date).FirstOrDefault(),
                                                             TUCAmount = 0,
                                                             CreateDate = x.CreateDate,
                                                             CreatedById = x.CreatedById,
                                                             CreatedByName = x.CreatedBy.DisplayName
                                                         })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        //if(DataItem.TUCFees < 1)
                        //{
                        //    DataItem.TUCFees = (DataItem.LoanAmount / 100) * 2;
                        //}
                        if (DataItem.TUCAmount < 1)
                        {
                            DataItem.TUCAmount = DataItem.LoanAmount - DataItem.TUCFees;
                        }
                        if (!string.IsNullOrEmpty(DataItem.IconURL))
                        {
                            DataItem.IconURL = _AppConfig.StorageUrl + DataItem.IconURL;
                        }
                        else
                        {
                            DataItem.IconURL = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetPendingLoans", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the pending loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPendingLoansOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                _Overview = new OMerchant.Overview();
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _Overview.PendingLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 0).Count();
                    _Overview.PendingLoansAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 0).Sum(a => a.Amount);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "BNPL0200", ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPendingLoansOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the redeemed loans.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemedLoans(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetRedeemedLoansDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetRedeemedLoansDownload>("ActorGetRedeemedLoansDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                .Where(x => x.Merchant.AccountId == _Request.ReferenceId
                                                && x.Merchant.Account.Guid == _Request.ReferenceKey
                                                && x.IsRedeemed == 1
                                                && x.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                .Select(x => new OMerchant.RedeemedLoans
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CustomerAccountId = x.Account.AccountId,
                                                    CustomerAccountKey = x.Account.Account.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,

                                                    LoanAmount = x.Amount,
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Count(),
                                                    TUCFees = x.Charge,
                                                    TotalAmount = x.TotalAmount,
                                                    ApprovedOn = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(a => a.Loan_Approved_Date).FirstOrDefault(),
                                                    RedeemedOn = x.RedeemDate,
                                                    SettlementAmount = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementAmount).FirstOrDefault(),
                                                    SettlementDate = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementDate).FirstOrDefault(),
                                                    StatusId = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.StatusId).FirstOrDefault(),
                                                    StatusCode = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault(),
                                                    StatusName = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.Name).FirstOrDefault(),
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByName = x.CreatedBy.DisplayName

                                                }).Where(_Request.SearchCondition)
                                                  .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OMerchant.RedeemedLoans> _List = _HCoreContext.BNPLAccountLoan
                                                          .Where(x => x.Merchant.AccountId == _Request.ReferenceId
                                                          && x.Merchant.Account.Guid == _Request.ReferenceKey
                                                          && x.IsRedeemed == 1
                                                          && x.StatusId == BNPLHelper.StatusHelper.Loan.Running)
                                                          .Select(x => new OMerchant.RedeemedLoans
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              CustomerAccountId = x.Account.AccountId,
                                                              CustomerAccountKey = x.Account.Account.Guid,
                                                              CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                              CustomerNumber = x.Account.MobileNumber,

                                                              LoanAmount = x.Amount,
                                                              TotalInstallments = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Count(),
                                                              TUCFees = x.Charge,
                                                              TotalAmount = x.TotalAmount,
                                                              ApprovedOn = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(a => a.Loan_Approved_Date).FirstOrDefault(),
                                                              RedeemedOn = x.RedeemDate,
                                                              SettlementAmount = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementAmount).FirstOrDefault(),
                                                              SettlementDate = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementDate).FirstOrDefault(),
                                                              StatusId = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.StatusId).FirstOrDefault(),
                                                              StatusCode = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault(),
                                                              StatusName = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.Name).FirstOrDefault(),
                                                              CreateDate = x.CreateDate,
                                                              CreatedById = x.CreatedById,
                                                              CreatedByName = x.CreatedBy.DisplayName

                                                          }).Where(_Request.SearchCondition)
                                                          .OrderBy(_Request.SortExpression)
                                                          .Skip(_Request.Offset)
                                                          .Take(_Request.Limit)
                                                          .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        if (DataItem.TUCFees > 0)
                        {
                            DataItem.TUCFeesPercentage = (DataItem.TUCFees / DataItem.TotalAmount) * 100;
                        }
                        else
                        {
                            DataItem.TUCFeesPercentage = 0;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetRedeemedLoans", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the redeemed loans download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRedeemedLoansDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _RedeemedLoansDownloads = new List<OMerchant.RedeemedLoansDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                            .Where(x => x.Merchant.AccountId == _Request.ReferenceId
                                            && x.Merchant.Account.Guid == _Request.ReferenceKey
                                            && x.IsRedeemed == 1)
                                            .Select(x => new OMerchant.RedeemedLoans
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                CustomerAccountId = x.Account.AccountId,
                                                CustomerAccountKey = x.Account.Account.Guid,
                                                CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                CustomerNumber = x.Account.MobileNumber,

                                                LoanAmount = x.Amount,
                                                TotalInstallments = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Count(),
                                                TUCFees = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(w => w.SystemAmount),
                                                TotalAmount = x.TotalAmount,
                                                ApprovedOn = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(a => a.Loan_Approved_Date).FirstOrDefault(),
                                                RedeemedOn = x.RedeemDate,
                                                SettlementAmount = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementAmount).FirstOrDefault(),
                                                SettlementDate = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementDate).FirstOrDefault(),
                                                StatusId = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.StatusId).FirstOrDefault(),
                                                StatusCode = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault(),
                                                StatusName = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.Name).FirstOrDefault()

                                            }).Where(_Request.SearchCondition)
                                              .Count();
                    #endregion
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.BNPLAccountLoan
                                                          .Where(x => x.Merchant.AccountId == _Request.ReferenceId
                                                          && x.Merchant.Account.Guid == _Request.ReferenceKey
                                                          && x.IsRedeemed == 1)
                                                          .Select(x => new OMerchant.RedeemedLoans
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              CustomerAccountId = x.Account.AccountId,
                                                              CustomerAccountKey = x.Account.Account.Guid,
                                                              CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                              CustomerNumber = x.Account.MobileNumber,

                                                              LoanAmount = x.Amount,
                                                              TotalInstallments = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Count(),
                                                              TUCFees = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(w => w.SystemAmount),
                                                              TotalAmount = x.TotalAmount,
                                                              ApprovedOn = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(a => a.Loan_Approved_Date).FirstOrDefault(),
                                                              RedeemedOn = x.RedeemDate,
                                                              SettlementAmount = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementAmount).FirstOrDefault(),
                                                              SettlementDate = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.SettlementDate).FirstOrDefault(),
                                                              StatusId = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.StatusId).FirstOrDefault(),
                                                              StatusCode = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.SystemName).FirstOrDefault(),
                                                              StatusName = x.BNPLMerchantSettlement.Where(a => a.LoanId == x.Id).Select(a => a.Status.Name).FirstOrDefault()

                                                          }).Where(_Request.SearchCondition)
                                                          .OrderBy(_Request.SortExpression)
                                                          .Skip(_Request.Offset)
                                                          .Take(_Request.Limit)
                                                          .ToList();

                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _RedeemedLoansDownloads.Add(new OMerchant.RedeemedLoansDownload
                            {
                                LoanId = _DataItem.ReferenceId,
                                CustomerDisplayName = _DataItem.CustomerDisplayName,
                                CustomerNumber = _DataItem.CustomerNumber,
                                ApprovedOn = _DataItem.ApprovedOn,
                                RedeemedOn = _DataItem.RedeemedOn,
                                LoanAmount = _DataItem.LoanAmount,
                                TotalInstallments = _DataItem.TotalInstallments,
                                TUCFeesPercentage = _DataItem.TUCFeesPercentage,
                                TUCFees = _DataItem.TUCFees,
                                SettlementAmount = _DataItem.SettlementAmount,
                                StatusName = _DataItem.StatusName,
                                SettlementDate = _DataItem.SettlementDate
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("BNPLRedeemedLoans_List", _RedeemedLoansDownloads, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemedLoansDownload", _Exception);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the redeemed loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRedeemedLoansOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                _Overview = new OMerchant.Overview();
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _Overview.RedeemedLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && x.IsRedeemed == 1).Count();
                    _Overview.RedeemedLoansAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && x.IsRedeemed == 1).Sum(a => a.Amount);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "BNPL0200", ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRedeemedLoansOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the loan overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoanOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                _Overview = new OMerchant.Overview();
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    _Overview.TotalLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate)).Count();
                    _Overview.TotalLoansAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate)).Sum(a => a.Amount);
                    _Overview.TotalRedeemedLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && x.IsRedeemed == 1 && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate)).Count();
                    _Overview.TotalRedeemedLoansAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && x.IsRedeemed == 1 && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate)).Sum(a => a.Amount);
                    _Overview.TotalPendingSettlementAmount = _HCoreContext.BNPLMerchantSettlement.Where(x => x.Merchant.AccountId == _Request.ReferenceId && x.Merchant.Account.Guid == _Request.ReferenceKey && x.StatusId != BNPLHelper.StatusHelper.MerchantSetelment.Successful && (x.CreateDate >= _Request.StartDate && x.CreateDate <= _Request.EndDate)).Sum(a => a.SettlementAmount);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "BNPL0200", ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetLoanOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the settlement list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSettlementList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetSettlementListDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetSettlementListDownload>("ActorGetSettlementListDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLMerchantSettlement
                                                  .Select(x => new OMerchant.SettlementList
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      TransactionId = x.TransactionReference,
                                                      TransactionReference = x.TransactionReferenceNavigation.ReferenceNumber,

                                                      LoanId = x.LoanId,
                                                      LoanKey = x.Loan.Guid,
                                                      LoanAmount = x.Loan.Amount,
                                                      LoanRedeemDate = x.LoanRedeenDate,

                                                      ProviderId = x.ProviderId,
                                                      ProviderName = x.Provider.Name,
                                                      ProviderIconUrl = x.Provider.IconStorage.Path,

                                                      MerchantId = x.MerchantId,
                                                      MerchantKey = x.Merchant.Guid,
                                                      MerchantAccountId = x.Merchant.AccountId,
                                                      MerchantAccountKey = x.Merchant.Account.Guid,
                                                      MerchantAccountName = x.Merchant.Account.DisplayName,
                                                      MerchantAccountAddress = x.Merchant.Account.Address,
                                                      MerchantIconUrl = x.Merchant.Account.IconStorage.Path,

                                                      CustomerName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                      CustomerMobileNumber = x.Loan.Account.MobileNumber,
                                                      CustomerEmailAddress = x.Loan.Account.EmailAddress,

                                                      TUCFees = x.TucFees,
                                                      SettlementDate = x.SettlementDate,
                                                      SettlementAmount = x.SettlementAmount,
                                                      StatusId = x.StatusId,
                                                      StatusName = x.Status.Name,
                                                      StatusCode = x.Status.SystemName,
                                                      CreateDate = x.CreateDate,
                                                      CreatedById = x.CreatedById,
                                                      CreatedByName = x.CreatedBy.DisplayName
                                                  }).
                                                  Where(_Request.SearchCondition)
                                                  .Count();


                        #endregion
                    }
                    #region Get Data
                    List<OMerchant.SettlementList> _List = _HCoreContext.BNPLMerchantSettlement
                                                  .Select(x => new OMerchant.SettlementList
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      TransactionId = x.TransactionReference,
                                                      TransactionReference = x.TransactionReferenceNavigation.ReferenceNumber,

                                                      LoanId = x.LoanId,
                                                      LoanKey = x.Loan.Guid,
                                                      LoanAmount = x.Loan.Amount,
                                                      LoanRedeemDate = x.LoanRedeenDate,

                                                      ProviderId = x.ProviderId,
                                                      ProviderName = x.Provider.Name,
                                                      ProviderIconUrl = x.Provider.IconStorage.Path,

                                                      MerchantId = x.MerchantId,
                                                      MerchantKey = x.Merchant.Guid,
                                                      MerchantAccountId = x.Merchant.AccountId,
                                                      MerchantAccountKey = x.Merchant.Account.Guid,
                                                      MerchantAccountName = x.Merchant.Account.DisplayName,
                                                      MerchantAccountAddress = x.Merchant.Account.Address,
                                                      MerchantIconUrl = x.Merchant.Account.IconStorage.Path,

                                                      CustomerName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                      CustomerMobileNumber = x.Loan.Account.MobileNumber,
                                                      CustomerEmailAddress = x.Loan.Account.EmailAddress,

                                                      TUCFees = x.TucFees,
                                                      SettlementDate = x.SettlementDate,
                                                      SettlementAmount = x.SettlementAmount,
                                                      StatusId = x.StatusId,
                                                      StatusName = x.Status.Name,
                                                      StatusCode = x.Status.SystemName,
                                                      CreateDate = x.CreateDate,
                                                      CreatedById = x.CreatedById,
                                                      CreatedByName = x.CreatedBy.DisplayName
                                                  })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();


                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSettlementList", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant settlement.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantSettlement(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    _SettlementDetails = new OMerchant.SettlementDetails();
                    _SettlementDetails = _HCoreContext.BNPLMerchantSettlement.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OMerchant.SettlementDetails
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            TransactionId = x.TransactionReference,
                            TransactionReference = x.TransactionReferenceNavigation.ReferenceNumber,
                            InvoiceNumber = x.InvoiceNumber,
                            LoanId = x.LoanId,
                            LoanKey = x.Loan.Guid,
                            LoanAmount = x.LoanAmount,
                            LoanRedeemDate = x.LoanRedeenDate,
                            ProviderId = x.ProviderId,
                            ProviderKey = x.Provider.Guid,
                            ProviderName = x.Provider.Name,
                            ProviderAddress = x.Provider.Address,
                            ProviderMobileNumber = x.Provider.MobileNumber,
                            ProviderEmailAddress = x.Provider.EmailAddress,
                            MerchantId = x.MerchantId,
                            MerchantKey = x.Merchant.Guid,
                            MerchantAccountId = x.Merchant.AccountId,
                            MerchantAccountKey = x.Merchant.Account.Guid,
                            MerchantAccountName = x.Merchant.Account.DisplayName,
                            MerchantAccountAddress = x.Merchant.Account.Address,
                            MerchantAccountEmailAddress = x.Merchant.Account.EmailAddress,
                            MerchantAccountMobileNumber = x.Merchant.Account.MobileNumber,
                            SettlementAmount = x.SettlementAmount,
                            SettlementDate = x.SettlementDate,
                            TUCFees = x.TucFees,
                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByName = x.CreatedBy.Name,
                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        }).FirstOrDefault();

                    if (_SettlementDetails != null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SettlementDetails, "BNPL0123", "Details loaded successfully.");
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchantSettlement", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
        }
        /// <summary>
        /// Description: Gets the merchant settlement download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetMerchantSettlementDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _MerchantSettlementListDownloads = new List<OMerchant.MerchantSettlementListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.BNPLMerchantSettlement
                                                  .Select(x => new OMerchant.SettlementList
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      TransactionId = x.TransactionReference,
                                                      TransactionReference = x.TransactionReferenceNavigation.ReferenceNumber,

                                                      LoanId = x.LoanId,
                                                      LoanKey = x.Loan.Guid,
                                                      LoanAmount = x.Loan.Amount,

                                                      MerchantId = x.MerchantId,
                                                      MerchantKey = x.Merchant.Guid,
                                                      MerchantAccountId = x.Merchant.AccountId,
                                                      MerchantAccountKey = x.Merchant.Account.Guid,

                                                      TUCFees = x.TucFees,
                                                      SettlementDate = x.SettlementDate,
                                                      SettlementAmount = x.SettlementAmount,
                                                      StatusId = x.StatusId,
                                                      StatusName = x.Status.Name,
                                                      StatusCode = x.Status.SystemName,

                                                      MerchantLogoURL = x.Merchant.Account.IconStorage.Path,
                                                      MerchantName = x.Merchant.Account.Name,
                                                      ProviderName = x.Provider.Name,
                                                      ProviderId = x.ProviderId,
                                                      ProviderLogoURL = x.Provider.IconStorage.Path,
                                                      CustomerName = x.Loan.Account.FirstName + x.Loan.Account.LastName,
                                                      MobileNumber = x.Loan.Account.MobileNumber,
                                                      RedeemedOn = x.Loan.RedeemDate

                                                  }).
                                                  Where(_Request.SearchCondition)
                                                  .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.BNPLMerchantSettlement
                                                  .Select(x => new OMerchant.SettlementList
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      LoanId = x.LoanId,
                                                      LoanKey = x.Loan.Guid,
                                                      LoanAmount = x.Loan.Amount,

                                                      MerchantId = x.MerchantId,
                                                      MerchantKey = x.Merchant.Guid,
                                                      MerchantAccountId = x.Merchant.AccountId,
                                                      MerchantAccountKey = x.Merchant.Account.Guid,

                                                      TUCFees = x.TucFees,
                                                      SettlementDate = x.SettlementDate,
                                                      SettlementAmount = x.SettlementAmount,
                                                      StatusId = x.StatusId,
                                                      StatusName = x.Status.Name,
                                                      StatusCode = x.Status.SystemName,
                                                      MerchantLogoURL = x.Merchant.Account.IconStorage.Path,
                                                      MerchantName = x.Merchant.Account.Name,
                                                      ProviderName = x.Provider.Name,
                                                      ProviderId = x.ProviderId,
                                                      ProviderLogoURL = x.Provider.IconStorage.Path,
                                                      CustomerName = x.Loan.Account.FirstName + x.Loan.Account.LastName,
                                                      MobileNumber = x.Loan.Account.MobileNumber,
                                                      RedeemedOn = x.Loan.RedeemDate
                                                  })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();

                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            if (!string.IsNullOrEmpty(_DataItem.MerchantLogoURL))
                            {
                                _DataItem.MerchantLogoURL = _AppConfig.StorageUrl + _DataItem.MerchantLogoURL;
                            }
                            else
                            {
                                _DataItem.MerchantLogoURL = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_DataItem.ProviderLogoURL))
                            {
                                _DataItem.ProviderLogoURL = _AppConfig.StorageUrl + _DataItem.ProviderLogoURL;
                            }
                            else
                            {
                                _DataItem.ProviderLogoURL = _AppConfig.Default_Icon;
                            }
                            _MerchantSettlementListDownloads.Add(new OMerchant.MerchantSettlementListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                StatusName = _DataItem.StatusName,
                                MerchantName = _DataItem.MerchantName,
                                LoanId = _DataItem.LoanId,
                                ProviderName = _DataItem.ProviderName,
                                CustomerName = _DataItem.CustomerName,
                                MobileNumber = _DataItem.MobileNumber,
                                RedeemedOn = _DataItem.RedeemedOn,
                                SettlementDate = _DataItem.SettlementDate,
                                LoanAmount = _DataItem.LoanAmount,
                                TUCFees = _DataItem.TUCFees,
                                SettlementAmount = _DataItem.SettlementAmount,
                                MerchantAddress = _DataItem.MerchantAddress,
                                MerchantLogoURL = _DataItem.MerchantLogoURL,
                                ProviderLogoURL = _DataItem.ProviderLogoURL
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("BNPLMerchantSettlement_List", _MerchantSettlementListDownloads, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchantSettlementDownload", _Exception, _Request.UserReference, "BNPL0500", "Internal server error occured. Please try after some time.");
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the settlement overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSettlementOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                _SettlementOverview = new OMerchant.SettlementOverview();

                using (_HCoreContext = new HCoreContext())
                {
                    if(_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        _SettlementOverview.TotalSettlements = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.Merchant.AccountId == _Request.AccountId).Count();
                        _SettlementOverview.TotalSettlementAmount = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.Merchant.AccountId == _Request.AccountId).Sum(x => x.SettlementAmount);
                        _SettlementOverview.TotalPendingSettlements = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.Merchant.AccountId == _Request.AccountId && x.StatusId != BNPLHelper.StatusHelper.MerchantSetelment.Successful).Count();
                        _SettlementOverview.TotalPendingSettlementAmount = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.Merchant.AccountId == _Request.AccountId && x.StatusId != BNPLHelper.StatusHelper.MerchantSetelment.Successful).Sum(a => a.SettlementAmount);
                        _SettlementOverview.TotalClearSettlements = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.Merchant.AccountId == _Request.AccountId && x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Successful).Count();
                        _SettlementOverview.TotalClearSettlementAmount = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.Merchant.AccountId == _Request.AccountId && x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Successful).Sum(a => a.SettlementAmount);
                        _SettlementOverview.TotalTUCFees = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.Merchant.AccountId == _Request.AccountId).Sum(a => a.TucFees);

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SettlementOverview, "BNPL0200", ResponseCode.BNPL0200);
                        #endregion
                    }
                    else
                    {
                        _SettlementOverview.TotalSettlements = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId).Count();
                        _SettlementOverview.TotalSettlementAmount = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId).Sum(x => x.SettlementAmount);
                        _SettlementOverview.TotalPendingSettlements = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.StatusId != BNPLHelper.StatusHelper.MerchantSetelment.Successful).Count();
                        _SettlementOverview.TotalPendingSettlementAmount = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.StatusId != BNPLHelper.StatusHelper.MerchantSetelment.Successful).Sum(a => a.SettlementAmount);
                        _SettlementOverview.TotalClearSettlements = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Successful).Count();
                        _SettlementOverview.TotalClearSettlementAmount = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId && x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Successful).Sum(a => a.SettlementAmount);
                        _SettlementOverview.TotalTUCFees = _HCoreContext.BNPLMerchantSettlement.Where(x => x.TransactionReference == _Request.ReferenceId).Sum(a => a.TucFees);

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SettlementOverview, "BNPL0200", ResponseCode.BNPL0200);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetSettlementOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }



        /// <summary>
        /// Description: Gets the settlements lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSettlementsLite(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.ModeId == TransactionMode.Credit
                                                && x.TypeId == TransactionType.TUCBnpl.LoanRedeem
                                                && x.SourceId == TransactionSource.TUCBnpl
                                                && x.Account.CountryId == _Request.UserReference.CountryId
                                                && x.PaymentMethodId == PaymentMethod.Bank)
                                                .Select(x => new OMerchant.Settlementslite
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Amount = x.Amount,
                                                    ReferenceNumber = x.ReferenceNumber,
                                                    TransactionDate = x.TransactionDate,

                                                    MerchantAccountId = x.ParentId,
                                                    MerchantAccountKey = x.Parent.Guid,
                                                    MerchantAccountName = x.Parent.DisplayName,
                                                    MerchantAccountAddress = x.Parent.Address,
                                                    MerchantIconUrl = x.Parent.IconStorage.Path,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusName = x.Status.Name,
                                                    StatusCode = x.Status.SystemName,
                                                }).
                                                  Where(_Request.SearchCondition)
                                                  .Count();


                        #endregion
                    }
                    #region Get Data
                    List<OMerchant.Settlementslite> _List = _HCoreContext.HCUAccountTransaction
                                                           .Where(x => x.ModeId == TransactionMode.Credit
                                                           && x.Account.CountryId == _Request.UserReference.CountryId
                                                           && x.TypeId == TransactionType.TUCBnpl.LoanRedeem
                                                           && x.SourceId == TransactionSource.TUCBnpl
                                                           && x.PaymentMethodId == PaymentMethod.Bank)
                                                           .Select(x => new OMerchant.Settlementslite
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               Amount = x.Amount,
                                                               ReferenceNumber = x.ReferenceNumber,
                                                               TransactionDate = x.TransactionDate,

                                                               MerchantAccountId = x.ParentId,
                                                               MerchantAccountKey = x.Parent.Guid,
                                                               MerchantAccountName = x.Parent.DisplayName,
                                                               MerchantAccountAddress = x.Parent.Address,
                                                               MerchantIconUrl = x.Parent.IconStorage.Path,

                                                               CreateDate = x.CreateDate,
                                                               CreatedById = x.CreatedById,
                                                               CreatedByName = x.CreatedBy.DisplayName,

                                                               ModifyDate = x.ModifyDate,
                                                               ModifyById = x.ModifyById,
                                                               ModifyByName = x.ModifyBy.DisplayName,

                                                               StatusId = x.StatusId,
                                                               StatusName = x.Status.Name,
                                                               StatusCode = x.Status.SystemName,
                                                           })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();


                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in _List)
                    {

                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSettlementList", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the settlement overviewlite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSettlementOverviewlite(OReference _Request)
        {
            try
            {
                _OverviewLite = new OMerchant.OverviewLite();
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.AccountId > 0 && !string.IsNullOrEmpty(_Request.AccountKey))
                    {
                        _OverviewLite.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                          .Count(x => x.ParentId == _Request.AccountId
                                                          && x.Parent.Guid == _Request.AccountKey
                                                          && x.ModeId == TransactionMode.Credit
                                                          && x.Parent.CountryId == _Request.UserReference.CountryId
                                                          && x.TypeId == TransactionType.TUCBnpl.LoanRedeem
                                                          && x.SourceId == TransactionSource.TUCBnpl
                                                          && x.PaymentMethodId == PaymentMethod.Bank);
                        _OverviewLite.TotalTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(x => x.ParentId == _Request.AccountId
                                                               && x.Parent.Guid == _Request.AccountKey
                                                               && x.ModeId == TransactionMode.Credit
                                                               && x.Parent.CountryId == _Request.UserReference.CountryId
                                                               && x.TypeId == TransactionType.TUCBnpl.LoanRedeem
                                                               && x.SourceId == TransactionSource.TUCBnpl
                                                               && x.PaymentMethodId == PaymentMethod.Bank).Sum(a => a.Amount);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OverviewLite, "HC0200", "Details loaded successfully.");
                    }
                    else
                    {
                        _OverviewLite.TotalTransactions = _HCoreContext.HCUAccountTransaction
                                                          .Count(x => x.ModeId == TransactionMode.Credit
                                                          && x.Parent.CountryId == _Request.UserReference.CountryId
                                                          && x.TypeId == TransactionType.TUCBnpl.LoanRedeem
                                                          && x.SourceId == TransactionSource.TUCBnpl
                                                          && x.PaymentMethodId == PaymentMethod.Bank);
                        _OverviewLite.TotalTransactionAmount = _HCoreContext.HCUAccountTransaction
                                                               .Where(x => x.ModeId == TransactionMode.Credit
                                                               && x.Parent.CountryId == _Request.UserReference.CountryId
                                                               && x.TypeId == TransactionType.TUCBnpl.LoanRedeem
                                                               && x.SourceId == TransactionSource.TUCBnpl
                                                               && x.PaymentMethodId == PaymentMethod.Bank).Sum(a => a.Amount);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OverviewLite, "HC0200", "Details loaded successfully.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSettlementOverviewlite", _Exception, _Request.UserReference, null, "BNPL0500", ResponseCode.BNPL0500);
            }
        }

        /// <summary>
        /// Description: Settlements the invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SettlementInvoice(OMerchant.InvoiceEmail _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var Settlement = _HCoreContext.BNPLMerchantSettlement.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            TransactionId = x.TransactionReference,
                            TransactionReference = x.TransactionReferenceNavigation.ReferenceNumber,
                            InvoiceNumber = x.InvoiceNumber,
                            LoanId = x.LoanId,
                            LoanKey = x.Loan.Guid,
                            LoanAmount = x.LoanAmount,
                            LoanRedeemDate = x.LoanRedeenDate.ToString(),
                            ProviderId = x.ProviderId,
                            ProviderKey = x.Provider.Guid,
                            ProviderName = x.Provider.Name,
                            ProviderAddress = x.Provider.Address,
                            ProviderMobileNumber = x.Provider.MobileNumber,
                            ProviderEmailAddress = x.Provider.EmailAddress,
                            MerchantId = x.MerchantId,
                            MerchantKey = x.Merchant.Guid,
                            MerchantAccountId = x.Merchant.AccountId,
                            MerchantAccountKey = x.Merchant.Account.Guid,
                            MerchantAccountName = x.Merchant.Account.DisplayName,
                            MerchantAccountAddress = x.Merchant.Account.Address,
                            MerchantAccountEmailAddress = x.Merchant.Account.EmailAddress,
                            MerchantAccountMobileNumber = x.Merchant.Account.MobileNumber,
                            SettlementAmount = x.SettlementAmount,
                            SettlementDate = x.SettlementDate,
                            TUCFees = x.TucFees,
                            CreateDate = x.CreateDate.ToString("dd-MMMM-yyyy hh:mm"),
                            CreatedById = x.CreatedById,
                            CreatedByName = x.CreatedBy.Name,
                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        }).FirstOrDefault();
                    if(Settlement != null)
                    {
                        HCoreHelper.BroadCastEmail("d-da033adc53454feebb5aeeeb5771b20c", Settlement.MerchantAccountName, Settlement.MerchantAccountEmailAddress, Settlement, _Request.UserReference);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "BNPLSUCCESS", "The settlement invoice has been sent successfully to the merchant.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0404", "Settlement details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("", _Exception, _Request.UserReference, "BNPL0500", "Something went wrong here! please, try after some time");
            }
        }
    }

    internal class ActorGetMerchantDownload : ReceiveActor
    {
        public ActorGetMerchantDownload()
        {
            FrameworkMerchantOperation _FrameworkMerchantOperation;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkMerchantOperation = new FrameworkMerchantOperation();
                _FrameworkMerchantOperation.GetMerchantDownload(_Request);
            });
        }
    }

    internal class ActorGetSettlementListDownload : ReceiveActor
    {
        public ActorGetSettlementListDownload()
        {
            FrameworkMerchantOperation _FrameworkMerchantOperation;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkMerchantOperation = new FrameworkMerchantOperation();
                _FrameworkMerchantOperation.GetMerchantSettlementDownload(_Request);
            });
        }
    }

    internal class ActorGetRedeemedLoansDownload : ReceiveActor
    {
        public ActorGetRedeemedLoansDownload()
        {
            FrameworkMerchantOperation _FrameworkMerchantOperation;
            Receive<OList.Request>(_Request =>
            {
                _FrameworkMerchantOperation = new FrameworkMerchantOperation();
                _FrameworkMerchantOperation.GetRedeemedLoansDownload(_Request);
            });
        }
    }
}
