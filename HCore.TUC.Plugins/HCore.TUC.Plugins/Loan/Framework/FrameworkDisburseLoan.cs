//==================================================================================
// FileName: FrameworkDisburseLoan.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to disburse loan
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Integration.Evolve;
using HCore.Integration.Paystack;

namespace HCore.TUC.Plugins.Loan.Framework
{
    /// <summary>
    /// Class FrameworkDisburseLoan.
    /// </summary>
    public class FrameworkDisburseLoan
    {
        HCoreContext _HCoreContext;
        BNPLMerchantSettlement _BNPLMerchantSettlement;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        OUserReference UserReference;
        //EvolveOperations _EvolveOperations;
        HCUAccountTransaction _HCUAccountTransaction;

        /// <summary>
        /// This cron is for to credit the settelment amount to merchant wallet.
        /// </summary>
        //public void CreditLoanAmount()
        //{
        //    try
        //    {
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            DateTime CurrentDateTime = HCoreHelper.GetGMTDateTime().AddHours(1);
        //            var PendingSettelments = _HCoreContext.BNPLMerchantSettlement
        //                                     .Where(x => (x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Initialized
        //                                     || x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Processing)
        //                                     && x.SettlementDate <= CurrentDateTime
        //                                     && x.Loan.IsRedeemed == 1
        //                                     && x.Merchant.IsMerchantSettelment == 1)
        //                                     .Select(x => new
        //                                     {
        //                                         x.Id,
        //                                         x.LoanId,
        //                                         MerchantId = x.MerchantId,
        //                                         SettlementAmount = x.SettlementAmount,
        //                                         Fees = x.TucFees,
        //                                         EmailAddress = x.Merchant.Account.EmailAddress,
        //                                         UserDisplayName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
        //                                         MerchanName = x.Merchant.Account.DisplayName,
        //                                         LoanAmount = x.LoanAmount,
        //                                         MerchantAccountId = x.Merchant.AccountId
        //                                     }).ToList();
        //            if (PendingSettelments.Count > 0)
        //            {
        //                _HCoreContext.Dispose();

        //                var Merchants = PendingSettelments.Select(x => x.MerchantId).Distinct().ToList();
        //                foreach (var MerchantId in Merchants)
        //                {
        //                    var PendingMerchantSettlements = PendingSettelments.Where(x => x.MerchantId == MerchantId).ToList();
        //                    var TotalAmount = PendingSettelments.Sum(x => x.SettlementAmount);
        //                    var Fees = PendingSettelments.Sum(x => x.Fees);

        //                    UserReference = new OUserReference();
        //                    UserReference.AccountId = MerchantId;
        //                    UserReference.AccountTypeId = UserAccountType.Merchant;
        //                    _CoreTransactionRequest = new OCoreTransaction.Request();
        //                    _CoreTransactionRequest.UserReference = UserReference;
        //                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
        //                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                    _CoreTransactionRequest.ParentId = MerchantId;
        //                    _CoreTransactionRequest.InvoiceAmount = (double)TotalAmount;
        //                    _CoreTransactionRequest.ReferenceInvoiceAmount = (double)TotalAmount;
        //                    _CoreTransactionRequest.ReferenceNumber = "SETTELMENT" + HCoreHelper.GenerateRandomNumber(10);
        //                    _CoreTransactionRequest.ReferenceAmount = (double)TotalAmount;
        //                    _CoreTransactionRequest.PaymentMethodId = Helpers.PaymentMethod.Bank;
        //                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                    {
        //                        UserAccountId = MerchantId,
        //                        ModeId = TransactionMode.Credit,
        //                        TypeId = TransactionType.MerchantCredit,
        //                        SourceId = TransactionSource.TUCBnpl,
        //                        Amount = (double)TotalAmount,
        //                        Comission = (double)Fees,
        //                        TotalAmount = (double)TotalAmount - (double)Fees,
        //                    });
        //                    _CoreTransactionRequest.Transactions = _TransactionItems;
        //                    _ManageCoreTransaction = new ManageCoreTransaction();
        //                    OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                    if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                    {
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            foreach (var PendingMerchantSettlementItem in PendingMerchantSettlements)
        //                            {
        //                                var details = _HCoreContext.BNPLMerchantSettlement.Where(x => x.Id == PendingMerchantSettlementItem.Id).FirstOrDefault();
        //                                if (details != null )
        //                                {
        //                                    details.StatusId = BNPLHelper.StatusHelper.MerchantSetelment.Successful;
        //                                    details.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                }
        //                            }
        //                            _HCoreContext.SaveChanges();
        //                        }
        //                        using (_HCoreContext = new HCoreContext())
        //                        {
        //                            var MerchantEmail = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(x => x.EmailAddress).FirstOrDefault();

        //                            var MerchantAccountId = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(x => x.MerchantAccountId).FirstOrDefault();
        //                            var UserDisplayName = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.UserDisplayName).Distinct().ToList();
        //                            var LoanAmount = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.LoanAmount).Distinct().ToList();
        //                            var LoanId = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.LoanId).Distinct().ToList();
        //                            var TotalAmounts = PendingSettelments.Where(x => x.MerchantId == MerchantId).Sum(a => a.LoanAmount);
        //                            var MerchantName = PendingSettelments.Where(x => x.EmailAddress == MerchantEmail).Select(a => a.MerchanName).FirstOrDefault();
        //                            var BankDetails = _HCoreContext.HCUAccountBank.Where(x => x.AccountId == MerchantAccountId).FirstOrDefault();

        //                            if (!string.IsNullOrEmpty(MerchantEmail))
        //                            {
        //                                var _EmailParameters = new
        //                                {
        //                                    UserDisplayName = UserDisplayName,
        //                                    MerchantName = MerchantName,
        //                                    Amount = TotalAmounts.ToString(),
        //                                    LoanAmount = LoanAmount.ToString(),
        //                                    LoanId = LoanId.ToString(),
        //                                    TransactionRef = TransactionResponse.ReferenceId,
        //                                    AccountName = BankDetails.Name,
        //                                    AccountNumber = BankDetails.AccountNumber,
        //                                    TransactionDate = TransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
        //                                    BankName = BankDetails.BankName,
        //                                };
        //                                //HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantSettlement, MerchantName, MerchantEmail, _EmailParameters, UserReference);
        //                                HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantSettlement, MerchantName, "omkar@thankucash.com", _EmailParameters, UserReference);
        //                                HCoreHelper.BroadCastEmail(NotificationTemplates.TUCMerchantSettlement, "ThankUCash", "bnpl@thankucash.com", _EmailParameters, UserReference);
        //                            }
        //                        }
        //                    }
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        HCoreHelper.LogException("CreditLoanAmount", _Exception, null, "HCD0500", "Unable to process. Please try after some time");
        //    }
        //}

        public void CreditLoanAmount()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentDateTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                    var PendingSettelments = _HCoreContext.BNPLMerchantSettlement
                                             .Where(x => (x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Initialized
                                             || x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Processing)
                                             && x.SettlementDate <= CurrentDateTime
                                             && x.Loan.IsRedeemed == 1
                                             && x.Merchant.IsMerchantSettelment == 1)
                                             .Select(x => new
                                             {
                                                 x.Id,
                                                 x.LoanId,
                                                 MerchantId = x.MerchantId,
                                                 SettlementAmount = x.SettlementAmount,
                                                 Fees = x.TucFees,
                                                 EmailAddress = x.Merchant.Account.EmailAddress,
                                                 UserDisplayName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                 MerchanName = x.Merchant.Account.DisplayName,
                                                 LoanAmount = x.LoanAmount,
                                                 MerchantAccountId = x.Merchant.AccountId
                                             }).ToList();
                    if (PendingSettelments.Count > 0)
                    {
                        var Merchants = PendingSettelments.Select(x => x.MerchantId).Distinct().ToList();
                        foreach (var MerchantId in Merchants)
                        {
                            var MerchantAccountId = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(x => x.MerchantAccountId).FirstOrDefault();
                            var PendingMerchantSettlements = PendingSettelments.Where(x => x.MerchantId == MerchantId).ToList();
                            var TotalAmount = PendingSettelments.Sum(x => x.SettlementAmount);
                            var Fees = PendingSettelments.Sum(x => x.Fees);
                            var LoanInformation = _HCoreContext.BNPLAccountLoan.Where(x => x.MerchantId == MerchantId)
                                                                 .Select(x => new
                                                                 {
                                                                     LoanReferenceId = x.Id,
                                                                     LoanReferenceKey = x.Guid,
                                                                     LoanMerchantId = x.MerchantId,
                                                                     LoanAmount = x.Amount,
                                                                     LoanPin = x.LoanPin,
                                                                     LoanStatusId = x.StatusId,
                                                                     IsRedeemed = x.IsRedeemed,
                                                                     AccountReferenceId = x.Account.Account.Id,
                                                                     AccountReferenceKey = x.Account.Account.Guid,
                                                                     AccountName = x.Account.Account.Name,
                                                                     AccountIconUrl = x.Account.Account.IconStorage.Path,
                                                                     AccountMobileNumber = x.Account.Account.MobileNumber,
                                                                     AccountStatusId = x.Account.Account.StatusId,
                                                                     AccountNumber = x.Account.Account.AccountCode,
                                                                     AccountPin = x.Account.Account.AccessPin,
                                                                     AccountEmailAddress = x.Account.Account.EmailAddress,
                                                                     MerchantSettelementCriterea = x.Merchant.SettelmentDays,
                                                                     MerchantTransactionSource = x.Merchant.TransactionSourceId,
                                                                     LoanProviderId = x.ProviderId
                                                                 }).FirstOrDefault();
                            if (LoanInformation.MerchantTransactionSource == TransactionSource.TUCBnpl)
                            {
                                UserReference = new OUserReference();
                                UserReference.AccountId = MerchantAccountId;
                                UserReference.AccountTypeId = UserAccountType.Merchant;
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = LoanInformation.AccountReferenceId;
                                _CoreTransactionRequest.UserReference = UserReference;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = MerchantAccountId;
                                _CoreTransactionRequest.InvoiceAmount = (double)TotalAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = (double)TotalAmount;
                                _CoreTransactionRequest.ReferenceAmount = (double)TotalAmount;
                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = MerchantAccountId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.MerchantCredit,
                                    SourceId = TransactionSource.TUCBnpl,
                                    Amount = (double)TotalAmount,
                                    Comission = (double)Fees,
                                    TotalAmount = (double)TotalAmount - (double)Fees,
                                });

                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response MTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                if (MTransactionResponse.Status == HelperStatus.Transaction.Success)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        foreach (var PendingMerchantSettlementItem in PendingMerchantSettlements)
                                        {
                                            var details = _HCoreContext.BNPLMerchantSettlement.Where(x => x.Id == PendingMerchantSettlementItem.Id).FirstOrDefault();
                                            if (details != null)
                                            {
                                                details.TransactionReference = MTransactionResponse.ReferenceId;
                                                details.StatusId = BNPLHelper.StatusHelper.MerchantSetelment.Successful;
                                                details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                            }
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var MerchantEmail = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(x => x.EmailAddress).FirstOrDefault();

                                        var UserDisplayName = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.UserDisplayName).Distinct().ToList();
                                        var LoanAmount = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.LoanAmount).Distinct().ToList();
                                        var LoanId = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.LoanId).Distinct().ToList();
                                        var TotalAmounts = PendingSettelments.Where(x => x.MerchantId == MerchantId).Sum(a => a.LoanAmount);
                                        var MerchantName = PendingSettelments.Where(x => x.EmailAddress == MerchantEmail).Select(a => a.MerchanName).FirstOrDefault();
                                        var BankDetails = _HCoreContext.HCUAccountBank.Where(x => x.AccountId == MerchantAccountId).FirstOrDefault();

                                        string MerchantNotificationUrl = _HCoreContext.HCUAccountSession
                                            .Where(x => x.AccountId == MerchantAccountId && x.StatusId == 2 && x.NotificationUrl != null)
                                            .OrderByDescending(x => x.LoginDate)
                                            .Select(x => x.NotificationUrl)
                                            .FirstOrDefault();
                                        if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                                        {
                                            string Message = "Payment of N" + HCoreHelper.RoundNumber((double)TotalAmount, _AppConfig.SystemExitRoundDouble).ToString() + " received from " + UserDisplayName;
                                            if (HostEnvironment == HostEnvironmentType.Live)
                                            {
                                                HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "Payment Alert:  N " + (double)TotalAmount + " received.", Message, "dashboard", 0, null, "View details", null);
                                            }
                                            else
                                            {
                                                HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "TEST : Payment Alert:  N " + (double)TotalAmount + " received.", Message, "dashboard", 0, null, "View details", null);
                                            }
                                        }
                                        #region Send Email
                                        if (!string.IsNullOrEmpty(MerchantEmail))
                                        {
                                            var _EmailParameters = new
                                            {
                                                UserDisplayName = UserDisplayName,
                                                MerchantName = MerchantName,
                                                Amount = TotalAmounts.ToString(),
                                                LoanAmount = LoanAmount.ToString(),
                                                LoanId = LoanId.ToString(),
                                                TransactionRef = MTransactionResponse.ReferenceId,
                                                AccountName = BankDetails.Name,
                                                AccountNumber = BankDetails.AccountNumber,
                                                TransactionDate = MTransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                                BankName = BankDetails.BankName,
                                            };

                                            var _Email = new
                                            {
                                                RedeemDate = HCoreHelper.GetGMTDateTime(),
                                                UserDisplayName = UserDisplayName,
                                                MerchantName = MerchantName,
                                                Amount = TotalAmounts.ToString(),
                                                TransactionRef = MTransactionResponse.ReferenceId,
                                                TransactionDate = MTransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                            };
                                            HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantSettlement, MerchantName, MerchantEmail, _EmailParameters, UserReference);
                                            HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.TUCLoanRedeemEmail, "ThankUCash", "bnpl@thankucash.com", _Email, UserReference);
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    string GroupKey = "bnpl" + MerchantAccountId + "settlement" + HCoreHelper.GenerateDateString();
                                    var BankDetails = _HCoreContext.HCUAccountBank.Where(x => x.AccountId == MerchantAccountId).FirstOrDefault();
                                    double WalletBalance = _ManageCoreTransaction.GetAccountBalance(MerchantAccountId, TransactionSource.TUCBnpl);
                                    var _TransferResponse = PaystackTransfer.CreateTransfer(_AppConfig.PaystackPrivateKey, GroupKey, BankDetails.ReferenceCode, Math.Round(LoanInformation.LoanAmount), "TUC BNPL");
                                    if (_TransferResponse != null)
                                    {
                                        UserReference = new OUserReference();
                                        UserReference.AccountId = MerchantAccountId;
                                        UserReference.AccountTypeId = UserAccountType.Merchant;
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = LoanInformation.AccountReferenceId;
                                        _CoreTransactionRequest.UserReference = UserReference;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = MerchantAccountId;
                                        _CoreTransactionRequest.InvoiceAmount = (double)TotalAmount;
                                        _CoreTransactionRequest.ReferenceInvoiceAmount = (double)TotalAmount;
                                        _CoreTransactionRequest.ReferenceAmount = (double)TotalAmount;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = MerchantAccountId,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = TransactionType.TUCBnpl.LoanRedeem,
                                            SourceId = TransactionSource.TUCBnpl,
                                            Amount = (double)TotalAmount,
                                            Comission = (double)Fees,
                                            TotalAmount = (double)TotalAmount - (double)Fees,
                                        });

                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        OCoreTransaction.Response TTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);

                                        double UpdatedBalance = Math.Round(WalletBalance - (LoanInformation.LoanAmount - ((LoanInformation.LoanAmount / 100) * 2)), 2);

                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            ///add the ytransaction record into the database
                                            _HCUAccountTransaction = new HCUAccountTransaction();
                                            _HCUAccountTransaction.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountTransaction.ParentId = MerchantAccountId;
                                            _HCUAccountTransaction.CustomerId = LoanInformation.AccountReferenceId;
                                            _HCUAccountTransaction.AccountId = LoanInformation.AccountReferenceId;
                                            _HCUAccountTransaction.ModeId = TransactionMode.Credit;
                                            _HCUAccountTransaction.TypeId = TransactionType.TUCBnpl.LoanRedeem;
                                            _HCUAccountTransaction.SourceId = TransactionSource.TUCBnpl;
                                            _HCUAccountTransaction.PurchaseAmount = LoanInformation.LoanAmount;
                                            _HCUAccountTransaction.ReferenceAmount = LoanInformation.LoanAmount;
                                            _HCUAccountTransaction.ReferenceInvoiceAmount = LoanInformation.LoanAmount;
                                            _HCUAccountTransaction.Amount = LoanInformation.LoanAmount;
                                            _HCUAccountTransaction.Charge = (LoanInformation.LoanAmount / 100) * 2;
                                            _HCUAccountTransaction.ComissionAmount = (LoanInformation.LoanAmount / 100) * 2;
                                            _HCUAccountTransaction.TotalAmount = (_HCUAccountTransaction.Amount + _HCUAccountTransaction.Charge + _HCUAccountTransaction.ComissionAmount);
                                            _HCUAccountTransaction.TotalAmountPercentage = HCoreHelper.GetAmountPercentage(_HCUAccountTransaction.Charge, LoanInformation.LoanAmount);
                                            _HCUAccountTransaction.TransactionDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountTransaction.ReferenceNumber = "SETTLEMENT" + HCoreHelper.GenerateRandomNumber(10);
                                            _HCUAccountTransaction.TCode = HCoreHelper.GenerateRandomNumber(4);
                                            _HCUAccountTransaction.RequestKey = HCoreHelper.GenerateGuid();
                                            _HCUAccountTransaction.GroupKey = HCoreHelper.GenerateGuid();
                                            _HCUAccountTransaction.PaymentMethodId = PaymentMethod.Bank;
                                            _HCUAccountTransaction.StatusId = HelperStatus.Transaction.Success;
                                            _HCUAccountTransaction.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountTransaction.CreatedById = MerchantAccountId;
                                            _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransaction);

                                            ///update the successful settlement
                                            foreach (var PendingMerchantSettlementItem in PendingMerchantSettlements)
                                            {
                                                var details = _HCoreContext.BNPLMerchantSettlement.Where(x => x.Id == PendingMerchantSettlementItem.Id).FirstOrDefault();
                                                if (details != null)
                                                {
                                                    details.TransactionReferenceNavigation = _HCUAccountTransaction;
                                                    details.StatusId = BNPLHelper.StatusHelper.MerchantSetelment.Successful;
                                                    details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                }
                                            }

                                            _HCoreContext.SaveChanges();
                                        }
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var MerchantEmail = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(x => x.EmailAddress).FirstOrDefault();

                                            var UserDisplayName = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.UserDisplayName).Distinct().ToList();
                                            var LoanAmount = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.LoanAmount).Distinct().ToList();
                                            var LoanId = PendingSettelments.Where(x => x.MerchantId == MerchantId).Select(a => a.LoanId).Distinct().ToList();
                                            var TotalAmounts = PendingSettelments.Where(x => x.MerchantId == MerchantId).Sum(a => a.LoanAmount);
                                            var MerchantName = PendingSettelments.Where(x => x.EmailAddress == MerchantEmail).Select(a => a.MerchanName).FirstOrDefault();

                                            string MerchantNotificationUrl = _HCoreContext.HCUAccountSession
                                                .Where(x => x.AccountId == MerchantAccountId && x.StatusId == 2 && x.NotificationUrl != null)
                                                .OrderByDescending(x => x.LoginDate)
                                                .Select(x => x.NotificationUrl)
                                                .FirstOrDefault();
                                            if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                                            {
                                                string Message = "Payment of N" + HCoreHelper.RoundNumber((double)TotalAmount, _AppConfig.SystemExitRoundDouble).ToString() + " received from " + UserDisplayName;
                                                if (HostEnvironment == HostEnvironmentType.Live)
                                                {
                                                    HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "Payment Alert:  N " + (double)TotalAmount + " received.", Message, "dashboard", 0, null, "View details", null);
                                                }
                                                else
                                                {
                                                    HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "dashboard", "TEST : Payment Alert:  N " + (double)TotalAmount + " received.", Message, "dashboard", 0, null, "View details", null);
                                                }
                                            }
                                            #region Send Email
                                            if (!string.IsNullOrEmpty(MerchantEmail))
                                            {
                                                var _EmailParameters = new
                                                {
                                                    UserDisplayName = UserDisplayName,
                                                    MerchantName = MerchantName,
                                                    Amount = TotalAmounts.ToString(),
                                                    LoanAmount = LoanAmount.ToString(),
                                                    LoanId = LoanId.ToString(),
                                                    TransactionRef = TTransactionResponse.ReferenceId,
                                                    AccountName = BankDetails.Name,
                                                    AccountNumber = BankDetails.AccountNumber,
                                                    TransactionDate = TTransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                                    BankName = BankDetails.BankName,
                                                };

                                                var _Email = new
                                                {
                                                    RedeemDate = HCoreHelper.GetGMTDateTime(),
                                                    UserDisplayName = UserDisplayName,
                                                    MerchantName = MerchantName,
                                                    Amount = TotalAmounts.ToString(),
                                                    TransactionRef = TTransactionResponse.ReferenceId,
                                                    TransactionDate = TTransactionResponse.TransactionDate.ToString("dd-MM-yyyy HH:mm"),
                                                };
                                                HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantSettlement, MerchantName, MerchantEmail, _EmailParameters, UserReference);
                                                HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.TUCLoanRedeemEmail, "ThankUCash", "bnpl@thankucash.com", _Email, UserReference);
                                            }
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreditLoanAmount", _Exception, null, "HCD0500", "Unable to process. Please try after some time");
            }
        }

        /// <summary>
        /// Updates the repayment status.
        /// </summary>
        // public void UpdateRepaymentStatus()
        // {
        //     try
        //     {
        //         using (_HCoreContext = new HCoreContext())
        //         {
        //             _EvolveOperations = new EvolveOperations();
        //             var _Response = _EvolveOperations.GetAllRepayments(119);
        //             var _Reapyments = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).ToList();
        //             foreach (var Repayment in _Reapyments)
        //             {
        //                 foreach (var Item in _Response.Data)
        //                 {
        //                     if (Repayment.VendorLoanRepaymentId == Item.Id)
        //                     {
        //                         var Details = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.VendorLoanRepaymentId == Item.Id)
        //                             .Select(x => new
        //                             {
        //                                 AccountId = x.Loan.Account.AccountId
        //                             }).FirstOrDefault();

        //                         if (DateTime.Parse(Item.Due_On).Date == HCoreHelper.GetGMTDateTime().Date)
        //                         {
        //                             if (Item.Status == "pending")
        //                             {
        //                                 Repayment.StatusId = BNPLHelper.StatusHelper.LoanPayment.Pending;
        //                                 Repayment.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                 Repayment.ModifyById = Details.AccountId;
        //                                 _HCoreContext.SaveChanges();
        //                             }
        //                             if (Item.Status == "paid")
        //                             {
        //                                 Repayment.StatusId = BNPLHelper.StatusHelper.LoanPayment.Successful;
        //                                 Repayment.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                                 Repayment.ModifyById = Details.AccountId;
        //                                 _HCoreContext.SaveChanges();
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //             _HCoreContext.Dispose();
        //         }
        //     }
        //     catch (Exception _Exception)
        //     {
        //         HCoreHelper.LogException("UpdateRepaymentStatus", _Exception, null, "HCD0500", "Unable to process. Please try after some time");
        //     }
        // }

        /// <summary>
        /// Updates the repayment status1.
        /// </summary>
        public void UpdateRepaymentStatus1()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentDateTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                    var _Reapyments = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.PaymentDate < CurrentDateTime && x.StatusId != BNPLHelper.StatusHelper.LoanPayment.Successful).ToList();
                    foreach (var Repayment in _Reapyments)
                    {
                        var Account = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.Id == Repayment.Id).Select(a => new
                        {
                            AccountId = a.Loan.Account.AccountId,
                        }).FirstOrDefault();
                        Repayment.StatusId = BNPLHelper.StatusHelper.LoanPayment.OverDue;
                        Repayment.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Repayment.ModifyById = Account.AccountId;
                        _HCoreContext.SaveChanges();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateRepaymentStatus1", _Exception, null, "HCD0500", "Unable to process. Please try after some time");
            }
        }

        /// <summary>
        /// Checks the overdue repayments.
        /// </summary>
        public void CheckOverdueRepayments()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var LoanAccounts = _HCoreContext.BNPLAccount.ToList();
                    foreach (var Account in LoanAccounts)
                    {
                        var _Reapyments = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.Loan.AccountId == Account.Id && x.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue).ToList();
                        foreach (var Repayment in _Reapyments)
                        {
                            if (!string.IsNullOrEmpty(Account.EmailAddress))
                            {
                                var _EmailParameters = new
                                {
                                    DisplayName = Account.FirstName + " " + Account.LastName,
                                    Amount = Repayment.Amount + Repayment.InterestAmount,
                                    Date = Repayment.PaymentDate,
                                };
                                HCoreHelper.BroadCastEmail(NotificationTemplates.OverDuePayments, Account.EmailAddress, Account.EmailAddress, _EmailParameters, UserReference);
                                //HCoreHelper.BroadCastEmail(NotificationTemplates.OverDuePayments, Account.EmailAddress, "omkar@thankucash.com", _EmailParameters, UserReference);
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CheckOverdueRepayments", _Exception, null, "HCD0500", "Unable to process. Please try after some time");
            }
        }
    }

    public static class SendGridEmailTemplateIds
    {
        public const string RewardEmail = "d-0e22a726fc3f4fd1aa72cbfee2fee6f1";
        public const string RedeemEmail = "d-1635d8f52c664ed285aae77a553d4046";
        public const string MerchantLoanRedeemEmail = "d-0e8bd8df20cd42e09b9230e3159e8daf";
        public const string CustomerLoanRedeemEmail = "d-87e53d4bfd8c448fa328057af779a00d";
        public const string TUCLoanRedeemEmail = "d-b6ffb35c81df41ad804d99ea5e96f4ac";
        public const string PendingRewardEmail = "d-dd04bdb1ea4e44478a5606e0eb85d5e2";

        public const string MerchantSettlement = "d-cfab42f2c0f541aca671b88b241d2a49";
        public const string TUCMerchantSettlement = "d-a509d17bf9d748ef8d148c22b441e885";
    }
}
