//==================================================================================
// FileName: FrameworkLoanOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to loan
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Loan.Object;
using HCore.TUC.Plugins.Loan.Resource;
using System;
using Akka.Actor;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Plugins.Loan.Framework
{
    public class FrameWorkLoanOperation
    {
        HCoreContext _HCoreContext;
        OLoanOperation.Overview _Overview;
        OLoanOperation.DisbursementOverview _DisbursementOverview;
        List<OLoanOperation.Providers> _Providers;
        List<OLoanOperation.ListDownload> _ReapaymentsDownload;
        List<OLoanOperation.LoanActivityTimeLine> _LoanActivityTimeLine;

        /// <summary>
        /// Description: Gets the loan request.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoanRequest(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ApprovedDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                .Where(x=> x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,
                                                    MerchantId = x.MerchantId,
                                                    ProviderId = x.ProviderId,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    LoanAmount = x.Amount,
                                                    InstallmentAmount = x.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    PaidInstallmentStatus = x.BNPLAccountLoanPayment.FirstOrDefault().Status.SystemName,
                                                    PaidInstallmentStatusId = x.BNPLAccountLoanPayment.FirstOrDefault().StatusId,
                                                    StatusId = x.StatusId,
                                                    Status = x.Status.Name,
                                                    CreatedDate = x.CreateDate,
                                                    ApprovedDate = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(m => m.Loan_Approved_Date).FirstOrDefault(),
                                                    MerchantRedeem = x.IsRedeemed,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OLoanOperation.List> _List = _HCoreContext.BNPLAccountLoan
                                                .Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,
                                                    MerchantId = x.MerchantId,
                                                    ProviderId = x.ProviderId,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    LoanAmount = x.Amount,
                                                    InstallmentAmount = x.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    PaidInstallmentStatus = x.BNPLAccountLoanPayment.FirstOrDefault().Status.SystemName,
                                                    PaidInstallmentStatusId = x.BNPLAccountLoanPayment.FirstOrDefault().StatusId,
                                                    StatusId = x.StatusId,
                                                    Status = x.Status.Name,
                                                    CreatedDate = x.CreateDate,
                                                    ApprovedDate = x.BNPLLoanProcess.Where(a => a.LoanId == x.Id).Select(m => m.Loan_Approved_Date).FirstOrDefault(),
                                                    MerchantRedeem = x.IsRedeemed,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetLoans", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets all loan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAllLoans(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                .Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.AllLoanList
                                                {

                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,
                                                    MerchantId = x.MerchantId,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Merchant.Account.Address,
                                                    ProviderReference = x.Account.ProviderReference,
                                                    ProviderId = x.ProviderId,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,
                                                    LoanAmount = x.Amount,
                                                    TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(a => a.InterestAmount),
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Count(),

                                                    TotalPaidAmount = x.BNPLAccountLoanPayment.Where(b => b.LoanId == x.Id && b.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)
                                                    .Sum(b => b.Amount),

                                                    InterestRate = x.Plan.ProviderInterestRate,

                                                    PaidInstallments = x.BNPLAccountLoanPayment.Count(c => c.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),

                                                    Status = x.Status.Name,
                                                    StatusCode = x.Status.SystemName,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    NextRepayment = x.BNPLAccountLoanPayment.Where(d => d.LoanId == x.Id && d.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending)
                                                            .Select(d => d.PaymentDate).FirstOrDefault(),
                                                    Repayment = x.BNPLAccountLoanPayment.Where(d => d.LoanId == x.Id)
                                                                    .Select(d => d.Amount).FirstOrDefault(),
                                                    OverDueInstallment = x.BNPLAccountLoanPayment.Count(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),

                                                    DueDate = x.BNPLAccountLoanPayment.Where(g => g.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue)
                                                                                    .OrderByDescending(g => g.PaymentDate).Select(g => g.PaymentDate).FirstOrDefault(),
                                                    TotalAmount = 0,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OLoanOperation.AllLoanList> _List = _HCoreContext.BNPLAccountLoan
                                                .Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.AllLoanList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + " " + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,
                                                    MerchantId = x.MerchantId,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Merchant.Account.Address,
                                                    ProviderReference = x.Account.ProviderReference,
                                                    ProviderId = x.ProviderId,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,
                                                    LoanAmount = x.Amount,
                                                    TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(a => a.InterestAmount),
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Count(),

                                                    TotalPaidAmount = x.BNPLAccountLoanPayment.Where(b => b.LoanId == x.Id && b.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)
                                                    .Sum(b => b.Amount),

                                                    InterestRate = x.Plan.ProviderInterestRate,

                                                    PaidInstallments = x.BNPLAccountLoanPayment.Count(c => c.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),

                                                    Status = x.Status.Name,
                                                    StatusCode = x.Status.SystemName,

                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    NextRepayment = x.BNPLAccountLoanPayment.Where(d => d.LoanId == x.Id && d.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending)
                                                            .Select(d => d.PaymentDate).FirstOrDefault(),
                                                    Repayment = x.BNPLAccountLoanPayment.Where(d => d.LoanId == x.Id)
                                                                    .Select(d => d.Amount).FirstOrDefault(),
                                                    OverDueInstallment = x.BNPLAccountLoanPayment.Count(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),

                                                    DueDate = x.BNPLAccountLoanPayment.Where(g => g.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue)
                                                                                    .OrderByDescending(g => g.PaymentDate).Select(g => g.PaymentDate).FirstOrDefault(),
                                                    TotalAmount = 0,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _List)
                    {
                        if (DataItem.TotalAmount == 0)
                        {
                            DataItem.TotalAmount = DataItem.LoanAmount + DataItem.TotalInterest;
                        }
                        if (DataItem.StartDate == DateTime.Parse("01/01/0001 00:00:00"))
                        {
                            DataItem.StartDate = null;
                        }
                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAllLoans", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the repayment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRepayments(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.IsDownload)
                {
                    var _Actor = ActorSystem.Create("ActorGetRepaymentsDownload");
                    var _ActorNotify = _Actor.ActorOf<ActorGetRepaymentsDownload>("ActorGetRepaymentsDownload");
                    _ActorNotify.Tell(_Request);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "Your file will be available for download in downloads section");
                }
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "LoanId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoanPayment
                                                .Where(x => x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.Repayments
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,

                                                    CustomerDisplayName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                    CustomerNumber = x.Loan.Account.MobileNumber,
                                                    MerchantId = x.Loan.MerchantId,
                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Loan.Merchant.Account.Address,
                                                    ProviderReference = x.Loan.Account.ProviderReference,
                                                    LoanAmount = x.Amount,
                                                    ProviderId = x.Loan.ProviderId,
                                                    ProviderName = x.Loan.Provider.Name,
                                                    ProviderIconUrl = x.Loan.Provider.IconStorage.Path,

                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(y => y.LoanId == x.LoanId),
                                                    TotalPaidAmount = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),

                                                    PaidInstallment = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),

                                                    LastPayment = x.PaymentDate,
                                                    NextRepayment = x.Loan.BNPLAccountLoanPayment.Where(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(f => f.PaymentDate).FirstOrDefault(),
                                                    InterestAmount = x.InterestAmount,

                                                    InterestRate = x.Loan.Plan.CustomerInterestRate,

                                                    Repayment = x.Amount,
                                                    OverDueInstallment = x.Loan.BNPLAccountLoanPayment.Count(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),

                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OLoanOperation.Repayments> _List = _HCoreContext.BNPLAccountLoanPayment
                                                .Where(x => x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.Repayments
                                                {

                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,

                                                    CustomerDisplayName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                    CustomerNumber = x.Loan.Account.MobileNumber,
                                                    MerchantId = x.Loan.MerchantId,
                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Loan.Merchant.Account.Address,
                                                    ProviderReference = x.Loan.Account.ProviderReference,
                                                    LoanAmount = x.Amount,
                                                    ProviderId = x.Loan.ProviderId,
                                                    ProviderName = x.Loan.Provider.Name,
                                                    ProviderIconUrl = x.Loan.Provider.IconStorage.Path,

                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(y => y.LoanId == x.LoanId),
                                                    TotalPaidAmount = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),

                                                    PaidInstallment = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),

                                                    LastPayment = x.PaymentDate,
                                                    NextRepayment = x.Loan.BNPLAccountLoanPayment.Where(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(f => f.PaymentDate).FirstOrDefault(),
                                                    InterestAmount = x.Charge,

                                                    InterestRate = x.Loan.Plan.CustomerInterestRate,

                                                    Repayment = x.Amount,
                                                    OverDueInstallment = x.Loan.BNPLAccountLoanPayment.Count(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),

                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetRepayments", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the repayments download.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal void GetRepaymentsDownload(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Request.Limit = 800;
                _ReapaymentsDownload = new List<OLoanOperation.ListDownload>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    _Request.TotalRecords = _HCoreContext.BNPLAccountLoanPayment
                                                .Where(x => x.Loan.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.ListDownload
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,

                                                    CustomerDisplayName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                    CustomerNumber = x.Loan.Account.MobileNumber,
                                                    MerchantId = x.Loan.MerchantId,
                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    LoanAmount = x.Amount,

                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(y => y.LoanId == x.LoanId),
                                                    TotalPaidAmount = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),

                                                    LastPayment = x.PaymentDate,
                                                    NextRepayment = x.Loan.BNPLAccountLoanPayment.Where(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending)
                                                            .Select(f => f.PaymentDate).FirstOrDefault(),
                                                    InterestAmount = x.InterestAmount,

                                                    Repayment = x.Amount,
                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),
                                                })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                    #endregion
                    double Iterations = Math.Round((double)_Request.TotalRecords / 800, MidpointRounding.AwayFromZero) + 1;
                    _Request.Offset = 0;
                    for (int i = 0; i < Iterations; i++)
                    {
                        #region Get Data
                        var _Data = _HCoreContext.BNPLAccountLoanPayment
                                                .Where(x => x.Loan.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.ListDownload
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,

                                                    CustomerDisplayName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                    CustomerNumber = x.Loan.Account.MobileNumber,
                                                    MerchantId = x.Loan.MerchantId,
                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    LoanAmount = x.Amount,

                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(y => y.LoanId == x.LoanId),
                                                    TotalPaidAmount = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),

                                                    LastPayment = x.PaymentDate,
                                                    NextRepayment = x.Loan.BNPLAccountLoanPayment.Where(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending)
                                                            .Select(f => f.PaymentDate).FirstOrDefault(),
                                                    InterestAmount = x.InterestAmount,

                                                    Repayment = x.Amount,
                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),
                                                })
                                                     .Where(_Request.SearchCondition)
                                                     .OrderBy(_Request.SortExpression)
                                                     .Skip(_Request.Offset)
                                                     .Take(_Request.Limit)
                                                     .ToList();
                        #endregion
                        foreach (var _DataItem in _Data)
                        {
                            _ReapaymentsDownload.Add(new OLoanOperation.ListDownload
                            {
                                ReferenceId = _DataItem.ReferenceId,
                                ReferenceKey = _DataItem.ReferenceKey,
                                LoanId = _DataItem.LoanId,
                                CustomerDisplayName = _DataItem.CustomerDisplayName,
                                CustomerNumber = _DataItem.CustomerNumber,
                                MerchantId = _DataItem.MerchantId,
                                MerchantDisplayName = _DataItem.MerchantDisplayName,
                                LoanAmount = _DataItem.LoanAmount,
                                TotalInstallment = _DataItem.TotalInstallment,
                                TotalPaidAmount = _DataItem.TotalPaidAmount,
                                LastPayment = _DataItem.LastPayment,
                                NextRepayment = _DataItem.NextRepayment,
                                InterestAmount = _DataItem.InterestAmount,
                                Repayment = _DataItem.Repayment,
                                DueDate = _DataItem.DueDate,
                            });
                        }
                        _Request.Offset += 800;
                    }

                    _HCoreContext.Dispose();
                    HCoreHelper.CreateDownload("Repayments_List", _ReapaymentsDownload, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchantDownload", _Exception);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the loan request overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoanRequestOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OSummaryOverview.Overview overview = new OSummaryOverview.Overview();
                    overview.Total = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId).Count();
                    overview.TotalAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId).Sum(x => x.Amount);

                    #endregion


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, overview, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetLoanRequestOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the repayments overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRepaymentsOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OSummaryOverview.RepaymentOverview overview = new OSummaryOverview.RepaymentOverview();
                    overview.OverDue = new OSummaryOverview.Overview
                    {
                        Total = _HCoreContext.BNPLAccountLoanPayment.Count(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId),
                        TotalAmount = _HCoreContext.BNPLAccountLoanPayment
                            .Where(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                            .Sum(x => x.Amount)
                    };

                    overview.Paid = new OSummaryOverview.Overview
                    {
                        Total = _HCoreContext.BNPLAccountLoanPayment.Count(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId),
                        TotalAmount = _HCoreContext.BNPLAccountLoanPayment
                           .Where(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                           .Sum(x => x.Amount)
                    };

                    overview.Pending = new OSummaryOverview.Overview
                    {
                        Total = _HCoreContext.BNPLAccountLoanPayment.Count(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId),
                        TotalAmount = _HCoreContext.BNPLAccountLoanPayment
                           .Where(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                           .Sum(x => x.Amount)
                    };

                    #endregion


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, overview, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRepaymentsOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets all loans overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAllLoansOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OSummaryOverview.AllLoansOverview data = new OSummaryOverview.AllLoansOverview();
                    OSummaryOverview.AllLoansSubOverview activeloans = new OSummaryOverview.AllLoansSubOverview();
                    activeloans.Loans = _HCoreContext.BNPLAccountLoan.Count(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Account.Account.CountryId == _Request.UserReference.CountryId);
                    activeloans.PrincipalAmount = _HCoreContext.BNPLAccountLoan
                                                        .Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running &&  x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                        .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.PrincipalAmount)).Sum();
                    activeloans.InterestAmount = _HCoreContext.BNPLAccountLoan
                                    .Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running &&  x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                    .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.InterestAmount)).Sum();
                    activeloans.TUCAmount = _HCoreContext.BNPLAccountLoan
                                  .Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                  .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.SystemAmount)).Sum();
                    data.ActiveLoans = activeloans;


                    OSummaryOverview.DefaultLoansOverView defaultLoansOverView = new OSummaryOverview.DefaultLoansOverView();

                    defaultLoansOverView.TotalLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Account.Account.CountryId == _Request.UserReference.CountryId
                                                    && x.BNPLAccountLoanPayment.Any(y => y.LoanId == x.Id && y.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue)).Count();
                    defaultLoansOverView.Repay = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.Account.Account.CountryId == _Request.UserReference.CountryId
                                                     && x.BNPLAccountLoanPayment.Any(y => y.LoanId == x.Id && y.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue))
                                                      .Where(y => y.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Count();

                    data.DefaultLoansOverView = defaultLoansOverView;


                    OSummaryOverview.AllLoansSubOverview closedLoans = new OSummaryOverview.AllLoansSubOverview();
                    closedLoans.Loans = _HCoreContext.BNPLAccountLoan.Count(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed && x.Account.Account.CountryId == _Request.UserReference.CountryId);
                    closedLoans.PrincipalAmount = _HCoreContext.BNPLAccountLoan
                                                        .Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed && x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                        .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.PrincipalAmount)).Sum();
                    closedLoans.InterestAmount = _HCoreContext.BNPLAccountLoan
                                    .Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed && x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                    .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.InterestAmount)).Sum();
                    closedLoans.TUCAmount = _HCoreContext.BNPLAccountLoan
                                  .Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Closed && x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                  .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.SystemAmount)).Sum();
                    data.ClosedLoansOverView = closedLoans;

                    OSummaryOverview.AllLoansSubOverview allLoans = new OSummaryOverview.AllLoansSubOverview();
                    allLoans.Loans = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId).Distinct().Count();
                    allLoans.PrincipalAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                        .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.PrincipalAmount)).Sum();
                    allLoans.InterestAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                    .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.InterestAmount)).Sum();
                    allLoans.TUCAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId)
                                  .Select(x => x.BNPLAccountLoanPayment.Sum(x => x.SystemAmount)).Sum();
                    data.AllLoansOverView = allLoans;


                    #endregion


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, data, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetAllLoansOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the loan details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoanDetails(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OLoanOperation.LoanDetails data = _HCoreContext.BNPLAccountLoan
                                                 .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                                 .OrderByDescending(x => x.CreateDate)
                                                 .Select(x => new OLoanOperation.LoanDetails
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     RequestedOn = x.CreateDate,
                                                     ApprovedOn = x.BNPLLoanProcess.Where(m => m.LoanId == x.Id).Select(a => a.Loan_Approved_Date).FirstOrDefault(),
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                     CustomerDetail = new OLoanOperation.LoanCustomerDetail
                                                     {
                                                         CustomerAccountId = x.Account.AccountId,
                                                         CustomerAccountKey = x.Account.Account.Guid,
                                                         CustomerName = x.Account.FirstName + x.Account.LastName,
                                                         MobileNumber = x.Account.MobileNumber,
                                                         CreditLimit = x.Account.CreditLimit,
                                                         IconURL = x.Account.Account.IconStorage.Path,
                                                         ApprovedOn = x.Account.CreateDate,
                                                         ContactNumber = x.Account.Account.ContactNumber,
                                                         ContactEmail = x.Account.Account.EmailAddress,
                                                         CustomerOtherLoans = x.Account.BNPLAccountLoan
                                                                             .Select(y => new OLoanOperation.CustomerOtherLoans
                                                                             {
                                                                                 ReferenceId = y.Id,
                                                                                 ReferenceKey = y.Guid
                                                                             }).ToList()
                                                     },
                                                     ProviderDetail = new OLoanOperation.LoanProviderDetail
                                                     {
                                                         ProviderReference = x.Account.ProviderReference,
                                                         ProviderId = x.ProviderId,
                                                         ProviderName = x.Provider.DisplayName,
                                                         ProviderIconUrl = x.Provider.IconStorage.Path,
                                                         PrincipalAmount = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Sum(x => x.PrincipalAmount),
                                                         InterestRate = x.Plan.ProviderInterestRate,
                                                         InterestAmount = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Sum(x => x.InterestAmount),
                                                         MerchantName = x.Merchant.Account.DisplayName,
                                                         MerchantAddress = x.Merchant.Account.Address,
                                                         MerchantIconURL = x.Merchant.Account.IconStorage.Path
                                                     },
                                                     PlanDetail = new OLoanOperation.LoanPlanDetail
                                                     {
                                                         TotalInstallments = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Count(),
                                                         PaidInstallments = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Count(y => y.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                         FinalAmount = x.Amount,
                                                         PaidAmount = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Where(y => y.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(y => y.Amount),
                                                         IntialAmount = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Select(x => x.Amount).FirstOrDefault(),
                                                         RepaymentAmount = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Select(x => x.Amount).Skip(1).FirstOrDefault(),
                                                         Duration = x.Plan.Tenture
                                                     },
                                                     RepaymentTimeLine = x.BNPLAccountLoanPayment
                                                     .Select(y => new OLoanOperation.LoanRepaymentTimeLine
                                                     {
                                                         ProviderId = x.ProviderId,
                                                         ProviderName = x.Provider.DisplayName,
                                                         ProviderIconUrl = x.Provider.IconStorage.Path,
                                                         DueDate = y.PaymentDate,
                                                         PaymentDate = y.PaymentDate,
                                                         PrincipalAmount = y.PrincipalAmount,
                                                         Interest = y.InterestAmount,
                                                         Total = y.PrincipalAmount + y.InterestAmount,
                                                         Status = y.Status.Name,
                                                         ToTUC = y.SystemAmount,
                                                         ProcessingFees = Math.Round((y.SystemAmount / y.TotalAmount) * 100, 2)
                                                     }).ToList(),
                                                     LoanDisbursmentTimeLine = new OLoanOperation.LoanDisbursmentTimeLine
                                                     {
                                                         ProviderId = x.ProviderId,
                                                         ProviderName = x.Provider.DisplayName,
                                                         ProviderIconUrl = x.Provider.IconStorage.Path,
                                                         Date = x.StartDate,
                                                         Status = x.BNPLAccountLoanPayment.Where(y => y.LoanId == x.Id).FirstOrDefault().Status.Name,
                                                         ProviderToTUC = x.Amount,
                                                         TUCFees = x.BNPLAccountLoanPayment.Where(m => m.LoanId == x.Id).Sum(w => w.SystemAmount),
                                                         TUCToMerchant = x.Amount - (x.BNPLAccountLoanPayment.Sum(w => w.SystemAmount)),
                                                         TUCToMerchantDate = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)
                                                                                .FirstOrDefault().PaymentDate
                                                     }
                                                 }).FirstOrDefault();

                    #endregion
                    if (data != null)
                    {
                        if (!string.IsNullOrEmpty(data.CustomerDetail.IconURL))
                        {
                            data.CustomerDetail.IconURL = _AppConfig.StorageUrl + data.CustomerDetail.IconURL;
                        }
                        else
                        {
                            data.CustomerDetail.IconURL = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(data.ProviderDetail.ProviderIconUrl))
                        {
                            data.ProviderDetail.ProviderIconUrl = _AppConfig.StorageUrl + data.ProviderDetail.ProviderIconUrl;
                        }
                        else
                        {
                            data.ProviderDetail.ProviderIconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(data.LoanDisbursmentTimeLine.ProviderIconUrl))
                        {
                            data.LoanDisbursmentTimeLine.ProviderIconUrl = _AppConfig.StorageUrl + data.LoanDisbursmentTimeLine.ProviderIconUrl;
                        }
                        else
                        {
                            data.LoanDisbursmentTimeLine.ProviderIconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(data.ProviderDetail.MerchantIconURL))
                        {
                            data.ProviderDetail.MerchantIconURL = _AppConfig.StorageUrl + data.ProviderDetail.MerchantIconURL;
                        }
                        else
                        {
                            data.ProviderDetail.MerchantIconURL = _AppConfig.Default_Icon;
                        }
                    }

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, data, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetLoanDetails", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the loan activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoanActivity(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                _LoanActivityTimeLine = new List<OLoanOperation.LoanActivityTimeLine>();

                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "LoanId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoanActivity
                                                .Where(x => x.LoanId == _Request.ReferenceId
                                                && x.Loan.Guid == _Request.ReferenceKey)
                                                .Select(x => new OLoanOperation.LoanActivityTimeLine
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Description = x.Description,
                                                    IsCompleted = x.IsSystemActivity,
                                                    FirstPartCreditAmount = x.Loan.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    FirstPartCreditDate = x.Loan.CreateDate,
                                                    LoanAmountCredited = x.Loan.Amount,
                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,
                                                    LoanAmountCreditDate = x.Loan.CreateDate,
                                                    LoanRedeemDate = x.Loan.RedeemDate,
                                                    SettelmentAmount = x.Loan.BNPLMerchantSettlement.Where(a => a.LoanId == x.LoanId).Select(a => a.SettlementAmount).FirstOrDefault(),
                                                    SettelmentDate = x.Loan.BNPLMerchantSettlement.Where(a => a.LoanId == x.LoanId).Select(a => a.SettlementDate).FirstOrDefault(),
                                                    StatusUpdatedTime = x.Loan.ModifyDate,
                                                    CreateDate = x.CreateDate,
                                                })
                                                .Where(_Request.SearchCondition)
                                                .Count();
                    }
                    List<OLoanOperation.LoanActivityTimeLine> Data = _HCoreContext.BNPLAccountLoanActivity
                                                .Where(x => x.LoanId == _Request.ReferenceId
                                                && x.Loan.Guid == _Request.ReferenceKey)
                                                .Select(x => new OLoanOperation.LoanActivityTimeLine
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Description = x.Description,
                                                    IsCompleted = x.IsSystemActivity,
                                                    FirstPartCreditAmount = x.Loan.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    FirstPartCreditDate = x.Loan.CreateDate,
                                                    LoanAmountCredited = x.Loan.Amount,
                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,
                                                    LoanAmountCreditDate = x.Loan.CreateDate,
                                                    LoanRedeemDate = x.Loan.RedeemDate,
                                                    SettelmentAmount = x.Loan.BNPLMerchantSettlement.Where(a => a.LoanId == x.LoanId).Select(a => a.SettlementAmount).FirstOrDefault(),
                                                    SettelmentDate = x.Loan.BNPLMerchantSettlement.Where(a => a.LoanId == x.LoanId).Select(a => a.SettlementDate).FirstOrDefault(),
                                                    StatusUpdatedTime = x.Loan.ModifyDate,
                                                    CreateDate = x.CreateDate,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(0)
                                             .Take(10)
                                             .ToList();
                    #region Get Data
                    //List<OLoanOperation.LoanActivityTimeLine> data = _HCoreContext.BNPLAccountLoanActivity
                    //                             .Where(x => x.LoanId == _Request.ReferenceId && x.Loan.Guid == _Request.ReferenceKey)
                    //                             .OrderByDescending(x => x.CreateDate)
                    //                             .Select(x => new OLoanOperation.LoanActivityTimeLine
                    //                             {
                    //                                 ReferenceId = x.Id,
                    //                                 ReferenceKey = x.Guid,
                    //                                 Description = x.Description,
                    //                                 IsCompleted = x.IsSystemActivity,
                    //                                 FirstPartCreditAmount = x.Loan.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                    //                                 FirstPartCreditDate = x.Loan.CreateDate,
                    //                                 LoanAmountCredited = x.Loan.Amount,
                    //                                 StatusUpdatedTime = x.Loan.ModifyDate,
                    //                             }).Take(4).ToList();
                    //int counter = 0;
                    //List<OLoanOperation.LoanActivityTimeLine> datarepayments = _HCoreContext.BNPLAccountLoanActivity
                    //                            .Where(x => x.LoanId == _Request.ReferenceId && x.Loan.Guid == _Request.ReferenceKey)
                    //                            .OrderByDescending(x => x.CreateDate)
                    //                            .Select(x => new OLoanOperation.LoanActivityTimeLine
                    //                            {
                    //                                ReferenceId = x.Id,
                    //                                ReferenceKey = x.Guid,
                    //                                Description = x.Description,
                    //                                IsCompleted = x.IsSystemActivity,
                    //                                Amount = x.Loan.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                    //                                StatusUpdatedTime = x.Loan.BNPLAccountLoanPayment.Skip(counter + 1).FirstOrDefault().PaymentDate,
                    //                            }).Skip(4).ToList();

                    //var Union = Enumerable.Union(data, datarepayments);

                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _LoanActivityTimeLine)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetLoanActivity", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the reconcilation report.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetReconcilationReport(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                // .Where(x=>x.StatusId== BNPLHelper.StatusHelper.Loan.Closed)  
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OLoanOperation.AllLoanList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,
                                                    MerchantId = x.MerchantId,
                                                    ProviderId = x.ProviderId,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Merchant.Account.Address,
                                                    ProviderReference = x.Account.ProviderReference,
                                                    PrincipalAmount = x.BNPLAccountLoanPayment.Where(z => z.LoanId == x.Id).FirstOrDefault().PrincipalAmount,
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Count(),
                                                    LoanAmount = x.Amount,
                                                    TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(a => a.InterestAmount),
                                                    InterestRate = x.Plan.CustomerInterestRate,
                                                    TUCFees = x.BNPLAccountLoanPayment.Sum(w => w.SystemAmount),
                                                    MerchantSettlement = x.BNPLAccountLoanPayment.Sum(w => w.ProviderAmount),
                                                    MerchantSettleStatus = x.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault(),
                                                    TUCStatus = "Paid",
                                                    Status = x.Status.Name
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OLoanOperation.AllLoanList> _List = _HCoreContext.BNPLAccountLoan
                                                .Select(x => new OLoanOperation.AllLoanList
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerDisplayName = x.Account.FirstName + x.Account.LastName,
                                                    CustomerNumber = x.Account.MobileNumber,
                                                    MerchantId = x.MerchantId,
                                                    ProviderId = x.ProviderId,
                                                    ProviderName = x.Provider.Name,
                                                    ProviderIconUrl = x.Provider.IconStorage.Path,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Merchant.Account.Address,
                                                    ProviderReference = x.Account.ProviderReference,
                                                    PrincipalAmount = x.BNPLAccountLoanPayment.Where(z => z.LoanId == x.Id).FirstOrDefault().PrincipalAmount,
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Count(),
                                                    LoanAmount = x.Amount,
                                                    TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(a => a.InterestAmount),
                                                    InterestRate = x.Plan.CustomerInterestRate,
                                                    TUCFees = x.BNPLAccountLoanPayment.Sum(w => w.SystemAmount),
                                                    MerchantSettlement = x.BNPLAccountLoanPayment.Sum(w => w.ProviderAmount),
                                                    MerchantSettleStatus = x.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault(),
                                                    TUCStatus = "Paid",
                                                    Status = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ProviderIconUrl))
                        {
                            DataItem.ProviderIconUrl = _AppConfig.StorageUrl + DataItem.ProviderIconUrl;
                        }
                        else
                        {
                            DataItem.ProviderIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetReconcilationReport", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the reconcilation report overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetReconcilationReportOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OSummaryOverview.ReconcilationReportOverview data = new OSummaryOverview.ReconcilationReportOverview();
                    data = _HCoreContext.BNPLAccountLoan
                                        .Select(x => new OSummaryOverview.ReconcilationReportOverview
                                        {
                                            TotalPrincipalAmount = x.BNPLAccountLoanPayment.Sum(x => x.PrincipalAmount),
                                            TotalInterest = x.BNPLAccountLoanPayment.Sum(x => x.InterestAmount),
                                            TUCFees = x.BNPLAccountLoanPayment.Sum(y => y.SystemAmount)

                                        }).FirstOrDefault();

                    data.TotalAmount = _HCoreContext.BNPLAccountLoan
                                        .Sum(x => x.Amount);
                    #endregion


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, data, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetReconcilationReportOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the BNPL overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBNPLOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    _Overview = new OLoanOperation.Overview();
                    _Overview.TotalLoanRequests = _HCoreContext.BNPLAccountLoan.Where(x => x.CreateDate.Date.Date >= _Request.StartDate.Date.Date && x.CreateDate.Date.Date <= _Request.EndDate.Date.Date).Count();
                    _Overview.TotalActiveLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && (x.CreateDate.Date.Date >= _Request.StartDate.Date.Date && x.CreateDate.Date.Date <= _Request.EndDate.Date.Date)).Count();
                    _Overview.PendingMerchantSettlements = _HCoreContext.BNPLMerchantSettlement.Where(x => x.StatusId != BNPLHelper.StatusHelper.MerchantSetelment.Successful && (x.CreateDate.Date.Date >= _Request.StartDate.Date.Date && x.CreateDate.Date.Date <= _Request.EndDate.Date.Date)).Sum(m => m.SettlementAmount);
                    _Overview.TUCFees = _HCoreContext.BNPLMerchantSettlement.Where(x => x.CreateDate.Date.Date >= _Request.StartDate.Date.Date && x.CreateDate.Date.Date <= _Request.EndDate.Date.Date).Sum(x => x.TucFees);
                    _Overview.Interest = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.CreateDate.Date.Date >= _Request.StartDate.Date.Date && x.CreateDate.Date.Date <= _Request.EndDate.Date.Date).Sum(x => x.InterestAmount);
                    _Overview.Revenue = _Overview.TUCFees + _Overview.Interest;
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("BNPLOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the disbursment distribution overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDisbursmentDistributionOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (string.IsNullOrEmpty(_Request.StartDate.ToString()))
                {
                    _Request.Date = HCoreHelper.GetGMTDateTime();
                }

                if (string.IsNullOrEmpty(_Request.EndDate.ToString()))
                {
                    _Request.Date = HCoreHelper.GetGMTDateTime().AddDays(-1);
                }

                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data

                    OSummaryOverview.DisbursmentDistributionOverview data = new OSummaryOverview.DisbursmentDistributionOverview();
                    data.TUCFees = _HCoreContext.BNPLMerchantSettlement
                                                .Where(x => (x.CreateDate.Date.Month >= _Request.StartDate.Date.Month && x.CreateDate.Date.Month <= _Request.EndDate.Date.Month)
                                                //.Where(x => x.StartDate == _Request.Date
                                                        && x.Loan.IsRedeemed == 1
                                                        && x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Successful
                                                    ).Sum(a => a.TucFees);
                    data.MerchantSettlement = _HCoreContext.BNPLMerchantSettlement
                                                .Where(x => (x.CreateDate.Date.Month >= _Request.StartDate.Date.Month && x.CreateDate.Date.Month <= _Request.EndDate.Date.Month)
                                                //.Where(x => x.SettlementDate == _Request.Date
                                                        && x.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Successful
                                                    )
                                                    .Sum(a => a.SettlementAmount);
                    data.TotalDisbursed = _HCoreContext.BNPLAccountLoan
                                                .Where(x => (x.CreateDate.Date.Month >= _Request.StartDate.Date.Month && x.CreateDate.Date.Month <= _Request.EndDate.Date.Month)
                                                //.Where(x => x.CreateDate == _Request.Date
                                                             && x.IsRedeemed == 1
                                                             && x.StatusId == BNPLHelper.StatusHelper.Loan.Running
                                                         ).Sum(x => x.Amount);



                    #endregion


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, data, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetDisbursmentDistributionOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the repayment status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRepaymentStatusOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (string.IsNullOrEmpty(_Request.Date.ToString()))
                {
                    _Request.Date = DateTime.Now.Date;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OSummaryOverview.RepaymentStatusOverview data = new OSummaryOverview.RepaymentStatusOverview();
                    data.OverDueCount = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month).Count(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue);
                    data.OverDueAmount = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(x => x.Amount);
                    data.PaidCount = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month).Count(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful);
                    data.PaidAmount = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(x => x.Amount);
                    data.UpComingCount = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month).Count(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending);
                    data.UpComingAmount = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(x => x.Amount);
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, data, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetRepaymentStatusOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the loan disbursement.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoanDisbursement(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    _DisbursementOverview = new OLoanOperation.DisbursementOverview();
                    _DisbursementOverview.TotalDisbursedLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 1 && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Count();
                    _DisbursementOverview.TotalDisbursedAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 1 && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(a => a.Amount);
                    _DisbursementOverview.TotalInterestAmount = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.Loan.IsRedeemed == 1 && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(a => a.InterestAmount);
                    _DisbursementOverview.TotalDisbursedLoansByEC = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 1 && x.ProviderId == BNPLHelper.StatusHelper.LoanProvider.EvolveCredit && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Count();
                    _DisbursementOverview.TotalDisbursedLoansByTUC = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 1 && x.ProviderId == BNPLHelper.StatusHelper.LoanProvider.ThankUCash && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Count();
                    _DisbursementOverview.TotalDisbursedAmountByEC = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 1 && x.ProviderId == BNPLHelper.StatusHelper.LoanProvider.EvolveCredit && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(a => a.Amount);
                    _DisbursementOverview.TotalDisbursedAmountByTUC = _HCoreContext.BNPLAccountLoan.Where(x => x.StatusId == BNPLHelper.StatusHelper.Loan.Running && x.IsRedeemed == 1 && x.ProviderId == BNPLHelper.StatusHelper.LoanProvider.ThankUCash && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(a => a.Amount);
                    _DisbursementOverview.TotalInterestByEC = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.Loan.IsRedeemed == 1 && x.Loan.ProviderId == BNPLHelper.StatusHelper.LoanProvider.EvolveCredit && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(a => a.InterestAmount);
                    _DisbursementOverview.TotalInterestByTUC = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.Loan.IsRedeemed == 1 && x.Loan.ProviderId == BNPLHelper.StatusHelper.LoanProvider.ThankUCash && (x.CreateDate.Month >= _Request.StartDate.Month && x.CreateDate.Month <= _Request.EndDate.Month)).Sum(a => a.InterestAmount);
                    #endregion

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DisbursementOverview, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetLoanDisbursement", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the disbursement by month.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDisbursementByMonth(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                OLoanOperation.DisbursementbyMonthResponse response = new OLoanOperation.DisbursementbyMonthResponse();
                long DaysDifference = (_Request.EndDate.Date.Date - _Request.StartDate.Date.Date).Days;
                List<string> Months = new List<string>();
                Months.Add((_Request.EndDate.Date.Date.ToString("MMM")));
                Months.Add((_Request.StartDate.Date.Date.ToString("MMM")));

                using (_HCoreContext = new HCoreContext())
                {
                    var _OTrList1 = _HCoreContext.BNPLAccountLoan
                        .Where(x => x.CreateDate.Date.Month == _Request.StartDate.Date.Month)
                        .GroupBy(x => x.CreateDate.Date)
                        .OrderBy(x => x.Key)
                         .Select(x => new OLoanOperation.DisbursementbyMonth
                         {
                             Date = x.Key,
                             Month = x.Key.ToString("MMM"),

                             LoanCount = x.Count(),
                             LoanAmount = x.Sum(x => x.Amount),
                             MonthNo = x.Key.Month
                         }).ToList();
                    var _OTrList2 = _HCoreContext.BNPLAccountLoan
                        .Where(x => x.CreateDate.Date.Month == _Request.EndDate.Date.Month)
                        .GroupBy(x => x.CreateDate.Date)
                        .OrderBy(x => x.Key)
                         .Select(x => new OLoanOperation.DisbursementbyMonth
                         {
                             Date = x.Key,
                             Month = x.Key.ToString("MMM"),
                             LoanCount = x.Count(),
                             LoanAmount = x.Sum(x => x.Amount),
                             MonthNo = x.Key.Month
                         }).ToList();
                    if (_OTrList1.Count() > 0 || _OTrList2.Count() > 0)
                    {
                        var _OTrList = Enumerable.Union(_OTrList1, _OTrList2);
                        if (_OTrList.Count() > 0)
                        {
                            response.DataSummary = _OTrList.GroupBy(x => x.Month)
                             .Select(x => new OLoanOperation.Data
                             {
                                 Title = x.Key,
                                 TotalLoanAmount = x.Sum(a => a.LoanAmount),
                                 TotalLoanCount = x.Sum(a => a.LoanCount),
                                 MonthNo = x.Select(a => a.MonthNo).FirstOrDefault()
                             }).OrderBy(x => x.MonthNo).ToList();

                            if (response.DataSummary.Count() != Months.Count())
                            {
                                var difference = Months.Except(response.DataSummary.Select(x => x.Title));
                                foreach (var item in difference)
                                {
                                    response.DataSummary.Add(new OLoanOperation.Data
                                    {
                                        Title = item,
                                        TotalLoanAmount = 0,
                                        TotalLoanCount = 0,
                                        MonthNo = GetMonthNumber_From_MonthName(item)
                                    });
                                }

                            }

                            response.DataByDate = _OTrList.GroupBy(x => x.Date)
                            .Select(x => new OLoanOperation.Data
                            {
                                Title = x.Key.Value.ToString("dd"),
                                Month = x.Key.Value.ToString("MMM"),
                                TotalLoanAmount = x.Sum(a => a.LoanAmount),
                                TotalLoanCount = x.Sum(a => a.LoanCount),
                                MonthNo = x.Select(a => a.MonthNo).FirstOrDefault()
                            }).ToList();

                            double totalamount = response.DataSummary.Count > 1 ? Convert.ToDouble(response.DataSummary[1].TotalLoanAmount) : 0;
                            response.Difference = Math.Abs(Convert.ToDouble(totalamount - response.DataSummary[0].TotalLoanAmount));
                            double Total = Convert.ToDouble(totalamount + response.DataSummary[0].TotalLoanAmount);
                            response.Differencepercent = Math.Ceiling((response.Difference * 100) / Total);
                        }
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, response, "BNPL0200", ResponseCode.BNPL0200);
                }


            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetDisbursementByMonth", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account loan history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountLoanHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                .Where(x => x.AccountId == _Request.ReferenceId && x.Account.Guid == _Request.ReferenceKey)
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OLoanOperation.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantId = x.MerchantId,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantIconURL = x.Merchant.Account.IconStorage.Path,
                                                    LoanAmount = x.Amount,
                                                    InstallmentAmount = x.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    PaidInstallments = x.BNPLAccountLoanPayment.Count(c => c.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Count(),
                                                    StatusId = x.StatusId,
                                                    Status = x.Status.Name,
                                                    MerchantRedeem = x.IsRedeemed,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OLoanOperation.List> _List = _HCoreContext.BNPLAccountLoan
                                                .Where(x => x.AccountId == _Request.ReferenceId && x.Account.Guid == _Request.ReferenceKey)
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OLoanOperation.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    MerchantId = x.MerchantId,
                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantIconURL = x.Merchant.Account.IconStorage.Path,
                                                    LoanAmount = x.Amount,
                                                    InstallmentAmount = x.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    PaidInstallments = x.BNPLAccountLoanPayment.Count(c => c.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    TotalInstallments = x.BNPLAccountLoanPayment.Count(),
                                                    StatusId = x.StatusId,
                                                    Status = x.Status.Name,
                                                    MerchantRedeem = x.IsRedeemed,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconURL))
                        {
                            DataItem.MerchantIconURL = _AppConfig.StorageUrl + DataItem.MerchantIconURL;
                        }
                        else
                        {
                            DataItem.MerchantIconURL = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAccountLoanHistory", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account repayment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountRepayments(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoanPayment
                                                .Where(x => x.Loan.Account.Id == _Request.ReferenceId 
                                                    && x.Loan.Account.Guid == _Request.ReferenceKey
                                                    && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .OrderByDescending(x => x.LoanId)
                                                .Select(x => new OLoanOperation.Repayments
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    LoanId = x.LoanId,
                                                    MerchantId = x.Loan.MerchantId,
                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    LoanAmount = x.Amount,
                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(y => y.LoanId == x.LoanId),
                                                    PaidInstallment = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    LastPayment = x.PaymentDate,
                                                    Repayment = x.Amount,
                                                    OverDueInstallment = x.Loan.BNPLAccountLoanPayment.Count(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OLoanOperation.Repayments> _List = _HCoreContext.BNPLAccountLoanPayment
                                                .Where(x => x.Loan.Account.Id == _Request.ReferenceId 
                                                && x.Loan.Account.Guid == _Request.ReferenceKey
                                                && x.Loan.Account.Account.CountryId == _Request.UserReference.CountryId)
                                                .OrderByDescending(x => x.LoanId)
                                                .Select(x => new OLoanOperation.Repayments
                                                {

                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    LoanId = x.LoanId,
                                                    MerchantId = x.Loan.MerchantId,
                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    LoanAmount = x.Amount,
                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(y => y.LoanId == x.LoanId),
                                                    PaidInstallment = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    LastPayment = x.PaymentDate,
                                                    Repayment = x.Amount,
                                                    OverDueInstallment = x.Loan.BNPLAccountLoanPayment.Count(f => f.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetAccountRepayments", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the name of the month number from month.
        /// </summary>
        /// <param name="monthname">The monthname.</param>
        /// <returns>System.Int32.</returns>
        public static int GetMonthNumber_From_MonthName(string monthname)
        {
            int monthNumber = 0;
            monthNumber = DateTime.ParseExact(monthname, "MMM", CultureInfo.CurrentCulture).Month;
            return monthNumber;
        }

        /// <summary>
        /// Description: Gets the loan provider list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoanProviders(OList.Request _Request)
        {
            try
            {
                _Providers = new List<OLoanOperation.Providers>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.LoanProvider
                                                && x.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.Providers
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ProviderName = x.DisplayName
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                    }
                    _Providers = _HCoreContext.HCUAccount
                                                .Where(x => x.AccountTypeId == UserAccountType.LoanProvider
                                                && x.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OLoanOperation.Providers
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    ProviderName = x.DisplayName
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();

                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _Providers, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetLoanProviders", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
        }
    }

    internal class ActorGetRepaymentsDownload : ReceiveActor
    {
        public ActorGetRepaymentsDownload()
        {
            FrameWorkLoanOperation _FrameWorkLoanOperation;
            Receive<OList.Request>(_Request =>
            {
                _FrameWorkLoanOperation = new FrameWorkLoanOperation();
                _FrameWorkLoanOperation.GetRepaymentsDownload(_Request);
            });
        }
    }
}

