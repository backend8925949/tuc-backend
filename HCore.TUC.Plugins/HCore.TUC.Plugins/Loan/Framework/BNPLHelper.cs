//==================================================================================
// FileName: BNPLHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.TUC.Plugins.Loan.Framework
{
    public static class BNPLHelper
    {
        public static class StatusHelper
        {
            public static class Loan
            {
                public const int Requested = 736;
                public const int PreApproved = 737;
                public const int Approved = 738;
                public const int Submitted = 772;
                public const int Rejected = 739;
                public const int Running = 740;
                public const int Closed = 741;
                public const int Cancelled = 742;
                public const int Default = 773;
            }   
            public static class LoanPayment
            {
                public const int Pending = 744;
                public const int Initialized = 745;
                public const int Processing = 746;
                public const int Successful = 747;
                public const int OverDue = 748;
            }
            public static class LoanProvider
            {
                public const int EvolveCredit = 10202;
                public const int ThankUCash = 10203;
            }

            public static class MerchantSetelment
            {
                public const int Initialized = 777;
                public const int Pending = 778;
                public const int Cancelled = 779;
                public const int Processing = 780;
                public const int Successful = 781;
            }
        }
    }
}
