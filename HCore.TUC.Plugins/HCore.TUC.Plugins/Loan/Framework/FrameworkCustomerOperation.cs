//==================================================================================
// FileName: FrameworkCustomerOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to customer operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Integration.Evolve;
using HCore.Integration.Evolve.EvolveObject.Requests;
using HCore.Operations;
using HCore.Operations.Object;
using HCore.TUC.Plugins.Loan.Object;
using HCore.TUC.Plugins.Loan.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
namespace HCore.TUC.Plugins.Loan.Framework
{
    internal class FrameworkCustomerOperation
    {
        HCoreContext _HCoreContext;
        BNPLAccount _BNPLAccount;
        BNPLAccountLoan _BNPLAccountLoan;
        BNPLAccountLoanPayment _BNPLAccountLoanPayment;
        List<BNPLAccountLoanPayment> _BNPLAccountLoanPayments;
        OCustomerOperation.Customer.Configuration.Response _CustomerConfig;
        OCustomerOperation.Customer.Configuration.SystemConfig _SystemConfig;
        //EvolveOperations _EvolveOperations;
        List<GetRepaymentss> _GetRepaymentss;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;

        /// <summary>
        /// Description: Gets the customer configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerConfiguration(OCustomerOperation.Customer.Configuration.Request _Request)
        {
            #region Manage Exception
            try
            {
                _CustomerConfig = new OCustomerOperation.Customer.Configuration.Response();
                _CustomerConfig.IsFeatureEnabled = true;
                _CustomerConfig.FeatureEnabledMessage = "Feature is not activated on your mobile number. Please try after sometime";
                using (_HCoreContext = new HCoreContext())
                {
                    _SystemConfig = new OCustomerOperation.Customer.Configuration.SystemConfig();
                    if (HostEnvironment == HostEnvironmentType.Test)
                    {
                        //_SystemConfig.monoKey = "test_pk_Fwtg0gRCK00JqUxLtpp3";
                        _SystemConfig.monoKey = "live_pk_9mnYehBh9QYNmelyz3ji";
                    }
                    else
                    {
                        _SystemConfig.monoKey = "live_pk_9mnYehBh9QYNmelyz3ji";
                    }
                    //if(_Request.Mode.Mono == "test")
                    //{
                    //    _SystemConfig.monoKey = "test_pk_Fwtg0gRCK00JqUxLtpp3";
                    //}
                    //else
                    //{
                    //    _SystemConfig.monoKey = "live_pk_9mnYehBh9QYNmelyz3ji";
                    //}

                    //if(_Request.Mode.Evolve == "test")
                    //{

                    //}
                    //else
                    //{

                    //}

                    _CustomerConfig.SystemConfig = _SystemConfig;
                    var LoanAccount = _HCoreContext.BNPLAccount.Where(x => x.AccountId == _Request.UserReference.AccountId)
                        .Select(x => new
                        {
                            Loans = x.BNPLAccountLoan.Count(),
                            ActiveLoans = x.BNPLAccountLoan.Count(a => a.StatusId == 2),
                            ActiveLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == 2).Sum(a => a.TotalAmount),
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            CreditLimit = x.CreditLimit,
                            AvailableCreditLimit = x.AvailableCreditLimit,
                        }).FirstOrDefault();
                    if (LoanAccount != null)
                    {
                        _CustomerConfig.IsBnplEnable = true;
                        _CustomerConfig.IsLoanActive = true;
                        _CustomerConfig.ReferenceId = LoanAccount.ReferenceId;
                        _CustomerConfig.ReferenceKey = LoanAccount.ReferenceKey;
                        _CustomerConfig.ActiveLoan = LoanAccount.ActiveLoans;
                        _CustomerConfig.CreditLimit = LoanAccount.CreditLimit;
                        _CustomerConfig.AvailableCredit = LoanAccount.AvailableCreditLimit;
                        _CustomerConfig.MaximumLoanAmount = _CustomerConfig.AvailableCredit;
                        _CustomerConfig.MinimumLoanAmount = 1000;
                        _CustomerConfig.Plans = _HCoreContext.BNPLPlan
                                                .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OCustomerOperation.Plan.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    Description = x.Description,
                                                    Policy = x.Policy,
                                                    Tenture = x.Tenture,
                                                    MinimumLoanAmount = x.MinimumLoanAmount,
                                                    CustomerInterestRate = x.ProviderInterestRate,
                                                })
                                             .ToList();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerConfig, "BNPL0200", ResponseCode.BNPL0200);
                    }
                    else
                    {
                        _CustomerConfig.IsBnplEnable = false;
                        _CustomerConfig.IsLoanActive = false;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _CustomerConfig, "BNPL0200", ResponseCode.BNPL0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, null, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Saves the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCustomer(OCustomerOperation.Customer.Profile.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0401", ResponseCode.BNPL0401);
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0402", ResponseCode.BNPL0402);
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0403", ResponseCode.BNPL0403);
                }
                if (_Request.DateOfBirth == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0405", ResponseCode.BNPL0405);
                }
                _CustomerConfig = new OCustomerOperation.Customer.Configuration.Response();
                using (_HCoreContext = new HCoreContext())
                {
                    var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            StatusId = x.StatusId,
                            MobileNumber = x.MobileNumber,
                        }).FirstOrDefault();
                    if (CustomerDetails != null)
                    {
                        if (CustomerDetails.StatusId == HelperStatus.Default.Active)
                        {
                            var LoanAccount = _HCoreContext.BNPLAccount.Where(x => x.AccountId == _Request.UserReference.AccountId)
                                               .Select(x => new
                                               {
                                                   ActiveLoans = x.BNPLAccountLoan.Count(a => a.StatusId == 2),
                                               }).FirstOrDefault();
                            if (LoanAccount == null)
                            {
                                _BNPLAccount = new BNPLAccount();
                                _BNPLAccount.Guid = HCoreHelper.GenerateGuid();
                                _BNPLAccount.AccountId = CustomerDetails.ReferenceId;
                                _BNPLAccount.FirstName = _Request.FirstName;
                                _BNPLAccount.LastName = _Request.LastName;
                                _BNPLAccount.EmailAddress = _Request.EmailAddress;
                                _BNPLAccount.WorkEmailAddress = _Request.WorkEmailAddress;
                                _BNPLAccount.MobileNumber = CustomerDetails.MobileNumber;
                                _BNPLAccount.DateOfBirth = _Request.DateOfBirth;
                                _BNPLAccount.BvnNumber = _Request.BvnNumber;
                                _BNPLAccount.Salary = _Request.Salary;
                                _BNPLAccount.SalaryDay = _Request.SalaryDay;
                                _BNPLAccount.Address = _Request.Address;
                                _BNPLAccount.StateName = _Request.StateName;
                                _BNPLAccount.Latitude = _Request.Latitude;
                                _BNPLAccount.Longitude = _Request.Longitude;
                                _BNPLAccount.LgaName = _Request.LgaName;
                                _BNPLAccount.Latitude = _Request.Latitude;
                                _BNPLAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                                _BNPLAccount.CreatedById = _Request.UserReference.AccountId;
                                _BNPLAccount.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.BNPLAccount.Add(_BNPLAccount);
                                _HCoreContext.SaveChanges();

                                using (_HCoreContext = new HCoreContext())
                                {

                                    var _Response = new
                                    {
                                        IsLoanActive = true,
                                        ReferenceId = _BNPLAccount.Id,
                                        ReferenceKey = _BNPLAccount.Guid,
                                        ActiveLoan = 0,
                                        MinimumLoanAmount = 50000,
                                        MaximumLoanAmount = 100000,
                                        Plans = _HCoreContext.BNPLPlan
                                                .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OCustomerOperation.Plan.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    Description = x.Description,
                                                    Policy = x.Policy,
                                                    Tenture = x.Tenture,
                                                    MinimumLoanAmount = x.MinimumLoanAmount,
                                                    CustomerInterestRate = x.ProviderInterestRate,
                                                })
                                             .ToList()
                                    };
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "BNPL0409", ResponseCode.BNPL0409);
                                }
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0406", ResponseCode.BNPL0406);
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0407", ResponseCode.BNPL0407);
                        }

                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0404", ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, null, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchants(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.SubReferenceId > 0)
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.BNPLMerchant
                                                    .Where(x => x.Account.StatusId == HelperStatus.Default.Active && x.StatusId == HelperStatus.Default.Active && x.Account.PrimaryCategoryId == _Request.SubReferenceId)
                                                    .Select(x => new OCustomerOperation.Merchant.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.Account.DisplayName,
                                                        Address = x.Account.Address,
                                                        StatusId = x.Account.StatusId,
                                                        StatusCode = x.Account.Status.SystemName,
                                                        StatusName = x.Account.Status.Name,
                                                        ActiveLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running),
                                                        ClosedLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed),
                                                        ActiveLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        ClosedLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        TotalInterest = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.Charge),
                                                        TUCFees = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.SystemAmount)),
                                                        MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.ProviderAmount)),
                                                        MerchantSettlementStatusId = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.StatusId).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusCode = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.SystemName).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusName = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                        IconUrl = x.Account.IconStorage.Path,
                                                        Locations = x.Account.InverseOwner.Count(a => a.AccountTypeId == UserAccountType.MerchantStore),
                                                        RewardPercentage = x.Account.AccountPercentage,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OCustomerOperation.Merchant.List> _List = _HCoreContext.BNPLMerchant
                                                    .Where(x => x.Account.StatusId == HelperStatus.Default.Active && x.StatusId == HelperStatus.Default.Active && x.Account.PrimaryCategoryId == _Request.SubReferenceId)
                                                    .Select(x => new OCustomerOperation.Merchant.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.Account.DisplayName,
                                                        Address = x.Account.Address,
                                                        StatusId = x.Account.StatusId,
                                                        StatusCode = x.Account.Status.SystemName,
                                                        StatusName = x.Account.Status.Name,
                                                        ActiveLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running),
                                                        ClosedLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed),
                                                        ActiveLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        ClosedLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        TotalInterest = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.Charge),
                                                        TUCFees = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.SystemAmount)),
                                                        MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.ProviderAmount)),
                                                        MerchantSettlementStatusId = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.StatusId).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusCode = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.SystemName).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusName = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                        IconUrl = x.Account.IconStorage.Path,
                                                        Locations = x.Account.InverseOwner.Count(a => a.AccountTypeId == UserAccountType.MerchantStore),
                                                        RewardPercentage = x.Account.AccountPercentage,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                        #endregion
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.BNPLMerchant
                                                    .Where(x => x.Account.StatusId == HelperStatus.Default.Active && x.StatusId == HelperStatus.Default.Active && x.Account.IconStorageId != null)
                                                    .Select(x => new OCustomerOperation.Merchant.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.Account.DisplayName,
                                                        Address = x.Account.Address,
                                                        ActiveLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running),
                                                        ClosedLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed),
                                                        ActiveLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        ClosedLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        TotalInterest = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.Charge),
                                                        TUCFees = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.SystemAmount)),
                                                        MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.ProviderAmount)),
                                                        MerchantSettlementStatusId = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.StatusId).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusCode = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.SystemName).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusName = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                        IconUrl = x.Account.IconStorage.Path,
                                                        Locations = x.Account.InverseOwner.Count(a => a.AccountTypeId == UserAccountType.MerchantStore),
                                                        RewardPercentage = x.Account.AccountPercentage,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OCustomerOperation.Merchant.List> _List = _HCoreContext.BNPLMerchant
                                                    .Where(x => x.Account.StatusId == HelperStatus.Default.Active && x.StatusId == HelperStatus.Default.Active && x.Account.IconStorageId != null)
                                                    .Select(x => new OCustomerOperation.Merchant.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.Account.DisplayName,
                                                        Address = x.Account.Address,
                                                        ActiveLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running),
                                                        ClosedLoans = x.BNPLAccountLoan.Count(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed),
                                                        ActiveLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        ClosedLoansAmount = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Closed && a.BNPLAccountLoanPayment.Any(z => z.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful)).Sum(m => m.TotalAmount),
                                                        TotalInterest = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.Charge),
                                                        TUCFees = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.SystemAmount)),
                                                        MerchantSettlement = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Sum(m => m.BNPLAccountLoanPayment.Sum(z => z.ProviderAmount)),
                                                        MerchantSettlementStatusId = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.StatusId).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusCode = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.SystemName).FirstOrDefault()).FirstOrDefault(),
                                                        MerchantSettlementStatusName = x.BNPLAccountLoan.Where(a => a.StatusId == BNPLHelper.StatusHelper.Loan.Running).Select(m => m.BNPLAccountLoanPayment.Select(z => z.Status.Name).FirstOrDefault()).FirstOrDefault(),
                                                        IconUrl = x.Account.IconStorage.Path,
                                                        Locations = x.Account.InverseOwner.Count(a => a.AccountTypeId == UserAccountType.MerchantStore),
                                                        RewardPercentage = x.Account.AccountPercentage,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        #region Create  Response Object
                        foreach (var DataItem in _List)
                        {
                            if (!string.IsNullOrEmpty(DataItem.IconUrl))
                            {
                                DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                            }
                            else
                            {
                                DataItem.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the plan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPlans(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLPlan
                                                .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OCustomerOperation.Plan.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    Description = x.Description,
                                                    Policy = x.Policy,
                                                    Tenture = x.Tenture,
                                                    MinimumLoanAmount = x.MinimumLoanAmount,
                                                    CustomerInterestRate = x.ProviderInterestRate,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCustomerOperation.Plan.List> _List = _HCoreContext.BNPLPlan
                                                .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OCustomerOperation.Plan.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    Description = x.Description,
                                                    Policy = x.Policy,
                                                    Tenture = x.Tenture,
                                                    MinimumLoanAmount = x.MinimumLoanAmount,
                                                    CustomerInterestRate = x.ProviderInterestRate,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.IconUrl))
                    //    {
                    //        DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.IconUrl = _AppConfig.Default_Icon;
                    //    }
                    //}
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Initializes the loan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        // internal OResponse InitializeLoan(OCustomerOperation.Loan.Initialize _Request)
        // {
        //     #region Manage Exception
        //     try
        //     {
        //         _CustomerConfig = new OCustomerOperation.Customer.Configuration.Response();
        //         using (_HCoreContext = new HCoreContext())
        //         {
        //             var LoanAccount = _HCoreContext.BNPLAccount.Where(x => x.AccountId == _Request.UserReference.AccountId)
        //                 .Select(x => new
        //                 {
        //                     ReferenceId = x.Id,
        //                     ReferenceKey = x.Guid,
        //                     AccountId = x.AccountId,
        //                     StatusId = x.StatusId,
        //                     AccountStatusId = x.Account.StatusId,
        //                 }).FirstOrDefault();
        //             if (LoanAccount != null)
        //             {
        //                 if (LoanAccount.AccountStatusId == HelperStatus.Default.Active)
        //                 {
        //                     if (LoanAccount.StatusId == HelperStatus.Default.Active)
        //                     {
        //                         var AccountLoan = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.AccountId == _Request.UserReference.AccountId && x.VenderLoanId == _Request.LoanRequestId).FirstOrDefault();
        //                         var MerchantDetails = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.AccountId == _Request.UserReference.AccountId && x.VenderLoanId == _Request.LoanRequestId)
        //                                               .Select(a => new
        //                                               {
        //                                                   MerchantName = a.Merchant.Account.DisplayName
        //                                               }).FirstOrDefault();
        //                         var PlanDetails = _HCoreContext.BNPLPlan.Where(x => x.Id == _Request.PlanId).FirstOrDefault();
        //                         if (PlanDetails != null)
        //                         {
        //                             _EvolveOperations = new EvolveOperations();
        //                             var _ResponseApproveLoanItem = _EvolveOperations.DisburseLoan("disbursement", (long)AccountLoan.VenderLoanId, _Request.UserReference);
        //                             if (_ResponseApproveLoanItem != null)
        //                             {
        //                                 _GetRepaymentss = new List<GetRepaymentss>();
        //                                 GetRepaymentsStructure _ResponseRepayment = _EvolveOperations.GetLoanRepaymentStructureCore(PlanDetails.Id, _ResponseApproveLoanItem.Data3.Loan_Id);
        //                                 if (_ResponseRepayment != null)
        //                                 {
        //                                     _GetRepaymentss = _ResponseRepayment.Data;
        //                                 }
        //                                 double PerMonthAmount = Math.Round(_Request.Amount / PlanDetails.Tenture, 2);
        //                                 double InterestAmount = HCoreHelper.GetPercentage(_Request.Amount, PlanDetails.ProviderInterestRate, 2);
        //                                 double TotalAmount = PerMonthAmount + InterestAmount;
        //                                 _BNPLAccountLoanPayments = new List<BNPLAccountLoanPayment>();
        //                                 // First Payment
        //                                 _BNPLAccountLoanPayment = new BNPLAccountLoanPayment();
        //                                 _BNPLAccountLoanPayment.LoanId = AccountLoan.Id;
        //                                 _BNPLAccountLoanPayment.VendorLoanRepaymentId = 0;
        //                                 _BNPLAccountLoanPayment.Guid = HCoreHelper.GenerateGuid();
        //                                 _BNPLAccountLoanPayment.Amount = PerMonthAmount;
        //                                 _BNPLAccountLoanPayment.InterestAmount = 0;
        //                                 _BNPLAccountLoanPayment.Charge = 0;
        //                                 _BNPLAccountLoanPayment.TotalAmount = PerMonthAmount;
        //                                 _BNPLAccountLoanPayment.PrincipalAmount = PerMonthAmount;
        //                                 _BNPLAccountLoanPayment.SystemAmount = 0;
        //                                 _BNPLAccountLoanPayment.ProviderAmount = 0;
        //                                 _BNPLAccountLoanPayment.PaymentReference = _Request.PaymentReference;
        //                                 _BNPLAccountLoanPayment.PaymentDate = HCoreHelper.GetGMTDateTime();
        //                                 _BNPLAccountLoanPayment.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                 _BNPLAccountLoanPayment.CreatedById = _Request.UserReference.AccountId;
        //                                 _BNPLAccountLoanPayment.StatusId = BNPLHelper.StatusHelper.LoanPayment.Successful;
        //                                 _HCoreContext.BNPLAccountLoanPayment.Add(_BNPLAccountLoanPayment);
        //                                 foreach (var item in _ResponseRepayment.Data)
        //                                 {
        //                                     _BNPLAccountLoanPayment = new BNPLAccountLoanPayment();
        //                                     _BNPLAccountLoanPayment.Guid = HCoreHelper.GenerateGuid();
        //                                     _BNPLAccountLoanPayment.LoanId = AccountLoan.Id;
        //                                     _BNPLAccountLoanPayment.VendorLoanRepaymentId = item.Id;
        //                                     _BNPLAccountLoanPayment.Amount = item.Amount;
        //                                     _BNPLAccountLoanPayment.InterestAmount = item.Interest;
        //                                     _BNPLAccountLoanPayment.Charge = 0;
        //                                     _BNPLAccountLoanPayment.TotalAmount = (item.Amount + item.Interest);
        //                                     _BNPLAccountLoanPayment.PrincipalAmount = PerMonthAmount;
        //                                     _BNPLAccountLoanPayment.SystemAmount = HCoreHelper.GetPercentage(InterestAmount, PlanDetails.SystemInterestRate, 2);
        //                                     _BNPLAccountLoanPayment.ProviderAmount = HCoreHelper.GetPercentage(InterestAmount, PlanDetails.ProviderInterestRate, 2);
        //                                     _BNPLAccountLoanPayment.PaymentDate = DateTime.Parse(item.Due_On);
        //                                     _BNPLAccountLoanPayment.CreateDate = HCoreHelper.GetGMTDateTime();
        //                                     _BNPLAccountLoanPayment.CreatedById = _Request.UserReference.AccountId;
        //                                     _BNPLAccountLoanPayment.StatusId = BNPLHelper.StatusHelper.LoanPayment.Pending;
        //                                     _HCoreContext.BNPLAccountLoanPayment.Add(_BNPLAccountLoanPayment);
        //                                 }
        //                                 _HCoreContext.SaveChanges();
        //                                 var _Response = new
        //                                 {
        //                                     ReferenceId = AccountLoan.Id,
        //                                     ReferenceKey = AccountLoan.Guid,
        //                                 };

        //                                 //Credits amount from TUC wallet to Customers BNPL wallet
        //                                 _CoreTransactionRequest = new OCoreTransaction.Request();
        //                                 _CoreTransactionRequest.CustomerId = LoanAccount.AccountId;
        //                                 _CoreTransactionRequest.UserReference = _Request.UserReference;
        //                                 _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
        //                                 _CoreTransactionRequest.ParentId = SystemAccounts.BNPLSystemId;
        //                                 _CoreTransactionRequest.InvoiceAmount = AccountLoan.Amount;
        //                                 _CoreTransactionRequest.ReferenceInvoiceAmount = AccountLoan.Amount;
        //                                 _CoreTransactionRequest.ReferenceAmount = AccountLoan.Amount;
        //                                 _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
        //                                 _TransactionItems = new List<OCoreTransaction.TransactionItem>();
        //                                 _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                 {
        //                                     UserAccountId = SystemAccounts.BNPLSystemId,
        //                                     ModeId = TransactionMode.Debit,
        //                                     TypeId = TransactionType.TUCBnpl.LoanCredit,
        //                                     SourceId = TransactionSource.TUCBnpl,
        //                                     Amount = AccountLoan.Amount,
        //                                     TotalAmount = AccountLoan.Amount,
        //                                 });
        //                                 _TransactionItems.Add(new OCoreTransaction.TransactionItem
        //                                 {
        //                                     UserAccountId = LoanAccount.AccountId,
        //                                     ModeId = TransactionMode.Credit,
        //                                     TypeId = TransactionType.TUCBnpl.LoanCredit,
        //                                     SourceId = TransactionSource.TUCBnpl,
        //                                     Amount = AccountLoan.Amount,
        //                                     TotalAmount = AccountLoan.Amount,
        //                                 });

        //                                 _CoreTransactionRequest.Transactions = _TransactionItems;
        //                                 _ManageCoreTransaction = new ManageCoreTransaction();
        //                                 OCoreTransaction.Response TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
        //                                 if (TransactionResponse.Status == HelperStatus.Transaction.Success)
        //                                 {
        //                                     string UserNotificationUrl = _HCoreContext.HCUAccountSession
        //                                         .Where(x => x.AccountId == LoanAccount.AccountId && x.StatusId == 2 && x.NotificationUrl != null)
        //                                         .OrderByDescending(x => x.LoginDate)
        //                                         .Select(x => x.NotificationUrl)
        //                                         .FirstOrDefault();
        //                                     if (!string.IsNullOrEmpty(UserNotificationUrl))
        //                                     {
        //                                         string Message = "Your loan request for amount N" + HCoreHelper.RoundNumber(AccountLoan.Amount, _AppConfig.SystemExitRoundDouble).ToString() + " Has disbursed successfully. You can redeem your loan at " + MerchantDetails.MerchantName;
        //                                         if (HostEnvironment == HostEnvironmentType.Live)
        //                                         {
        //                                             HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "Loan Alert:  N " + AccountLoan.Amount + " disbursed.", Message, "dashboard", 0, null, "View details", false, null, null);
        //                                         }
        //                                         else
        //                                         {
        //                                             HCoreHelper.SendPushToDevice(UserNotificationUrl, "dashboard", "TEST : Loan Alert:  N " + AccountLoan.Amount + " disbursed.", Message, "dashboard", 0, null, "View details", false, null, null);
        //                                         }
        //                                     }
        //                                     return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "BNPLSAVED", "Loan approved");
        //                                 }
        //                                 else
        //                                 {
        //                                     return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _Response, "BNPLSAVED", "Loan approvel failed. Please try after some time");
        //                                 }
        //                             }
        //                             else
        //                             {
        //                                 return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPLSAVED", "Loan disbursement failed. Please try after some time");
        //                             }
        //                         }
        //                         else
        //                         {
        //                             return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0411", ResponseCode.BNPL0411);
        //                         }
        //                     }
        //                     else
        //                     {
        //                         return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0413", ResponseCode.BNPL0413);
        //                     }
        //                 }
        //                 else
        //                 {
        //                     return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0414", ResponseCode.BNPL0414);
        //                 }
        //             }
        //             else
        //             {
        //                 return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0412", ResponseCode.BNPL0412);
        //             }
        //         }
        //     }
        //     catch (Exception _Exception)
        //     {
        //         return HCoreHelper.LogException("InitializeLoan", _Exception, _Request.UserReference, null, "BNPL0500", ResponseCode.BNPL0500);
        //     }
        //     #endregion
        // }

        /// <summary>
        /// Description: Gets the loan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoans(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoan
                                                .Select(x => new OCustomerOperation.LoanPurchase.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerName = x.Account.FirstName + " " + x.Account.LastName,
                                                    CustomerMobileNumber = x.Account.MobileNumber,

                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Merchant.Account.Address,
                                                    MerchantIconUrl = x.Merchant.Account.IconStorage.Path,

                                                    ProviderReference = x.Account.ProviderReference,
                                                    TotalAmount = x.TotalAmount,
                                                    TotalPaidAmount = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),
                                                    LoanAmount = x.Amount,
                                                    InstallmentAmount = x.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    InterestRate = x.Plan.ProviderInterestRate,
                                                    TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(m => m.InterestAmount),
                                                    TotalInstallment = x.BNPLAccountLoanPayment.Count(),
                                                    PaidInstallment = x.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    TUCFees = x.BNPLAccountLoanPayment.Where(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.SystemAmount),

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }

                    #region Get Data
                    List<OCustomerOperation.LoanPurchase.List> _List = _HCoreContext.BNPLAccountLoan
                                                .Select(x => new OCustomerOperation.LoanPurchase.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerName = x.Account.FirstName + " " + x.Account.LastName,
                                                    CustomerMobileNumber = x.Account.MobileNumber,

                                                    MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Merchant.Account.Address,
                                                    MerchantIconUrl = x.Merchant.Account.IconStorage.Path,

                                                    ProviderReference = x.Account.ProviderReference,
                                                    TotalAmount = x.TotalAmount,
                                                    TotalPaidAmount = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),
                                                    LoanAmount = x.Amount,
                                                    InstallmentAmount = x.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                    InterestRate = x.Plan.ProviderInterestRate,
                                                    TotalInterest = x.BNPLAccountLoanPayment.Where(a => a.LoanId == x.Id).Sum(m => m.InterestAmount),
                                                    TotalInstallment = x.BNPLAccountLoanPayment.Count(),
                                                    PaidInstallment = x.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    TUCFees = x.BNPLAccountLoanPayment.Where(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.SystemAmount),

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        if (DataItem.TUCFees > 0)
                        {
                            DataItem.TUCFeesPercentage = (DataItem.TUCFees / DataItem.TotalAmount) * 100;
                        }
                        else
                        {
                            DataItem.TUCFeesPercentage = 0;
                        }
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetLoans", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the loan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetLoan(OReference _Request)
        {
            #region Manage Exception
            try
            {
                _CustomerConfig = new OCustomerOperation.Customer.Configuration.Response();
                using (_HCoreContext = new HCoreContext())
                {
                    OCustomerOperation.LoanPurchase.Details DataItem = _HCoreContext.BNPLAccountLoan
                                              .Where(x => x.Account.AccountId == _Request.UserReference.AccountId && x.Id == _Request.ReferenceId)
                                              .OrderByDescending(x => x.CreateDate)
                                              .Select(x => new OCustomerOperation.LoanPurchase.Details
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  MerchantDisplayName = x.Merchant.Account.DisplayName,
                                                  MerchantIconUrl = x.Merchant.Account.IconStorage.Path,
                                                  LoanAmount = x.Amount,
                                                  InstallmentAmount = x.BNPLAccountLoanPayment.FirstOrDefault().Amount,
                                                  InterestRate = x.Plan.ProviderInterestRate,
                                                  TotalInstallment = x.BNPLAccountLoanPayment.Count(),
                                                  PaidInstallment = x.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                                  ClosedOn = x.EndDate,
                                                  LoanCode = x.LoanCode,
                                                  RequestDate = x.CreateDate,
                                              }).FirstOrDefault();
                    #endregion
                    #region Create  Response Object
                    DataItem.Payments = _HCoreContext.BNPLAccountLoanPayment.Where(x => x.LoanId == DataItem.ReferenceId)
                        .Select(x => new OCustomerOperation.LoanPurchase.LoanPayment
                        {
                            ReferenceId = x.Id,
                            Amount = x.TotalAmount,
                            DueDate = x.PaymentDate,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).ToList();
                    if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                    {
                        DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                    }
                    else
                    {
                        DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, DataItem, "BNPL0200", ResponseCode.BNPL0200);


                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, null, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the repayment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRepayments(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccountLoanPayment
                                                .Select(x => new OLoanOperations.Repayments
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CustomerName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                    CustomerMobileNumber = x.Loan.Account.MobileNumber,

                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Loan.Merchant.Account.Address,

                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,
                                                    ProviderReference = x.Loan.Account.ProviderReference,
                                                    TotalAmount = x.TotalAmount,
                                                    TotalPaidAmount = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),
                                                    LoanAmount = x.Loan.Amount,
                                                    InstallmentAmount = x.Amount,
                                                    InterestRate = x.Loan.Plan.ProviderInterestRate,
                                                    TotalInterest = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId).Sum(m => m.InterestAmount),
                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(),
                                                    PaidInstallment = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    PendingInstallments = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending),
                                                    OverdueInstallments = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),
                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),
                                                    LastPaid = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Select(m => m.PaymentDate).FirstOrDefault(),

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OLoanOperations.Repayments> _List = _HCoreContext.BNPLAccountLoanPayment
                                                .Select(x => new OLoanOperations.Repayments
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    CustomerName = x.Loan.Account.FirstName + " " + x.Loan.Account.LastName,
                                                    CustomerMobileNumber = x.Loan.Account.MobileNumber,

                                                    MerchantDisplayName = x.Loan.Merchant.Account.DisplayName,
                                                    MerchantAddress = x.Loan.Merchant.Account.Address,

                                                    LoanId = x.LoanId,
                                                    LoanKey = x.Loan.Guid,
                                                    ProviderReference = x.Loan.Account.ProviderReference,
                                                    TotalAmount = x.TotalAmount,
                                                    TotalPaidAmount = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Sum(m => m.Amount),
                                                    LoanAmount = x.Loan.Amount,
                                                    InstallmentAmount = x.Amount,
                                                    InterestRate = x.Loan.Plan.ProviderInterestRate,
                                                    TotalInterest = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId).Sum(m => m.InterestAmount),
                                                    TotalInstallment = x.Loan.BNPLAccountLoanPayment.Count(),
                                                    PaidInstallment = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful),
                                                    PendingInstallments = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending),
                                                    OverdueInstallments = x.Loan.BNPLAccountLoanPayment.Count(a => a.StatusId == BNPLHelper.StatusHelper.LoanPayment.OverDue),
                                                    DueDate = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Pending).Select(m => m.PaymentDate).FirstOrDefault(),
                                                    LastPaid = x.Loan.BNPLAccountLoanPayment.Where(a => a.LoanId == x.LoanId && a.StatusId == BNPLHelper.StatusHelper.LoanPayment.Successful).Select(m => m.PaymentDate).FirstOrDefault(),

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetLoans", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Checks the customer configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CheckCustomerConfiguration(OCustomerOperation.Configuration _Request)
        {
            try
            {
                using (_HCoreContext =  new HCoreContext())
                {
                    bool CheckConfiguration = _HCoreContext.HCUAccountParameter.Any(x => x.AccountId == _Request.UserReference.AccountId && x.Value == "1");
                    if(!CheckConfiguration)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPLERROR", "You are not eligible for BNPL");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "BNPLSUCCESS", "You are eligible for BNPL");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CheckCustomerConfiguration", _Exception,_Request.UserReference);
            }
        }
    }
}
