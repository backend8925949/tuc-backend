//==================================================================================
// FileName: FrameworkEligibleCustomer.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to bnpl eligible customers
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Loan.Object;
using HCore.TUC.Plugins.Loan.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Threading.Tasks;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.Loan.Framework
{
    internal class FrameworkEligibleCustomer
    {
        HCoreContext _HCoreContext;      
        OCustomerOperation.Overview _Overview;

        /// <summary>
        /// Description: Gets the eligible customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetEligibleCustomer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.BNPLAccount
                                                .Where(x => x.Account.StatusId == HelperStatus.Default.Active 
                                                        && x.StatusId == HelperStatus.Default.Active
                                                        && x.Account.CountryId == _Request.UserReference.CountryId)
                                                .Select(x => new OCustomerOperation.EligibleCustomer
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerName = x.FirstName +" "+ x.LastName,
                                                    IconURL = x.Account.IconStorage.Path,
                                                    ActiveLoans = x.BNPLAccountLoan.Where(a => a.AccountId == x.Id && (a.StatusId == BNPLHelper.StatusHelper.Loan.Running || a.StatusId == BNPLHelper.StatusHelper.Loan.Approved)).Count(),
                                                    MobileNumber=x.MobileNumber,
                                                    LastActivity= x.Account.LastActivityDate,
                                                    CreditLimit= x.CreditLimit,
                                                    ApprovedOn= x.CreateDate,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OCustomerOperation.EligibleCustomer> _List = _HCoreContext.BNPLAccount
                                                .Where(x => x.Account.StatusId == HelperStatus.Default.Active 
                                                  && x.StatusId == HelperStatus.Default.Active
                                                  && x.Account.CountryId == _Request.UserReference.CountryId)
                                                .OrderByDescending(x => x.CreateDate)
                                                .Select(x => new OCustomerOperation.EligibleCustomer
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CustomerName = x.FirstName + x.LastName,
                                                    IconURL = x.Account.IconStorage.Path,
                                                    MobileNumber = x.MobileNumber,
                                                    LastActivity = x.Account.LastActivityDate,
                                                    ActiveLoans = x.BNPLAccountLoan.Where(a => a.AccountId == x.Id && (a.StatusId == BNPLHelper.StatusHelper.Loan.Running || a.StatusId == BNPLHelper.StatusHelper.Loan.Approved)).Count(),
                                                    CreditLimit = x.CreditLimit,
                                                    ApprovedOn = x.CreateDate,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconURL))
                        {
                            DataItem.IconURL = _AppConfig.StorageUrl + DataItem.IconURL;
                        }
                        else
                        {
                            DataItem.IconURL = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetEligibleCustomer", _Exception, _Request.UserReference, _OResponse, "BNPL0500", ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the eligible customer overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetEligibleCustomerOverview(OSummaryOverview.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OSummaryOverview.Overview overview = new OSummaryOverview.Overview();
                    overview.Total = _HCoreContext.BNPLAccount.Where(x => x.Account.StatusId == HelperStatus.Default.Active && x.StatusId == HelperStatus.Default.Active && x.Account.CountryId == _Request.UserReference.CountryId).Count();

                    #endregion


                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, overview, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetEligibleCustomerOverview", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the eligible customers overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetEligibleCustomersOverview(OReference _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    _Overview = new OCustomerOperation.Overview();
                    #endregion
                    _Overview.TotalLoans = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId).Count();
                    _Overview.TotalLoansAmount = _HCoreContext.BNPLAccountLoan.Where(x => x.Account.Account.CountryId == _Request.UserReference.CountryId).Sum(a => a.Amount);
                    _Overview.TotalCreditLimit = _HCoreContext.BNPLAccount.Where(x => x.Account.CountryId == _Request.UserReference.CountryId).Sum(a => a.CreditLimit);
                    _Overview.TotalCustomers = _HCoreContext.BNPLAccount.Where(x => x.Account.CountryId == _Request.UserReference.CountryId).Count();
                    _Overview.TotalLoanCustomers = _HCoreContext.BNPLAccount.Where(x => x.Account.CountryId == _Request.UserReference.CountryId).Count(x => x.BNPLAccountLoan.Any());
                    _Overview.UsedCredit = _Overview.TotalLoanCustomers;
                    _Overview.UnUsedCredit = _Overview.TotalCustomers - _Overview.TotalLoanCustomers;

                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Overview, "BNPL0200", ResponseCode.BNPL0200);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetTotalEligibleCustomerTillToday", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
    }
}
