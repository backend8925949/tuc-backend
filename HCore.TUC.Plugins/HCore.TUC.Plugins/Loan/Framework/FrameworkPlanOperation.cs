//==================================================================================
// FileName: FrameworkPlanOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to plan operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Loan.Object;
using HCore.TUC.Plugins.Loan.Resource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Plugins.Loan.Framework
{
    public class FrameworkPlanOperation
    {
        HCoreContext _HCoreContext;
        BNPLPlan _BNPLPlan;
        BNPLMerchant _BNPLMerchant;
        BNPLMerchantSettlement _BNPLMerchantSettlement;

        /// <summary>
        /// Description: Saves the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SavePlan(OPlanOperation.Save.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0415);
                }
                if (_Request.Tenture < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0416);
                }
                if (_Request.MinimumLoanAmount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0417);
                }
                if (_Request.CustomerInterestRate < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0418);
                }
                if (_Request.ProviderInterestRate < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0419);
                }
                if (_Request.SystemInterestRate < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0420);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLSTATUS);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLINSTATUS);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    bool _IsExists = _HCoreContext.BNPLPlan.Any(x => x.Name == _Request.Name);
                    if (_IsExists)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLEXISTS);
                    }

                    _BNPLPlan = new BNPLPlan();
                    _BNPLPlan.Guid = HCoreHelper.GenerateGuid();
                    _BNPLPlan.Name = _Request.Name;
                    _BNPLPlan.Description = _Request.Description;
                    _BNPLPlan.Policy = _Request.Policy;
                    _BNPLPlan.Tenture = _Request.Tenture;
                    _BNPLPlan.MinimumLoanAmount = _Request.MinimumLoanAmount;
                    _BNPLPlan.CustomerInterestRate = _Request.CustomerInterestRate;
                    _BNPLPlan.ProviderInterestRate = _Request.ProviderInterestRate;
                    _BNPLPlan.SystemInterestRate = _Request.SystemInterestRate;
                    _BNPLPlan.CreateDate = HCoreHelper.GetGMTDateTime();
                    _BNPLPlan.CreatedById = _Request.UserReference.AccountId;
                    _BNPLPlan.StatusId = StatusId;
                    _HCoreContext.BNPLPlan.Add(_BNPLPlan);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _BNPLPlan.Id,
                        ReferenceKey = _BNPLPlan.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLSAVE);
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SavePlan", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdatePlan(OPlanOperation.Update _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLINSTATUS);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _PlanDetails = _HCoreContext.BNPLPlan.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_PlanDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _PlanDetails.Name != _Request.Name)
                        {
                            bool _IsExist = _HCoreContext.BNPLPlan.Any(x => x.Name == _Request.Name && x.Id != _PlanDetails.Id);
                            if (_IsExist)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLEXISTS);
                            }

                            _PlanDetails.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && _PlanDetails.Description != _Request.Description)
                        {
                            _PlanDetails.Description = _Request.Description;
                        }
                        if (!string.IsNullOrEmpty(_Request.Policy) && _PlanDetails.Policy != _Request.Policy)
                        {
                            _PlanDetails.Policy = _Request.Policy;
                        }
                        if (_Request.Tenture > 0)
                        {
                            _PlanDetails.Tenture = _Request.Tenture;
                        }
                        if (_Request.MinimumLoanAmount > 0)
                        {
                            _PlanDetails.MinimumLoanAmount = _Request.MinimumLoanAmount;
                        }
                        if (_Request.CustomerInterestRate > 0)
                        {
                            _PlanDetails.CustomerInterestRate = _Request.CustomerInterestRate;
                        }
                        if (_Request.ProviderInterestRate > 0)
                        {
                            _PlanDetails.ProviderInterestRate = _Request.ProviderInterestRate;
                        }
                        if (_Request.SystemInterestRate > 0)
                        {
                            _PlanDetails.SystemInterestRate = _Request.SystemInterestRate;
                        }
                        if (StatusId > 0)
                        {
                            _PlanDetails.StatusId = StatusId;
                        }

                        _PlanDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _PlanDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var _Response = new
                        {
                            ReferenceId = _PlanDetails.Id,
                            ReferenceKey = _PlanDetails.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLUPDATE);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdatePlan", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeletePlan(OReference _Request)
        {
            #region
            try
            {
                var _PlanDetails = _HCoreContext.BNPLPlan
                    .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                    .FirstOrDefault();
                if (_PlanDetails != null)
                {
                    _HCoreContext.BNPLPlan.Remove(_PlanDetails);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _BNPLPlan.Id,
                        ReferenceKey = _BNPLPlan.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLREMOVE);
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeletePlan", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPlan(OReference _Request)
        {
            #region
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _PlanDetails = _HCoreContext.BNPLPlan.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new OPlanOperation.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            Name = x.Name,
                            Description = x.Description,
                            Policy = x.Policy,
                            Tenture = x.Tenture,
                            MinimumLoanAmount = x.MinimumLoanAmount,
                            CustomerInterestRate = x.CustomerInterestRate,
                            ProviderInterestRate = x.ProviderInterestRate,
                            SystemInterestRate = x.SystemInterestRate,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifyById,
                            ModifyByKey = x.ModifyBy.Guid,
                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();

                    if (_PlanDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PlanDetails, ResponseCode.BNPL0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPlan", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the plan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetPlans(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.BNPLPlan
                            .Select(x => new OPlanOperation.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                Name = x.Name,
                                Description = x.Description,
                                Policy = x.Policy,
                                Tenture = x.Tenture,
                                MinimumLoanAmount = x.MinimumLoanAmount,
                                CustomerInterestRate = x.CustomerInterestRate,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OPlanOperation.List> Data = _HCoreContext.BNPLPlan
                                                .Select(x => new OPlanOperation.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    Description = x.Description,
                                                    Policy = x.Policy,
                                                    Tenture = x.Tenture,
                                                    MinimumLoanAmount = x.MinimumLoanAmount,
                                                    CustomerInterestRate = x.CustomerInterestRate,

                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetPlans", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }


        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveMerchant(OMerchantOperation.Save.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0421);
                }
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0422);
                }
                if (string.IsNullOrEmpty(_Request.SettelmentTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLSETTELMENT);
                }
                if (string.IsNullOrEmpty(_Request.TransactionSourceCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLSETTELMENT);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLSTATUS);
                }
                int? SettelmentTypeId = HCoreHelper.GetHelperId(_Request.SettelmentTypeCode);
                int? TransactionSourceId = HCoreHelper.GetHelperId(_Request.TransactionSourceCode);
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLINSTATUS);
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    bool _IsExists = _HCoreContext.HCUAccount.Any(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey);
                    if (_IsExists)
                    {
                        bool _IsMerchantExists = _HCoreContext.BNPLMerchant.Any(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey);
                        if (_IsMerchantExists)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLEXISTS);
                        }

                        _BNPLMerchant = new BNPLMerchant();
                        _BNPLMerchant.Guid = HCoreHelper.GenerateGuid();
                        _BNPLMerchant.AccountId = _Request.AccountId;
                        _BNPLMerchant.SettelmentTypeId = (int)SettelmentTypeId;
                        if (SettelmentTypeId == SettelmentType.DaysAfterRedeem)
                        {
                            _BNPLMerchant.SettelmentDays = _Request.SettelmentDays * 24;
                        }
                        _BNPLMerchant.TransactionSourceId = (int)TransactionSourceId;
                        _BNPLMerchant.IsMerchantSettelment = _Request.IsMerchantSettelment;
                        _BNPLMerchant.StartDate = _Request.StartDate;
                        //_BNPLMerchant.EndDate = _Request.EndDate;
                        _BNPLMerchant.CreateDate = HCoreHelper.GetGMTDateTime();
                        _BNPLMerchant.CreatedById = _Request.UserReference.AccountId;
                        _BNPLMerchant.StatusId = StatusId;
                        _HCoreContext.BNPLMerchant.Add(_BNPLMerchant);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var _Response = new
                        {
                            ReferenceId = _BNPLMerchant.Id,
                            ReferenceKey = _BNPLMerchant.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLSAVE);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLNOTEXISTS);
                    }
                }

            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveMerchant", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateMerchant(OMerchantOperation.Update _Request)
        {
            #region
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }
                if (_Request.SettelmentDays < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0101);
                }

                int? SettelmentTypeId = HCoreHelper.GetHelperId(_Request.SettelmentTypeCode);

                using (_HCoreContext = new HCoreContext())
                {
                    var _MerchantDetails = _HCoreContext.BNPLMerchant.Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey).FirstOrDefault();
                    var _Settelments = _HCoreContext.BNPLMerchantSettlement.Where(x => x.Merchant.AccountId == _Request.AccountId && x.Merchant.Account.Guid == _Request.AccountKey).ToList();
                    if (_MerchantDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.SettelmentTypeCode))
                        {
                            _MerchantDetails.SettelmentTypeId = (int)SettelmentTypeId;
                        }
                        if (_Request.SettelmentDays != null || _Request.SettelmentDays >= 0)
                        {
                            _MerchantDetails.SettelmentDays = _Request.SettelmentDays * 24;
                        }
                        foreach(var Settelement in _Settelments)
                        {
                            DateTime SettelementDate = (DateTime)Settelement.LoanRedeenDate;
                            Settelement.SettlementDate = SettelementDate.AddDays((double)_MerchantDetails.SettelmentDays/24);
                            Settelement.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Settelement.ModifyByid = _Request.UserReference.AccountId;
                        }

                        _MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _MerchantDetails.ModifiyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var _Response = new
                        {
                            ReferenceId = _MerchantDetails.Id,
                            ReferenceKey = _MerchantDetails.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLUPDATE);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateMerchant", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the merchant status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateMerchantStatus(OMerchantOperation.Update _Request)
        {
            #region
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }
                if (_Request.SettelmentDays < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0101);
                }

                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);

                using (_HCoreContext = new HCoreContext())
                {
                    var _MerchantDetails = _HCoreContext.BNPLMerchant.Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey).FirstOrDefault();
                    if (_MerchantDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.StatusCode))
                        {
                            _MerchantDetails.StatusId = (int)StatusId;
                        }

                        _MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _MerchantDetails.ModifiyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();

                        var _Response = new
                        {
                            ReferenceId = _MerchantDetails.Id,
                            ReferenceKey = _MerchantDetails.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLUPDATE, "Merchant status updated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateMerchantStatus", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the merchant settelement criteria.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateMerchantSettelementCriteria(OMerchantOperation.Update _Request)
        {
            #region
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }

                int? TransactionSourceId = HCoreHelper.GetHelperId(_Request.TransactionSourceCode);

                using (_HCoreContext = new HCoreContext())
                {
                    var _MerchantDetails = _HCoreContext.BNPLMerchant.Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey).FirstOrDefault();
                    if (_MerchantDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.TransactionSourceCode))
                        {
                            _MerchantDetails.TransactionSourceId = (int)TransactionSourceId;
                        }

                        _MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _MerchantDetails.ModifiyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        var _Response = new
                        {
                            ReferenceId = _MerchantDetails.Id,
                            ReferenceKey = _MerchantDetails.Guid
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLUPDATE);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("UpdateMerchantSettelementCriteria", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Deletes the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteMerchant(OReference _Request)
        {
            #region
            try
            {
                var _MerchantDetails = _HCoreContext.BNPLMerchant
                    .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                    .FirstOrDefault();
                if (_MerchantDetails != null)
                {
                    _HCoreContext.BNPLMerchant.Remove(_MerchantDetails);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();

                    var _Response = new
                    {
                        ReferenceId = _BNPLMerchant.Id,
                        ReferenceKey = _BNPLMerchant.Guid
                    };
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResponseCode.BNPLREMOVE);
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("DeleteMerchant", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the BNPL merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBNPLMerchant(OReference _Request)
        {
            #region
            try
            {
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREF);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPLREFK);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var _MerchantDetails = _HCoreContext.BNPLMerchant.Where(x => x.AccountId == _Request.AccountId && x.Account.Guid == _Request.AccountKey)
                        .Select(x => new OMerchantOperation.Details
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            AccountId = x.AccountId,
                            AccountKey = x.Account.Guid,
                            AccountDisplayName = x.Account.DisplayName,

                            AccountTypeId = x.Account.AccountTypeId,
                            AccountTypeCode = x.Account.AccountType.Guid,
                            AccountTypeName = x.Account.AccountType.Name,

                            IsMerchantSettelment = x.IsMerchantSettelment,

                            SettelmentTypeId = x.SettelmentTypeId,
                            SettelmentTypeCode = x.SettelmentType.SystemName,
                            SettelmentTypeName = x.SettelmentType.Name,

                            SettelmentDays = x.SettelmentDays,

                            TransactionSourceId = x.TransactionSourceId,
                            TransactionSourceCode = x.TransactionSource.SystemName,
                            TransactionSourceName = x.TransactionSource.Name,

                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByKey = x.CreatedBy.Guid,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                            ModifyDate = x.ModifyDate,
                            ModifyById = x.ModifiyById,
                            ModifyByKey = x.ModifiyBy.Guid,
                            ModifyByDisplayName = x.ModifiyBy.DisplayName,

                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name,
                        }).FirstOrDefault();
                    _HCoreContext.Dispose();

                    if (_MerchantDetails != null)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _MerchantDetails, ResponseCode.BNPL0200, "Details Loaded Successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetBNPLMerchant", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the merchants.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchants(OList.Request _Request)
        {
            #region
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = _HCoreContext.BNPLMerchant
                            .Select(x => new OMerchantOperation.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountDisplayName = x.Account.DisplayName,

                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,

                                StartDate = x.StartDate,
                                EndDate = x.EndDate,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                //ModifyDate = x.ModifyDate,
                                //ModifyById = x.ModifiyById,
                                //ModifyByDisplayName = x.ModifiyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .Where(_Request.SearchCondition)
                            .Count();

                    }
                    List<OMerchantOperation.List> Data = _HCoreContext.BNPLMerchant
                            .Select(x => new OMerchantOperation.List
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                AccountId = x.AccountId,
                                AccountKey = x.Account.Guid,
                                AccountDisplayName = x.Account.DisplayName,

                                AccountTypeId = x.Account.AccountTypeId,
                                AccountTypeCode = x.Account.AccountType.Guid,
                                AccountTypeName = x.Account.AccountType.Name,

                                StartDate = x.StartDate,
                                EndDate = x.EndDate,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                //ModifyDate = x.ModifyDate,
                                //ModifyById = x.ModifiyById,
                                //ModifyByDisplayName = x.ModifiyBy.DisplayName,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    _HCoreContext.Dispose();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, ResponseCode.BNPL0200);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResponseCode.BNPL0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Manages the settlement.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ManageSettlement(OMerchantOperation.SettelmentRequest _Request)
        {
            try
            {
                if (_Request.MerchantId <= 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPLINMERID", "Invalid merchant id");
                }
                int StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "BNPLINVSTAT", "Invalid status");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.BNPLMerchant.Where(x => x.AccountId == _Request.MerchantId).FirstOrDefault();
                    var Settelments = _HCoreContext.BNPLMerchantSettlement
                                          .Where(a => a.Merchant.AccountId == _Request.MerchantId
                                          && a.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Initialized
                                          || a.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Processing
                                          || a.StatusId == BNPLHelper.StatusHelper.MerchantSetelment.Cancelled
                                          && a.Merchant.IsMerchantSettelment == 1).ToList();

                    if (Settelments.Count > 0)
                    {
                        foreach (var Settelment in Settelments)
                        {
                            Settelment.StatusId = StatusId;
                            Settelment.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Settelment.ModifyByid = Settelment.Merchant.AccountId;
                        }
                        _HCoreContext.SaveChanges();
                    }
                    if (MerchantDetails != null)
                    {
                        MerchantDetails.IsMerchantSettelment = _Request.IsMerchantSettelment;
                        MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        MerchantDetails.ModifiyById = MerchantDetails.AccountId;
                        _HCoreContext.SaveChanges();

                        var _Respose = new
                        {
                            ReferenceId = MerchantDetails.Id,
                            ReferenceKey = MerchantDetails.Guid,
                            StatusId = MerchantDetails.StatusId
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Respose, "BNPLUPDATE", "Settelments cancelled");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "BNPL0404", "No data available");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ManageSettlement", _Exception, _Request.UserReference);
            }
        }
    }
}

