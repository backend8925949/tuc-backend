//==================================================================================
// FileName: ManagePlan.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Loan.Framework;
using HCore.TUC.Plugins.Loan.Object;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HCore.Api.App.v3.Tuc.Plugins.Loan
{
    public class ManagePlan
    {
        FrameworkPlanOperation _FrameworkPlanOpration;

        /// <summary>
        /// Description: Saves the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SavePlan(OPlanOperation.Save.Request _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.SavePlan(_Request);
        }
        /// <summary>
        /// Description: Updates the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdatePlan(OPlanOperation.Update _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.UpdatePlan(_Request);
        }
        /// <summary>
        /// Description: Deletes the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeletePlan(OReference _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.DeletePlan(_Request);
        }
        /// <summary>
        /// Description: Gets the plan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPlan(OReference _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.GetPlan(_Request);
        }
        /// <summary>
        /// Description: Gets the plan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPlans(OList.Request _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.GetPlans(_Request);
        }


        /// <summary>
        /// Description: Saves the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveMerchant(OMerchantOperation.Save.Request _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.SaveMerchant(_Request);
        }
        /// <summary>
        /// Description: Updates the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateMerchant(OMerchantOperation.Update _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.UpdateMerchant(_Request);
        }
        /// <summary>
        /// Description: Updates the merchant status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateMerchantStatus(OMerchantOperation.Update _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.UpdateMerchantStatus(_Request);
        }
        /// <summary>
        /// Description: Updates the merchant settelement criteria.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateMerchantSettelementCriteria(OMerchantOperation.Update _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.UpdateMerchantSettelementCriteria(_Request);
        }
        /// <summary>
        /// Description: Deletes the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteMerchant(OReference _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.DeleteMerchant(_Request);
        }
        /// <summary>
        /// Description: Gets the BNPL merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBNPLMerchant(OReference _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.GetBNPLMerchant(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchants(OList.Request _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.GetMerchants(_Request);
        }
        /// <summary>
        /// Description: Manages the settlement.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ManageSettlement(OMerchantOperation.SettelmentRequest _Request)
        {
            _FrameworkPlanOpration = new FrameworkPlanOperation();
            return _FrameworkPlanOpration.ManageSettlement(_Request);
        }
    }
}
