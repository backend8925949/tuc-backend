//==================================================================================
// FileName: ManageCustomerOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.Loan.Framework;
using HCore.TUC.Plugins.Loan.Object;

namespace HCore.TUC.Plugins.Loan
{
    public class ManageCustomerOperation
    {
        FrameworkCustomerOperation _FrameworkCustomerOperation;
        /// <summary>
        /// Description: Gets the customer configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerConfiguration(OCustomerOperation.Customer.Configuration.Request _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.GetCustomerConfiguration(_Request);
        }
        /// <summary>
        /// Description: Saves the customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCustomer(OCustomerOperation.Customer.Profile.Request _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.SaveCustomer(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchants(OList.Request _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.GetMerchants(_Request);
        }
        /// <summary>
        /// Description: Gets the plan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetPlans(OList.Request _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.GetPlans(_Request);
        }
        /// <summary>
        /// Description: Initializes the loan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        // public OResponse InitializeLoan(OCustomerOperation.Loan.Initialize _Request)
        // {
        //     _FrameworkCustomerOperation = new FrameworkCustomerOperation();
        //     return _FrameworkCustomerOperation.InitializeLoan(_Request);
        // }
        /// <summary>
        /// Description: Gets the loan list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoans(OList.Request _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.GetLoans(_Request);
        }
        /// <summary>
        /// Description: Gets the loan.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetLoan(OReference _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.GetLoan(_Request);
        }
        /// <summary>
        /// Description: Gets the repayment list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRepayments (OList.Request _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.GetRepayments(_Request);
        }
        /// <summary>
        /// Description: Checks the customer configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CheckCustomerConfiguration(OCustomerOperation.Configuration _Request)
        {
            _FrameworkCustomerOperation = new FrameworkCustomerOperation();
            return _FrameworkCustomerOperation.CheckCustomerConfiguration(_Request);
        }
    }
}
