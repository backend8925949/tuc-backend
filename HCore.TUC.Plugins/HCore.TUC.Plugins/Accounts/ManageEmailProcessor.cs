﻿using System;
using Akka.Actor;
using HCore.TUC.Plugins.Accounts.Objects;
using HCore.TUC.Plugins.Accounts.FrameWork;

namespace HCore.TUC.Plugins.Accounts
{
	public class ManageEmailProcessor
	{
		internal class ActorRegistrationEmail : ReceiveActor
		{
			public ActorRegistrationEmail()
			{
				Receive<OEmailProcessor.Registration>(_Request =>
				{
					FrameworkEmailProcessor _FrameworkEmailProcessor = new FrameworkEmailProcessor();
					_FrameworkEmailProcessor.RegistrationEmail(_Request);
                });
			}
		}

        internal class ActorEmailVerification : ReceiveActor
        {
            public ActorEmailVerification()
            {
                Receive<OEmailProcessor.EmailVerification>(_Request =>
                {
                    FrameworkEmailProcessor _FrameworkEmailProcessor = new FrameworkEmailProcessor();
                    _FrameworkEmailProcessor.EmailVerification(_Request);
                });
            }
        }
    }
}

