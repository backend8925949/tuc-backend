﻿using System;
using HCore.Helper;
using HCore.TUC.Plugins.Accounts.FrameWork;
using HCore.TUC.Plugins.Accounts.Objects;

namespace HCore.TUC.Plugins.Accounts
{
	public class ManageStoreManagement
    {
        FrameworkStoreManagement? _FrameworkStoreManagement;

        public async Task<OResponse> SaveStore(OStoreManagement.CreateStore.Request _Request)
        {
            _FrameworkStoreManagement = new FrameworkStoreManagement();
            return await _FrameworkStoreManagement.SaveStore(_Request);
        }

        public async Task<OResponse> UpdateStore(OStoreManagement.UpdateStore.Request _Request)
        {
            _FrameworkStoreManagement = new FrameworkStoreManagement();
            return await _FrameworkStoreManagement.UpdateStore(_Request);
        }

        public async Task<OResponse> UpdateStoreStatus(OReference _Request)
        {
            _FrameworkStoreManagement = new FrameworkStoreManagement();
            return await _FrameworkStoreManagement.UpdateStoreStatus(_Request);
        }

        public async Task<OResponse> DeleteStore(OReference _Request)
        {
            _FrameworkStoreManagement = new FrameworkStoreManagement();
            return await _FrameworkStoreManagement.DeleteStore(_Request);
        }

        public async Task<OResponse> GetStore(OReference _Request)
        {
            _FrameworkStoreManagement = new FrameworkStoreManagement();
            return await _FrameworkStoreManagement.GetStore(_Request);
        }

        public async Task<OResponse> GetStores(OList.Request _Request)
        {
            _FrameworkStoreManagement = new FrameworkStoreManagement();
            return await _FrameworkStoreManagement.GetStores(_Request);
        }
    }
}

