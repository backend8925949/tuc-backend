﻿using System;
using HCore.Helper;
using HCore.TUC.Plugins.Accounts.FrameWork;
using HCore.TUC.Plugins.Accounts.Objects;

namespace HCore.TUC.Plugins.Accounts
{
	public class ManageMerchantOnboarding
	{
		FrameworkMerchantOnboarding? _FrameworkMerchantOnboarding;

		public async Task<OResponse> OnboardMerchantRequest(OAccounts.OnboardingRequest.Request _Request)
		{
			_FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
			return await _FrameworkMerchantOnboarding.OnboardMerchantRequest(_Request);
        }

        public async Task<OResponse> UpdateMerchantDetails(OAccounts.UpdateMerchantDetails.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.UpdateMerchantDetails(_Request);
        }

        public async Task<OResponse> ForgotPassword(OAccounts.ForgotPassword.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.ForgotPassword(_Request);
        }

        public async Task<OResponse> VerifyEmail(OAccounts.VerifyEmailRequest.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.VerifyEmail(_Request);
        }

        public async Task<OResponse> ResendEmailOtp(OAccounts.ResendEmaiLOtpRequest.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.ResendEmailOtp(_Request);
        }

        public async Task<OResponse> UpdatePassword(OAccounts.UpdatePasswordRequest.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.UpdatePassword(_Request);
        }

        public async Task<OResponse> TwoFactorAuthentication(OAccounts.TwoFactorAuthRequest.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.TwoFactorAuthentication(_Request);
        }

        public async Task<OResponse> VerifyTwoFactorAuthentication(OAccounts.VerifyTwoFactorAuthRequest.Request _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.VerifyTwoFactorAuthentication(_Request);
        }

        public async Task<OResponse> GetMerchantDetails(OReference _Request)
        {
            _FrameworkMerchantOnboarding = new FrameworkMerchantOnboarding();
            return await _FrameworkMerchantOnboarding.GetMerchantDetails(_Request);
        }
    }
}

