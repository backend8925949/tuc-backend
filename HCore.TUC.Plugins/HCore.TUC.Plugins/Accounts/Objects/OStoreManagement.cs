﻿using System;
using HCore.Helper;
using static HCore.TUC.Plugins.Accounts.Objects.OStoreManagement.CreateStore;

namespace HCore.TUC.Plugins.Accounts.Objects
{
    public class OStoreManagement
    {
        public class CreateStore
        {
            public class Request
            {
                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreManagerMobileNumber { get; set; }
                public string? StoreEmailAddress { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public long? CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public string? CityAreaName { get; set; }
                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }
                public string? Address { get; set; }
                public string? MapAddress { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public string? PostalCode { get; set; }
                public StoreBankDetails? BankDetails { get; set; }
                public List<StoreCategoryDetails>? CategoryDetails { get; set; }
                public OStorageContent? IconContent { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class StoreBankDetails
            {
                public string? AccountNumber { get; set; }
                public string? AccountName { get; set; }
                public string? BankName { get; set; }
            }

            public class StoreCategoryDetails
            {
                public long? CategoryId { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class UpdateStore
        {
            public class Request
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public long? AddressId { get; set; }
                public string? AddressKey { get; set; }
                public string? StoreDisplayName { get; set; }
                public string? StoreEmailAddress { get; set; }
                public string? StoreManagerMobileNumber { get; set; }
                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public long? CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public string? CityAreaName { get; set; }
                public string? Address { get; set; }
                public string? MapAddress { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public string? PostalCode { get; set; }
                public StoreBankDetails? BankDetails { get; set; }
                public List<StoreCategoryDetails>? CategoryDetails { get; set; }
                public OStorageContent? IconContent { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class StoreBankDetails
            {
                public string? AccountNumber { get; set; }
                public string? AccountName { get; set; }
                public string? BankName { get; set; }
            }

            public class StoreCategoryDetails
            {
                public long? CategoryId { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class StoreList
        {
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? DisplayName { get; set; }

                public string? ContactNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? Address { get; set; }

                public string? CatagoryName { get; set; }

                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }

                public DateTime? CreateDate { get; set; }

                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
                public List<StoreCategories>? Categories { get; set; }
            }

            public class StoreCategories
            {
                public long? CategoryId { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
            }
        }

        public class StoreDetails
        {
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? DisplayName { get; set; }
                public string? Name { get; set; }
                public string? ContactNumber { get; set; }
                public string? MobileNumber { get; set; }
                public string? EmailAddress { get; set; }

                public string? IconUrl { get; set; }

                public long? MerchantId { get; set; }
                public string? MerchantKey { get; set; }

                public int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public StoreBankDetails? BankDetails { get; set; }
                public StoreAddressDetails? AddressDetails { get; set; }
                public List<StoreCategoryDetails>? CategoryDetails { get; set; }
            }

            public class StoreBankDetails
            {
                public string? AccountNumber { get; set; }
                public string? AccountName { get; set; }
                public string? BankName { get; set; }
            }

            public class StoreCategoryDetails
            {
                public long? CategoryId { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
            }

            public class StoreAddressDetails
            {
                public long? AddressId { get; set; }
                public string? AddressKey { get; set; }
                public long? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? CountryName { get; set; }
                public long? StateId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public long? CityAreaId { get; set; }
                public string? CityAreaKey { get; set; }
                public string? CityAreaName { get; set; }
                public string? Address { get; set; }
                public string? MapAddress { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
                public string? PostalCode { get; set; }
            }
        }
    }
}

