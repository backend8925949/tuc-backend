﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Accounts.Objects
{
	public class OEmailProcessor
	{
		public class Registration
        {
            public string? EmailAddress { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? PanelUrl { get; set; }
            public OUserReference? UserReference { get; set; }
        }

		public class EmailVerification
		{
			public string? EmailAddress { get; set; }
			public string? Code { get; set; }
			public string? UserDisplayName { get; set; }
			public OUserReference? UserReference { get; set; }
        }
	}
}

