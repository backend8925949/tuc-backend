﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.Accounts.Objects
{
	public class OAccounts
	{
		public class OnboardingRequest
		{
			public class Request
			{
				public string? BusinessName { get; set; }
				public string? FirstName { get; set; }
				public string? LastName { get; set; }
				public string? BusinessTypeCode { get; set; }
				public string? EmailAddress { get; set; }
				public string? MobileNumber { get; set; }
				public string? Password { get; set; }
				public string? CountryIsd { get; set; }
				public OUserReference? UserReference { get; set; }
			}
		}

		public class CreateAccountReuqest
		{
            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

		public class UpdateMerchantDetails
		{
			public class Request
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
				public long? StateId { get; set; }
				public string? StateKey { get; set; }
				public string? StateName { get; set; }
				public long? CityId { get; set; }
				public string? CityKey { get; set; }
				public string? CityName { get; set; }
				public string? Address { get; set; }
				public string? MapAddress { get; set; }
				public double? Latitude { get; set; }
				public double? Longitude { get; set; }
				public string? PostalCode { get; set; }
				public bool IsBusinessRegistered { get; set; }
				public string? ReferralCode { get; set; }
				public OStorageContent? ImageContent { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

		public class ForgotPassword
		{
			public class Request
            {
                public string? EmailAddress { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
            }
        }

		public class VerifyEmailRequest
		{
			public class Request
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
                public string? VerificationCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
            }
        }

		public class UpdatePasswordRequest
        {
            public class Request
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
                public string? NewPassword { get; set; }
                public string? ConfirmedPassword { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class ResendEmaiLOtpRequest
        {
            public class Request
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
            }
        }

        public class TwoFactorAuthRequest
        {
            public class Request
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
            }
        }

        public class VerifyTwoFactorAuthRequest
        {
            public class Request
            {
                public string? EmailAddress { get; set; }
                public string? Reference { get; set; }
                public string? VerificationCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }

        public class MerchantDetails
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DisplayName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public string? ImageUrl { get; set; }
            public long? TotalProductDeals { get; set; }
            public long? TotalServiceDeals { get; set; }
            public long? TotalSoldDeals { get; set; }
            public long? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
        }
    }
}

