﻿using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.TUC.Plugins.Accounts.Objects;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.TUC.Plugins.Accounts.Objects.OStoreManagement.StoreDetails;
using Microsoft.IdentityModel.Tokens;

namespace HCore.TUC.Plugins.Accounts.FrameWork
{
	public class FrameworkStoreManagement
    {
        HCoreContext? _HCoreContext;
        HCUAccountAuth? _HCUAccountAuth;
        HCUAccount? _HCUAccount;
        HCCoreAddress? _HCCoreAddress;
        List<TUCCategoryAccount>? _TUCCategoryAccount;
        HCUAccountBank? _HCUAccountBank;
        Random? _Random;
        List<OStoreManagement.StoreList.Response>? _StoreList;
        OStoreManagement.StoreDetails.Response? _StoreDetails;
        OStoreManagement.StoreDetails.StoreBankDetails? _StoreBankDetails;
        List<OStoreManagement.StoreDetails.StoreCategoryDetails>? _StoreCategories;
        OStoreManagement.StoreDetails.StoreAddressDetails? _StoreAddressDetails;
        List<OStoreManagement.StoreList.StoreCategories>? _Categories;

        internal async Task<OResponse> SaveStore(OStoreManagement.CreateStore.Request _Request)
        {
            try
            {
                if (_Request.MerchantId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0001", "Merchant id required");
                }
                if (string.IsNullOrEmpty(_Request.MerchantKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0002", "Merchant key required");
                }
                if (string.IsNullOrEmpty(_Request.StoreDisplayName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0003", "Store display name required");
                }
                if (string.IsNullOrEmpty(_Request.StoreManagerMobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0004", "Store manager phone number required");
                }
                
                if (_Request.CategoryDetails == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0008", "Select category");
                }
                if (_Request.StateId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0009", "State id required");
                }
                if (string.IsNullOrEmpty(_Request.StateKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0010", "State key required");
                }
                if (_Request.CityId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0011", "City id required");
                }
                if (string.IsNullOrEmpty(_Request.CityKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0012", "City key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var StoreDetails = await _HCoreContext.HCUAccount.Where(x => x.OwnerId == _Request.MerchantId && x.Owner.Guid == _Request.MerchantKey && x.DisplayName == _Request.StoreDisplayName && x.AccountTypeId == UserAccountType.MerchantStore).FirstOrDefaultAsync();
                    if (StoreDetails != null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0013", "Store display name already used");
                    }

                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.MerchantId && x.Guid == _Request.MerchantKey && x.AccountTypeId == UserAccountType.Merchant)
                                          .Select(x => new
                                          {
                                              x.Id,
                                              x.CountryId,
                                              x.Country.TimeZoneName,
                                              x.Country.MobileNumberLength,
                                              x.Country.Isd,
                                              x.EmailAddress
                                          }).FirstOrDefaultAsync();
                    if(MerchantDetails != null)
                    {
                        _Random = new Random();
                        string? ReferralCode = HCoreHelper.GenerateSystemName(_Request.StoreDisplayName);

                        if (!string.IsNullOrEmpty(_Request.StoreManagerMobileNumber))
                        {
                            _Request.StoreManagerMobileNumber = HCoreHelper.FormatMobileNumber(MerchantDetails.Isd, _Request.StoreManagerMobileNumber, MerchantDetails.MobileNumberLength);
                        }

                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), MerchantDetails.TimeZoneName);
                        _HCUAccountAuth.CreatedById = MerchantDetails.Id;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.HCUAccountAuth.AddAsync(_HCUAccountAuth);

                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccount.OwnerId = MerchantDetails.Id;
                        _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                        _HCUAccount.Name = _Request.StoreDisplayName;
                        _HCUAccount.DisplayName = _Request.StoreDisplayName;
                        _HCUAccount.MobileNumber = _Request.StoreManagerMobileNumber;
                        _HCUAccount.ContactNumber = _Request.StoreManagerMobileNumber;
                        if (!string.IsNullOrEmpty(_Request.StoreEmailAddress))
                        {
                            _HCUAccount.EmailAddress = _Request.StoreEmailAddress;
                        }
                        else
                        {
                            _HCUAccount.EmailAddress = MerchantDetails.EmailAddress;
                        }

                        if (_Request.CategoryDetails != null && _Request.CategoryDetails.Count > 0)
                        {
                            _TUCCategoryAccount = new List<TUCCategoryAccount>();
                            foreach (var Category in _Request.CategoryDetails)
                            {
                                _TUCCategoryAccount.Add(new TUCCategoryAccount
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    TypeId = 1,
                                    CategoryId = (int)Category.CategoryId,
                                    CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), MerchantDetails.TimeZoneName),
                                    CreatedById = MerchantDetails.Id,
                                    StatusId = HelperStatus.Default.Active,
                                });
                                _HCUAccount.TUCCategoryAccountAccount = _TUCCategoryAccount;
                            }
                        }

                        _HCUAccount.CountryId = MerchantDetails.CountryId;
                        _HCUAccount.StateId = _Request.StateId;
                        _HCUAccount.CityId = _Request.CityId;
                        _HCUAccount.Address = _Request.Address;
                        _HCUAccount.ReferralUrl = _AppConfig.WebsiteUrl + "?refby={ReferralCode}";
                        _HCUAccount.ReferralCode = ReferralCode;
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                        _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                        _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                        _HCUAccount.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), MerchantDetails.TimeZoneName);
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.EmailVerificationStatus = 2;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), MerchantDetails.TimeZoneName);
                        _HCUAccount.NumberVerificationStatus = 2;
                        _HCUAccount.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), MerchantDetails.TimeZoneName);
                        _HCUAccount.DocumentVerificationStatus = 2;
                        _HCUAccount.DocumentVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), MerchantDetails.TimeZoneName);
                        _HCUAccount.User = _HCUAccountAuth;
                        _HCUAccount.PrimaryCategoryId = (int?)_Request.CategoryDetails.FirstOrDefault().CategoryId;
                        await _HCoreContext.HCUAccount.AddAsync(_HCUAccount);

                        _HCCoreAddress = new HCCoreAddress();
                        _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreAddress.Account = _HCUAccount;
                        _HCCoreAddress.DisplayName = _Request.StoreDisplayName;
                        _HCCoreAddress.Name = _Request.StoreDisplayName;
                        _HCCoreAddress.ContactNumber = _Request.StoreManagerMobileNumber;
                        _HCCoreAddress.AdditionalContactNumber = _Request.StoreManagerMobileNumber;
                        _HCCoreAddress.EmailAddress = MerchantDetails.EmailAddress;
                        _HCCoreAddress.IsPrimaryAddress = 1;
                        _HCCoreAddress.LocationTypeId = 792;
                        _HCCoreAddress.Latitude = (double)_Request.Latitude;
                        _HCCoreAddress.Longitude = (double)_Request.Longitude;
                        _HCCoreAddress.CountryId = (int)MerchantDetails.CountryId;
                        _HCCoreAddress.StateId = _Request.StateId;
                        _HCCoreAddress.CityId = _Request.CityId;
                        _HCCoreAddress.CityAreaId = _Request.CityAreaId;
                        _HCCoreAddress.AddressLine1 = _Request.Address;
                        _HCCoreAddress.MapAddress = _Request.Address;
                        _HCCoreAddress.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), MerchantDetails.TimeZoneName);
                        _HCCoreAddress.CreatedBy = _HCUAccount;
                        _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.HCCoreAddress.AddAsync(_HCCoreAddress);

                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        long? AddressId = _HCCoreAddress.Id;
                        long? IconStorageId = 0;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId > 0 && IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var AccountDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == _HCUAccount.Guid).FirstOrDefaultAsync();
                                if (AccountDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        AccountDetails.AddressId = AddressId;
                                        AccountDetails.IconStorageId = IconStorageId;
                                    }
                                    await _HCoreContext.SaveChangesAsync();
                                    await _HCoreContext.DisposeAsync();
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                }
                            }
                        }

                        OStoreManagement.CreateStore.Response _Response = new OStoreManagement.CreateStore.Response();
                        _Response.ReferenceId = _HCUAccount.Id;
                        _Response.ReferenceKey = _HCUAccount.Guid;

                        return HCoreHelper.SendResponse(null, ResponseStatus.Success, _Response, "OS0014", "Store created successfully.");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OS0015", "Invalid merchant selected.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveStore", _Exception, _Request.UserReference, "OS0500", _Exception.Message);
            }
        }

        internal async Task<OResponse> UpdateStore(OStoreManagement.UpdateStore.Request _Request)
        {
            try
            {
                if(_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0014", "Reference id required");
                }
                if(string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0015", "Reference key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var StoreDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountTypeId == UserAccountType.MerchantStore).FirstOrDefaultAsync();
                    if(StoreDetails != null)
                    {
                        string? TimeZoneId = await _HCoreContext.HCCoreCountry.Where(x => x.Id == StoreDetails.CountryId).Select(x => x.TimeZoneName).FirstOrDefaultAsync();

                        if(!string.IsNullOrEmpty(_Request.StoreDisplayName) && StoreDetails.DisplayName != _Request.StoreDisplayName)
                        {
                            bool Exists = await _HCoreContext.HCUAccount.AnyAsync(x => x.DisplayName == _Request.StoreDisplayName || x.Name == _Request.StoreDisplayName);
                            if(Exists)
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0013", "Store display name already used");
                            }
                        }

                        if(!string.IsNullOrEmpty(_Request.StoreDisplayName) && StoreDetails.DisplayName != _Request.StoreDisplayName)
                        {
                            StoreDetails.DisplayName = _Request.StoreDisplayName;
                        }
                        if(!string.IsNullOrEmpty(_Request.StoreDisplayName) && StoreDetails.Name != _Request.StoreDisplayName)
                        {
                            StoreDetails.Name = _Request.StoreDisplayName;
                        }
                        if(!string.IsNullOrEmpty(_Request.StoreEmailAddress) && StoreDetails.EmailAddress != _Request.StoreEmailAddress)
                        {
                            StoreDetails.EmailAddress = _Request.StoreEmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.StoreManagerMobileNumber) && StoreDetails.MobileNumber != _Request.StoreManagerMobileNumber)
                        {
                            StoreDetails.MobileNumber = _Request.StoreManagerMobileNumber;
                            StoreDetails.ContactNumber = _Request.StoreManagerMobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Address) && StoreDetails.Address != _Request.Address)
                        {
                            StoreDetails.Address = _Request.Address;
                        }
                        if (_Request.CountryId > 0 && StoreDetails.CountryId != _Request.CountryId)
                        {
                            StoreDetails.CountryId = (int?)_Request.CountryId;
                        }
                        if (_Request.StateId > 0 && StoreDetails.StateId != _Request.StateId)
                        {
                            StoreDetails.StateId = _Request.StateId;
                        }
                        if (_Request.CityId > 0 && StoreDetails.StateId != _Request.StateId)
                        {
                            StoreDetails.CityId = _Request.CityId;
                        }
                        if (_Request.CityAreaId > 0 && StoreDetails.CityAreaId != _Request.CityAreaId)
                        {
                            StoreDetails.CityAreaId = _Request.CityAreaId;
                        }
                        if(_Request.BankDetails != null)
                        {
                            var StoreBankDetails = await _HCoreContext.HCUAccountBank.Where(x => x.AccountId == StoreDetails.Id).FirstOrDefaultAsync();
                            if(StoreBankDetails != null)
                            {
                                if (!string.IsNullOrEmpty(_Request.BankDetails.AccountNumber) && StoreBankDetails.AccountNumber != _Request.BankDetails.AccountNumber)
                                {
                                    StoreBankDetails.AccountNumber = _Request.BankDetails.AccountNumber;
                                }
                                if (!string.IsNullOrEmpty(_Request.BankDetails.AccountName) && StoreBankDetails.Name != _Request.BankDetails.AccountName)
                                {
                                    StoreBankDetails.Name = _Request.BankDetails.AccountName;
                                }
                                if (!string.IsNullOrEmpty(_Request.BankDetails.BankName) && StoreBankDetails.BankName != _Request.BankDetails.BankName)
                                {
                                    StoreBankDetails.BankName = _Request.BankDetails.BankName;
                                }

                                StoreBankDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                                StoreBankDetails.ModifyById = _Request.UserReference.AccountId;
                            }
                        }
                        if(_Request.CategoryDetails.Count > 0)
                        {
                            _TUCCategoryAccount = new List<TUCCategoryAccount>();
                            var StoreCategories = await _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == StoreDetails.Id).ToListAsync();
                            if(StoreCategories.Count > 0)
                            {
                                _HCoreContext.RemoveRange(StoreCategories);
                            }
                            _TUCCategoryAccount = new List<TUCCategoryAccount>();
                            foreach (var Category in _Request.CategoryDetails)
                            {
                                _TUCCategoryAccount.Add(new TUCCategoryAccount
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    TypeId = 1,
                                    CategoryId = (int)Category.CategoryId,
                                    CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId),
                                    CreatedById = StoreDetails.Id,
                                    StatusId = HelperStatus.Default.Active,
                                });
                                StoreDetails.TUCCategoryAccountAccount = _TUCCategoryAccount;
                            }

                            StoreDetails.PrimaryCategoryId = (int?)_Request.CategoryDetails.FirstOrDefault().CategoryId;
                        }

                        var StoreAddressDetails = await _HCoreContext.HCCoreAddress.Where(x => x.AccountId == StoreDetails.Id && x.Id == _Request.AddressId).FirstOrDefaultAsync();
                        if(StoreAddressDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.StoreDisplayName) && StoreAddressDetails.DisplayName != _Request.StoreDisplayName)
                            {
                                StoreAddressDetails.DisplayName = _Request.StoreDisplayName;
                            }
                            if (!string.IsNullOrEmpty(_Request.StoreDisplayName) && StoreAddressDetails.Name != _Request.StoreDisplayName)
                            {
                                StoreAddressDetails.Name = _Request.StoreDisplayName;
                            }
                            if (!string.IsNullOrEmpty(_Request.StoreEmailAddress) && StoreAddressDetails.EmailAddress != _Request.StoreEmailAddress)
                            {
                                StoreAddressDetails.EmailAddress = _Request.StoreEmailAddress;
                            }
                            if (!string.IsNullOrEmpty(_Request.StoreManagerMobileNumber) && StoreAddressDetails.ContactNumber != _Request.StoreEmailAddress)
                            {
                                StoreAddressDetails.ContactNumber = _Request.StoreManagerMobileNumber;
                            }
                            if (!string.IsNullOrEmpty(_Request.StoreManagerMobileNumber) && StoreAddressDetails.AdditionalContactNumber != _Request.StoreManagerMobileNumber)
                            {
                                StoreAddressDetails.AdditionalContactNumber = _Request.StoreManagerMobileNumber;
                            }
                            if (!string.IsNullOrEmpty(_Request.Address) && StoreAddressDetails.AddressLine1 != _Request.Address)
                            {
                                StoreAddressDetails.AddressLine1 = _Request.Address;
                            }
                            if (_Request.CountryId > 0 && StoreAddressDetails.CountryId != _Request.CountryId)
                            {
                                StoreAddressDetails.CountryId = (int)_Request.CountryId;
                            }
                            if (_Request.StateId > 0 && StoreAddressDetails.StateId != _Request.StateId)
                            {
                                StoreAddressDetails.StateId = _Request.StateId;
                            }
                            if (_Request.CityId > 0 && StoreAddressDetails.CityId != _Request.CityId)
                            {
                                StoreAddressDetails.CityId = _Request.CityId;
                            }
                            if (_Request.CityAreaId > 0 && StoreAddressDetails.CityAreaId != _Request.CityAreaId)
                            {
                                StoreAddressDetails.CityAreaId = _Request.CityAreaId;
                            }

                            StoreAddressDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            StoreAddressDetails.ModifyById = _Request.UserReference.AccountId;
                        }

                        StoreDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                        StoreDetails.ModifyById = _Request.UserReference.AccountId;

                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        long? IconStorageId = 0;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, StoreDetails.IconStorageId, _Request.UserReference);
                        }
                        if (IconStorageId > 0 && IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var AccountDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == StoreDetails.Guid).FirstOrDefaultAsync();
                                if (AccountDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        AccountDetails.IconStorageId = IconStorageId;
                                    }
                                    await _HCoreContext.SaveChangesAsync();
                                    await _HCoreContext.DisposeAsync();
                                }
                                else
                                {
                                    await _HCoreContext.DisposeAsync();
                                }
                            }
                        }

                        OStoreManagement.UpdateStore.Response? _Response = new OStoreManagement.UpdateStore.Response();
                        _Response.ReferenceId = StoreDetails.Id;
                        _Response.ReferenceKey = StoreDetails.Guid;

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OS0016", "Store details updated successfully");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0404", "Store details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateStore", _Exception, _Request.UserReference, "OS0500", _Exception.Message);
            }
        }

        internal async Task<OResponse> GetStores(OList.Request _Request)
        {
            try
            {
                _StoreList = new List<OStoreManagement.StoreList.Response>();
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if(string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "DisplayName", "desc");
                }

                using(_HCoreContext = new HCoreContext())
                {
                    if(_Request.RefreshCount)
                    {
                        _Request.TotalRecords = await _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                            .Select(x => new OStoreManagement.StoreList.Response
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                DisplayName = x.DisplayName,
                                ContactNumber = x.MobileNumber,
                                EmailAddress = x.EmailAddress,
                                Address = x.Address,
                                MerchantId = x.OwnerId,
                                MerchantKey = x.Owner.Guid,
                                CreateDate = x.CreateDate,
                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            }).Where(_Request.SearchCondition)
                            .CountAsync();
                    }

                    _StoreList = await _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => new OStoreManagement.StoreList.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            ContactNumber = x.MobileNumber,
                            EmailAddress = x.EmailAddress,
                            Address = x.Address,
                            MerchantId = x.OwnerId,
                            MerchantKey = x.Owner.Guid,
                            CreateDate = x.CreateDate,
                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        }).Where(_Request.SearchCondition)
                        .OrderBy(_Request.SortExpression)
                        .Skip(_Request.Offset)
                        .Take(_Request.Limit)
                        .ToListAsync();
                    foreach(var Store in _StoreList)
                    {
                        _Categories = new List<OStoreManagement.StoreList.StoreCategories>();
                        _Categories = await _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == Store.ReferenceId)
                            .Select(x => new OStoreManagement.StoreList.StoreCategories
                            {
                                CategoryId = x.CategoryId,
                                CategoryKey = x.Category.Guid,
                                CategoryName = x.Category.Name
                            }).ToListAsync();
                        Store.Categories = _Categories;
                    }
                    await _HCoreContext.DisposeAsync();
                    OList.Response _Response = HCoreHelper.GetListResponse(_Request.TotalRecords, _StoreList, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OS0200", "Stores loaded successfully");
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetStores", _Exception, _Request.UserReference, "OS0500", _Exception.Message);
            }
        }

        internal async Task<OResponse> GetStore(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0017", "Reference id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0018", "Reference key required");
                }

                using(_HCoreContext = new HCoreContext())
                {
                    _StoreDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountTypeId == UserAccountType.MerchantStore)
                        .Select(x => new OStoreManagement.StoreDetails.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            DisplayName = x.DisplayName,
                            MobileNumber = x.MobileNumber,
                            ContactNumber = x.ContactNumber,
                            EmailAddress = x.EmailAddress,
                            IconUrl = x.IconStorage.Path,
                            MerchantId = x.OwnerId,
                            MerchantKey = x.Owner.Guid,
                            StatusId = x.StatusId,
                            StatusCode = x.Status.SystemName,
                            StatusName = x.Status.Name
                        }).FirstOrDefaultAsync();
                    if(_StoreDetails != null)
                    {
                        if(!string.IsNullOrEmpty(_StoreDetails.IconUrl))
                        {
                            _StoreDetails.IconUrl = _AppConfig.StorageUrl + _StoreDetails.IconUrl;
                        }
                        else
                        {
                            _StoreDetails.IconUrl = _AppConfig.Default_Icon;
                        }

                        _StoreBankDetails = await _HCoreContext.HCUAccountBank.Where(x => x.AccountId == _StoreDetails.ReferenceId)
                            .Select(x => new OStoreManagement.StoreDetails.StoreBankDetails
                            {
                                AccountNumber = x.AccountNumber,
                                AccountName = x.Name,
                                BankName = x.BankName,
                            }).FirstOrDefaultAsync();
                        _StoreDetails.BankDetails = _StoreBankDetails;

                        _StoreCategories = await _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == _StoreDetails.ReferenceId)
                            .Select(x => new OStoreManagement.StoreDetails.StoreCategoryDetails
                            {
                                CategoryId = x.CategoryId,
                                CategoryKey = x.Category.Guid,
                                CategoryName = x.Category.Name
                            }).ToListAsync();
                        _StoreDetails.CategoryDetails = _StoreCategories;

                        _StoreAddressDetails = await _HCoreContext.HCCoreAddress.Where(x => x.AccountId == _StoreDetails.ReferenceId)
                            .Select(x => new OStoreManagement.StoreDetails.StoreAddressDetails
                            {
                                AddressId = x.Id,
                                AddressKey = x.Guid,
                                Address = x.AddressLine1,
                                CountryId = x.CountryId,
                                CountryKey = x.Country.Guid,
                                CountryName = x.Country.Name,
                                StateId = x.StateId,
                                StateKey = x.State.Guid,
                                StateName = x.State.Name,
                                CityId = x.CityId,
                                CityKey = x.City.Guid,
                                CityName = x.City.Name,
                                CityAreaId = x.CityAreaId,
                                CityAreaKey = x.CityArea.Guid,
                                CityAreaName = x.CityArea.Name,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                MapAddress = x.MapAddress,
                            }).FirstOrDefaultAsync();
                        _StoreDetails.AddressDetails = _StoreAddressDetails;

                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _StoreDetails, "OS0200", "Store details loaded successfully");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0404", "Store details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetStore", _Exception, _Request.UserReference, "OS0500", _Exception.Message);
            }
        }

        internal async Task<OResponse> UpdateStoreStatus(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0019", "Reference id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0020", "Reference key required");
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0021", "Status code required");
                }

                int? StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                if(StatusId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0022", "Invalid status");
                }

                using(_HCoreContext = new HCoreContext())
                {
                    var StoreDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountTypeId == UserAccountType.MerchantStore).FirstOrDefaultAsync();
                    if(StoreDetails != null)
                    {
                        string? TimeZoneId = await _HCoreContext.HCCoreCountry.Where(x => x.Id == StoreDetails.CountryId).Select(x => x.TimeZoneName).FirstOrDefaultAsync();

                        StoreDetails.StatusId = (int)StatusId;
                        StoreDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                        StoreDetails.ModifyById = _Request.UserReference.AccountId;
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "OS0023", "Store status updated successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0404", "Store details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateStoreStatus", _Exception, _Request.UserReference, "OS0500", _Exception.Message);
            }
        }

        internal async Task<OResponse> DeleteStore(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0024", "Reference id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0025", "Reference key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var StoreDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.AccountTypeId == UserAccountType.MerchantStore).FirstOrDefaultAsync();
                    if (StoreDetails != null)
                    {
                        var StoreAddressDetails = await _HCoreContext.HCCoreAddress.Where(x => x.AccountId == StoreDetails.Id).FirstOrDefaultAsync();
                        if(StoreAddressDetails != null)
                        {
                            _HCoreContext.HCCoreAddress.Remove(StoreAddressDetails);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        var StoreCategories = await _HCoreContext.TUCCategoryAccount.Where(x => x.AccountId == StoreDetails.Id).ToListAsync();
                        if(StoreCategories.Count > 0)
                        {
                            _HCoreContext.TUCCategoryAccount.RemoveRange(StoreCategories);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        var StoreBankDetails = await _HCoreContext.HCUAccountBank.Where(x => x.AccountId ==  StoreDetails.Id).FirstOrDefaultAsync();
                        if(StoreBankDetails != null)
                        {
                            _HCoreContext.HCUAccountBank.Remove(StoreBankDetails);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        var DealLocations = await _HCoreContext.MDDealLocation.Where(x => x.LocationId == StoreDetails.Id).ToListAsync();
                        if (DealLocations.Count > 0)
                        {
                            _HCoreContext.MDDealLocation.RemoveRange(DealLocations);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        var BranchAccounts = await _HCoreContext.TUCBranchAccount.Where(x => x.StoreId == StoreDetails.Id).ToListAsync();
                        if (BranchAccounts.Count > 0)
                        {
                            _HCoreContext.TUCBranchAccount.RemoveRange(BranchAccounts);
                            await _HCoreContext.SaveChangesAsync();
                        }

                        _HCoreContext.HCUAccount.Remove(StoreDetails);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "OS0026", "Store details removed successfully");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OS0404", "Store details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("DeleteStore", _Exception, _Request.UserReference, "OS0500", _Exception.Message);
            }
        }
    }
}

