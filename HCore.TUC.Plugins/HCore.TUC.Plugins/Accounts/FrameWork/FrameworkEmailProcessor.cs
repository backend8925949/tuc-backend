﻿using System;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Plugins.Accounts.Objects;

namespace HCore.TUC.Plugins.Accounts.FrameWork
{
	public class FrameworkEmailProcessor
	{
		internal void RegistrationEmail(OEmailProcessor.Registration _Request)
		{
			try
            {
                if (!string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail(TemplateId.VerificationId, "TUC Merchant", _Request.EmailAddress, _Request, _Request.UserReference);
                    HCoreHelper.BroadCastEmail(TemplateId.RegistrationId, "TUC Merchant", _Request.EmailAddress, _Request, _Request.UserReference);
                }
            }
			catch (Exception _Exception)
			{
				HCoreHelper.LogException("RegistrationEmail", _Exception, null);
			}
        }

        internal void EmailVerification(OEmailProcessor.EmailVerification _Request)
        {
            try
            {   
                if(!string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    HCoreHelper.BroadCastEmail(NotificationTemplates.MerchantOnboardingEmailVerificationCode, "TUC Merchant", _Request.EmailAddress, _Request, _Request.UserReference);
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("RegistrationEmail", _Exception, null);
            }
        }

        private class TemplateId
        {
            public static string RegistrationId = "d-d114d293a57f4009bb44c60c784504a6";
            public static string VerificationId = "d-1ad2e6ec52c24605b203c85eff9bde98";
        }
    }
}

