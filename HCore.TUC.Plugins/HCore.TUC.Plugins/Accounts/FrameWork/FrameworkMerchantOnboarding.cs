﻿using System;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Accounts.Objects;
using Microsoft.EntityFrameworkCore;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Plugins.Accounts.ManageEmailProcessor;
using static HCore.TUC.Plugins.Deals.Framework.FrameworkDealOperations;
using static HCore.TUC.Plugins.Deals.PromoCode.Object.OPromoCode.PromoCodeList;

namespace HCore.TUC.Plugins.Accounts.FrameWork
{
    public class FrameworkMerchantOnboarding
    {
        HCoreContext? _HCoreContext;
        HCoreContextOperations? _HCoreContextOperations;
        HCTMerchantOnboarding? _HCTMerchantOnboarding;
        Random? _Random;
        List<HCUAccountParameter>? _HCUAccountParameters;
        HCUAccountParameter? _HCUAccountParameter;
        HCUAccountAuth? _HCUAccountAuth;
        HCUAccount? _HCUAccount;
        HCCoreAddress? _HCCoreAddress;
        List<TUCCategoryAccount>? _TUCCategoryAccount;
        HCUAccountBank? _HCUAccountBank;
        MDDealAddress? _MDDealAddress;

        internal async Task<OResponse> OnboardMerchantRequest(OAccounts.OnboardingRequest.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.BusinessName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB001", "Business name required.");
                }
                if (string.IsNullOrEmpty(_Request.FirstName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB002", "First name required.");
                }
                if (string.IsNullOrEmpty(_Request.LastName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB003", "Last name required.");
                }
                if (string.IsNullOrEmpty(_Request.BusinessTypeCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB004", "Business type required.");
                }
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB005", "Email address required.");
                }
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB006", "Mobile number required.");
                }
                if (string.IsNullOrEmpty(_Request.Password))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB007", "Password required.");
                }
                if (string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB008", "Country ISD required.");
                }
                if (_Request.Password.Length < 8)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB009", "Password must be 8 characters long.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var ExistingUserDetails = await _HCoreContext.HCUAccount.Where(x => x.EmailAddress == _Request.EmailAddress && x.AccountTypeId == UserAccountType.Merchant).FirstOrDefaultAsync();
                    if (ExistingUserDetails != null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB010", "User already exists with this email address.");
                    }
                    var CountryDetails = await _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.CountryIsd)
                        .Select(x => new
                        {
                            x.Id,
                            x.MobileNumberLength,
                            x.Isd,
                            x.StatusId,
                            x.TimeZoneName
                        })
                        .FirstOrDefaultAsync();
                    if (CountryDetails == null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB011", "Invalid country code");
                    }

                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, _Request.MobileNumber, CountryDetails.MobileNumberLength);
                    }

                    await _HCoreContext.DisposeAsync();

                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        var CheckOnboarding = await _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.EmailAddress).FirstOrDefaultAsync();
                        if (CheckOnboarding != null)
                        {
                            _HCoreContextOperations.HCTMerchantOnboarding.Remove(CheckOnboarding);
                            await _HCoreContextOperations.SaveChangesAsync();
                        }
                        await _HCoreContextOperations.DisposeAsync();
                    }

                    using (_HCoreContextOperations = new HCoreContextOperations())
                    {
                        var CheckOnboarding = await _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.EmailAddress == _Request.EmailAddress).FirstOrDefaultAsync();
                        if (CheckOnboarding != null)
                        {
                            await _HCoreContextOperations.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB013", "Operation failed. Please contact support");
                        }
                        var BusinessNameCheck = await _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.DisplayName == _Request.BusinessName).FirstOrDefaultAsync();
                        if (BusinessNameCheck != null)
                        {
                            await _HCoreContextOperations.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB014", "Business Name address already used");
                        }
                        _HCTMerchantOnboarding = new HCTMerchantOnboarding();
                        _HCTMerchantOnboarding.Guid = HCoreHelper.GenerateGuid();
                        _HCTMerchantOnboarding.DisplayName = _Request.BusinessName;
                        _HCTMerchantOnboarding.EmailAddress = _Request.EmailAddress;
                        _HCTMerchantOnboarding.MobileNumber = _Request.MobileNumber;
                        _HCTMerchantOnboarding.Name = _Request.FirstName + " " + _Request.LastName;
                        _HCTMerchantOnboarding.CountryIsd = CountryDetails.Isd;
                        _HCTMerchantOnboarding.CountryId = CountryDetails.Id;
                        _HCTMerchantOnboarding.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                        _HCTMerchantOnboarding.IsEmailVerified = 2;
                        _HCTMerchantOnboarding.IsMobileVerified = 2;
                        _HCTMerchantOnboarding.Categories = _Request.BusinessTypeCode;
                        _HCTMerchantOnboarding.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), CountryDetails.TimeZoneName);
                        _HCTMerchantOnboarding.StatusId = HelperStatus.Default.Inactive;
                        await _HCoreContextOperations.HCTMerchantOnboarding.AddAsync(_HCTMerchantOnboarding);
                        await _HCoreContextOperations.SaveChangesAsync();
                        await _HCoreContextOperations.DisposeAsync();

                        return await CreateAccount(_HCTMerchantOnboarding.Id);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("OnboardMerchantRequest", _Exception, _Request.UserReference, "OB0500", "Unable to process request.");
            }
        }

        private async Task<OResponse> CreateAccount(long Id)
        {
            try
            {
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var MerchantDetails = await _HCoreContextOperations.HCTMerchantOnboarding.Where(x => x.Id == Id && x.StatusId == HelperStatus.Default.Inactive).FirstOrDefaultAsync();
                    if (MerchantDetails != null)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            string? TimeZoneId = await _HCoreContext.HCCoreCountry.Where(x => x.Id == MerchantDetails.CountryId).Select(x => x.TimeZoneName).FirstOrDefaultAsync();

                            string SysPassword = HCoreHelper.GenerateGuid();

                            _Random = new Random();
                            string AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();

                            long RewardPercentage = Convert.ToInt64(await _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewardpercentage" && x.TypeId == HelperType.Configuration).Select(x => x.Value).FirstOrDefaultAsync());

                            string? CountryIsd = await _HCoreContext.HCCoreCountry.Where(x => x.Id == MerchantDetails.CountryId).Select(x => x.Isd).FirstOrDefaultAsync();
                            string? ReferralCode = HCoreHelper.GenerateSystemName(MerchantDetails.DisplayName);

                            string[] FullName = MerchantDetails.Name.Split(" ");
                            string FirstName = string.Empty, LastName = string.Empty;
                            if (FullName.Length == 0 || FullName.Length == 1)
                            {
                                FirstName = MerchantDetails.Name;
                            }
                            else if (FullName.Length == 2)
                            {
                                FirstName = FullName[0];
                                LastName = FullName[1];
                            }
                            else if (FullName.Length == 3)
                            {
                                FirstName = FullName[0];
                                LastName = FullName[2];
                            }

                            var BusinessType = await _HCoreContext.HCCore.Where(x => x.SystemName == MerchantDetails.Categories)
                                .Select(x => new
                                {
                                    x.Id,
                                    x.SystemName
                                }).FirstOrDefaultAsync();

                            _HCUAccountParameters = new List<HCUAccountParameter>();
                            _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                            _HCUAccountParameter.CommonId = await _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "rewarddeductiontype" && x.TypeId == HelperType.Configuration).Select(x => x.Id).FirstOrDefaultAsync();
                            _HCUAccountParameter.Value = "Prepay";
                            _HCUAccountParameter.HelperId = 253;
                            _HCUAccountParameter.StartTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            _HCUAccountParameter.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            _HCUAccountParameters.Add(_HCUAccountParameter);

                            _HCUAccountParameter = new HCUAccountParameter();
                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountParameter.TypeId = BusinessType.Id;
                            _HCUAccountParameter.Value = BusinessType.SystemName;
                            _HCUAccountParameter.HelperId = 253;
                            _HCUAccountParameter.StartTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            _HCUAccountParameter.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                            _HCUAccountParameters.Add(_HCUAccountParameter);

                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountAuth.Username = MerchantDetails.EmailAddress;
                            _HCUAccountAuth.Password = MerchantDetails.Password;
                            _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(SysPassword);
                            _HCUAccountAuth.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                            await _HCoreContext.HCUAccountAuth.AddAsync(_HCUAccountAuth);

                            _HCUAccount = new HCUAccount();
                            _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                            _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccount.AccountTypeId = UserAccountType.Merchant;
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                            _HCUAccount.DisplayName = MerchantDetails.DisplayName;
                            _HCUAccount.ReferralUrl = _AppConfig.WebsiteUrl + "?refby={ReferralCode}";
                            _HCUAccount.ReferralCode = ReferralCode;
                            _HCUAccount.Name = MerchantDetails.Name;
                            _HCUAccount.FirstName = FirstName;
                            _HCUAccount.LastName = LastName;
                            _HCUAccount.CpFirstName = FirstName;
                            _HCUAccount.CpLastName = LastName;
                            _HCUAccount.CpEmailAddress = MerchantDetails.EmailAddress;
                            _HCUAccount.CpMobileNumber = MerchantDetails.MobileNumber;
                            _HCUAccount.EmailAddress = MerchantDetails.EmailAddress;
                            _HCUAccount.ContactNumber = MerchantDetails.MobileNumber;
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                            _HCUAccount.AccountCode = HCoreHelper.GenerateRandomNumber(15);
                            _HCUAccount.MobileNumber = MerchantDetails.MobileNumber;
                            _HCUAccount.CountryId = (int)MerchantDetails.CountryId;
                            _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.Website;
                            _HCUAccount.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            _HCUAccount.StatusId = HelperStatus.Default.Active;
                            _HCUAccount.EmailVerificationStatus = 1;
                            _HCUAccount.NumberVerificationStatus = 1;
                            _HCUAccount.DocumentVerificationStatus = 1;
                            _HCUAccount.AccountPercentage = RewardPercentage;
                            _HCUAccount.User = _HCUAccountAuth;
                            await _HCoreContext.HCUAccount.AddAsync(_HCUAccount);
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            long MerchantId = _HCUAccount.Id;
                            string MerchantKey = _HCUAccount.Guid;

                            MerchantDetails.AccountId = MerchantId;
                            MerchantDetails.StatusId = HelperStatus.Default.Active;
                            MerchantDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            await _HCoreContextOperations.SaveChangesAsync();
                            await _HCoreContextOperations.DisposeAsync();

                            using (_HCoreContext = new HCoreContext())
                            {
                                _HCUAccountAuth = new HCUAccountAuth();
                                _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountAuth.Username = HCoreHelper.GenerateRandomNumber(10);
                                _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(8));
                                _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                                _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                _HCUAccountAuth.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                                _HCUAccountAuth.CreatedById = MerchantId;
                                _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                                await _HCoreContext.HCUAccountAuth.AddAsync(_HCUAccountAuth);

                                string? StoreReferralCode = HCoreHelper.GenerateSystemName(MerchantDetails.DisplayName);
                                _HCUAccount = new HCUAccount();
                                _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccount.AccountTypeId = UserAccountType.MerchantStore;
                                _HCUAccount.AccountOperationTypeId = AccountOperationType.OnlineAndOffline;
                                _HCUAccount.OwnerId = MerchantId;
                                _HCUAccount.DisplayName = MerchantDetails.DisplayName;
                                _HCUAccount.ReferralUrl = _AppConfig.WebsiteUrl + "?refby={StoreReferralCode}";
                                _HCUAccount.ReferralCode = StoreReferralCode;
                                _HCUAccount.Name = MerchantDetails.DisplayName;
                                _HCUAccount.EmailAddress = MerchantDetails.EmailAddress;
                                _HCUAccount.ContactNumber = MerchantDetails.MobileNumber;
                                _HCUAccount.MobileNumber = MerchantDetails.MobileNumber;
                                _HCUAccount.CountryId = (int)MerchantDetails.CountryId;
                                _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateRandomNumber(4));
                                _HCUAccount.AccountCode = _Random.Next(100000000, 999999999).ToString();
                                _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                                _HCUAccount.CreatedById = MerchantId;
                                _HCUAccount.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                                _HCUAccount.StatusId = HelperStatus.Default.Active;
                                _HCUAccount.EmailVerificationStatus = 2;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                                _HCUAccount.NumberVerificationStatus = 2;
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                                _HCUAccount.DocumentVerificationStatus = 2;
                                _HCUAccount.DocumentVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                                _HCUAccount.User = _HCUAccountAuth;
                                await _HCoreContext.HCUAccount.AddAsync(_HCUAccount);
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();
                            }

                            OAccounts.CreateAccountReuqest.Response _Response = new OAccounts.CreateAccountReuqest.Response();
                            _Response.ReferenceId = MerchantId;
                            _Response.ReferenceKey = MerchantKey;

                            return HCoreHelper.SendResponse(null, ResponseStatus.Success, _Response, "OB015", "Account created successfully.");

                        }
                    }
                    else
                    {
                        await _HCoreContextOperations.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CreateAccount", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> UpdateMerchantDetails(OAccounts.UpdateMerchantDetails.Request _Request)
        {
            try
            {
                if (_Request.ReferenceId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB016", "Reference id required.");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB017", "Reference key required.");
                }
                if (_Request.StateId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB018", "State id required.");
                }
                if (string.IsNullOrEmpty(_Request.StateKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB019", "State key required.");
                }
                if (_Request.CityId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB020", "City id required.");
                }
                if (string.IsNullOrEmpty(_Request.CityKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB021", "City key required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefaultAsync();
                    if (MerchantDetails != null)
                    {
                        string? TimeZoneId = await _HCoreContext.HCCoreCountry.Where(x => x.Id == MerchantDetails.CountryId).Select(x => x.TimeZoneName).FirstOrDefaultAsync();

                        if (!string.IsNullOrEmpty(_Request.ReferralCode))
                        {
                            long? OwnerId = await _HCoreContext.HCUAccount.Where(x => x.ReferralCode == _Request.ReferralCode && x.AccountTypeId == UserAccountType.Merchant).Select(x => x.Id).FirstOrDefaultAsync();
                            if (OwnerId > 0 && OwnerId != null)
                            {
                                MerchantDetails.OwnerId = OwnerId;
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB022", "Please use a valid referral code.");
                            }
                        }
                        else
                        {
                            MerchantDetails.OwnerId = 3;
                        }

                        MerchantDetails.Latitude = (double)_Request.Latitude;
                        MerchantDetails.Longitude = (double)_Request.Longitude;
                        MerchantDetails.CountryId = (int)MerchantDetails.CountryId;
                        MerchantDetails.StateId = _Request.StateId;
                        MerchantDetails.CityId = _Request.CityId;
                        MerchantDetails.Address = _Request.Address;

                        _HCCoreAddress = new HCCoreAddress();
                        _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreAddress.AccountId = MerchantDetails.Id;
                        _HCCoreAddress.DisplayName = MerchantDetails.DisplayName;
                        _HCCoreAddress.Name = MerchantDetails.Name;
                        _HCCoreAddress.ContactNumber = MerchantDetails.ContactNumber;
                        _HCCoreAddress.AdditionalContactNumber = MerchantDetails.ContactNumber;
                        _HCCoreAddress.EmailAddress = MerchantDetails.EmailAddress;
                        _HCCoreAddress.IsPrimaryAddress = 1;
                        _HCCoreAddress.LocationTypeId = 792;
                        _HCCoreAddress.Latitude = (double)_Request.Latitude;
                        _HCCoreAddress.Longitude = (double)_Request.Longitude;
                        _HCCoreAddress.CountryId = (int)MerchantDetails.CountryId;
                        _HCCoreAddress.StateId = _Request.StateId;
                        _HCCoreAddress.CityId = _Request.CityId;
                        _HCCoreAddress.AddressLine1 = _Request.Address;
                        _HCCoreAddress.MapAddress = _Request.Address;
                        _HCCoreAddress.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                        _HCCoreAddress.CreatedById = MerchantDetails.Id;
                        _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.HCCoreAddress.AddAsync(_HCCoreAddress);
                        await _HCoreContext.SaveChangesAsync();

                        _MDDealAddress = new MDDealAddress();
                        _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                        _MDDealAddress.AccountId = MerchantDetails.Id;
                        _MDDealAddress.AddressId = _HCCoreAddress.Id;
                        await _HCoreContext.MDDealAddress.AddAsync(_MDDealAddress);
                        await _HCoreContext.SaveChangesAsync();

                        MerchantDetails.AddressId = _HCCoreAddress.Id;
                        await _HCoreContext.SaveChangesAsync();

                        var StoreDetails = await _HCoreContext.HCUAccount.Where(x => x.OwnerId == MerchantDetails.Id && x.AccountTypeId == UserAccountType.MerchantStore).FirstOrDefaultAsync();
                        if (StoreDetails != null)
                        {

                            StoreDetails.Latitude = (double)_Request.Latitude;
                            StoreDetails.Longitude = (double)_Request.Longitude;
                            StoreDetails.CountryId = (int)StoreDetails.CountryId;
                            StoreDetails.StateId = _Request.StateId;
                            StoreDetails.CityId = _Request.CityId;
                            StoreDetails.Address = _Request.Address;

                            _HCCoreAddress = new HCCoreAddress();
                            _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreAddress.AccountId = StoreDetails.Id;
                            _HCCoreAddress.DisplayName = StoreDetails.DisplayName;
                            _HCCoreAddress.Name = StoreDetails.Name;
                            _HCCoreAddress.ContactNumber = StoreDetails.ContactNumber;
                            _HCCoreAddress.AdditionalContactNumber = StoreDetails.ContactNumber;
                            _HCCoreAddress.EmailAddress = StoreDetails.EmailAddress;
                            _HCCoreAddress.IsPrimaryAddress = 1;
                            _HCCoreAddress.LocationTypeId = 792;
                            _HCCoreAddress.Latitude = (double)_Request.Latitude;
                            _HCCoreAddress.Longitude = (double)_Request.Longitude;
                            _HCCoreAddress.CountryId = (int)StoreDetails.CountryId;
                            _HCCoreAddress.StateId = _Request.StateId;
                            _HCCoreAddress.CityId = _Request.CityId;
                            _HCCoreAddress.AddressLine1 = _Request.Address;
                            _HCCoreAddress.MapAddress = _Request.Address;
                            _HCCoreAddress.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            _HCCoreAddress.CreatedById = MerchantDetails.Id;
                            _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                            await _HCoreContext.HCCoreAddress.AddAsync(_HCCoreAddress);
                            await _HCoreContext.SaveChangesAsync();

                            _MDDealAddress = new MDDealAddress();
                            _MDDealAddress.Guid = HCoreHelper.GenerateGuid();
                            _MDDealAddress.AccountId = StoreDetails.Id;
                            _MDDealAddress.AddressId = _HCCoreAddress.Id;
                            await _HCoreContext.MDDealAddress.AddAsync(_MDDealAddress);
                            await _HCoreContext.SaveChangesAsync();

                            StoreDetails.AddressId = _HCCoreAddress.Id;
                            await _HCoreContext.SaveChangesAsync();
                        }

                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.AccountId = MerchantDetails.Id;
                        _HCUAccountParameter.TypeId = HelperType.ThankUCashDeals;
                        _HCUAccountParameter.SubTypeId = HelperType.ThankUCashDeal.Both;
                        _HCUAccountParameter.CommonId = await _HCoreContext.HCCoreCommon.Where(x => x.SystemName == "dealcategory").Select(x => x.Id).FirstOrDefaultAsync();
                        _HCUAccountParameter.Value = "3";
                        _HCUAccountParameter.StartTime = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                        _HCUAccountParameter.CreateDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                        _HCUAccountParameter.CreatedById = MerchantDetails.Id;
                        _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                        await _HCoreContext.HCUAccountParameter.AddAsync(_HCUAccountParameter);
                        await _HCoreContext.SaveChangesAsync();

                        long? DocumentStorageId = 0;
                        if (_Request.IsBusinessRegistered == true)
                        {
                            MerchantDetails.EmailVerificationStatus = 2;
                            MerchantDetails.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            MerchantDetails.NumberVerificationStatus = 2;
                            MerchantDetails.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            MerchantDetails.DocumentVerificationStatus = 2;
                            MerchantDetails.DocumentVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                            {
                                DocumentStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                            }
                            if (DocumentStorageId > 0 && DocumentStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var AccountDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == MerchantDetails.Guid).FirstOrDefaultAsync();
                                    if (AccountDetails != null)
                                    {
                                        if (DocumentStorageId != null)
                                        {
                                            AccountDetails.PosterStorageId = DocumentStorageId;
                                        }
                                        await _HCoreContext.SaveChangesAsync();
                                        await _HCoreContext.DisposeAsync();
                                    }
                                    else
                                    {
                                        await _HCoreContext.DisposeAsync();
                                    }
                                }
                            }
                        }
                        else
                        {
                            MerchantDetails.EmailVerificationStatus = 2;
                            MerchantDetails.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            MerchantDetails.NumberVerificationStatus = 2;
                            MerchantDetails.EmailVerificationStatusDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                            MerchantDetails.DocumentVerificationStatus = 1;
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                            {
                                DocumentStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                            }
                            if (DocumentStorageId > 0 && DocumentStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var AccountDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == MerchantDetails.Guid).FirstOrDefaultAsync();
                                    if (AccountDetails != null)
                                    {
                                        if (DocumentStorageId != null)
                                        {
                                            AccountDetails.PosterStorageId = DocumentStorageId;
                                        }
                                        await _HCoreContext.SaveChangesAsync();
                                        await _HCoreContext.DisposeAsync();
                                    }
                                    else
                                    {
                                        await _HCoreContext.DisposeAsync();
                                    }
                                }
                            }
                        }

                        string PanelUrl = "";
                        if (HostEnvironment == HostEnvironmentType.Dev)
                        {
                            PanelUrl = "https://merchant-dealday.thankucash.dev";
                        }
                        else if (HostEnvironment == HostEnvironmentType.Tech)
                        {
                            PanelUrl = "https://merchant-dealday.thankucash.tech";
                        }
                        else if (HostEnvironment == HostEnvironmentType.Test)
                        {
                            PanelUrl = "https://merchant.dealday.com";
                        }
                        else
                        {
                            PanelUrl = "https://merchant.dealday.com";
                        }


                        OAccounts.UpdateMerchantDetails.Response _Response = new OAccounts.UpdateMerchantDetails.Response();
                        _Response.ReferenceId = MerchantDetails.Id;
                        _Response.ReferenceKey = MerchantDetails.Guid;

                        OEmailProcessor.Registration _EmailRequest = new OEmailProcessor.Registration();
                        _EmailRequest.EmailAddress = MerchantDetails.EmailAddress;
                        _EmailRequest.MerchantDisplayName = MerchantDetails.DisplayName;
                        _EmailRequest.PanelUrl = PanelUrl;
                        _EmailRequest.UserReference = _Request.UserReference;

                        var _Actor = ActorSystem.Create("ActorRegistrationEmail");
                        var _ActorNotify = _Actor.ActorOf<ActorRegistrationEmail>("ActorRegistrationEmail");
                        _ActorNotify.Tell(_EmailRequest);

                        return HCoreHelper.SendResponse(null, ResponseStatus.Success, _Response, "OB022", "Account details updated successfully.");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdateMerchantDetails", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> ForgotPassword(OAccounts.ForgotPassword.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB023", "Email address required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantAccountDetails = await _HCoreContext.HCUAccount.Where(x => x.EmailAddress == _Request.EmailAddress && x.AccountTypeId == UserAccountType.Merchant).FirstOrDefaultAsync();
                    if (MerchantAccountDetails != null)
                    {
                        string? EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                        MerchantAccountDetails.AccessPin = HCoreEncrypt.EncryptHash(EmailOtp);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        OAccounts.ForgotPassword.Response _Response = new OAccounts.ForgotPassword.Response();
                        _Response.EmailAddress = MerchantAccountDetails.EmailAddress;
                        _Response.Reference = MerchantAccountDetails.Guid;

                        OEmailProcessor.EmailVerification _Verification = new OEmailProcessor.EmailVerification();
                        _Verification.EmailAddress = MerchantAccountDetails.EmailAddress;
                        _Verification.Code = EmailOtp;
                        _Verification.UserDisplayName = MerchantAccountDetails.DisplayName;

                        var _Actor = ActorSystem.Create("ActorEmailVerification");
                        var _ActorNotify = _Actor.ActorOf<ActorEmailVerification>("ActorEmailVerification");
                        _ActorNotify.Tell(_Verification);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OB024", "Verification code sent to your registered email address.");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ForgotPassword", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> VerifyEmail(OAccounts.VerifyEmailRequest.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB025", "Email address required.");
                }
                if (string.IsNullOrEmpty(_Request.VerificationCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB026", "Email verification code required.");
                }
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB027", "Reference required.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.Reference).FirstOrDefaultAsync();
                    if (MerchantDetails != null)
                    {
                        string? AccessPin = HCoreEncrypt.DecryptHash(MerchantDetails.AccessPin);
                        if (AccessPin == _Request.VerificationCode)
                        {
                            OAccounts.VerifyEmailRequest.Response _Response = new OAccounts.VerifyEmailRequest.Response();
                            _Response.EmailAddress = MerchantDetails.EmailAddress;
                            _Response.Reference = MerchantDetails.Guid;
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OB028", "User verification successful.");
                        }
                        else
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB029", "Invalid verification code.");
                        }
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("VerifyEmail", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> ResendEmailOtp(OAccounts.ResendEmaiLOtpRequest.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB031", "Email address required.");
                }
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB032", "Reference required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.Reference).FirstOrDefaultAsync();
                    if (MerchantDetails != null)
                    {
                        string? EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                        MerchantDetails.AccessPin = HCoreEncrypt.EncryptHash(EmailOtp);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        OAccounts.ResendEmaiLOtpRequest.Response _Response = new OAccounts.ResendEmaiLOtpRequest.Response();
                        _Response.EmailAddress = MerchantDetails.EmailAddress;
                        _Response.Reference = MerchantDetails.Guid;

                        OEmailProcessor.EmailVerification _Verification = new OEmailProcessor.EmailVerification();
                        _Verification.EmailAddress = MerchantDetails.EmailAddress;
                        _Verification.Code = EmailOtp;
                        _Verification.UserDisplayName = MerchantDetails.DisplayName;

                        var _Actor = ActorSystem.Create("ActorEmailVerification");
                        var _ActorNotify = _Actor.ActorOf<ActorEmailVerification>("ActorEmailVerification");
                        _ActorNotify.Tell(_Verification);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OB033", "Verification code sent to your registered email address.");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ResendEmailOtp", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> UpdatePassword(OAccounts.UpdatePasswordRequest.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB025", "Email address required.");
                }
                if (string.IsNullOrEmpty(_Request.ConfirmedPassword))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB026", "Password confirmation required.");
                }
                if (string.IsNullOrEmpty(_Request.NewPassword))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB026", "New password required.");
                }
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB027", "Reference required.");
                }
                if (_Request.ConfirmedPassword != _Request.NewPassword)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB030", "Confirmed password and new password must be same.");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.Reference && x.AccountTypeId == UserAccountType.Merchant).FirstOrDefaultAsync();
                    if (MerchantDetails != null)
                    {
                        string? TimeZoneId = await _HCoreContext.HCCoreCountry.Where(x => x.Id == MerchantDetails.CountryId).Select(x => x.TimeZoneName).FirstOrDefaultAsync();

                        var UserDetails = await _HCoreContext.HCUAccountAuth.Where(x => x.Id == MerchantDetails.UserId).FirstOrDefaultAsync();
                        if (UserDetails != null)
                        {
                            string? AuthPassword = HCoreEncrypt.DecryptHash(UserDetails.Password);
                            if (AuthPassword != _Request.NewPassword)
                            {
                                UserDetails.Password = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                UserDetails.SecondaryPassword = HCoreEncrypt.EncryptHash(_Request.NewPassword);
                                UserDetails.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                                UserDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                                UserDetails.ModifyById = MerchantDetails.Id;
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();

                                OAccounts.UpdatePasswordRequest.Response _Response = new OAccounts.UpdatePasswordRequest.Response();
                                _Response.ReferenceId = MerchantDetails.Id;
                                _Response.ReferenceKey = MerchantDetails.Guid;
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OB028", "Password updated successfully.");
                            }
                            else
                            {
                                await _HCoreContext.DisposeAsync();
                                return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB029", "Please use the password which you haven't used before.");
                            }
                        }
                        else
                        {
                            await _HCoreContext.DisposeAsync();
                            return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                        }
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("UpdatePassword", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> TwoFactorAuthentication(OAccounts.TwoFactorAuthRequest.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB023", "Email address required.");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.Reference).FirstOrDefaultAsync();
                    if (MerchantDetails != null)
                    {
                        string? EmailOtp = HCoreHelper.GenerateRandomNumber(4);
                        MerchantDetails.AccessPin = HCoreEncrypt.EncryptHash(EmailOtp);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        OAccounts.ForgotPassword.Response _Response = new OAccounts.ForgotPassword.Response();
                        _Response.EmailAddress = MerchantDetails.EmailAddress;
                        _Response.Reference = MerchantDetails.Guid;

                        OEmailProcessor.EmailVerification _Verification = new OEmailProcessor.EmailVerification();
                        _Verification.EmailAddress = MerchantDetails.EmailAddress;
                        _Verification.Code = EmailOtp;
                        _Verification.UserDisplayName = MerchantDetails.DisplayName;

                        var _Actor = ActorSystem.Create("ActorEmailVerification");
                        var _ActorNotify = _Actor.ActorOf<ActorEmailVerification>("ActorEmailVerification");
                        _ActorNotify.Tell(_Verification);

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OB024", "Verification code sent to your registered email address.");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("ForgotPassword", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> VerifyTwoFactorAuthentication(OAccounts.VerifyTwoFactorAuthRequest.Request _Request)
        {
            try
            {
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB025", "Email address required.");
                }
                if (string.IsNullOrEmpty(_Request.VerificationCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB026", "Email verification code required.");
                }
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB027", "Reference required.");
                }

                OAccounts.VerifyTwoFactorAuthRequest.Response _Response = new OAccounts.VerifyTwoFactorAuthRequest.Response();

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.Reference && x.AccountTypeId == UserAccountType.Merchant).FirstOrDefaultAsync();
                    if (MerchantDetails != null)
                    {
                        string? TimeZoneId = await _HCoreContext.HCCoreCountry.Where(x => x.Id == MerchantDetails.CountryId).Select(x => x.TimeZoneName).FirstOrDefaultAsync();

                        MerchantDetails.ModifyDate = HCoreHelper.ConvertFromUTC(HCoreHelper.GetGMTDateTime(), TimeZoneId);
                        await _HCoreContext.SaveChangesAsync();
                        await _HCoreContext.DisposeAsync();

                        _Response.ReferenceId = MerchantDetails.Id;
                        _Response.ReferenceKey = MerchantDetails.Guid;
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "OB028", "User verification successfull.");
                    }
                    else
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(null, ResponseStatus.Error, null, "OB0404", "Details not found.");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("VerifyTwoFactorAuthentication", _Exception, null, "OB0500", "Unable to process request.");
            }
        }

        internal async Task<OResponse> GetMerchantDetails(OReference _Request)
        {
            try
            {
                if (_Request.ReferenceId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OBREF", "Reference id required");
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OBREFKEY", "Reference key required");
                }

                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                          .Select(x => new OAccounts.MerchantDetails
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,
                                              DisplayName = x.DisplayName,
                                              MobileNumber = x.MobileNumber,
                                              EmailAddress = x.EmailAddress,
                                              Address = x.Address,
                                              ImageUrl = x.IconStorage.Path,
                                              TotalProductDeals = x.MDDealAccount.Where(y => y.AccountId == x.Id && y.DealTypeId == DealType.ProductDeal && y.StatusId == HelperStatus.Deals.Published).Count(),
                                              TotalServiceDeals = x.MDDealAccount.Where(y => y.AccountId == x.Id && y.DealTypeId == DealType.ServiceDeal && y.StatusId == HelperStatus.Deals.Published).Count(),
                                              TotalSoldDeals = x.MDDealAccount.Where(y => y.AccountId == x.Id).Sum(a => a.MDDealCode.Sum(m => m.ItemCount)),
                                              CategoryId = x.PrimaryCategoryId,
                                              CategoryKey = x.PrimaryCategory.Guid,
                                              CategoryName = x.PrimaryCategory.Name,
                                          }).FirstOrDefaultAsync();

                    if (MerchantDetails != null)
                    {
                        await _HCoreContext.DisposeAsync();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, MerchantDetails, "OB0200", "Merchant details loaded successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "OB00404", "Merchant details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("", _Exception, _Request.UserReference, "OB0500", _Exception.Message);
            }
        }
    }
}