//==================================================================================
// FileName: ManageCore.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for syncing data in cache by 3 min intereval 
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using Akka.Actor;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Core;

namespace HCore.TUC.Plugins.MadDeals
{
    public class ManageCore
    {
        CoreMadDeals _CoreMadDeals;
        public void StartMadDealsStorage()
        {
            _CoreMadDeals = new CoreMadDeals();
            _CoreMadDeals.SyncDealCategory();
            _CoreMadDeals.SyncMerchant();
            _CoreMadDeals.SyncStore();
            _CoreMadDeals.SyncDeal();
            _CoreMadDeals.SyncDealPromotion();
        }
        public void ProcessSync()
        {
            try
            {
                var ActorMadDeals_CategoryHandler = ActorSystem.Create("ActorMadDealsCategoryHandler");
                var ActorMadDeals_CategoryHandlerN = ActorMadDeals_CategoryHandler.ActorOf<ActorMadDealsCategoryHandler>("ActorMadDealsCategoryHandler");
                ActorMadDeals_CategoryHandlerN.Tell(true);

                var ActorMadDeals_DealHandler = ActorSystem.Create("ActorMadDealsDealHandler");
                var ActorMadDeals_DealHandlerN = ActorMadDeals_DealHandler.ActorOf<ActorMadDealsDealHandler>("ActorMadDealsDealHandler");
                ActorMadDeals_DealHandlerN.Tell(true);
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("HCore.TUC.Plugins.MadDeals.ProcessSync", ex);
            }

        }
    }

    internal class ActorMadDealsCategoryHandler : ReceiveActor
    {
        CoreMadDeals _CoreMadDeals;
        public ActorMadDealsCategoryHandler()
        {
            Receive<bool>(_Request =>
            {
                try
                {
                    _CoreMadDeals = new CoreMadDeals();
                    _CoreMadDeals.SyncDealCategory();
                }
                catch (Exception ex)
                {
                    HCoreHelper.LogException("ActorMadDealsCategoryHandler", ex);
                }

            });
        }
    }
    internal class ActorMadDealsDealHandler : ReceiveActor
    {
        CoreMadDeals _CoreMadDeals;
        public ActorMadDealsDealHandler()
        {
            Receive<bool>(_Request =>
            {
                try
                {
                    _CoreMadDeals = new CoreMadDeals();
                    _CoreMadDeals.SyncMerchant();
                    _CoreMadDeals.SyncStore();
                    _CoreMadDeals.SyncDeal();
                    _CoreMadDeals.SyncDealPromotion();
                }
                catch (Exception ex)
                {
                    HCoreHelper.LogException("ActorMadDealsDealHandler", ex);
                }

            });
        }
    }
}
