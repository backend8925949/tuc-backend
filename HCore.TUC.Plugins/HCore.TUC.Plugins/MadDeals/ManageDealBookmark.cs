//==================================================================================
// FileName: ManageDealBookmark.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Framework;
using HCore.TUC.Plugins.MadDeals.Object;

namespace HCore.TUC.Plugins.MadDeals
{
    public class ManageDealBookmark
    {
        FrameworkDealBookmark _FrameworkDealBookmark;

        /// <summary>
        /// Description: Saves the book mark.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveBookMark(ODealBookmark.Save.Request _Request)
        {
            _FrameworkDealBookmark = new FrameworkDealBookmark();
            return _FrameworkDealBookmark.SaveBookMark(_Request);
        }
        /// <summary>
        /// Description: Gets the bookmark.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetBookmark(OList.Request _Request)
        {
            _FrameworkDealBookmark = new FrameworkDealBookmark();
            return _FrameworkDealBookmark.GetBookmark(_Request);
        }
    }
}
