//==================================================================================
// FileName: ManageDealNG.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Framework;

namespace HCore.TUC.Plugins.MadDeals
{
    public class ManageDealNG
    {
        FrameworkDealsNG _FrameworkDealsNG;
        /// <summary>
        /// Gets the stats.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStats(OList.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetStats(_Request);
        }
        /// <summary>
        /// Gets the search states.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSearchStates(OList.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetSearchStates(_Request);
        }
        /// <summary>
        /// Gets the search cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSearchCities(OList.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetSearchCities(_Request);
        }
        /// <summary>
        /// Gets the cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCities(OList.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetCities(_Request);
        }
        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCategories(OList.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetCategories(_Request);
        }
        /// <summary>
        /// Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(MadDeals.Object.OMadDealsNG.Deals.List.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetDeal(_Request);
        }
        /// <summary>
        /// Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(MadDeals.Object.OMadDealsNG.Deal.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetDeal(_Request);
        }
        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchant(OList.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetMerchant(_Request);
        }
        /// <summary>
        /// Gets the slider images.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSliderImages(MadDeals.Object.OMadDealsNG.Slider.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetSliderImages(_Request);
        }
        /// <summary>
        /// Gets the deal promotions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealPromotions(MadDeals.Object.OMadDealsNG.Slider.Request _Request)
        {
            _FrameworkDealsNG = new FrameworkDealsNG();
            return _FrameworkDealsNG.GetDealPromotions(_Request);
        }

    }
}

