//==================================================================================
// FileName: FrameworkDealBookmark.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deal bookmark
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Resource;
using HCore.TUC.Plugins.MadDeals.Object;
using static HCore.Helper.HCoreConstant;

namespace HCore.TUC.Plugins.MadDeals.Framework
{
    public class FrameworkDealBookmark
    {
        HCoreContext _HCoreContext;
        MDDealBookmark _MDDealBookmark;

        /// <summary>
        /// Description: Saves the book mark.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveBookMark(ODealBookmark.Save.Request _Request)
        {
            #region Save Bookmark
            try
            {
                #region Validation
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP0188", ResponseCode.HCP0188);
                }
                if (_Request.UserReference.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP0191", ResponseCode.HCP0191);
                }
                #endregion

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var Details = _HCoreContext.MDDealBookmark.Where(x => x.Deal.Guid == _Request.DealKey && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                    if(Details != null)
                    {
                        _HCoreContext.MDDealBookmark.Remove(Details);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            IsBookmarked = false
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "HCP0187", ResponseCode.HCP0187);
                    }
                    else
                    {
                        long DealId = _HCoreContext.MDDeal.Where(x => x.Guid == _Request.DealKey).Select(x=>x.Id).FirstOrDefault();
                        if (DealId  < 1)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP0189", ResponseCode.HCP0189);
                        }

                        _MDDealBookmark = new MDDealBookmark();
                        _MDDealBookmark.Guid = HCoreHelper.GenerateGuid();
                        _MDDealBookmark.DealId = DealId;
                        _MDDealBookmark.AccountId = _Request.AuthAccountId;
                        _MDDealBookmark.CreateDate = HCoreHelper.GetGMTDateTime();
                        _MDDealBookmark.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.MDDealBookmark.Add(_MDDealBookmark);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        var _Response = new
                        {
                            IsBookmarked = true
                        };
                        #region Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, "HCP0186", ResponseCode.HCP0186);
                        #endregion
                    }
                }
                #endregion
            }
            #region Exception
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SaveBookmark", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
            #endregion
        }

        /// <summary>
        /// Description: Gets the bookmark.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetBookmark(OList.Request _Request)
        {
            #region Get Bookmark
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.MDDealBookmark
                            .Where(x => x.AccountId == _Request.UserReference.AccountId)
                        .Select(x => new ODealBookmark.List
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            
                            DealId = x.DealId,
                            DealKey = x.Deal.Guid,

                            Title = x.Deal.Title,

                            MerchantKey = x.Deal.Account.Guid,
                            MerchantName = x.Deal.Account.Name,
                            MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                            CategoryKey = x.Deal.Category.Guid,
                            CategoryName = x.Deal.Category.Name,

                            DealCreateDate = x.Deal.CreateDate,
                            DealEndDate = x.Deal.EndDate,

                            ImageUrl = x.Deal.PosterStorage.Path,

                            ActualPrice = x.Deal.ActualPrice,
                            SellingPrice = x.Deal.SellingPrice,
                            Address = x.Deal.Account.Address,
                            CityAreaName = x.Deal.Account.CityArea.Name,
                            CityName = x.Deal.Account.City.Name,
                            StateName = x.Deal.Account.State.Name,
                            DealSlug = x.Deal.Slug,
                            //AccountId = x.AccountId,
                            //AccountKey = x.Account.Guid,
                            //AccountName = x.Account.Name,
                            //AccountDisplayName = x.Account.DisplayName,
                            //MobileNumber = x.Account.MobileNumber,
                            //EmailAddress = x.Account.EmailAddress,
                            //AccountTypeId = x.Account.AccountTypeId,
                            //AccountTypeCode = x.Account.AccountType.Guid,
                            //AccountTypeName = x.Account.AccountType.Name,
                            //AccountTypeDisplayName = x.Account.AccountType.SystemName,

                            CreateDate = x.CreateDate,
                            //ModifyDate = x.ModifyDate,

                            //StatusId = x.StatusId,
                            //StatusCode = x.Status.Guid,
                            //StatusName = x.Status.Name,
                        }).Where(_Request.SearchCondition)
                          .Count();
                    }
                    #endregion

                    #region Data
                    List<ODealBookmark.List> Data = _HCoreContext.MDDealBookmark
                            .Where(x => x.AccountId == _Request.UserReference.AccountId)
                        .Select(x => new ODealBookmark.List
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,

                            DealId = x.DealId,
                            DealKey = x.Deal.Guid,

                            Title = x.Deal.Title,

                            MerchantKey = x.Deal.Account.Guid,
                            MerchantName = x.Deal.Account.Name,
                            MerchantIconUrl = x.Deal.Account.IconStorage.Path,

                            CategoryKey = x.Deal.Category.Guid,
                            CategoryName = x.Deal.Category.Name,

                            DealCreateDate = x.Deal.CreateDate,
                            DealEndDate = x.Deal.EndDate,

                            ImageUrl = x.Deal.PosterStorage.Path,

                            ActualPrice = x.Deal.ActualPrice,
                            SellingPrice = x.Deal.SellingPrice,
                            Address = x.Deal.Account.Address,
                            CityAreaName = x.Deal.Account.CityArea.Name,
                            CityName = x.Deal.Account.City.Name,
                            StateName = x.Deal.Account.State.Name,
                            DealSlug = x.Deal.Slug,
                            //AccountId = x.AccountId,
                            //AccountKey = x.Account.Guid,
                            //AccountName = x.Account.Name,
                            //AccountDisplayName = x.Account.DisplayName,
                            //MobileNumber = x.Account.MobileNumber,
                            //EmailAddress = x.Account.EmailAddress,
                            //AccountTypeId = x.Account.AccountTypeId,
                            //AccountTypeCode = x.Account.AccountType.Guid,
                            //AccountTypeName = x.Account.AccountType.Name,
                            //AccountTypeDisplayName = x.Account.AccountType.SystemName,

                            CreateDate = x.CreateDate,
                            //ModifyDate = x.ModifyDate,

                            //StatusId = x.StatusId,
                            //StatusCode = x.Status.Guid,
                            //StatusName = x.Status.Name,
                        }).Where(_Request.SearchCondition)
                          .OrderBy(_Request.SortExpression)
                          .Skip(_Request.Offset)
                          .Take(_Request.Limit)
                          .ToList();
                    #endregion
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                        {
                            DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                        }
                        else
                        {
                            DataItem.ImageUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.MerchantIconUrl))
                        {
                            DataItem.MerchantIconUrl = _AppConfig.StorageUrl + DataItem.MerchantIconUrl;
                        }
                        else
                        {
                            DataItem.MerchantIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Response
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    #endregion
                }
                #endregion
            }
            #region Exception
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBookmark", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
            #endregion
        }
    }
}
