//==================================================================================
// FileName: FrameworkDealReview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deal review
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations;
using HCore.Operations.Object;
//using HCore.TUC.Plugins.MadDeals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using HCore.TUC.Plugins.MadDeals.Object;
using HCore.TUC.Plugins.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.TUC.Plugins.MadDeals.Framework
{

    public class FrameworkDealReview
    {
        HCoreContext _HCoreContext;
        MDDealReview _MDDealReview;
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        HCore.TUC.Plugins.Operations.OOperations.Wallet.Credit.Response _CreditResponse;

        #region Framework
        /// <summary>
        /// Saves the deal review
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SaveDealReview(ODealReview.Save _Request)
        {
            try
            {
                #region Validation
                if (string.IsNullOrEmpty(_Request.DealKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0193", ResponseCode.HCP0193);
                }
                if (_Request.DealId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0192", ResponseCode.HCP0192);
                }
                #endregion

                #region Operations
                using (_HCoreContext = new HCoreContext())
                {

                    MDDeal Details = _HCoreContext.MDDeal.Where(x => x.Id == _Request.DealId && x.Guid == _Request.DealKey).FirstOrDefault();
                    if (Details != null)
                    {

                        var purchasedDeal = _HCoreContext.MDDealCode.Where(x => x.DealId == _Request.DealId && x.Deal.Guid == _Request.DealKey && x.AccountId == _Request.AuthAccountId).Select(x => x.StatusId).ToList();
                        if (purchasedDeal == null || purchasedDeal.Count == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0195", ResponseCode.HCP0195);
                        }
                        if (!purchasedDeal.Contains(HelperStatus.DealCodes.Used))
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0211", ResponseCode.HCP0211);
                        }
                        //validate he havent reviewed 
                        if (_HCoreContext.MDDealReview.Count(x => x.DealId == _Request.DealId && x.CreatedById == _Request.AuthAccountId) != 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0210", ResponseCode.HCP0210);
                        }

                        _MDDealReview = new MDDealReview();
                        _MDDealReview.Guid = HCoreHelper.GenerateGuid();
                        _MDDealReview.DealId = _Request.DealId;
                        _MDDealReview.Rating = _Request.Rating;
                        _MDDealReview.IsWorking = 1;
                        _MDDealReview.Review = _Request.Review;
                        _MDDealReview.CreateDate = HCoreHelper.GetGMTDateTime();
                        _MDDealReview.CreatedById = _Request.AuthAccountId;
                        _MDDealReview.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.MDDealReview.Add(_MDDealReview);
                        _HCoreContext.SaveChanges();
                        _HCoreContext.Dispose();
                        #region SendResponse
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP0194", ResponseCode.HCP0194);
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP0189", ResponseCode.HCP0189);
                    }
                }
                #endregion
            }
            #region Exception
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("SaveDealReview", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }



        /// <summary>
        /// Gets the deal review
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetDealReview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if(!string.IsNullOrEmpty(_Request.Slug))
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDealReview
                                                    .Where(x => x.Deal.Slug == _Request.Slug
                                                    && x.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODealReview.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountDisplayName = x.CreatedBy.DisplayName,
                                                        AccountIconUrl = x.CreatedBy.IconStorage.Path,

                                                        Review = x.Review,
                                                        Rating = x.Rating,
                                                        IsWorking = x.IsWorking,
                                                        Comment = x.Comment,
                                                        CreateDate = x.CreateDate,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<ODealReview.List> Data = _HCoreContext.MDDealReview
                                                     .Where(x => x.Deal.Slug == _Request.Slug
                                                    && x.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODealReview.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountDisplayName = x.CreatedBy.DisplayName,
                                                        AccountIconUrl = x.CreatedBy.IconStorage.Path,

                                                        Review = x.Review,
                                                        Rating = x.Rating,
                                                        IsWorking = x.IsWorking,
                                                        Comment = x.Comment,
                                                        CreateDate = x.CreateDate,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in Data)
                        {
                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.MDDealReview
                                                    .Where(x => x.Deal.Slug == _Request.ReferenceKey
                                                    && x.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODealReview.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountDisplayName = x.CreatedBy.DisplayName,
                                                        AccountIconUrl = x.CreatedBy.IconStorage.Path,

                                                        Review = x.Review,
                                                        Rating = x.Rating,
                                                        IsWorking = x.IsWorking,
                                                        Comment = x.Comment,
                                                        CreateDate = x.CreateDate,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<ODealReview.List> Data = _HCoreContext.MDDealReview
                                                     .Where(x => x.Deal.Slug == _Request.ReferenceKey
                                                    && x.StatusId == HelperStatus.Default.Active)
                                                    .Select(x => new ODealReview.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        AccountDisplayName = x.CreatedBy.DisplayName,
                                                        AccountIconUrl = x.CreatedBy.IconStorage.Path,

                                                        Review = x.Review,
                                                        Rating = x.Rating,
                                                        IsWorking = x.IsWorking,
                                                        Comment = x.Comment,
                                                        CreateDate = x.CreateDate,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        foreach (var DataItem in Data)
                        {
                            if (!string.IsNullOrEmpty(DataItem.AccountIconUrl))
                            {
                                DataItem.AccountIconUrl = _AppConfig.StorageUrl + DataItem.AccountIconUrl;
                            }
                            else
                            {
                                DataItem.AccountIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDealReview", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        #endregion
    }
}
