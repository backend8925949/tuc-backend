//==================================================================================
// FileName: FrameworkDealsNG.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to deals
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using GeoCoordinatePortable;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.Deals.Resource;
using HCore.TUC.Plugins.MadDeals.Core;
using HCore.TUC.Plugins.MadDeals.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.TUC.Plugins.Deals.Framework.FrameworkDeal;

namespace HCore.TUC.Plugins.MadDeals.Framework
{
    public class FrameworkDealsNG
    {
        HCoreContext _HCoreContext;
        MDDealReview _MDDealReview;
        List<OMadDealsNG.Deals.List.Response> _DealsList;
        List<OMadDealsNG.Merchant.List.Response> _MerchantList;
        OMadDealsNG.Deal.Response _DetalDetails;
        OMadDealsNG.Slider.Response _SliderResponse;
        List<OMadDealsNG.Slider.Data> _SliderImages;
        /// <summary>
        /// Gets the stats.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStats(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                //DateTime ActiveTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                long ActiveDeals = Core.CoreMadDeals._CDeals.Count(a => ActiveTime >= a.StartDate && ActiveTime <= a.EndDate);
                var _Cities = Core.CoreMadDeals._CCities.Where(x => Core.CoreMadDeals._CDeals.Count(a => a.CityName == x.Name && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate) > 0).OrderBy(x => x.Name).Select(x => new OMadDealsNG.Stats
                {
                    ReferenceId = x.ReferenceId,
                    ReferenceKey = x.ReferenceKey,
                    Name = x.Name,
                    DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.CityName == x.Name && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate),
                    MerchantsCount = Core.CoreMadDeals._CMerchants.Count(a => a.CityName == x.Name),
                    StoresCount = Core.CoreMadDeals._CStores.Count(a => a.CityName == x.Name),
                }).ToList();
                foreach (var _City in _Cities)
                {
                    _City.Merchants = Core.CoreMadDeals._CMerchants
                        .Where(a => a.CityName == _City.Name)
                        .Select(x => new OMadDealsNG.Account
                        {
                            ReferenceId = x.ReferenceId,
                            ReferenceKey = x.ReferenceKey,
                            Name = x.DisplayName,
                            DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.CityName == _City.Name && a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate)
                        }).ToList();

                    _City.Stores = Core.CoreMadDeals._CStores
                       .Where(a => a.CityName == _City.Name)
                       .Select(x => new OMadDealsNG.Account
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           Name = x.DisplayName,
                           DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.CityName == _City.Name && a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate)
                       }).ToList();
                    _City.Deals = Core.CoreMadDeals._CDeals.Where(a => a.CityName == _City.Name && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate)
                       .Select(x => new OMadDealsNG.StatsDeals
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           MerchantDisplayName = x.MerchantName,
                           MerchantIconUrl = x.MerchantIconUrl,
                           Title = x.Title,
                           StartDate = x.StartDate,
                           EndDate = x.EndDate,
                       }).ToList();
                }


                var Data = new
                {
                    ActiveDeals = ActiveDeals,
                    ActiveCities = _Cities.Count,
                    ActiveMerchant = Core.CoreMadDeals._CMerchants.Count(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate) > 0),
                    ActiveStores = Core.CoreMadDeals._CStores.Count(x => CoreMadDeals._CDeals.Count(a => a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate) > 0),
                    Cities = _Cities,
                    Merchants = Core.CoreMadDeals._CMerchants
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate) > 0)
                    .Select(x => new OMadDealsNG.Account
                    {
                        Name = x.DisplayName,
                        ReferenceId = x.ReferenceId,
                        ReferenceKey = x.ReferenceKey,
                        CityName = x.CityName,
                        DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate)
                    }).OrderBy(x => x.CityName)
                    .ToList(),
                    Stores = Core.CoreMadDeals._CStores
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate) > 0)
                     .Select(x => new OMadDealsNG.Account
                     {
                         Name = x.DisplayName,
                         ReferenceId = x.ReferenceId,
                         ReferenceKey = x.ReferenceKey,
                         CityName = x.CityName,
                         DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate)
                     }).OrderBy(x => x.CityName)
                    .ToList(),
                    Deals = Core.CoreMadDeals._CDeals.Where(a => ActiveTime >= a.StartDate && ActiveTime <= a.EndDate).OrderBy(x => x.CityName).ToList(),
                };
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Cities.Count, Data, 0, _Cities.Count);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the search states.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSearchStates(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OMadDealsNG.Location> _States = _HCoreContext.HCCoreCountryState
                        .Where(x => x.CountryId == 1 && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OMadDealsNG.Location
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            SystemName = x.Name,
                            Deals = 0
                        })
                        .OrderBy(x => x.Name)
                        .ToList();
                    foreach (var _State in _States)
                    {
                        _State.Deals = Core.CoreMadDeals._CDeals.Where(z => z.StateId == _State.ReferenceId).Count();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_States.Count, _States, 0, _States.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSearchStates", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the search cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSearchCities(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OMadDealsNG.Location> _Result = _HCoreContext.HCCoreCountryStateCity
                        .Where(x => x.StateId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OMadDealsNG.Location
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            SystemName = x.Name,
                            Deals = 0,
                        })
                        .OrderBy(x => x.Name)
                        .ToList();
                    foreach (var Item in _Result)
                    {
                        Item.Deals = Core.CoreMadDeals._CDeals.Count(a => a.CityId == Item.ReferenceId && a.StateId == _Request.ReferenceId);
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Result.Count, _Result, 0, _Result.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSearchCities", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCities(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                var _Cities = Core.CoreMadDeals._CCities.Where(x => CoreMadDeals._CDeals.Count(a => a.CityName == x.Name) > 0).OrderBy(x => x.Name).Select(x => new OMadDealsNG.Location
                {
                    ReferenceId = x.ReferenceId,
                    ReferenceKey = x.ReferenceKey,
                    Name = x.Name,
                    Deals = Core.CoreMadDeals._CDeals.Count(a => a.CityName == x.Name),
                }).ToList();
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Cities.Count, _Cities, 0, _Cities.Count);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCities", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                //DateTime ActiveTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                if (!string.IsNullOrEmpty(_Request.Type))
                {
                    var _Merchants = Core.CoreMadDeals._CMerchants
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && x.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && x.CountryId == _Request.CountryId) > 0)
                    .OrderBy(x => x.DisplayName)
                    .Select(x => new OMadDealsNG.Category
                    {
                        ReferenceId = x.ReferenceId,
                        ReferenceKey = x.ReferenceKey,
                        Name = x.DisplayName,
                        IconUrl = x.IconUrl,
                        Deals = Core.CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && x.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && x.CountryId == _Request.CountryId),
                    }).ToList();
                    foreach (var _Merchant in _Merchants)
                    {
                        _Merchant.CategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.IsFlashDeal == false && x.CountryId == _Request.CountryId)
                            .GroupBy(x => x.CategoryKey)
                            .Select(x => new
                            {
                                CategoryKey = x.Key,
                                Count = x.Count(),
                            }).ToList();
                        _Merchant.SubCategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.IsFlashDeal == false && x.CountryId == _Request.CountryId)
                           .GroupBy(x => x.SubCategoryKey)
                           .Select(x => new
                           {
                               CategoryKey = x.Key,
                               Count = x.Count(),
                           }).ToList();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Merchants.Count, _Merchants, 0, _Merchants.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
                else
                {
                    var _Merchants = Core.CoreMadDeals._CMerchants
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && x.CountryId == _Request.CountryId) > 0)
                    .OrderBy(x => x.DisplayName)
                    .Select(x => new OMadDealsNG.Category
                    {
                        ReferenceId = x.ReferenceId,
                        ReferenceKey = x.ReferenceKey,
                        Name = x.DisplayName,
                        IconUrl = x.IconUrl,
                        Deals = Core.CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && x.CountryId == _Request.CountryId),
                    }).ToList();
                    foreach (var _Merchant in _Merchants)
                    {
                        _Merchant.CategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.IsFlashDeal == false && x.CountryId == _Request.CountryId)
                            .GroupBy(x => x.CategoryKey)
                            .Select(x => new
                            {
                                CategoryKey = x.Key,
                                Count = x.Count(),
                            }).ToList();
                        _Merchant.SubCategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.IsFlashDeal == false && x.CountryId == _Request.CountryId)
                          .GroupBy(x => x.SubCategoryKey)
                          .Select(x => new
                          {
                              CategoryKey = x.Key,
                              Count = x.Count(),
                          }).ToList();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Merchants.Count, _Merchants, 0, _Merchants.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// Gets the categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategories(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                //DateTime ActiveTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                if (!string.IsNullOrEmpty(_Request.Type))
                {
                    var _Categories = Core.CoreMadDeals._CCategories
                        .OrderBy(x => x.Name)
                        .Distinct()
                        .Select(x => new OMadDealsNG.Category
                        {
                            ReferenceId = x.ReferenceId,
                            ReferenceKey = x.ReferenceKey,
                            Name = x.Name,
                            IconUrl = x.IconUrl,
                            Deals = x.ParentCategoryId == null ? Core.CoreMadDeals._CDeals.Count(a => a.CategoryId == x.ReferenceId && a.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && a.CountryId == _Request.CountryId) :
                            Core.CoreMadDeals._CDeals.Count(a => a.SubCategoryId == x.ReferenceId && a.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && a.CountryId == _Request.CountryId),
                            ParentCategoryId = x.ParentCategoryId,
                        }).ToList();
                    var LIst = _Categories.Where(x => x.ParentCategoryId == 1).ToList();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(LIst.Count, LIst, 0, LIst.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
                else
                {
                    var ParentCategories = Core.CoreMadDeals._CCategories.Where(x => x.ParentCategoryId == null)
                        .Select(x => new OMadDealsNG.Category
                        {
                            ReferenceId = x.ReferenceId,
                            ReferenceKey = x.ReferenceKey,
                            Name = x.Name,
                            IconUrl = x.IconUrl,
                        })
                        .ToList();
                    foreach (var ParentCategory in ParentCategories)
                    {
                        ParentCategory.SubCategories = Core.CoreMadDeals._CCategories.Where(x => x.ParentCategoryId == ParentCategory.ReferenceId)
                        .Select(x => new OMadDealsNG.SubCategory
                        {
                            ReferenceId = x.ReferenceId,
                            ReferenceKey = x.ReferenceKey,
                            Name = x.Name,
                            IconUrl = x.IconUrl,
                        })
                        .ToList();
                    }
                    //var _Categories = Core.CoreMadDeals._CCategories.OrderBy(x => x.Name)
                    //    .Distinct()
                    //    .Select(x => new OMadDealsNG.Categories
                    //    {
                    //        ReferenceId = x.ReferenceId,
                    //        ReferenceKey = x.ReferenceKey,
                    //        Name = x.Name,
                    //        IconUrl = x.IconUrl,
                    //        //Deals = x.ParentCategoryId == null ? Core.CoreMadDeals._CDeals.Count(a => a.CategoryId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && a.CountryId == _Request.CountryId) :
                    //        //Core.CoreMadDeals._CDeals.Count(a => a.SubCategoryId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && a.CountryId == _Request.CountryId),
                    //        ParentCategoryId = x.ParentCategoryId,
                    //    }).ToList();
                    //var LIst = _Categories.Where(x => x.ParentCategoryId == 1).ToList();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(ParentCategories.Count, ParentCategories, 0, ParentCategories.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// This api will return deals list as per users country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OMadDealsNG.Deals.List.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            if (_Request.Merchants == null)
            {
                _Request.Merchants = new List<string>();
            }
            if (_Request.Categories == null)
            {
                _Request.Categories = new List<string>();
            }
            if (_Request.SubCategories == null)
            {
                _Request.SubCategories = new List<string>();
            }
            #region Manage Exception
            try
            {
                if (_Request.Merchants != null && _Request.Merchants.Count > 0)
                {
                    _Request.Merchants = _Request.Merchants.Distinct().ToList();
                }
                if (_Request.Categories != null && _Request.Categories.Count > 0)
                {
                    _Request.Categories = _Request.Categories.Distinct().ToList();
                }
                if (_Request.SubCategories != null && _Request.SubCategories.Count > 0)
                {
                    _Request.Categories = _Request.SubCategories.Distinct().ToList();
                }
                else
                {
                    _Request.Categories = new List<string>();
                }
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                var MDDealBookmark = new List<MDDealBookmark>();
                if (_Request.UserReference != null && _Request.UserReference.AccountId > 0)
                    using (_HCoreContext = new HCoreContext())
                    {
                        MDDealBookmark = _HCoreContext.MDDealBookmark
                           .Where(x => x.AccountId == _Request.UserReference.AccountId).ToList();
                    }
                if (!string.IsNullOrEmpty(_Request.SortExpression) && _Request.SortExpression == "Flash")
                {
                    _DealsList = new List<OMadDealsNG.Deals.List.Response>();
                    if (_Request.Categories.Count > 0 && _Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.MerchantKey == Merchant && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    IsSoldout = x.IsSoldout,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).OrderByDescending(x => x.CreateDate).ToList();

                            if (CatDeals.Count > 0)
                            {
                                foreach (var Deal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            IsSoldout = Deal.IsSoldout,
                                            IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };
                                        _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                        {
                                            Url = Deal.ImageUrl
                                        });
                                        _DealsList.Add(_Item);
                                    }
                                }
                            }

                        }

                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.CategoryKey == Category && (_Request.SubCategories.Count == 0 || _Request.SubCategories.Contains(x.SubCategoryKey)) && x.IsFlashDeal == true && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                foreach (var Deal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            IsSoldout = Deal.IsSoldout,
                                            IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };
                                        _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                        {
                                            Url = Deal.ImageUrl
                                        });
                                        _DealsList.Add(_Item);
                                    }
                                }
                            }

                        }
                    }
                    else if (_Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.MerchantKey == Merchant && x.IsFlashDeal == true && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,
                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).ToList();

                            if (CatDeals.Count > 0)
                            {
                                foreach (var Deal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            IsSoldout = Deal.IsSoldout,
                                            IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };
                                        _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                        {
                                            Url = Deal.ImageUrl
                                        });
                                        _DealsList.Add(_Item);
                                    }
                                }
                            }

                        }
                    }
                    else if (_Request.Categories.Count > 0)
                    {
                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.CategoryKey == Category && x.IsFlashDeal == true && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                foreach (var Deal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            IsSoldout = Deal.IsSoldout,
                                            IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };
                                        _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                        {
                                            Url = Deal.ImageUrl
                                        });
                                        _DealsList.Add(_Item);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var CatDeals = Core.CoreMadDeals._CFlashDeals
                                .Where(x => ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).OrderByDescending(x => x.CreateDate).ToList();
                        if (CatDeals.Count > 0)
                        {
                            foreach (var Deal in CatDeals)
                            {
                                var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                if (!CheckItem)
                                {
                                    OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                    {
                                        CreateDate = Deal.CreateDate,
                                        Purchase = Deal.Purchase,

                                        ReferenceId = Deal.ReferenceId,
                                        ReferenceKey = Deal.ReferenceKey,
                                        Title = Deal.Title,

                                        ActualPrice = Deal.ActualPrice,
                                        SellingPrice = Deal.SellingPrice,

                                        IsFlashDeal = Deal.IsFlashDeal,
                                        Views = Deal.Views,

                                        IsSoldout = Deal.IsSoldout,
                                        IsBookmarked = Deal.IsBookmarked,

                                        DealTypeCode = Deal.DealTypeCode,
                                        DeliveryTypeCode = Deal.DeliveryTypeCode,

                                        Merchant = new OMadDealsNG.Deals.List.Merchant
                                        {
                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Name = Deal.MerchantName,
                                            IconUrl = Deal.MerchantIconUrl,
                                        },
                                        Category = new OMadDealsNG.Deals.List.Category
                                        {
                                            ReferenceId = Deal.CategoryId,
                                            ReferenceKey = Deal.CategoryKey,
                                            Name = Deal.CategoryName,
                                            SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                            {
                                                ReferenceId = Deal.SubCategoryId,
                                                ReferenceKey = Deal.SubCategoryName,
                                                Name = Deal.SubCategoryName,
                                            }
                                        },
                                        Shedule = new OMadDealsNG.Deals.List.Shedule
                                        {
                                            StartDate = Deal.StartDate,
                                            EndDate = Deal.EndDate,
                                        },
                                        Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                    };
                                    _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                    {
                                        Url = Deal.ImageUrl
                                    });
                                    _DealsList.Add(_Item);
                                }
                            }
                        }
                    }
                    if (_Request.MinPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice >= _Request.MinPrice).ToList();
                    }
                    if (_Request.MaxPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice <= _Request.MaxPrice).ToList();
                    }
                    if (!string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        _Request.SearchCondition = _Request.SearchCondition.ToLower();
                        var _DealsListResult = _DealsList.Where(x => x.Title != null && x.Merchant.Name != null && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.Merchant.Name.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).OrderByDescending(x => x.CreateDate).ToList();

                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        var _DealsListResult = _DealsList.Skip(_Request.Offset).Take(_Request.Limit).OrderByDescending(x => x.CreateDate).ToList();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                }
                else
                {

                    _DealsList = new List<OMadDealsNG.Deals.List.Response>();
                    if (_Request.Categories.Count > 0 && _Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CDeals.Where(x => x.MerchantKey == Merchant && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                foreach (var Deal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            IsSoldout = Deal.IsSoldout,
                                            IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };
                                        _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                        {
                                            Url = Deal.ImageUrl
                                        });
                                        _DealsList.Add(_Item);
                                    }
                                }
                            }
                        }
                        var tDeals = new List<OMadDealsNG.Deals.List.Response>();
                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = _DealsList.Where(x => x.Category.ReferenceKey == Category && (_Request.SubCategories.Count == 0 || _Request.SubCategories.Contains(x.Category.SubCategory.ReferenceKey)))
                                //.Where(x => x.IsFlashDeal == false)
                                .ToList();
                            if (CatDeals.Count > 0)
                            {
                                tDeals.AddRange(CatDeals);
                            }
                        }
                        if (tDeals.Count > 0)
                        {
                            _DealsList = tDeals;
                        }
                    }
                    else if (_Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CDeals.Where(x => x.MerchantKey == Merchant && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                foreach (var Deal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            IsSoldout = Deal.IsSoldout,
                                            IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };
                                        _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                        {
                                            Url = Deal.ImageUrl
                                        });
                                        _DealsList.Add(_Item);
                                    }
                                }
                            }
                        }
                    }
                    else if (_Request.Categories.Count > 0)
                    {
                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = Core.CoreMadDeals._CDeals.Where(x => x.CategoryKey == Category && (_Request.SubCategories.Count == 0 || _Request.SubCategories.Contains(x.SubCategoryKey)) && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,

                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                foreach (var Deal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            IsSoldout = Deal.IsSoldout,
                                            IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };
                                        _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                        {
                                            Url = Deal.ImageUrl
                                        });
                                        _DealsList.Add(_Item);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var CatDeals = Core.CoreMadDeals._CDeals
                                .Where(x => ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    IsSoldout = x.IsSoldout,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                }).ToList();
                        if (CatDeals.Count > 0)
                        {
                            foreach (var Deal in CatDeals)
                            {
                                var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                if (!CheckItem)
                                {
                                    OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                    {
                                        CreateDate = Deal.CreateDate,
                                        Purchase = Deal.Purchase,

                                        ReferenceId = Deal.ReferenceId,
                                        ReferenceKey = Deal.ReferenceKey,
                                        Title = Deal.Title,

                                        ActualPrice = Deal.ActualPrice,
                                        SellingPrice = Deal.SellingPrice,

                                        IsFlashDeal = Deal.IsFlashDeal,
                                        Views = Deal.Views,

                                        IsSoldout = Deal.IsSoldout,
                                        IsBookmarked = Deal.IsBookmarked,

                                        DealTypeCode = Deal.DealTypeCode,
                                        DeliveryTypeCode = Deal.DeliveryTypeCode,

                                        Merchant = new OMadDealsNG.Deals.List.Merchant
                                        {
                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Name = Deal.MerchantName,
                                            IconUrl = Deal.MerchantIconUrl,
                                        },
                                        Category = new OMadDealsNG.Deals.List.Category
                                        {
                                            ReferenceId = Deal.CategoryId,
                                            ReferenceKey = Deal.CategoryKey,
                                            Name = Deal.CategoryName,
                                            SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                            {
                                                ReferenceId = Deal.SubCategoryId,
                                                ReferenceKey = Deal.SubCategoryName,
                                                Name = Deal.SubCategoryName,
                                            }
                                        },
                                        Shedule = new OMadDealsNG.Deals.List.Shedule
                                        {
                                            StartDate = Deal.StartDate,
                                            EndDate = Deal.EndDate,
                                        },
                                        Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                    };
                                    _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                    {
                                        Url = Deal.ImageUrl
                                    });
                                    _DealsList.Add(_Item);
                                }
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        if (_Request.SortExpression == "Ending")
                        {
                            _DealsList = _DealsList.OrderBy(x => x.Shedule.EndDate).ToList();
                        }
                        if (_Request.SortExpression == "Discount")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.DiscountPercentage).ToList();
                        }
                        if (_Request.SortExpression == "Newest")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.CreateDate).ToList();
                        }
                        if (_Request.SortExpression == "Flash")
                        {
                            _DealsList = _DealsList.Where(x => x.IsFlashDeal == true).OrderBy(x => x.Shedule.EndDate).ToList();
                        }
                        if (_Request.SortExpression == "Amount")
                        {
                            _DealsList = _DealsList.OrderBy(x => x.SellingPrice).ToList();
                        }
                        if (_Request.SortExpression == "Total Savings")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.Saving).ToList();
                        }
                        if (_Request.SortExpression == "Popularity")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.CreateDate).ToList();
                        }
                        if (_Request.SortExpression == "BestSeller")
                        {
                            _DealsList = _DealsList.OrderBy(x => x.Purchase).ToList();
                        }
                    }
                    if (_Request.MinPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice >= _Request.MinPrice).ToList();
                    }
                    if (_Request.MaxPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice <= _Request.MaxPrice).ToList();
                    }
                    if (!string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        _Request.SearchCondition = _Request.SearchCondition.ToLower();
                        var _DealsListResult = _DealsList.Where(x => x.Title != null && x.Merchant.Name != null && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.Merchant.Name.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                        if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                        {
                            _DealsListResult = _DealsList.Where(x => x.Title != null && x.Merchant.Name != null && x.DealTypeCode == _Request.DealTypeCode && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.Merchant.Name.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                        }
                        if (_Request.Latitude != 0 && _Request.Longitude != 0)
                        {
                            var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                            _DealsListResult = _DealsListResult.Where(x => x.GeoCoordinate != null).OrderBy(x => x.GeoCoordinate.GetDistanceTo(_UserCoordinates)).ToList();
                            foreach (var Details in _DealsListResult)
                            {
                                Details.Distance = Math.Round(Details.GeoCoordinate.GetDistanceTo(_UserCoordinates) / 1000, 2);
                            }
                        }
                        else
                        {
                            var DealCities = _DealsListResult.Where(x => x.Location.CityName == _Request.City).ToList();
                            if (DealCities.Count > 0)
                            {
                                _DealsListResult = DealCities;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_DealsList.Count, _DealsListResult, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        //_DealsList = _DealsList.Where(x => x.IsFlashDeal == false).ToList();
                        if (_Request.Latitude != 0 && _Request.Longitude != 0)
                        {
                            if (_Request.SortExpression == "Popularity")
                            {
                                var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                                int TotalRecords = _DealsList.Count;
                                var _DealsListResult = _DealsList.Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                                {
                                    _DealsListResult = _DealsList.Where(x => x.Title != null && x.Merchant.Name != null && x.DealTypeCode == _Request.DealTypeCode && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.Merchant.Name.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                }
                                foreach (var Details in _DealsListResult)
                                {
                                    if (Details.GeoCoordinate != null)
                                    {
                                        Details.Distance = Math.Round(Details.GeoCoordinate.GetDistanceTo(_UserCoordinates) / 1000, 2);
                                    }
                                    else
                                    {
                                        Details.Distance = 0;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                            else
                            {
                                var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                                int TotalRecords = _DealsList.Count;
                                var _DealsListResult = _DealsList.Where(x => x.GeoCoordinate != null).OrderBy(x => x.GeoCoordinate.GetDistanceTo(_UserCoordinates)).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                                {
                                    _DealsListResult = _DealsList.Where(x => x.Title != null && x.Merchant.Name != null && x.DealTypeCode == _Request.DealTypeCode && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.Merchant.Name.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                }
                                foreach (var Details in _DealsListResult)
                                {
                                    Details.Distance = Math.Round(Details.GeoCoordinate.GetDistanceTo(_UserCoordinates) / 1000, 2);
                                }

                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                            {
                                _DealsList = _DealsList.Where(x => x.Title != null && x.Merchant.Name != null && x.DealTypeCode == _Request.DealTypeCode).ToList();
                            }

                            if (!string.IsNullOrEmpty(_Request.City))
                            {
                                int TotalRecords = _DealsList.Count(x => x.Location.CityName == _Request.City);
                                var DealCities = _DealsList.Where(x => x.Location.CityName == _Request.City).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, DealCities, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                            else
                            {
                                int TotalRecords = _DealsList.Count();
                                var AllDeals = _DealsList.Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, AllDeals, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("MadDeals-GetDeals", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the deal merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealMerchant(OMadDealsNG.Merchant.List.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                //DateTime ActiveTime = HCoreHelper.GetGMTDateTime().AddHours(1);
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                _MerchantList = new List<OMadDealsNG.Merchant.List.Response>();
                if (_Request.Categories.Count > 0)
                {
                    foreach (var Category in _Request.Categories)
                    {
                        var _Result = Core.CoreMadDeals._CMerchants.Where(x => x.CategoryKey == Category && x.CountryId == _Request.CountryId)
                            .Select(x => new OMadDealsNG.Merchant.List.Response
                            {
                                ReferenceId = x.ReferenceId,
                                ReferenceKey = x.ReferenceKey,

                                DisplayName = x.DisplayName,
                                Stores = CoreMadDeals._CStores.Count(a => a.MerchantId == x.ReferenceId),
                                Deals = CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate),

                                CategoryKey = x.CategoryKey,
                                CategoryName = x.CategoryName,


                                Address = x.Address,
                                CityAreaName = x.CityAreaName,
                                CityName = x.CityName,
                                StateName = x.StateName
                            }).ToList();
                        if (_Result.Count > 0)
                        {
                            _MerchantList.AddRange(_Result);
                        }
                    }
                }
                else
                {
                    var _Result = Core.CoreMadDeals._CMerchants
                            .Where(x => x.CountryId == _Request.CountryId)
                            .Select(x => new OMadDealsNG.Merchant.List.Response
                            {
                                ReferenceId = x.ReferenceId,
                                ReferenceKey = x.ReferenceKey,

                                DisplayName = x.DisplayName,
                                Stores = CoreMadDeals._CStores.Count(a => a.MerchantId == x.ReferenceId),
                                Deals = CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate),

                                CategoryKey = x.CategoryKey,
                                CategoryName = x.CategoryName,


                                Address = x.Address,
                                CityAreaName = x.CityAreaName,
                                CityName = x.CityName,
                                StateName = x.StateName
                            }).ToList();
                    if (_Result.Count > 0)
                    {
                        _MerchantList.AddRange(_Result);
                    }
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OMadDealsNG.Deal.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    OMadDealsNG.Deal.Response _Details = _HCoreContext.MDDeal
                        .Where(x => x.Guid == _Request.ReferenceKey)
                        .Select(x => new OMadDealsNG.Deal.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Title = x.Title,
                            TitleContent = x.TitleContent,
                            Description = x.Description,
                            Terms = x.Terms,
                            CategoryKey = x.Category.Guid,
                            CategoryName = x.Category.Name,
                            MerchantId = x.AccountId,
                            MerchantKey = x.Account.Guid,
                            MerchantIconUrl = x.Account.IconStorage.Path,
                            MerchantName = x.Account.DisplayName,
                            Latitude = x.Account.Latitude,
                            Longitude = x.Account.Longitude,

                            ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                            StartDate = x.StartDate,
                            EndDate = x.EndDate,
                            ActualPrice = x.ActualPrice,
                            SellingPrice = x.SellingPrice,
                            Saving = 0,
                            DiscountPercentage = 0,
                            Address = x.Account.Address,
                            StatusCode = x.Status.SystemName,
                            SoldCount = x.MDDealCode.Count(),
                            DealTypeCode = x.DealType.SystemName,
                            DeliveryTypeCode = x.DeliveryType.SystemName,
                        }).FirstOrDefault();
                    if (_Details != null)
                    {
                        _Details.Gallery = new List<string>();
                        _Details.Gallery.Add(_Details.ImageUrl);
                        _Details.Gallery = _HCoreContext.MDDealGallery.Where(x => x.DealId == _Details.ReferenceId)
                           .Select(x => _AppConfig.StorageUrl + x.ImageStorage.Path).ToList();

                        _Details.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _Details.MerchantId
                       && x.AccountTypeId == UserAccountType.MerchantStore
                       && x.StatusId == HelperStatus.Default.Active)
                            .Select(x => new OMadDealsNG.Deal.Location
                            {
                                Name = x.DisplayName,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                Address = x.Address,
                            }).ToList();

                        if (_Request.UserReference.AccountId > 0)
                        {
                            var IsDealpurchased = _HCoreContext.MDDealCode
                               .Where(x => x.DealId == _Details.ReferenceId && x.AccountId == _Request.UserReference.AccountId)
                               .Select(x => new
                               {
                                   ReferenceId = x.Id,
                                   ReferenceKey = x.Guid,
                               }).FirstOrDefault();
                            if (IsDealpurchased != null)
                            {
                                _Details.IsDealpurchased = true;
                                _Details.DealPurchaseId = IsDealpurchased.ReferenceId;
                                _Details.DealPurchaseKey = IsDealpurchased.ReferenceKey;
                            }
                            else
                            {
                                _Details.IsDealpurchased = false;
                            }

                            _Details.IsBookmarked = _HCoreContext.MDDealBookmark
                                .Any(x => x.DealId == _Details.ReferenceId && x.AccountId == _Request.UserReference.AccountId);
                        }


                        //_Details.Deals = Core.CoreMadDeals._CDeals
                        //    .Where(x => x.MerchantKey == _Details.MerchantKey)
                        //    .Select(x => new OMadDealsNG.Deals.List.Response
                        //    {
                        //        ReferenceId = x.ReferenceId,
                        //        ReferenceKey = x.ReferenceKey,

                        //        Title = x.Title,

                        //        MerchantKey = x.MerchantKey,
                        //        MerchantName = x.MerchantName,
                        //        MerchantIconUrl = x.MerchantIconUrl,

                        //        CategoryKey = x.CategoryKey,
                        //        CategoryName = x.CategoryName,

                        //        StartDate = x.StartDate,
                        //        EndDate = x.EndDate,

                        //        ImageUrl = x.ImageUrl,

                        //        ActualPrice = x.ActualPrice,
                        //        SellingPrice = x.SellingPrice,
                        //        Saving = x.Saving,
                        //        DiscountPercentage = x.DiscountPercentage,
                        //        CreateDate = x.CreateDate,
                        //        Address = x.Address,
                        //        CityAreaName = x.CityAreaName,
                        //        CityName = x.CityName,
                        //        StateName = x.StateName,
                        //        DealTypeCode = x.DealTypeCode,
                        //        DeliveryTypeCode = x.DeliveryTypeCode,
                        //    }).Skip(0).Take(8).ToList();
                        //if (!string.IsNullOrEmpty(_Details.MerchantIconUrl))
                        //{
                        //    _Details.MerchantIconUrl = _AppConfig.StorageUrl + _Details.MerchantIconUrl;
                        //}
                        //else
                        //{
                        //    _Details.MerchantIconUrl = _AppConfig.Default_Icon;
                        //}
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Gets the slider images.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSliderImages(OMadDealsNG.Slider.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                    _SliderImages = new List<OMadDealsNG.Slider.Data>();
                    var PromotionalItems = Core.CoreMadDeals._CDealPromotions
                       .Where(x => ActiveTime > x.StartDate && ActiveTime < x.EndDate && x.Locations != null && x.CountryId == _Request.CountryId && x.Locations.Contains("Promotional Slider"))
                       .OrderByDescending(x => x.CreateDate)
                       .Select(x => new OMadDealsNG.Slider.Data
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           Url = x.Url,
                           ImageUrl = x.ImageUrl,
                           Locations = x.Locations,
                           DealId = x.DealId,
                       })
                       .Skip(0)
                       .Take(12)
                       .ToList();
                    if (PromotionalItems.Count > 0)
                    {
                        foreach (var PromotionalItem in PromotionalItems)
                        {
                            if (PromotionalItem.DealId > 0)
                            {
                                var deal = Core.CoreMadDeals._CDeals.Where(x => x.ReferenceId == PromotionalItem.DealId && x.CountryId == _Request.CountryId).FirstOrDefault();
                                if (deal != null)
                                {
                                    PromotionalItem.DealKey = deal.ReferenceKey;
                                    PromotionalItem.Title = deal.Title;
                                    PromotionalItem.TypeCode = "deal";
                                    if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                    {
                                        PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                    }
                                    else
                                    {
                                        PromotionalItem.ImageUrl = deal.ImageUrl;
                                    }
                                }
                            }
                            else
                            {
                                PromotionalItem.TypeCode = "url";
                                if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                {
                                    PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                }
                            }
                        }
                        _SliderImages.AddRange(PromotionalItems);
                    }

                    //if (_SliderImages.Count < 6)
                    //{
                    //    int RemainingItems = 6 - _SliderImages.Count;
                    //    _SliderImages.AddRange(Core.CoreMadDeals._CDeals
                    //  .Where(x => (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.CountryId == _Request.CountryId)
                    //  .OrderBy(a => Guid.NewGuid())
                    //      .Select(x => new OMadDealsNG.Slider.Data
                    //      {
                    //          TypeCode = "deal",
                    //          ReferenceId = x.ReferenceId,
                    //          ReferenceKey = x.ReferenceKey,
                    //          DealId = x.ReferenceId,
                    //          DealKey = x.ReferenceKey,
                    //          Title = x.Title,
                    //          MerchantKey = x.MerchantKey,
                    //          MerchantName = x.MerchantName,
                    //          MerchantIconUrl = x.MerchantIconUrl,
                    //          CategoryKey = x.CategoryKey,
                    //          CategoryName = x.CategoryName,
                    //   StartDate = x.StartDate,
                    //          EndDate = x.EndDate,
                    //          ImageUrl = x.ImageUrl,
                    //          ActualPrice = x.ActualPrice,
                    //          SellingPrice = x.SellingPrice,
                    //          Saving = x.Saving,
                    //          DiscountPercentage = x.DiscountPercentage,
                    //          CreateDate = x.CreateDate,
                    //          Address = x.Address,
                    //          CityAreaName = x.CityAreaName,
                    //          CityName = x.CityName,
                    //          StateName = x.StateName
                    //      }).Skip(0).Take(RemainingItems).ToList());
                    //}


                    _SliderResponse = new OMadDealsNG.Slider.Response();
                    _SliderResponse.Location = _Request.Location;
                    _SliderResponse.Items = new List<OMadDealsNG.Slider.Item>();
                    foreach (var item in _SliderImages)
                    {
                        _SliderResponse.Items.Add(new OMadDealsNG.Slider.Item
                        {
                            TypeCode = item.TypeCode,
                            Title = item.Title,
                            DealId = item.DealId,
                            DealKey = item.DealKey,
                            ImageUrl = item.ImageUrl,
                            ReferenceId = item.ReferenceId,
                            ReferenceKey = item.ReferenceKey,
                            Url = item.Url

                        });
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SliderResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// This api returns the deals on website which are promoted from panel.
        /// As per user country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealPromotions(OMadDealsNG.Slider.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                    _DealsList = new List<OMadDealsNG.Deals.List.Response>();
                    var PromotionalItems = Core.CoreMadDeals._CDealPromotions
                       .Where(x => ActiveTime > x.StartDate && ActiveTime < x.EndDate && x.CountryId == _Request.CountryId && x.Locations.Contains(_Request.Location))
                       .OrderByDescending(x => x.CreateDate)
                       .Select(x => new OMadDealsNG.Slider.Data
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           Url = x.Url,
                           ImageUrl = x.ImageUrl,
                           Locations = x.Locations,
                           DealId = x.DealId,
                       })
                       .Skip(0)
                       .Take(12)
                       .ToList();
                    if (PromotionalItems.Count > 0)
                    {
                        foreach (var PromotionalItem in PromotionalItems)
                        {
                            if (PromotionalItem.DealId != null && PromotionalItem.DealId > 0)
                            {
                                var Deal = Core.CoreMadDeals._CDeals.Where(x => x.ReferenceId == PromotionalItem.DealId).Select(x => new
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryId = x.CategoryId,
                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryId = x.SubCategoryId,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,


                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                }).FirstOrDefault();
                                if (Deal != null)
                                {
                                    PromotionalItem.DealKey = Deal.ReferenceKey;
                                    PromotionalItem.Title = Deal.Title;
                                    PromotionalItem.TypeCode = "deal";

                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == Deal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        OMadDealsNG.Deals.List.Response _Item = new OMadDealsNG.Deals.List.Response
                                        {
                                            CreateDate = Deal.CreateDate,
                                            Purchase = Deal.Purchase,

                                            ReferenceId = Deal.ReferenceId,
                                            ReferenceKey = Deal.ReferenceKey,
                                            Title = Deal.Title,

                                            ActualPrice = Deal.ActualPrice,
                                            SellingPrice = Deal.SellingPrice,

                                            IsFlashDeal = Deal.IsFlashDeal,
                                            Views = Deal.Views,

                                            //IsSoldout = Deal.IsSoldout,
                                            //IsBookmarked = Deal.IsBookmarked,

                                            DealTypeCode = Deal.DealTypeCode,
                                            DeliveryTypeCode = Deal.DeliveryTypeCode,

                                            Merchant = new OMadDealsNG.Deals.List.Merchant
                                            {
                                                ReferenceId = Deal.ReferenceId,
                                                ReferenceKey = Deal.ReferenceKey,
                                                Name = Deal.MerchantName,
                                                IconUrl = Deal.MerchantIconUrl,
                                            },
                                            Category = new OMadDealsNG.Deals.List.Category
                                            {
                                                ReferenceId = Deal.CategoryId,
                                                ReferenceKey = Deal.CategoryKey,
                                                Name = Deal.CategoryName,
                                                SubCategory = new OMadDealsNG.Deals.List.SubCategory
                                                {
                                                    ReferenceId = Deal.SubCategoryId,
                                                    ReferenceKey = Deal.SubCategoryName,
                                                    Name = Deal.SubCategoryName,
                                                }
                                            },
                                            Shedule = new OMadDealsNG.Deals.List.Shedule
                                            {
                                                StartDate = Deal.StartDate,
                                                EndDate = Deal.EndDate,
                                            },
                                            Gallery = new List<OMadDealsNG.Deals.List.Gallery>(),
                                        };

                                        if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                        {
                                            PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                            _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                            {
                                                Url = PromotionalItem.ImageUrl
                                            });
                                        }
                                        else
                                        {
                                            _Item.Gallery.Add(new OMadDealsNG.Deals.List.Gallery
                                            {
                                                Url = Deal.ImageUrl
                                            });
                                        }
                                        _DealsList.Add(_Item);

                                    }

                                    //_DealsList.Add(deal);
                                }
                            }
                        }
                    }
                    //if (_DealsList.Count < 8)
                    //{
                    //    int RemainingItems = 8 - _DealsList.Count;
                    //    if (!string.IsNullOrEmpty(_Request.City))
                    //    {
                    //        _DealsList.AddRange(Core.CoreMadDeals._CDeals
                    //        .Where(x => (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.CountryId == _Request.CountryId && x.CityName == _Request.City)
                    //        .OrderBy(a => Guid.NewGuid())
                    //   .Select(x => new OMadDealsNG.Deals.List.Response
                    //   {
                    //       ReferenceId = x.ReferenceId,
                    //       ReferenceKey = x.ReferenceKey,

                    //       Title = x.Title,

                    //       MerchantKey = x.MerchantKey,
                    //       MerchantName = x.MerchantName,
                    //       MerchantIconUrl = x.MerchantIconUrl,

                    //       CategoryKey = x.CategoryKey,
                    //       CategoryName = x.CategoryName,

                    //   StartDate = x.StartDate,
                    //       EndDate = x.EndDate,

                    //       ImageUrl = x.ImageUrl,

                    //       ActualPrice = x.ActualPrice,
                    //       SellingPrice = x.SellingPrice,
                    //       CreateDate = x.CreateDate,
                    //       Address = x.Address,
                    //       CityAreaName = x.CityAreaName,
                    //       CityName = x.CityName,
                    //       StateName = x.StateName,
                    //       IsFlashDeal = x.IsFlashDeal,
                    //       Views = x.Views,
                    //       Purchase = x.Purchase
                    //   }).Skip(0).Take(RemainingItems).ToList());
                    //    }
                    //    RemainingItems = 8 - _DealsList.Count;
                    //    if (RemainingItems > 0)
                    //    {
                    //        _DealsList.AddRange(Core.CoreMadDeals._CDeals
                    //          .Where(x => (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.CountryId == _Request.CountryId)
                    //          .OrderBy(a => Guid.NewGuid())
                    //          .Select(x => new OMadDealsNG.Deals.List.Response
                    //          {
                    //              ReferenceId = x.ReferenceId,
                    //              ReferenceKey = x.ReferenceKey,

                    //              Title = x.Title,

                    //              MerchantKey = x.MerchantKey,
                    //              MerchantName = x.MerchantName,
                    //              MerchantIconUrl = x.MerchantIconUrl,

                    //              CategoryKey = x.CategoryKey,
                    //              CategoryName = x.CategoryName,

                    //   StartDate = x.StartDate,
                    //              EndDate = x.EndDate,

                    //              ImageUrl = x.ImageUrl,

                    //              ActualPrice = x.ActualPrice,
                    //              SellingPrice = x.SellingPrice,
                    //              CreateDate = x.CreateDate,
                    //              Address = x.Address,
                    //              CityAreaName = x.CityAreaName,
                    //              CityName = x.CityName,
                    //              StateName = x.StateName,
                    //              IsFlashDeal = x.IsFlashDeal,
                    //              Views = x.Views,
                    //              Purchase = x.Purchase
                    //          }).Skip(0).Take(RemainingItems).ToList());
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_DealsList.Count, _DealsList, 0, _DealsList.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
    }
}

