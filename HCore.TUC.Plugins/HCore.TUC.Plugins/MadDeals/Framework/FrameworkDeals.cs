//==================================================================================
// FileName: FrameworkDeals.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related deals
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Core;
using HCore.TUC.Plugins.MadDeals.Object;
using HCore.TUC.Plugins.Deals.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using GeoCoordinatePortable;

namespace HCore.TUC.Plugins.MadDeals.Framework
{
    public class FrameworkDeals
    {

        List<OMadDeals.Deals.List.Response>? _DealsList;
        List<OMadDeals.Merchant.List.Response>? _MerchantList;
        OMadDeals.Deal.Response? _DetalDetails;
        HCoreContext? _HCoreContext;
        MDDealReview? _MDDealReview;
        OMadDeals.Slider.Response? _SliderResponse;
        List<OMadDeals.Slider.Data>? _SliderImages;
        List<OMadDeals.Banner.Data>? _BannerImages;
        OMadDeals.Banner.Response? _BannerResponse;

        /// <summary>
        /// Description: Gets the stats.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStats(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                string? CountryTimeZone = string.Empty;
                using (_HCoreContext = new HCoreContext())
                {
                    CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                    _HCoreContext.Dispose();
                }


                
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                long ActiveDeals = Core.CoreMadDeals._CDeals.Count(a => ActiveTime >= a.StartDate && ActiveTime <= a.EndDate);
                var _Cities = Core.CoreMadDeals._CCities.Where(x => Core.CoreMadDeals._CDeals.Count(a => a.CityName == x.Name && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId) > 0).OrderBy(x => x.Name).Select(x => new OMadDeals.Stats
                {
                    ReferenceId = x.ReferenceId,
                    ReferenceKey = x.ReferenceKey,
                    Name = x.Name,
                    DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.CityName == x.Name && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId),
                    MerchantsCount = Core.CoreMadDeals._CMerchants.Count(a => a.CityName == x.Name && a.CountryId == _Request.CountryId),
                    StoresCount = Core.CoreMadDeals._CStores.Count(a => a.CityName == x.Name && a.CountryId == _Request.CountryId),
                }).ToList();
                foreach (var _City in _Cities)
                {
                    _City.Merchants = Core.CoreMadDeals._CMerchants
                        .Where(a => a.CityName == _City.Name)
                        .Select(x => new OMadDeals.Account
                        {
                            ReferenceId = x.ReferenceId,
                            ReferenceKey = x.ReferenceKey,
                            Name = x.DisplayName,
                            DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.CityName == _City.Name && a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId)
                        }).ToList();

                    _City.Stores = Core.CoreMadDeals._CStores
                       .Where(a => a.CityName == _City.Name)
                       .Select(x => new OMadDeals.Account
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           Name = x.DisplayName,
                           DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.CityName == _City.Name && a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId)
                       }).ToList();
                    _City.Deals = Core.CoreMadDeals._CDeals.Where(a => a.CityName == _City.Name && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId)
                       .Select(x => new OMadDeals.StatsDeals
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           MerchantDisplayName = x.MerchantName,
                           MerchantIconUrl = x.MerchantIconUrl,
                           Title = x.Title,
                           StartDate = x.StartDate,
                           EndDate = x.EndDate,
                       }).ToList();
                }


                var Data = new
                {
                    ActiveDeals = ActiveDeals,
                    ActiveCities = _Cities.Count,
                    ActiveMerchant = Core.CoreMadDeals._CMerchants.Count(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId) > 0),
                    ActiveStores = Core.CoreMadDeals._CStores.Count(x => CoreMadDeals._CDeals.Count(a => a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId) > 0),
                    Cities = _Cities,
                    Merchants = Core.CoreMadDeals._CMerchants
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId) > 0)
                    .Select(x => new OMadDeals.Account
                    {
                        Name = x.DisplayName,
                        ReferenceId = x.ReferenceId,
                        ReferenceKey = x.ReferenceKey,
                        CityName = x.CityName,
                        DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId)
                    }).OrderBy(x => x.CityName)
                    .ToList(),
                    Stores = Core.CoreMadDeals._CStores
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId) > 0)
                     .Select(x => new OMadDeals.Account
                     {
                         Name = x.DisplayName,
                         ReferenceId = x.ReferenceId,
                         ReferenceKey = x.ReferenceKey,
                         CityName = x.CityName,
                         DealsCount = Core.CoreMadDeals._CDeals.Count(a => a.StoreId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId)
                     }).OrderBy(x => x.CityName)
                    .ToList(),
                    Deals = Core.CoreMadDeals._CDeals.Where(a => ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId).OrderBy(x => x.CityName).ToList(),
                };
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Cities.Count, Data, 0, _Cities.Count);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the search states.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSearchStates(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OMadDeals.Location> _States = _HCoreContext.HCCoreCountryState
                        .Where(x => x.CountryId == _Request.CountryId && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OMadDeals.Location
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            SystemName = x.Name,
                            Deals = 0
                        })
                        .OrderBy(x => x.Name)
                        .ToList();
                    foreach (var _State in _States)
                    {
                        _State.Deals = Core.CoreMadDeals._CDeals.Where(z => z.StateId == _State.ReferenceId && z.CountryId == _Request.CountryId).Count();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_States.Count, _States, 0, _States.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSearchStates", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the search cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSearchCities(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OMadDeals.Location> _Result = _HCoreContext.HCCoreCountryStateCity
                        .Where(x => x.StateId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active)
                        .Select(x => new OMadDeals.Location
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            Name = x.Name,
                            SystemName = x.Name,
                            Deals = 0,
                        })
                        .OrderBy(x => x.Name)
                        .ToList();
                    foreach (var Item in _Result)
                    {
                        Item.Deals = Core.CoreMadDeals._CDeals.Count(a => a.CityId == Item.ReferenceId && a.StateId == _Request.ReferenceId);
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Result.Count, _Result, 0, _Result.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetSearchCities", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCities(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                var _Cities = Core.CoreMadDeals._CCities.Where(x => CoreMadDeals._CDeals.Count(a => a.CityName == x.Name && a.CountryId == _Request.CountryId) > 0).OrderBy(x => x.Name).Select(x => new OMadDeals.Location
                {
                    ReferenceId = x.ReferenceId,
                    ReferenceKey = x.ReferenceKey,
                    Name = x.Name,
                    Deals = Core.CoreMadDeals._CDeals.Count(a => a.CityName == x.Name),
                }).ToList();
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Cities.Count, _Cities, 0, _Cities.Count);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCities", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategories(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                string? CountryTimeZone = string.Empty;
                using (_HCoreContext = new HCoreContext())
                {
                    CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                    _HCoreContext.Dispose();
                }

                
                
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                if (!string.IsNullOrEmpty(_Request.Type))
                {
                    var _Categories = Core.CoreMadDeals._CCategories
                        .OrderBy(x => x.Name)
                        .Distinct()
                        .Select(x => new OMadDeals.Categories
                        {
                            ReferenceId = x.ReferenceId,
                            ReferenceKey = x.ReferenceKey,
                            Name = x.Name,
                            IconUrl = x.IconUrl,
                            Deals = x.ParentCategoryId == null ? Core.CoreMadDeals._CDeals.Count(a => a.CategoryId == x.ReferenceId && a.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && a.CountryId == _Request.CountryId) :
                            Core.CoreMadDeals._CDeals.Count(a => a.SubCategoryId == x.ReferenceId && a.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.IsFlashDeal == false && a.CountryId == _Request.CountryId),
                            ParentCategoryId = x.ParentCategoryId,
                        }).ToList();
                    var LIst = _Categories.Where(x => x.ParentCategoryId == 1).ToList();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Categories.Count, _Categories, 0, _Categories.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
                else
                {
                    var _Categories = Core.CoreMadDeals._CCategories.OrderBy(x => x.Name)
                        .Distinct()
                        .Select(x => new OMadDeals.Categories
                        {
                            ReferenceId = x.ReferenceId,
                            ReferenceKey = x.ReferenceKey,
                            Name = x.Name,
                            IconUrl = x.IconUrl,
                            Deals = x.ParentCategoryId == null ? Core.CoreMadDeals._CDeals.Count(a => a.CategoryId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId) :
                            Core.CoreMadDeals._CDeals.Count(a => a.SubCategoryId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId),
                            ParentCategoryId = x.ParentCategoryId,
                        }).ToList();
                    var LIst = _Categories.Where(x => x.ParentCategoryId == 1).ToList();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Categories.Count, _Categories, 0, _Categories.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetCategories", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchant(OList.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                string? CountryTimeZone = string.Empty;
                using (_HCoreContext = new HCoreContext())
                {
                    CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                    _HCoreContext.Dispose();
                }


                
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                if (!string.IsNullOrEmpty(_Request.Type))
                {
                    var _Merchants = Core.CoreMadDeals._CMerchants
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && x.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && x.CountryId == _Request.CountryId) > 0)
                    .OrderBy(x => x.DisplayName)
                    .Select(x => new OMadDeals.Categories
                    {
                        ReferenceId = x.ReferenceId,
                        ReferenceKey = x.ReferenceKey,
                        Name = x.DisplayName,
                        IconUrl = x.IconUrl,
                        Deals = Core.CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && x.CityName == _Request.Type && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && x.CountryId == _Request.CountryId),
                    }).ToList();
                    foreach (var _Merchant in _Merchants)
                    {
                        _Merchant.CategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                            .GroupBy(x => x.CategoryKey)
                            .Select(x => new
                            {
                                CategoryKey = x.Key,
                                Count = x.Count(),
                            }).ToList();
                        _Merchant.SubCategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                           .GroupBy(x => x.SubCategoryKey)
                           .Select(x => new
                           {
                               CategoryKey = x.Key,
                               Count = x.Count(),
                           }).ToList();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Merchants.Count, _Merchants, 0, _Merchants.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
                else
                {
                    var _Merchants = Core.CoreMadDeals._CMerchants
                    .Where(x => CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && x.CountryId == _Request.CountryId) > 0)
                    .OrderBy(x => x.DisplayName)
                    .Select(x => new OMadDeals.Categories
                    {
                        ReferenceId = x.ReferenceId,
                        ReferenceKey = x.ReferenceKey,
                        Name = x.DisplayName,
                        IconUrl = x.IconUrl,
                        Deals = Core.CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && x.CountryId == _Request.CountryId),
                    }).ToList();
                    foreach (var _Merchant in _Merchants)
                    {
                        _Merchant.CategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                            .GroupBy(x => x.CategoryKey)
                            .Select(x => new
                            {
                                CategoryKey = x.Key,
                                Count = x.Count(),
                            }).ToList();
                        _Merchant.SubCategoryDistribution = Core.CoreMadDeals._CDeals.Where(x => x.MerchantId == _Merchant.ReferenceId && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                          .GroupBy(x => x.SubCategoryKey)
                          .Select(x => new
                          {
                              CategoryKey = x.Key,
                              Count = x.Count(),
                          }).ToList();
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Merchants.Count, _Merchants, 0, _Merchants.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetMerchants", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: This api will return deals list as per users country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OMadDeals.Deals.List.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                if (_Request.Merchants != null && _Request.Merchants.Count > 0)
                {
                    _Request.Merchants = _Request.Merchants.Distinct().ToList();
                }
                if (_Request.Categories != null && _Request.Categories.Count > 0)
                {
                    _Request.Categories = _Request.Categories.Distinct().ToList();
                }
                if (_Request.SubCategories != null && _Request.SubCategories.Count > 0)
                {
                    _Request.SubCategories = _Request.SubCategories.Distinct().ToList();
                }
                else
                {
                    _Request.SubCategories = new List<string>();
                }

                string? CountryTimeZone = string.Empty;
                using (_HCoreContext = new HCoreContext())
                {
                    CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                    _HCoreContext.Dispose();
                }


                
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                var MDDealBookmark = new List<MDDealBookmark>();    
                if (_Request.UserReference != null && _Request.UserReference.AccountId > 0)
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        MDDealBookmark = _HCoreContext.MDDealBookmark.Where(x => x.AccountId == _Request.UserReference.AccountId).ToList();
                        _HCoreContext.Dispose();
                    }
                }

                if (!string.IsNullOrEmpty(_Request.SortExpression) && _Request.SortExpression == "Flash")
                {
                    _DealsList = new List<OMadDeals.Deals.List.Response>();
                    if (_Request.Categories.Count > 0 && _Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.MerchantKey == Merchant && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    IsSoldout = x.IsSoldout,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).OrderByDescending(x => x.CreateDate).ToList();
                            if (CatDeals.Count > 0)
                            {
                                foreach (var CatDeal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == CatDeal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        _DealsList.Add(CatDeal);
                                    }
                                }
                            }
                        }

                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.CategoryKey == Category && (_Request.SubCategories.Count == 0 || _Request.SubCategories.Contains(x.SubCategoryKey)) && x.IsFlashDeal == true && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                _DealsList.AddRange(CatDeals);
                            }
                        }
                    }
                    else if (_Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.MerchantKey == Merchant && x.IsFlashDeal == true && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                _DealsList.AddRange(CatDeals);
                            }
                        }
                    }
                    else if (_Request.Categories.Count > 0)
                    {
                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = Core.CoreMadDeals._CFlashDeals.Where(x => x.CategoryKey == Category && (_Request.SubCategories.Count == 0 || _Request.SubCategories.Contains(x.SubCategoryKey)) && x.IsFlashDeal == true && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                _DealsList.AddRange(CatDeals);
                            }
                        }
                    }
                    else
                    {
                        var CatDeals = Core.CoreMadDeals._CFlashDeals
                                .Where(x => ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).OrderByDescending(x => x.CreateDate).ToList();
                        if (CatDeals.Count > 0)
                        {
                            _DealsList.AddRange(CatDeals);
                        }
                    }
                    if (_Request.MinPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice >= _Request.MinPrice).ToList();
                    }
                    if (_Request.MaxPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice <= _Request.MaxPrice).ToList();
                    }
                    if (!string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        _Request.SearchCondition = _Request.SearchCondition.ToLower();
                        var _DealsListResult = _DealsList.Where(x => x.Title != null && x.MerchantName != null && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.MerchantName.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).OrderByDescending(x => x.CreateDate).ToList();

                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        var _DealsListResult = _DealsList.Skip(_Request.Offset).Take(_Request.Limit).OrderByDescending(x => x.CreateDate).ToList();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                }
                else
                {

                    _DealsList = new List<OMadDeals.Deals.List.Response>();
                    if (_Request.Categories.Count > 0 && _Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CDeals.Where(x => x.MerchantKey == Merchant && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                foreach (var CatDeal in CatDeals)
                                {
                                    var CheckItem = _DealsList.Any(x => x.ReferenceId == CatDeal.ReferenceId);
                                    if (!CheckItem)
                                    {
                                        _DealsList.Add(CatDeal);
                                    }
                                }
                            }
                        }
                        var tDeals = new List<OMadDeals.Deals.List.Response>();
                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = _DealsList.Where(x => x.CategoryKey == Category && (_Request.SubCategories.Count == 0 || _Request.SubCategories.Contains(x.SubCategoryKey)))
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                tDeals.AddRange(CatDeals);
                            }
                        }
                        if (tDeals.Count > 0)
                        {
                            _DealsList = tDeals;
                        }
                    }
                    else if (_Request.Merchants.Count > 0)
                    {
                        foreach (var Merchant in _Request.Merchants)
                        {
                            var CatDeals = Core.CoreMadDeals._CDeals.Where(x => x.MerchantKey == Merchant && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                _DealsList.AddRange(CatDeals);
                            }
                        }
                    }
                    else if (_Request.Categories.Count > 0)
                    {
                        foreach (var Category in _Request.Categories)
                        {
                            var CatDeals = Core.CoreMadDeals._CDeals.Where(x => x.CategoryKey == Category && (_Request.SubCategories.Count == 0 || _Request.SubCategories.Contains(x.SubCategoryKey)) && ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsSoldout = x.IsSoldout,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                            if (CatDeals.Count > 0)
                            {
                                _DealsList.AddRange(CatDeals);
                            }
                        }
                    }
                    else
                    {
                        var CatDeals = Core.CoreMadDeals._CDeals
                                .Where(x => ActiveTime >= x.StartDate && ActiveTime <= x.EndDate && x.CountryId == _Request.CountryId)
                                //.Where(x => x.IsFlashDeal == false)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantId = (long)x.MerchantId,
                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,
                                    SubCategoryKey = x.SubCategoryKey,
                                    SubCategoryName = x.SubCategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    GeoCoordinate = x.GeoCoordinate,
                                    IsSoldout = x.IsSoldout,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    IsBookmarked = MDDealBookmark.Any(b => b.DealId == x.ReferenceId),
                                    Slug = x.Slug,

                                    DealCodeValidityTypeId = x.DealCodeValidityTypeId,
                                    DealCodeValidityTypeCode = x.DealCodeValidityTypeCode,
                                    DealCodeValidityTypeName = x.DealCodeValidityTypeName,
                                    DealCodeValidityDays = x.DealCodeValidityDays,
                                    DealCodeValidityEndDate = x.DealCodeValidityEndDate,

                                    AverageRatings = x.AverageRatings,
                                    TotalReviews = x.TotalReviews,
                                }).ToList();
                        if (CatDeals.Count > 0)
                        {
                            _DealsList.AddRange(CatDeals);
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.SortExpression))
                    {
                        if (_Request.SortExpression == "Ending")
                        {
                            _DealsList = _DealsList.OrderBy(x => x.EndDate).ToList();
                        }
                        if (_Request.SortExpression == "Discount")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.DiscountPercentage).ToList();
                        }
                        if (_Request.SortExpression == "Newest")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.CreateDate).ToList();
                        }
                        if (_Request.SortExpression == "Flash")
                        {
                            _DealsList = _DealsList.Where(x => x.IsFlashDeal == true).OrderBy(x => x.EndDate).ToList();
                        }
                        if (_Request.SortExpression == "Amount")
                        {
                            _DealsList = _DealsList.OrderBy(x => x.SellingPrice).ToList();
                        }
                        if (_Request.SortExpression == "Total Savings")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.Saving).ToList();
                        }
                        if (_Request.SortExpression == "Popularity")
                        {
                            _DealsList = _DealsList.OrderByDescending(x => x.CreateDate).ToList();
                        }
                        if (_Request.SortExpression == "BestSeller")
                        {
                            _DealsList = _DealsList.OrderBy(x => x.Purchase).ToList();
                        }
                    }
                    if (_Request.MinPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice >= _Request.MinPrice).ToList();
                    }
                    if (_Request.MaxPrice > 0)
                    {
                        _DealsList = _DealsList.Where(x => x.SellingPrice <= _Request.MaxPrice).ToList();
                    }
                    if (!string.IsNullOrEmpty(_Request.SearchCondition))
                    {
                        _Request.SearchCondition = _Request.SearchCondition.ToLower();
                        var _DealsListResult = _DealsList.Where(x => x.Title != null && x.MerchantName != null && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.MerchantName.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                        if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                        {
                            _DealsListResult = _DealsList.Where(x => x.Title != null && x.MerchantName != null && x.DealTypeCode == _Request.DealTypeCode && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.MerchantName.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                        }
                        if (_Request.Latitude != 0 && _Request.Longitude != 0)
                        {
                            var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                            _DealsListResult = _DealsListResult.Where(x => x.GeoCoordinate != null).OrderBy(x => x.GeoCoordinate.GetDistanceTo(_UserCoordinates)).ToList();
                            foreach (var Details in _DealsListResult)
                            {
                                Details.Distance = Math.Round(Details.GeoCoordinate.GetDistanceTo(_UserCoordinates) / 1000, 2);
                            }
                        }
                        else
                        {
                            var DealCities = _DealsListResult.Where(x => x.CityName == _Request.City).ToList();
                            if (DealCities.Count > 0)
                            {
                                _DealsListResult = DealCities;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_DealsList.Count, _DealsListResult, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                    }
                    else
                    {
                        //_DealsList = _DealsList.Where(x => x.IsFlashDeal == false).ToList();
                        if (_Request.Latitude != 0 && _Request.Longitude != 0)
                        {
                            if (_Request.SortExpression == "Popularity")
                            {
                                var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                                int TotalRecords = _DealsList.Count;
                                var _DealsListResult = _DealsList.Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                                {
                                    _DealsListResult = _DealsList.Where(x => x.Title != null && x.MerchantName != null && x.DealTypeCode == _Request.DealTypeCode && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.MerchantName.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                }
                                foreach (var Details in _DealsListResult)
                                {
                                    if (Details.GeoCoordinate != null)
                                    {
                                        Details.Distance = Math.Round(Details.GeoCoordinate.GetDistanceTo(_UserCoordinates) / 1000, 2);
                                    }
                                    else
                                    {
                                        Details.Distance = 0;
                                    }
                                }
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                            else
                            {
                                var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                                int TotalRecords = _DealsList.Count;
                                var _DealsListResult = _DealsList.Where(x => x.GeoCoordinate != null).OrderBy(x => x.GeoCoordinate.GetDistanceTo(_UserCoordinates)).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                                {
                                    _DealsListResult = _DealsList.Where(x => x.Title != null && x.MerchantName != null && x.DealTypeCode == _Request.DealTypeCode && (x.Title.ToLower().Contains(_Request.SearchCondition) || x.MerchantName.ToLower().Contains(_Request.SearchCondition))).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                }
                                foreach (var Details in _DealsListResult)
                                {
                                    Details.Distance = Math.Round(Details.GeoCoordinate.GetDistanceTo(_UserCoordinates) / 1000, 2);
                                }

                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, _DealsListResult, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(_Request.DealTypeCode))
                            {
                                _DealsList = _DealsList.Where(x => x.Title != null && x.MerchantName != null && x.DealTypeCode == _Request.DealTypeCode).ToList();
                            }

                            if (!string.IsNullOrEmpty(_Request.City))
                            {
                                int TotalRecords = _DealsList.Count(x => x.CityName == _Request.City);
                                var DealCities = _DealsList.Where(x => x.CityName == _Request.City).Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, DealCities, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                            else
                            {
                                int TotalRecords = _DealsList.Count();
                                var AllDeals = _DealsList.Skip(_Request.Offset).Take(_Request.Limit).ToList();
                                OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, AllDeals, _Request.Offset, _Request.Limit);
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("MadDeals-GetDeals", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealMerchant(OMadDeals.Merchant.List.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                string? CountryTimeZone = string.Empty;
                using (_HCoreContext = new HCoreContext())
                {
                    CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                    _HCoreContext.Dispose();
                }


                
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                _MerchantList = new List<OMadDeals.Merchant.List.Response>();
                if (_Request.Categories.Count > 0)
                {
                    foreach (var Category in _Request.Categories)
                    {
                        var _Result = Core.CoreMadDeals._CMerchants.Where(x => x.CategoryKey == Category && x.CountryId == _Request.CountryId)
                            .Select(x => new OMadDeals.Merchant.List.Response
                            {
                                ReferenceId = x.ReferenceId,
                                ReferenceKey = x.ReferenceKey,

                                DisplayName = x.DisplayName,
                                Stores = CoreMadDeals._CStores.Count(a => a.MerchantId == x.ReferenceId && a.CountryId == _Request.CountryId),
                                Deals = CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId),

                                CategoryKey = x.CategoryKey,
                                CategoryName = x.CategoryName,


                                Address = x.Address,
                                CityAreaName = x.CityAreaName,
                                CityName = x.CityName,
                                StateName = x.StateName
                            }).ToList();
                        if (_Result.Count > 0)
                        {
                            _MerchantList.AddRange(_Result);
                        }
                    }
                }
                else
                {
                    var _Result = Core.CoreMadDeals._CMerchants
                            .Where(x => x.CountryId == _Request.CountryId)
                            .Select(x => new OMadDeals.Merchant.List.Response
                            {
                                ReferenceId = x.ReferenceId,
                                ReferenceKey = x.ReferenceKey,

                                DisplayName = x.DisplayName,
                                Stores = CoreMadDeals._CStores.Count(a => a.MerchantId == x.ReferenceId && a.CountryId == _Request.CountryId),
                                Deals = CoreMadDeals._CDeals.Count(a => a.MerchantId == x.ReferenceId && ActiveTime >= a.StartDate && ActiveTime <= a.EndDate && a.CountryId == _Request.CountryId),

                                CategoryKey = x.CategoryKey,
                                CategoryName = x.CategoryName,


                                Address = x.Address,
                                CityAreaName = x.CityAreaName,
                                CityName = x.CityName,
                                StateName = x.StateName
                            }).ToList();
                    if (_Result.Count > 0)
                    {
                        _MerchantList.AddRange(_Result);
                    }
                }
                OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _DealsList, _Request.Offset, _Request.Limit);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, _OResponse, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDeal(OMadDeals.Deal.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }

            #region Manage Exception
            try
            {
                DateTime currentdatetime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    if(!string.IsNullOrEmpty(_Request.Slug))
                    {
                        OMadDeals.Deal.Response? _Details = _HCoreContext.MDDeal
                            .Where(x => x.Slug == _Request.Slug && x.Account.CountryId == _Request.CountryId)
                            .Select(x => new OMadDeals.Deal.Response
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Title = x.Title,
                                TitleContent = x.TitleContent,
                                Description = x.Description,
                                Terms = x.Terms,
                                CategoryKey = x.Category.Guid,
                                CategoryName = x.Category.Name,
                                MerchantId = x.AccountId,
                                MerchantKey = x.Account.Guid,
                                MerchantIconUrl = x.Account.IconStorage.Path,
                                MerchantName = x.Account.DisplayName,
                                Latitude = x.Account.Latitude,
                                Longitude = x.Account.Longitude,

                                ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                ActualPrice = x.ActualPrice,
                                SellingPrice = x.SellingPrice,
                                Saving = 0,
                                DiscountPercentage = 0,
                                Address = x.Account.Address,
                                StatusCode = x.Status.SystemName,
                                SoldCount = (long)x.MDDealCode.Sum(a => a.ItemCount),
                                DealTypeCode = x.DealType.SystemName,
                                DeliveryTypeCode = x.DeliveryType.SystemName,
                                Slug = x.Slug,
                                MaximumUnitSalePerPerson = (int?)x.MaximumUnitSalePerPerson,
                                MaximumUnitSale = (int?)x.MaximumUnitSale,

                                DealCodeValidityTypeId = x.UsageTypeId,
                                DealCodeValidityTypeCode = x.UsageType.SystemName,
                                DealCodeValidityTypeName = x.UsageType.Name,
                                DealCodeValidityDays = x.CodeValidtyDays,
                                DealCodeValidityEndDate = x.CodeValidityEndDate,

                                AverageRatings = x.MDDealReview.Average(y => y.Rating),
                                TotalReviews = x.MDDealReview.Count(),
                            }).FirstOrDefault();

                        if (_Details.EndDate < currentdatetime)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                        }
                        if (_Details != null)
                        {
                            _Details.Gallery = new List<string>();
                            _Details.Gallery.Add(_Details.ImageUrl);
                            _Details.Gallery = _HCoreContext.MDDealGallery.Where(x => x.DealId == _Details.ReferenceId)
                               .Select(x => _AppConfig.StorageUrl + x.ImageStorage.Path).ToList();

                            _Details.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _Details.MerchantId
                           && x.AccountTypeId == UserAccountType.MerchantStore
                           && x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new OMadDeals.Deal.Location
                                {
                                    Name = x.DisplayName,
                                    Latitude = x.Latitude,
                                    Longitude = x.Longitude,
                                    Address = x.Address,
                                }).ToList();

                            if (_Request.UserReference.AccountId > 0)
                            {
                                var IsDealpurchased = _HCoreContext.MDDealCode
                                   .Where(x => x.DealId == _Details.ReferenceId && x.AccountId == _Request.UserReference.AccountId)
                                   .Select(x => new
                                   {
                                       ReferenceId = x.Id,
                                       ReferenceKey = x.Guid,
                                   }).FirstOrDefault();
                                if (IsDealpurchased != null)
                                {
                                    _Details.IsDealpurchased = true;
                                    _Details.DealPurchaseId = IsDealpurchased.ReferenceId;
                                    _Details.DealPurchaseKey = IsDealpurchased.ReferenceKey;
                                }
                                else
                                {
                                    _Details.IsDealpurchased = false;
                                }

                                _Details.AvailableQuantityPerPerson = (int?)(_Details.MaximumUnitSalePerPerson - _HCoreContext.MDDealCode.Where(z => z.AccountId == _Request.UserReference.AccountId).Sum(a => a.ItemCount));
                                if(_Details.AvailableQuantityPerPerson < 1)
                                {
                                    _Details.AvailableQuantityPerPerson = 0;
                                }

                                _Details.AvailableQuantity = (int?)(_Details.MaximumUnitSale - _HCoreContext.MDDealCode.Where(z => z.AccountId == _Request.UserReference.AccountId).Sum(a => a.ItemCount));
                                if (_Details.AvailableQuantity < 1)
                                {
                                    _Details.AvailableQuantity = 0;
                                    _Details.IsSoldOut = true;
                                }

                                _Details.IsBookmarked = _HCoreContext.MDDealBookmark
                                    .Any(x => x.DealId == _Details.ReferenceId && x.AccountId == _Request.UserReference.AccountId);
                            }


                            _Details.Deals = Core.CoreMadDeals._CDeals
                                .Where(x => x.MerchantKey == _Details.MerchantKey)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    Slug = x.Slug,
                                }).Skip(0).Take(8).ToList();
                            if (!string.IsNullOrEmpty(_Details.MerchantIconUrl))
                            {
                                _Details.MerchantIconUrl = _AppConfig.StorageUrl + _Details.MerchantIconUrl;
                            }
                            else
                            {
                                _Details.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HCD0200", ResponseCode.HCD0200);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                        }
                    }
                    else
                    {
                        OMadDeals.Deal.Response? _Details = _HCoreContext.MDDeal
                            .Where(x => x.Guid == _Request.ReferenceKey && x.Account.CountryId == _Request.CountryId)
                            .Select(x => new OMadDeals.Deal.Response
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,
                                Title = x.Title,
                                TitleContent = x.TitleContent,
                                Description = x.Description,
                                Terms = x.Terms,
                                CategoryKey = x.Category.Guid,
                                CategoryName = x.Category.Name,
                                MerchantId = x.AccountId,
                                MerchantKey = x.Account.Guid,
                                MerchantIconUrl = x.Account.IconStorage.Path,
                                MerchantName = x.Account.DisplayName,
                                Latitude = x.Account.Latitude,
                                Longitude = x.Account.Longitude,

                                ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                StartDate = x.StartDate,
                                EndDate = x.EndDate,
                                ActualPrice = x.ActualPrice,
                                SellingPrice = x.SellingPrice,
                                Saving = 0,
                                DiscountPercentage = 0,
                                Address = x.Account.Address,
                                StatusCode = x.Status.SystemName,
                                SoldCount = x.MDDealCode.Count(),
                                DealTypeCode = x.DealType.SystemName,
                                DeliveryTypeCode = x.DeliveryType.SystemName,
                                Slug = x.Slug,
                                MaximumUnitSalePerPerson = (int?)x.MaximumUnitSalePerPerson,
                                MaximumUnitSale = (int?)x.MaximumUnitSale,
                            }).FirstOrDefault();
                        if (_Details != null)
                        {
                            _Details.Gallery = new List<string>();
                            _Details.Gallery.Add(_Details.ImageUrl);
                            _Details.Gallery = _HCoreContext.MDDealGallery.Where(x => x.DealId == _Details.ReferenceId)
                               .Select(x => _AppConfig.StorageUrl + x.ImageStorage.Path).ToList();

                            _Details.Locations = _HCoreContext.HCUAccount.Where(x => x.OwnerId == _Details.MerchantId
                           && x.AccountTypeId == UserAccountType.MerchantStore
                           && x.StatusId == HelperStatus.Default.Active)
                                .Select(x => new OMadDeals.Deal.Location
                                {
                                    Name = x.DisplayName,
                                    Latitude = x.Latitude,
                                    Longitude = x.Longitude,
                                    Address = x.Address,
                                }).ToList();

                            if (_Request.UserReference.AccountId > 0)
                            {
                                var IsDealpurchased = _HCoreContext.MDDealCode
                                   .Where(x => x.DealId == _Details.ReferenceId && x.AccountId == _Request.UserReference.AccountId)
                                   .Select(x => new
                                   {
                                       ReferenceId = x.Id,
                                       ReferenceKey = x.Guid,
                                   }).FirstOrDefault();
                                if (IsDealpurchased != null)
                                {
                                    _Details.IsDealpurchased = true;
                                    _Details.DealPurchaseId = IsDealpurchased.ReferenceId;
                                    _Details.DealPurchaseKey = IsDealpurchased.ReferenceKey;
                                }
                                else
                                {
                                    _Details.IsDealpurchased = false;
                                }

                                _Details.AvailableQuantityPerPerson = (int?)(_Details.MaximumUnitSalePerPerson - _HCoreContext.MDDealCode.Where(z => z.AccountId == _Request.UserReference.AccountId).Sum(a => a.ItemCount));
                                if (_Details.AvailableQuantityPerPerson < 1)
                                {
                                    _Details.AvailableQuantityPerPerson = 0;
                                }

                                _Details.AvailableQuantity = (int?)(_Details.MaximumUnitSale - _HCoreContext.MDDealCode.Where(z => z.AccountId == _Request.UserReference.AccountId).Sum(a => a.ItemCount));
                                if (_Details.AvailableQuantity < 1)
                                {
                                    _Details.AvailableQuantity = 0;
                                    _Details.IsSoldOut = true;
                                }

                                _Details.IsBookmarked = _HCoreContext.MDDealBookmark
                                    .Any(x => x.DealId == _Details.ReferenceId && x.AccountId == _Request.UserReference.AccountId);
                            }


                            _Details.Deals = Core.CoreMadDeals._CDeals
                                .Where(x => x.MerchantKey == _Details.MerchantKey)
                                .Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    Saving = x.Saving,
                                    DiscountPercentage = x.DiscountPercentage,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    Slug = x.Slug,
                                }).Skip(0).Take(8).ToList();
                            if (!string.IsNullOrEmpty(_Details.MerchantIconUrl))
                            {
                                _Details.MerchantIconUrl = _AppConfig.StorageUrl + _Details.MerchantIconUrl;
                            }
                            else
                            {
                                _Details.MerchantIconUrl = _AppConfig.Default_Icon;
                            }
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HCD0200", ResponseCode.HCD0200);
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets store full data.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreRatings(OMadDeals.Deal.StoreRequest _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }

            #region Manage Exception
            try
            {
                DateTime currentdatetime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.MerchantKey.ToString()))
                    {
                        OMadDeals.Deal.RatingsResponse? _Details = _HCoreContext.MDDeal
                            .Where(x => x.AccountId  == _Request.MerchantKey)
                            .Select(x => new OMadDeals.Deal.RatingsResponse
                            {
                                ReferenceId = x.Id,                              
                                AverageRatings = x.MDDealReview.Average(y => y.Rating),
                                TotalReviews = x.MDDealReview.Count(),
                            }).FirstOrDefault();

                        
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HCD0200", ResponseCode.HCD0200);

                    }
                    else
                    {

                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);
                        
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetStoreRatings", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets store totar reviews and average ratings deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreData(OMadDeals.Deal.StoreRequest _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }

            #region Manage Exception
            try
            {
                DateTime currentdatetime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.MerchantKey.ToString()))
                    {
                        List<OMadDeals.Deal.StoreListingResponse?> _Details = _HCoreContext.MDDeal
                            .Where(x => x.AccountId == _Request.MerchantKey)
                            .Select(x => new OMadDeals.Deal.StoreListingResponse
                            {
                                ReferenceId = x.Id,
                                Date = x.CreateDate,
                                Creator = x.CreatedBy.Name,
                                Title = x.Title,
                                Description = x.Description,
                                Rating = x.MDDealReview.Where(a => a.DealId == x.Id).Select(p => p.Rating).FirstOrDefault(),
                                ImageUrl = _AppConfig.StorageUrl + x.PosterStorage.Path,
                                Review = x.MDDealReview.Where(a => a.DealId == x.Id).Select(q => q.Review).FirstOrDefault(),
                            }).ToList();


                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, "HCD0200", ResponseCode.HCD0200);

                    }
                    else
                    {

                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCD0404", ResponseCode.HCD0404);

                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetStoreData", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the slider images.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSliderImages(OMadDeals.Slider.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    string? CountryTimeZone = string.Empty;
                    using (_HCoreContext = new HCoreContext())
                    {
                        CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                        _HCoreContext.Dispose();
                    }


                    
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                    _SliderImages = new List<OMadDeals.Slider.Data>();
                    var PromotionalItems = Core.CoreMadDeals._CDealPromotions
                       .Where(x => ActiveTime > x.StartDate && ActiveTime < x.EndDate && x.Locations != null && x.CountryId == _Request.CountryId && x.Locations.Contains("Promotional Slider"))
                       .OrderByDescending(x => x.CreateDate)
                       .Select(x => new OMadDeals.Slider.Data
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           Url = x.Url,
                           ImageUrl = x.ImageUrl,
                           Locations = x.Locations,
                           DealId = x.DealId,
                           PromotionalSliderTypeCode = x.PromotionalSliderTypeCode,
                       })
                       .Skip(0)
                       .Take(12)
                       .ToList();
                    if (PromotionalItems.Count > 0)
                    {
                        foreach (var PromotionalItem in PromotionalItems)
                        {
                            if (PromotionalItem.DealId > 0)
                            {
                                var deal = Core.CoreMadDeals._CDeals.Where(x => x.ReferenceId == PromotionalItem.DealId && x.CountryId == _Request.CountryId).FirstOrDefault();
                                if (deal != null)
                                {
                                    PromotionalItem.DealKey = deal.ReferenceKey;
                                    PromotionalItem.Title = deal.Title;
                                    PromotionalItem.TypeCode = "deal";
                                    PromotionalItem.PromotionalSliderTypeCode = PromotionalItem.PromotionalSliderTypeCode;
                                    if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                    {
                                        PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                    }
                                    else
                                    {
                                        PromotionalItem.ImageUrl = deal.ImageUrl;
                                    }
                                }
                            }
                            else
                            {
                                PromotionalItem.TypeCode = "url";
                                if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                {
                                    PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                }
                            }
                        }
                        _SliderImages.AddRange(PromotionalItems);
                    }

                    //if (_SliderImages.Count < 6)
                    //{
                    //    int RemainingItems = 6 - _SliderImages.Count;
                    //    _SliderImages.AddRange(Core.CoreMadDeals._CDeals
                    //  .Where(x => (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.CountryId == _Request.CountryId)
                    //  .OrderBy(a => Guid.NewGuid())
                    //      .Select(x => new OMadDeals.Slider.Data
                    //      {
                    //          TypeCode = "deal",
                    //          ReferenceId = x.ReferenceId,
                    //          ReferenceKey = x.ReferenceKey,
                    //          DealId = x.ReferenceId,
                    //          DealKey = x.ReferenceKey,
                    //          Title = x.Title,
                    //          MerchantKey = x.MerchantKey,
                    //          MerchantName = x.MerchantName,
                    //          MerchantIconUrl = x.MerchantIconUrl,
                    //          CategoryKey = x.CategoryKey,
                    //          CategoryName = x.CategoryName,
                    //   StartDate = x.StartDate,
                    //          EndDate = x.EndDate,
                    //          ImageUrl = x.ImageUrl,
                    //          ActualPrice = x.ActualPrice,
                    //          SellingPrice = x.SellingPrice,
                    //          Saving = x.Saving,
                    //          DiscountPercentage = x.DiscountPercentage,
                    //          CreateDate = x.CreateDate,
                    //          Address = x.Address,
                    //          CityAreaName = x.CityAreaName,
                    //          CityName = x.CityName,
                    //          StateName = x.StateName
                    //      }).Skip(0).Take(RemainingItems).ToList());
                    //}


                    _SliderResponse = new OMadDeals.Slider.Response();
                    _SliderResponse.Location = _Request.Location;
                    _SliderResponse.Items = new List<OMadDeals.Slider.Item>();
                    foreach (var item in _SliderImages)
                    {
                        _SliderResponse.Items.Add(new OMadDeals.Slider.Item
                        {
                            TypeCode = item.TypeCode,
                            Title = item.Title,
                            DealId = item.DealId,
                            DealKey = item.DealKey,
                            ImageUrl = item.ImageUrl,
                            ReferenceId = item.ReferenceId,
                            ReferenceKey = item.ReferenceKey,
                            Url = item.Url,
                            Type = item.PromotionalSliderTypeCode,
                        });
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _SliderResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
        /// <summary>
        /// This api returns the deals on website which are promoted from panel.
        /// As per user country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealPromotions(OMadDeals.Slider.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    string? CountryTimeZone = string.Empty;
                    using (_HCoreContext = new HCoreContext())
                    {
                        CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                        _HCoreContext.Dispose();
                    }


                    
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                    _DealsList = new List<OMadDeals.Deals.List.Response>();
                    var PromotionalItems = Core.CoreMadDeals._CDealPromotions
                       .Where(x => ActiveTime > x.StartDate && ActiveTime < x.EndDate && x.StatusId == HelperStatus.Default.Active && x.CountryId == _Request.CountryId && x.Locations.Contains(_Request.Location))
                       .OrderByDescending(x => x.CreateDate)
                       .Select(x => new OMadDeals.Slider.Data
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           Url = x.Url,
                           ImageUrl = x.ImageUrl,
                           Locations = x.Locations,
                           DealId = x.DealId,
                       })
                       .Skip(0)
                       .Take(12)
                       .ToList();
                    if (PromotionalItems.Count > 0)
                    {
                        foreach (var PromotionalItem in PromotionalItems)
                        {
                            if (PromotionalItem.DealId != null && PromotionalItem.DealId > 0)
                            {
                                var deal = Core.CoreMadDeals._CDeals.Where(x => x.ReferenceId == PromotionalItem.DealId).Select(x => new OMadDeals.Deals.List.Response
                                {
                                    ReferenceId = x.ReferenceId,
                                    ReferenceKey = x.ReferenceKey,

                                    Title = x.Title,

                                    MerchantKey = x.MerchantKey,
                                    MerchantName = x.MerchantName,
                                    MerchantIconUrl = x.MerchantIconUrl,

                                    CategoryKey = x.CategoryKey,
                                    CategoryName = x.CategoryName,

                                    StartDate = x.StartDate,
                                    EndDate = x.EndDate,

                                    ImageUrl = x.ImageUrl,

                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    CreateDate = x.CreateDate,
                                    Address = x.Address,
                                    CityAreaName = x.CityAreaName,
                                    CityName = x.CityName,
                                    StateName = x.StateName,
                                    IsFlashDeal = x.IsFlashDeal,
                                    Views = x.Views,
                                    Purchase = x.Purchase,
                                    DealTypeCode = x.DealTypeCode,
                                    DeliveryTypeCode = x.DeliveryTypeCode,
                                    Slug = x.Slug,
                                }).FirstOrDefault();
                                if (deal != null)
                                {
                                    PromotionalItem.DealKey = deal.ReferenceKey;
                                    PromotionalItem.Title = deal.Title;
                                    PromotionalItem.TypeCode = "deal";
                                    if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                    {
                                        PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                        deal.ImageUrl = PromotionalItem.ImageUrl;
                                    }
                                    _DealsList.Add(deal);
                                }
                            }
                        }
                    }
                    //if (_DealsList.Count < 8)
                    //{
                    //    int RemainingItems = 8 - _DealsList.Count;
                    //    if (!string.IsNullOrEmpty(_Request.City))
                    //    {
                    //        _DealsList.AddRange(Core.CoreMadDeals._CDeals
                    //        .Where(x => (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.CountryId == _Request.CountryId && x.CityName == _Request.City)
                    //        .OrderBy(a => Guid.NewGuid())
                    //   .Select(x => new OMadDeals.Deals.List.Response
                    //   {
                    //       ReferenceId = x.ReferenceId,
                    //       ReferenceKey = x.ReferenceKey,

                    //       Title = x.Title,

                    //       MerchantKey = x.MerchantKey,
                    //       MerchantName = x.MerchantName,
                    //       MerchantIconUrl = x.MerchantIconUrl,

                    //       CategoryKey = x.CategoryKey,
                    //       CategoryName = x.CategoryName,

                    //   StartDate = x.StartDate,
                    //       EndDate = x.EndDate,

                    //       ImageUrl = x.ImageUrl,

                    //       ActualPrice = x.ActualPrice,
                    //       SellingPrice = x.SellingPrice,
                    //       CreateDate = x.CreateDate,
                    //       Address = x.Address,
                    //       CityAreaName = x.CityAreaName,
                    //       CityName = x.CityName,
                    //       StateName = x.StateName,
                    //       IsFlashDeal = x.IsFlashDeal,
                    //       Views = x.Views,
                    //       Purchase = x.Purchase
                    //   }).Skip(0).Take(RemainingItems).ToList());
                    //    }
                    //    RemainingItems = 8 - _DealsList.Count;
                    //    if (RemainingItems > 0)
                    //    {
                    //        _DealsList.AddRange(Core.CoreMadDeals._CDeals
                    //          .Where(x => (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate) && x.CountryId == _Request.CountryId)
                    //          .OrderBy(a => Guid.NewGuid())
                    //          .Select(x => new OMadDeals.Deals.List.Response
                    //          {
                    //              ReferenceId = x.ReferenceId,
                    //              ReferenceKey = x.ReferenceKey,

                    //              Title = x.Title,

                    //              MerchantKey = x.MerchantKey,
                    //              MerchantName = x.MerchantName,
                    //              MerchantIconUrl = x.MerchantIconUrl,

                    //              CategoryKey = x.CategoryKey,
                    //              CategoryName = x.CategoryName,

                    //   StartDate = x.StartDate,
                    //              EndDate = x.EndDate,

                    //              ImageUrl = x.ImageUrl,

                    //              ActualPrice = x.ActualPrice,
                    //              SellingPrice = x.SellingPrice,
                    //              CreateDate = x.CreateDate,
                    //              Address = x.Address,
                    //              CityAreaName = x.CityAreaName,
                    //              CityName = x.CityName,
                    //              StateName = x.StateName,
                    //              IsFlashDeal = x.IsFlashDeal,
                    //              Views = x.Views,
                    //              Purchase = x.Purchase
                    //          }).Skip(0).Take(RemainingItems).ToList());
                    //    }
                    //}
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_DealsList.Count, _DealsList, 0, _DealsList.Count);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetDeal", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }

        /// <summary>
        /// This API is to get the list of the promotional banners uploaded from the console and the merchant panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse GetBannerImages(OMadDeals.Banner.Request _Request)
        {
            if (_Request.CountryId == null || _Request.CountryId < 1)
            {
                _Request.CountryId = _Request.UserReference.CountryId;
                _Request.CountryKey = _Request.UserReference.CountryKey;
            }
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    
                        
                    string? CountryTimeZone = string.Empty;
                    using (_HCoreContext = new HCoreContext())
                    {
                        CountryTimeZone = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId && x.Guid == _Request.CountryKey).Select(x => x.TimeZoneName).FirstOrDefault();
                        _HCoreContext.Dispose();
                    }


                    
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();

                    _BannerImages = new List<OMadDeals.Banner.Data>();
                    var PromotionalItems = Core.CoreMadDeals._CDealPromotions
                       .Where(x => ActiveTime > x.StartDate && ActiveTime < x.EndDate && x.Locations != null && x.CountryId == _Request.CountryId && x.Locations.Contains("Promotional Banner"))
                       .OrderByDescending(x => x.CreateDate)
                       .Select(x => new OMadDeals.Banner.Data
                       {
                           ReferenceId = x.ReferenceId,
                           ReferenceKey = x.ReferenceKey,
                           Url = x.Url,
                           ImageUrl = x.ImageUrl,
                           Locations = x.Locations,
                           DealId = x.DealId,
                           PromotionalSliderTypeCode = x.PromotionalSliderTypeCode,
                       })
                       .Skip(0)
                       .Take(12)
                       .ToList();
                    if (PromotionalItems.Count > 0)
                    {
                        foreach (var PromotionalItem in PromotionalItems)
                        {
                            if (PromotionalItem.DealId > 0)
                            {
                                var deal = Core.CoreMadDeals._CDeals.Where(x => x.ReferenceId == PromotionalItem.DealId && x.CountryId == _Request.CountryId).FirstOrDefault();
                                if (deal != null)
                                {
                                    PromotionalItem.DealKey = deal.ReferenceKey;
                                    PromotionalItem.Title = deal.Title;
                                    PromotionalItem.DiscountPercentage = (deal.SellingPrice / deal.ActualPrice) * 100;
                                    PromotionalItem.TypeCode = "deal";
                                    PromotionalItem.PromotionalSliderTypeCode = PromotionalItem.PromotionalSliderTypeCode;
                                    if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                    {
                                        PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                    }
                                    else
                                    {
                                        PromotionalItem.ImageUrl = deal.ImageUrl;
                                    }
                                }
                            }
                            else
                            {
                                PromotionalItem.TypeCode = "url";
                                PromotionalItem.DiscountPercentage = 30; // Added hard coded value due to not able to get percentage if it is an external promotion (Promotion is created using external URL.)
                                if (!string.IsNullOrEmpty(PromotionalItem.ImageUrl))
                                {
                                    PromotionalItem.ImageUrl = _AppConfig.StorageUrl + PromotionalItem.ImageUrl;
                                }
                            }
                        }
                        _BannerImages.AddRange(PromotionalItems);
                    }


                    _BannerResponse = new OMadDeals.Banner.Response();
                    _BannerResponse.Location = _Request.Location;
                    _BannerResponse.Items = new List<OMadDeals.Banner.Item>();

                    foreach (var item in _BannerImages)
                    {
                        _BannerResponse.Items.Add(new OMadDeals.Banner.Item
                        {
                            TypeCode = item.TypeCode,
                            Title = item.Title,
                            DealId = item.DealId,
                            DealKey = item.DealKey,
                            ImageUrl = item.ImageUrl,
                            ReferenceId = item.ReferenceId,
                            ReferenceKey = item.ReferenceKey,
                            Url = item.Url,
                            Type = item.PromotionalSliderTypeCode,
                            DiscountPercentage = item.DiscountPercentage,
                        });
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BannerResponse, "HCD0200", ResponseCode.HCD0200);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetBannerImages", _Exception, _Request.UserReference, null, "HCD0500", ResponseCode.HCD0500);
            }
            #endregion
        }
    }
}
