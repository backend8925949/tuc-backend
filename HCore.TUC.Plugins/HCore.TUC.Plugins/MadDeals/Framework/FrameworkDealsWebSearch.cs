﻿using System;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Object;
using static HCore.Helper.HCoreConstant;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace HCore.TUC.Plugins.MadDeals.Framework
{
	public class FrameworkDealsWebSearch
	{
		HCoreContext? _HCoreContext;
		MDDealSearch? _MDDealSearch;

		internal async Task<OResponse> SaveSearch(ODealsWebSearch.Save.Request _Request)
		{
			try
            {
                ODealsWebSearch.Save.Response _Response = new ODealsWebSearch.Save.Response();
                if (string.IsNullOrEmpty(_Request.SearchText))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "DS003", "Search text required");
                }
				if(_Request.UserReference.AccountId > 0 && !string.IsNullOrEmpty(_Request.UserReference.AccountKey))
				{
					_Request.AccountId = _Request.UserReference.AccountId;
					_Request.AccountKey = _Request.UserReference.AccountKey;
				}

				using(_HCoreContext = new HCoreContext())
				{
					long? AccountId = await _HCoreContext.HCUAccount.Where(x => x.Id == _Request.AccountId && x.Guid == _Request.AccountKey).Select(x => x.Id).FirstOrDefaultAsync();

					var IsSearched = await _HCoreContext.MDDealSearch.AnyAsync(x => x.SearchText == _Request.SearchText);
					if(!IsSearched)
                    {
						if (AccountId > 0)
						{
							IsSearched = await _HCoreContext.MDDealSearch.AnyAsync(x => x.SearchText == _Request.SearchText && x.AccountId == _Request.AccountId);
							if (!IsSearched)
                            {

                                _MDDealSearch = new MDDealSearch();
                                _MDDealSearch.Guid = HCoreHelper.GenerateGuid();
                                if (AccountId > 0)
                                {
                                    _MDDealSearch.AccountId = AccountId;
                                }
                                _MDDealSearch.SearchText = _Request.SearchText;
                                _MDDealSearch.CreateDate = HCoreHelper.GetGMTDateTime();
                                if (AccountId > 0)
                                {
                                    _MDDealSearch.CreatedById = AccountId;
                                }
                                else
                                {
                                    _MDDealSearch.CreatedById = 1;
                                }
                                await _HCoreContext.MDDealSearch.AddAsync(_MDDealSearch);
                                await _HCoreContext.SaveChangesAsync();
                                await _HCoreContext.DisposeAsync();

                                _Response.ReferenceId = _MDDealSearch.Id;
                                _Response.ReferenceKey = _MDDealSearch.Guid;
                            }
						}
						else
                        {

                            _MDDealSearch = new MDDealSearch();
                            _MDDealSearch.Guid = HCoreHelper.GenerateGuid();
                            if (AccountId > 0)
                            {
                                _MDDealSearch.AccountId = AccountId;
                            }
                            _MDDealSearch.SearchText = _Request.SearchText;
                            _MDDealSearch.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (AccountId > 0)
                            {
                                _MDDealSearch.CreatedById = AccountId;
                            }
                            else
                            {
                                _MDDealSearch.CreatedById = 1;
                            }
                            await _HCoreContext.MDDealSearch.AddAsync(_MDDealSearch);
                            await _HCoreContext.SaveChangesAsync();
                            await _HCoreContext.DisposeAsync();

                            _Response.ReferenceId = _MDDealSearch.Id;
                            _Response.ReferenceKey = _MDDealSearch.Guid;
                        }
                    }

                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "DS0200", "Search details saved successfully");
                }
			}
			catch (Exception _Exception)
			{
				return HCoreHelper.LogException("SaveSearch", _Exception, _Request.UserReference, "DS0500", "Unable to process your request.");
			}
		}

		internal async Task<OResponse> GetSearch(OList.Request _Request)
		{
			try
			{
				if(string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                }

				using(_HCoreContext = new HCoreContext())
				{
					if(_Request.RefreshCount)
					{
						_Request.TotalRecords = await _HCoreContext.MDDealSearch
							.Select(x => new ODealsWebSearch.List.Response
							{
								ReferenceId = x.Id,
								ReferenceKey = x.Guid,
								SearchText = x.SearchText,
								AccountId = x.AccountId,
								AccountKey = x.Account.Guid,
								AccountDisplayName = x.Account.DisplayName,
								CreateDate = x.CreateDate,
								CreatedById = x.CreatedById,
								CreatedByDisplayName = x.CreatedBy.DisplayName,
							}).Where(_Request.SearchCondition)
							.CountAsync();
					}

					List<ODealsWebSearch.List.Response>? _SearchList = await _HCoreContext.MDDealSearch
						.Select(x => new ODealsWebSearch.List.Response
                        {
                            ReferenceId = x.Id,
                            ReferenceKey = x.Guid,
                            SearchText = x.SearchText,
                            AccountId = x.AccountId,
                            AccountKey = x.Account.Guid,
                            AccountDisplayName = x.Account.DisplayName,
                            CreateDate = x.CreateDate,
                            CreatedById = x.CreatedById,
                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                        }).Where(_Request.SearchCondition)
						.OrderBy(_Request.SortExpression)
						.Skip(_Request.Offset)
						.Take(_Request.Limit)
						.ToListAsync();

					OList.Response _ListResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _SearchList, _Request.Offset, _Request.Limit);
					return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ListResponse, "DS0200", "Search list loaded successfully");
				}
            }
			catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetSearch", _Exception, _Request.UserReference, "DS0500", "Unable to process your request.");
            }
		}
	}
}

