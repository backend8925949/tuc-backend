//==================================================================================
// FileName: ODealReview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.MadDeals.Object
{
    public class ODealReview
    {
        public class Save
        {
            public long DealId { get; set; }
            public string? DealKey { get; set; }
            public int Rating { get; set; }
            public string? Review { get; set; }
            public string? Comment { get; set; }
            public string? SystemComment { get; set; }
            public string? StatusCode { get; set; }
            public long AuthAccountId { get; set; }

            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? AccountIconUrl { get; set; }
            public int? Rating { get; set; }
            public int? IsWorking { get; set; }
            public string? Review { get; set; }
            public string? Comment { get; set; }
            public DateTime? CreateDate { get; set; }
        }

    }
}
