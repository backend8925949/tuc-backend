//==================================================================================
// FileName: OMadDealsStorage.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using GeoCoordinatePortable;

namespace HCore.TUC.Plugins.MadDeals.Object
{

    public class OMadDealsStorage
    {
        public class City
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
        }
        public class Categories
        {
            public int ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public int? ParentCategoryId { get; set; }
        }
        public class Merchant
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? DisplayName { get; set; }

            public long? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public string? IconUrl { get; set; }

            public string? Address { get; set; }
            public long? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }

            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }

            public long? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }
            public GeoCoordinate? GeoCoordinate { get; set; }
        }
        public class Store
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? DisplayName { get; set; }

            public long? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }


            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }

            public string? IconUrl { get; set; }

            public string? Address { get; set; }
            public long? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }

            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }

            public long? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }
            public GeoCoordinate? GeoCoordinate { get; set; }
        }

        public class Deals
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Title { get; set; }

            public long? CategoryId { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public long? SubCategoryId { get; set; }
            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName { get; set; }


            public long? MerchantId { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }
            public string? MerchantIconUrl { get; set; }

            public string? ImageUrl { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }

            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }
            public double? Saving { get; set; }
            public double? DiscountPercentage { get; set; }

            public int StoreCount { get; set; }

            public Merchant? Merchant { get; set; }
            public List<Store>? Stores { get; set; }

            public long? StoreId { get; set; }
            public string? StoreKey { get; set; }
            public string? StoreDisplayName { get; set; }
            public string? StoreAddress { get; set; }


            public string? Address { get; set; }
            public long? CountryId { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public long? StateId { get; set; }
            public string? StateKey { get; set; }
            public string? StateName { get; set; }

            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }

            public long? CityAreaId { get; set; }
            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }
            public bool IsFlashDeal { get; set; } = false;
            public long Views { get; set; } = 0;
            public long Purchase { get; set; } = 0;
            public bool IsSoldout { get; set; } = false;
            public GeoCoordinate? GeoCoordinate { get; set; }

            public double? AverageRatings { get; set; }
            public long? TotalReviews { get; set; }

            public int? DealCodeValidityTypeId { get; set; }
            public string? DealCodeValidityTypeCode { get; set; }
            public string? DealCodeValidityTypeName { get; set; }
            public long? DealCodeValidityDays { get; set; }
            public DateTime? DealCodeValidityEndDate { get; set; }

            public string? DealTypeCode { get; set; }
            public string? DeliveryTypeCode { get; set; }

            public string? Slug { get; set; }
        }

        public class DealPromotion
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Locations { get; set; }

            public long? DealId { get; set; }
            public long? CityId { get; set; }
            public string? CityKey { get; set; }
            public string? CityName { get; set; }

            public string? Url { get; set; }

            public string? PromotionalSliderTypeCode { get; set; }

            public string? ImageUrl { get; set; }

            public int? CountryId { get; set; }
            public string? CountryKey { get; set; }

            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public GeoCoordinate? GeoCoordinate { get; set; }

            public int? StatusId { get; set; }
        }



    }

}
