//==================================================================================
// FileName: ODealBookmark.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.MadDeals.Object
{
    public class ODealBookmark
    {
        public class Save
        {
            public class Request
            {
                public long AuthAccountId { get; set; }
                public long DealId { get; set; }
                public string? DealKey { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long DealId { get; set; }
            public string? DealKey { get; set; }
            public string? Title { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? MerchantKey { get; set; }
            public string? MerchantName { get; set; }
            public string? Name { get; set; }
            public string? ImageUrl { get; set; }
            public DateTime? DealEndDate { get; set; }
            public DateTime? DealCreateDate { get; set; }
            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }
            public double? Saving { get; set; }
            public double? DiscountPercentage { get; set; }
            public int StoreCount { get; set; }
            public string? Address { get; set; }
            public string? DealSlug { get; set; }
            public string? CityAreaName { get; set; }
            public string? CityName { get; set; }
            public string? StateName { get; set; }

            //public long AccountId { get; set; }
            //public string? AccountKey { get; set; }
            //public string? AccountName { get; set; }
            //public string? AccountDisplayName { get; set; }
            //public string? MobileNumber { get; set; }
            //public string? EmailAddress { get; set; }
            //public int AccountTypeId { get; set; }
            //public string? AccountTypeCode { get; set; }
            //public string? AccountTypeName { get; set; }
            //public string? AccountTypeDisplayName { get; set; }  

            public DateTime? CreateDate { get; set; }
            //public DateTime? ModifyDate { get; set; }

            //public int StatusId { get; set; }
            //public string? StatusCode { get; set; }
            //public string? StatusName { get; set; }
            //public OUserReference? UserReference { get; set; }
        }
    }
}
