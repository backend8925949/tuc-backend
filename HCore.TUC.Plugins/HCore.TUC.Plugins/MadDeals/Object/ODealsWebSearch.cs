﻿using System;
using HCore.Helper;

namespace HCore.TUC.Plugins.MadDeals.Object
{
	public class ODealsWebSearch
	{
		public class Save
		{
			public class Request
			{
				public long? AccountId { get; set; }
				public string? AccountKey { get; set; }
				public string? SearchText { get; set; }
				public OUserReference? UserReference { get; set; }
			}
			public class Response
            {
                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }
		}

		public class List
		{
			public class Response
			{
				public long? ReferenceId { get; set; }
				public string? ReferenceKey { get; set; }
				public string? SearchText { get; set; }
				public long? AccountId { get; set; }
				public string? AccountKey { get; set; }
				public string? AccountDisplayName { get; set; }
				public DateTime? CreateDate { get; set; }
				public long? CreatedById { get; set; }
				public string? CreatedByDisplayName { get; set; }
			}
		}
	}
}

