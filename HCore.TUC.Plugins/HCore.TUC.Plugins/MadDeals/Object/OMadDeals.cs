//==================================================================================
// FileName: OMadDeals.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using GeoCoordinatePortable;
using HCore.Helper;

namespace HCore.TUC.Plugins.MadDeals.Object
{
    public class OMadDeals
    {
        public class Stats
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public long DealsCount { get; set; }
            public long MerchantsCount { get; set; }
            public long StoresCount { get; set; }
            public List<Account>? Merchants { get; set; }
            public List<Account>? Stores { get; set; }
            public List<StatsDeals>? Deals { get; set; }
        }
        public class Account
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? CityName { get; set; }
            public List<Account>? Stores { get; set; }
            public List<StatsDeals>? Deals { get; set; }
            public long DealsCount { get; set; }
        }
        public class StatsDeals
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? MerchantDisplayName { get; set; }
            public string? MerchantIconUrl { get; set; }
            public string? Title { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
        }

        public class Location
        {
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public long Deals { get; set; }
        }
        public class Categories
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public long Deals { get; set; }
            public object? CategoryDistribution { get; set; }

            public object? SubCategoryDistribution { get; set; }

            public long? ParentCategoryId { get; set; }
        }
        public class Deals
        {
            public class List
            {
                public class Request
                {

                    public int? CountryId { get; set; }
                    public string? CountryKey { get; set; }

                    public long AuthAccountId { get; set; }
                    public long AccountId { get; set; }
                    public string? AccountKey { get; set; }
                    public bool RefreshCount { get; set; } = true;
                    public int Offset { get; set; }
                    public int Limit { get; set; }
                    public int TotalRecords { get; set; }
                    public string? SearchParameter { get; set; }
                    public string? SearchCondition { get; set; }
                    public string? SortExpression { get; set; } = "Newest";
                    public string? ReferenceKey { get; set; }
                    public string? SubReferenceKey { get; set; }
                    public long ReferenceId { get; set; }
                    public long SubReferenceId { get; set; }
                    public string? Type { get; set; }
                    public List<string>? Categories { get; set; }
                    public List<string>? SubCategories { get; set; }
                    public List<string>? Merchants { get; set; }

                    public double Latitude { get; set; }
                    public double Longitude { get; set; }

                    public bool IsDownload { get; set; }
                    public bool IsTucPlus { get; set; }

                    public int ListType { get; set; }
                    public long Status { get; set; }
                    public int Radius { get; set; }

                    public long TypeId { get; set; }

                    public DateTime? StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    public OUserReference? UserReference { get; set; }
                    public long? StoreReferenceId { get; set; }
                    public long? CustomerReferenceId { get; set; }

                    public string? AccountTypeCode { get; set; }

                    public string? City { get; set; }

                    public double MinPrice { get; set; }
                    public double MaxPrice { get; set; }

                    public string? DealTypeCode { get; set; }
                    public string? DeliveryTypeCode { get; set; }
                }
                public class Response
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? Title { get; set; }
                    public string? CategoryKey { get; set; }
                    public string? CategoryName { get; set; }
                    public long MerchantId { get; set; }
                    public string? MerchantIconUrl { get; set; }
                    public string? MerchantKey { get; set; }
                    public string? MerchantName { get; set; }
                    public string? Name { get; set; }
                    public string? ImageUrl { get; set; }
                    public DateTime? StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    public DateTime? CreateDate { get; set; }
                    public double? ActualPrice { get; set; }
                    public double? SellingPrice { get; set; }
                    public double? Saving { get; set; }
                    public double? DiscountPercentage { get; set; }
                    public int StoreCount { get; set; }
                    public string? Address { get; set; }

                    public string? CityAreaName { get; set; }
                    public string? CityName { get; set; }
                    public string? StateName { get; set; }
                    public bool IsFlashDeal { get; set; } = false;
                    public long Views { get; set; } = 0;
                    public long Purchase { get; set; } = 0;
                    public double? Distance { get; set; }

                    public bool IsSoldout { get; set; } = false;
                    public bool IsBookmarked { get; set; }
                    public string? SubCategoryKey { get; set; }
                    public string? SubCategoryName { get; set; }
                    internal GeoCoordinate? GeoCoordinate { get; set; }

                    public string? DealTypeCode { get; set; }
                    public string? DeliveryTypeCode { get; set; }

                    public string? Slug { get; set; }

                    public double? AverageRatings { get; set; }
                    public long? TotalReviews { get; set; }

                    public int? DealCodeValidityTypeId { get; set; }
                    public string? DealCodeValidityTypeCode { get; set; }
                    public string? DealCodeValidityTypeName { get; set; }
                    public long? DealCodeValidityDays { get; set; }
                    public DateTime? DealCodeValidityEndDate { get; set; }
                }
            }
        }
        public class Deal
        {
            public class Request
            {
                public int? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Slug { get; set; }
                public OUserReference? UserReference { get; set; }
            }

            public class StoreRequest
            {
                public int? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? ReferenceKey { get; set; }
                public long? MerchantKey { get; set; }
                public string? Slug { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Title { get; set; }
                public string? TitleContent { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public long MerchantId { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantName { get; set; }
                public string? Name { get; set; }
                public string? ImageUrl { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public double? ActualPrice { get; set; }
                public double? SellingPrice { get; set; }
                public double? Saving { get; set; }
                public double? DiscountPercentage { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }

                public bool IsBookmarked { get; set; }
                public bool IsDealpurchased { get; set; }
                public bool IsSoldOut { get; set; }
                public long DealPurchaseId { get; set; }
                public string? DealPurchaseKey { get; set; }

                //public int StoreCount { get; set; }
                //public string? Address { get; set; }

                //public string? CityAreaName { get; set; }
                //public string? CityName { get; set; }
                //public string? StateName { get; set; }
                public long SoldCount { get; set; }
                public string? Description { get; set; }
                public string? Address { get; set; }
                public string? Terms { get; set; }
                public string? StatusCode { get; set; }
                public List<string>? Gallery { get; set; }
                public List<Deals.List.Response>? Deals { get; set; }
                public List<Location>? Locations { get; set; }

                public int? AvailableQuantity { get; set; }
                public int? AvailableQuantityPerPerson { get; set; }
                public int? MaximumUnitSalePerPerson { get; set; }
                public int? MaximumUnitSale { get; set; }

                public string? DealTypeCode { get; set; }
                public string? DeliveryTypeCode { get; set; }

                public string? Slug { get; set; }

                public double? AverageRatings { get; set; }
                public long? TotalReviews { get; set; }

                public int? DealCodeValidityTypeId { get; set; }
                public string? DealCodeValidityTypeCode { get; set; }
                public string? DealCodeValidityTypeName { get; set; }
                public long? DealCodeValidityDays { get; set; }
                public DateTime? DealCodeValidityEndDate { get; set; }
            }

            public class RatingsResponse
            {
                public long ReferenceId { get; set; }

                public double? AverageRatings { get; set; }
                public long? TotalReviews { get; set; }

            }

            public class StoreListingResponse
            {
                public long? ReferenceId { get; set; }
                public DateTime? Date { get; set; }
                public string? Creator { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public long? Rating { get; set; }
                public string? ImageUrl { get; set; }
                public string? Review { get; set; }

            }

            public class StoreListingResponseData
            {
                public long? ReferenceId { get; set; }
                public DateTime? Date { get; set; }
                public string? Creator { get; set; }
                public string? Title { get; set; }
                public string? Description { get; set; }
                public long? Rating { get; set; }
                public string? ImageUrl { get; set; }
                public string? Review { get; set; }

            }

            public class Location
            {
                public string? Name { get; set; }
                public string? Address { get; set; }
                public double? Latitude { get; set; }
                public double? Longitude { get; set; }
            }
        }
        public class Merchant
        {
            public class List
            {
                public class Request
                {
                    public int? CountryId { get; set; }
                    public string? CountryKey { get; set; }

                    public long AuthAccountId { get; set; }
                    public long AccountId { get; set; }
                    public string? AccountKey { get; set; }
                    public bool RefreshCount { get; set; } = true;
                    public int Offset { get; set; }
                    public int Limit { get; set; }
                    public int TotalRecords { get; set; }
                    public string? SearchParameter { get; set; }
                    public string? SearchCondition { get; set; }
                    public string? SortExpression { get; set; }
                    public List<string>? Categories { get; set; }

                    public OUserReference? UserReference { get; set; }
                }
                public class Response
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }

                    public string? DisplayName { get; set; }

                    public long? CategoryId { get; set; }
                    public string? CategoryKey { get; set; }
                    public string? CategoryName { get; set; }



                    public string? IconUrl { get; set; }

                    public long? Stores { get; set; }
                    public long? Deals { get; set; }
                    public double? MinimumDiscount { get; set; }
                    public double? MaximumDiscount { get; set; }

                    public string? Address { get; set; }
                    public long? CountryId { get; set; }
                    public string? CountryKey { get; set; }
                    public string? CountryName { get; set; }

                    public long? StateId { get; set; }
                    public string? StateKey { get; set; }
                    public string? StateName { get; set; }

                    public long? CityId { get; set; }
                    public string? CityKey { get; set; }
                    public string? CityName { get; set; }

                    public long? CityAreaId { get; set; }
                    public string? CityAreaKey { get; set; }
                    public string? CityAreaName { get; set; }
                }
            }
        }
        public class Slider
        {
            public class Request
            {
                public int? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? Location { get; set; }
                public string? City { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? Location { get; set; }
                public List<Item>? Items { get; set; }
            }
            public class Item
            {
                public string? TypeCode { get; set; }
                public string? Title { get; set; }
                public string? Url { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public string? ImageUrl { get; set; }
                public string? Type { get; set; }

                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }

            public class Data
            {
                public long PromoReferenceId { get; set; }
                public string? PromoReferenceKey { get; set; }
                public string? Locations { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? TypeCode { get; set; }
                public string? PromotionalSliderTypeCode { get; set; }
                public long? DealId { get; set; }
                public string? Title { get; set; }
                public string? DealKey { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantName { get; set; }
                public string? Name { get; set; }
                public string? ImageUrl { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public double? ActualPrice { get; set; }
                public double? SellingPrice { get; set; }
                public double? Saving { get; set; }
                public double? DiscountPercentage { get; set; }
                public int StoreCount { get; set; }
                public string? Address { get; set; }

                public string? Url { get; set; }
                public string? DealImageUrl { get; set; }
                public string? CityAreaName { get; set; }
                public string? CityName { get; set; }
                public string? StateName { get; set; }
                public bool IsFlashDeal { get; set; } = false;
                public long Views { get; set; } = 0;
                public long Purchase { get; set; } = 0;
            }
        }

        public class Banner
        {
            public class Request
            {
                public int? CountryId { get; set; }
                public string? CountryKey { get; set; }
                public string? Location { get; set; }
                public string? City { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public string? Location { get; set; }
                public string? LogoImage { get; set; }
                public List<Item> Items { get; set; }
            }
            public class Item
            {
                public string? TypeCode { get; set; }
                public string? Title { get; set; }
                public string? Url { get; set; }
                public long? DealId { get; set; }
                public string? DealKey { get; set; }
                public string? ImageUrl { get; set; }
                public string? Type { get; set; }
                public double? DiscountPercentage { get; set; }

                public long? ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
            }

            public class Data
            {
                public long PromoReferenceId { get; set; }
                public string? PromoReferenceKey { get; set; }
                public string? Locations { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? TypeCode { get; set; }
                public string? PromotionalSliderTypeCode { get; set; }
                public long? DealId { get; set; }
                public string? Title { get; set; }
                public string? DealKey { get; set; }
                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }
                public string? MerchantIconUrl { get; set; }
                public string? MerchantKey { get; set; }
                public string? MerchantName { get; set; }
                public string? Name { get; set; }
                public string? ImageUrl { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }
                public DateTime? CreateDate { get; set; }
                public double? ActualPrice { get; set; }
                public double? SellingPrice { get; set; }
                public double? Saving { get; set; }
                public double? DiscountPercentage { get; set; }
                public int StoreCount { get; set; }
                public string? Address { get; set; }

                public string? Url { get; set; }
                public string? DealImageUrl { get; set; }
                public string? CityAreaName { get; set; }
                public string? CityName { get; set; }
                public string? StateName { get; set; }
                public bool IsFlashDeal { get; set; } = false;
                public long Views { get; set; } = 0;
                public long Purchase { get; set; } = 0;
            }
        }
    }
}
