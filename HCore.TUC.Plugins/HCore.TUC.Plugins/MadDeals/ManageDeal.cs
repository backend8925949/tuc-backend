//==================================================================================
// FileName: ManageDeal.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 15-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Framework;

namespace HCore.TUC.Plugins.MadDeals
{
    public class ManageDeal
    {
        FrameworkDeals _FrameworkDeals;
        /// <summary>
        /// Description: Gets the stats.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStats(OList.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetStats(_Request);
        }
        /// <summary>
        /// Description: Gets the search states.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSearchStates(OList.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetSearchStates(_Request);
        }
        /// <summary>
        /// Description: Gets the search cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSearchCities(OList.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetSearchCities(_Request);
        }
        /// <summary>
        /// Description: Gets the cities.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCities(OList.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetCities(_Request);
        }
        /// <summary>
        /// Description: Gets the categories.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCategories(OList.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetCategories(_Request);
        }
        /// <summary>
        /// Description: Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(MadDeals.Object.OMadDeals.Deals.List.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetDeal(_Request);
        }

        /// <summary>
        /// Description: Gets the deal.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDeal(MadDeals.Object.OMadDeals.Deal.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetDeal(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        ///

        /// <summary>
        /// Description: Gets store total ratings arnage ratigs.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetstoreRatings(MadDeals.Object.OMadDeals.Deal.StoreRequest _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetStoreRatings(_Request);
        }

        /// <summary>
        /// Description: Gets store full data.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetstoreData(MadDeals.Object.OMadDeals.Deal.StoreRequest _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetStoreData(_Request);
        }

        /// <summary>
        /// Description: Gets the merchant.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>

        public OResponse GetMerchant(OList.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetMerchant(_Request);
        }
        
        /// <summary>
        /// Description: Gets the slider images.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSliderImages(MadDeals.Object.OMadDeals.Slider.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetSliderImages(_Request);
        }
        /// <summary>
        /// Description: Gets the deal promotions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealPromotions(MadDeals.Object.OMadDeals.Slider.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetDealPromotions(_Request);
        }

        /// <summary>
        /// This API is to get the list of the promotional banners uploaded from the console and the merchant panel.
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse GetBannerImages(MadDeals.Object.OMadDeals.Banner.Request _Request)
        {
            _FrameworkDeals = new FrameworkDeals();
            return _FrameworkDeals.GetBannerImages(_Request);
        }
    }
}
