//==================================================================================
// FileName: CoreMadDeals.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for caching deals, categories, merchant
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Object;
using HCore.TUC.Plugins.Resource;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using GeoCoordinatePortable;

namespace HCore.TUC.Plugins.MadDeals.Core
{
    internal class CoreMadDeals
    {
        HCoreContext _HCoreContext;
        internal static DateTime _CategoriesSyncTime = new DateTime(2000, 01, 01);
        internal static DateTime _CStoresSyncTime = new DateTime(2000, 01, 01);
        internal static DateTime _CMerchantsSyncTime = new DateTime(2000, 01, 01);
        internal static DateTime _CDealsSyncTime = new DateTime(2000, 01, 01);
        internal static List<OMadDealsStorage.Categories> _CCategories = new List<OMadDealsStorage.Categories>();
        internal static List<OMadDealsStorage.Store> _CStores = new List<OMadDealsStorage.Store>();
        internal static List<OMadDealsStorage.Merchant> _CMerchants = new List<OMadDealsStorage.Merchant>();
        internal static List<OMadDealsStorage.City> _CCities = new List<OMadDealsStorage.City>();
        internal static List<OMadDealsStorage.Deals> _CDeals = new List<OMadDealsStorage.Deals>();
        internal static List<OMadDealsStorage.Deals> _CFlashDeals = new List<OMadDealsStorage.Deals>();
        internal static List<OMadDealsStorage.DealPromotion> _CDealPromotions = new List<OMadDealsStorage.DealPromotion>();
        private object _lockDeal = new object();
        internal void SyncDealStatus()
        {
            using (_HCoreContext = new HCoreContext())
            {
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                var Deals = _HCoreContext.MDDeal.Where(x => x.StatusId == HelperStatus.Deals.Published
                                                    && x.PosterStorageId != null
                                                    && ActiveTime >= x.StartDate
                                                    && ActiveTime <= x.EndDate)
                                                    .ToList();
                if (Deals.Count > 0)
                {
                    foreach (var Deal in Deals)
                    {
                        Deal.VisiblityStatusId = 1;
                        _HCoreContext.SaveChanges();
                    }
                }
                else
                {
                    _HCoreContext.Dispose();
                }
                //foreach (var Country in Countries)
                //{
                //    DateTime ActiveTime = HCoreHelper.GetDateTime(HCoreHelper.GetGMTDateTime(), Country.CountryTimeZone);
                //    var PublishedDeals = _HCoreContext.MDDeal.Where(x => x.StatusId == HelperStatus.Deals.Published && x.PosterStorageId != null)
                //}
            }
        }
        /// <summary>
        /// cache deal categories
        /// </summary>
        internal void SyncDealCategory()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var _UpdatedCategories = _HCoreContext.MDCategory
                                                        .Where(x => x.StatusId == HelperStatus.Default.Active
                                                        && x.RootCategory.StatusId == HelperStatus.Default.Active
                                                        && (x.CreateDate > _CategoriesSyncTime || x.ModifyDate > _CategoriesSyncTime))
                                                        .Select(x => new OMadDealsStorage.Categories
                                                        {
                                                            ReferenceId = x.RootCategory.Id,
                                                            ReferenceKey = x.RootCategory.Guid,
                                                            Name = x.RootCategory.Name,
                                                            IconUrl = x.RootCategory.IconStorage.Path,
                                                            ParentCategoryId = x.RootCategory.ParentCategoryId,
                                                        })
                                                     .ToList();
                    _UpdatedCategories = _UpdatedCategories.OrderBy(x => x.Name).ToList();
                    _HCoreContext.Dispose();
                    if (_UpdatedCategories.Count > 0)
                    {
                        foreach (var _UpdatedCategory in _UpdatedCategories)
                        {
                            if (!string.IsNullOrEmpty(_UpdatedCategory.IconUrl))
                            {
                                _UpdatedCategory.IconUrl = _AppConfig.StorageUrl + _UpdatedCategory.IconUrl;
                            }
                            else
                            {
                                _UpdatedCategory.IconUrl = _AppConfig.Default_Poster;
                            }
                            var ItemCheck = _CCategories.Where(x => x.ReferenceId == _UpdatedCategory.ReferenceId).FirstOrDefault();
                            if (ItemCheck != null)
                            {
                                ItemCheck.IconUrl = _UpdatedCategory.IconUrl;
                                ItemCheck.Name = _UpdatedCategory.Name;
                                ItemCheck.ParentCategoryId = _UpdatedCategory.ParentCategoryId;
                            }
                            else
                            {
                                _CCategories.Add(_UpdatedCategory);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CRITICAL-SyncDealCategory", ex);
            }
        }
        /// <summary>
        /// Cache stores 
        /// </summary>
        internal void SyncStore()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var _Results = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.MerchantStore
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    && x.SyncTime > _CStoresSyncTime)
                                                    .Select(x => new OMadDealsStorage.Store
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,

                                                        CategoryId = x.Owner.PrimaryCategoryId,
                                                        CategoryKey = x.Owner.PrimaryCategory.Guid,
                                                        CategoryName = x.Owner.PrimaryCategory.Name,

                                                        MerchantId = x.Owner.Id,
                                                        MerchantKey = x.Owner.Guid,
                                                        MerchantName = x.Owner.Name,

                                                        IconUrl = x.Owner.IconStorage.Path,

                                                        Address = x.Address,
                                                        CountryId = x.CountryId,
                                                        CountryKey = x.Country.Guid,
                                                        CountryName = x.Country.Name,

                                                        StateId = x.StatusId,
                                                        StateKey = x.Status.Guid,
                                                        StateName = x.Status.Name,

                                                        CityId = x.CityId,
                                                        CityKey = x.City.Guid,
                                                        CityName = x.City.Name,

                                                        CityAreaId = x.CityAreaId,
                                                        CityAreaKey = x.CityArea.Guid,
                                                        CityAreaName = x.CityArea.Name,
                                                        GeoCoordinate = new GeoCoordinate(x.Latitude, x.Longitude)
                                                    })
                                               .ToList();
                    _HCoreContext.Dispose();
                    if (_Results.Count > 0)
                    {
                        foreach (var _Item in _Results)
                        {
                            if (!string.IsNullOrEmpty(_Item.IconUrl))
                            {
                                _Item.IconUrl = _AppConfig.StorageUrl + _Item.IconUrl;
                            }
                            else
                            {
                                _Item.IconUrl = _AppConfig.Default_Poster;
                            }
                            var ItemCheck = _CStores.Where(x => x.ReferenceId == _Item.ReferenceId).FirstOrDefault();
                            if (ItemCheck != null)
                            {
                                _CStores.Remove(ItemCheck);
                                _CStores.Add(_Item);
                            }
                            else
                            {
                                _CStores.Add(_Item);
                            }
                        }
                    }

                    _CCities = _CStores.Where(x => x.CityId != null && x.CityName != null)
                        .GroupBy(x => x.CityName).Select(x => new OMadDealsStorage.City
                        {
                            ReferenceId = x.FirstOrDefault().CityId,
                            ReferenceKey = x.FirstOrDefault().CityKey,
                            Name = x.Key.Trim(),
                        }).ToList();
                }

            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CRITICAL-SyncStore", ex);
            }
        }
        /// <summary>
        /// Cache merchant 
        /// </summary>
        internal void SyncMerchant()
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    var _Results = _HCoreContext.HCUAccount.Where(x => x.AccountTypeId == UserAccountType.Merchant
                                                    && x.StatusId == HelperStatus.Default.Active
                                                    //&& x.IconStorageId != null
                                                    && x.SyncTime > _CMerchantsSyncTime)
                                                    .Select(x => new OMadDealsStorage.Merchant
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        DisplayName = x.DisplayName,
                                                        CategoryId = x.PrimaryCategoryId,
                                                        CategoryKey = x.PrimaryCategory.Guid,
                                                        CategoryName = x.PrimaryCategory.Name,

                                                        IconUrl = x.IconStorage.Path,

                                                        Address = x.Address,
                                                        CountryId = x.CountryId,
                                                        CountryKey = x.Country.Guid,
                                                        CountryName = x.Country.Name,

                                                        StateId = x.StatusId,
                                                        StateKey = x.Status.Guid,
                                                        StateName = x.Status.Name,

                                                        CityId = x.CityId,
                                                        CityKey = x.City.Guid,
                                                        CityName = x.City.Name,

                                                        CityAreaId = x.CityAreaId,
                                                        CityAreaKey = x.CityArea.Guid,
                                                        CityAreaName = x.CityArea.Name,
                                                        GeoCoordinate = new GeoCoordinate(x.Latitude, x.Longitude)
                                                    })
                                               .ToList();
                    _HCoreContext.Dispose();
                    if (_Results.Count > 0)
                    {
                        foreach (var _Item in _Results)
                        {
                            if (!string.IsNullOrEmpty(_Item.IconUrl))
                            {
                                _Item.IconUrl = _AppConfig.StorageUrl + _Item.IconUrl;
                            }
                            else
                            {
                                _Item.IconUrl = _AppConfig.Default_Store_Icon;
                            }
                            var ItemCheck = _CMerchants.Where(x => x.ReferenceId == _Item.ReferenceId).FirstOrDefault();
                            if (ItemCheck != null)
                            {
                                _CMerchants.Remove(ItemCheck);
                                _CMerchants.Add(_Item);
                            }
                            else
                            {
                                _CMerchants.Add(_Item);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CRITICAL-SyncMerchant", ex);
            }

        }
        /// <summary>
        /// Cache published deal for deal website
        /// </summary>
        internal void SyncDeal()
        {
            try
            {
                lock (_lockDeal)
                {
                    DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                    bool status = false;

                    using (_HCoreContext = new HCoreContext())
                    {

                            var _Deals = _HCoreContext.MDDeal.Where(x => x.StatusId == HelperStatus.Deals.Published && x.PosterStorageId != null)
                                                        .Select(x => new OMadDealsStorage.Deals
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            Title = x.Title,

                                                            CountryId = x.Account.CountryId,

                                                            MerchantId = x.AccountId,
                                                            MerchantKey = x.Account.Guid,
                                                            MerchantName = x.Account.DisplayName,
                                                            MerchantIconUrl = x.Account.IconStorage.Path,

                                                            CategoryId = x.CategoryId,
                                                            CategoryKey = x.Category.Guid,
                                                            CategoryName = x.Category.Name,

                                                            SubCategoryId = x.SubcategoryId,
                                                            SubCategoryKey = x.Subcategory.Guid,
                                                            SubCategoryName = x.Subcategory.Name,

                                                            StartDate = x.StartDate,
                                                            EndDate = x.EndDate,

                                                            ImageUrl = x.PosterStorage.Path,

                                                            ActualPrice = x.ActualPrice,
                                                            SellingPrice = x.SellingPrice,
                                                            CreateDate = x.CreateDate,
                                                            IsFlashDeal = x.MDFlashDeal.Any(),
                                                            Views = x.MDDealView.Count(),
                                                            Purchase = (long)x.MDDealCode.Sum(x => x.ItemCount),
                                                            IsSoldout = (x.MaximumUnitSale - x.MDDealCode.Sum(x => x.ItemCount)) > 0 ? false : true,

                                                            DealTypeCode = x.DealType.SystemName,
                                                            DeliveryTypeCode = x.DeliveryType.SystemName,
                                                            Slug = x.Slug,

                                                            DealCodeValidityTypeId = x.UsageTypeId,
                                                            DealCodeValidityTypeCode = x.UsageType.SystemName,
                                                            DealCodeValidityTypeName = x.UsageType.Name,
                                                            DealCodeValidityDays = x.CodeValidtyDays,
                                                            DealCodeValidityEndDate = x.CodeValidityEndDate,

                                                            AverageRatings = x.MDDealReview.Average(y => y.Rating),
                                                            TotalReviews = x.MDDealReview.Count(),
                                                        })
                                                   .ToList();
                        _HCoreContext.Dispose();
                        if (_Deals.Count > 0)
                        {
                            _CDeals.Clear();
                            foreach (var _Deal in _Deals)
                            {
                                _Deal.Merchant = _CMerchants.Where(x => x.ReferenceId == _Deal.MerchantId).FirstOrDefault();
                                if (_Deal.Merchant != null)
                                {
                                    _Deal.Address = _Deal.Merchant.Address;
                                    _Deal.CountryId = _Deal.Merchant.CountryId;
                                    _Deal.CountryKey = _Deal.Merchant.CountryKey;
                                    _Deal.CountryName = _Deal.Merchant.CountryName;
                                    _Deal.StateId = _Deal.Merchant.StateId;
                                    _Deal.StateKey = _Deal.Merchant.StateKey;
                                    _Deal.StateName = _Deal.Merchant.StateName;
                                    _Deal.CityId = _Deal.Merchant.CityId;
                                    _Deal.CityKey = _Deal.Merchant.CityKey;
                                    _Deal.CityName = _Deal.Merchant.CityName;
                                    _Deal.CityAreaId = _Deal.Merchant.CityAreaId;
                                    _Deal.CityAreaKey = _Deal.Merchant.CityAreaKey;
                                    _Deal.CityAreaName = _Deal.Merchant.CityAreaName;
                                    _Deal.GeoCoordinate = _Deal.Merchant.GeoCoordinate;
                                    _Deal.Stores = _CStores.Where(x => x.MerchantId == _Deal.MerchantId).ToList();
                                }
                                _Deal.Saving = _Deal.ActualPrice - _Deal.SellingPrice;
                                _Deal.DiscountPercentage = Math.Round(HCoreHelper.GetAmountPercentage((double)_Deal.Saving, (double)_Deal.ActualPrice, false));
                                if (!string.IsNullOrEmpty(_Deal.MerchantIconUrl))
                                {
                                    _Deal.MerchantIconUrl = _AppConfig.StorageUrl + _Deal.MerchantIconUrl;
                                }
                                else
                                {
                                    _Deal.MerchantIconUrl = _AppConfig.Default_Store_Icon;
                                }
                                if (!string.IsNullOrEmpty(_Deal.ImageUrl))
                                {
                                    _Deal.ImageUrl = _AppConfig.StorageUrl + _Deal.ImageUrl;
                                }
                                else
                                {
                                    _Deal.ImageUrl = _AppConfig.Default_Poster;
                                }

                                var ItemCheck = _CDeals.Where(x => x.ReferenceId == _Deal.ReferenceId).FirstOrDefault();
                                if (ItemCheck != null)
                                {
                                    _CDeals.Remove(ItemCheck);
                                    _CDeals.Add(_Deal);
                                }
                                else
                                {
                                    _CDeals.Add(_Deal);
                                }
                            }
                        }
                        _CDeals = _CDeals.OrderBy(a => Guid.NewGuid()).ToList();
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var FlashDeals = _HCoreContext.MDFlashDeal.Where(x => x.StatusId == HelperStatus.Default.Active && x.Deal.StatusId == HelperStatus.Deals.Published)
                                                        .Select(x => new
                                                        {
                                                            ReferenceId = x.DealId,
                                                            StartDate = x.StartDate,
                                                            EndDate = x.EndDate,
                                                        })
                                                   .ToList();
                        _HCoreContext.Dispose();
                        if (FlashDeals.Count > 0)
                        {
                            _CFlashDeals.Clear();
                            foreach (var _FlashDeal in FlashDeals)
                            {
                                var Deal = _CDeals.Where(x => x.ReferenceId == _FlashDeal.ReferenceId).FirstOrDefault();
                                if (Deal != null)
                                {
                                    _CFlashDeals.Add(new OMadDealsStorage.Deals
                                    {
                                        ReferenceId = Deal.ReferenceId,
                                        ReferenceKey = Deal.ReferenceKey,
                                        Title = Deal.Title,
                                        CategoryId = Deal.CategoryId,
                                        CategoryKey = Deal.CategoryKey,
                                        CategoryName = Deal.CategoryName,
                                        SubCategoryId = Deal.SubCategoryId,
                                        SubCategoryKey = Deal.SubCategoryKey,
                                        SubCategoryName = Deal.SubCategoryName,
                                        MerchantId = Deal.MerchantId,
                                        MerchantKey = Deal.MerchantKey,
                                        MerchantName = Deal.MerchantName,
                                        MerchantIconUrl = Deal.MerchantIconUrl,
                                        ImageUrl = Deal.ImageUrl,
                                        StartDate = _FlashDeal.StartDate,
                                        EndDate = _FlashDeal.EndDate,
                                        CreateDate = Deal.CreateDate,
                                        ActualPrice = Deal.ActualPrice,
                                        SellingPrice = Deal.SellingPrice,
                                        Saving = Deal.Saving,
                                        DiscountPercentage = Deal.DiscountPercentage,
                                        StoreCount = Deal.StoreCount,
                                        Merchant = Deal.Merchant,
                                        Stores = Deal.Stores,
                                        StoreId = Deal.StoreId,
                                        StoreKey = Deal.StoreKey,
                                        StoreDisplayName = Deal.StoreDisplayName,
                                        StoreAddress = Deal.StoreAddress,
                                        Address = Deal.Address,
                                        CountryId = Deal.CountryId,
                                        CountryKey = Deal.CountryKey,
                                        CountryName = Deal.CountryName,
                                        StateId = Deal.StateId,
                                        StateKey = Deal.StateKey,
                                        StateName = Deal.StateName,
                                        CityId = Deal.CityId,
                                        CityKey = Deal.CityKey,
                                        CityName = Deal.CityName,
                                        CityAreaId = Deal.CityAreaId,
                                        CityAreaKey = Deal.CityAreaKey,
                                        CityAreaName = Deal.CityAreaName,
                                        IsFlashDeal = true,
                                        Views = Deal.Views,
                                        Purchase = Deal.Purchase,
                                        GeoCoordinate = Deal.GeoCoordinate,
                                        IsSoldout = Deal.IsSoldout,
                                        DealTypeCode = Deal.DealTypeCode,
                                        DeliveryTypeCode = Deal.DeliveryTypeCode,
                                        Slug = Deal.Slug,

                                        DealCodeValidityTypeId = Deal.DealCodeValidityTypeId,
                                        DealCodeValidityTypeCode = Deal.DealCodeValidityTypeCode,
                                        DealCodeValidityTypeName = Deal.DealCodeValidityTypeName,
                                        DealCodeValidityDays = Deal.DealCodeValidityDays,
                                        DealCodeValidityEndDate = Deal.DealCodeValidityEndDate,

                                        AverageRatings = Deal.AverageRatings,
                                        TotalReviews = Deal.TotalReviews,
                                    });
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.SaveLog("CRITICAL-SyncDeal" + HCoreConstant.HostName, ex.ToString());
            }
        }
        /// <summary>
        /// cashe promotional deals
        /// </summary>
        internal void SyncDealPromotion()
        {
            try
            {
                DateTime ActiveTime = HCoreHelper.GetGMTDateTime();
                using (_HCoreContext = new HCoreContext())
                {
                    var _TDealPromotions = _HCoreContext.MDDealPromotion.Where(x => x.StatusId == HelperStatus.Default.Active && (ActiveTime >= x.StartDate && ActiveTime <= x.EndDate))
                                                    .Select(x => new OMadDealsStorage.DealPromotion
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Locations = x.Location,

                                                        DealId = x.DealId,
                                                        Url = x.Url,

                                                        ImageUrl = x.ImageStorage.Path,

                                                        CountryId = x.CountryId,
                                                        CountryKey = x.Country.Guid,

                                                        StartDate = x.StartDate,
                                                        EndDate = x.EndDate,
                                                        CreateDate = x.CreateDate,

                                                        PromotionalSliderTypeCode = x.Type.SystemName,

                                                        StatusId = x.StatusId,
                                                    })
                                               .ToList();
                    _HCoreContext.Dispose();
                    if (_TDealPromotions.Count > 0)
                    {
                        _CDealPromotions.Clear();
                        foreach (var _DealPromotion in _TDealPromotions)
                        {
                            if (_DealPromotion.DealId > 0)
                            {
                                var Deal = _CDeals.Where(x => x.ReferenceId == _DealPromotion.DealId).FirstOrDefault();
                                if (Deal != null)
                                {
                                    _DealPromotion.CityId = Deal.CityId;
                                    _DealPromotion.CityKey = Deal.CityKey;
                                    _DealPromotion.CityName = Deal.CityName;
                                }
                            }
                            var ItemCheck = _CDealPromotions.Where(x => x.ReferenceId == _DealPromotion.ReferenceId).FirstOrDefault();
                            if (ItemCheck != null)
                            {
                                _CDealPromotions.Remove(ItemCheck);
                                _CDealPromotions.Add(_DealPromotion);
                            }
                            else
                            {
                                _CDealPromotions.Add(_DealPromotion);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("CRITICAL-SyncDealPromotion", ex);
            }
        }
    }
}
