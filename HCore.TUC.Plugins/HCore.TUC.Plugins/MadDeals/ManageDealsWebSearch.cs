﻿using System;
using HCore.TUC.Plugins.MadDeals.Framework;
using HCore.TUC.Plugins.MadDeals.Object;
using HCore.Helper;

namespace HCore.TUC.Plugins.MadDeals
{
	public class ManageDealsWebSearch
	{
		FrameworkDealsWebSearch? _FrameworkDealsWebSearch;

		public async Task<OResponse> SaveSearch(ODealsWebSearch.Save.Request _Request)
		{
			_FrameworkDealsWebSearch = new FrameworkDealsWebSearch();
			return await _FrameworkDealsWebSearch.SaveSearch(_Request);
        }

        public async Task<OResponse> GetSearch(OList.Request _Request)
        {
            _FrameworkDealsWebSearch = new FrameworkDealsWebSearch();
            return await _FrameworkDealsWebSearch.GetSearch(_Request);
        }
    }
}

