//==================================================================================
// FileName: ManageDealReview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.TUC.Plugins.MadDeals.Framework;
using HCore.TUC.Plugins.MadDeals.Object;

namespace HCore.TUC.Plugins.MadDeals
{
    public class ManageDealReview
    {
        FrameworkDealReview _FrameworkDealReview;

        /// <summary>
        /// Saves the deal review.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveDealReview(ODealReview.Save _Request)
        {
            _FrameworkDealReview = new FrameworkDealReview();
            return _FrameworkDealReview.SaveDealReview(_Request);
        }

       

        /// <summary>
        /// Gets the deal review.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealReview(OList.Request _Request)
        {
            _FrameworkDealReview = new FrameworkDealReview();
            return _FrameworkDealReview.GetDealReview(_Request);
        }

    }
}
