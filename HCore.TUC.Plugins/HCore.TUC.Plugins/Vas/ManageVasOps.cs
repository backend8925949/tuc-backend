//==================================================================================
// FileName: ManageVasOps.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Vas.Framework;
using HCore.TUC.Plugins.Vas.Object;

namespace HCore.TUC.Plugins.Vas
{    
    public class ManageVasOps
    {        
        FrameworkVasOperations _FrameworkVasOperations;
        /// <summary>
        /// Description: Gets the vas options.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasOptions(OList.Request _Request)
        {
            _FrameworkVasOperations = new FrameworkVasOperations();
            return _FrameworkVasOperations.GetVasOptions(_Request);
        }
        /// <summary>
        /// Description: Vas payment initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VasPayment_Initialize(OVasOp.Purchase.Initialize.Request _Request)
        {
            _FrameworkVasOperations = new FrameworkVasOperations();
            return _FrameworkVasOperations.VasPayment_Initialize(_Request);
        }
        /// <summary>
        /// Description: Vas payment confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VasPayment_Confirm(OVasOp.Purchase.Confirm.Request _Request)
        {
            _FrameworkVasOperations = new FrameworkVasOperations();
            return _FrameworkVasOperations.VasPayment_Confirm(_Request);
        }
        /// <summary>
        /// Description: Vas payment cancel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VasPayment_Cancel(OVasOp.Purchase.Cancel.Request _Request)
        {
            _FrameworkVasOperations = new FrameworkVasOperations();
            return _FrameworkVasOperations.VasPayment_Cancel(_Request);
        }

    }
}
