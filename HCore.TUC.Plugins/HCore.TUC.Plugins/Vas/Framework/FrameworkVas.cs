//==================================================================================
// FileName: FrameworkVas.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Plugins.Vas.Object;
using HCore.TUC.Plugins.Resource;

namespace HCore.TUC.Plugins.Vas.Framework
{
    internal class FrameworkVas
    {
        HCoreContext _HCoreContext;
        OVas.PurchaseHistory.Overview _PurchaseHistoryOverview;
        internal OResponse GetVasCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.VASCategory
                            .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                            .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OVas.Category.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    SystemName = x.SystemName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OVas.Category.List> _List = _HCoreContext.VASCategory
                            .Where(x => x.CountryId == _Request.UserReference.SystemCountry)
                            .Where(x => x.StatusId == HelperStatus.Default.Active)
                                                .Select(x => new OVas.Category.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,
                                                    SystemName = x.SystemName,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                    //    {
                    //        DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.ImageUrl = _AppConfig.Default_Poster;
                    //    }
                    //}
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetVasCategory", _Exception, _Request.UserReference, _OResponse, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse GetVasProduct(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.VASProduct
                            .Where(x => x.Category.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OVas.Product.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CategoryKey = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    Name = x.Name,
                                                    RewardPercentage = x.RewardPercentage,
                                                    UserRewardPercentage = x.UserPercentage,
                                                    CommissionPercentage = (100 - x.UserPercentage),
                                                    IconUrl = x.IconStorage.Path,
                                                    TotalItem = x.VASProductItem.Count,
                                                    TotalPurchase = x.VASProductItem.Sum(a => a.VasPayment.Count(c => c.StatusId == HelperStatus.BillPaymentStatus.Success)),
                                                    TotalPurchaseAmount = x.VASProductItem.Sum(a => a.VasPayment.Where(c => c.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(g => g.Amount)),
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OVas.Product.List> _List = _HCoreContext.VASProduct
                            .Where(x => x.Category.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OVas.Product.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    CategoryKey = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    Name = x.Name,
                                                    RewardPercentage = x.RewardPercentage,
                                                    UserRewardPercentage = x.UserPercentage,
                                                    CommissionPercentage = (100 - x.UserPercentage),
                                                    IconUrl = x.IconStorage.Path,
                                                    TotalItem = x.VASProductItem.Count,
                                                    TotalPurchase = x.VASProductItem.Sum(a => a.VasPayment.Count(c => c.StatusId == HelperStatus.BillPaymentStatus.Success)),
                                                    TotalPurchaseAmount = x.VASProductItem.Sum(a => a.VasPayment.Where(c => c.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(g => g.Amount)),
                                                    CreateDate = x.CreateDate,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    foreach (var DataItem in _List)
                    {
                        if (!string.IsNullOrEmpty(DataItem.IconUrl))
                        {
                            DataItem.IconUrl = _AppConfig.StorageUrl + DataItem.IconUrl;
                        }
                        else
                        {
                            DataItem.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetVasProductItem", _Exception, _Request.UserReference, _OResponse, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse GetVasProductItem(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ReferenceId > 0 && !string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.VASProductItem
                            .Where(x => x.Product.Category.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.ProductId == _Request.ReferenceId && x.Product.Guid == _Request.ReferenceKey)
                                                    .Select(x => new OVas.ProductItem.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        Amount = x.Amount,
                                                        TotalPurchase = x.VasPayment.Count(c => c.StatusId == HelperStatus.BillPaymentStatus.Success),
                                                        TotalPurchaseAmount = x.VasPayment.Where(c => c.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(a => a.Amount),
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        ModifyByKey = x.ModifyBy.Guid,
                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OVas.ProductItem.List> _List = _HCoreContext.VASProductItem
                            .Where(x => x.Product.Category.CountryId == _Request.UserReference.SystemCountry)
                                                    .Where(x => x.ProductId == _Request.ReferenceId && x.Product.Guid == _Request.ReferenceKey)
                                                    .Select(x => new OVas.ProductItem.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        Amount = x.Amount,
                                                        TotalPurchase = x.VasPayment.Count(c => c.StatusId == HelperStatus.BillPaymentStatus.Success),
                                                        TotalPurchaseAmount = x.VasPayment.Where(c => c.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(a => a.Amount),
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        ModifyByKey = x.ModifyBy.Guid,
                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                    }
                    else
                    {
                        if (_Request.RefreshCount)
                        {
                            #region Total Records
                            _Request.TotalRecords = _HCoreContext.VASProductItem
                            .Where(x => x.Product.Category.CountryId == _Request.UserReference.SystemCountry)
                                                    .Select(x => new OVas.ProductItem.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        Amount = x.Amount,
                                                        TotalPurchase = x.VasPayment.Count(c => c.StatusId == HelperStatus.BillPaymentStatus.Success),
                                                        TotalPurchaseAmount = x.VasPayment.Where(c => c.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(a => a.Amount),
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        ModifyByKey = x.ModifyBy.Guid,
                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                            #endregion
                        }
                        #region Get Data
                        List<OVas.ProductItem.List> _List = _HCoreContext.VASProductItem
                            .Where(x => x.Product.Category.CountryId == _Request.UserReference.SystemCountry)
                                                    .Select(x => new OVas.ProductItem.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,
                                                        Name = x.Name,
                                                        Amount = x.Amount,
                                                        TotalPurchase = x.VasPayment.Count(c => c.StatusId == HelperStatus.BillPaymentStatus.Success),
                                                        TotalPurchaseAmount = x.VasPayment.Where(c => c.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(a => a.Amount),
                                                        ModifyDate = x.ModifyDate,
                                                        ModifyById = x.ModifyById,
                                                        ModifyByKey = x.ModifyBy.Guid,
                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name,
                                                    })
                                                 .Where(_Request.SearchCondition)
                                                 .OrderBy(_Request.SortExpression)
                                                 .Skip(_Request.Offset)
                                                 .Take(_Request.Limit)
                                                 .ToList();
                        #endregion
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                    }

                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetVasProductItem", _Exception, _Request.UserReference, _OResponse, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse GetVasProductPurchase(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OVas.PurchaseHistory.List
                                                {
                                                    ParentId = x.Account.Owner.OwnerId,
                                                    ParentKey = x.Account.Owner.Owner.Guid,
                                                    ParentName = x.Account.Owner.Owner.Name,
                                                    ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                    ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                    SubParentId = x.Account.OwnerId,
                                                    SubParentKey = x.Account.Owner.Guid,
                                                    SubParentName = x.Account.Owner.Name,
                                                    SubParentDisplayName = x.Account.Owner.DisplayName,
                                                    SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                    CashierId = x.Account.SubOwnerId,
                                                    CashierKey = x.Account.SubOwner.Guid,
                                                    CashierName = x.Account.SubOwner.Name,
                                                    CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                    CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    TypeId = x.TypeId,
                                                    TypeKey = x.Type.Guid,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    BillerName = x.BillerName,

                                                    ProductId = x.ProductItem.ProductId,
                                                    ProductKey = x.ProductItem.Product.Guid,
                                                    ProductName = x.ProductItem.Product.Name,

                                                    ProductItemId = x.ProductItemId,
                                                    ProductItemKey = x.ProductItem.Guid,
                                                    ProductItemName = x.ProductItem.Name,

                                                    ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                    ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                    ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    AccountNumber = x.AccountNumber,

                                                    Amount = x.Amount,
                                                    RewardAmount = x.RewardAmount,
                                                    UserRewardAmount = x.UserRewardAmount,
                                                    CommissionAmount = x.CommissionAmount,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    PaymentReference = x.PaymentReference,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OVas.PurchaseHistory.List> _List = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OVas.PurchaseHistory.List
                                                {
                                                    ParentId = x.Account.Owner.OwnerId,
                                                    ParentKey = x.Account.Owner.Owner.Guid,
                                                    ParentName = x.Account.Owner.Owner.Name,
                                                    ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                    ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                    SubParentId = x.Account.OwnerId,
                                                    SubParentKey = x.Account.Owner.Guid,
                                                    SubParentName = x.Account.Owner.Name,
                                                    SubParentDisplayName = x.Account.Owner.DisplayName,
                                                    SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                    CashierId = x.Account.SubOwnerId,
                                                    CashierKey = x.Account.SubOwner.Guid,
                                                    CashierName = x.Account.SubOwner.Name,
                                                    CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                    CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    TypeId = x.TypeId,
                                                    TypeKey = x.Type.Guid,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    BillerName = x.BillerName,

                                                    ProductId = x.ProductItem.ProductId,
                                                    ProductKey = x.ProductItem.Product.Guid,
                                                    ProductName = x.ProductItem.Product.Name,


                                                    ProductItemId = x.ProductItemId,
                                                    ProductItemKey = x.ProductItem.Guid,
                                                    ProductItemName = x.ProductItem.Name,

                                                    ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                    ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                    ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,
                                                    AccountNumber = x.AccountNumber,
                                                    Amount = x.Amount,
                                                    RewardAmount = x.RewardAmount,
                                                    UserRewardAmount = x.UserRewardAmount,
                                                    CommissionAmount = x.CommissionAmount,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    PaymentReference = x.PaymentReference,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                    //    {
                    //        DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.ImageUrl = _AppConfig.Default_Poster;
                    //    }
                    //}
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetVasProductPurchase", _Exception, _Request.UserReference, _OResponse, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse GetVasProductPurchaseCustomer(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.RefreshCount)
                    {
                        #region Total Records
                        _Request.TotalRecords = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountId == _Request.UserReference.AccountId)
                                                .Select(x => new OVas.PurchaseHistory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    //ProductId = x.ProductItem.ProductId,
                                                    //ProductKey = x.ProductItem.Product.Guid,
                                                    ProductName = x.ProductItem.Product.Name,

                                                    //ProductItemId = x.ProductItemId,
                                                    //ProductItemKey = x.ProductItem.Guid,
                                                    ProductItemName = x.ProductItem.Name,

                                                    //ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                    //ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                    ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                    AccountNumber = x.AccountNumber,

                                                    Amount = x.Amount,
                                                    UserRewardAmount = x.UserRewardAmount,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    PaymentReference = x.PaymentReference,

                                                    Value = x.Comment,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        #endregion
                    }
                    #region Get Data
                    List<OVas.PurchaseHistory.List> _List = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Where(x => x.AccountId == _Request.UserReference.AccountId)
                                                .Select(x => new OVas.PurchaseHistory.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    //ProductId = x.ProductItem.ProductId,
                                                    //ProductKey = x.ProductItem.Product.Guid,
                                                    ProductName = x.ProductItem.Product.Name,

                                                    //ProductItemId = x.ProductItemId,
                                                    //ProductItemKey = x.ProductItem.Guid,
                                                    ProductItemName = x.ProductItem.Name,

                                                    //ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                    //ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                    ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                    AccountNumber = x.AccountNumber,

                                                    Amount = x.Amount,
                                                    UserRewardAmount = x.UserRewardAmount,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    PaymentReference = x.PaymentReference,

                                                    Value = x.Comment,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                             .Where(_Request.SearchCondition)
                                             .OrderBy(_Request.SortExpression)
                                             .Skip(_Request.Offset)
                                             .Take(_Request.Limit)
                                             .ToList();
                    #endregion
                    //foreach (var DataItem in _List)
                    //{
                    //    if (!string.IsNullOrEmpty(DataItem.ImageUrl))
                    //    {
                    //        DataItem.ImageUrl = _AppConfig.StorageUrl + DataItem.ImageUrl;
                    //    }
                    //    else
                    //    {
                    //        DataItem.ImageUrl = _AppConfig.Default_Poster;
                    //    }
                    //}
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _List, _Request.Offset, _Request.Limit);
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                return HCoreHelper.LogException("GetVasProductPurchase", _Exception, _Request.UserReference, _OResponse, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse GetVasProductPurchase(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    OVas.PurchaseHistory.Details _Details = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OVas.PurchaseHistory.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,


                                                    ProductId = x.ProductItem.ProductId,
                                                    ProductKey = x.ProductItem.Product.Guid,
                                                    ProductName = x.ProductItem.Product.Name,

                                                    ProductItemId = x.ProductItemId,
                                                    ProductItemKey = x.ProductItem.Guid,
                                                    ProductItemName = x.ProductItem.Name,

                                                    ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                    ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                    ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    Amount = x.Amount,
                                                    RewardAmount = x.RewardAmount,
                                                    UserRewardAmount = x.UserRewardAmount,
                                                    CommissionAmount = x.CommissionAmount,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    PaymentReference = x.PaymentReference,

                                                    BillerName = x.BillerName,

                                                    Comment = x.Comment,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    PaymentToken = x.PaymentToken,
                                                    TransactionId = x.TransactionId,
                                                    TransactionKey = x.Transaction.Guid,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (_Details != null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasProductPurchase", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse GetVasProductPurchaseOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    HCoreHelper.GetSortCondition(_Request, "StartDate", "desc");
                }
                if (_Request.Limit < 1)
                {
                    _Request.Limit = _AppConfig.DefaultRecordsLimit;
                }
                using (_HCoreContext = new HCoreContext())
                {

                    _PurchaseHistoryOverview = new OVas.PurchaseHistory.Overview();
                    _PurchaseHistoryOverview.Transactions = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OVas.PurchaseHistory.List
                                                {
                                                    ParentId = x.Account.Owner.OwnerId,
                                                    ParentKey = x.Account.Owner.Owner.Guid,
                                                    ParentName = x.Account.Owner.Owner.Name,
                                                    ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                    ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                    SubParentId = x.Account.OwnerId,
                                                    SubParentKey = x.Account.Owner.Guid,
                                                    SubParentName = x.Account.Owner.Name,
                                                    SubParentDisplayName = x.Account.Owner.DisplayName,
                                                    SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                    CashierId = x.Account.SubOwnerId,
                                                    CashierKey = x.Account.SubOwner.Guid,
                                                    CashierName = x.Account.SubOwner.Name,
                                                    CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                    CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    TypeId = x.TypeId,
                                                    TypeKey = x.Type.Guid,
                                                    TypeCode = x.Type.SystemName,
                                                    TypeName = x.Type.Name,

                                                    BillerName = x.BillerName,

                                                    ProductId = x.ProductItem.ProductId,
                                                    ProductKey = x.ProductItem.Product.Guid,
                                                    ProductName = x.ProductItem.Product.Name,

                                                    ProductItemId = x.ProductItemId,
                                                    ProductItemKey = x.ProductItem.Guid,
                                                    ProductItemName = x.ProductItem.Name,

                                                    ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                    ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                    ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    Amount = x.Amount,
                                                    RewardAmount = x.RewardAmount,
                                                    UserRewardAmount = x.UserRewardAmount,
                                                    CommissionAmount = x.CommissionAmount,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    PaymentReference = x.PaymentReference,

                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                    _PurchaseHistoryOverview.Customers = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                              .Select(x => new OVas.PurchaseHistory.List
                                              {
                                                  ParentId = x.Account.Owner.OwnerId,
                                                  ParentKey = x.Account.Owner.Owner.Guid,
                                                  ParentName = x.Account.Owner.Owner.Name,
                                                  ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                  ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                  SubParentId = x.Account.OwnerId,
                                                  SubParentKey = x.Account.Owner.Guid,
                                                  SubParentName = x.Account.Owner.Name,
                                                  SubParentDisplayName = x.Account.Owner.DisplayName,
                                                  SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                  CashierId = x.Account.SubOwnerId,
                                                  CashierKey = x.Account.SubOwner.Guid,
                                                  CashierName = x.Account.SubOwner.Name,
                                                  CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                  CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  TypeId = x.TypeId,
                                                  TypeKey = x.Type.Guid,
                                                  TypeCode = x.Type.SystemName,
                                                  TypeName = x.Type.Name,

                                                  BillerName = x.BillerName,

                                                  ProductId = x.ProductItem.ProductId,
                                                  ProductKey = x.ProductItem.Product.Guid,
                                                  ProductName = x.ProductItem.Product.Name,

                                                  ProductItemId = x.ProductItemId,
                                                  ProductItemKey = x.ProductItem.Guid,
                                                  ProductItemName = x.ProductItem.Name,

                                                  ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                  ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                  ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,

                                                  Amount = x.Amount,
                                                  RewardAmount = x.RewardAmount,
                                                  UserRewardAmount = x.UserRewardAmount,
                                                  CommissionAmount = x.CommissionAmount,
                                                  StartDate = x.StartDate,
                                                  EndDate = x.EndDate,
                                                  PaymentReference = x.PaymentReference,

                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                              })
                                             .Where(_Request.SearchCondition)
                                             .Select(x => x.AccountId)
                                             .Distinct()
                                     .Count();
                    _PurchaseHistoryOverview.InvoiceAmount = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                               .Select(x => new OVas.PurchaseHistory.List
                                               {
                                                   ParentId = x.Account.Owner.OwnerId,
                                                   ParentKey = x.Account.Owner.Owner.Guid,
                                                   ParentName = x.Account.Owner.Owner.Name,
                                                   ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                   ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                   SubParentId = x.Account.OwnerId,
                                                   SubParentKey = x.Account.Owner.Guid,
                                                   SubParentName = x.Account.Owner.Name,
                                                   SubParentDisplayName = x.Account.Owner.DisplayName,
                                                   SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                   CashierId = x.Account.SubOwnerId,
                                                   CashierKey = x.Account.SubOwner.Guid,
                                                   CashierName = x.Account.SubOwner.Name,
                                                   CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                   CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,

                                                   TypeId = x.TypeId,
                                                   TypeKey = x.Type.Guid,
                                                   TypeCode = x.Type.SystemName,
                                                   TypeName = x.Type.Name,

                                                   BillerName = x.BillerName,

                                                   ProductId = x.ProductItem.ProductId,
                                                   ProductKey = x.ProductItem.Product.Guid,
                                                   ProductName = x.ProductItem.Product.Name,

                                                   ProductItemId = x.ProductItemId,
                                                   ProductItemKey = x.ProductItem.Guid,
                                                   ProductItemName = x.ProductItem.Name,

                                                   ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                   ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                   ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                   AccountId = x.AccountId,
                                                   AccountKey = x.Account.Guid,
                                                   AccountDisplayName = x.Account.DisplayName,
                                                   AccountMobileNumber = x.Account.MobileNumber,

                                                   Amount = x.Amount,
                                                   RewardAmount = x.RewardAmount,
                                                   UserRewardAmount = x.UserRewardAmount,
                                                   CommissionAmount = x.CommissionAmount,
                                                   StartDate = x.StartDate,
                                                   EndDate = x.EndDate,
                                                   PaymentReference = x.PaymentReference,

                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name,
                                               })
                                              .Where(_Request.SearchCondition)
                                      .Sum(x => x.Amount);


                    _PurchaseHistoryOverview.RewardAmount = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                              .Select(x => new OVas.PurchaseHistory.List
                                              {
                                                  ParentId = x.Account.Owner.OwnerId,
                                                  ParentKey = x.Account.Owner.Owner.Guid,
                                                  ParentName = x.Account.Owner.Owner.Name,
                                                  ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                  ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                  SubParentId = x.Account.OwnerId,
                                                  SubParentKey = x.Account.Owner.Guid,
                                                  SubParentName = x.Account.Owner.Name,
                                                  SubParentDisplayName = x.Account.Owner.DisplayName,
                                                  SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                  CashierId = x.Account.SubOwnerId,
                                                  CashierKey = x.Account.SubOwner.Guid,
                                                  CashierName = x.Account.SubOwner.Name,
                                                  CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                  CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  TypeId = x.TypeId,
                                                  TypeKey = x.Type.Guid,
                                                  TypeCode = x.Type.SystemName,
                                                  TypeName = x.Type.Name,

                                                  BillerName = x.BillerName,

                                                  ProductId = x.ProductItem.ProductId,
                                                  ProductKey = x.ProductItem.Product.Guid,
                                                  ProductName = x.ProductItem.Product.Name,

                                                  ProductItemId = x.ProductItemId,
                                                  ProductItemKey = x.ProductItem.Guid,
                                                  ProductItemName = x.ProductItem.Name,

                                                  ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                  ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                  ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,

                                                  Amount = x.Amount,
                                                  RewardAmount = x.RewardAmount,
                                                  UserRewardAmount = x.UserRewardAmount,
                                                  CommissionAmount = x.CommissionAmount,
                                                  StartDate = x.StartDate,
                                                  EndDate = x.EndDate,
                                                  PaymentReference = x.PaymentReference,

                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                              })
                                             .Where(_Request.SearchCondition)
                                     .Sum(x => x.RewardAmount);
                    _PurchaseHistoryOverview.UserRewardAmount = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                              .Select(x => new OVas.PurchaseHistory.List
                                              {
                                                  ParentId = x.Account.Owner.OwnerId,
                                                  ParentKey = x.Account.Owner.Owner.Guid,
                                                  ParentName = x.Account.Owner.Owner.Name,
                                                  ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                                  ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                                  SubParentId = x.Account.OwnerId,
                                                  SubParentKey = x.Account.Owner.Guid,
                                                  SubParentName = x.Account.Owner.Name,
                                                  SubParentDisplayName = x.Account.Owner.DisplayName,
                                                  SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                                  CashierId = x.Account.SubOwnerId,
                                                  CashierKey = x.Account.SubOwner.Guid,
                                                  CashierName = x.Account.SubOwner.Name,
                                                  CashierDisplayName = x.Account.SubOwner.DisplayName,
                                                  CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  TypeId = x.TypeId,
                                                  TypeKey = x.Type.Guid,
                                                  TypeCode = x.Type.SystemName,
                                                  TypeName = x.Type.Name,

                                                  BillerName = x.BillerName,

                                                  ProductId = x.ProductItem.ProductId,
                                                  ProductKey = x.ProductItem.Product.Guid,
                                                  ProductName = x.ProductItem.Product.Name,

                                                  ProductItemId = x.ProductItemId,
                                                  ProductItemKey = x.ProductItem.Guid,
                                                  ProductItemName = x.ProductItem.Name,

                                                  ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                  ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                  ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                  AccountId = x.AccountId,
                                                  AccountKey = x.Account.Guid,
                                                  AccountDisplayName = x.Account.DisplayName,
                                                  AccountMobileNumber = x.Account.MobileNumber,

                                                  Amount = x.Amount,
                                                  RewardAmount = x.RewardAmount,
                                                  UserRewardAmount = x.UserRewardAmount,
                                                  CommissionAmount = x.CommissionAmount,
                                                  StartDate = x.StartDate,
                                                  EndDate = x.EndDate,
                                                  PaymentReference = x.PaymentReference,

                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,
                                              })
                                             .Where(_Request.SearchCondition)
                                     .Sum(x => x.UserRewardAmount);
                    _PurchaseHistoryOverview.CommissionAmount = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                           .Select(x => new OVas.PurchaseHistory.List
                                           {
                                               ParentId = x.Account.Owner.OwnerId,
                                               ParentKey = x.Account.Owner.Owner.Guid,
                                               ParentName = x.Account.Owner.Owner.Name,
                                               ParentDisplayName = x.Account.Owner.Owner.DisplayName,
                                               ParentMobileNumber = x.Account.Owner.Owner.MobileNumber,

                                               SubParentId = x.Account.OwnerId,
                                               SubParentKey = x.Account.Owner.Guid,
                                               SubParentName = x.Account.Owner.Name,
                                               SubParentDisplayName = x.Account.Owner.DisplayName,
                                               SubParentMobileNumber = x.Account.Owner.MobileNumber,

                                               CashierId = x.Account.SubOwnerId,
                                               CashierKey = x.Account.SubOwner.Guid,
                                               CashierName = x.Account.SubOwner.Name,
                                               CashierDisplayName = x.Account.SubOwner.DisplayName,
                                               CashierMobileNumber = x.Account.SubOwner.MobileNumber,

                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,

                                               TypeId = x.TypeId,
                                               TypeKey = x.Type.Guid,
                                               TypeCode = x.Type.SystemName,
                                               TypeName = x.Type.Name,

                                               BillerName = x.BillerName,

                                               ProductId = x.ProductItem.ProductId,
                                               ProductKey = x.ProductItem.Product.Guid,
                                               ProductName = x.ProductItem.Product.Name,

                                               ProductItemId = x.ProductItemId,
                                               ProductItemKey = x.ProductItem.Guid,
                                               ProductItemName = x.ProductItem.Name,

                                               ProductCategoryId = x.ProductItem.Product.CategoryId,
                                               ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                               ProductCategoryName = x.ProductItem.Product.Category.Name,

                                               AccountId = x.AccountId,
                                               AccountKey = x.Account.Guid,
                                               AccountDisplayName = x.Account.DisplayName,
                                               AccountMobileNumber = x.Account.MobileNumber,

                                               Amount = x.Amount,
                                               RewardAmount = x.RewardAmount,
                                               UserRewardAmount = x.UserRewardAmount,
                                               CommissionAmount = x.CommissionAmount,
                                               StartDate = x.StartDate,
                                               EndDate = x.EndDate,
                                               PaymentReference = x.PaymentReference,

                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           })
                                          .Where(_Request.SearchCondition)
                                  .Sum(x => x.CommissionAmount);

                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _PurchaseHistoryOverview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasProductPurchaseOverview", _Exception, _Request.UserReference, null, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse UpdateVasProduct(OVas.Product.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPINSTATUS, TUCPluginResource.HCPINSTATUSM);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    VASProduct _Details = _HCoreContext.VASProduct
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _Details.Name != _Request.Name)
                        {
                            _Details.Name = _Request.Name;
                        }
                        if (StatusId > 0)
                        {
                            _Details.StatusId = StatusId;
                        }
                        if (_Request.RewardPercentage != null && _Request.RewardPercentage != _Details.RewardPercentage)
                        {
                            _Details.RewardPercentage = _Request.RewardPercentage;
                        }
                        if (_Request.UserRewardPercentage != null && _Request.UserRewardPercentage != _Details.UserPercentage)
                        {
                            _Details.UserPercentage = _Request.UserRewardPercentage;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCPluginResource.HCP0127, TUCPluginResource.HCP0127M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasProductPurchase", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse UpdateVasProductItem(OVas.ProductItem.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                int StatusId = 0;
                if (!string.IsNullOrEmpty(_Request.StatusCode))
                {
                    StatusId = HCoreHelper.GetStatusId(_Request.StatusCode, _Request.UserReference);
                    if (StatusId < 1)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPINSTATUS, TUCPluginResource.HCPINSTATUSM);
                    }
                }
                using (_HCoreContext = new HCoreContext())
                {
                    VASProductItem _Details = _HCoreContext.VASProductItem
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _Details.Name != _Request.Name)
                        {
                            _Details.Name = _Request.Name;
                        }
                        if (StatusId > 0)
                        {
                            _Details.StatusId = StatusId;
                        }
                        //if (_Request.RewardPercentage != null && _Request.RewardPercentage != _Details.RewardPercentage)
                        //{
                        //    _Details.RewardPercentage = _Request.RewardPercentage;
                        //}
                        //if (_Request.UserRewardPercentage != null && _Request.UserRewardPercentage != _Details.UserPercentage)
                        //{
                        //    _Details.UserPercentage = _Request.UserRewardPercentage;
                        //}
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCPluginResource.HCP0127, TUCPluginResource.HCP0127M);
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasProductPurchase", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        internal OResponse VasPayment_Confirm(OVasOp.Purchase.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0121, TUCPluginResource.HCP0121M);
                }
                using (_HCoreContext = new HCoreContext())
                {

                    //if (CustomerDetails.StatusId != HelperStatus.Default.Active)
                    //{
                    //    _HCoreContext.Dispose();
                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0130, TUCPluginResource.HCP0130M);
                    //}
                    //var PurchaseDetails = _HCoreContext.VasPayment
                    //    .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey 
                    //    && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                    //if (PurchaseDetails == null)
                    //{
                    //    _HCoreContext.Dispose();
                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    //}
                    var PurchaseDetails = _HCoreContext.VasPayment
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .Select(x => new
                        {
                            ReferenceId = x.Id,
                            PaymentReference = x.PaymentReference,
                            OrderId = x.OrderId,
                            Amount = x.Amount,
                            AccountNumber = x.AccountNumber,
                            StatusId = x.StatusId,
                            PackageSlug = x.PackageName,
                            AccountId = x.AccountId,
                            //CategorySystemName = x.Product.Category.SystemName,
                            //ProductReference = x.Product.ReferenceKey,
                            //ProductItemReference = x.ReferenceKey,
                            //ProductItemName = x.Name,
                            //ProductName = x.Product.Name,
                            //ProductCategoryName = x.Product.Category.Name,
                            //ItemStatusId = x.StatusId,
                            //ProductStatusId = x.Product.StatusId,
                            //CategoryStatusId = x.Product.Category.StatusId,
                            //RewardPercentage = x.Product.RewardPercentage,
                            //UserPercentage = x.Product.UserPercentage,

                        }).FirstOrDefault();
                    var CustomerDetails = _HCoreContext.HCUAccount
                       .Where(x => x.Id == PurchaseDetails.AccountId)
                       .Select(x => new
                       {
                           CustomerId = x.Id,
                           Name = x.Name,
                           MobileNumber = x.MobileNumber,
                           EmailAddress = x.EmailAddress,
                           StatusId = x.StatusId
                       })
                       .FirstOrDefault();
                    if (PurchaseDetails != null)
                    {
                        if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                        {

                            //var _VasReversalResponse = Integration.CoralPayVas.CoralPayVas.CoralPayCancelTransaction(_AppConfig.VasCoralPayUrl, _AppConfig.VasCoralPayUserName, _AppConfig.VasCoralPayPassword,
                            //      PurchaseDetails.AccountNumber, ProductItemDetails.ProductItemReference, PurchaseDetails.PaymentReference, (double)PurchaseDetails.Amount,
                            //      PurchaseDetails.OrderId, CustomerDetails.Name, CustomerDetails.MobileNumber, CustomerDetails.EmailAddress, _Request.UserReference.AccountId);
                            var _VasReversalResponse = Integration.CoralPayVas.CoralPayVas.CoralPayCancelTransaction(
                                _AppConfig.VasCoralPayUrl,
                                _AppConfig.VasCoralPayUserName,
                                _AppConfig.VasCoralPayPassword,
                                 PurchaseDetails.AccountNumber,
                                 PurchaseDetails.PackageSlug,
                                 PurchaseDetails.PaymentReference,
                                 (double)PurchaseDetails.Amount,
                                 PurchaseDetails.OrderId,
                                 CustomerDetails.Name,
                                 CustomerDetails.MobileNumber,
                                 CustomerDetails.EmailAddress,
                                 _Request.UserReference.AccountId);
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PurchaseDetailsX = _HCoreContext.VasPayment
                                  .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                                 .FirstOrDefault();
                                if (PurchaseDetailsX != null)
                                {
                                    if (_VasReversalResponse.Status == "Success")
                                    {
                                        PurchaseDetailsX.StatusId = HelperStatus.BillPaymentStatus.Refunded;
                                    }
                                    PurchaseDetailsX.PaymentToken = "reversal attempted";
                                    _HCoreContext.SaveChanges();
                                }
                            }
                            HCoreHelper.LogData(LogType.Log, "_VasReversalResponse_Request", PurchaseDetails.ReferenceId.ToString(), _VasReversalResponse.RequestContent);
                            HCoreHelper.LogData(LogType.Log, "_VasReversalResponse_Request", PurchaseDetails.ReferenceId.ToString(), _VasReversalResponse.ResponseContent);
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCPluginResource.HCP0135, TUCPluginResource.HCP0135M);

                            //double PurchaseAmount = (double)PurchaseDetails.Amount;
                            //_ManageCoreTransaction = new ManageCoreTransaction();
                            //double Balance = _ManageCoreTransaction.GetAppUserBalance(_Request.UserReference.AccountId);
                            //if (Balance < PurchaseDetails.Amount)
                            //{
                            //    using (_HCoreContext = new HCoreContext())
                            //    {
                            //        var OpPurchaseDetails = _HCoreContext.VasPayment
                            //                            .Where(x => x.Id == _Request.ReferenceId
                            //                            && x.Guid == _Request.ReferenceKey
                            //                            && x.PaymentReference == _Request.PaymentReference
                            //                            && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                            //        OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                            //        OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //        OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                            //        OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                            //        _HCoreContext.SaveChanges();
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCPluginResource.HCP0129, TUCPluginResource.HCP0129M);
                            //    }
                            //}

                            //_CoreTransactionRequest = new OCoreTransaction.Request();
                            //_CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                            //_CoreTransactionRequest.UserReference = _Request.UserReference;
                            //_CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            //_CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            //_CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                            //_CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                            //_CoreTransactionRequest.AccountNumber = PurchaseDetails.AccountNumber;
                            //_CoreTransactionRequest.ReferenceNumber = PurchaseDetails.PaymentReference;
                            //_CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            //int PaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                            //if (ProductItemDetails.CategorySystemName == "lcctopup")
                            //{
                            //    PaymentTransactionType = TransactionType.Vas.LccTopup;
                            //}
                            //if (ProductItemDetails.CategorySystemName == "airtime")
                            //{
                            //    PaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                            //}
                            //if (ProductItemDetails.CategorySystemName == "tv")
                            //{
                            //    PaymentTransactionType = TransactionType.Vas.TVPayment;
                            //}
                            //if (ProductItemDetails.CategorySystemName == "electricity")
                            //{
                            //    PaymentTransactionType = TransactionType.Vas.ElectricityPayment;
                            //}
                            //_TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                            //{
                            //    UserAccountId = _Request.UserReference.AccountId,
                            //    ModeId = TransactionMode.Debit,
                            //    TypeId = PaymentTransactionType,
                            //    SourceId = TransactionSource.TUC,
                            //    Amount = PurchaseAmount,
                            //    Charge = 0,
                            //    TotalAmount = PurchaseAmount,
                            //});
                            //_TransactionItems.Add(new OCoreTransaction.TransactionItem
                            //{
                            //    UserAccountId = SystemAccounts.TUCVas,
                            //    ModeId = TransactionMode.Credit,
                            //    TypeId = PaymentTransactionType,
                            //    SourceId = TransactionSource.TUCVas,
                            //    Amount = PurchaseAmount,
                            //    Charge = 0,
                            //    TotalAmount = PurchaseAmount,
                            //});
                            //_CoreTransactionRequest.Transactions = _TransactionItems;
                            //_ManageCoreTransaction = new ManageCoreTransaction();
                            //var _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            //if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                            //{
                            //    var _VasResponse = Integration.CoralPayVas.CoralPayVas.CoralPayPayment(_AppConfig.VasCoralPayUrl, _AppConfig.VasCoralPayUserName, _AppConfig.VasCoralPayPassword,
                            //        PurchaseDetails.AccountNumber, ProductItemDetails.ProductItemReference, PurchaseDetails.PaymentReference, (double)PurchaseDetails.Amount,
                            //        PurchaseDetails.OrderId, CustomerDetails.Name, CustomerDetails.MobileNumber, CustomerDetails.EmailAddress, _Request.UserReference.AccountId);
                            //    using (_HCoreContext = new HCoreContext())
                            //    {
                            //        if (_VasResponse.Status == StatusSuccess)
                            //        {
                            //            double RewardAmount = 0;
                            //            double UserRewardAmount = 0;
                            //            double CommmissionAmount = 0;
                            //            if (ProductItemDetails.RewardPercentage != null && ProductItemDetails.RewardPercentage > 0 && ProductItemDetails.RewardPercentage < 10)
                            //            {
                            //                RewardAmount = HCoreHelper.GetPercentage(Math.Round(PurchaseAmount, 2), (double)ProductItemDetails.RewardPercentage);
                            //                if (RewardAmount > 0)
                            //                {
                            //                    UserRewardAmount = HCoreHelper.GetPercentage(Math.Round(RewardAmount, 2), (double)ProductItemDetails.UserPercentage);
                            //                    if (UserRewardAmount > 0)
                            //                    {
                            //                        CommmissionAmount = RewardAmount - UserRewardAmount;
                            //                    }
                            //                    else
                            //                    {
                            //                        CommmissionAmount = RewardAmount;
                            //                    }
                            //                }
                            //            }
                            //            var OpPurchaseDetails = _HCoreContext.VasPayment
                            //                                 .Where(x => x.Id == _Request.ReferenceId
                            //                                 && x.Guid == _Request.ReferenceKey
                            //                                 && x.PaymentReference == _Request.PaymentReference
                            //                                 && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                            //            OpPurchaseDetails.RewardAmount = RewardAmount;
                            //            OpPurchaseDetails.UserRewardAmount = UserRewardAmount;
                            //            OpPurchaseDetails.CommissionAmount = CommmissionAmount;
                            //            OpPurchaseDetails.Request = _VasResponse.RequestContent;
                            //            OpPurchaseDetails.Response = _VasResponse.ResponseContent;
                            //            OpPurchaseDetails.TransactionId = _TransactionResponse.ReferenceId;
                            //            OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Success;
                            //            OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                            //            OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //            OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                            //            if (_VasResponse.Token != null)
                            //            {
                            //                if (_VasResponse.Token.Value != null)
                            //                {
                            //                    OpPurchaseDetails.Comment = _VasResponse.Token.Value;
                            //                }
                            //            }
                            //            _HCoreContext.SaveChanges();

                            //            if (RewardAmount > 0)
                            //            {
                            //                _CoreTransactionRequest = new OCoreTransaction.Request();
                            //                _CoreTransactionRequest.CustomerId = OpPurchaseDetails.AccountId;
                            //                _CoreTransactionRequest.UserReference = _Request.UserReference;
                            //                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            //                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            //                _CoreTransactionRequest.ParentId = 3;
                            //                _CoreTransactionRequest.InvoiceAmount = PurchaseAmount;
                            //                _CoreTransactionRequest.ReferenceInvoiceAmount = PurchaseAmount;
                            //                _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                            //                _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                            //                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            //                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            //                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            //                {
                            //                    UserAccountId = SystemAccounts.TUCVas,
                            //                    ModeId = TransactionMode.Debit,
                            //                    TypeId = TransactionType.BillPayment,
                            //                    SourceId = TransactionSource.Merchant,
                            //                    Amount = UserRewardAmount,
                            //                    Comission = CommmissionAmount,
                            //                    TotalAmount = RewardAmount,
                            //                    TransactionDate = HCoreHelper.GetGMTDateTime(),
                            //                });
                            //                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            //                {
                            //                    UserAccountId = OpPurchaseDetails.AccountId,
                            //                    ModeId = TransactionMode.Credit,
                            //                    TypeId = TransactionType.BillPayment,
                            //                    SourceId = TransactionSource.TUC,
                            //                    Amount = UserRewardAmount,
                            //                    TotalAmount = UserRewardAmount,
                            //                    TransactionDate = HCoreHelper.GetGMTDateTime(),
                            //                });
                            //                _CoreTransactionRequest.Transactions = _TransactionItems;
                            //                _ManageCoreTransaction = new ManageCoreTransaction();
                            //                OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            //            }


                            //            var _Response = new OVasOp.Purchase.Confirm.Response
                            //            {
                            //                VasCategory = ProductItemDetails.ProductCategoryName,
                            //                VasProduct = ProductItemDetails.ProductName,
                            //                VasProductItem = ProductItemDetails.ProductItemName,
                            //                ReferenceId = OpPurchaseDetails.Id,
                            //                ReferenceKey = OpPurchaseDetails.Guid,
                            //                PaymentReference = OpPurchaseDetails.PaymentReference,
                            //                StartDate = (DateTime)OpPurchaseDetails.StartDate,
                            //                EndDate = OpPurchaseDetails.EndDate,
                            //                UserRewardAmount = UserRewardAmount,
                            //            };
                            //            if (_VasResponse.Token != null)
                            //            {
                            //                if (_VasResponse.Token.Value != null)
                            //                {
                            //                    _Response.Token = _VasResponse.Token.Value;
                            //                }
                            //            }
                            //            if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                            //            {
                            //                _Response.StatusCode = "paymentstatus.success";
                            //                _Response.StatusName = "Success";
                            //                _Response.Message = "Payment successful";
                            //            }
                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0126, TUCPluginResource.HCP0126M);
                            //        }
                            //        else
                            //        {
                            //            var OpPurchaseDetails = _HCoreContext.VasPayment
                            //                                 .Where(x => x.Id == _Request.ReferenceId
                            //                                 && x.Guid == _Request.ReferenceKey
                            //                                 && x.PaymentReference == _Request.PaymentReference
                            //                                 && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                            //            OpPurchaseDetails.Request = _VasResponse.RequestContent;
                            //            OpPurchaseDetails.Response = _VasResponse.ResponseContent;
                            //            OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                            //            OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                            //            OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //            OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                            //            _HCoreContext.SaveChanges();
                            //            var _Response = new OVasOp.Purchase.Confirm.Response
                            //            {
                            //                VasCategory = ProductItemDetails.ProductCategoryName,
                            //                VasProduct = ProductItemDetails.ProductName,
                            //                VasProductItem = ProductItemDetails.ProductItemName,
                            //                ReferenceId = OpPurchaseDetails.Id,
                            //                ReferenceKey = OpPurchaseDetails.Guid,
                            //                PaymentReference = OpPurchaseDetails.PaymentReference,
                            //                StartDate = (DateTime)OpPurchaseDetails.StartDate,
                            //                EndDate = OpPurchaseDetails.EndDate,
                            //                Data = _VasResponse
                            //            };
                            //            if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                            //            {
                            //                _Response.StatusCode = "paymentstatus.failed";
                            //                _Response.StatusName = "Failed";
                            //                _Response.Message = "Payment failed";
                            //            }
                            //            #region Refund Customer
                            //            _CoreTransactionRequest = new OCoreTransaction.Request();
                            //            _CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                            //            _CoreTransactionRequest.UserReference = _Request.UserReference;
                            //            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                            //            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                            //            _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                            //            _CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                            //            _CoreTransactionRequest.AccountNumber = PurchaseDetails.AccountNumber;
                            //            _CoreTransactionRequest.ReferenceNumber = PurchaseDetails.PaymentReference;
                            //            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                            //            int RefundPaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                            //            if (ProductItemDetails.CategorySystemName == "lcctopup")
                            //            {
                            //                RefundPaymentTransactionType = TransactionType.VasRefund.LccTopup;
                            //            }
                            //            if (ProductItemDetails.CategorySystemName == "airtime")
                            //            {
                            //                RefundPaymentTransactionType = TransactionType.VasRefund.AirtimePurchase;
                            //            }
                            //            if (ProductItemDetails.CategorySystemName == "tv")
                            //            {
                            //                RefundPaymentTransactionType = TransactionType.VasRefund.TVPayment;
                            //            }
                            //            if (ProductItemDetails.CategorySystemName == "electricity")
                            //            {
                            //                RefundPaymentTransactionType = TransactionType.VasRefund.ElectricityPayment;
                            //            }
                            //            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                            //            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            //            {
                            //                UserAccountId = SystemAccounts.TUCVas,
                            //                ModeId = TransactionMode.Debit,
                            //                TypeId = RefundPaymentTransactionType,
                            //                SourceId = TransactionSource.TUCVas,
                            //                Amount = PurchaseAmount,
                            //                Charge = 0,
                            //                TotalAmount = PurchaseAmount,
                            //            });
                            //            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                            //            {
                            //                UserAccountId = _Request.UserReference.AccountId,
                            //                ModeId = TransactionMode.Credit,
                            //                TypeId = RefundPaymentTransactionType,
                            //                SourceId = TransactionSource.TUC,
                            //                Amount = PurchaseAmount,
                            //                Charge = 0,
                            //                TotalAmount = PurchaseAmount,
                            //            });
                            //            _CoreTransactionRequest.Transactions = _TransactionItems;
                            //            _ManageCoreTransaction = new ManageCoreTransaction();
                            //            _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                            //            #endregion
                            //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0126, TUCPluginResource.HCP0126M);
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    var OpPurchaseDetails = _HCoreContext.VasPayment
                            //                             .Where(x => x.Id == _Request.ReferenceId
                            //                             && x.Guid == _Request.ReferenceKey
                            //                             && x.PaymentReference == _Request.PaymentReference
                            //                             && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                            //    OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                            //    OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                            //    OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //    OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                            //    _HCoreContext.SaveChanges();
                            //    var _Response = new OVasOp.Purchase.Confirm.Response
                            //    {
                            //        VasCategory = ProductItemDetails.ProductCategoryName,
                            //        VasProduct = ProductItemDetails.ProductName,
                            //        VasProductItem = ProductItemDetails.ProductItemName,
                            //        ReferenceId = OpPurchaseDetails.Id,
                            //        ReferenceKey = OpPurchaseDetails.Guid,
                            //        PaymentReference = OpPurchaseDetails.PaymentReference,
                            //        StartDate = (DateTime)OpPurchaseDetails.StartDate,
                            //        EndDate = OpPurchaseDetails.EndDate,
                            //    };
                            //    if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                            //    {
                            //        _Response.StatusCode = "paymentstatus.failed";
                            //        _Response.StatusName = "Failed";
                            //        _Response.Message = "Payment failed";
                            //    }
                            //    #region Send Response
                            //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, TUCPluginResource.HCP0132, TUCPluginResource.HCP0132M);
                            //    #endregion
                            //}
                        }
                        else
                        {
                            //var _Response = new OVasOp.Purchase.Confirm.Response
                            //{
                            //    VasCategory = ProductItemDetails.ProductCategoryName,
                            //    VasProduct = ProductItemDetails.ProductName,
                            //    VasProductItem = ProductItemDetails.ProductItemName,
                            //    ReferenceId = PurchaseDetails.Id,
                            //    ReferenceKey = PurchaseDetails.Guid,
                            //    PaymentReference = PurchaseDetails.PaymentReference,
                            //    StartDate = (DateTime)PurchaseDetails.StartDate,
                            //    EndDate = PurchaseDetails.EndDate,
                            //};
                            //if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Initialized)
                            //{
                            //    _Response.StatusCode = "paymentstatus.initialized";
                            //    _Response.StatusName = "Initialized";
                            //}
                            //if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Processing)
                            //{
                            //    _Response.StatusCode = "paymentstatus.processing";
                            //    _Response.StatusName = "Processing";
                            //}
                            //if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                            //{
                            //    _Response.StatusCode = "paymentstatus.success";
                            //    _Response.StatusName = "Success";
                            //}
                            //if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                            //{
                            //    _Response.StatusCode = "paymentstatus.failed";
                            //    _Response.StatusName = "Failed";
                            //}
                            //if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Refunded)
                            //{
                            //    _Response.StatusCode = "paymentstatus.refunded";
                            //    _Response.StatusName = "Refunded";
                            //}
                            //if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Cancelled)
                            //{
                            //    _Response.StatusCode = "paymentstatus.cancelled";
                            //    _Response.StatusName = "Cancelled";
                            //}
                            //_HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0133, TUCPluginResource.HCP0133M);
                        }
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0134, TUCPluginResource.HCP0134M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("VasPayment_Cancel", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        FrameworkVasOperations _FrameworkVasOperations;
        internal OResponse VerifyVasPayment(OReference _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    OVas.PurchaseHistory.Details _Details = _HCoreContext.VasPayment
                            .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                        .Where(x => x.Id == _Request.ReferenceId
                        && x.Guid == _Request.ReferenceKey)
                                                .Select(x => new OVas.PurchaseHistory.Details
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,


                                                    ProductId = x.ProductItem.ProductId,
                                                    ProductKey = x.ProductItem.Product.Guid,
                                                    ProductName = x.ProductItem.Product.Name,

                                                    ProductItemId = x.ProductItemId,
                                                    ProductItemKey = x.ProductItem.Guid,
                                                    ProductItemName = x.ProductItem.Name,

                                                    ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                    ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                    ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                    AccountId = x.AccountId,
                                                    AccountKey = x.Account.Guid,
                                                    AccountDisplayName = x.Account.DisplayName,
                                                    AccountMobileNumber = x.Account.MobileNumber,

                                                    Amount = x.Amount,
                                                    RewardAmount = x.RewardAmount,
                                                    UserRewardAmount = x.UserRewardAmount,
                                                    CommissionAmount = x.CommissionAmount,
                                                    StartDate = x.StartDate,
                                                    EndDate = x.EndDate,
                                                    PaymentReference = x.PaymentReference,

                                                    BillerName = x.BillerName,

                                                    Comment = x.Comment,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByKey = x.CreatedBy.Guid,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                    ModifyDate = x.ModifyDate,
                                                    ModifyByKey = x.ModifyBy.Guid,
                                                    ModifyById = x.ModifyById,
                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                    PaymentToken = x.PaymentToken,
                                                    TransactionId = x.TransactionId,
                                                    TransactionKey = x.Transaction.Guid,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).FirstOrDefault();
                    if (_Details != null)
                    {
                        _HCoreContext.Dispose();
                        if (_Details.StatusId == HelperStatus.BillPaymentStatus.Pending)
                        {
                            _FrameworkVasOperations = new FrameworkVasOperations();
                            _FrameworkVasOperations.VasPayment_Verify(_Details.ReferenceId);
                            using (_HCoreContext = new HCoreContext())
                            {
                                OVas.PurchaseHistory.Details _DetailsN = _HCoreContext.VasPayment
                                        .Where(x => x.Account.CountryId == _Request.UserReference.SystemCountry)
                                    .Where(x => x.Id == _Request.ReferenceId
                                    && x.Guid == _Request.ReferenceKey)
                                                            .Select(x => new OVas.PurchaseHistory.Details
                                                            {
                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,


                                                                ProductId = x.ProductItem.ProductId,
                                                                ProductKey = x.ProductItem.Product.Guid,
                                                                ProductName = x.ProductItem.Product.Name,

                                                                ProductItemId = x.ProductItemId,
                                                                ProductItemKey = x.ProductItem.Guid,
                                                                ProductItemName = x.ProductItem.Name,

                                                                ProductCategoryId = x.ProductItem.Product.CategoryId,
                                                                ProductCategoryKey = x.ProductItem.Product.Category.Guid,
                                                                ProductCategoryName = x.ProductItem.Product.Category.Name,

                                                                AccountId = x.AccountId,
                                                                AccountKey = x.Account.Guid,
                                                                AccountDisplayName = x.Account.DisplayName,
                                                                AccountMobileNumber = x.Account.MobileNumber,

                                                                Amount = x.Amount,
                                                                RewardAmount = x.RewardAmount,
                                                                UserRewardAmount = x.UserRewardAmount,
                                                                CommissionAmount = x.CommissionAmount,
                                                                StartDate = x.StartDate,
                                                                EndDate = x.EndDate,
                                                                PaymentReference = x.PaymentReference,

                                                                BillerName = x.BillerName,

                                                                Comment = x.Comment,
                                                                CreateDate = x.CreateDate,
                                                                CreatedById = x.CreatedById,
                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                ModifyDate = x.ModifyDate,
                                                                ModifyByKey = x.ModifyBy.Guid,
                                                                ModifyById = x.ModifyById,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                PaymentToken = x.PaymentToken,
                                                                TransactionId = x.TransactionId,
                                                                TransactionKey = x.Transaction.Guid,

                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,
                                                            }).FirstOrDefault();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _DetailsN, TUCPluginResource.HCP0200, "Transaction verified successfully");
                            }
                        }
                        else
                        {
                            var _VasResponse = Integration.CoralPayVas.CoralPayVas.CoralPayTransactionEnquiry(_AppConfig.VasCoralPayUrl, _AppConfig.VasCoralPayUserName, _AppConfig.VasCoralPayPassword, _Details.PaymentReference);
                            if (_VasResponse.Status == StatusSuccess)
                            {
                                _Details.Response = _VasResponse.ResponseContent;
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, TUCPluginResource.HCP0200, "Transaction verified successfully");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, _VasResponse.StatusMessage);
                            }
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasProductPurchase", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }

    }
}
