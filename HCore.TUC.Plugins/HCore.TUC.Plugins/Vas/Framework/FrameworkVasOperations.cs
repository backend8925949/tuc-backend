//==================================================================================
// FileName: FrameworkVasOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to Vas Operations
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Plugins.Vas.Object;
using HCore.TUC.Plugins.Resource;
using HCore.Operations;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant.Helpers;
using Akka.Util.Internal;

namespace HCore.TUC.Plugins.Vas.Framework
{
    internal class FrameworkVasOperations
    {
        internal class TVasPayment
        {
            internal long Id { get; set; }
            internal long AccountId { get; set; }
            internal long? ProductItemId { get; set; }
            internal double? Amount { get; set; }
            internal string? PaymentReference { get; set; }
            internal string? AccountNumber { get; set; }
        }

        public List<TVasPayment>? _TVasPayments;
        HCoreContext? _HCoreContext;
        VasPayment? _VasPayment;
        ManageCoreTransaction? _ManageCoreTransaction;
        OCoreTransaction.Request? _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem>? _TransactionItems;

        /// <summary>
        /// Description: Gets the vas options.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVasOptions(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    List<OVasOp.Option.Category> _List = _HCoreContext.VASCategory
                        .Where(x => x.StatusId == HelperStatus.Default.Active && x.CountryId == _Request.UserReference.SystemCountry)
                                                .Select(x => new OVasOp.Option.Category
                                                {
                                                    ReferenceId = x.Id,
                                                    Name = x.Name,
                                                    SystemName = x.SystemName,
                                                })
                                             .ToList();

                    var Products = _HCoreContext.VASProduct
                      .Where(x => x.StatusId == HelperStatus.Default.Active && x.Category.CountryId == _Request.UserReference.SystemCountry)
                                              .Select(x => new OVasOp.Option.Product
                                              {
                                                  ReferenceId = x.Id,
                                                  Name = x.Name,
                                                  CategoryId = x.CategoryId,
                                                  MaximumRewardPercentage = (double)x.RewardPercentage,
                                                  UserPercentage = x.UserPercentage,
                                                  IconUrl = x.IconStorage.Path,
                                              })
                                           .ToList();

                    var ProductItems = _HCoreContext.VASProductItem
                      .Where(x => x.StatusId == HelperStatus.Default.Active)
                                              .Select(x => new OVasOp.Option.ProductItem
                                              {
                                                  ReferenceId = x.Id,
                                                  ProductId = x.ProductId,
                                                  Name = x.Name,
                                                  Amount = (double)x.Amount,
                                                  MaximumRewardPercentage = (double)x.UserPercentage,
                                                  UserPercentage = x.UserPercentage,
                                                  Validity = "n/a",
                                              })
                                           .ToList();

                    foreach (var Product in Products)
                    {
                        if (Product.MaximumRewardPercentage > 0 && Product.UserPercentage > 0)
                        {
                            Product.MaximumRewardPercentage = HCoreHelper.GetPercentage(Product.MaximumRewardPercentage, (double)Product.UserPercentage);
                        }
                        Product.ProductItems = ProductItems.Where(x => x.ProductId == Product.ReferenceId).ToList();
                        if (Product.ProductItems != null && Product.ProductItems.Count > 0)
                        {
                            foreach (var ProductItem in Product.ProductItems)
                            {
                                if (Product.MaximumRewardPercentage > 0 && Product.UserPercentage > 0)
                                {
                                    ProductItem.MaximumRewardPercentage = HCoreHelper.GetPercentage(Product.MaximumRewardPercentage, (double)Product.UserPercentage);
                                    ProductItem.RewardAmount = HCoreHelper.GetPercentage(ProductItem.Amount, Product.MaximumRewardPercentage);
                                }

                                if (ProductItem.RewardAmount > 0)
                                {
                                    ProductItem.RewardAmountText = "EARN " + ProductItem.RewardAmount + " ON THIS TOPUP";
                                }
                                else
                                {
                                    ProductItem.RewardAmountText = "EARN UPTO " + ProductItem.MaximumRewardPercentage + "% ON THIS TOPUP";
                                }
                            }
                        }



                        if (!string.IsNullOrEmpty(Product.IconUrl))
                        {
                            Product.IconUrl = _AppConfig.StorageUrl + Product.IconUrl;
                        }
                        else
                        {
                            Product.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (Product.MaximumRewardPercentage == 0)
                        {
                            Product.MaximumRewardPercentage = Product.ProductItems.OrderBy(x => x.MaximumRewardPercentage).Select(x => x.MaximumRewardPercentage).FirstOrDefault();
                        }
                    }
                    foreach (var ListItem in _List)
                    {
                        ListItem.Products = Products.Where(x => x.CategoryId == ListItem.ReferenceId).ToList();
                        ListItem.MaximumRewardPercentage = ListItem.Products.OrderBy(x => x.MaximumRewardPercentage).Select(x => x.MaximumRewardPercentage).FirstOrDefault();
                    }
                    _HCoreContext.Dispose();
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _List, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasOptions", _Exception, _Request.UserReference, null, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }

        public class VasPaymentMetaData
        {
            public string? CategorySystemName { get; set; }
            public string? ProductReference { get; set; }
            public string? ProductItemReference { get; set; }
            public string? ProductItemName { get; set; }
            public string? ProductName { get; set; }
            public long? ProductCategoryId { get; set; }
            public string? ProductCategoryName { get; set; }
            public int? ItemStatusId { get; set; }
            public int? ProductStatusId { get; set; }
            public int? CategoryStatusId { get; set; }
            public double? RewardPercentage { get; set; }
            public double? UserPercentage { get; set; }
            public double? Amount { get; set; }
        }

        VasPaymentMetaData? _VasPaymentMetaData;

        /// <summary>
        /// Description: Vas payment initialize.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VasPayment_Initialize(OVasOp.Purchase.Initialize.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0 && _Request.ProductId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.AccountNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0121, TUCPluginResource.HCP0121M);
                }
                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0122, TUCPluginResource.HCP0122M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    long CustomerStatusId = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.UserReference.AccountId)
                        .Select(x => x.StatusId)
                        .FirstOrDefault();
                    if (CustomerStatusId != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0130, TUCPluginResource.HCP0130M);
                    }

                    _VasPaymentMetaData = new VasPaymentMetaData();
                    if (_Request.ProductId > 0)
                    {
                        _VasPaymentMetaData = _HCoreContext.VASProduct
                                              .Where(x => x.Id == _Request.ProductId)
                                              .Select(x => new VasPaymentMetaData
                                              {
                                                  ProductReference = x.ReferenceKey,
                                                  ProductName = x.Name,
                                                  ProductCategoryId = x.Category.Id,
                                                  ProductCategoryName = x.Category.Name,
                                                  ItemStatusId = x.StatusId,
                                                  ProductStatusId = x.StatusId,
                                                  CategoryStatusId = x.Category.StatusId,
                                                  RewardPercentage = x.RewardPercentage,
                                                  UserPercentage = x.UserPercentage,
                                                  Amount = x.Amount,
                                              }).FirstOrDefault();
                    }
                    if (_Request.ReferenceId > 0)
                    {
                        _VasPaymentMetaData = _HCoreContext.VASProductItem
                                              .Where(x => x.Id == _Request.ReferenceId)
                                              .Select(x => new VasPaymentMetaData
                                              {
                                                  ProductReference = x.Product.ReferenceKey,
                                                  ProductItemReference = x.ReferenceKey,
                                                  ProductItemName = x.Name,
                                                  ProductName = x.Product.Name,
                                                  ProductCategoryId = x.Product.Category.Id,
                                                  ProductCategoryName = x.Product.Category.Name,
                                                  ItemStatusId = x.StatusId,
                                                  ProductStatusId = x.Product.StatusId,
                                                  CategoryStatusId = x.Product.Category.StatusId,
                                                  RewardPercentage = x.Product.RewardPercentage,
                                                  UserPercentage = x.Product.UserPercentage,
                                                  Amount = x.Amount,
                                              }).FirstOrDefault();
                    }
                    if (_VasPaymentMetaData == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                    if (_VasPaymentMetaData.ItemStatusId == HelperStatus.Default.Active && _VasPaymentMetaData.ProductStatusId == HelperStatus.Default.Active && _VasPaymentMetaData.CategoryStatusId == HelperStatus.Default.Active)
                    {
                        if (_VasPaymentMetaData.Amount > 0 && _VasPaymentMetaData.Amount != _Request.Amount)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0136, TUCPluginResource.HCP0136M.Replace("{{amount}]", _VasPaymentMetaData.Amount.ToString()));
                        }
                        DateTime ActiveDate = HCoreHelper.GetGMTDate();
                        if (_VasPaymentMetaData.ProductCategoryName == "Airtime")
                        {
                            double? PurchaseSum = _HCoreContext.VasPayment.Where(x => x.CreateDate.Value.Date == ActiveDate && x.AccountId == _Request.AccountId && x.ProductItem.Product.CategoryId == _VasPaymentMetaData.ProductCategoryId && x.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(x => x.Amount);
                            if (_Request.Amount > 20000)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0138, "Airtime amount must be less than 20000");
                            }
                            else if (PurchaseSum <= 20000)
                            {

                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0138, TUCPluginResource.HCP0138M);
                            }
                        }
                        else if(_VasPaymentMetaData.ProductCategoryName == "Tv")
                        {
                            double? PurchaseSum = _HCoreContext.VasPayment.Where(x => x.CreateDate.Value.Date == ActiveDate && x.AccountId == _Request.AccountId && x.ProductItem.Product.Category.Name == "Tv" && x.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(x => x.Amount);
                            if (_Request.Amount > 80000)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0138, "TV recharge amount must be less than 80000");
                            }
                            else if (PurchaseSum <= 80000)
                            {

                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0139, "Your daily limit for tv recharge payments is reached. Please try after sometime");
                            }
                        }
                        else if (_VasPaymentMetaData.ProductCategoryName == "Electricity")
                        {
                            double? PurchaseSum = _HCoreContext.VasPayment.Where(x => x.CreateDate.Value.Date == ActiveDate && x.AccountId == _Request.AccountId && x.ProductItem.Product.Category.Name == "Electricity" && x.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(x => x.Amount);
                            if (_Request.Amount > 50000)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0138, "Electricity amount must be less than 50000");
                            }
                            else if (PurchaseSum <= 50000)
                            {

                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0139, "Your daily limit for electricity payments is reached. Please try after sometime");
                            }
                        }
                        else
                        {
                            double? PurchaseSum = _HCoreContext.VasPayment.Where(x => x.CreateDate.Value.Date == ActiveDate && x.AccountId == _Request.AccountId && (x.ProductItem.Product.Category.Name != "Airtime" && x.ProductItem.Product.Category.Name != "Tv" && x.ProductItem.Product.Category.Name != "Electricity") && x.StatusId == HelperStatus.BillPaymentStatus.Success).Sum(x => x.Amount);
                            if (_Request.Amount > 20000)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0138, "Payment amount must be less than 20000");
                            }
                            else if (PurchaseSum <= 20000)
                            {

                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0139, "Your daily limit for payments is reached. Please try after sometime");
                            }
                        }
                        _HCoreContext.Dispose();

                        _ManageCoreTransaction = new ManageCoreTransaction();

                        if (_Request.UserReference.CountryId == 87)
                        {
                            double RewardAmount = 0;
                            double UserRewardAmount = 0;
                            double CommmissionAmount = 0;
                            if (_VasPaymentMetaData.RewardPercentage != null && _VasPaymentMetaData.RewardPercentage > 0 && _VasPaymentMetaData.RewardPercentage < 10)
                            {
                                RewardAmount = HCoreHelper.GetPercentage(Math.Round(_Request.Amount, 2), (double)_VasPaymentMetaData.RewardPercentage);
                                if (RewardAmount > 0)
                                {
                                    UserRewardAmount = HCoreHelper.GetPercentage(Math.Round(RewardAmount, 2), (double)_VasPaymentMetaData.UserPercentage);
                                    if (UserRewardAmount > 0)
                                    {
                                        CommmissionAmount = RewardAmount - UserRewardAmount;
                                    }
                                    else
                                    {
                                        CommmissionAmount = RewardAmount;
                                    }
                                }
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _VasPayment = new VasPayment();
                                _VasPayment.Guid = HCoreHelper.GenerateGuid();
                                if (_Request.ReferenceId > 0)
                                {
                                    _VasPayment.ProductItemId = _Request.ReferenceId;
                                }
                                if (_Request.ProductId > 0)
                                {
                                    _VasPayment.ProductId = _Request.ProductId;
                                }
                                if (_Request.ReferenceId == 1)
                                {
                                    _VasPayment.TypeId = 564;
                                }
                                else if (_Request.ReferenceId == 2)
                                {
                                    _VasPayment.TypeId = 566;
                                }
                                else
                                {
                                    _VasPayment.TypeId = 565;
                                }
                                _VasPayment.AccountId = _Request.UserReference.AccountId;
                                _VasPayment.AccountNumber = _Request.AccountNumber;
                                _VasPayment.Amount = _Request.Amount;
                                _VasPayment.RewardAmount = RewardAmount;
                                _VasPayment.UserRewardAmount = UserRewardAmount;
                                _VasPayment.CommissionAmount = CommmissionAmount;
                                _VasPayment.PaymentSource = "wallet";
                                _VasPayment.BillerName = _VasPaymentMetaData.ProductName;
                                _VasPayment.PackageName = _VasPaymentMetaData.ProductItemName;
                                _VasPayment.PaymentReference = HCoreHelper.GenerateDateString();
                                _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                                _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                                _VasPayment.CreatedById = _Request.UserReference.AccountId;
                                _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Initialized;
                                _HCoreContext.VasPayment.Add(_VasPayment);
                                _HCoreContext.SaveChanges();
                                var _Response = new OVasOp.Purchase.Initialize.Response
                                {
                                    VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                    VasProduct = _VasPaymentMetaData.ProductName,
                                    VasProductItem = _VasPaymentMetaData.ProductItemName,
                                    ReferenceId = _VasPayment.Id,
                                    ReferenceKey = _VasPayment.Guid,
                                    PaymentReference = _VasPayment.PaymentReference,
                                    StartDate = (DateTime)_VasPayment.StartDate,
                                    StatusCode = "paymentstatus.initialized",
                                    StatusName = "Initialized",
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0124, TUCPluginResource.HCP0124M);
                            }
                        }
                        else if (_VasPaymentMetaData.ProductName == "MTN" || _VasPaymentMetaData.ProductName == "AIRTEL" || _VasPaymentMetaData.ProductName == "GLO" || _VasPaymentMetaData.ProductName == "9MOBILE")
                        {
                            double RewardAmount = 0;
                            double UserRewardAmount = 0;
                            double CommmissionAmount = 0;
                            if (_VasPaymentMetaData.RewardPercentage != null && _VasPaymentMetaData.RewardPercentage > 0 && _VasPaymentMetaData.RewardPercentage < 10)
                            {
                                RewardAmount = HCoreHelper.GetPercentage(Math.Round(_Request.Amount, 2), (double)_VasPaymentMetaData.RewardPercentage);
                                if (RewardAmount > 0)
                                {
                                    UserRewardAmount = HCoreHelper.GetPercentage(Math.Round(RewardAmount, 2), (double)_VasPaymentMetaData.UserPercentage);
                                    if (UserRewardAmount > 0)
                                    {
                                        CommmissionAmount = RewardAmount - UserRewardAmount;
                                    }
                                    else
                                    {
                                        CommmissionAmount = RewardAmount;
                                    }
                                }
                            }
                            using (_HCoreContext = new HCoreContext())
                            {
                                _VasPayment = new VasPayment();
                                _VasPayment.Guid = HCoreHelper.GenerateGuid();
                                if (_Request.ReferenceId > 0)
                                {
                                    _VasPayment.ProductItemId = _Request.ReferenceId;
                                }
                                if (_Request.ProductId > 0)
                                {
                                    _VasPayment.ProductId = _Request.ProductId;
                                }
                                if (_Request.ReferenceId == 1)
                                {
                                    _VasPayment.TypeId = 564;
                                }
                                else if (_Request.ReferenceId == 2)
                                {
                                    _VasPayment.TypeId = 566;
                                }
                                else
                                {
                                    _VasPayment.TypeId = 565;
                                }
                                _VasPayment.AccountId = _Request.UserReference.AccountId;
                                _VasPayment.AccountNumber = _Request.AccountNumber;
                                _VasPayment.Amount = _Request.Amount;
                                _VasPayment.RewardAmount = RewardAmount;
                                _VasPayment.UserRewardAmount = UserRewardAmount;
                                _VasPayment.CommissionAmount = CommmissionAmount;
                                _VasPayment.PaymentSource = "wallet";
                                _VasPayment.BillerName = _VasPaymentMetaData.ProductName;
                                _VasPayment.PackageName = _VasPaymentMetaData.ProductItemName;
                                _VasPayment.PaymentReference = HCoreHelper.GenerateDateString();
                                _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                                _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                                _VasPayment.CreatedById = _Request.UserReference.AccountId;
                                _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Initialized;
                                _HCoreContext.VasPayment.Add(_VasPayment);
                                _HCoreContext.SaveChanges();
                                var _Response = new OVasOp.Purchase.Initialize.Response
                                {
                                    VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                    VasProduct = _VasPaymentMetaData.ProductName,
                                    VasProductItem = _VasPaymentMetaData.ProductItemName,
                                    ReferenceId = _VasPayment.Id,
                                    ReferenceKey = _VasPayment.Guid,
                                    PaymentReference = _VasPayment.PaymentReference,
                                    StartDate = (DateTime)_VasPayment.StartDate,
                                    StatusCode = "paymentstatus.initialized",
                                    StatusName = "Initialized",
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0124, TUCPluginResource.HCP0124M);
                            }
                        }
                        else
                        {
                            var _VasResponse = Integration.CoralPayVas.CoralPayVas.CoralPayAccountVerify(_AppConfig.VasCoralPayUrl, _AppConfig.VasCoralPayUserName, _AppConfig.VasCoralPayPassword, _Request.AccountNumber, _VasPaymentMetaData.ProductReference, _VasPaymentMetaData.ProductItemReference);
                            if (_VasResponse.Status == HCoreConstant.StatusSuccess)
                            {
                                double RewardAmount = 0;
                                double UserRewardAmount = 0;
                                double CommmissionAmount = 0;
                                if (_VasPaymentMetaData.RewardPercentage != null && _VasPaymentMetaData.RewardPercentage > 0 && _VasPaymentMetaData.RewardPercentage < 10)
                                {
                                    RewardAmount = HCoreHelper.GetPercentage(Math.Round(_Request.Amount, 2), (double)_VasPaymentMetaData.RewardPercentage);
                                    if (RewardAmount > 0)
                                    {
                                        UserRewardAmount = HCoreHelper.GetPercentage(Math.Round(RewardAmount, 2), (double)_VasPaymentMetaData.UserPercentage);
                                        if (UserRewardAmount > 0)
                                        {
                                            CommmissionAmount = RewardAmount - UserRewardAmount;
                                        }
                                        else
                                        {
                                            CommmissionAmount = RewardAmount;
                                        }
                                    }
                                }
                                using (_HCoreContext = new HCoreContext())
                                {
                                    _VasPayment = new VasPayment();
                                    _VasPayment.Guid = HCoreHelper.GenerateGuid();
                                    if (_Request.ReferenceId > 0)
                                    {
                                        _VasPayment.ProductItemId = _Request.ReferenceId;
                                    }
                                    if (_Request.ProductId > 0)
                                    {
                                        _VasPayment.ProductId = _Request.ProductId;
                                    }
                                    if (_Request.ReferenceId == 1)
                                    {
                                        _VasPayment.TypeId = 564;
                                    }
                                    else if (_Request.ReferenceId == 2)
                                    {
                                        _VasPayment.TypeId = 566;
                                    }
                                    else
                                    {
                                        _VasPayment.TypeId = 565;
                                    }
                                    _VasPayment.AccountId = _Request.UserReference.AccountId;
                                    _VasPayment.AccountNumber = _Request.AccountNumber;
                                    _VasPayment.Amount = _Request.Amount;
                                    _VasPayment.RewardAmount = RewardAmount;
                                    _VasPayment.UserRewardAmount = UserRewardAmount;
                                    _VasPayment.CommissionAmount = CommmissionAmount;
                                    _VasPayment.PaymentSource = "wallet";
                                    _VasPayment.OrderId = _VasResponse.OrderId;
                                    _VasPayment.Request = _VasResponse.RequestContent;
                                    _VasPayment.Response = _VasResponse.ResponseContent;
                                    _VasPayment.BillerName = _VasPaymentMetaData.ProductName;
                                    _VasPayment.PackageName = _VasPaymentMetaData.ProductItemName;
                                    _VasPayment.PaymentReference = HCoreHelper.GenerateDateString();
                                    _VasPayment.StartDate = HCoreHelper.GetGMTDateTime();
                                    _VasPayment.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _VasPayment.CreatedById = _Request.UserReference.AccountId;
                                    _VasPayment.StatusId = HelperStatus.BillPaymentStatus.Initialized;
                                    _HCoreContext.VasPayment.Add(_VasPayment);
                                    _HCoreContext.SaveChanges();
                                    var _Response = new OVasOp.Purchase.Initialize.Response
                                    {
                                        VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                        VasProduct = _VasPaymentMetaData.ProductName,
                                        VasProductItem = _VasPaymentMetaData.ProductItemName,
                                        ReferenceId = _VasPayment.Id,
                                        ReferenceKey = _VasPayment.Guid,
                                        PaymentReference = _VasPayment.PaymentReference,
                                        StartDate = (DateTime)_VasPayment.StartDate,
                                        StatusCode = "paymentstatus.initialized",
                                        StatusName = "Initialized",
                                        Data = _VasResponse
                                    };
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0124, TUCPluginResource.HCP0124M);
                                }
                            }
                            else
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0131, TUCPluginResource.HCP0131M + _VasResponse.StatusMessage);
                            }
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0123, TUCPluginResource.HCP0123M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("VasPayment_Initialize", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Vas payment confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VasPayment_Confirm(OVasOp.Purchase.Confirm.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0 && _Request.ProductId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0121, TUCPluginResource.HCP0121M);
                }
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0125, TUCPluginResource.HCP0125M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var CustomerDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.UserReference.AccountId)
                        .Select(x => new
                        {
                            CustomerId = x.Id,
                            Name = x.Name,
                            MobileNumber = x.MobileNumber,
                            EmailAddress = x.EmailAddress,
                            StatusId = x.StatusId,
                            CountryId = x.CountryId,
                        })
                        .FirstOrDefault();
                    if (CustomerDetails.StatusId != HelperStatus.Default.Active)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0130, TUCPluginResource.HCP0130M);
                    }
                    var PurchaseDetails = _HCoreContext.VasPayment
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.PaymentReference == _Request.PaymentReference
                        && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                    if (PurchaseDetails == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                    HCoreHelper.LogData(HCoreConstant.LogType.Log, "RES", "", JsonConvert.SerializeObject(_Request));
                    _VasPaymentMetaData = new VasPaymentMetaData();
                    if (CustomerDetails.CountryId == 1)
                    {
                        _VasPaymentMetaData = _HCoreContext.VasPayment
                                              .Where(x => x.Id == _Request.ReferenceId)
                                              .Select(x => new VasPaymentMetaData
                                              {
                                                  CategorySystemName = x.ProductItem.Product.Category.SystemName,
                                                  ProductReference = x.ProductItem.Product.ReferenceKey,
                                                  ProductItemReference = x.ProductItem.ReferenceKey,
                                                  ProductItemName = x.ProductItem.Name,
                                                  ProductName = x.ProductItem.Product.Name,
                                                  ProductCategoryName = x.ProductItem.Product.Category.Name,
                                                  ItemStatusId = x.StatusId,
                                                  ProductStatusId = x.ProductItem.Product.StatusId,
                                                  CategoryStatusId = x.ProductItem.Product.Category.StatusId,
                                                  RewardPercentage = x.ProductItem.Product.RewardPercentage,
                                                  UserPercentage = x.ProductItem.Product.UserPercentage,
                                                  Amount = x.Amount,
                                              }).FirstOrDefault();
                    }
                    else
                    {
                        if (_Request.ProductId > 0)
                        {
                            _VasPaymentMetaData = _HCoreContext.VASProduct
                                                  .Where(x => x.Id == _Request.ProductId)
                                                  .Select(x => new VasPaymentMetaData
                                                  {
                                                      CategorySystemName = x.Category.SystemName,
                                                      ProductReference = x.ReferenceKey,
                                                      ProductName = x.Name,
                                                      ProductCategoryName = x.Category.Name,
                                                      ItemStatusId = x.StatusId,
                                                      ProductStatusId = x.StatusId,
                                                      CategoryStatusId = x.Category.StatusId,
                                                      RewardPercentage = x.RewardPercentage,
                                                      UserPercentage = x.UserPercentage,
                                                      Amount = x.Amount,
                                                  }).FirstOrDefault();
                        }
                        else
                        {
                            if (_Request.ReferenceId > 0)
                            {
                                _VasPaymentMetaData = _HCoreContext.VASProductItem
                                                      .Where(x => x.Id == _Request.ReferenceId)
                                                      .Select(x => new VasPaymentMetaData
                                                      {
                                                          CategorySystemName = x.Product.Category.SystemName,
                                                          ProductReference = x.Product.ReferenceKey,
                                                          ProductItemReference = x.ReferenceKey,
                                                          ProductItemName = x.Name,
                                                          ProductName = x.Product.Name,
                                                          ProductCategoryName = x.Product.Category.Name,
                                                          ItemStatusId = x.StatusId,
                                                          ProductStatusId = x.Product.StatusId,
                                                          CategoryStatusId = x.Product.Category.StatusId,
                                                          RewardPercentage = x.Product.RewardPercentage,
                                                          UserPercentage = x.Product.UserPercentage,
                                                          Amount = x.Amount,
                                                      }).FirstOrDefault();
                            }
                        }
                    }


                    if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Initialized)
                    {
                        double PurchaseAmount = (double)PurchaseDetails.Amount;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        double Balance = _ManageCoreTransaction.GetAppUserBalance(_Request.UserReference.AccountId);
                        if (Balance < PurchaseDetails.Amount)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var OpPurchaseDetails = _HCoreContext.VasPayment
                                                        .Where(x => x.Id == _Request.ReferenceId
                                                        && x.Guid == _Request.ReferenceKey
                                                        && x.PaymentReference == _Request.PaymentReference
                                                        && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                                _HCoreContext.SaveChanges();
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCPluginResource.HCP0129, TUCPluginResource.HCP0129M);
                            }
                        }

                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                        _CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                        _CoreTransactionRequest.AccountNumber = PurchaseDetails.AccountNumber;
                        _CoreTransactionRequest.ReferenceNumber = PurchaseDetails.PaymentReference;
                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                        int PaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                        if (_VasPaymentMetaData.CategorySystemName == "lcctopup")
                        {
                            PaymentTransactionType = TransactionType.Vas.LccTopup;
                        }
                        if (_VasPaymentMetaData.CategorySystemName == "airtime")
                        {
                            PaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                        }
                        if (_VasPaymentMetaData.CategorySystemName == "tv")
                        {
                            PaymentTransactionType = TransactionType.Vas.TVPayment;
                        }
                        if (_VasPaymentMetaData.CategorySystemName == "electricity")
                        {
                            PaymentTransactionType = TransactionType.Vas.ElectricityPayment;
                        }
                        if (_VasPaymentMetaData.CategorySystemName == "utilities")
                        {
                            PaymentTransactionType = TransactionType.Vas.ElectricityPayment;
                        }
                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = _Request.UserReference.AccountId,
                            ModeId = TransactionMode.Debit,
                            TypeId = PaymentTransactionType,
                            SourceId = TransactionSource.TUC,
                            Amount = PurchaseAmount,
                            Charge = 0,
                            TotalAmount = PurchaseAmount,
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = SystemAccounts.TUCVas,
                            ModeId = TransactionMode.Credit,
                            TypeId = PaymentTransactionType,
                            SourceId = TransactionSource.TUCVas,
                            Amount = PurchaseAmount,
                            Charge = 0,
                            TotalAmount = PurchaseAmount,
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        var _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (_TransactionResponse.Status == HelperStatus.Transaction.Success)
                        {
                            if (CustomerDetails.CountryId == 87)
                            {
                                string Username = "";
                                string AuthToken = "";
                                if (HostEnvironment == HostEnvironmentType.BackgroudProcessor || HostEnvironment == HostEnvironmentType.Live)
                                {
                                    Username = "thankucash_billpay@expresspaygh.com";
                                    AuthToken = "Dnc91Kjo91kp2BuSXQN3o-W4Xg07qz2QYcnOJxesTL-T6enmS5VLaUGgsM8rzmc-tlpcJ9OQqXLYRmmKFzwN-PzRPvGNzJawjPj6o9wN";
                                }
                                else
                                {
                                    Username = "thankucash_billpay@expresspaygh.com";
                                    AuthToken = "pkHovmCfFHzQtamsDxNBE-TVviA12YVNYZ8FnsE7rj-cboUilIHvwlvONx3z6mW-JBZudjd5ac8ASRMUnfrf-3sy2wKdSOFRBho1gVpE";
                                }

                                var _VasResponse = Integration.ExpressPay.ExpressPayOperations.MakePayment(Username, AuthToken, _VasPaymentMetaData.ProductReference, PurchaseDetails.AccountNumber, PurchaseDetails.Guid, string.Format("{0:f2}", PurchaseDetails.Amount.Value));
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (_VasResponse.status == 0)
                                    {
                                        double RewardAmount = 0;
                                        double UserRewardAmount = 0;
                                        double CommmissionAmount = 0;
                                        if (_VasPaymentMetaData.RewardPercentage != null && _VasPaymentMetaData.RewardPercentage > 0 && _VasPaymentMetaData.RewardPercentage < 10)
                                        {
                                            RewardAmount = HCoreHelper.GetPercentage(Math.Round(PurchaseAmount, 2), (double)_VasPaymentMetaData.RewardPercentage);
                                            if (RewardAmount > 0)
                                            {
                                                UserRewardAmount = HCoreHelper.GetPercentage(Math.Round(RewardAmount, 2), (double)_VasPaymentMetaData.UserPercentage);
                                                if (UserRewardAmount > 0)
                                                {
                                                    CommmissionAmount = RewardAmount - UserRewardAmount;
                                                }
                                                else
                                                {
                                                    CommmissionAmount = RewardAmount;
                                                }
                                            }
                                        }

                                        var OpPurchaseDetails = _HCoreContext.VasPayment
                                                                .Where(x => x.Id == _Request.ReferenceId
                                                                && x.Guid == _Request.ReferenceKey
                                                                && x.PaymentReference == _Request.PaymentReference
                                                                && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                        OpPurchaseDetails.RewardAmount = RewardAmount;
                                        OpPurchaseDetails.UserRewardAmount = UserRewardAmount;
                                        OpPurchaseDetails.CommissionAmount = CommmissionAmount;
                                        OpPurchaseDetails.Response = _VasResponse.responseContent;
                                        OpPurchaseDetails.TransactionId = _TransactionResponse.ReferenceId;
                                        OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                                        OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                        OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Success;
                                        _HCoreContext.SaveChanges();

                                        if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                                        {

                                            #region Refund Customer
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                                            _CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                                            _CoreTransactionRequest.AccountNumber = PurchaseDetails.AccountNumber;
                                            _CoreTransactionRequest.ReferenceNumber = PurchaseDetails.PaymentReference;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            int RefundPaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                                            if (_VasPaymentMetaData.CategorySystemName == "lcctopup")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.LccTopup;
                                            }
                                            if (_VasPaymentMetaData.CategorySystemName == "airtime")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.AirtimePurchase;
                                            }
                                            if (_VasPaymentMetaData.CategorySystemName == "tv")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.TVPayment;
                                            }
                                            if (_VasPaymentMetaData.CategorySystemName == "electricity")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.ElectricityPayment;
                                            }
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = SystemAccounts.TUCVas,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = RefundPaymentTransactionType,
                                                SourceId = TransactionSource.TUCVas,
                                                Amount = PurchaseAmount,
                                                Charge = 0,
                                                TotalAmount = PurchaseAmount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.UserReference.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = RefundPaymentTransactionType,
                                                SourceId = TransactionSource.TUC,
                                                Amount = PurchaseAmount,
                                                Charge = 0,
                                                TotalAmount = PurchaseAmount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            #endregion

                                        }
                                        if (RewardAmount > 0 && OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                                        {
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = OpPurchaseDetails.AccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = 3;
                                            _CoreTransactionRequest.InvoiceAmount = PurchaseAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = PurchaseAmount;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                            _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = SystemAccounts.TUCVas,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.BillPayment,
                                                SourceId = TransactionSource.Merchant,
                                                Amount = UserRewardAmount,
                                                Comission = CommmissionAmount,
                                                TotalAmount = RewardAmount,
                                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = OpPurchaseDetails.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.BillPayment,
                                                SourceId = TransactionSource.TUC,
                                                Amount = UserRewardAmount,
                                                TotalAmount = UserRewardAmount,
                                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        }

                                        var _Response = new OVasOp.Purchase.Confirm.Response
                                        {
                                            VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                            VasProduct = _VasPaymentMetaData.ProductName,
                                            VasProductItem = _VasPaymentMetaData.ProductItemName,
                                            ReferenceId = OpPurchaseDetails.Id,
                                            ReferenceKey = OpPurchaseDetails.Guid,
                                            PaymentReference = OpPurchaseDetails.PaymentReference,
                                            StartDate = (DateTime)OpPurchaseDetails.StartDate,
                                            EndDate = OpPurchaseDetails.EndDate,
                                            UserRewardAmount = UserRewardAmount,
                                        };
                                        if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                                        {
                                            _Response.StatusCode = "paymentstatus.success";
                                            _Response.StatusName = "Success";
                                            _Response.Message = "Payment successful";
                                        }
                                        else if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Pending)
                                        {
                                            _Response.StatusCode = "paymentstatus.pending";
                                            _Response.StatusName = "Pending";
                                            _Response.Message = "awating service provider";
                                        }
                                        else
                                        {
                                            _Response.StatusCode = "paymentstatus.failed";
                                            _Response.StatusName = "Failed";
                                            _Response.Message = "Transaction failed from provider";
                                        }
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0126, TUCPluginResource.HCP0126M);
                                    }
                                    else
                                    {
                                        var OpPurchaseDetails = _HCoreContext.VasPayment
                                                                .Where(x => x.Id == _Request.ReferenceId
                                                                && x.Guid == _Request.ReferenceKey
                                                                && x.PaymentReference == _Request.PaymentReference
                                                                && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                        OpPurchaseDetails.Response = _VasResponse.responseContent;
                                        OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                        OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                        OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                                        _HCoreContext.SaveChanges();
                                        var _Response = new OVasOp.Purchase.Confirm.Response
                                        {
                                            VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                            VasProduct = _VasPaymentMetaData.ProductName,
                                            VasProductItem = _VasPaymentMetaData.ProductItemName,
                                            ReferenceId = OpPurchaseDetails.Id,
                                            ReferenceKey = OpPurchaseDetails.Guid,
                                            PaymentReference = OpPurchaseDetails.PaymentReference,
                                            StartDate = (DateTime)OpPurchaseDetails.StartDate,
                                            EndDate = OpPurchaseDetails.EndDate,
                                            Data = _VasResponse
                                        };
                                        if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                                        {
                                            _Response.StatusCode = "paymentstatus.failed";
                                            _Response.StatusName = "Failed";
                                            _Response.Message = "Payment failed";
                                        }
                                        #region Refund Customer
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                                        _CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                                        _CoreTransactionRequest.AccountNumber = PurchaseDetails.AccountNumber;
                                        _CoreTransactionRequest.ReferenceNumber = PurchaseDetails.PaymentReference;
                                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                        int RefundPaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                                        if (_VasPaymentMetaData.CategorySystemName == "lcctopup")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.LccTopup;
                                        }
                                        if (_VasPaymentMetaData.CategorySystemName == "airtime")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.AirtimePurchase;
                                        }
                                        if (_VasPaymentMetaData.CategorySystemName == "tv")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.TVPayment;
                                        }
                                        if (_VasPaymentMetaData.CategorySystemName == "electricity")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.ElectricityPayment;
                                        }
                                        if (_VasPaymentMetaData.CategorySystemName == "utilities")
                                        {
                                            PaymentTransactionType = TransactionType.Vas.ElectricityPayment;
                                        }
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = SystemAccounts.TUCVas,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = RefundPaymentTransactionType,
                                            SourceId = TransactionSource.TUCVas,
                                            Amount = PurchaseAmount,
                                            Charge = 0,
                                            TotalAmount = PurchaseAmount,
                                        });
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _Request.UserReference.AccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = RefundPaymentTransactionType,
                                            SourceId = TransactionSource.TUC,
                                            Amount = PurchaseAmount,
                                            Charge = 0,
                                            TotalAmount = PurchaseAmount,
                                        });
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        #endregion
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0126, TUCPluginResource.HCP0126M);
                                    }
                                }

                            }
                            else
                            {
                                var _VasResponse = Integration.CoralPayVas.CoralPayVas.CoralPayPayment(_AppConfig.VasCoralPayUrl, _AppConfig.VasCoralPayUserName, _AppConfig.VasCoralPayPassword,
                                                             PurchaseDetails.AccountNumber, _VasPaymentMetaData.ProductItemReference, PurchaseDetails.PaymentReference, (double)PurchaseDetails.Amount,
                                                             PurchaseDetails.OrderId, CustomerDetails.Name, CustomerDetails.MobileNumber, CustomerDetails.EmailAddress, _Request.UserReference.AccountId);
                                using (_HCoreContext = new HCoreContext())
                                {
                                    if (_VasResponse.Status == StatusSuccess)
                                    {
                                        double RewardAmount = 0;
                                        double UserRewardAmount = 0;
                                        double CommmissionAmount = 0;
                                        if (_VasPaymentMetaData.RewardPercentage != null && _VasPaymentMetaData.RewardPercentage > 0 && _VasPaymentMetaData.RewardPercentage < 10)
                                        {
                                            RewardAmount = HCoreHelper.GetPercentage(Math.Round(PurchaseAmount, 2), (double)_VasPaymentMetaData.RewardPercentage);
                                            if (RewardAmount > 0)
                                            {
                                                UserRewardAmount = HCoreHelper.GetPercentage(Math.Round(RewardAmount, 2), (double)_VasPaymentMetaData.UserPercentage);
                                                if (UserRewardAmount > 0)
                                                {
                                                    CommmissionAmount = RewardAmount - UserRewardAmount;
                                                }
                                                else
                                                {
                                                    CommmissionAmount = RewardAmount;
                                                }
                                            }
                                        }

                                        var OpPurchaseDetails = _HCoreContext.VasPayment
                                                                .Where(x => x.Id == _Request.ReferenceId
                                                                && x.Guid == _Request.ReferenceKey
                                                                && x.PaymentReference == _Request.PaymentReference
                                                                && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                        OpPurchaseDetails.RewardAmount = RewardAmount;
                                        OpPurchaseDetails.UserRewardAmount = UserRewardAmount;
                                        OpPurchaseDetails.CommissionAmount = CommmissionAmount;
                                        OpPurchaseDetails.Request = _VasResponse.RequestContent;
                                        OpPurchaseDetails.Response = _VasResponse.ResponseContent;
                                        OpPurchaseDetails.TransactionId = _TransactionResponse.ReferenceId;
                                        OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                                        if (_VasResponse.Token != null)
                                        {
                                            if (_VasResponse.Token.Value != null)
                                            {
                                                OpPurchaseDetails.Comment = _VasResponse.Token.Value;
                                            }
                                        }

                                        if (_VasResponse.VendStatus == "CONFIRMED")
                                        {
                                            OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                            OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Success;
                                        }
                                        else if (_VasResponse.VendStatus == "AWAITING_SERVICE_PROVIDER" || _VasResponse.VendStatus == "PENDING")
                                        {
                                            OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Pending;
                                        }
                                        else
                                        {
                                            OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                            OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                        }
                                        _HCoreContext.SaveChanges();

                                        if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                                        {

                                            #region Refund Customer
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                                            _CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                                            _CoreTransactionRequest.AccountNumber = PurchaseDetails.AccountNumber;
                                            _CoreTransactionRequest.ReferenceNumber = PurchaseDetails.PaymentReference;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            int RefundPaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                                            if (_VasPaymentMetaData.CategorySystemName == "lcctopup")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.LccTopup;
                                            }
                                            if (_VasPaymentMetaData.CategorySystemName == "airtime")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.AirtimePurchase;
                                            }
                                            if (_VasPaymentMetaData.CategorySystemName == "tv")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.TVPayment;
                                            }
                                            if (_VasPaymentMetaData.CategorySystemName == "electricity")
                                            {
                                                RefundPaymentTransactionType = TransactionType.VasRefund.ElectricityPayment;
                                            }
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = SystemAccounts.TUCVas,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = RefundPaymentTransactionType,
                                                SourceId = TransactionSource.TUCVas,
                                                Amount = PurchaseAmount,
                                                Charge = 0,
                                                TotalAmount = PurchaseAmount,
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = _Request.UserReference.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = RefundPaymentTransactionType,
                                                SourceId = TransactionSource.TUC,
                                                Amount = PurchaseAmount,
                                                Charge = 0,
                                                TotalAmount = PurchaseAmount,
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                            #endregion

                                        }
                                        if (RewardAmount > 0 && OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                                        {
                                            _CoreTransactionRequest = new OCoreTransaction.Request();
                                            _CoreTransactionRequest.CustomerId = OpPurchaseDetails.AccountId;
                                            _CoreTransactionRequest.UserReference = _Request.UserReference;
                                            _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                            _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                            _CoreTransactionRequest.ParentId = 3;
                                            _CoreTransactionRequest.InvoiceAmount = PurchaseAmount;
                                            _CoreTransactionRequest.ReferenceInvoiceAmount = PurchaseAmount;
                                            _CoreTransactionRequest.ReferenceNumber = _Request.PaymentReference;
                                            _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                            _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                            _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = SystemAccounts.TUCVas,
                                                ModeId = TransactionMode.Debit,
                                                TypeId = TransactionType.BillPayment,
                                                SourceId = TransactionSource.Merchant,
                                                Amount = UserRewardAmount,
                                                Comission = CommmissionAmount,
                                                TotalAmount = RewardAmount,
                                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                            });
                                            _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                            {
                                                UserAccountId = OpPurchaseDetails.AccountId,
                                                ModeId = TransactionMode.Credit,
                                                TypeId = TransactionType.BillPayment,
                                                SourceId = TransactionSource.TUC,
                                                Amount = UserRewardAmount,
                                                TotalAmount = UserRewardAmount,
                                                TransactionDate = HCoreHelper.GetGMTDateTime(),
                                            });
                                            _CoreTransactionRequest.Transactions = _TransactionItems;
                                            _ManageCoreTransaction = new ManageCoreTransaction();
                                            OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        }

                                        var _Response = new OVasOp.Purchase.Confirm.Response
                                        {
                                            VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                            VasProduct = _VasPaymentMetaData.ProductName,
                                            VasProductItem = _VasPaymentMetaData.ProductItemName,
                                            ReferenceId = OpPurchaseDetails.Id,
                                            ReferenceKey = OpPurchaseDetails.Guid,
                                            PaymentReference = OpPurchaseDetails.PaymentReference,
                                            StartDate = (DateTime)OpPurchaseDetails.StartDate,
                                            EndDate = OpPurchaseDetails.EndDate,
                                            UserRewardAmount = UserRewardAmount,
                                        };
                                        if (_VasResponse.Token != null)
                                        {
                                            if (_VasResponse.Token.Value != null)
                                            {
                                                _Response.Token = _VasResponse.Token.Value;
                                            }
                                        }
                                        if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                                        {
                                            _Response.StatusCode = "paymentstatus.success";
                                            _Response.StatusName = "Success";
                                            _Response.Message = "Payment successful";
                                        }
                                        else if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Pending)
                                        {
                                            _Response.StatusCode = "paymentstatus.pending";
                                            _Response.StatusName = "Pending";
                                            _Response.Message = "awating service provider";
                                        }
                                        else
                                        {
                                            _Response.StatusCode = "paymentstatus.failed";
                                            _Response.StatusName = "Failed";
                                            _Response.Message = "Transaction failed from provider";
                                        }
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0126, TUCPluginResource.HCP0126M);
                                    }
                                    else
                                    {
                                        var OpPurchaseDetails = _HCoreContext.VasPayment
                                                                .Where(x => x.Id == _Request.ReferenceId
                                                                && x.Guid == _Request.ReferenceKey
                                                                && x.PaymentReference == _Request.PaymentReference
                                                                && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                                        OpPurchaseDetails.Request = _VasResponse.RequestContent;
                                        OpPurchaseDetails.Response = _VasResponse.ResponseContent;
                                        OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                        OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                        OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                                        _HCoreContext.SaveChanges();
                                        var _Response = new OVasOp.Purchase.Confirm.Response
                                        {
                                            VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                            VasProduct = _VasPaymentMetaData.ProductName,
                                            VasProductItem = _VasPaymentMetaData.ProductItemName,
                                            ReferenceId = OpPurchaseDetails.Id,
                                            ReferenceKey = OpPurchaseDetails.Guid,
                                            PaymentReference = OpPurchaseDetails.PaymentReference,
                                            StartDate = (DateTime)OpPurchaseDetails.StartDate,
                                            EndDate = OpPurchaseDetails.EndDate,
                                            Data = _VasResponse
                                        };
                                        if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                                        {
                                            _Response.StatusCode = "paymentstatus.failed";
                                            _Response.StatusName = "Failed";
                                            _Response.Message = "Payment failed";
                                        }
                                        #region Refund Customer
                                        _CoreTransactionRequest = new OCoreTransaction.Request();
                                        _CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                        _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                                        _CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                                        _CoreTransactionRequest.AccountNumber = PurchaseDetails.AccountNumber;
                                        _CoreTransactionRequest.ReferenceNumber = PurchaseDetails.PaymentReference;
                                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                        int RefundPaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                                        if (_VasPaymentMetaData.CategorySystemName == "lcctopup")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.LccTopup;
                                        }
                                        if (_VasPaymentMetaData.CategorySystemName == "airtime")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.AirtimePurchase;
                                        }
                                        if (_VasPaymentMetaData.CategorySystemName == "tv")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.TVPayment;
                                        }
                                        if (_VasPaymentMetaData.CategorySystemName == "electricity")
                                        {
                                            RefundPaymentTransactionType = TransactionType.VasRefund.ElectricityPayment;
                                        }
                                        _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = SystemAccounts.TUCVas,
                                            ModeId = TransactionMode.Debit,
                                            TypeId = RefundPaymentTransactionType,
                                            SourceId = TransactionSource.TUCVas,
                                            Amount = PurchaseAmount,
                                            Charge = 0,
                                            TotalAmount = PurchaseAmount,
                                        });
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = _Request.UserReference.AccountId,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = RefundPaymentTransactionType,
                                            SourceId = TransactionSource.TUC,
                                            Amount = PurchaseAmount,
                                            Charge = 0,
                                            TotalAmount = PurchaseAmount,
                                        });
                                        _CoreTransactionRequest.Transactions = _TransactionItems;
                                        _ManageCoreTransaction = new ManageCoreTransaction();
                                        _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                        #endregion
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0126, TUCPluginResource.HCP0126M);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var OpPurchaseDetails = _HCoreContext.VasPayment
                                                    .Where(x => x.Id == _Request.ReferenceId
                                                    && x.Guid == _Request.ReferenceKey
                                                    && x.PaymentReference == _Request.PaymentReference
                                                    && x.AccountId == _Request.UserReference.AccountId).FirstOrDefault();
                            OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                            OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                            OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            OpPurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                            _HCoreContext.SaveChanges();
                            var _Response = new OVasOp.Purchase.Confirm.Response
                            {
                                VasCategory = _VasPaymentMetaData.ProductCategoryName,
                                VasProduct = _VasPaymentMetaData.ProductName,
                                VasProductItem = _VasPaymentMetaData.ProductItemName,
                                ReferenceId = OpPurchaseDetails.Id,
                                ReferenceKey = OpPurchaseDetails.Guid,
                                PaymentReference = OpPurchaseDetails.PaymentReference,
                                StartDate = (DateTime)OpPurchaseDetails.StartDate,
                                EndDate = OpPurchaseDetails.EndDate,
                            };
                            if (OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                            {
                                _Response.StatusCode = "paymentstatus.failed";
                                _Response.StatusName = "Failed";
                                _Response.Message = "Payment failed";
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, TUCPluginResource.HCP0132, TUCPluginResource.HCP0132M);
                            #endregion
                        }
                    }
                    else
                    {
                        var _Response = new OVasOp.Purchase.Confirm.Response
                        {
                            VasCategory = _VasPaymentMetaData.ProductCategoryName,
                            VasProduct = _VasPaymentMetaData.ProductName,
                            VasProductItem = _VasPaymentMetaData.ProductItemName,
                            ReferenceId = PurchaseDetails.Id,
                            ReferenceKey = PurchaseDetails.Guid,
                            PaymentReference = PurchaseDetails.PaymentReference,
                            StartDate = (DateTime)PurchaseDetails.StartDate,
                            EndDate = PurchaseDetails.EndDate,
                        };
                        if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Initialized)
                        {
                            _Response.StatusCode = "paymentstatus.initialized";
                            _Response.StatusName = "Initialized";
                        }
                        if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Processing)
                        {
                            _Response.StatusCode = "paymentstatus.processing";
                            _Response.StatusName = "Processing";
                        }
                        if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                        {
                            _Response.StatusCode = "paymentstatus.success";
                            _Response.StatusName = "Success";
                        }
                        if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Failed)
                        {
                            _Response.StatusCode = "paymentstatus.failed";
                            _Response.StatusName = "Failed";
                        }
                        if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Refunded)
                        {
                            _Response.StatusCode = "paymentstatus.refunded";
                            _Response.StatusName = "Refunded";
                        }
                        if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Cancelled)
                        {
                            _Response.StatusCode = "paymentstatus.cancelled";
                            _Response.StatusName = "Cancelled";
                        }
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, TUCPluginResource.HCP0126, TUCPluginResource.HCP0126M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("VasPayment_Confirm", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Vas payment verify.
        /// </summary>
        /// <param name="ReferenceId">The reference identifier.</param>
        internal void VasPayment_Verify(long ReferenceId = 0)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _TVasPayments = new List<TVasPayment>();
                    if (ReferenceId > 0)
                    {
                        _TVasPayments = _HCoreContext.VasPayment
                                                    .Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Pending && x.Id == ReferenceId)
                                                    .Select(x => new TVasPayment
                                                    {
                                                        Id = x.Id,
                                                        AccountId = x.AccountId,
                                                        ProductItemId = x.ProductItemId,
                                                        Amount = x.Amount,
                                                        PaymentReference = x.PaymentReference,
                                                        AccountNumber = x.AccountNumber,
                                                    })
                                                    .Skip(0)
                                                    .Take(1)
                                                    .ToList();
                    }
                    else
                    {
                        _TVasPayments = _HCoreContext.VasPayment
                                                    .Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Pending)
                                                   .Select(x => new TVasPayment
                                                   {
                                                       Id = x.Id,
                                                       AccountId = x.AccountId,
                                                       ProductItemId = x.ProductItemId,
                                                       Amount = x.Amount,
                                                       PaymentReference = x.PaymentReference,
                                                       AccountNumber = x.AccountNumber,
                                                   })
                                                    .Skip(0)
                                                    .Take(500)
                                                    .ToList();
                    }
                    _HCoreContext.Dispose();
                    if (_TVasPayments.Count > 0)
                    {
                        foreach (var PendingVasPayment in _TVasPayments)
                        {
                            var _VasResponse = Integration.CoralPayVas.CoralPayVas.CoralPayTransactionEnquiry(_AppConfig.VasCoralPayUrl, _AppConfig.VasCoralPayUserName, _AppConfig.VasCoralPayPassword, PendingVasPayment.PaymentReference);
                            if (_VasResponse.Status == StatusSuccess)
                            {

                                using (_HCoreContext = new HCoreContext())
                                {
                                    var CustomerDetails = _HCoreContext.HCUAccount
                                       .Where(x => x.Id == PendingVasPayment.AccountId)
                                       .Select(x => new
                                       {
                                           CustomerId = x.Id,
                                           Name = x.Name,
                                           MobileNumber = x.MobileNumber,
                                           EmailAddress = x.EmailAddress,
                                           StatusId = x.StatusId
                                       })
                                       .FirstOrDefault();

                                    var ProductItemDetails = _HCoreContext.VASProductItem
                                      .Where(x => x.Id == PendingVasPayment.ProductItemId)
                                      .Select(x => new
                                      {
                                          CategorySystemName = x.Product.Category.SystemName,
                                          ProductReference = x.Product.ReferenceKey,
                                          ProductItemReference = x.ReferenceKey,
                                          ProductItemName = x.Name,
                                          ProductName = x.Product.Name,
                                          ProductCategoryName = x.Product.Category.Name,
                                          ItemStatusId = x.StatusId,
                                          ProductStatusId = x.Product.StatusId,
                                          CategoryStatusId = x.Product.Category.StatusId,
                                          RewardPercentage = x.Product.RewardPercentage,
                                          UserPercentage = x.Product.UserPercentage,
                                      }).FirstOrDefault();

                                    OUserReference _OUserReference = new OUserReference();
                                    _OUserReference.AccountId = CustomerDetails.CustomerId;
                                    _OUserReference.AccountTypeId = UserAccountType.Appuser;
                                    if (_VasResponse.Status == StatusSuccess)
                                    {
                                        if (_VasResponse.VendStatus == "CONFIRMED" || _VasResponse.VendStatus == "FAILED" || _VasResponse.VendStatus == "STAGED_FOR_REVERSAL" || _VasResponse.VendStatus == "REVERSED" || _VasResponse.VendStatus == "EXPIRED" || _VasResponse.VendStatus == "CANCELLED_BEFORE_EXECUTION")
                                        {
                                            var OpPurchaseDetails = _HCoreContext.VasPayment
                                                                .Where(x => x.Id == PendingVasPayment.Id).FirstOrDefault();
                                            double PurchaseAmount = (double)OpPurchaseDetails.Amount;
                                            if (_VasResponse.VendStatus == "CONFIRMED")
                                            {
                                                double RewardAmount = 0;
                                                double UserRewardAmount = 0;
                                                double CommmissionAmount = 0;
                                                if (ProductItemDetails.RewardPercentage != null && ProductItemDetails.RewardPercentage > 0 && ProductItemDetails.RewardPercentage < 10)
                                                {
                                                    RewardAmount = HCoreHelper.GetPercentage(Math.Round((double)PendingVasPayment.Amount, 2), (double)ProductItemDetails.RewardPercentage);
                                                    if (RewardAmount > 0)
                                                    {
                                                        UserRewardAmount = HCoreHelper.GetPercentage(Math.Round(RewardAmount, 2), (double)ProductItemDetails.UserPercentage);
                                                        if (UserRewardAmount > 0)
                                                        {
                                                            CommmissionAmount = RewardAmount - UserRewardAmount;
                                                        }
                                                        else
                                                        {
                                                            CommmissionAmount = RewardAmount;
                                                        }
                                                    }
                                                }
                                                OpPurchaseDetails.Request = _VasResponse.RequestContent;
                                                OpPurchaseDetails.Response = _VasResponse.ResponseContent;
                                                OpPurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                OpPurchaseDetails.ModifyById = 1;
                                                if (_VasResponse.Token != null)
                                                {
                                                    if (_VasResponse.Token.Value != null)
                                                    {
                                                        OpPurchaseDetails.Comment = _VasResponse.Token.Value;
                                                    }
                                                }
                                                OpPurchaseDetails.RewardAmount = RewardAmount;
                                                OpPurchaseDetails.UserRewardAmount = UserRewardAmount;
                                                OpPurchaseDetails.CommissionAmount = CommmissionAmount;
                                                OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                                OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Success;
                                                _HCoreContext.SaveChanges();
                                                if (RewardAmount > 0 && OpPurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Success)
                                                {
                                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                                    _CoreTransactionRequest.CustomerId = OpPurchaseDetails.AccountId;
                                                    _CoreTransactionRequest.UserReference = _OUserReference;
                                                    _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                    _CoreTransactionRequest.ParentId = 3;
                                                    _CoreTransactionRequest.InvoiceAmount = PurchaseAmount;
                                                    _CoreTransactionRequest.ReferenceInvoiceAmount = PurchaseAmount;
                                                    _CoreTransactionRequest.ReferenceNumber = PendingVasPayment.PaymentReference;
                                                    _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                                    _CoreTransactionRequest.CreatedById = PendingVasPayment.AccountId;
                                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = SystemAccounts.TUCVas,
                                                        ModeId = TransactionMode.Debit,
                                                        TypeId = TransactionType.BillPayment,
                                                        SourceId = TransactionSource.Merchant,
                                                        Amount = UserRewardAmount,
                                                        Comission = CommmissionAmount,
                                                        TotalAmount = RewardAmount,
                                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                    });
                                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                    {
                                                        UserAccountId = OpPurchaseDetails.AccountId,
                                                        ModeId = TransactionMode.Credit,
                                                        TypeId = TransactionType.BillPayment,
                                                        SourceId = TransactionSource.TUC,
                                                        Amount = UserRewardAmount,
                                                        TotalAmount = UserRewardAmount,
                                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                                    });
                                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                                    OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                }
                                            }
                                            else if (_VasResponse.VendStatus == "FAILED" || _VasResponse.VendStatus == "STAGED_FOR_REVERSAL" || _VasResponse.VendStatus == "REVERSED" || _VasResponse.VendStatus == "EXPIRED" || _VasResponse.VendStatus == "CANCELLED_BEFORE_EXECUTION")
                                            {
                                                OpPurchaseDetails.ModifyById = 1;
                                                OpPurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                                                OpPurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                                                _HCoreContext.SaveChanges();
                                                #region Refund Customer
                                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                                _CoreTransactionRequest.CustomerId = CustomerDetails.CustomerId;
                                                _CoreTransactionRequest.UserReference = _OUserReference;
                                                _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                                _CoreTransactionRequest.ParentId = SystemAccounts.TUCVas;
                                                _CoreTransactionRequest.InvoiceAmount = _CoreTransactionRequest.ReferenceInvoiceAmount = _CoreTransactionRequest.ReferenceAmount = PurchaseAmount;
                                                _CoreTransactionRequest.AccountNumber = PendingVasPayment.AccountNumber;
                                                _CoreTransactionRequest.ReferenceNumber = PendingVasPayment.PaymentReference;
                                                _CoreTransactionRequest.CreatedById = PendingVasPayment.AccountId;
                                                int RefundPaymentTransactionType = TransactionType.Vas.AirtimePurchase;
                                                if (ProductItemDetails.CategorySystemName == "lcctopup")
                                                {
                                                    RefundPaymentTransactionType = TransactionType.VasRefund.LccTopup;
                                                }
                                                if (ProductItemDetails.CategorySystemName == "airtime")
                                                {
                                                    RefundPaymentTransactionType = TransactionType.VasRefund.AirtimePurchase;
                                                }
                                                if (ProductItemDetails.CategorySystemName == "tv")
                                                {
                                                    RefundPaymentTransactionType = TransactionType.VasRefund.TVPayment;
                                                }
                                                if (ProductItemDetails.CategorySystemName == "electricity")
                                                {
                                                    RefundPaymentTransactionType = TransactionType.VasRefund.ElectricityPayment;
                                                }
                                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = SystemAccounts.TUCVas,
                                                    ModeId = TransactionMode.Debit,
                                                    TypeId = RefundPaymentTransactionType,
                                                    SourceId = TransactionSource.TUCVas,
                                                    Amount = PurchaseAmount,
                                                    Charge = 0,
                                                    TotalAmount = PurchaseAmount,
                                                });
                                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                                {
                                                    UserAccountId = CustomerDetails.CustomerId,
                                                    ModeId = TransactionMode.Credit,
                                                    TypeId = RefundPaymentTransactionType,
                                                    SourceId = TransactionSource.TUC,
                                                    Amount = PurchaseAmount,
                                                    Charge = 0,
                                                    TotalAmount = PurchaseAmount,
                                                });
                                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                                _ManageCoreTransaction = new ManageCoreTransaction();
                                                _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                                #endregion
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("VasPayment_Verify", _Exception, null, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }

        /// <summary>
        /// Description: Vas payment cancel.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VasPayment_Cancel(OVasOp.Purchase.Cancel.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREF, TUCPluginResource.HCPACCREFM);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCPACCREFKEY, TUCPluginResource.HCPACCREFKEYM);
                }
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0118, TUCPluginResource.HCP0118M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var PurchaseDetails = _HCoreContext.VasPayment.Where(x => x.Id == _Request.ReferenceId
                                          && x.Guid == _Request.ReferenceKey
                                          && x.PaymentReference == _Request.PaymentReference).FirstOrDefault();
                    if (PurchaseDetails == null)
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0404, TUCPluginResource.HCP0404M);
                    }
                    if (PurchaseDetails.StatusId == HelperStatus.BillPaymentStatus.Initialized)
                    {
                        PurchaseDetails.StatusId = HelperStatus.BillPaymentStatus.Failed;
                        PurchaseDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        PurchaseDetails.EndDate = HCoreHelper.GetGMTDateTime();
                        PurchaseDetails.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, TUCPluginResource.HCP0120, TUCPluginResource.HCP0120M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, TUCPluginResource.HCP0119, TUCPluginResource.HCP0119M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("VasPayment_Cancel", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
    }
}
