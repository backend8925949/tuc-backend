//==================================================================================
// FileName: FrameworkAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to analytics
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using System.Linq.Dynamic.Core;
using static HCore.Helper.HCoreConstant;
using HCore.TUC.Plugins.Vas.Object;
using HCore.TUC.Plugins.Resource;

namespace HCore.TUC.Plugins.Vas.Framework
{
    internal class FrameworkAnalytics
    {
        OVasAnalytics.Overview _VasOverview;
        OVasAnalytics.StatusOverview _VasStatusOverview;
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the vas purchase overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVasPurchaseOverview(OVasAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _VasOverview = new OVasAnalytics.Overview();
                    if (_Request.CategoryId > 0)
                    {
                        _VasOverview.Transactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                        && x.Account.CountryId == _Request.UserReference.SystemCountry
                                                  && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId);


                        _VasOverview.SuccessfulTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                            && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                        _VasOverview.FailedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed
                                              && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                        _VasOverview.InitializedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized
                                          && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId);

                        _VasOverview.Customers = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                               && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId).Select(x => x.AccountId).Distinct().Count();
                        _VasOverview.InvoiceAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId).Sum(x => x.Amount);
                        _VasOverview.RewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId).Sum(x => x.RewardAmount);
                        _VasOverview.UserRewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId).Sum(x => x.UserRewardAmount);
                        _VasOverview.CommissionAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == _Request.CategoryId).Sum(x => x.CommissionAmount);
                    }
                    else if (_Request.ProductId > 0)
                    {
                        _VasOverview.Transactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                             && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId);
                        _VasOverview.SuccessfulTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                            && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId);
                        _VasOverview.InitializedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized
                                                        && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId);
                        _VasOverview.FailedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed
                                                              && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId);

                        _VasOverview.Customers = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                   && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Select(x => x.AccountId).Distinct().Count();
                        _VasOverview.InvoiceAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.Amount);
                        _VasOverview.RewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.RewardAmount);
                        _VasOverview.UserRewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.UserRewardAmount);
                        _VasOverview.CommissionAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.CommissionAmount);
                    }
                    else if (_Request.ProductItemId > 0)
                    {
                        _VasOverview.Transactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                              && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == _Request.ProductItemId);

                        _VasOverview.SuccessfulTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                           && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == _Request.ProductItemId);
                        _VasOverview.InitializedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized
                                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == _Request.ProductItemId);
                        _VasOverview.FailedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed
                                                                    && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == _Request.ProductItemId);

                        _VasOverview.Customers = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Select(x => x.AccountId).Distinct().Count();
                        _VasOverview.InvoiceAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.Amount);
                        _VasOverview.RewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.RewardAmount);
                        _VasOverview.UserRewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.UserRewardAmount);
                        _VasOverview.CommissionAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == _Request.ProductId).Sum(x => x.CommissionAmount);
                    }
                    else
                    {
                        _VasOverview.Transactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate);
                        _VasOverview.SuccessfulTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate);
                        _VasOverview.InitializedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized
                                                                        && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate);
                        _VasOverview.FailedTransactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed
                                                                       && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate);

                        _VasOverview.Customers = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate).Select(x => x.AccountId).Distinct().Count();
                        _VasOverview.InvoiceAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate).Sum(x => x.Amount);
                        _VasOverview.RewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                 && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate).Sum(x => x.RewardAmount);
                        _VasOverview.UserRewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate).Sum(x => x.UserRewardAmount);
                        _VasOverview.CommissionAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                  && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate).Sum(x => x.CommissionAmount);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VasOverview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasPurchaseOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the vas purchase status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVasPurchaseStatusOverview(OVasAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    _VasStatusOverview = new OVasAnalytics.StatusOverview();
                    if (_Request.CategoryId > 0)
                    {
                        _VasStatusOverview.Initialized = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                        _VasStatusOverview.Processing = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Processing && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                        _VasStatusOverview.Success = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                        _VasStatusOverview.Failed = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed && x.Account.CountryId == _Request.UserReference.SystemCountry  && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                        _VasStatusOverview.Refunded = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Refunded && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                        _VasStatusOverview.Cancelled = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Cancelled && x.ProductItem.Product.CategoryId == _Request.CategoryId);
                    }
                    else if (_Request.ProductId > 0)
                    {
                        _VasStatusOverview.Initialized = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.ProductId == _Request.ProductId);
                        _VasStatusOverview.Processing = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Processing && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.ProductId == _Request.ProductId);
                        _VasStatusOverview.Success = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.ProductId == _Request.ProductId);
                        _VasStatusOverview.Failed = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.ProductId == _Request.ProductId);
                        _VasStatusOverview.Refunded = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Refunded && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.ProductId == _Request.ProductId);
                        _VasStatusOverview.Cancelled = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Cancelled && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItem.ProductId == _Request.ProductId);
                    }
                    else if (_Request.ProductItemId > 0)
                    {
                        _VasStatusOverview.Initialized = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItemId == _Request.ProductItemId);
                        _VasStatusOverview.Processing = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Processing && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItemId == _Request.ProductItemId);
                        _VasStatusOverview.Success = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItemId == _Request.ProductItemId);
                        _VasStatusOverview.Failed = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItemId == _Request.ProductItemId);
                        _VasStatusOverview.Refunded = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Refunded && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItemId == _Request.ProductItemId);
                        _VasStatusOverview.Cancelled = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Cancelled && x.Account.CountryId == _Request.UserReference.SystemCountry && x.ProductItemId == _Request.ProductItemId);
                    }
                    else
                    {
                        _VasStatusOverview.Initialized = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Initialized && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _VasStatusOverview.Processing = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Processing && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _VasStatusOverview.Success = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _VasStatusOverview.Failed = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Failed && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _VasStatusOverview.Refunded = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Refunded && x.Account.CountryId == _Request.UserReference.SystemCountry);
                        _VasStatusOverview.Cancelled = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Cancelled && x.Account.CountryId == _Request.UserReference.SystemCountry);
                    }
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VasStatusOverview, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasPurchaseStatusOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the vas purchase category overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVasPurchaseCategoryOverview(OVasAnalytics.Request _Request)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.CategoryId > 0)
                    {
                        var CategoryProducts = _HCoreContext.VASProduct.Where(x => x.CategoryId == _Request.CategoryId
                            && x.StatusId ==  HelperStatus.Default.Active)
                            .Select(x => new OVasAnalytics.Stats
                            {
                                ReferenceId = x.Id,
                                Name = x.Name
                            }).ToList();
                        foreach (var Item in CategoryProducts)
                        {
                            _VasOverview = new OVasAnalytics.Overview();
                            _VasOverview.Transactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                                         && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == Item.ReferenceId);
                            _VasOverview.Customers = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                      && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == Item.ReferenceId).Select(x => x.AccountId).Distinct().Count();
                            _VasOverview.InvoiceAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                       && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == Item.ReferenceId).Sum(x => x.Amount);
                            _VasOverview.RewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                       && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == Item.ReferenceId).Sum(x => x.RewardAmount);
                            _VasOverview.UserRewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                      && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == Item.ReferenceId).Sum(x => x.UserRewardAmount);
                            _VasOverview.CommissionAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                      && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.ProductId == Item.ReferenceId).Sum(x => x.CommissionAmount);
                            Item.Overview = _VasOverview;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, CategoryProducts, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                    }
                    else if (_Request.ProductId > 0)
                    {
                        var ProductItems = _HCoreContext.VASProductItem.Where(x => x.ProductId == _Request.ProductId
                            && x.StatusId ==  HelperStatus.Default.Active)
                            .Select(x => new OVasAnalytics.Stats
                            {
                                ReferenceId = x.Id,
                                Name = x.Name
                            }).ToList();
                        foreach (var Item in ProductItems)
                        {
                            _VasOverview = new OVasAnalytics.Overview();
                            _VasOverview.Transactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                                          && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == Item.ReferenceId);
                            _VasOverview.Customers = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                      && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == Item.ReferenceId).Select(x => x.AccountId).Distinct().Count();
                            _VasOverview.InvoiceAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                       && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == Item.ReferenceId).Sum(x => x.Amount);
                            _VasOverview.RewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                      && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == Item.ReferenceId).Sum(x => x.RewardAmount);
                            _VasOverview.UserRewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                      && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == Item.ReferenceId).Sum(x => x.UserRewardAmount);
                            _VasOverview.CommissionAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                     && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItemId == Item.ReferenceId).Sum(x => x.CommissionAmount);
                            Item.Overview = _VasOverview;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ProductItems, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                    }
                    else
                    {
                        var ProductCategories = _HCoreContext.VASCategory.Where(x =>  x.StatusId == HelperStatus.Default.Active).Select(x => new OVasAnalytics.Stats
                        {
                            ReferenceId = x.Id,
                            Name = x.Name
                        }).ToList();
                        foreach (var ProductCategory in ProductCategories)
                        {
                            _VasOverview = new OVasAnalytics.Overview();
                            _VasOverview.Transactions = _HCoreContext.VasPayment.Count(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                                       && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == ProductCategory.ReferenceId);
                            _VasOverview.Customers = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                     && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == ProductCategory.ReferenceId).Select(x => x.AccountId).Distinct().Count();
                            _VasOverview.InvoiceAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                     && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == ProductCategory.ReferenceId).Sum(x => x.Amount);
                            _VasOverview.RewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                     && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == ProductCategory.ReferenceId).Sum(x => x.RewardAmount);
                            _VasOverview.UserRewardAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                    && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == ProductCategory.ReferenceId).Sum(x => x.UserRewardAmount);
                            _VasOverview.CommissionAmount = _HCoreContext.VasPayment.Where(x => x.StatusId == HelperStatus.BillPaymentStatus.Success
                                                   && x.Account.CountryId == _Request.UserReference.SystemCountry && x.EndDate > _Request.StartDate && x.EndDate < _Request.EndDate && x.ProductItem.Product.CategoryId == ProductCategory.ReferenceId).Sum(x => x.CommissionAmount);
                            ProductCategory.Overview = _VasOverview;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, ProductCategories, TUCPluginResource.HCP0200, TUCPluginResource.HCP0200M);
                    }
                }
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("GetVasPurchaseCategoryOverview", _Exception, _Request.UserReference, TUCPluginResource.HCP0500, TUCPluginResource.HCP0500M);
            }
            #endregion
        }
    }
}
