//==================================================================================
// FileName: OVas.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Plugins.Vas.Object
{
    public class OVas
    {
        public class Category
        {
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? SystemName { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class Product
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public double? RewardPercentage { get; set; }
                public double? UserRewardPercentage { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? CategoryKey { get; set; }
                public string? CategoryName { get; set; }

                public string? Name { get; set; }

                public string? IconUrl { get; set; }

                public long? TotalItem { get; set; }
                public long? TotalPurchase { get; set; }
                public double? TotalPurchaseAmount { get; set; }

                public double? RewardPercentage { get; set; }
                public double? UserRewardPercentage { get; set; }
                public double? CommissionPercentage { get; set; }
                public DateTime? CreateDate { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class ProductItem
        {
            public class Request
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public string? Name { get; set; }
                public string? StatusCode { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class List
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public string? Name { get; set; }

                public double? Amount { get; set; }

                public long? TotalPurchase { get; set; }
                public double? TotalPurchaseAmount { get; set; }

                //public double? RewardPercentage { get; set; }
                //public double? UserRewardPercentage { get; set; }


                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }
        }
        public class PurchaseHistory
        {
            public class Overview
            {
                public long Transactions { get; set; }
                public long Customers { get; set; }
                public double? InvoiceAmount { get; set; }
                public double? RewardAmount { get; set; }
                public double? UserRewardAmount { get; set; }
                public double? CommissionAmount { get; set; }
            }

            public class List
            {
                public long? ParentId { get; set; }
                public string? ParentKey { get; set; }
                public string? ParentName { get; set; }
                public string? ParentDisplayName { get; set; }
                public string? ParentMobileNumber { get; set; }

                public long? SubParentId { get; set; }
                public string? SubParentKey { get; set; }
                public string? SubParentName { get; set; }
                public string? SubParentDisplayName { get; set; }
                public string? SubParentMobileNumber { get; set; }

                public long? CashierId { get; set; }
                public string? CashierKey { get; set; }
                public string? CashierName { get; set; }
                public string? CashierDisplayName { get; set; }
                public string? CashierMobileNumber { get; set; }

                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }

                public long? TypeId { get; set; }
                public string? TypeKey { get; set; }
                public string? TypeCode { get; set; }
                public string? TypeName { get; set; }

                public long? ProductItemId { get; set; }
                public string? ProductItemKey { get; set; }
                public string? ProductItemName { get; set; }

                public string? BillerName { get; set; }

                public long? ProductId { get; set; }
                public string? ProductKey { get; set; }
                public string? ProductName { get; set; }
                public long? ProductCategoryId { get; set; }
                public string? ProductCategoryKey { get; set; }
                public string? ProductCategoryName { get; set; }

                public string? AccountNumber { get; set; }
                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountDisplayName { get; set; }
                public string? AccountMobileNumber { get; set; }

                public double? Amount { get; set; }
                public double? RewardAmount { get; set; }
                public double? UserRewardAmount { get; set; }
                public double? CommissionAmount { get; set; }
                public string? PaymentReference { get; set; } 
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }

                public string? Value { get; set; }


                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }
            }

            public class Details
            {
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }


                public long? ProductItemId { get; set; }
                public string? ProductItemKey { get; set; }
                public string? ProductItemName { get; set; }


                public long? ProductId { get; set; }
                public string? ProductKey { get; set; }
                public string? ProductName { get; set; }
                public long? ProductCategoryId { get; set; }
                public string? ProductCategoryKey { get; set; }
                public string? ProductCategoryName { get; set; }


                public long? AccountId { get; set; }
                public string? AccountKey { get; set; }
                public string? AccountDisplayName { get; set; }
                public string? AccountMobileNumber { get; set; }

                public double? Amount { get; set; }
                public double? Charge { get; set; }
                public double? RewardAmount { get; set; }
                public double? UserRewardAmount { get; set; }
                public double? CommissionAmount { get; set; }
                public string? PaymentReference { get; set; }
                public string? PaymentToken { get; set; }
                public DateTime? StartDate { get; set; }
                public DateTime? EndDate { get; set; }

                public string? BillerName { get; set; }

                public long? TransactionId { get; set; }
                public string? TransactionKey { get; set; }
                public string? Comment { get; set; }


                public DateTime? CreateDate { get; set; }
                public long? CreatedById { get; set; }
                public string? CreatedByKey { get; set; }
                public string? CreatedByDisplayName { get; set; }



                public DateTime? ModifyDate { get; set; }
                public long? ModifyById { get; set; }
                public string? ModifyByKey { get; set; }
                public string? ModifyByDisplayName { get; set; }

                internal int? StatusId { get; set; }
                public string? StatusCode { get; set; }
                public string? StatusName { get; set; }

                public string? Response { get; set; }
            }
        }
    }
}
