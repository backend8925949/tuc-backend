//==================================================================================
// FileName: OVasOp.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Plugins.Vas.Object
{
    public class OVasOp
    {
        public class Option
        {
            public class Response
            {
                public long TotalItem { get; set; }
                public List<Category> Categories { get; set; }
            }
            public class Category
            {
                public long ReferenceId { get; set; }
                public string? Name { get; set; }
                public string? SystemName { get; set; }
                public double MaximumRewardPercentage { get; set; }
                internal double UserPercentage { get; set; }
                public List<Product> Products { get; set; }
            }
            public class Product
            {
                public long ReferenceId { get; set; }
                internal long CategoryId { get; set; }
                public string? Name { get; set; }
                public string? IconUrl { get; set; }
                public double MaximumRewardPercentage { get; set; }
                internal double? UserPercentage { get; set; }
                public List<ProductItem> ProductItems { get; set; }
            }
            public class ProductItem
            {
                public long ReferenceId { get; set; }
                public string? Name { get; set; }
                public double Amount { get; set; }
                public string? Validity { get; set; }
                public string? RewardAmountText { get; set; }
                public double RewardAmount { get; set; }
                public double MaximumRewardPercentage { get; set; }
                internal double? UserPercentage { get; set; }
                internal long ProductId { get; set; }
            }
        }
        public class Purchase
        {
            public class Initialize
            {
                public class Request
                {
                    public long AccountId { get; set; }
                    public string AccountKey { get; set; }
                    public long ReferenceId { get; set; }
                    public double Amount { get; set; }
                    public string? AccountNumber { get; set; }
                    public long ProductId { get; set; }
                    public OUserReference? UserReference { get; set; }
                }

                public class Response
                {
                    public string? VasCategory { get; set; }
                    public string? VasProduct { get; set; }
                    public string? VasProductItem { get; set; }
                    public double Amount { get; set; }
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? PaymentReference { get; set; }
                    public DateTime StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    public string? AccountNumber { get; set; }
                    public string? StatusCode { get; set; }
                    public string? StatusName { get; set; }
                    public string? Message { get; set; }
                    public object Data { get; set; }
                }
            }
            public class Validate
            {
                public class Request
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public OUserReference? UserReference { get; set; }
                }

                public class Response
                {
                    public string? VasCategory { get; set; }
                    public string? VasProduct { get; set; }
                    public string? VasProductItem { get; set; }
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? PaymentReference { get; set; }
                    public DateTime StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    public double Amount { get; set; }
                    public string? StatusCode { get; set; }
                    public string? StatusName { get; set; }
                    public object Data { get; set; }
                    public string? Message { get; set; }
                    public string? Token { get; set; }
                    public double UserRewardAmount { get; set; }
                }
            }

            public class Confirm
            {
                public class Request
                {
                    public long ProductId { get; set; }
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? PaymentReference { get; set; }
                    public OUserReference? UserReference { get; set; }
                }

                public class Response
                {
                    public string? VasCategory { get; set; }
                    public string? VasProduct { get; set; }
                    public string? VasProductItem { get; set; }
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? PaymentReference { get; set; }
                    public DateTime StartDate { get; set; }
                    public DateTime? EndDate { get; set; }
                    public double Amount { get; set; }
                    public string? StatusCode { get; set; }
                    public string? StatusName { get; set; }
                    public object Data { get; set; }
                    public string? Message { get; set; }
                    public string? Token { get; set; }
                    public double UserRewardAmount { get; set; }
                }
            }

            public class Cancel
            {
                public class Request
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? PaymentReference { get; set; }
                    public OUserReference? UserReference { get; set; }
                }

                public class Response
                {
                    public long ReferenceId { get; set; }
                    public string? ReferenceKey { get; set; }
                    public string? PaymentReference { get; set; }
                    public DateTime StartDate { get; set; }
                    public DateTime EndDate { get; set; }

                    public string? StatusCode { get; set; }
                    public string? StatusName { get; set; }
                }
            }
        }
    }
}
