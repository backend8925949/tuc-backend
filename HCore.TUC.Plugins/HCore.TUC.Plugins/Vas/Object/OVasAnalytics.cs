//==================================================================================
// FileName: OVasAnalytics.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Helper;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Plugins.Vas.Object
{
    public class OVasAnalytics
    {
        public class Request
        {
            public long CategoryId { get; set; }
            public long ProductId { get; set; }
            public long ProductItemId { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Stats
        {
            public  long ReferenceId { get; set; }
            public string? Name { get; set; }
            public Overview Overview { get; set; }
        }
        public class Overview
        {
            public long Transactions { get; set; }
            public long SuccessfulTransactions { get; set; }
            public long InitializedTransactions { get; set; }
            public long FailedTransactions { get; set; }
            public long Customers { get; set; }
            public double? InvoiceAmount { get; set; }
            public double? RewardAmount { get; set; }
            public double? UserRewardAmount { get; set; }
            public double? CommissionAmount { get; set; }
        }
        public class StatusOverview
        {
            public long Initialized { get; set; }
            public long Processing { get; set; }
            public long Success { get; set; }
            public long Failed { get; set; }
            public long Refunded { get; set; }
            public long Cancelled { get; set; }
        }
    }
}
