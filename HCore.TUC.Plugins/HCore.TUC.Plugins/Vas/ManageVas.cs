//==================================================================================
// FileName: ManageVas.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.TUC.Plugins.Vas.Framework;
using HCore.TUC.Plugins.Vas.Object;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Plugins.Vas
{
    public class ManageVas
    {
        FrameworkVas _FrameworkVas;
        FrameworkAnalytics _FrameworkAnalytics;
        /// <summary>
        /// Description: Gets the vas purchase overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasPurchaseOverview(OVasAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetVasPurchaseOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the vas purchase status overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasPurchaseStatusOverview(OVasAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetVasPurchaseStatusOverview(_Request);
        }
        /// <summary>
        /// Gets the vas purchase category overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasPurchaseCategoryOverview(OVasAnalytics.Request _Request)
        {
            _FrameworkAnalytics = new FrameworkAnalytics();
            return _FrameworkAnalytics.GetVasPurchaseCategoryOverview(_Request);
        }

        /// <summary>
        /// Description: Gets the vas category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasCategory(OList.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.GetVasCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the vas product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasProduct(OList.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.GetVasProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the vas product item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasProductItem(OList.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.GetVasProductItem(_Request);
        }

        /// <summary>
        /// Gets the vas product purchase.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasProductPurchase(OList.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.GetVasProductPurchase(_Request);
        }
        /// <summary>
        /// Description: Gets the vas product purchase overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasProductPurchaseOverview(OList.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.GetVasProductPurchaseOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the vas product purchase.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasProductPurchase(OReference _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.GetVasProductPurchase(_Request);
        }
        /// <summary>
        /// Gets the vas product purchase customer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVasProductPurchaseCustomer(OList.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.GetVasProductPurchaseCustomer(_Request);
        }
        /// <summary>
        /// Description: Updates the vas product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateVasProduct(OVas.Product.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.UpdateVasProduct(_Request);
        }
        /// <summary>
        /// Updates the vas product item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateVasProductItem(OVas.ProductItem.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.UpdateVasProductItem(_Request);
        }
        /// <summary>
        /// Description: Vases the payment confirm.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VasPayment_Confirm(OVasOp.Purchase.Confirm.Request _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.VasPayment_Confirm(_Request);
        }
        /// <summary>
        /// Description: Verifies the vas payment.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifyVasPayment(OReference _Request)
        {
            _FrameworkVas = new FrameworkVas();
            return _FrameworkVas.VerifyVasPayment(_Request);
        }
    }
}
