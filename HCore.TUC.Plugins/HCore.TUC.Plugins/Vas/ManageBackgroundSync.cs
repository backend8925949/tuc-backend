//==================================================================================
// FileName: ManageBackgroundSync.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using Akka.Actor;
using HCore.TUC.Plugins.Vas.Background;
using HCore.TUC.Plugins.Vas.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.TUC.Plugins.Vas
{
    public class ManageBackgroundSync
    {
        public void SyncVasItem()
        {
            var _ActorSystem = ActorSystem.Create("ActorSyncVasItem");
            var _ActorGreet = _ActorSystem.ActorOf<ActorSyncVasItem>("ActorSyncVasItem");
            _ActorGreet.Tell("SyncVasItem");
        }
        public void ProcessPendingVasPayments()
        {
            var _ActorSystem = ActorSystem.Create("ActorPendingVasPayments");
            var _ActorGreet = _ActorSystem.ActorOf<ActorPendingVasPayments>("ActorPendingVasPayments");
            _ActorGreet.Tell("PendingVasPayments");
        }
    }
    internal class ActorSyncVasItem : ReceiveActor
    {
        BackgroundSync _BackgroundSync;
        public ActorSyncVasItem()
        {
            Receive<string>(_Request =>
            {
                _BackgroundSync = new BackgroundSync();
                _BackgroundSync.SyncVasItem();
            });
        }
    }
    internal class ActorPendingVasPayments : ReceiveActor
    {
        FrameworkVasOperations _FrameworkVasOperations;
        public ActorPendingVasPayments()
        {
            Receive<string>(_Request =>
            {
                _FrameworkVasOperations = new FrameworkVasOperations();
                _FrameworkVasOperations.VasPayment_Verify();
            });
        }
    }
}
