//==================================================================================
// FileName: BackgroundSync.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Linq;
using static HCore.Helper.HCoreConstant;
using RestSharp;

namespace HCore.TUC.Plugins.Vas.Background
{
    internal class OBackgroundSync
    {
        public class OBiller
        {
            public int id { get; set; }
            public string? name { get; set; }
            public string? slug { get; set; }
            public int groupId { get; set; }
            public double? amount { get; set; }
            public double rewardPercentage { get; set; }
            public List<OBiller> Packages { get; set; }
        }
        public class OBillersResponse
        {
            public string? error { get; set; }
            public string? status { get; set; }
            public string? message { get; set; }
            public string? responseCode { get; set; }
            public List<OBiller> responseData { get; set; }
            public List<services> services { get; set; }
        }
        public class services
        {
            public string? service { get; set; }
            public string? name { get; set; }
            public string? category { get; set; }
            public string? query { get; set; }
        }
    }
    internal class BackgroundSync
    {
        VASProductItem _VASProductItem;
        VASProduct _VASProduct;
        HCoreContext _HCoreContext;
        internal void SyncVasItem()
        {
            try
            {

                #region Sync Nigeria Operations
                string Url = "http://204.8.207.124:8080/coralpay-vas/api/";
                if (HostEnvironment == HostEnvironmentType.BackgroudProcessor)
                {
                    Url = "https://vas.coralpay.com/vas-service/api/";
                }

                //using (_HCoreContext = new HCoreContext())
                //{
                //    var _ProductCategory = _HCoreContext.VASCategory.Where(x => x.ReferenceKey != null && x.CountryId == 1).ToList();
                //    foreach (var _Slug in _ProductCategory)
                //    {
                //        HttpWebRequest _GroupHttpWebRequest = (HttpWebRequest)WebRequest.Create(Url + "billers/group/slug/" + _Slug.ReferenceKey);
                //        _GroupHttpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                //        if (HostEnvironment == HostEnvironmentType.BackgroudProcessor)
                //        {
                //            string _GroupCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("thankucash:Th@nkUc@$h123"));
                //            _GroupHttpWebRequest.Headers.Add("Authorization", "Basic " + _GroupCredentials);
                //        }
                //        else
                //        {
                //            string _GroupCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("smashlabs:$m@$hl@b$1234"));
                //            _GroupHttpWebRequest.Headers.Add("Authorization", "Basic " + _GroupCredentials);
                //        }
                //        using (HttpWebResponse _GroupHttpWebResponse = (HttpWebResponse)_GroupHttpWebRequest.GetResponse())
                //        using (Stream _GroupStream = _GroupHttpWebResponse.GetResponseStream())
                //        using (StreamReader _GroupReader = new StreamReader(_GroupStream))
                //        {
                //            OBackgroundSync.OBillersResponse _GroupRequestBodyContent = JsonConvert.DeserializeObject<OBackgroundSync.OBillersResponse>(_GroupReader.ReadToEnd());
                //            if (_GroupRequestBodyContent != null && _GroupRequestBodyContent.status == "success" && _GroupRequestBodyContent.responseData != null && _GroupRequestBodyContent.responseData.Count > 0)
                //            {
                //                foreach (var _Item in _GroupRequestBodyContent.responseData)
                //                {
                //                    try
                //                    {
                //                        HttpWebRequest _GroupItemHttpWebRequest = (HttpWebRequest)WebRequest.Create(Url + "packages/biller/slug/" + _Item.slug);
                //                        _GroupItemHttpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                //                        if (HostEnvironment == HostEnvironmentType.BackgroudProcessor || HostEnvironment == HostEnvironmentType.Live)
                //                        {
                //                            string _GroupItemCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("thankucash:Th@nkUc@$h123"));
                //                            _GroupItemHttpWebRequest.Headers.Add("Authorization", "Basic " + _GroupItemCredentials);
                //                        }
                //                        else
                //                        {
                //                            string _GroupItemCredentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("smashlabs:$m@$hl@b$1234"));
                //                            _GroupItemHttpWebRequest.Headers.Add("Authorization", "Basic " + _GroupItemCredentials);
                //                        }
                //                        using (HttpWebResponse _GroupItemHttpWebResponse = (HttpWebResponse)_GroupItemHttpWebRequest.GetResponse())
                //                        using (Stream _GroupItemStream = _GroupItemHttpWebResponse.GetResponseStream())
                //                        using (StreamReader _GroupItemReader = new StreamReader(_GroupItemStream))
                //                        {
                //                            OBackgroundSync.OBillersResponse _GroupItemRequestBodyContent = JsonConvert.DeserializeObject<OBackgroundSync.OBillersResponse>(_GroupItemReader.ReadToEnd());
                //                            if (_GroupItemRequestBodyContent != null && _GroupItemRequestBodyContent.status == "success" && _GroupItemRequestBodyContent.responseData != null && _GroupItemRequestBodyContent.responseData.Count > 0)
                //                            {
                //                                _Item.Packages = _GroupItemRequestBodyContent.responseData;
                //                                using (_HCoreContext = new HCoreContext())
                //                                {
                //                                    long ProductId = 0;
                //                                    var VasProduct = _HCoreContext.VASProduct.Where(x => x.ReferenceKey == _Item.slug).FirstOrDefault();
                //                                    if (VasProduct == null)
                //                                    {
                //                                        _VASProduct = new VASProduct();
                //                                        _VASProduct.Guid = HCoreHelper.GenerateGuid();
                //                                        _VASProduct.CategoryId = _Slug.Id;
                //                                        _VASProduct.Name = _Item.name;
                //                                        _VASProduct.SystemName = HCoreHelper.GenerateSystemName(_Item.name);
                //                                        _VASProduct.ReferenceId = _Item.id;
                //                                        _VASProduct.ReferenceKey = _Item.slug;
                //                                        _VASProduct.IsFixedAmount = 0;
                //                                        if (_Item.amount != null && _Item.amount > 0)
                //                                        {
                //                                            _VASProduct.Amount = _Item.amount;
                //                                        }
                //                                        _VASProduct.RewardPercentage = 0;
                //                                        _VASProduct.UserPercentage = 0;
                //                                        _VASProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                //                                        _VASProduct.CreatedById = 1;
                //                                        _VASProduct.StatusId = 2;
                //                                        _HCoreContext.VASProduct.Add(_VASProduct);
                //                                        _HCoreContext.SaveChanges();
                //                                        ProductId = _VASProduct.Id;
                //                                    }
                //                                    else
                //                                    {
                //                                        VasProduct.ModifyDate = HCoreHelper.GetGMTDateTime();
                //                                        VasProduct.StatusId = 2;
                //                                        ProductId = VasProduct.Id;
                //                                        _HCoreContext.SaveChanges();
                //                                    }
                //                                    if (_Item.Packages.Count > 0)
                //                                    {
                //                                        using (_HCoreContext = new HCoreContext())
                //                                        {
                //                                            var VASProductItems = _HCoreContext.VASProductItem.Where(x => x.ProductId == ProductId).ToList();
                //                                            foreach (var VASProductItem in VASProductItems)
                //                                            {
                //                                                VASProductItem.StatusId = 3;
                //                                            }
                //                                            foreach (var item in _Item.Packages)
                //                                            {
                //                                                //var VasItem = _HCoreContext.VASProductItem.Where(x => x.ProductId == ProductId && x.ReferenceKey == item.slug).FirstOrDefault();
                //                                                var VasItem = VASProductItems.Where(x => x.ProductId == ProductId && x.ReferenceKey == item.slug).FirstOrDefault();
                //                                                if (VasItem != null)
                //                                                {
                //                                                    VasItem.StatusId = 2;
                //                                                    if (item.amount != null && item.amount > 0)
                //                                                    {
                //                                                        VasItem.Amount = item.amount;
                //                                                    }
                //                                                    VasItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                //                                                    VasItem.SyncTime = HCoreHelper.GetGMTDateTime();
                //                                                }
                //                                                else
                //                                                {
                //                                                    _VASProductItem = new VASProductItem();
                //                                                    _VASProductItem.Guid = HCoreHelper.GenerateGuid();
                //                                                    _VASProductItem.ProductId = ProductId;
                //                                                    _VASProductItem.Name = item.name;
                //                                                    _VASProductItem.SystemName = HCoreHelper.GenerateSystemName(item.name);
                //                                                    _VASProductItem.ReferenceId = item.id;
                //                                                    _VASProductItem.ReferenceKey = item.slug;
                //                                                    _VASProductItem.IsFixedAmount = 0;
                //                                                    if (item.amount != null && item.amount > 0)
                //                                                    {
                //                                                        _VASProductItem.Amount = item.amount;
                //                                                    }
                //                                                    _VASProductItem.RewardPercentage = 0;
                //                                                    _VASProductItem.UserPercentage = 0;
                //                                                    _VASProductItem.CreateDate = HCoreHelper.GetGMTDateTime();
                //                                                    _VASProductItem.CreatedById = 1;
                //                                                    _VASProductItem.StatusId = 2;
                //                                                    _HCoreContext.VASProductItem.Add(_VASProductItem);
                //                                                }
                //                                            }
                //                                            _HCoreContext.SaveChanges();
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                    catch (Exception _Exception)
                //                    {

                //                        throw;
                //                    }

                //                }
                //            }
                //        }
                //    }
                //}

                #endregion

                #region Sync Ghana Operations
                Url = "https://sandbox.expresspaygh.com/billpay/api.php";
                if (HostEnvironment == HostEnvironmentType.BackgroudProcessor || HostEnvironment == HostEnvironmentType.Live)
                {
                    Url = "https://expresspaygh.com/billpay/api.php";
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var client = new RestClient(Url);
                    var request = new RestRequest();
                    request.Method = Method.Post;
                    if (HostEnvironment == HostEnvironmentType.BackgroudProcessor || HostEnvironment == HostEnvironmentType.Live)
                    {
                        request.AddParameter("username", "thankucash_billpay@expresspaygh.com");
                        request.AddParameter("auth-token", "Dnc91Kjo91kp2BuSXQN3o-W4Xg07qz2QYcnOJxesTL-T6enmS5VLaUGgsM8rzmc-tlpcJ9OQqXLYRmmKFzwN-PzRPvGNzJawjPj6o9wN");
                    }
                    else
                    {
                        request.AddParameter("username", "thankucash_billpay@expresspaygh.com");
                        request.AddParameter("auth-token", "pkHovmCfFHzQtamsDxNBE-TVviA12YVNYZ8FnsE7rj-cboUilIHvwlvONx3z6mW-JBZudjd5ac8ASRMUnfrf-3sy2wKdSOFRBho1gVpE");
                    }
                    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                    request.AddParameter("type", "SERVICES");
                    var response = client.Execute(request);
                    OBackgroundSync.OBillersResponse _GroupRequestBodyContent = JsonConvert.DeserializeObject<OBackgroundSync.OBillersResponse>(response.Content);
                    foreach (var item in _GroupRequestBodyContent.services)
                    {
                        var VasItem = _HCoreContext.VASProduct.Where(x => x.ReferenceKey == item.service && x.Category.CountryId == 87).FirstOrDefault();
                        if (VasItem != null)
                        {
                            //VasItem.StatusId = 2;
                            //if (item.amount != null && item.amount > 0)
                            //{
                            //    VasItem.Amount = item.amount;
                            //}
                            //VasItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //VasItem.SyncTime = HCoreHelper.GetGMTDateTime();
                        }
                        else
                        {
                            _VASProduct = new VASProduct();
                            _VASProduct.Guid = HCoreHelper.GenerateGuid();
                            _VASProduct.CategoryId = 8;
                            _VASProduct.Name = item.name;
                            _VASProduct.SystemName = HCoreHelper.GenerateSystemName(item.name);
                            if (item.query == "TRUE")
                            {
                                _VASProduct.ReferenceId = 1;
                            }
                            else
                            {
                                _VASProduct.ReferenceId = 0;

                            }
                            _VASProduct.ReferenceKey = item.service;
                            _VASProduct.IsFixedAmount = 0;

                            _VASProduct.RewardPercentage = 0;
                            _VASProduct.UserPercentage = 0;
                            _VASProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                            _VASProduct.CreatedById = 1;
                            _VASProduct.StatusId = 2;
                            _HCoreContext.VASProduct.Add(_VASProduct);
                            _HCoreContext.SaveChanges();
                        }
                    }
                }
                #endregion


                #region Sync Ghana Operations
                Url = "https://sandbox.expresspaygh.com/billpay/api.php";
                if (HostEnvironment == HostEnvironmentType.BackgroudProcessor || HostEnvironment == HostEnvironmentType.Live)
                {
                    Url = "https://expresspaygh.com/billpay/api.php";
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _ProductCategory = _HCoreContext.VASProduct.Where(x => x.Category.CountryId == 87).ToList();
                    foreach (var Product in _ProductCategory)
                    {
                        var client = new RestClient(Url);
                        var request = new RestRequest();
                        request.Method = Method.Post;
                        if (HostEnvironment == HostEnvironmentType.BackgroudProcessor || HostEnvironment == HostEnvironmentType.Live)
                        {
                            request.AddParameter("username", "thankucash_billpay@expresspaygh.com");
                            request.AddParameter("auth-token", "Dnc91Kjo91kp2BuSXQN3o-W4Xg07qz2QYcnOJxesTL-T6enmS5VLaUGgsM8rzmc-tlpcJ9OQqXLYRmmKFzwN-PzRPvGNzJawjPj6o9wN");
                        }
                        else
                        {
                            request.AddParameter("username", "thankucash_billpay@expresspaygh.com");
                            request.AddParameter("auth-token", "pkHovmCfFHzQtamsDxNBE-TVviA12YVNYZ8FnsE7rj-cboUilIHvwlvONx3z6mW-JBZudjd5ac8ASRMUnfrf-3sy2wKdSOFRBho1gVpE");
                        }
                        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                        request.AddParameter("type", "SERVICES");
                        request.AddParameter("services", Product.ReferenceKey);
                        var response = client.Execute(request);
                        OBackgroundSync.OBillersResponse _GroupRequestBodyContent = JsonConvert.DeserializeObject<OBackgroundSync.OBillersResponse>(response.Content);
                        foreach (var item in _GroupRequestBodyContent.services)
                        {
                            var VasItem = _HCoreContext.VASProductItem.Where(x => x.ProductId == Product.Id && x.ReferenceKey == item.service).FirstOrDefault();
                            if (VasItem != null)
                            {
                                //VasItem.StatusId = 2;
                                //if (item.amount != null && item.amount > 0)
                                //{
                                //    VasItem.Amount = item.amount;
                                //}
                                //VasItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                //VasItem.SyncTime = HCoreHelper.GetGMTDateTime();
                            }
                            else
                            {
                                _VASProductItem = new VASProductItem();
                                _VASProductItem.Guid = HCoreHelper.GenerateGuid();
                                _VASProductItem.ProductId = Product.Id;
                                _VASProductItem.Name = item.name;
                                _VASProductItem.SystemName = HCoreHelper.GenerateSystemName(item.name);
                                //_VASProductItem.ReferenceId = item.id;
                                _VASProductItem.ReferenceKey = item.service;
                                if (item.query == "TRUE")
                                {
                                    _VASProductItem.IsFixedAmount = 1;
                                }
                                else
                                {
                                    _VASProductItem.IsFixedAmount = 0;
                                }
                                _VASProductItem.RewardPercentage = 0;
                                _VASProductItem.UserPercentage = 0;
                                _VASProductItem.CreateDate = HCoreHelper.GetGMTDateTime();
                                _VASProductItem.CreatedById = 1;
                                _VASProductItem.StatusId = 2;
                                _HCoreContext.VASProductItem.Add(_VASProductItem);
                            }
                        }
                        _HCoreContext.SaveChanges();
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                HCoreHelper.LogException("SyncVasItem", ex);
            }
        }
    }
}
