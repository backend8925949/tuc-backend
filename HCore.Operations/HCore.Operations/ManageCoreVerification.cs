//==================================================================================
// FileName: ManageCoreVerification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;

namespace HCore.Operations
{
    public class ManageCoreVerification
    {
        FrameworkVerification _FrameworkVerification;
        /// <summary>
        /// Description: Requests the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestOtp(OCoreVerificationManager.Request _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.RequestOtp(_Request);
        }
        /// <summary>
        /// Description: Verifies the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifyOtp(OCoreVerificationManager.RequestVerify _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.VerifyOtp(_Request);
        }


        /// <summary>
        /// Description: Requests the otp country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestOtpCountry(OCoreVerificationManager.Request _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.RequestOtpCountry(_Request);
        }
        /// <summary>
        /// Description: Verifies the otp country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifyOtpCountry(OCoreVerificationManager.RequestVerify _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.VerifyOtpCountry(_Request);
        }

        /// <summary>
        /// Description: Requests the otp country v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestOtpCountryV2(OCoreVerificationManager.Request _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.RequestOtpCountryV2(_Request);
        }
        /// <summary>
        /// Description: Verifies the otp country v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifyOtpCountryV2(OCoreVerificationManager.RequestVerify _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.VerifyOtpCountryV2(_Request);
        }
        /// <summary>
        /// Description: Requests the otp over voice call.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestOtpVoiceCall(OCoreVerificationManager.Request _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.RequestOtpVoiceCall(_Request);
        }
        /// <summary>
        /// Description: Requests the otp for merchant username update.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RequestOtpUserNameUpdate(OCoreVerificationManager.Request _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.RequestOtpUserNameUpdate(_Request);
        }
        /// <summary>
        /// Description: Requests the otp for merchant username update.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse VerifyOtpforUserName(OCoreVerificationManager.RequestVerify _Request)
        {
            _FrameworkVerification = new FrameworkVerification();
            return _FrameworkVerification.VerifyOtpforUserName(_Request);
        }
    }
}
