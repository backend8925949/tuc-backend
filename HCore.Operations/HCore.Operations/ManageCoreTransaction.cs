//==================================================================================
// FileName: ManageCoreTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations
{
    public class ManageCoreTransaction
    {
        FrameworkCoreTransaction _FrameworkCoreTransaction;
        /// <summary>
        /// Description: Refreshes the user balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse RefreshUserBalance(OBalance.Request _Request)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.RefreshUserBalance(_Request);
        }

        /// <summary>
        /// Description: Gets the user balance history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserBalanceHistory(OList.Request _Request)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetUserBalanceHistory(_Request);
        }
        /// <summary>
        /// Description: Updates the account balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>OAccountBalance.</returns>
        public OAccountBalance UpdateAccountBalance(long UserAccountId, int SourceId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.UpdateAccountBalance(UserAccountId, SourceId);
        }
        /// <summary>
        /// Description: Updates the account balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <param name="ParentId">The parent identifier.</param>
        /// <returns>OAccountBalance.</returns>
        public OAccountBalance UpdateAccountBalance(long UserAccountId, int SourceId, long ParentId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.UpdateAccountBalance(UserAccountId, SourceId, ParentId);
        }
        /// <summary>
        /// Description: Gets the user account balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <returns>HCore.Helper.OResponse.</returns>
        public HCore.Helper.OResponse GetUserAccountBalance(OBalance.Request UserAccountId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAccountBalance(UserAccountId);
        }

        /// <summary>
        /// Description: Gets the application user balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="ForceBalance">if set to <c>true</c> [force balance].</param>
        /// <returns>System.Double.</returns>
        public double GetAppUserBalance(long UserAccountId, bool ForceBalance = false)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAppUserBalance(UserAccountId, ForceBalance);
        }
        /// <summary>
        /// Description: Gets the customer balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <returns>OAccountBalance.</returns>
        public OAccountBalance GetCustomerBalance(long UserAccountId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAppUserBalance(UserAccountId);
        }
        /// <summary>
        /// Description: Gets the application user gift points balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <returns>System.Double.</returns>
        public double GetAppUserGiftPointsBalance(long UserAccountId, long MerchantId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAppUserGiftPointsBalance(UserAccountId, MerchantId);
        }
        /// <summary>
        /// Description: Gets the application user thank u cash plus balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <returns>System.Double.</returns>
        public double GetAppUserThankUCashPlusBalance(long UserAccountId, long MerchantId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAppUserThankUCashPlusBalance(UserAccountId, MerchantId);
        }

        /// <summary>
        /// Description :  Get loyalty program balance
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ProgramAccountId"></param>
        /// <returns></returns>
        public OLoyaltyProgram GetLoyaltyProgramBalance(long CustomerId, long MerchantId, long ProgramAccountId, int? TransactionSource = TransactionSource.TUCBlack)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetLoyaltyProgramBalance(CustomerId, MerchantId, ProgramAccountId, TransactionSource);
        }

        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>System.Double.</returns>
        public double GetAccountBalance(long AccountId, long SourceId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAccountBalance(AccountId, SourceId);
        }
        /// <summary>
        /// Description: Gets the merchant balance.
        /// </summary>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>System.Double.</returns>
        public double GetMerchantBalance(long MerchantId, long SourceId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetMerchantBalance(MerchantId, SourceId);
        }
        /// <summary>
        /// Description: Gets the application user balance.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>System.Double.</returns>
        public double GetAppUserBalance(long AccountId, long MerchantId, long SourceId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAppUserBalance(AccountId, MerchantId, SourceId);
        }
        /// <summary>
        /// Description: Gets the account balance overview.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>OAccountBalance.</returns>
        public OAccountBalance GetAccountBalanceOverview(long AccountId, long SourceId)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.GetAccountBalanceOverview(AccountId, SourceId);
        }

        /// <summary>
        /// Description: Processes the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OCoreTransaction.Response.</returns>
        public OCoreTransaction.Response ProcessTransaction(OCoreTransaction.Request _Request)
        {
            _FrameworkCoreTransaction = new FrameworkCoreTransaction();
            return _FrameworkCoreTransaction.ProcessTransaction(_Request);
        }
    }
}
