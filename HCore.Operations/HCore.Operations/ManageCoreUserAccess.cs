//==================================================================================
// FileName: ManageCoreUserAccess.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Operations.Framework;
using HCore.Operations.Object;

namespace HCore.Operations
{
    public class ManageCoreUserAccess
    {
        FrameworkCoreUserAccess _FrameworkCoreUserAccess;

        /// <summary>
        /// Creates app user account
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OAppProfile.Response CreateAppUserAccount(OAppProfile.Request _Request)
        {
            _FrameworkCoreUserAccess = new FrameworkCoreUserAccess();
            return _FrameworkCoreUserAccess.CreateAppUserAccount(_Request);

        }

        public OAppProfile.Response NewCreateAppUserAccount(OAppProfile.Request _Request)
        {
            _FrameworkCoreUserAccess = new FrameworkCoreUserAccess();
            return _FrameworkCoreUserAccess.NewCreateAppUserAccount(_Request);
        }

        public OAppProfile.Response CreateAppUserAccountLite(OAppProfileLite.Request _Request)
        {
            _FrameworkCoreUserAccess = new FrameworkCoreUserAccess();
            return _FrameworkCoreUserAccess.CreateAppUserAccountLite(_Request);
        }
    }
}
