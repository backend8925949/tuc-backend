//==================================================================================
// FileName: ManageCountry.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.Operations.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace HCore.Operations
{
    public class ManageCountry
    {

        FrameworkCountryManager _FrameworkCountryManager;
        /// <summary>
        /// Description: Gets the country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCountry(OList.Request _Request)
        {
            _FrameworkCountryManager = new FrameworkCountryManager();
            return _FrameworkCountryManager.GetCountry(_Request);
        }
        /// <summary>
        /// Description: Gets the region.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRegion(OList.Request _Request)
        {
            _FrameworkCountryManager = new FrameworkCountryManager();
            return _FrameworkCountryManager.GetRegion(_Request);
        }
        /// <summary>
        /// Description: Gets the city.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCity(OList.Request _Request)
        {
            _FrameworkCountryManager = new FrameworkCountryManager();
            return _FrameworkCountryManager.GetCity(_Request);
        }
        /// <summary>
        /// Description: Gets the city area.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCityArea(OList.Request _Request)
        {
            _FrameworkCountryManager = new FrameworkCountryManager();
            return _FrameworkCountryManager.GetCityArea(_Request);
        }
    }
}
