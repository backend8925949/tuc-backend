//==================================================================================
// FileName: ActorOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations.Actor
{
    public class ORequest
    {
        public string? zx { get; set; }
    }
    public class PostTransactionBalanceManagerActor : ReceiveActor
    {
        HCoreContext? _HCoreContext;
        HCUAccountBalance _HCUAccountBalance;
        public PostTransactionBalanceManagerActor()
        {
            Receive<OCoreTransaction.Request>(async _Request =>
            {
                var _PostTransactionActivityManagerActor = ActorSystem.Create("PostTransactionActivityManagerActor");
                var _PostTransactionActivityManagerActorNotify = _PostTransactionActivityManagerActor.ActorOf<PostTransactionActivityManagerActor>("PostTransactionActivityManagerActor");
                _PostTransactionActivityManagerActorNotify.Tell(_Request);

                var _TransactionPostProcessActor = ActorSystem.Create("TransactionPostProcessActor");
                var _TransactionPostProcessActorNotify = _TransactionPostProcessActor.ActorOf<TransactionPostProcessActor>("TransactionPostProcessActor");
                _TransactionPostProcessActorNotify.Tell(_Request);
                #region Transaction Balance Updater
                try
                {
                    FrameworkCoreTransactionProcessor _FrameworkCoreTransactionProcessor = new FrameworkCoreTransactionProcessor();
                    _FrameworkCoreTransactionProcessor.ValidateTransactions(_Request);
                    _FrameworkCoreTransactionProcessor.ProcessAccountBalanceOverview(_Request);
                }
                catch (Exception _ExceptionBalance)
                {
                    HCoreHelper.LogException("ProcessTransaction-UPDATEBALANCE", _ExceptionBalance, _Request.UserReference);
                }
                #endregion
            });
        }
    }

    public class PostTransactionActivityManagerActor : ReceiveActor
    {
        public PostTransactionActivityManagerActor()
        {
            Receive<OCoreTransaction.Request>(async _Request =>
            {
                #region Update Last Activity
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    DateTime TransactionDate = HCoreHelper.GetGMTDateTime();
                    if (_Request.TerminalId > 0)
                    {
                        var TerminalDetails = await _HCoreContext.TUCTerminal.Where(x => x.Id == _Request.TerminalId).FirstOrDefaultAsync();
                        if (TerminalDetails != null)
                        {
                            TerminalDetails.LastActivityDate = TransactionDate;
                            TerminalDetails.LastTransactionDate = TransactionDate;
                        }
                    }
                    var AccountDetails = await _HCoreContext.HCUAccount.
                       Where(x => x.Id == _Request.ParentId
                       || x.Id == _Request.SubParentId
                       || x.Id == _Request.BankId
                       || x.Id == _Request.CashierId
                       || x.Id == _Request.CustomerId
                       || x.Id == _Request.ProviderId
                       ).ToListAsync();
                    foreach (var AccountDetail in AccountDetails)
                    {
                        AccountDetail.LastTransactionDate = TransactionDate;
                    }
                    foreach (var item in _Request.Transactions)
                    {
                        var AccountInfo = await _HCoreContext.HCUAccount.Where(x => x.Id == item.UserAccountId).FirstOrDefaultAsync();
                        if (AccountInfo != null)
                        {
                            AccountInfo.LastTransactionDate = TransactionDate;
                        }
                    }
                    await _HCoreContext.SaveChangesAsync();
                }
                #endregion

            });
        }
    }

    public class TransactionPostProcessActor : ReceiveActor
    {
        public TransactionPostProcessActor()
        {
            Receive<OCoreTransaction.Request>(async _Request =>
            {
                FrameworkCoreTransactionProcessor _FrameworkCoreTransactionProcessor = new FrameworkCoreTransactionProcessor();
                _Request.CardBrandId = await _FrameworkCoreTransactionProcessor.ProcessBinNumber(_Request);
                _Request.UserReference = null;
                if (_Request.TerminalId > 0)
                {
                    if (Data.Store.HCoreDataStore.Accounts != null && Data.Store.HCoreDataStore.Accounts.Count > 0)
                    {
                        if (_Request.TerminalId > 0)
                        {
                            var CreatedBy = Data.Store.HCoreDataStore.Accounts.Where(x => x.AccountTypeId == _Request.CreatedById).FirstOrDefault();
                            if (CreatedBy != null)
                            {
                                Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.CreatedById).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                Data.Store.HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.CreatedById).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                            }
                        }
                        var ParentIdD = Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ParentId).FirstOrDefault();
                        if (ParentIdD != null)
                        {
                            Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ParentId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                            if (Data.Store.HCoreDataStore.Terminals != null && Data.Store.HCoreDataStore.Terminals.Count > 0)
                            {
                                if (Data.Store.HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.ParentId).FirstOrDefault() != null)
                                {
                                    Data.Store.HCoreDataStore.Terminals.Where(x => x.ReferenceId == _Request.ParentId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                }
                            }
                        }
                        var SubParentIdD = Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.SubParentId).FirstOrDefault();
                        if (SubParentIdD != null)
                        {
                            Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.SubParentId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                        }
                        var BankIdD = Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.BankId).FirstOrDefault();
                        if (BankIdD != null)
                        {
                            Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.BankId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                        }
                        var ProviderIdD = Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ProviderId).FirstOrDefault();
                        if (ProviderIdD != null)
                        {
                            Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == _Request.ProviderId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                        }
                        foreach (var TrItem in _Request.Transactions)
                        {
                            var AccDetails = Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == TrItem.UserAccountId).FirstOrDefault();
                            if (AccDetails != null)
                            {
                                Data.Store.HCoreDataStore.Accounts.Where(x => x.ReferenceId == TrItem.UserAccountId).FirstOrDefault().LastTransactionDate = HCoreHelper.GetGMTDateTime();
                            }
                        }
                    }
                }
                string ResponseBody = JsonConvert.SerializeObject(_Request,
                       new JsonSerializerSettings()
                       { NullValueHandling = NullValueHandling.Ignore });
                ORequest _ORequest = new ORequest
                {
                    zx = HCoreEncrypt.EncodeText(ResponseBody)
                };
                HttpClient _HttpClient = new HttpClient();
                string json = JsonConvert.SerializeObject(_ORequest);
                string PostUrl = "https://webconnect.thankucash.com/api/v2/thankucash/notifysystransaction";
                if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live
                || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test
                || HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Play)
                {
                    if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Live)
                    {
                        PostUrl = "https://webconnect.thankucash.com/api/v2/thankucash/notifysystransaction";
                    }
                    if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Test)
                    {
                        PostUrl = "https://testwebconnect.thankucash.com/api/v2/thankucash/notifysystransaction";
                    }
                    if (HCoreConstant.HostEnvironment == HCoreConstant.HostEnvironmentType.Play)
                    {
                        PostUrl = "https://playwebconnect.thankucash.com/api/v2/thankucash/notifysystransaction";
                    }
                    try
                    {
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(PostUrl);
                        request.ContentType = "application/json";
                        request.Headers["hcak"] = "MzNmNTFkYTQxYzk1NDE1ZmJhODJmZjhhOWY0OTgxYTU=";
                        request.Headers["hcavk"] = "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ=";
                        request.Headers["hctk"] = "dXBkYXRldGVybWluYWx0cmFuc2FjdGlvbg==";
                        request.Headers["hcudk"] = "MA==";
                        request.Headers["hcudln"] = "MA==";
                        request.Headers["hcudlt"] = "MA==";
                        request.Method = "POST";
                        request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                        using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                        {
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }
                        var httpResponse = (HttpWebResponse)request.GetResponse();
                        httpResponse.Close();
                    }
                    catch (System.Exception _Exception)
                    {
                        HCoreHelper.LogException("WEBCONNECT NOTIFY FAILED", _Exception);
                    }
                }
            });
        }
    }
}
