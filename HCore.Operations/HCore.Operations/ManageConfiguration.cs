//==================================================================================
// FileName: ManageConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;

namespace HCore.Operations
{
    public class ManageConfiguration
    {
        FrameworkConfiguration _FrameworkConfiguration;
        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfiguration(OSystemConfiguration.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfiguration(_Request);
        }

        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfiguration(OConfigurationManager.Save _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.SaveConfiguration(_Request);
        }

        /// <summary>
        /// Description: Updates the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateConfiguration(OConfigurationManager.Save _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.UpdateConfiguration(_Request);
        }
        /// <summary>
        /// Description: Deletes the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteConfiguration(OConfigurationManager.Save _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.DeleteConfiguration(_Request);
        }
        /// <summary>
        /// Description: Saves the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfigurationValue(OConfigurationManager.Save _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.SaveConfigurationValue(_Request);
        }

        /// <summary>
        /// Description: Saves the type of the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfigurationAccountType(OConfigurationManager.Save _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.SaveConfigurationAccountType(_Request);
        }
        /// <summary>
        /// Description: Deletes the type of the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteConfigurationAccountType(OConfigurationManager.Save _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.DeleteConfigurationAccountType(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfiguration(OConfigurationManager.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfiguration(OList.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfigurationValue(OList.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfigurationValue(_Request);
        }
        /// <summary>
        /// Description: Gets the type of the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfigurationAccountType(OList.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfigurationAccountType(_Request);
        }
    }
}
