//==================================================================================
// FileName: ManageCoreOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;

namespace HCore.Operations
{
    public class ManageCoreOperations
    {
        FrameworkCoreHelper _FrameworkCoreHelper;
        FrameworkCoreHelperManager _FrameworkCoreHelperManager;
        /// <summary>
        /// Description: Saves the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.SaveCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Saves the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.SaveCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Saves the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.SaveCoreParameter(_Request);
        }
        /// <summary>
        /// Description: Updates the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.UpdateCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Updates the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.UpdateCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Updates the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.UpdateCoreParameter(_Request);
        }
        /// <summary>
        /// Description: Deletes the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.DeleteCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Deletes the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.DeleteCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Deletes the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelperManager = new FrameworkCoreHelperManager();
            return _FrameworkCoreHelperManager.DeleteCoreParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreHelper(OCoreHelper.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Gets the status list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStatusList(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetStatusList(_Request);
        }
        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreHelper(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreHelper(_Request);
        }
        /// <summary>
        /// Description: Gets the core helper lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreHelperLite(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreHelperLite(_Request);
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreCommon(OCoreCommon.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreCommon(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreCommon(_Request);
        }
        /// <summary>
        /// Description: Gets the core common lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreCommonLite(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreCommonLite(_Request);
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreParameter(OCoreParameter.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreParameter(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreLog(OCoreLog _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreLog(_Request);
        }
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreLog(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreLog(_Request);
        }
        /// <summary>
        /// Description: Gets the core usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreUsage(OCoreUsage.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreUsage(_Request);
        }
        /// <summary>
        /// Description: Gets the core usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreUsage(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreUsage(_Request);
        }
        /// <summary>
        /// Description: Gets the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStorage(OStorage.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetStorage(_Request);
        }
        /// <summary>
        /// Description: Gets the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStorage(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetStorage(_Request);
        }
        /// <summary>
        /// Description: Gets the core verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreVerification(OCoreVerification.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreVerification(_Request);
        }
        /// <summary>
        /// Description: Gets the core verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCoreVerification(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetCoreVerification(_Request);
        }
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccount(OUserAccount.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccount(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserAccount(_Request);
        }
        /// <summary>
        /// Description: Gets the user account lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccountLite(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserAccountLite(_Request);
        }
        /// <summary>
        /// Description: Gets the user account owner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccountOwner(OUserAccountOwner.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserAccountOwner(_Request);
        }
        /// <summary>
        /// Description: Gets the user account owner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserAccountOwner(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserAccountOwner(_Request);
        }
        /// <summary>
        /// Description: Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransaction(OTransaction.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetTransaction(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the user activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserActivity(OUserActivity.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserActivity(_Request);
        }
        /// <summary>
        /// Description: Gets the user activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserActivity(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserActivity(_Request);
        }
        /// <summary>
        /// Description: Gets the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserDevice(OUserDevice.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserDevice(_Request);
        }
        /// <summary>
        /// Description: Gets the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserDevice(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserDevice(_Request);
        }
        /// <summary>
        /// Description: Gets the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserSession(OUserSession.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserSession(_Request);
        }
        /// <summary>
        /// Description: Gets the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserSession(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserSession(_Request);
        }
        /// <summary>
        /// Description: Gets the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserParameter(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserParameter(_Request);
        }
        /// <summary>
        /// Description: Gets the user location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserLocation(OUserLocation.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserLocation(_Request);
        }
        /// <summary>
        /// Description: Gets the user location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserLocation(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserLocation(_Request);
        }
        /// <summary>
        /// Description: Gets the user invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserInvoice(OUserInvoice.Manage _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserInvoice(_Request);
        }
        /// <summary>
        /// Description: Gets the user invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetUserInvoice(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetUserInvoice(_Request);
        }
        /// <summary>
        /// Description: Gets the role list for user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRoleListForUser(OList.Request _Request)
        {
            _FrameworkCoreHelper = new FrameworkCoreHelper();
            return _FrameworkCoreHelper.GetRoleListForUser(_Request);
        }


    }
}
