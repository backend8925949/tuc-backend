//==================================================================================
// FileName: ManageSystem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;

namespace HCore.Operations
{
    public class ManageSystem
    {
        FrameworkSystem _FrameworkSystem;
        /// <summary>
        /// Description: Saves the state of the list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveListState(OListState _Request)
        {   
            _FrameworkSystem = new FrameworkSystem();
            return _FrameworkSystem.SaveListState(_Request);
        }
        /// <summary>
        /// Description: Deletes the state of the list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteListState(OListState _Request)
        {
            _FrameworkSystem = new FrameworkSystem();
            return _FrameworkSystem.DeleteListState(_Request);
        }
        /// <summary>
        /// Description: Gets the state of the list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetListState(OList.Request _Request)
        {
            _FrameworkSystem = new FrameworkSystem();
            return _FrameworkSystem.GetListState(_Request);
        }
     }
}
