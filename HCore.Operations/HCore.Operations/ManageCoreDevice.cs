//==================================================================================
// FileName: ManageCoreDevice.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;

namespace HCore.Operations
{
    public class ManageCoreDevice
    {
        FrameworkDevice _FrameworkDevice;
        /// <summary>
        /// Description: Checks the application version.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CheckAppVersion(AppVersionCheck.Request _Request)
        {
            _FrameworkDevice = new FrameworkDevice();
            return _FrameworkDevice.CheckAppVersion(_Request);
        }
        /// <summary>
        /// Description: Connects the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ConnectApp(ODevice.Request _Request)
        {
            _FrameworkDevice = new FrameworkDevice();
            return _FrameworkDevice.ConnectApp(_Request);
        }
        /// <summary>
        /// Description: Connects the device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ConnectDevice(ODevice.Request _Request)
        {
            _FrameworkDevice = new FrameworkDevice();
            return _FrameworkDevice.ConnectDevice(_Request);
        }
        /// <summary>
        /// Description: Updates the device notification URL.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateDeviceNotificationUrl(ODevice.Request _Request)
        {
            _FrameworkDevice = new FrameworkDevice();
            return _FrameworkDevice.UpdateDeviceNotificationUrl(_Request);
        }
    }
}
