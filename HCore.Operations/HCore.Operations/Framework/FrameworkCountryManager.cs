//==================================================================================
// FileName: FrameworkCountryManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to country
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations.Framework
{
    public class ORef
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }
        public string? Name { get; set; }
    }

    internal class FrameworkCountryManager
    {
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Gets the country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCountry(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreCountry
                                                 where x.StatusId == HelperStatus.Default.Active
                                                 select new ORef
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                 })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    }

                    #endregion
                    #region Get Data
                    List<ORef> Data = (from x in _HCoreContext.HCCoreCountry
                                       where x.StatusId == HelperStatus.Default.Active
                                       select new ORef
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           Name = x.Name,
                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCountry", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the region.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRegion(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreCountryState
                                                 where x.CountryId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                                 select new ORef
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                 })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    }

                    #endregion
                    #region Get Data
                    List<ORef> Data = (from x in _HCoreContext.HCCoreCountryState
                                       where x.CountryId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                       select new ORef
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           Name = x.Name,
                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRegion", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the city.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCity(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreCountryStateCity
                                                 where x.StateId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                                 select new ORef
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                 })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    }

                    #endregion
                    #region Get Data
                    List<ORef> Data = (from x in _HCoreContext.HCCoreCountryStateCity
                                       where x.StateId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                       select new ORef
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           Name = x.Name,
                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRegion", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Gets the city area.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCityArea(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "Name", "asc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreCountryStateCityArea
                                                 where x.CityId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                                 select new ORef
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                 })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    }

                    #endregion
                    #region Get Data
                    List<ORef> Data = (from x in _HCoreContext.HCCoreCountryStateCityArea
                                       where x.CityId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active
                                       select new ORef
                                       {
                                           ReferenceId = x.Id,
                                           ReferenceKey = x.Guid,
                                           Name = x.Name,
                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCityArea", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
