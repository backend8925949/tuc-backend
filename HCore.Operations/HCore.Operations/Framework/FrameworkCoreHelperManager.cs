//==================================================================================
// FileName: FrameworkCoreHelperManager.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core helper manager
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;

namespace HCore.Operations.Framework
{
    public class FrameworkCoreHelperManager
    {
        #region Declare
        HCoreContext _HCoreContext;
        HCCore _HCCore;
        HCCoreCommon _HCCoreCommon;
        HCCoreParameter _HCCoreParameter;
        #endregion
        /// <summary>
        /// Description: Saves the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                long? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    int? ParentId = HCoreHelper.GetSystemHelperId(_Request.ParentCode, _Request.UserReference);
                    int? SubParentId = HCoreHelper.GetSystemHelperId(_Request.SubParentCode, _Request.UserReference);
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _Request.AllowDuplicateName == false)
                        {
                            long NameCheck = _HCoreContext.HCCore
                               .Where(x => x.Name == _Request.Name)
                               .Select(x => x.Id).FirstOrDefault();
                            if (NameCheck != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                #endregion
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.SystemName) && _Request.AllowDuplicateSystemName == false)
                        {
                            long NameCheck = _HCoreContext.HCCore
                               .Where(x => x.SystemName == _Request.SystemName)
                               .Select(x => x.Id).FirstOrDefault();
                            if (NameCheck != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                #endregion
                            }
                        }
                        if (!string.IsNullOrEmpty(_Request.ParentCode) && _Request.AllowDuplicateParentCode == false)
                        {
                            long NameCheck = _HCoreContext.HCCore
                               .Where(x => x.Parent.SystemName == _Request.ParentCode)
                               .Select(x => x.Id).FirstOrDefault();
                            if (NameCheck != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                #endregion
                            }
                        }
                        long VSystemName = _HCoreContext.HCCore
                                .Where(x => x.SystemName == _Request.SystemName)
                                .Select(x => x.Id).FirstOrDefault();
                        if (VSystemName == 0)
                        {
                            _HCCore = new HCCore();
                            //_HCCore.Guid = HCoreHelper.GenerateGuid();
                            _HCCore.ParentId = ParentId;
                            _HCCore.SubParentId = SubParentId;
                            _HCCore.Name = _Request.Name;
                            _HCCore.SystemName = _Request.SystemName;
                            _HCCore.TypeName = _Request.TypeName;
                            _HCCore.Value = _Request.Value;
                            //_HCCore.Description = _Request.Description;
                            _HCCore.Sequence = _Request.Sequence;
                            //_HCCore.IconStorageId = IconStorageId;
                            //_HCCore.PosterStorageId = PosterStorageId;
                            _HCCore.Sequence = _Request.Sequence;
                            //_HCCore.CreateDate = HCoreHelper.GetGMTDateTime();
                            //_HCCore.CreatedById = _Request.UserReference.AccountId;
                            _HCCore.StatusId = (int)StatusId;
                            _HCoreContext.HCCore.Add(_HCCore);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            _Request.ReferenceKey = _HCCore.SystemName;



                            //long? IconStorageId = null;
                            //long? PosterStorageId = null;
                            //if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                            //{
                            //    IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                            //}
                            //if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                            //{
                            //    PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                            //}
                            //if (IconStorageId != null || PosterStorageId != null)
                            //{
                            //    using (_HCoreContext = new HCoreContext())
                            //    {
                            //        var UserAccDetails = _HCoreContext.HCCore.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                            //        if (UserAccDetails != null)
                            //        {
                            //            if (IconStorageId != null)
                            //            {
                            //                UserAccDetails.IconStorageId = IconStorageId;
                            //            }
                            //            if (PosterStorageId != null)
                            //            {
                            //                UserAccDetails.PosterStorageId = PosterStorageId;
                            //            }
                            //            _HCoreContext.SaveChanges();
                            //        }
                            //    }
                            //}


                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    long? ParentId = HCoreHelper.GetCoreCommonId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreCommonId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    #region Operations
                    if (ParentId != 0 && _Request.DisableChild == true)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var ChildList = _HCoreContext.HCCoreCommon.Where(x => x.ParentId == ParentId && x.StatusId == HelperStatus.Default.Active).ToList();
                            if (ChildList.Count > 0)
                            {
                                foreach (var ChildItem in ChildList)
                                {
                                    ChildItem.StatusId = HelperStatus.Default.Blocked;
                                    ChildItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    ChildItem.ModifyById = _Request.UserReference.AccountId;
                                }
                                _HCoreContext.SaveChanges();
                            }
                        }
                    }

                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.AllowDuplicateName == false && !string.IsNullOrEmpty(_Request.Name))
                        {
                            long NameCheck = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == TypeId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                            if (NameCheck != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                                #endregion
                            }
                        }
                        _HCCoreCommon = new HCCoreCommon();
                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreCommon.ParentId = ParentId;
                        _HCCoreCommon.SubParentId = SubParentId;
                        _HCCoreCommon.AccountId = UserAccountId;
                        _HCCoreCommon.TypeId = (int)TypeId;
                        _HCCoreCommon.Name = _Request.Name;
                        _HCCoreCommon.SystemName = _Request.SystemName;
                        _HCCoreCommon.Value = _Request.Value;
                        _HCCoreCommon.SubValue = _Request.SubValue;
                        _HCCoreCommon.Data = _Request.Data;
                        _HCCoreCommon.Description = _Request.Description;
                        _HCCoreCommon.HelperId = HelperId;
                        _HCCoreCommon.Sequence = _Request.Sequence;
                        _HCCoreCommon.Count = _Request.Count;
                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreCommon.StatusId = (int)StatusId;
                        _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _HCCoreCommon.Guid;




                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.IconStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        UserAccDetails.PosterStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }


                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCoreCommon", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                long? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName) && !string.IsNullOrEmpty(_Request.Name))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    long? ParentId = HCoreHelper.GetCoreParameterId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreParameterId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                    long? SubCommonId = HCoreHelper.GetCoreCommonId(_Request.SubCommonKey, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        if (_Request.AllowDuplicateName == false && !string.IsNullOrEmpty(_Request.Name))
                        {
                            long NameCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == TypeId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                            if (NameCheck != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                                #endregion
                            }
                        }
                        _HCCoreParameter = new HCCoreParameter();
                        _HCCoreParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCCoreParameter.ParentId = ParentId;
                        _HCCoreParameter.SubParentId = SubParentId;
                        _HCCoreParameter.AccountId = UserAccountId;
                        _HCCoreParameter.TypeId = TypeId;
                        _HCCoreParameter.Name = _Request.Name;
                        _HCCoreParameter.SystemName = _Request.SystemName;
                        _HCCoreParameter.Value = _Request.Value;
                        _HCCoreParameter.SubValue = _Request.SubValue;
                        _HCCoreParameter.Data = _Request.Data;
                        _HCCoreParameter.Description = _Request.Description;
                        _HCCoreParameter.HelperId = HelperId;
                        _HCCoreParameter.CommonId = CommonId;
                        _HCCoreParameter.SubCommonId = SubCommonId;
                        _HCCoreParameter.Sequence = _Request.Sequence;
                        _HCCoreParameter.Count = _Request.Count;
                        _HCCoreParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCCoreParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCCoreParameter.StatusId = (int)StatusId;
                        _HCoreContext.HCCoreParameter.Add(_HCCoreParameter);
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _HCCoreParameter.Guid;
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.IconStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        UserAccDetails.PosterStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Name))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    #region Operations
                    int? ParentId = HCoreHelper.GetSystemHelperId(_Request.ParentCode, _Request.UserReference);
                    int? SubParentId = HCoreHelper.GetSystemHelperId(_Request.SubParentCode, _Request.UserReference);
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);

                    using (_HCoreContext = new HCoreContext())
                    {
                        var Details = _HCoreContext.HCCore.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name && _Request.AllowDuplicateName == false)
                            {
                                long NameCheck = _HCoreContext.HCCore
                                   .Where(x => x.Name == _Request.Name)
                                   .Select(x => x.Id).FirstOrDefault();
                                if (NameCheck != 0)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                    #endregion
                                }
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName) && Details.SystemName != _Request.SystemName && _Request.AllowDuplicateSystemName == false)
                            {
                                long NameCheck = _HCoreContext.HCCore
                                   .Where(x => x.SystemName == _Request.SystemName)
                                   .Select(x => x.Id).FirstOrDefault();
                                if (NameCheck != 0)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                    #endregion
                                }
                            }
                            if (!string.IsNullOrEmpty(_Request.ParentCode) && Details.ParentId != ParentId && _Request.AllowDuplicateParentCode == false)
                            {
                                long NameCheck = _HCoreContext.HCCore
                                   .Where(x => x.Parent.SystemName == _Request.ParentCode)
                                   .Select(x => x.Id).FirstOrDefault();
                                if (NameCheck != 0)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                    #endregion
                                }
                            }
                            long VSystemName = _HCoreContext.HCCore
                            .Where(x => x.SystemName == _Request.SystemName && x.Id != Details.Id)
                            .Select(x => x.Id).FirstOrDefault();
                            if (VSystemName != 0)
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                                #endregion
                            }

                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }
                            if (SubParentId != null)
                            {
                                Details.SubParentId = SubParentId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName))
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value))
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.TypeName))
                            {
                                Details.TypeName = _Request.TypeName;
                            }
                            //if (!string.IsNullOrEmpty(_Request.Description))
                            //{
                            //    Details.Description = _Request.Description;
                            //}
                            if (_Request.Sequence != 0)
                            {
                                Details.Sequence = _Request.Sequence;
                            }
                            //Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            //long? TIconStorageId = Details.IconStorageId;
                            //long? TPosterStorageId = Details.PosterStorageId;
                            long HelperId = Details.Id;
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            //long? IconStorageId = null;
                            //long? PosterStorageId = null;
                            //if (_Request.IconContent != null)
                            //{
                            //    IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, TIconStorageId, _Request.UserReference);
                            //}
                            //if (_Request.PosterContent != null)
                            //{
                            //    PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, TPosterStorageId, _Request.UserReference);
                            //}
                            //if (IconStorageId != null || PosterStorageId != null)
                            //{
                            //    using (_HCoreContext = new HCoreContext())
                            //    {
                            //        var UpdateDetails = _HCoreContext.HCCore.Where(x => x.Id == HelperId).FirstOrDefault();
                            //        if (UpdateDetails != null)
                            //        {
                            //            if (IconStorageId != null)
                            //            {
                            //                UpdateDetails.IconStorageId = IconStorageId;
                            //            }

                            //            if (PosterStorageId != null)
                            //            {
                            //                UpdateDetails.PosterStorageId = PosterStorageId;
                            //            }
                            //            UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            //            UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                            //            _HCoreContext.SaveChanges();
                            //        }
                            //    }
                            //}

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    long? ParentId = HCoreHelper.GetCoreCommonId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreCommonId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon Details = _HCoreContext.HCCoreCommon
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            if (_Request.AllowDuplicateName == false && !string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                            {
                                long NameCheck = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == TypeId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                                if (NameCheck != 0)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                                    #endregion
                                }
                            }
                            //if (Details.SystemName != _Request.SystemName)
                            //{
                            //    long VSystemName = _HCoreContext.HCCoreCommon
                            //.Where(x => x.SystemName == _Request.SystemName && x.TypeId == TypeId && x.Id != Details.Id)
                            //.Select(x => x.Id).FirstOrDefault();
                            //    if (VSystemName != 0)
                            //    {
                            //        #region Send Response
                            //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            //        #endregion
                            //    }
                            //}
                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }

                            if (SubParentId != null)
                            {
                                Details.SubParentId = SubParentId;
                            }
                            if (UserAccountId != null)
                            {
                                Details.AccountId = UserAccountId;
                            }
                            if (TypeId != null)
                            {
                                Details.TypeId = (int)TypeId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName))
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value))
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.Data))
                            {
                                Details.Data = _Request.Data;
                            }
                            if (!string.IsNullOrEmpty(_Request.SubValue))
                            {
                                Details.SubValue = _Request.SubValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.Description))
                            {
                                Details.Description = _Request.Description;
                            }
                            if (HelperId != null)
                            {
                                Details.HelperId = HelperId;
                            }
                            if (_Request.Sequence != null)
                            {
                                Details.Sequence = _Request.Sequence;
                            }
                            if (_Request.Count != null)
                            {
                                Details.Count = _Request.Count;
                            }
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            long? TIconStorageId = Details.IconStorageId;
                            long? TPosterStorageId = Details.PosterStorageId;
                            _HCoreContext.SaveChanges();
                            long? IconStorageId = null;
                            long? PosterStorageId = null;
                            if (_Request.IconContent != null)
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, TIconStorageId, _Request.UserReference);
                            }
                            if (_Request.PosterContent != null)
                            {
                                PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, TPosterStorageId, _Request.UserReference);
                            }
                            if (IconStorageId != null || PosterStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UpdateDetails = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                    if (UpdateDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            UpdateDetails.IconStorageId = IconStorageId;
                                        }

                                        if (PosterStorageId != null)
                                        {
                                            UpdateDetails.PosterStorageId = PosterStorageId;
                                        }
                                        UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    if (string.IsNullOrEmpty(_Request.SystemName) && !string.IsNullOrEmpty(_Request.Name))
                    {
                        _Request.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    }
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    long? ParentId = HCoreHelper.GetCoreParameterId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetCoreParameterId(_Request.SubParentKey, _Request.UserReference);
                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                    long? SubCommonId = HCoreHelper.GetCoreCommonId(_Request.SubCommonKey, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreParameter Details = _HCoreContext.HCCoreParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            if (_Request.AllowDuplicateName == false && !string.IsNullOrEmpty(_Request.Name) && Details.Name != _Request.Name)
                            {
                                long NameCheck = _HCoreContext.HCCoreParameter.Where(x => x.TypeId == TypeId && x.Name == _Request.Name).Select(x => x.Id).FirstOrDefault();
                                if (NameCheck != 0)
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                                    #endregion
                                }
                            }
                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }
                            if (TypeId != null)
                            {
                                Details.TypeId = TypeId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName))
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value))
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.SubValue))
                            {
                                Details.SubValue = _Request.SubValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.Data))
                            {
                                Details.Data = _Request.Data;
                            }
                            if (!string.IsNullOrEmpty(_Request.Description))
                            {
                                Details.Description = _Request.Description;
                            }
                            if (HelperId != null)
                            {
                                Details.HelperId = HelperId;
                            }
                            if (CommonId != null)
                            {
                                Details.CommonId = CommonId;
                            }
                            if (UserAccountId != null)
                            {
                                Details.AccountId = UserAccountId;
                            }
                            if (_Request.Sequence != null)
                            {
                                Details.Sequence = _Request.Sequence;
                            }
                            if (_Request.Count != null)
                            {
                                Details.Count = _Request.Count;
                            }
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            long? TIconStorageId = Details.IconStorageId;
                            long? TPosterStorageId = Details.PosterStorageId;

                            _HCoreContext.SaveChanges();
                            long? IconStorageId = null;
                            long? PosterStorageId = null;
                            if (_Request.IconContent != null)
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, TIconStorageId, _Request.UserReference);
                            }
                            if (_Request.PosterContent != null)
                            {
                                PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, TPosterStorageId, _Request.UserReference);
                            }
                            if (IconStorageId != null || PosterStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UpdateDetails = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                    if (UpdateDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            UpdateDetails.IconStorageId = IconStorageId;
                                        }

                                        if (PosterStorageId != null)
                                        {
                                            UpdateDetails.PosterStorageId = PosterStorageId;
                                        }
                                        UpdateDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        UpdateDetails.ModifyById = _Request.UserReference.AccountId;
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCore Details = _HCoreContext.HCCore.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.HCCore.Remove(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1002", Resources.Resources.ResourceItems.HC1002);
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon Details = _HCoreContext.HCCoreCommon.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.HCCoreCommon.Remove(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000", Resources.Resources.ResourceItems.HC1000);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1001", Resources.Resources.ResourceItems.HC1001);
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("DeleteCoreCommon", _Exception, _Request.UserReference);
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1003", Resources.Resources.ResourceItems.HC1003);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreParameter Details = _HCoreContext.HCCoreParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.HCCoreParameter.Remove(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
    }
}
