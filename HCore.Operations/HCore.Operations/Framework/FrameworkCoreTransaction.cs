//==================================================================================
// FileName: FrameworkCoreTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core transaction
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations.Actor;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;
namespace HCore.Operations.Framework
{
    public class FrameworkCoreTransaction
    {
        OCoreTransaction.Response _TransactionResponse;
        HCoreContext _HCoreContext;
        HCUAccountBalance _HCUAccountBalance;
        HCUAccountTransaction _HCUAccountTransaction;
        HCUAccountTransaction _HCUAccountTransactionParent;
        List<HCUAccountTransaction> _HCUAccountTransactionChild;
        OAccountBalance _OAccountBalance;
        OBalance.Response _BalanceResponse;
        List<OBalance.UserMerchantBalance> _UserMerchantBalances;
        /// <summary>
        /// Description: Refreshes the user balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RefreshUserBalance(OBalance.Request _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentDate = HCoreHelper.GetGMTDate();
                    var UserTransactionMerchants = _HCoreContext.HCUAccountTransaction
                                                                .Where(x => x.AccountId == _Request.UserAccountId)
                                                                .Select(x => x.ParentId).Distinct().ToList();
                    foreach (var ParentId in UserTransactionMerchants)
                    {
                        if (ParentId != null)
                        {
                            var UserTransactionSources = _HCoreContext.HCUAccountTransaction
                                                             .Where(x => x.AccountId == _Request.UserAccountId && x.ParentId == ParentId)
                                                             .Select(x => x.SourceId).Distinct().ToList();
                            foreach (var SourceId in UserTransactionSources)
                            {

                                double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                .Where(x => x.AccountId == _Request.UserAccountId &&
                                                       x.SourceId == SourceId &&
                                                   x.ProgramId == null &&
                                                       x.ParentId == ParentId &&
                                                       x.ModeId == TransactionMode.Credit &&
                                                       x.StatusId == Transaction.Success)
                                                .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                 .Where(x => x.AccountId == _Request.UserAccountId &&
                                                                          x.SourceId == SourceId &&
                                                   x.ProgramId == null &&
                                                                     x.ParentId == ParentId &&
                                                                         x.ModeId == TransactionMode.Debit &&
                                                                         x.StatusId == Transaction.Success)
                                                                  .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                long Transactions = _HCoreContext.HCUAccountTransaction
                                                             .Where(x => x.AccountId == _Request.UserAccountId &&
                                                                      x.SourceId == SourceId &&
                                                   x.ProgramId == null &&
                                                                      x.ParentId == ParentId &&
                                                                     (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit) &&
                                                                     x.StatusId == Transaction.Success).Count();
                                double Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                                DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == _Request.UserAccountId
                                                                                            && x.ParentId == ParentId
                                                                                           && x.SourceId == SourceId)
                                                              .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                                var UserBalance = _HCoreContext.HCUAccountBalance
                                                  .Where(x => x.AccountId == _Request.UserAccountId
                                                  && x.SourceId == SourceId
                                                  && x.ParentId == ParentId
                                                  ).FirstOrDefault();
                                if (UserBalance != null)
                                {
                                    UserBalance.Credit = Credit;
                                    UserBalance.Debit = Debit;
                                    UserBalance.Balance = Balance;
                                    UserBalance.Transactions = Transactions;
                                    UserBalance.LastTransactionDate = LastTransactionDate;
                                }
                                else
                                {
                                    if (Transactions > 0)
                                    {
                                        _HCUAccountBalance = new HCUAccountBalance();
                                        _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                        _HCUAccountBalance.AccountId = _Request.UserAccountId;
                                        _HCUAccountBalance.ParentId = ParentId;
                                        _HCUAccountBalance.SourceId = (int)SourceId;
                                        _HCUAccountBalance.Credit = Credit;
                                        _HCUAccountBalance.Debit = Debit;
                                        _HCUAccountBalance.Balance = Balance;
                                        _HCUAccountBalance.Transactions = Transactions;
                                        _HCUAccountBalance.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                        _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                        _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                                    }
                                }
                            }
                        }
                    }
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                    UpdateAccountBalance(_Request.UserAccountId, TransactionSource.TUC);
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013", "Balance refresh complete");
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("RefreshUserBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
        }
        /// <summary>
        /// Description: Updates the account balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>OAccountBalance.</returns>
        internal OAccountBalance UpdateAccountBalance(long UserAccountId, int SourceId)
        {
            using (_HCoreContext = new HCoreContext())
            {
                _OAccountBalance = new OAccountBalance();
                DateTime CurrentDate = HCoreHelper.GetGMTDate();
                _OAccountBalance.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                              .Where(x => x.AccountId == UserAccountId &&
                                                     x.SourceId == SourceId &&
                                                   x.ProgramId == null &&
                                                     x.ModeId == TransactionMode.Credit &&
                                                     x.StatusId == Transaction.Success)
                                              .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                _OAccountBalance.Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.AccountId == UserAccountId &&
                                                          x.SourceId == SourceId &&
                                                   x.ProgramId == null &&
                                                         x.ModeId == TransactionMode.Debit &&
                                                         x.StatusId == Transaction.Success)
                                                  .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);


                _OAccountBalance.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.AccountId == UserAccountId &&
                                                         x.SourceId == SourceId &&
                                                   x.ProgramId == null &&
                                                        (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit) &&
                                                        x.StatusId == Transaction.Success).Count();
                _OAccountBalance.Balance = HCoreHelper.RoundNumber((_OAccountBalance.Credit - _OAccountBalance.Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                                                                                           && x.SourceId == SourceId)
                                                              .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                var UserBalance = _HCoreContext.HCUAccountBalance
                    .Where(x => x.AccountId == UserAccountId
                    && x.SourceId == SourceId
                    && x.ParentId == 1
                    ).FirstOrDefault();
                if (UserBalance != null)
                {
                    UserBalance.Credit = Math.Round(_OAccountBalance.Credit, 2);
                    UserBalance.Debit = Math.Round(_OAccountBalance.Debit, 2);
                    UserBalance.Balance = Math.Round(_OAccountBalance.Balance, 2);
                    UserBalance.LastTransactionDate = LastTransactionDate;
                    UserBalance.Transactions = _OAccountBalance.Transactions;
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
                else
                {
                    _HCUAccountBalance = new HCUAccountBalance();
                    _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountBalance.AccountId = UserAccountId;
                    _HCUAccountBalance.ParentId = 1;
                    _HCUAccountBalance.SourceId = SourceId;
                    _HCUAccountBalance.Credit = _OAccountBalance.Credit;
                    _HCUAccountBalance.Debit = _OAccountBalance.Debit;
                    _HCUAccountBalance.Balance = _OAccountBalance.Balance;
                    _HCUAccountBalance.Transactions = _OAccountBalance.Transactions;
                    _HCUAccountBalance.LastTransactionDate = LastTransactionDate;
                    _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
                return _OAccountBalance;
            }

        }
        /// <summary>
        /// Description: Updates the account balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <param name="ParentId">The parent identifier.</param>
        /// <returns>OAccountBalance.</returns>
        internal OAccountBalance UpdateAccountBalance(long UserAccountId, int SourceId, long ParentId)
        {
            UpdateAccountBalance(UserAccountId, SourceId);
            using (_HCoreContext = new HCoreContext())
            {
                _OAccountBalance = new OAccountBalance();
                DateTime CurrentDate = HCoreHelper.GetGMTDate();
                _OAccountBalance.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                              .Where(x => x.AccountId == UserAccountId &&
                                                     x.SourceId == SourceId &&
                                                     x.ParentId == ParentId &&
                                                     x.ModeId == TransactionMode.Credit &&
                                                     x.StatusId == Transaction.Success)
                                              .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                _OAccountBalance.Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.AccountId == UserAccountId &&
                                                             x.SourceId == SourceId &&
                                                     x.ParentId == ParentId &&
                                                         x.ModeId == TransactionMode.Debit &&
                                                         x.StatusId == Transaction.Success)
                                                  .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                _OAccountBalance.Transactions = _HCoreContext.HCUAccountTransaction
                                                .Where(x => x.AccountId == UserAccountId &&
                                                         x.SourceId == SourceId &&
                                                     x.ParentId == ParentId &&
                                                        (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit) &&
                                                        x.StatusId == Transaction.Success).Count();
                _OAccountBalance.Balance = HCoreHelper.RoundNumber((_OAccountBalance.Credit - _OAccountBalance.Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                DateTime? LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                                                                                            && x.ParentId == ParentId
                                                                                           && x.SourceId == SourceId)
                                                              .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                var UserBalance = _HCoreContext.HCUAccountBalance
                    .Where(x => x.AccountId == UserAccountId
                    && x.SourceId == SourceId
                    && x.ParentId == ParentId
                    ).FirstOrDefault();
                if (UserBalance != null)
                {
                    UserBalance.Credit = _OAccountBalance.Credit;
                    UserBalance.Debit = _OAccountBalance.Debit;
                    UserBalance.Balance = _OAccountBalance.Balance;
                    UserBalance.LastTransactionDate = LastTransactionDate;
                    UserBalance.Transactions = _OAccountBalance.Transactions;
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
                else
                {
                    _HCUAccountBalance = new HCUAccountBalance();
                    _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                    _HCUAccountBalance.AccountId = UserAccountId;
                    _HCUAccountBalance.ParentId = ParentId;
                    _HCUAccountBalance.SourceId = SourceId;
                    _HCUAccountBalance.Credit = _OAccountBalance.Credit;
                    _HCUAccountBalance.Debit = _OAccountBalance.Debit;
                    _HCUAccountBalance.Balance = _OAccountBalance.Balance;
                    _HCUAccountBalance.Transactions = _OAccountBalance.Transactions;
                    _HCUAccountBalance.LastTransactionDate = LastTransactionDate;
                    _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                    _HCoreContext.HCUAccountBalance.Add(_HCUAccountBalance);
                    _HCoreContext.SaveChanges();
                    _HCoreContext.Dispose();
                }
                return _OAccountBalance;
            }
        }
        /// <summary>
        /// Description: Gets the application user balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="ForceBalance">if set to <c>true</c> [force balance].</param>
        /// <returns>System.Double.</returns>
        internal double GetAppUserBalance(long UserAccountId, bool ForceBalance = false)
        {
            #region Manage Exception
            try
            {
                double Balance = 0;
                DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                            .Where(x => x.AccountId == UserAccountId &&
                                                   (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.Payments) &&
                                                   x.ModeId == TransactionMode.Credit &&
                                                   x.ProgramId == null &&
                                                   (x.StatusId == Transaction.Success))
                                            .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == UserAccountId &&
                                                              (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.Payments) &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             x.ProgramId == null &&
                                                             (x.StatusId == Transaction.Success))
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble);
                    LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                                                                                              && x.SourceId == TransactionSource.TUC)
                                                                 .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                    if (LastTransactionDate != null)
                    {
                        if (ForceBalance)
                        {
                            return Balance;
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return Balance;
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application user balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <returns>OAccountBalance.</returns>
        internal OAccountBalance GetAppUserBalance(long UserAccountId)
        {
            #region Manage Exception
            try
            {
                _OAccountBalance = new OAccountBalance();
                _OAccountBalance.Credit = 0;
                _OAccountBalance.Debit = 0;
                _OAccountBalance.Balance = 0;
                DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    _OAccountBalance.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                            .Where(x => x.AccountId == UserAccountId &&
                                                   x.SourceId == TransactionSource.TUC &&
                                                   x.ProgramId == null &&
                                                   x.ModeId == TransactionMode.Credit &&
                                                   x.StatusId == Transaction.Success)
                                            .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    _OAccountBalance.Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == UserAccountId &&
                                                              x.SourceId == TransactionSource.TUC &&
                                                   x.ProgramId == null &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             x.StatusId == Transaction.Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                    //long Transactions = _HCoreContext.HCUAccountTransaction
                    //                                .Where(x => x.AccountId == UserAccountId &&
                    //                                         x.SourceId == TransactionSource.TUC &&
                    //                                        (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit) &&
                    //                                        x.StatusId == Transaction.Success).Count();
                    _OAccountBalance.Balance = HCoreHelper.RoundNumber((_OAccountBalance.Credit - _OAccountBalance.Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                    LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                                                                  && x.SourceId == TransactionSource.TUC)
                                                                 .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                    if (LastTransactionDate != null)
                    {
                        int DaysDifference = (LastTransactionDate - HCoreHelper.GetGMTDate()).Value.Days;
                        if (DaysDifference < 91)
                        {
                            if (DaysDifference > 0)
                            {
                                _OAccountBalance.BalanceValidity = DaysDifference;
                            }
                            _HCoreContext.Dispose();
                            return _OAccountBalance;
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return _OAccountBalance;
                        }
                    }
                    else
                    {
                        return _OAccountBalance;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return _OAccountBalance;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application user gift points balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <returns>System.Double.</returns>
        internal double GetAppUserGiftPointsBalance(long UserAccountId, long MerchantId)
        {
            #region Manage Exception
            try
            {
                double Balance = 0;
                DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    var UserAccountBalance = _HCoreContext.HCUAccountBalance
                        .Where(x => x.AccountId == UserAccountId
                        && x.ParentId == MerchantId
                        && x.SourceId == TransactionSource.GiftPoints)
                        .Select(x => new
                        {
                            Balance = x.Balance,
                            LastTransactionDate = x.LastTransactionDate,
                        }).FirstOrDefault();
                    if (UserAccountBalance != null)
                    {
                        if (UserAccountBalance.LastTransactionDate != null)
                        {
                            if (UserAccountBalance.LastTransactionDate.Value.Year == 0001)
                            {
                                OAccountBalance UserBalanceDetails = UpdateAccountBalance(UserAccountId, TransactionSource.GiftPoints, MerchantId);
                                Balance = UserBalanceDetails.Balance;
                                LastTransactionDate = UserBalanceDetails.LastTransactionDate;
                            }
                            else
                            {
                                Balance = UserAccountBalance.Balance;
                                LastTransactionDate = UserAccountBalance.LastTransactionDate;
                            }
                        }
                        else
                        {
                            Balance = UserAccountBalance.Balance;
                            LastTransactionDate = UserAccountBalance.LastTransactionDate;
                        }
                    }
                    else
                    {
                        OAccountBalance UserBalanceDetails = UpdateAccountBalance(UserAccountId, TransactionSource.GiftPoints, MerchantId);
                        Balance = UserBalanceDetails.Balance;
                        LastTransactionDate = UserBalanceDetails.LastTransactionDate;
                    }
                    int DaysDifference = (LastTransactionDate - HCoreHelper.GetGMTDate()).Value.Days;
                    if (DaysDifference < 91)
                    {
                        _HCoreContext.Dispose();
                        return Balance;
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return 0;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        public class OTr
        {
            public long SourceId { get; set; }
            public double Balance { get; set; }
            public double Credit { get; set; }
            public double Debit { get; set; }
            public DateTime? LastTransactionDate { get; set; }
        }
        /// <summary>
        /// Description: Gets the application user balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>System.Double.</returns>
        internal double GetAppUserBalance(long UserAccountId, long MerchantId, long SourceId)
        {

            #region Manage Exception
            try
            {
                double Balance = 0;
                DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                            .Where(x => x.AccountId == UserAccountId &&
                                                    x.ParentId == MerchantId &&
                                                   x.ProgramId == null &&
                                                   x.SourceId == SourceId &&
                                                   x.ModeId == TransactionMode.Credit &&
                                                   (x.StatusId == Transaction.Success || x.StatusId == Transaction.Initialized)

                                                   )
                                            .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == UserAccountId &&
                                                     x.ParentId == MerchantId &&
                                                   x.ProgramId == null &&
                                                               x.SourceId == SourceId &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                              (x.StatusId == Transaction.Success || x.StatusId == Transaction.Initialized))
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                    Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                    LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                    && x.ParentId == MerchantId
                                                                                              && x.SourceId == SourceId)
                                                                 .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (LastTransactionDate != null)
                    {
                        return Balance;
                    }
                    else
                    {
                        return 0;
                    }
                }

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the application user thank u cash plus balance.
        /// </summary>
        /// <param name="UserAccountId">The user account identifier.</param>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <returns>System.Double.</returns>
        internal double GetAppUserThankUCashPlusBalance(long UserAccountId, long MerchantId)
        {

            #region Manage Exception
            try
            {
                double Balance = 0;
                DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                            .Where(x => x.AccountId == UserAccountId &&
                                                    x.ParentId == MerchantId &&
                                                   x.SourceId == TransactionSource.ThankUCashPlus &&
                                                   x.ProgramId == null &&
                                                   x.ModeId == TransactionMode.Credit &&
                                                   x.StatusId == Transaction.Success)
                                            .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == UserAccountId &&
                                                     x.ParentId == MerchantId &&
                                                              x.SourceId == TransactionSource.ThankUCashPlus &&
                                                   x.ProgramId == null &&
                                                             x.ModeId == TransactionMode.Debit &&
                                                             x.StatusId == Transaction.Success)
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    //long Transactions = _HCoreContext.HCUAccountTransaction
                    //                                .Where(x => x.AccountId == UserAccountId &&
                    //                                         x.SourceId == TransactionSource.ThankUCashPlus &&
                    //                                        (x.ModeId == TransactionMode.Debit || x.ModeId == TransactionMode.Credit) &&
                    //                                        x.StatusId == Transaction.Success).Count();
                    Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                    LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserAccountId
                    && x.ParentId == MerchantId && x.SourceId == TransactionSource.ThankUCashPlus)
                                                                 .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (LastTransactionDate != null)
                    {
                        return Balance;
                    }
                    else
                    {
                        return 0;
                    }
                }

                //double Balance = 0;
                //DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                //using (_HCoreContext = new HCoreContext())
                //{
                //    var UserAccountBalance = _HCoreContext.HCUAccountBalance
                //        .Where(x => x.AccountId == UserAccountId
                //        && x.ParentId == MerchantId
                //        && x.SourceId == TransactionSource.ThankUCashPlus)
                //        .Select(x => new
                //        {
                //            Balance = x.Balance,
                //            LastTransactionDate = x.LastTransactionDate,
                //        }).FirstOrDefault();
                //    if (UserAccountBalance != null)
                //    {
                //        if (UserAccountBalance.LastTransactionDate != null)
                //        {
                //            if (UserAccountBalance.LastTransactionDate.Value.Year == 0001)
                //            {
                //                OAccountBalance UserBalanceDetails = UpdateAccountBalance(UserAccountId, TransactionSource.ThankUCashPlus, MerchantId);
                //                Balance = UserBalanceDetails.Balance;
                //                LastTransactionDate = UserBalanceDetails.LastTransactionDate;
                //            }
                //            else
                //            {
                //                Balance = UserAccountBalance.Balance;
                //                LastTransactionDate = UserAccountBalance.LastTransactionDate;
                //            }
                //        }
                //        else
                //        {
                //            Balance = UserAccountBalance.Balance;
                //            LastTransactionDate = UserAccountBalance.LastTransactionDate;
                //        }
                //    }
                //    else
                //    {
                //        OAccountBalance UserBalanceDetails = UpdateAccountBalance(UserAccountId, TransactionSource.ThankUCashPlus, MerchantId);
                //        Balance = UserBalanceDetails.Balance;
                //        LastTransactionDate = UserBalanceDetails.LastTransactionDate;
                //    }
                //    int DaysDifference = (LastTransactionDate - HCoreHelper.GetGMTDate()).Value.Days;
                //    if (DaysDifference < 91)
                //    {
                //        _HCoreContext.Dispose();
                //        return Balance;
                //    }
                //    else
                //    {
                //        _HCoreContext.Dispose();
                //        return 0;
                //    }
                //}
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Gets balance of loyalty program
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <param name="ProgramAccountId"></param>
        /// <returns></returns>
        ///

        internal OLoyaltyProgram GetLoyaltyProgramBalance(long CustomerId, long MerchantId, long ProgramAccountId, int? TransactionSource = TransactionSource.TUCBlack)
        {
            #region Manage Exception
            try
            {
                double Balance = 0;
                //DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    OLoyaltyProgram? LoyaltyProgram = _HCoreContext.TUCLProgramGroup
                   .Where(x => x.AccountId == MerchantId
                   && x.Program.AccountId == ProgramAccountId
                   && x.Program.Account.AccountTypeId == UserAccountType.Acquirer
                   && x.StatusId == HelperStatus.Default.Active)
                   .Select(x => new OLoyaltyProgram
                   {
                       ProgramId = x.ProgramId,
                       Name = x.Program.Name,
                   }).FirstOrDefault();
                    if (LoyaltyProgram != null)
                    {
                        double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                           .Where(x => x.AccountId == CustomerId &&
                                                   x.ProgramId == LoyaltyProgram.ProgramId &&
                                                   x.SourceId == TransactionSource &&
                                                   x.ModeId == TransactionMode.Credit &&
                                                   (x.StatusId == Transaction.Success || x.StatusId == Transaction.Initialized))
                                           .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                        double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.AccountId == CustomerId &&
                                                        x.ProgramId == LoyaltyProgram.ProgramId &&
                                                        x.SourceId == TransactionSource &&
                                                        x.ModeId == TransactionMode.Debit &&
                                                        (x.StatusId == Transaction.Success || x.StatusId == Transaction.Initialized))
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                        Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                        _HCoreContext.Dispose();
                        return new OLoyaltyProgram
                        {
                            ProgramId = LoyaltyProgram.ProgramId,
                            Name = LoyaltyProgram.Name,
                            Credit = Credit,
                            Debit = Debit,
                            Balance = Balance
                        };
                    }
                    else
                    {
                        return null;
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return null;
                #endregion
            }
            #endregion
        }

        internal OLoyaltyProgram GetLoyaltyProgramBalance(long CustomerId, long ProgramId, int? TransactionSource = TransactionSource.TUCBlack)
        {
            #region Manage Exception
            try
            {
                double Balance = 0;
                //DateTime? LastTransactionDate = HCoreHelper.GetGMTDate();
                using (_HCoreContext = new HCoreContext())
                {
                    var LoyaltyProgram = _HCoreContext.TUCLProgram.Where(x => x.Id == ProgramId)
                        .Select(x => new
                        {
                            ProgramId = x.Id,
                            Name = x.Name,
                            IconUrl = x.Account.IconStorage.Path,
                        }).FirstOrDefault();

                    double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                       .Where(x => x.AccountId == CustomerId &&
                                               x.ProgramId == LoyaltyProgram.ProgramId &&
                                               x.SourceId == TransactionSource &&
                                               x.ModeId == TransactionMode.Credit &&
                                               (x.StatusId == Transaction.Success || x.StatusId == Transaction.Initialized))
                                       .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == CustomerId &&
                                                    x.ProgramId == LoyaltyProgram.ProgramId &&
                                                    x.SourceId == TransactionSource &&
                                                    x.ModeId == TransactionMode.Debit &&
                                                    (x.StatusId == Transaction.Success || x.StatusId == Transaction.Initialized))
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                    Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                    _HCoreContext.Dispose();
                    return new OLoyaltyProgram
                    {
                        ProgramId = LoyaltyProgram.ProgramId,
                        Name = LoyaltyProgram.Name,
                        Credit = Credit,
                        Debit = Debit,
                        Balance = Balance,
                        IconUrl = LoyaltyProgram.IconUrl,
                    };

                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return null;
                #endregion
            }
            #endregion
        }


        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>System.Double.</returns>
        internal double GetAccountBalance(long AccountId, long SourceId)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                             .Where(x => x.AccountId == AccountId &&
                                                    (x.SourceId == SourceId || x.SourceId == TransactionSource.Payments)
                                                    && x.ProgramId == null &&
                                                    x.ModeId == TransactionMode.Credit &&
                                                    (x.StatusId == Transaction.Success))
                                             .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == AccountId &&
                                                              (x.SourceId == SourceId || x.SourceId == TransactionSource.Payments)
                                                              && x.ModeId == TransactionMode.Debit &&
                                                             (x.StatusId == Transaction.Success))
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                    return HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant balance.
        /// </summary>
        /// <param name="MerchantId">The merchant identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>System.Double.</returns>
        internal double GetMerchantBalance(long MerchantId, long SourceId)
        {
            #region Manage Exception
            try
            {
                using (_HCoreContext = new HCoreContext())
                {

                    double Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                             .Where(x => x.AccountId == MerchantId
                                                   && x.SourceId == SourceId
                                                  && x.ProgramId == null
                                                   && x.ModeId == TransactionMode.Credit
                                                   && (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                             .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                    double Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                     .Where(x => x.AccountId == MerchantId
                                                            && x.SourceId == SourceId
                                                            && x.ModeId == TransactionMode.Debit
                                                            && (x.StatusId == Transaction.Success || x.StatusId == Transaction.Pending))
                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                    return HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return 0;
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the account balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountBalance(OBalance.Request _Request)
        {
            // Customer App Balance
            #region Declare
            _BalanceResponse = new OBalance.Response();
            _BalanceResponse.SystemDate = HCoreHelper.GetGMTDate();
            #endregion
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Source))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0002", "Balance source missing");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.UserAccountKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0003", "User reference missing");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        var UserDetails = _HCoreContext.HCUAccount
                                                           .Where(x => x.Guid == _Request.UserAccountKey)
                                                           .Select(x => new
                                                           {
                                                               UserAccountId = x.Id,
                                                               UserAccountType = x.AccountTypeId,
                                                           }).FirstOrDefault();
                        if (UserDetails != null)
                        {
                            if (UserDetails.UserAccountType == UserAccountType.Appuser)
                            {
                                double PendingReward = HCoreHelper.RoundNumber(_HCoreContext.TUCLoyaltyPending
                                           .Where(x => x.ToAccountId == UserDetails.UserAccountId &&
                                                  x.SourceId == TransactionSource.TUC &&
                                                  x.ModeId == TransactionMode.Credit &&
                                                  x.StatusId == Transaction.Pending)
                                           .Sum(x => (double?)x.Amount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                // TUC Balance
                                long BalanceValidity = 0;
                                double InvoiceAmount = 0;
                                double Credit = 0;
                                double Debit = 0;
                                double Balance = 0;
                                DateTime? TucLastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserDetails.UserAccountId
                                                                                                && x.SourceId == TransactionSource.TUC)
                                                                    .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();

                                DateTime? TucPlusLastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserDetails.UserAccountId
                                                                                                  && x.SourceId == TransactionSource.ThankUCashPlus)
                                                                     .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                                if (TucLastTransactionDate != null)
                                {
                                    InvoiceAmount = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                       .Where(x => x.AccountId == UserDetails.UserAccountId
                                       && x.SourceId == TransactionSource.TUC
                                              && x.ProgramId == null
                                              && x.ModeId == TransactionMode.Credit
                                              && x.StatusId == Transaction.Success)
                                       .Sum(x => (double?)x.PurchaseAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                    Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                           .Where(x => x.AccountId == UserDetails.UserAccountId &&
                                                  (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.Payments) &&
                                                   x.ProgramId == null &&
                                                  x.ModeId == TransactionMode.Credit &&
                                                  x.StatusId == Transaction.Success)
                                           .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                    Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                     .Where(x => x.AccountId == UserDetails.UserAccountId
                                       && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.Payments)
                                              && x.ProgramId == null
                                              && x.ModeId == TransactionMode.Debit
                                              && x.StatusId == Transaction.Success)
                                                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                    Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble);
                                }
                                double TucPlusCredit = 0;
                                double TucPlusDebit = 0;
                                long TucPlusBalanceValidity = 0;
                                double TucPlusBalance = 0;
                                if (TucLastTransactionDate != null)
                                {
                                    // TUC+ Balance
                                    TucPlusCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                       .Where(x => x.AccountId == UserDetails.UserAccountId &&
                                              x.SourceId == TransactionSource.ThankUCashPlus &&
                                              x.ModeId == TransactionMode.Credit &&
                                              x.StatusId == Transaction.Success)
                                       .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                    TucPlusDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                                    .Where(x => x.AccountId == UserDetails.UserAccountId &&
                                                                             x.SourceId == TransactionSource.ThankUCashPlus &&
                                                   x.ProgramId == null &&
                                                                            x.ModeId == TransactionMode.Debit &&
                                                                            x.StatusId == Transaction.Success)
                                                                     .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                    TucPlusBalance = HCoreHelper.RoundNumber((TucPlusCredit - TucPlusDebit), _AppConfig.SystemEntryRoundDouble); ;
                                }
                                int TucDaysDifference = (HCoreHelper.GetGMTDate() - TucLastTransactionDate).Value.Days;
                                if (TucDaysDifference > -1 && TucDaysDifference < (_AppConfig.AppUserBalanceValidityDays + 1))
                                {
                                    int Validity = _AppConfig.AppUserBalanceValidityDays - TucDaysDifference;
                                    BalanceValidity = Validity;
                                }
                                else
                                {
                                    Debit = Credit;
                                    Balance = 0;
                                    BalanceValidity = 0;
                                }

                                #region GiftCard Balance
                                DateTime? TDate = HCoreHelper.GetGMTDate().AddDays(-1);
                                double? ProductCodeAmount = _HCoreContext.CAProductCode.Where(x => x.StatusId == HelperStatus.ProdutCode.Unused && x.AccountId == UserDetails.UserAccountId && x.EndDate > TDate).Sum(x => x.AvailableAmount);
                                if (ProductCodeAmount != null)
                                {
                                    Balance += (double)ProductCodeAmount;
                                }
                                #endregion

                                int TucPlusDaysDifference = (HCoreHelper.GetGMTDate() - TucPlusLastTransactionDate).Value.Days;
                                if (TucPlusDaysDifference > -1 && TucPlusDaysDifference < (_AppConfig.AppUserBalanceValidityDays + 1))
                                {
                                    int Validity = _AppConfig.AppUserBalanceValidityDays - TucPlusDaysDifference;
                                    TucPlusBalanceValidity = Validity;
                                    _UserMerchantBalances = new List<OBalance.UserMerchantBalance>();
                                    var UserTUCMerchants = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserDetails.UserAccountId && x.SourceId == TransactionSource.ThankUCashPlus).Select(x => x.ParentId).Distinct().ToList();
                                    foreach (var UserTUCMerchant in UserTUCMerchants)
                                    {
                                        double TUCMCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                          .Where(x => x.AccountId == UserDetails.UserAccountId &&
                                                 x.SourceId == TransactionSource.ThankUCashPlus &&
                                                 x.ModeId == TransactionMode.Credit &&
                                                 x.ParentId == UserTUCMerchant &&
                                                 x.StatusId == Transaction.Success)
                                          .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                        double TUCMDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                      .Where(x => x.AccountId == UserDetails.UserAccountId &&
                                             x.SourceId == TransactionSource.ThankUCashPlus &&
                                             x.ModeId == TransactionMode.Debit &&
                                             x.ParentId == UserTUCMerchant &&
                                             x.StatusId == Transaction.Success)
                                      .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                        DateTime? TUCMerchantLastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserDetails.UserAccountId && x.ParentId == UserTUCMerchant).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                                        if (TUCMerchantLastTransactionDate != null && TUCMerchantLastTransactionDate.Value.Year > 1)
                                        {
                                            int TucPlusMDaysDifference = (HCoreHelper.GetGMTDate() - TucPlusLastTransactionDate).Value.Days;
                                            if (TucPlusMDaysDifference > -1 && TucPlusMDaysDifference < 91)
                                            {

                                            }
                                            else
                                            {
                                                TUCMDebit = TUCMCredit;
                                            }
                                        }
                                        double TUCMBalance = TUCMCredit - TUCMDebit;
                                        if (TUCMBalance > 0)
                                        {
                                            var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == UserTUCMerchant)
                                           .Select(x => new
                                           {
                                               ReferenceId = x.Id,
                                               DisplayName = x.DisplayName,
                                               IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                                           }).FirstOrDefault();
                                            _UserMerchantBalances.Add(new OBalance.UserMerchantBalance
                                            {
                                                ReferenceId = MerchantDetails.ReferenceId,
                                                DisplayName = MerchantDetails.DisplayName,
                                                IconUrl = MerchantDetails.IconUrl,
                                                Credit = Convert.ToInt64(Math.Round(TUCMCredit, 2) * 100),
                                                Debit = Convert.ToInt64(Math.Round(TUCMDebit, 2) * 100),
                                                Balance = Convert.ToInt64(Math.Round(TUCMBalance, 2) * 100),
                                            });
                                        }
                                    }
                                    _BalanceResponse.UserMerchantBalance = _UserMerchantBalances;
                                }
                                else
                                {
                                    TucPlusDebit = TucPlusCredit;
                                    TucPlusBalance = 0;
                                    TucPlusBalanceValidity = 0;
                                }
                                _BalanceResponse.Credit = Convert.ToInt64(Math.Round(Credit, 2) * 100);
                                _BalanceResponse.Debit = Convert.ToInt64(Math.Round(Debit, 2) * 100);
                                _BalanceResponse.Balance = Convert.ToInt64(Math.Round(Balance, 2) * 100);
                                _BalanceResponse.BalanceValidity = BalanceValidity;
                                _BalanceResponse.InvoiceAmount = InvoiceAmount;
                                if (InvoiceAmount < 50000)
                                {
                                    _BalanceResponse.AccountClass = "purple";
                                    _BalanceResponse.AccountClassLimit = 50000 - InvoiceAmount;
                                }
                                else if (InvoiceAmount > 49999 && InvoiceAmount < 200000)
                                {
                                    _BalanceResponse.AccountClass = "bronze";
                                    _BalanceResponse.AccountClassLimit = 200000 - InvoiceAmount;
                                }
                                else if (InvoiceAmount > 199999 && InvoiceAmount < 750000)
                                {
                                    _BalanceResponse.AccountClass = "silver";
                                    _BalanceResponse.AccountClassLimit = 750000 - InvoiceAmount;
                                }
                                else if (InvoiceAmount > 749999 && InvoiceAmount < 3000000)
                                {
                                    _BalanceResponse.AccountClass = "gold";
                                    _BalanceResponse.AccountClassLimit = 3000000 - InvoiceAmount;
                                }
                                else if (InvoiceAmount > 2999999 && InvoiceAmount < 5000000)
                                {
                                    _BalanceResponse.AccountClass = "diamond";
                                    _BalanceResponse.AccountClassLimit = 5000000 - InvoiceAmount;
                                }
                                else if (InvoiceAmount > 4999999 && InvoiceAmount < 15000000)
                                {
                                    _BalanceResponse.AccountClass = "platinum";
                                    _BalanceResponse.AccountClassLimit = 15000000 - InvoiceAmount;
                                }
                                else if (InvoiceAmount > 14999999)
                                {
                                    _BalanceResponse.AccountClass = "platinumplus";
                                    _BalanceResponse.AccountClassLimit = 14999999 - InvoiceAmount;
                                }
                                else
                                {
                                    _BalanceResponse.AccountClass = "purple";
                                    _BalanceResponse.AccountClassLimit = 50000 - InvoiceAmount;
                                }
                                _BalanceResponse.InvoiceAmount = InvoiceAmount;
                                _BalanceResponse.TucPlusCredit = Convert.ToInt64(Math.Round(TucPlusCredit, 2) * 100);
                                _BalanceResponse.TucPlusDebit = Convert.ToInt64(Math.Round(TucPlusDebit, 2) * 100);
                                _BalanceResponse.TucPlusBalance = Convert.ToInt64(Math.Round(TucPlusBalance, 2) * 100);
                                _BalanceResponse.TucPlusBalanceValidity = TucPlusBalanceValidity;
                                if (_BalanceResponse.TucPlusBalance < 0)
                                {
                                    _BalanceResponse.TucPlusBalance = 0;
                                }
                                if (_BalanceResponse.TucPlusDebit < 0)
                                {
                                    _BalanceResponse.TucPlusDebit = 0;
                                }
                                if (_BalanceResponse.TucPlusCredit < 0)
                                {
                                    _BalanceResponse.TucPlusCredit = 0;
                                }
                                if (_BalanceResponse.TucPlusCredit < 0)
                                {
                                    _BalanceResponse.TucPlusCredit = 0;
                                }
                                if (_BalanceResponse.Balance < 0)
                                {
                                    _BalanceResponse.Balance = 0;
                                }
                                _BalanceResponse.PendingReward = PendingReward;
                                //_BalanceResponse.UserMerchantBalance = UserBalanceHistory;
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                #endregion

                            }
                            else
                            {
                                double TotalCredit = _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == UserDetails.UserAccountId &&
                                                           x.Source.SystemName == _Request.Source &&
                                                           x.ModeId == TransactionMode.Credit &&
                                                           x.StatusId == HelperStatus.Transaction.Success)
                                                    .Sum(x => (double?)x.TotalAmount) ?? 0;

                                double TotalDebit = _HCoreContext.HCUAccountTransaction
                                                                 .Where(x => x.AccountId == UserDetails.UserAccountId &&
                                                                         x.Source.SystemName == _Request.Source &&
                                                                         x.ModeId == TransactionMode.Debit &&
                                                                         x.StatusId == HelperStatus.Transaction.Success)
                                                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                                double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                                _BalanceResponse.Credit = Convert.ToInt64(Math.Round(TotalCredit, 2) * 100);
                                _BalanceResponse.Debit = Convert.ToInt64(Math.Round(TotalDebit, 2) * 100);
                                _BalanceResponse.Balance = Convert.ToInt64(Balance * 100);
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HCUAD013", "Invalid account. Please contact support");
                            #endregion
                        }
                    }
                }
                //else
                //{
                //    using (_HCoreContext = new HCoreContext())
                //    {
                //        var UserDetails = _HCoreContext.HCUAccount
                //                                           .Where(x => x.Guid == _Request.UserAccountKey)
                //                                           .Select(x => new
                //                                           {
                //                                               UserAccountId = x.Id,
                //                                               UserAccountType = x.AccountTypeId,
                //                                           }).FirstOrDefault();
                //        if (UserDetails != null)
                //        {
                //            if (UserDetails.UserAccountType == UserAccountType.Appuser)
                //            {
                //                double InvoiceAmount = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                //                                              .Where(x => x.AccountId == UserDetails.UserAccountId
                //                                                     && (x.SourceId == TransactionSource.TUC || x.SourceId == TransactionSource.ThankUCashPlus)
                //                                                     && x.ModeId == TransactionMode.Credit
                //                                                     && x.StatusId == Transaction.Success)
                //                                               .Sum(x => (double?)x.PurchaseAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                //                var UserBalanceHistory = _HCoreContext.HCUAccountBalance.Where(x => x.AccountId == UserDetails.UserAccountId)
                //                                       .Select(x => new
                //                                       {
                //                                           SourceId = x.SourceId,
                //                                           Credit = x.Credit,
                //                                           Debit = x.Debit,
                //                                           Balance = x.Balance,
                //                                           LastTransactionDate = x.LastTransactionDate,
                //                                       }).ToList();
                //                if (UserBalanceHistory.Count > 0)
                //                {
                //                    double Credit = 0;
                //                    double Debit = 0;
                //                    double Balance = 0;
                //                    long BalanceValidity = 0;
                //                    double TucPlusCredit = 0;
                //                    double TucPlusDebit = 0;
                //                    double TucPlusBalance = 0;
                //                    long TucPlusBalanceValidity = 0;
                //                    var TucDetails = UserBalanceHistory.Where(x => x.SourceId == TransactionSource.TUC).FirstOrDefault();
                //                    var TucPlusDetails = UserBalanceHistory.Where(x => x.SourceId == TransactionSource.ThankUCashPlus).FirstOrDefault();
                //                    if (TucDetails != null)
                //                    {
                //                        DateTime? TucLastTransactionDate = TucDetails.LastTransactionDate;
                //                        int TucDaysDifference = (HCoreHelper.GetGMTDate() - TucLastTransactionDate).Value.Days;
                //                        if (TucDaysDifference > -1 && TucDaysDifference < 91)
                //                        {
                //                            int Validity = 90 - TucDaysDifference;
                //                            Credit = TucDetails.Credit;
                //                            Debit = TucDetails.Debit;
                //                            Balance = TucDetails.Balance;
                //                            BalanceValidity = Validity;
                //                        }
                //                        else
                //                        {
                //                            Credit = TucDetails.Credit;
                //                            Debit = TucDetails.Credit;
                //                            Balance = 0;
                //                            BalanceValidity = 0;
                //                        }
                //                    }
                //                    if (TucPlusDetails != null)
                //                    {
                //                        DateTime? TucPlusLastTransactionDate = TucPlusDetails.LastTransactionDate;
                //                        int TucPlusDaysDifference = (HCoreHelper.GetGMTDate() - TucPlusLastTransactionDate).Value.Days;
                //                        if (TucPlusDaysDifference > -1 && TucPlusDaysDifference < 91)
                //                        {
                //                            int Validity = 90 - TucPlusDaysDifference;
                //                            TucPlusCredit = TucPlusDetails.Credit;
                //                            TucPlusDebit = TucPlusDetails.Debit;
                //                            TucPlusBalance = TucPlusDetails.Balance;
                //                            TucPlusBalanceValidity = Validity;
                //                            _UserMerchantBalances = new List<OBalance.UserMerchantBalance>();
                //                            var UserTUCMerchants = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserDetails.UserAccountId && x.SourceId == TransactionSource.ThankUCashPlus).Select(x => x.ParentId).Distinct().ToList();
                //                            foreach (var UserTUCMerchant in UserTUCMerchants)
                //                            {
                //                                double TUCMCredit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                //                                  .Where(x => x.AccountId == UserDetails.UserAccountId &&
                //                                         x.SourceId == TransactionSource.ThankUCashPlus &&
                //                                         x.ModeId == TransactionMode.Credit &&
                //                                         x.ParentId == UserTUCMerchant &&
                //                                         x.StatusId == Transaction.Success)
                //                                  .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                //                                double TUCMDebit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                //                              .Where(x => x.AccountId == UserDetails.UserAccountId &&
                //                                     x.SourceId == TransactionSource.ThankUCashPlus &&
                //                                     x.ModeId == TransactionMode.Debit &&
                //                                     x.ParentId == UserTUCMerchant &&
                //                                     x.StatusId == Transaction.Success)
                //                              .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                //                                DateTime? TUCMerchantLastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == UserDetails.UserAccountId && x.ParentId == UserTUCMerchant).OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                //                                if (TUCMerchantLastTransactionDate != null && TUCMerchantLastTransactionDate.Value.Year > 1)
                //                                {
                //                                    int TucPlusMDaysDifference = (HCoreHelper.GetGMTDate() - TucPlusLastTransactionDate).Value.Days;
                //                                    if (TucPlusMDaysDifference > -1 && TucPlusMDaysDifference < 91)
                //                                    {

                //                                    }
                //                                    else
                //                                    {
                //                                        TUCMDebit = TUCMCredit;
                //                                    }
                //                                }
                //                                double TUCMBalance = TUCMCredit - TUCMDebit;
                //                                if (TUCMBalance > 0)
                //                                {
                //                                    var MerchantDetails = _HCoreContext.HCUAccount.Where(x => x.Id == UserTUCMerchant)
                //                                   .Select(x => new
                //                                   {
                //                                       ReferenceId = x.Id,
                //                                       DisplayName = x.DisplayName,
                //                                       IconUrl = _AppConfig.StorageUrl + x.IconStorage.Path,
                //                                   }).FirstOrDefault();
                //                                    _UserMerchantBalances.Add(new OBalance.UserMerchantBalance
                //                                    {
                //                                        ReferenceId = MerchantDetails.ReferenceId,
                //                                        DisplayName = MerchantDetails.DisplayName,
                //                                        IconUrl = MerchantDetails.IconUrl,
                //                                        Credit = Convert.ToInt64(Math.Round(TUCMCredit, 2) * 100),
                //                                        Debit = Convert.ToInt64(Math.Round(TUCMDebit, 2) * 100),
                //                                        Balance = Convert.ToInt64(Math.Round(TUCMBalance, 2) * 100),
                //                                    });
                //                                }
                //                            }
                //                            _BalanceResponse.UserMerchantBalance = _UserMerchantBalances;
                //                        }
                //                        else
                //                        {
                //                            TucPlusCredit = TucPlusDetails.Credit;
                //                            TucPlusDebit = TucPlusDetails.Credit;
                //                            TucPlusBalance = 0;
                //                            TucPlusBalanceValidity = 0;
                //                        }
                //                    }

                //                    _BalanceResponse.Credit = Convert.ToInt64(Math.Round(Credit, 2) * 100);
                //                    _BalanceResponse.Debit = Convert.ToInt64(Math.Round(Debit, 2) * 100);
                //                    _BalanceResponse.Balance = Convert.ToInt64(Math.Round(Balance, 2) * 100);
                //                    _BalanceResponse.BalanceValidity = BalanceValidity;
                //                    _BalanceResponse.InvoiceAmount = InvoiceAmount;
                //                    if (InvoiceAmount < 50000)
                //                    {
                //                        _BalanceResponse.AccountClass = "purple";
                //                        _BalanceResponse.AccountClassLimit = 50000 - InvoiceAmount;
                //                    }
                //                    else if (InvoiceAmount > 49999 && InvoiceAmount < 200000)
                //                    {
                //                        _BalanceResponse.AccountClass = "bronze";
                //                        _BalanceResponse.AccountClassLimit = 200000 - InvoiceAmount;
                //                    }
                //                    else if (InvoiceAmount > 199999 && InvoiceAmount < 750000)
                //                    {
                //                        _BalanceResponse.AccountClass = "silver";
                //                        _BalanceResponse.AccountClassLimit = 750000 - InvoiceAmount;
                //                    }
                //                    else if (InvoiceAmount > 749999 && InvoiceAmount < 3000000)
                //                    {
                //                        _BalanceResponse.AccountClass = "gold";
                //                        _BalanceResponse.AccountClassLimit = 3000000 - InvoiceAmount;
                //                    }
                //                    else if (InvoiceAmount > 2999999 && InvoiceAmount < 5000000)
                //                    {
                //                        _BalanceResponse.AccountClass = "diamond";
                //                        _BalanceResponse.AccountClassLimit = 5000000 - InvoiceAmount;
                //                    }
                //                    else if (InvoiceAmount > 4999999 && InvoiceAmount < 15000000)
                //                    {
                //                        _BalanceResponse.AccountClass = "platinum";
                //                        _BalanceResponse.AccountClassLimit = 15000000 - InvoiceAmount;
                //                    }
                //                    else if (InvoiceAmount > 14999999)
                //                    {
                //                        _BalanceResponse.AccountClass = "platinumplus";
                //                        _BalanceResponse.AccountClassLimit = 14999999 - InvoiceAmount;
                //                    }
                //                    else
                //                    {
                //                        _BalanceResponse.AccountClass = "purple";
                //                        _BalanceResponse.AccountClassLimit = 50000 - InvoiceAmount;
                //                    }
                //                    _BalanceResponse.InvoiceAmount = InvoiceAmount;
                //                    _BalanceResponse.TucPlusCredit = Convert.ToInt64(Math.Round(TucPlusCredit, 2) * 100);
                //                    _BalanceResponse.TucPlusDebit = Convert.ToInt64(Math.Round(TucPlusDebit, 2) * 100);
                //                    _BalanceResponse.TucPlusBalance = Convert.ToInt64(Math.Round(TucPlusBalance, 2) * 100);
                //                    _BalanceResponse.TucPlusBalanceValidity = TucPlusBalanceValidity;
                //                    if (_BalanceResponse.TucPlusBalance < 0)
                //                    {
                //                        _BalanceResponse.TucPlusBalance = 0;
                //                    }
                //                    if (_BalanceResponse.TucPlusDebit < 0)
                //                    {
                //                        _BalanceResponse.TucPlusDebit = 0;
                //                    }
                //                    if (_BalanceResponse.TucPlusCredit < 0)
                //                    {
                //                        _BalanceResponse.TucPlusCredit = 0;
                //                    }
                //                    if (_BalanceResponse.TucPlusCredit < 0)
                //                    {
                //                        _BalanceResponse.TucPlusCredit = 0;
                //                    }
                //                    if (_BalanceResponse.Balance < 0)
                //                    {
                //                        _BalanceResponse.Balance = 0;
                //                    }
                //                    //_BalanceResponse.UserMerchantBalance = UserBalanceHistory;
                //                    #region Send Response
                //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                //                    #endregion
                //                }
                //                else
                //                {
                //                    _BalanceResponse.Credit = 0;
                //                    _BalanceResponse.Debit = 0;
                //                    _BalanceResponse.Balance = 0;
                //                    _BalanceResponse.BalanceValidity = 0;
                //                    _BalanceResponse.TucPlusCredit = 0;
                //                    _BalanceResponse.TucPlusDebit = 0;
                //                    _BalanceResponse.TucPlusBalance = 0;
                //                    _BalanceResponse.TucPlusBalanceValidity = 0;
                //                    _BalanceResponse.InvoiceAmount = 0;
                //                    #region Send Response
                //                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                //                    #endregion
                //                }
                //            }
                //            else
                //            {
                //                double TotalCredit = _HCoreContext.HCUAccountTransaction
                //                                    .Where(x => x.AccountId == UserDetails.UserAccountId &&
                //                                           x.Source.SystemName == _Request.Source &&
                //                                           x.ModeId == TransactionMode.Credit &&
                //                                           x.StatusId == HelperStatus.Transaction.Success)
                //                                    .Sum(x => (double?)x.TotalAmount) ?? 0;

                //                double TotalDebit = _HCoreContext.HCUAccountTransaction
                //                                                 .Where(x => x.AccountId == UserDetails.UserAccountId &&
                //                                                         x.Source.SystemName == _Request.Source &&
                //                                                         x.ModeId == TransactionMode.Debit &&
                //                                                         x.StatusId == HelperStatus.Transaction.Success)
                //                                                  .Sum(x => (double?)x.TotalAmount) ?? 0;
                //                double Balance = Math.Round((TotalCredit - TotalDebit), 2);
                //                _BalanceResponse.Credit = Convert.ToInt64(Math.Round(TotalCredit, 2) * 100);
                //                _BalanceResponse.Debit = Convert.ToInt64(Math.Round(TotalDebit, 2) * 100);
                //                _BalanceResponse.Balance = Convert.ToInt64(Balance * 100);
                //                #region Send Response
                //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _BalanceResponse, "HCUAD013");
                //                #endregion
                //            }
                //        }
                //        else
                //        {
                //            #region Send Response
                //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _BalanceResponse, "HCUAD013", "Invalid account. Please contact support");
                //            #endregion
                //        }
                //    }
                //}
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAccountBalance", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user balance history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        ///
        List<OBalance.BalanceList> _BalanceList;
        internal OResponse GetUserBalanceHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                #endregion 
                #region Operation
                _BalanceList = new List<OBalance.BalanceList>();
                using (_HCoreContext = new HCoreContext())
                {
                    var ProgramListId = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == _Request.UserReference.AccountId
                    && x.ProgramId != null)
                        .Select(x => x.ProgramId).ToList();
                    ProgramListId = ProgramListId.Distinct().ToList();
                    if (ProgramListId.Count > 0)
                    {
                        foreach (var ProgramId in ProgramListId)
                        {
                            var LoyaltyProgram = GetLoyaltyProgramBalance(_Request.UserReference.AccountId, (long)ProgramId, TransactionSource.TUCBlack);
                            if (LoyaltyProgram != null)
                            {
                                _BalanceList.Add(new OBalance.BalanceList
                                {
                                    ReferenceId = LoyaltyProgram.ProgramId,
                                    ReferenceKey = "",

                                    //UserAccountId = x.AccountId,
                                    //UserAccountKey = x.Account.Guid,
                                    //UserAccountDisplayName = x.Account.DisplayName,
                                    //UserAccountMobileNumber = x.Account.MobileNumber,

                                    ParentId = LoyaltyProgram.ProgramId,
                                    ParentKey = LoyaltyProgram.Name,
                                    ParentDisplayName = LoyaltyProgram.Name,
                                    ParentIconUrl = LoyaltyProgram.IconUrl,

                                    SourceId = TransactionSource.TUCBlack,
                                    SourceKey = LoyaltyProgram.Name,
                                    SourceName = LoyaltyProgram.Name,

                                    Credit = LoyaltyProgram.Credit,
                                    Debit = LoyaltyProgram.Debit,
                                    Balance = LoyaltyProgram.Balance,
                                    Transactions = 0,


                                    CreateDate = HCoreHelper.GetGMTDateTime(),

                                    StatusId = HelperStatus.Default.Active,
                                    StatusCode = "active",
                                    StatusName = "active",
                                });
                            }
                        }
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccountBalance
                                                 where x.SourceId != TransactionSource.TUCBlack
                                                 select new OBalance.BalanceList
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     UserAccountId = x.AccountId,
                                                     UserAccountKey = x.Account.Guid,
                                                     UserAccountDisplayName = x.Account.DisplayName,
                                                     UserAccountMobileNumber = x.Account.MobileNumber,

                                                     ParentId = x.ParentId,
                                                     ParentKey = x.Parent.Guid,
                                                     ParentDisplayName = x.Parent.DisplayName,
                                                     ParentIconUrl = x.Parent.IconStorage.Path,

                                                     SourceId = x.SourceId,
                                                     SourceKey = x.Source.SystemName,
                                                     SourceName = x.Source.Name,

                                                     Credit = x.Credit,
                                                     Debit = x.Debit,
                                                     Balance = x.Balance,
                                                     Transactions = x.Transactions,
                                                     //LastTransactionDate = x.LastTransactionDate,


                                                     CreateDate = x.LastTransactionDate,
                                                     ModifyDate = x.LastTransactionDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,
                                                 })
                                      .Where(_Request.SearchCondition)
                              .Count();
                    }
                    #endregion
                    #region Get Data
                    _BalanceList.AddRange((from x in _HCoreContext.HCUAccountBalance
                                           where x.SourceId != TransactionSource.TUCBlack
                                           select new OBalance.BalanceList
                                           {
                                               ReferenceId = x.Id,
                                               ReferenceKey = x.Guid,

                                               UserAccountId = x.AccountId,
                                               UserAccountKey = x.Account.Guid,
                                               UserAccountDisplayName = x.Account.DisplayName,
                                               UserAccountMobileNumber = x.Account.MobileNumber,

                                               ParentId = x.ParentId,
                                               ParentKey = x.Parent.Guid,
                                               ParentDisplayName = x.Parent.DisplayName,
                                               ParentIconUrl = x.Parent.IconStorage.Path,

                                               SourceId = x.SourceId,
                                               SourceKey = x.Source.SystemName,
                                               SourceName = x.Source.Name,

                                               Credit = x.Credit,
                                               Debit = x.Debit,
                                               Balance = x.Balance,
                                               Transactions = x.Transactions,


                                               CreateDate = x.LastTransactionDate,
                                               ModifyDate = x.LastTransactionDate,

                                               StatusId = x.StatusId,
                                               StatusCode = x.Status.SystemName,
                                               StatusName = x.Status.Name,
                                           })
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList());
                    #endregion
                    #region Format List
                    _HCoreContext.Dispose();
                    #endregion
                    foreach (var item in _BalanceList)
                    {
                        if (!string.IsNullOrEmpty(item.ParentIconUrl))
                        {
                            item.ParentIconUrl = _AppConfig.StorageUrl + item.ParentIconUrl;
                        }
                    }
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, _BalanceList, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreHelperLite", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Processes the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OCoreTransaction.Response.</returns>
        internal OCoreTransaction.Response ProcessTransaction(OCoreTransaction.Request _Request)
        {
            #region Declare
            _TransactionResponse = new OCoreTransaction.Response();
            #endregion
            #region Manage Exception
            try
            {
                DateTime TransctionDate = HCoreHelper.GetGMTDateTime();
                if (_Request.TTransactionDate != null && HostEnvironment == HostEnvironmentType.Test && HostEnvironment == HostEnvironmentType.Local)
                {
                    TransctionDate = (DateTime)_Request.TTransactionDate;
                }

                if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                {
                    _Request.ProviderId = _Request.UserReference.AccountId;
                }

                if (_Request.CreatedById != 0)
                {
                    _Request.CreatedById = _Request.CreatedById;
                }
                else
                {
                    if (_Request.UserReference.AccountId != 0)
                    {
                        _Request.CreatedById = _Request.UserReference.AccountId;
                        _Request.CreatedByAccountTypeId = _Request.UserReference.AccountTypeId;
                    }
                }

                using (_HCoreContext = new HCoreContext())
                {
                    string GroupKey = null;
                    string TCode = HCoreHelper.GenerateRandomNumber(4);
                    double ReferenceAmountPercentage = HCoreHelper.GetAmountPercentage(_Request.InvoiceAmount, _Request.ReferenceAmount);
                    OCoreTransaction.TransactionItem? _ParentTransaction = _Request.Transactions.FirstOrDefault();
                    List<OCoreTransaction.TransactionItem> _ChildTransactions = _Request.Transactions.Skip(1).ToList();
                    #region Save Parent Transaction
                    _HCUAccountTransactionParent = new HCUAccountTransaction();
                    _HCUAccountTransactionParent.InoviceNumber = _Request.InvoiceNumber;
                    if (string.IsNullOrEmpty(GroupKey) && !string.IsNullOrEmpty(_Request.GroupKey))
                    {
                        GroupKey = _Request.GroupKey;
                        if (!string.IsNullOrEmpty(_Request.ParentTransactionKey) && _Request.GroupKey == _Request.ParentTransactionKey)
                        {
                            _HCUAccountTransactionParent.Guid = HCoreHelper.GenerateGuid();
                        }
                        else
                        {
                            _HCUAccountTransactionParent.Guid = GroupKey;
                        }
                    }
                    else
                    {
                        _HCUAccountTransactionParent.Guid = HCoreHelper.GenerateGuid();
                    }
                    if (!string.IsNullOrEmpty(_Request.ParentTransactionKey))
                    {
                        GroupKey = _Request.ParentTransactionKey;
                    }
                    if (_Request.ParentId > 0)
                    {
                        _HCUAccountTransactionParent.ParentId = _Request.ParentId;
                    }
                    if (_ParentTransaction?.UserAccountId != 0)
                    {
                        _HCUAccountTransactionParent.AccountId = _ParentTransaction?.UserAccountId;
                    }
                    #region Save Transaction
                    _ParentTransaction.TransactionKey = _HCUAccountTransactionParent.Guid;
                    _HCUAccountTransactionParent.ModeId = _ParentTransaction.ModeId;
                    _HCUAccountTransactionParent.TypeId = _ParentTransaction.TypeId;
                    _HCUAccountTransactionParent.SourceId = _ParentTransaction.SourceId;
                    _HCUAccountTransactionParent.Amount = _ParentTransaction.Amount;
                    _HCUAccountTransactionParent.Charge = _ParentTransaction.Charge;
                    _HCUAccountTransactionParent.ComissionAmount = _ParentTransaction.Comission;
                    _HCUAccountTransactionParent.TotalAmount = _ParentTransaction.TotalAmount;
                    _HCUAccountTransactionParent.Balance = 0;
                    _HCUAccountTransactionParent.PurchaseAmount = _Request.InvoiceAmount;
                    if (_Request.PaymentMethodId > 0)
                    {
                        _HCUAccountTransactionParent.PaymentMethodId = _Request.PaymentMethodId;
                    }
                    if (_Request.ReferenceInvoiceAmount != 0)
                    {
                        _HCUAccountTransactionParent.ReferenceInvoiceAmount = _Request.ReferenceInvoiceAmount;
                    }
                    else
                    {
                        _HCUAccountTransactionParent.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                    }
                    _HCUAccountTransactionParent.ReferenceAmount = (_ParentTransaction.ReferenceAmount == null || _ParentTransaction.ReferenceAmount == 0) ? _Request.ReferenceAmount : _ParentTransaction.ReferenceAmount;
                    if (_Request.BankId != 0)
                    {
                        _HCUAccountTransactionParent.BankId = _Request.BankId;
                    }
                    if (_Request.CustomerId != 0)
                    {
                        _HCUAccountTransactionParent.CustomerId = _Request.CustomerId;
                    }
                    else
                    {
                        if (_ParentTransaction.SourceId == TransactionSource.TUC)
                        {
                            _HCUAccountTransactionParent.CustomerId = _ParentTransaction.UserAccountId;
                        }
                    }
                    if (_Request.SubParentId != 0)
                    {
                        _HCUAccountTransactionParent.SubParentId = _Request.SubParentId;
                    }
                    if (_Request.CashierId != 0)
                    {
                        _HCUAccountTransactionParent.CashierId = _Request.CashierId;
                    }
                    if (_Request.TerminalId != 0)
                    {
                        _HCUAccountTransactionParent.TerminalId = _Request.TerminalId;
                    }
                    if (_Request.BinNumberId != 0)
                    {
                        _HCUAccountTransactionParent.BinNumberId = _Request.BinNumberId;
                    }
                    if (_ParentTransaction.TransactionDate != null)
                    {
                        _HCUAccountTransactionParent.TransactionDate = (DateTime)_ParentTransaction.TransactionDate;
                    }
                    else
                    {
                        _HCUAccountTransactionParent.TransactionDate = TransctionDate;
                    }
                    if (!string.IsNullOrEmpty(_Request.AccountNumber))
                    {
                        _Request.AccountNumber = _Request.AccountNumber.Trim().Replace("*", "X").Replace("x", "X").ToUpper();
                        _HCUAccountTransactionParent.AccountNumber = _Request.AccountNumber;
                    }
                    _HCUAccountTransactionParent.ReferenceNumber = _Request.ReferenceNumber;
                    _HCUAccountTransactionParent.Comment = _ParentTransaction.Comment;
                    _HCUAccountTransactionParent.CreateDate = TransctionDate;
                    if (_Request.CreatedById != 0)
                    {
                        _HCUAccountTransactionParent.CreatedById = _Request.CreatedById;
                    }
                    else
                    {
                        if (_Request.UserReference.AccountId != 0)
                        {
                            _HCUAccountTransactionParent.CreatedById = _Request.UserReference.AccountId;
                        }
                    }
                    if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                    {
                        _HCUAccountTransactionParent.ProviderId = _Request.UserReference.AccountId;
                    }
                    if (!string.IsNullOrEmpty(GroupKey))
                    {
                        _HCUAccountTransactionParent.GroupKey = GroupKey;
                    }
                    if (!string.IsNullOrEmpty(_Request.UserReference.RequestKey))
                    {
                        _HCUAccountTransactionParent.RequestKey = _Request.UserReference.RequestKey;
                    }
                    if (_Request.CampaignId != 0)
                    {
                        _HCUAccountTransactionParent.CampaignId = _Request.CampaignId;
                    }
                    _HCUAccountTransactionParent.TCode = TCode;
                    if (_ParentTransaction.StatusId != 0)
                    {
                        _HCUAccountTransactionParent.StatusId = _ParentTransaction.StatusId;
                    }
                    else
                    {
                        _HCUAccountTransactionParent.StatusId = _Request.StatusId;
                    }
                    if (_Request.ProgramId > 0)
                    {
                        _HCUAccountTransactionParent.ProgramId = _Request.ProgramId;
                    }
                    if (_Request.CashierReward > 0)
                    {
                        _HCUAccountTransactionParent.CashierReward = _Request.CashierReward;
                    }
                    if (!string.IsNullOrEmpty(_ParentTransaction.TransactionHash))
                    {
                        _HCUAccountTransactionParent.TransHash = _ParentTransaction.TransactionHash;
                    }
                    _ParentTransaction.TransactionKey = _HCUAccountTransactionParent.Guid;
                    #endregion
                    #endregion
                    #region Save Child Transactions
                    for (int i = 0; i < _ChildTransactions.Count; i++)
                    {
                        OCoreTransaction.TransactionItem Transaction = _ChildTransactions[i];
                        #region Save Transaction
                        _HCUAccountTransaction = new HCUAccountTransaction();
                        _HCUAccountTransaction.InoviceNumber = _Request.InvoiceNumber;
                        _HCUAccountTransaction.Guid = GroupKey + "-" + (i + 1);
                        Transaction.TransactionKey = _HCUAccountTransaction.Guid;
                        if (_Request.ParentId > 0)
                        {
                            _HCUAccountTransaction.ParentId = _Request.ParentId;
                        }
                        if (Transaction.UserAccountId != 0)
                        {
                            _HCUAccountTransaction.AccountId = Transaction.UserAccountId;
                        }
                        _HCUAccountTransaction.ModeId = Transaction.ModeId;
                        _HCUAccountTransaction.TypeId = Transaction.TypeId;
                        _HCUAccountTransaction.SourceId = Transaction.SourceId;
                        _HCUAccountTransaction.Amount = Transaction.Amount;
                        _HCUAccountTransaction.Charge = Transaction.Charge;
                        _HCUAccountTransaction.ComissionAmount = Transaction.Comission;
                        _HCUAccountTransaction.TotalAmount = Transaction.TotalAmount;
                        _HCUAccountTransaction.Balance = 0;
                        _HCUAccountTransaction.PurchaseAmount = _Request.InvoiceAmount;
                        if (_Request.PaymentMethodId > 0)
                        {
                            _HCUAccountTransaction.PaymentMethodId = _Request.PaymentMethodId;
                        }
                        if (_Request.ReferenceInvoiceAmount != 0)
                        {
                            _HCUAccountTransaction.ReferenceInvoiceAmount = _Request.ReferenceInvoiceAmount;
                        }
                        else
                        {
                            _HCUAccountTransaction.ReferenceInvoiceAmount = _Request.InvoiceAmount;
                        }
                        _HCUAccountTransaction.ReferenceAmount = (Transaction.ReferenceAmount == null || Transaction.ReferenceAmount == 0) ? _Request.ReferenceAmount : Transaction.ReferenceAmount;
                        if (_Request.BankId != 0)
                        {
                            _HCUAccountTransaction.BankId = _Request.BankId;
                        }
                        if (_Request.CustomerId != 0)
                        {
                            _HCUAccountTransaction.CustomerId = _Request.CustomerId;
                        }
                        else
                        {
                            if (Transaction.SourceId == TransactionSource.TUC)
                            {
                                _HCUAccountTransaction.CustomerId = Transaction.UserAccountId;
                            }
                        }
                        if (_Request.SubParentId != 0)
                        {
                            _HCUAccountTransaction.SubParentId = _Request.SubParentId;
                        }
                        if (_Request.CashierId != 0)
                        {
                            _HCUAccountTransaction.CashierId = _Request.CashierId;
                        }
                        if (_Request.TerminalId != 0)
                        {
                            _HCUAccountTransaction.TerminalId = _Request.TerminalId;
                        }
                        if (_Request.BinNumberId != 0)
                        {
                            _HCUAccountTransaction.BinNumberId = _Request.BinNumberId;
                        }
                        if (Transaction.TransactionDate != null)
                        {
                            _HCUAccountTransaction.TransactionDate = (DateTime)Transaction.TransactionDate;
                        }
                        else
                        {
                            _HCUAccountTransaction.TransactionDate = TransctionDate;
                        }
                        if (!string.IsNullOrEmpty(_Request.AccountNumber))
                        {
                            _Request.AccountNumber = _Request.AccountNumber.Trim().Replace("*", "X").Replace("x", "X").ToUpper();
                            _HCUAccountTransaction.AccountNumber = _Request.AccountNumber;
                        }
                        _HCUAccountTransaction.ReferenceNumber = _Request.ReferenceNumber;
                        _HCUAccountTransaction.Comment = Transaction.Comment;
                        _HCUAccountTransaction.CreateDate = TransctionDate;
                        if (_Request.CreatedById != 0)
                        {
                            _HCUAccountTransaction.CreatedById = _Request.CreatedById;
                        }
                        else
                        {
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountTransaction.CreatedById = _Request.UserReference.AccountId;
                            }
                        }
                        if (_Request.UserReference.AccountTypeId == UserAccountType.PosAccount)
                        {
                            _HCUAccountTransaction.ProviderId = _Request.UserReference.AccountId;
                        }
                        if (!string.IsNullOrEmpty(GroupKey))
                        {
                            _HCUAccountTransaction.GroupKey = GroupKey;
                        }
                        if (!string.IsNullOrEmpty(_Request.UserReference.RequestKey))
                        {
                            _HCUAccountTransaction.RequestKey = _Request.UserReference.RequestKey;
                        }
                        if (_Request.CampaignId != 0)
                        {
                            _HCUAccountTransaction.CampaignId = _Request.CampaignId;
                        }
                        _HCUAccountTransaction.TCode = TCode;
                        if (Transaction.StatusId != 0)
                        {
                            _HCUAccountTransaction.StatusId = Transaction.StatusId;
                        }
                        else
                        {
                            _HCUAccountTransaction.StatusId = _Request.StatusId;
                        }
                        if (_Request.ProgramId > 0)
                        {
                            _HCUAccountTransaction.ProgramId = _Request.ProgramId;
                        }
                        if (_Request.CashierReward > 0)
                        {
                            _HCUAccountTransaction.CashierReward = _Request.CashierReward;
                        }
                        if (!string.IsNullOrEmpty(Transaction.TransactionHash))
                        {
                            _HCUAccountTransaction.TransHash = Transaction.TransactionHash;
                        }

                        Transaction.TransactionKey = _HCUAccountTransaction.Guid;
                        _HCUAccountTransactionParent.InverseParentTransaction.Add(_HCUAccountTransaction);
                        #endregion
                    }
                    #endregion
                    _HCoreContext.HCUAccountTransaction.Add(_HCUAccountTransactionParent);
                    _HCoreContext.SaveChanges();
                    _ParentTransaction.TransactionId = _HCUAccountTransactionParent.Id;
                    _ParentTransaction.TransactionKey = _HCUAccountTransactionParent.Guid;
                    foreach (var item in _ChildTransactions)
                    {
                        item.TransactionId = _HCUAccountTransactionParent.InverseParentTransaction.Where(x => x.Guid == item.TransactionKey).Select(x => x.Id).FirstOrDefault();
                    }
                    #region TransactionPostProcess
                    _Request.TransactionDate = TransctionDate;
                    _Request.GroupKey = GroupKey;
                    _Request.ParentTransactionId = _ParentTransaction.TransactionId;

                    var _PostTransactionBalanceManagerActor = ActorSystem.Create("PostTransactionBalanceManagerActor");
                    var _PostTransactionBalanceManagerActorNotify = _PostTransactionBalanceManagerActor.ActorOf<PostTransactionBalanceManagerActor>("PostTransactionBalanceManagerActor");
                    _PostTransactionBalanceManagerActorNotify.Tell(_Request);

                    #endregion
                    #region Send Response
                    _TransactionResponse.Status = Transaction.Success;
                    _TransactionResponse.TransactionDate = _HCUAccountTransaction.CreateDate;
                    _TransactionResponse.ReferenceId = _ParentTransaction.TransactionId;
                    _TransactionResponse.ReferenceKey = GroupKey;
                    _TransactionResponse.TCode = TCode;
                    if (!string.IsNullOrEmpty(_Request.GroupKey))
                    {
                        _TransactionResponse.GroupKey = _Request.GroupKey;
                    }
                    _TransactionResponse.Message = "Transaction complete";
                    return _TransactionResponse;
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ProcessTransaction", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                _TransactionResponse.Status = Transaction.Error;
                _TransactionResponse.Message = "Error occured while processing request";
                return _TransactionResponse;
                #endregion
            }
            #endregion
        }



        //List<HCUAccountTransaction> _HCUAccountTransactions;
        HCUAccountTransaction _CreditTransaction;
        OCoreTransaction.Initialize.Response _InitializeResponse;
        /// <summary>
        /// Description: Gets the account balance overview.
        /// </summary>
        /// <param name="AccountId">The account identifier.</param>
        /// <param name="SourceId">The source identifier.</param>
        /// <returns>OAccountBalance.</returns>
        internal OAccountBalance GetAccountBalanceOverview(long AccountId, long SourceId)
        {
            #region Manage Exception
            try
            {
                _OAccountBalance = new OAccountBalance();
                using (_HCoreContext = new HCoreContext())
                {
                    if (SourceId == TransactionSource.TUC)
                    {
                        _OAccountBalance.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.AccountId == AccountId &&
                                                        (x.SourceId == SourceId || x.SourceId == TransactionSource.Payments) &&
                                                       x.ProgramId == null &&
                                                        x.ModeId == TransactionMode.Credit &&
                                                        x.StatusId == Transaction.Success)
                                                 .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                        _OAccountBalance.Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.AccountId == AccountId &&
                                                                  (x.SourceId == SourceId || x.SourceId == TransactionSource.Payments) &&
                                                       x.ProgramId == null &&
                                                                 x.ModeId == TransactionMode.Debit &&
                                                                 x.StatusId == Transaction.Success)
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                        _OAccountBalance.Balance = HCoreHelper.RoundNumber((_OAccountBalance.Credit - _OAccountBalance.Debit), _AppConfig.SystemEntryRoundDouble);
                        _OAccountBalance.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == AccountId
                                                                                                     && x.SourceId == SourceId)
                                                                         .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                        _HCoreContext.Dispose();
                        return _OAccountBalance;
                    }
                    else
                    {
                        _OAccountBalance.Credit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                 .Where(x => x.AccountId == AccountId &&
                                                        x.SourceId == SourceId &&
                                                       x.ProgramId == null &&
                                                        x.ModeId == TransactionMode.Credit &&
                                                        x.StatusId == Transaction.Success)
                                                 .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                        _OAccountBalance.Debit = HCoreHelper.RoundNumber(_HCoreContext.HCUAccountTransaction
                                                         .Where(x => x.AccountId == AccountId &&
                                                                  x.SourceId == SourceId &&
                                                       x.ProgramId == null &&
                                                                 x.ModeId == TransactionMode.Debit &&
                                                                 x.StatusId == Transaction.Success)
                                                          .Sum(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                        _OAccountBalance.Balance = HCoreHelper.RoundNumber((_OAccountBalance.Credit - _OAccountBalance.Debit), _AppConfig.SystemEntryRoundDouble);
                        _OAccountBalance.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.AccountId == AccountId
                                                                                                     && x.SourceId == SourceId)
                                                                         .OrderByDescending(x => x.TransactionDate).Select(x => x.TransactionDate).FirstOrDefault();
                        _HCoreContext.Dispose();
                        return _OAccountBalance;
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetAppUserBalance", _Exception);
                #endregion
                #region Send Response
                return null;
                #endregion
            }
            #endregion
        }
    }
}
