//==================================================================================
// FileName: FrameworkCoreUser.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core user
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Store;
using HCore.Helper;
using HCore.Operations.Object;
using Pipedrive;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations.Framework
{
    public class FrameworkCoreUser
    {

        #region Declare
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        HCUAccount _HCUAccount;
        //HCUAccountDevice _HCUs erDevice;
        HCUAccountOwner _HCUAccountOwner;
        HCoreContext _HCoreContext;
        HCUAccountParameter _HCUAccountParameter;
        HCCoreStorage _HCCoreStorage;
        List<HCUAccount> _HCUAccounts;
        List<HCUAccountOwner> _HCUAccountOwners;
        List<HCUAccountParameter> _HCUAccountParameters;
        #endregion
        /// <summary>
        /// Description: Saves the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveUserAccount(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Random = new Random();
                if (string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else
                {
                    long? AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                    string AccountCode = _Random.Next(100000000, 999999999).ToString();
                    switch (AccountTypeId)
                    {
                        case Helpers.UserAccountType.Controller:
                            AccountCode = _Random.Next(1, 9).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.Admin:
                            AccountCode = _Random.Next(1, 9).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.Merchant:
                            AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.MerchantStore:
                            AccountCode = _Random.Next(100000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.MerchantCashier:
                            AccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.Appuser:
                            AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                            break;
                        //case Helpers.UserAccountType.Carduser:
                        //AccountCode = _Random.Next(10000000, 99999999).ToString();
                        //break;
                        case Helpers.UserAccountType.PgAccount:
                            AccountCode = _Random.Next(10, 99).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.PosAccount:
                            AccountCode = _Random.Next(10000, 99999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        //case Helpers.UserAccountType.TerminalAccount:
                        //    AccountCode = _Random.Next(100000, 999999).ToString() + _Random.Next(000000000, 999999999).ToString();
                        //    break;
                        default:
                            AccountCode = _Random.Next(10000000, 999999999).ToString() + "" + _Random.Next(10000000, 999999999).ToString();
                            break;
                    }
                    if (string.IsNullOrEmpty(_Request.DisplayName))
                    {
                        _Request.DisplayName = "User";
                    }
                    if (string.IsNullOrEmpty(_Request.Name))
                    {
                        _Request.Name = "User";
                    }
                    if (string.IsNullOrEmpty(_Request.AccessPin))
                    {
                        _Request.AccessPin = _Random.Next(1000, 9999).ToString();
                    }
                    if (string.IsNullOrEmpty(_Request.ReferralCode))
                    {
                        _Request.ReferralCode = _Random.Next(100000000, 999999999).ToString();
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, 10);
                    }
                    if (string.IsNullOrEmpty(_Request.UserName))
                    {
                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            _Request.UserName = _Request.EmailAddress;
                        }
                        else
                        {
                            _Request.UserName = HCoreHelper.GenerateRandomNumber(10);
                        }
                    }
                    if (string.IsNullOrEmpty(_Request.Password))
                    {
                        _Request.Password = HCoreHelper.GenerateRandomNumber(8);
                    }
                    if (string.IsNullOrEmpty(_Request.SecondaryPassword))
                    {
                        _Request.SecondaryPassword = HCoreHelper.GenerateRandomNumber(8);
                    }
                    if (AccountTypeId == UserAccountType.Appuser)
                    {
                        _Request.UserName = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        _Request.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                    }
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    int? RegistrationSourceId = HCoreHelper.GetSystemHelperId(_Request.RegistrationSourceCode, _Request.UserReference);
                    int? AccountOperationTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountOperationTypeCode, _Request.UserReference);
                    int? ApplicationStatusId = HCoreHelper.GetSystemHelperId(_Request.ApplicationStatusCode, _Request.UserReference);
                    long? OwnerId = HCoreHelper.GetUserAccountId(_Request.OwnerKey, _Request.UserReference);
                    long? SubOwnerId = HCoreHelper.GetUserAccountId(_Request.SubOwnerKey, _Request.UserReference);
                    long? BankId = HCoreHelper.GetUserAccountId(_Request.BankKey, _Request.UserReference);
                    int? GenderId = HCoreHelper.GetSystemHelperId(_Request.GenderCode, HelperType.Gender, _Request.UserReference);
                    int? CountryId = HCoreHelper.GetCountryId(_Request.CountryKey, _Request.UserReference);
                    long? RegionId = HCoreHelper.GetCoreParameterId(_Request.RegionKey, _Request.UserReference);
                    long? CityId = HCoreHelper.GetCoreParameterId(_Request.CityKey, _Request.UserReference);
                    long? CityAreaId = HCoreHelper.GetCoreParameterId(_Request.CityAreaKey, _Request.UserReference);
                    long? RoleId = HCoreHelper.GetCoreParameterId(_Request.RoleKey, _Request.UserReference);
                    long? SubscriptionId = HCoreHelper.GetCoreCommonId(_Request.SubscriptionKey, _Request.UserReference);
                    int? AccountLevelId = HCoreHelper.GetSystemHelperId(_Request.AccountLevelCode, _Request.UserReference);
                    #region Operation
                    var Location = HCoreHelper.GetAddressComponent(_Request.Location, _Request.UserReference);
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserAccountDetails = _HCoreContext.HCUAccountAuth.Where(x => x.Username == _Request.UserName).Select(x => x.Id).FirstOrDefault();
                        if (UserAccountDetails == 0)
                        {
                            _HCUAccountOwners = new List<HCUAccountOwner>();
                            _HCUAccountParameters = new List<HCUAccountParameter>();
                            if (OwnerId != null)
                            {
                                _HCUAccountOwner = new HCUAccountOwner();
                                _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                _HCUAccountOwner.OwnerId = (long)OwnerId;
                                _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                _HCUAccountOwners.Add(_HCUAccountOwner);
                            }
                            if (SubOwnerId != null)
                            {
                                _HCUAccountOwner = new HCUAccountOwner();
                                _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                _HCUAccountOwner.OwnerId = (long)SubOwnerId;
                                _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                _HCUAccountOwners.Add(_HCUAccountOwner);
                            }
                            if (BankId != null)
                            {
                                _HCUAccountOwner = new HCUAccountOwner();
                                _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                _HCUAccountOwner.OwnerId = (long)BankId;
                                _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                _HCUAccountOwners.Add(_HCUAccountOwner);
                            }
                            if (_Request.Owners != null)
                            {
                                foreach (var OwnerItem in _Request.Owners)
                                {
                                    if (OwnerItem.OwnerKey != _Request.OwnerKey
                                        && OwnerItem.OwnerKey != _Request.SubOwnerKey
                                        && OwnerItem.OwnerKey != _Request.BankKey)
                                    {
                                        long ParentOwnerId = _HCoreContext.HCUAccount.Where(x => x.Guid == OwnerItem.OwnerKey).Select(x => x.Id).FirstOrDefault();
                                        if (ParentOwnerId != 0)
                                        {
                                            _HCUAccountOwner = new HCUAccountOwner();
                                            _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                            _HCUAccountOwner.OwnerId = ParentOwnerId;
                                            _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                            _HCUAccountOwners.Add(_HCUAccountOwner);
                                        }
                                    }
                                }
                            }
                            if (_Request.Configurations != null && _Request.Configurations.Count > 0)
                            {
                                foreach (var Configuration in _Request.Configurations)
                                {
                                    if (string.IsNullOrEmpty(Configuration.TypeCode))
                                    {
                                        if (!string.IsNullOrEmpty(Configuration.Value) && !string.IsNullOrEmpty(Configuration.SystemName))
                                        {
                                            long ConfigurationId = _HCoreContext.HCCoreCommon
                                                         .Where(x => x.SystemName == Configuration.SystemName && x.TypeId == HelperType.Configuration)
                                                         .Select(x => x.Id)
                                                         .FirstOrDefault();
                                            if (ConfigurationId != 0)
                                            {
                                                _HCUAccountParameter = new HCUAccountParameter();
                                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                                _HCUAccountParameter.CommonId = ConfigurationId;
                                                _HCUAccountParameter.Value = Configuration.Value;
                                                if (!string.IsNullOrEmpty(Configuration.HelperCode))
                                                {
                                                    _HCUAccountParameter.HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == Configuration.HelperCode).Select(x => x.Id).FirstOrDefault();
                                                }
                                                _HCUAccountParameter.StartTime = Configuration.StartTime;
                                                _HCUAccountParameter.EndTime = Configuration.EndTime;
                                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                                if (_Request.UserReference.AccountId != 0)
                                                {
                                                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                                }
                                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                                _HCUAccountParameters.Add(_HCUAccountParameter);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var ParameterTypeId = _HCoreContext.HCCore.Where(x => x.SystemName == Configuration.TypeCode).Select(x => x.Id).FirstOrDefault();
                                        if (ParameterTypeId != 0)
                                        {
                                            _HCUAccountParameter = new HCUAccountParameter();
                                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountParameter.TypeId = ParameterTypeId;
                                            _HCUAccountParameter.Value = Configuration.Value;
                                            if (!string.IsNullOrEmpty(Configuration.CommonKey))
                                            {
                                                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.Guid == Configuration.CommonKey || x.SystemName == Configuration.CommonKey).Select(x => x.Id).FirstOrDefault();
                                            }
                                            if (!string.IsNullOrEmpty(Configuration.ParameterKey))
                                            {
                                                _HCUAccountParameter.ParameterId = _HCoreContext.HCCoreParameter.Where(x => x.Guid == Configuration.ParameterKey).Select(x => x.Id).FirstOrDefault();
                                            }
                                            if (!string.IsNullOrEmpty(Configuration.HelperCode))
                                            {
                                                _HCUAccountParameter.HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == Configuration.HelperCode).Select(x => x.Id).FirstOrDefault();
                                            }
                                            _HCUAccountParameter.StartTime = Configuration.StartTime;
                                            _HCUAccountParameter.EndTime = Configuration.EndTime;
                                            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                            if (_Request.UserReference.AccountId != 0)
                                            {
                                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                            }
                                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                            _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                            _HCUAccountParameters.Add(_HCUAccountParameter);
                                        }
                                    }
                                }
                            }
                            _HCUAccounts = new List<HCUAccount>();
                            _Request.ReferenceKey = HCoreHelper.GenerateGuid();
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.Guid = _Request.ReferenceKey;
                            _HCUAccount.AccountTypeId = (int)AccountTypeId;
                            if (AccountOperationTypeId != null)
                            {
                                _HCUAccount.AccountOperationTypeId = (int)AccountOperationTypeId;
                            }
                            else
                            {
                                _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.OnlineAndOffline;
                            }
                            if (OwnerId != null)
                            {
                                _HCUAccount.OwnerId = OwnerId;
                            }
                            if (SubOwnerId != null)
                            {
                                _HCUAccount.SubOwnerId = SubOwnerId;
                            }
                            if (BankId != null)
                            {
                                _HCUAccount.BankId = BankId;
                            }
                            if (_Request.DisplayName.Length > 30)
                            {
                                _HCUAccount.DisplayName = _Request.DisplayName.Substring(0, 29);
                            }
                            else
                            {
                                _HCUAccount.DisplayName = _Request.DisplayName;
                            }
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                            _HCUAccount.AccountCode = AccountCode;
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ReferralCode = _Request.ReferralCode;
                            _HCUAccount.ReferralUrl = _Request.ReferralUrl;
                            _HCUAccount.Description = _Request.Description;
                            _HCUAccount.CountValue = _Request.CountValue;
                            _HCUAccount.AverageValue = _Request.AverageValue;
                            if (RoleId != null)
                            {
                                _HCUAccount.RoleId = RoleId;
                            }
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }

                            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;

                            if (ApplicationStatusId != null)
                            {
                                _HCUAccount.ApplicationStatusId = ApplicationStatusId;
                            }
                            if (RegistrationSourceId != null)
                            {
                                _HCUAccount.RegistrationSourceId = RegistrationSourceId;
                            }
                            else
                            {
                                _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                            }
                            if (AccountLevelId != null && AccountLevelId != 0)
                            {
                                _HCUAccount.AccountLevel = AccountLevelId;
                            }
                            if (SubscriptionId != null && SubscriptionId != 0)
                            {
                                _HCUAccount.SubscriptionId = SubscriptionId;
                            }

                            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            }
                            if (StatusId != null)
                            {
                                _HCUAccount.StatusId = (int)StatusId;
                            }
                            else
                            {
                                _HCUAccount.StatusId = HelperStatus.Default.Inactive;
                            }
                            if (_HCUAccountOwners != null && _HCUAccountOwners.Count > 0)
                            {
                                _HCUAccount.HCUAccountOwnerAccount = _HCUAccountOwners;
                            }
                            if (_HCUAccountParameters != null && _HCUAccountParameters.Count > 0)
                            {
                                _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                            }
                            _HCUAccount.AccountPercentage = _Request.AccountPercentage;
                            _HCUAccount.Name = _Request.Name;
                            _HCUAccount.FirstName = _Request.FirstName;
                            _HCUAccount.LastName = _Request.LastName;
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ContactNumber = _Request.ContactNumber;
                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                            _HCUAccount.SecondaryEmailAddress = _Request.SecondaryEmailAddress;
                            if (GenderId != null)
                            {
                                _HCUAccount.GenderId = GenderId;
                            }
                            if (_Request.DateOfBirth != null)
                            {
                                _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                            }
                            _HCUAccount.Address = _Request.Address;
                            if (_Request.Latitude != 0)
                            {
                                _HCUAccount.Latitude = _Request.Latitude;
                            }
                            if (_Request.Longitude != 0)
                            {
                                _HCUAccount.Longitude = _Request.Longitude;
                            }
                            if (CountryId != null)
                            {
                                _HCUAccount.CountryId = CountryId;
                            }
                            if (RegionId != null)
                            {
                                _HCUAccount.StateId = RegionId;
                            }
                            //if (RegionAreaId != null)
                            //{
                            //    _HCUAccount.RegionAreaId = RegionAreaId;
                            //}
                            if (CityId != null)
                            {
                                _HCUAccount.CityId = CityId;
                            }
                            if (CityAreaId != null)
                            {
                                _HCUAccount.CityAreaId = CityAreaId;
                            }
                            #region Location Manager
                            if (_Request.Location != null && Location != null)
                            {
                                if (Location.CountryId > 0)
                                {
                                    _HCUAccount.CountryId = Location.CountryId;
                                }
                                if (Location.StateId > 0)
                                {
                                    _HCUAccount.StateId = Location.StateId;
                                }
                                if (Location.CityId > 0)
                                {
                                    _HCUAccount.CityId = Location.CityId;
                                }
                                if (Location.CityAreaId > 0)
                                {
                                    _HCUAccount.CityAreaId = Location.CityAreaId;
                                }
                                if (Location.Latitude != 0)
                                {
                                    _HCUAccount.Latitude = Location.Latitude;
                                }
                                if (Location.Longitude != 0)
                                {
                                    _HCUAccount.Longitude = Location.Longitude;
                                }
                                if (!string.IsNullOrEmpty(Location.Address))
                                {
                                    _HCUAccount.Address = Location.Address;
                                }
                            }
                            #endregion


                            _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                            _HCUAccount.EmailVerificationStatus = _Request.EmailVerificationStatus;
                            if (_Request.EmailVerificationStatusDate != null)
                            {
                                _HCUAccount.EmailVerificationStatusDate = _Request.EmailVerificationStatusDate;
                            }
                            else
                            {
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            }
                            _HCUAccount.NumberVerificationStatus = _Request.NumberVerificationStatus;
                            if (_Request.NumberVerificationStatusDate != null)
                            {
                                _HCUAccount.NumberVerificationStatusDate = _Request.NumberVerificationStatusDate;
                            }
                            else
                            {
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            }
                            _HCUAccounts.Add(_HCUAccount);
                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountAuth.Username = _Request.UserName;
                            _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                            _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(_Request.SecondaryPassword);
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                            _HCUAccountAuth.HCUAccount = _HCUAccounts;
                            _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                            _HCoreContext.SaveChanges();
                            long? IconStorageId = null;
                            long? PosterStorageId = null;
                            if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                            }
                            if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                            {
                                PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                            }
                            if (IconStorageId != null || PosterStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                    if (UserAccDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            UserAccDetails.IconStorageId = IconStorageId;
                                        }
                                        if (PosterStorageId != null)
                                        {
                                            UserAccDetails.PosterStorageId = PosterStorageId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }

                            if (_HCUAccount.AccountTypeId != UserAccountType.Appuser)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    OHCoreDataStore.Account Account = (from x in _HCoreContext.HCUAccount
                                                                       where x.Id == _HCUAccount.Id && (x.AccountTypeId == Helpers.UserAccountType.Acquirer
                                                                      || x.AccountTypeId == Helpers.UserAccountType.PgAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.PosAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                                      || x.AccountTypeId == Helpers.UserAccountType.Merchant
                                                                      || x.AccountTypeId == Helpers.UserAccountType.MerchantCashier
                                                                      || x.AccountTypeId == Helpers.UserAccountType.AcquirerSubAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.MerchantSubAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.Controller
                                                                      || x.AccountTypeId == Helpers.UserAccountType.Admin)
                                                                       select new OHCoreDataStore.Account
                                                                       {
                                                                           ReferenceId = x.Id,
                                                                           ReferenceKey = x.Guid,

                                                                           UserName = x.User.Username,

                                                                           DisplayName = x.DisplayName,
                                                                           Name = x.Name,
                                                                           FirstName = x.FirstName,
                                                                           LastName = x.LastName,

                                                                           EmailAddress = x.EmailAddress,
                                                                           SecondaryEmailAddress = x.SecondaryEmailAddress,

                                                                           ContactNumber = x.ContactNumber,
                                                                           MobileNumber = x.MobileNumber,

                                                                           Address = x.Address,
                                                                           Latitude = x.Latitude,
                                                                           Longitude = x.Longitude,

                                                                           IconUrl = x.IconStorage.Path,
                                                                           PosterUrl = x.PosterStorage.Path,

                                                                           Description = x.Description,


                                                                           AccountTypeId = x.AccountTypeId,
                                                                           AccountTypeCode = x.AccountType.SystemName,
                                                                           AccountTypeName = x.AccountType.Name,

                                                                           RoleId = x.RoleId,
                                                                           RoleKey = x.Role.Guid,
                                                                           RoleName = x.Role.Name,


                                                                           LastLoginDate = x.LastLoginDate,
                                                                           LastActivityDate = null,

                                                                           LastTransactionDate = x.LastTransactionDate,

                                                                           CreateDate = x.CreateDate,
                                                                           CreatedById = x.CreatedById,
                                                                           CreatedByKey = x.CreatedBy.Guid,
                                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                           CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                                           CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                                           CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                                           ModifyDate = x.ModifyDate,
                                                                           ModifyById = x.ModifyById,
                                                                           ModifyByKey = x.ModifyBy.Guid,
                                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                           ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                                           ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                                           ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                                           StatusId = x.StatusId,
                                                                           StatusCode = x.Status.SystemName,
                                                                           StatusName = x.Status.Name,

                                                                           OwnerId = x.OwnerId,
                                                                           OwnerKey = x.Owner.Guid,
                                                                           OwnerDisplayName = x.Owner.DisplayName,
                                                                           OwnerIconUrl = x.Owner.IconStorage.Path,

                                                                           SubOwnerId = x.SubOwnerId,
                                                                           SubOwnerKey = x.SubOwner.Guid,
                                                                           SubOwnerDisplayName = x.SubOwner.DisplayName,
                                                                           SubOwnerIconUrl = x.SubOwner.IconStorage.Path,

                                                                           BankId = x.BankId,
                                                                           BankKey = x.Bank.Guid,
                                                                           BankDisplayName = x.Bank.DisplayName,
                                                                           BankIconUrl = x.Bank.IconStorage.Path,

                                                                           MerchantsCount = 0,
                                                                           StoresCount = 0,
                                                                           AcquirersCount = 0,
                                                                           TerminalsCount = 0,
                                                                           CashiersCount = 0,
                                                                           PgAccountsCount = 0,
                                                                           PosAccountsCount = 0,
                                                                           SubAccountsCount = 0,
                                                                       }).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        //if (_HCUAccount.AccountTypeId == UserAccountType.TerminalAccount)
                                        //{
                                        //    #region Terminals
                                        //    var Terminal = (from x in _HCoreContext.HCUAccount
                                        //                    where x.Guid == _Request.ReferenceKey && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.TerminalAccount
                                        //                    select new OHCoreDataStore.Terminal
                                        //                    {
                                        //                        ReferenceId = x.Id,
                                        //                        ReferenceKey = x.Guid,
                                        //                        DisplayName = x.DisplayName,
                                        //                        ProviderId = x.OwnerId,
                                        //                        MerchantId = x.SubOwner.OwnerId,
                                        //                        StoreId = x.SubOwnerId,
                                        //                        AcquirerId = x.BankId,

                                        //                        ApplicationStatusId = x.ApplicationStatusId,
                                        //                        LastTransactionDate = x.LastTransactionDate,

                                        //                        CreateDate = x.CreateDate,
                                        //                        CreatedById = x.CreatedById,
                                        //                        CreatedByKey = x.CreatedBy.Guid,
                                        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                        //                        CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                        //                        CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                        //                        CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                        //                        ModifyDate = x.ModifyDate,
                                        //                        ModifyById = x.ModifyById,
                                        //                        ModifyByKey = x.ModifyBy.Guid,
                                        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,

                                        //                        ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                        //                        ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                        //                        ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                        //                        StatusId = x.StatusId,
                                        //                        StatusCode = x.Status.SystemName,
                                        //                        StatusName = x.Status.Name,

                                        //                    }).FirstOrDefault();

                                        //    if (Terminal != null)
                                        //    {
                                        //        #region Accounts Data Update
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.Merchant)
                                        //        {
                                        //            Account.StoresCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore);
                                        //            Account.CashiersCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantCashier);
                                        //            Account.SubAccountsCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantSubAccount);
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        //            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        //        }
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.Acquirer)
                                        //        {
                                        //            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        //            Account.SubAccountsCount = HCoreDataStore.Accounts.Where(x => x.OwnerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        //            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        //        }
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.PosAccount)
                                        //        {
                                        //            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        //            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        //        }
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                                        //        {
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        //            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.IconUrl))
                                        //        {
                                        //            Account.IconUrl = _AppConfig.StorageUrl + Account.IconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.IconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.OwnerIconUrl))
                                        //        {
                                        //            Account.OwnerIconUrl = _AppConfig.StorageUrl + Account.OwnerIconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.OwnerIconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.SubOwnerIconUrl))
                                        //        {
                                        //            Account.SubOwnerIconUrl = _AppConfig.StorageUrl + Account.SubOwnerIconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.SubOwnerIconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.BankIconUrl))
                                        //        {
                                        //            Account.BankIconUrl = _AppConfig.StorageUrl + Account.BankIconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.BankIconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        #endregion
                                        //        #region Terminals Data Update
                                        //        Terminal.RmId = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == Terminal.StoreId
                                        //        && x.Account.OwnerId == Terminal.AcquirerId && x.StatusId == HelperStatus.Default.Active
                                        //        && x.Account.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager).Select(x => x.AccountId).FirstOrDefault();
                                        //        Terminal.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == Terminal.ReferenceId).OrderByDescending(x => x.Id).Select(x => x.TransactionDate).FirstOrDefault();
                                        //        var ProviderDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.ProviderId).FirstOrDefault();
                                        //        if (ProviderDetails != null)
                                        //        {
                                        //            Terminal.ProviderKey = ProviderDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(ProviderDetails.IconUrl))
                                        //            {
                                        //                Terminal.ProviderIconUrl = ProviderDetails.IconUrl;
                                        //            }
                                        //            Terminal.ProviderDisplayName = ProviderDetails.DisplayName;
                                        //        }

                                        //        var MerchantDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault();
                                        //        if (MerchantDetails != null)
                                        //        {
                                        //            Terminal.MerchantKey = MerchantDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                                        //            {
                                        //                Terminal.MerchantIconUrl = MerchantDetails.IconUrl;
                                        //            }
                                        //            Terminal.MerchantDisplayName = MerchantDetails.DisplayName;
                                        //        }

                                        //        var StoreDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.StoreId).FirstOrDefault();
                                        //        if (StoreDetails != null)
                                        //        {
                                        //            Terminal.StoreKey = StoreDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(StoreDetails.IconUrl))
                                        //            {
                                        //                Terminal.StoreIconUrl = StoreDetails.IconUrl;
                                        //            }
                                        //            Terminal.StoreDisplayName = StoreDetails.DisplayName;
                                        //        }
                                        //        var AcquirerDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.AcquirerId).FirstOrDefault();
                                        //        if (AcquirerDetails != null)
                                        //        {
                                        //            Terminal.AcquirerKey = AcquirerDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(AcquirerDetails.IconUrl))
                                        //            {
                                        //                Terminal.AcquirerIconUrl = AcquirerDetails.IconUrl;
                                        //            }
                                        //            Terminal.AcquirerDisplayName = AcquirerDetails.DisplayName;
                                        //        }
                                        //        var RmDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.RmId).FirstOrDefault();
                                        //        if (RmDetails != null)
                                        //        {
                                        //            Terminal.RmKey = RmDetails.ReferenceKey;
                                        //            Terminal.RmDisplayName = RmDetails.DisplayName;
                                        //        }
                                        //        #endregion
                                        //        HCoreDataStore.Accounts.Add(Account);
                                        //        HCoreDataStore.Terminals.Add(Terminal);
                                        //    }
                                        //    #endregion
                                        //    _HCoreContext.Dispose();
                                        //}
                                        //else
                                        //{
                                        #region Accounts Data Update
                                        if (Account.AccountTypeId == Helpers.UserAccountType.Merchant)
                                        {
                                            Account.StoresCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore);
                                            Account.CashiersCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantCashier);
                                            Account.SubAccountsCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantSubAccount);
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        }
                                        if (Account.AccountTypeId == Helpers.UserAccountType.Acquirer)
                                        {
                                            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                            Account.SubAccountsCount = HCoreDataStore.Accounts.Where(x => x.OwnerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        }
                                        if (Account.AccountTypeId == Helpers.UserAccountType.PosAccount)
                                        {
                                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        }
                                        if (Account.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                                        {
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        }
                                        if (!string.IsNullOrEmpty(Account.IconUrl))
                                        {
                                            Account.IconUrl = _AppConfig.StorageUrl + Account.IconUrl;
                                        }
                                        else
                                        {
                                            Account.IconUrl = _AppConfig.Default_Icon;
                                        }
                                        if (!string.IsNullOrEmpty(Account.OwnerIconUrl))
                                        {
                                            Account.OwnerIconUrl = _AppConfig.StorageUrl + Account.OwnerIconUrl;
                                        }
                                        else
                                        {
                                            Account.OwnerIconUrl = _AppConfig.Default_Icon;
                                        }
                                        if (!string.IsNullOrEmpty(Account.SubOwnerIconUrl))
                                        {
                                            Account.SubOwnerIconUrl = _AppConfig.StorageUrl + Account.SubOwnerIconUrl;
                                        }
                                        else
                                        {
                                            Account.SubOwnerIconUrl = _AppConfig.Default_Icon;
                                        }
                                        if (!string.IsNullOrEmpty(Account.BankIconUrl))
                                        {
                                            Account.BankIconUrl = _AppConfig.StorageUrl + Account.BankIconUrl;
                                        }
                                        else
                                        {
                                            Account.BankIconUrl = _AppConfig.Default_Icon;
                                        }
                                        #endregion

                                        HCoreDataStore.Accounts.Add(Account);
                                        _HCoreContext.Dispose();
                                        //}
                                    }
                                    else
                                    {
                                        _HCoreContext.Dispose();
                                    }
                                }
                            }
                            if (_HCUAccount.AccountTypeId == UserAccountType.Merchant)
                            {
                                try
                                {
                                    PipedriveClient client = new PipedriveClient(new ProductHeaderValue("0001"), new Uri("https://thankucashsales.pipedrive.com"))
                                    {
                                        Credentials = new Credentials("b4cda3bb82c8af5b1ef71f9c1152fb155b27f81f", AuthenticationType.ApiToken),
                                    };

                                    NewActivity _NewActivity = new NewActivity("New Merchant Created " + _Request.Name, "Merchant");
                                    _NewActivity.Subject = "New Merchant Registration : " + _Request.Name;
                                    _NewActivity.Done = 0;
                                    _NewActivity.Type = "call";
                                    _NewActivity.OrgId = 1;
                                    _NewActivity.Note = "Merchant Request <br> Name:" + _Request.Name + "<br>Contact Number : " + _Request.MobileNumber + "<br>Email Address : " + _Request.EmailAddress;
                                    client.Activity.Create(_NewActivity);
                                }
                                catch (Exception _Exception)
                                {
                                    HCoreHelper.LogException("PIPEDRIVE", _Exception);
                                }

                                if (!string.IsNullOrEmpty(_Request.EmailAddress))
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var MerchantDetails = _HCoreContext.HCUAccount
                                                .Where(x => x.Guid == _Request.ReferenceKey)
                                                .Select(x => new
                                                {
                                                    UserName = x.User.Username,
                                                    Pasword = x.User.Password,
                                                    Name = x.Name,
                                                    DisplayName = x.DisplayName,
                                                    EmailAddress = x.EmailAddress,
                                                }).FirstOrDefault();
                                        if (MerchantDetails != null)
                                        {
                                            var _EmailParameters = new
                                            {
                                                UserDisplayName = MerchantDetails.DisplayName,
                                                UserName = MerchantDetails.UserName,
                                                Password = HCoreEncrypt.DecryptHash(MerchantDetails.Pasword),
                                                Name = MerchantDetails.Name,
                                            };
                                            HCoreHelper.BroadCastEmail("d-8a7bc7cd93d84cd7ac25684a3e851aea", MerchantDetails.DisplayName, MerchantDetails.EmailAddress, _EmailParameters, _Request.UserReference);
                                        }
                                    }
                                }
                            }
                            var TReq = _Request;
                            TReq.UserReference = null;
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, TReq, "HC1097");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1101", "Account already exists");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SaveUserAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1102");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user account parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OUserAccount.Request.</returns>
        internal OUserAccount.Request SaveUserAccountParameter(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                _Random = new Random();
                if (string.IsNullOrEmpty(_Request.AccountTypeCode))
                {
                    return null;
                }
                else
                {
                    int? AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                    string AccountCode = _Random.Next(100000000, 999999999).ToString();
                    switch (AccountTypeId)
                    {
                        case Helpers.UserAccountType.Controller:
                            AccountCode = _Random.Next(1, 9).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.Admin:
                            AccountCode = _Random.Next(1, 9).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.Merchant:
                            AccountCode = _Random.Next(100, 999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.MerchantStore:
                            AccountCode = _Random.Next(100000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.MerchantCashier:
                            AccountCode = _Random.Next(1000, 9999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.Appuser:
                            AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                            break;
                        //case Helpers.UserAccountType.Carduser:
                        //AccountCode = _Random.Next(10000000, 99999999).ToString();
                        //break;
                        case Helpers.UserAccountType.PgAccount:
                            AccountCode = _Random.Next(10, 99).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        case Helpers.UserAccountType.PosAccount:
                            AccountCode = _Random.Next(10000, 99999).ToString() + _Random.Next(000000000, 999999999).ToString();
                            break;
                        //case Helpers.UserAccountType.TerminalAccount:
                        //    AccountCode = _Random.Next(100000, 999999).ToString() + _Random.Next(000000000, 999999999).ToString();
                        //    break;
                        default:
                            AccountCode = _Random.Next(10000000, 999999999).ToString() + "" + _Random.Next(10000000, 999999999).ToString();
                            break;
                    }
                    if (string.IsNullOrEmpty(_Request.DisplayName))
                    {
                        _Request.DisplayName = "User";
                    }
                    if (string.IsNullOrEmpty(_Request.Name))
                    {
                        _Request.Name = "User";
                    }
                    if (string.IsNullOrEmpty(_Request.AccessPin))
                    {
                        _Request.AccessPin = _Random.Next(1000, 9999).ToString();
                    }
                    if (string.IsNullOrEmpty(_Request.ReferralCode))
                    {
                        _Request.ReferralCode = _Random.Next(100000000, 999999999).ToString();
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, 10);
                    }
                    if (string.IsNullOrEmpty(_Request.UserName))
                    {
                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            _Request.UserName = _Request.EmailAddress;
                        }
                        else
                        {
                            _Request.UserName = HCoreHelper.GenerateRandomNumber(10);
                        }
                    }
                    if (string.IsNullOrEmpty(_Request.Password))
                    {
                        _Request.Password = HCoreHelper.GenerateRandomNumber(8);
                    }
                    if (string.IsNullOrEmpty(_Request.SecondaryPassword))
                    {
                        _Request.SecondaryPassword = HCoreHelper.GenerateRandomNumber(8);
                    }
                    if (AccountTypeId == UserAccountType.Appuser)
                    {
                        _Request.UserName = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        _Request.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                    }
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    int? RegistrationSourceId = HCoreHelper.GetSystemHelperId(_Request.RegistrationSourceCode, _Request.UserReference);
                    int? AccountOperationTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountOperationTypeCode, _Request.UserReference);
                    int? ApplicationStatusId = HCoreHelper.GetSystemHelperId(_Request.ApplicationStatusCode, _Request.UserReference);
                    long? OwnerId = HCoreHelper.GetUserAccountId(_Request.OwnerKey, _Request.UserReference);
                    long? SubOwnerId = HCoreHelper.GetUserAccountId(_Request.SubOwnerKey, _Request.UserReference);
                    long? BankId = HCoreHelper.GetUserAccountId(_Request.BankKey, _Request.UserReference);
                    int? GenderId = HCoreHelper.GetSystemHelperId(_Request.GenderCode, HelperType.Gender, _Request.UserReference);
                    int? CountryId = HCoreHelper.GetCountryId(_Request.CountryKey, _Request.UserReference);
                    long? RegionId = HCoreHelper.GetCoreParameterId(_Request.RegionKey, _Request.UserReference);
                    long? RegionAreaId = HCoreHelper.GetCoreParameterId(_Request.RegionAreaKey, _Request.UserReference);
                    long? CityId = HCoreHelper.GetCoreParameterId(_Request.CityKey, _Request.UserReference);
                    long? CityAreaId = HCoreHelper.GetCoreParameterId(_Request.CityAreaKey, _Request.UserReference);
                    long? RoleId = HCoreHelper.GetCoreParameterId(_Request.RoleKey, _Request.UserReference);
                    long? SubscriptionId = HCoreHelper.GetCoreCommonId(_Request.SubscriptionKey, _Request.UserReference);
                    int? AccountLevelId = HCoreHelper.GetSystemHelperId(_Request.AccountLevelCode, _Request.UserReference);
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        long UserAccountDetails = _HCoreContext.HCUAccountAuth.Where(x => x.Username == _Request.UserName).Select(x => x.Id).FirstOrDefault();
                        if (UserAccountDetails == 0 || _Request.SourceId == TransactionType.GiftCard)
                        {
                            _HCUAccountOwners = new List<HCUAccountOwner>();
                            _HCUAccountParameters = new List<HCUAccountParameter>();
                            if (OwnerId != null)
                            {
                                _HCUAccountOwner = new HCUAccountOwner();
                                _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                _HCUAccountOwner.OwnerId = (long)OwnerId;
                                _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                _HCUAccountOwners.Add(_HCUAccountOwner);
                            }
                            if (SubOwnerId != null)
                            {
                                _HCUAccountOwner = new HCUAccountOwner();
                                _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                _HCUAccountOwner.OwnerId = (long)SubOwnerId;
                                _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                _HCUAccountOwners.Add(_HCUAccountOwner);
                            }
                            if (BankId != null)
                            {
                                _HCUAccountOwner = new HCUAccountOwner();
                                _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                _HCUAccountOwner.OwnerId = (long)BankId;
                                _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                _HCUAccountOwners.Add(_HCUAccountOwner);
                            }
                            if (_Request.Owners != null)
                            {
                                foreach (var OwnerItem in _Request.Owners)
                                {
                                    if (OwnerItem.OwnerKey != _Request.OwnerKey
                                        && OwnerItem.OwnerKey != _Request.SubOwnerKey
                                        && OwnerItem.OwnerKey != _Request.BankKey)
                                    {
                                        long ParentOwnerId = _HCoreContext.HCUAccount.Where(x => x.Guid == OwnerItem.OwnerKey).Select(x => x.Id).FirstOrDefault();
                                        if (ParentOwnerId != 0)
                                        {
                                            _HCUAccountOwner = new HCUAccountOwner();
                                            _HCUAccountOwner.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountOwner.AccountTypeId = (int)AccountTypeId;
                                            _HCUAccountOwner.OwnerId = ParentOwnerId;
                                            _HCUAccountOwner.StartDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountOwner.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _HCUAccountOwner.StatusId = HelperStatus.Default.Active;
                                            _HCUAccountOwners.Add(_HCUAccountOwner);
                                        }
                                    }
                                }
                            }
                            if (_Request.Configurations != null && _Request.Configurations.Count > 0)
                            {
                                foreach (var Configuration in _Request.Configurations)
                                {
                                    if (string.IsNullOrEmpty(Configuration.TypeCode))
                                    {
                                        if (!string.IsNullOrEmpty(Configuration.Value) && !string.IsNullOrEmpty(Configuration.SystemName))
                                        {
                                            long ConfigurationId = _HCoreContext.HCCoreCommon
                                                         .Where(x => x.SystemName == Configuration.SystemName && x.TypeId == HelperType.Configuration)
                                                         .Select(x => x.Id)
                                                         .FirstOrDefault();
                                            if (ConfigurationId != 0)
                                            {
                                                _HCUAccountParameter = new HCUAccountParameter();
                                                _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                                _HCUAccountParameter.TypeId = HelperType.ConfigurationValue;
                                                _HCUAccountParameter.CommonId = ConfigurationId;
                                                _HCUAccountParameter.Value = Configuration.Value;
                                                if (!string.IsNullOrEmpty(Configuration.HelperCode))
                                                {
                                                    _HCUAccountParameter.HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == Configuration.HelperCode).Select(x => x.Id).FirstOrDefault();
                                                }
                                                _HCUAccountParameter.StartTime = Configuration.StartTime;
                                                _HCUAccountParameter.EndTime = Configuration.EndTime;
                                                _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                                if (_Request.UserReference.AccountId != 0)
                                                {
                                                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                                }
                                                _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                                _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                                _HCUAccountParameters.Add(_HCUAccountParameter);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        var ParameterTypeId = _HCoreContext.HCCore.Where(x => x.SystemName == Configuration.TypeCode).Select(x => x.Id).FirstOrDefault();
                                        if (ParameterTypeId != 0)
                                        {
                                            _HCUAccountParameter = new HCUAccountParameter();
                                            _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                            _HCUAccountParameter.TypeId = ParameterTypeId;
                                            _HCUAccountParameter.Value = Configuration.Value;
                                            if (!string.IsNullOrEmpty(Configuration.CommonKey))
                                            {
                                                _HCUAccountParameter.CommonId = _HCoreContext.HCCoreCommon.Where(x => x.Guid == Configuration.CommonKey || x.SystemName == Configuration.CommonKey).Select(x => x.Id).FirstOrDefault();
                                            }
                                            if (!string.IsNullOrEmpty(Configuration.ParameterKey))
                                            {
                                                _HCUAccountParameter.ParameterId = _HCoreContext.HCCoreParameter.Where(x => x.Guid == Configuration.ParameterKey).Select(x => x.Id).FirstOrDefault();
                                            }
                                            if (!string.IsNullOrEmpty(Configuration.HelperCode))
                                            {
                                                _HCUAccountParameter.HelperId = _HCoreContext.HCCore.Where(x => x.SystemName == Configuration.HelperCode).Select(x => x.Id).FirstOrDefault();
                                            }
                                            _HCUAccountParameter.StartTime = Configuration.StartTime;
                                            _HCUAccountParameter.EndTime = Configuration.EndTime;
                                            _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                            if (_Request.UserReference.AccountId != 0)
                                            {
                                                _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                            }
                                            _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                            _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                                            _HCUAccountParameters.Add(_HCUAccountParameter);
                                        }
                                    }
                                }
                            }
                            _HCUAccounts = new List<HCUAccount>();
                            _Request.ReferenceKey = HCoreHelper.GenerateGuid();
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.Guid = _Request.ReferenceKey;
                            _HCUAccount.AccountTypeId = (int)AccountTypeId;
                            if (AccountOperationTypeId != null)
                            {
                                _HCUAccount.AccountOperationTypeId = (int)AccountOperationTypeId;
                            }
                            else
                            {
                                _HCUAccount.AccountOperationTypeId = Helpers.AccountOperationType.OnlineAndOffline;
                            }
                            if (OwnerId != null)
                            {
                                _HCUAccount.OwnerId = OwnerId;
                            }
                            if (SubOwnerId != null)
                            {
                                _HCUAccount.SubOwnerId = SubOwnerId;
                            }
                            if (BankId != null)
                            {
                                _HCUAccount.BankId = BankId;
                            }
                            _HCUAccount.DisplayName = _Request.DisplayName;
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                            _HCUAccount.AccountCode = AccountCode;
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ReferralCode = _Request.ReferralCode;
                            _HCUAccount.ReferralUrl = _Request.ReferralUrl;
                            _HCUAccount.Description = _Request.Description;
                            _HCUAccount.CountValue = _Request.CountValue;
                            _HCUAccount.AverageValue = _Request.AverageValue;
                            if (RoleId != null)
                            {
                                _HCUAccount.RoleId = RoleId;
                            }
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }

                            _HCUAccount.RequestKey = _Request.UserReference.RequestKey;

                            if (ApplicationStatusId != null)
                            {
                                _HCUAccount.ApplicationStatusId = ApplicationStatusId;
                            }
                            if (RegistrationSourceId != null)
                            {
                                _HCUAccount.RegistrationSourceId = RegistrationSourceId;
                            }
                            else
                            {
                                _HCUAccount.RegistrationSourceId = Helpers.RegistrationSource.System;
                            }
                            if (AccountLevelId != null && AccountLevelId != 0)
                            {
                                _HCUAccount.AccountLevel = AccountLevelId;
                            }
                            if (SubscriptionId != null && SubscriptionId != 0)
                            {
                                _HCUAccount.SubscriptionId = SubscriptionId;
                            }

                            _HCUAccount.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            }
                            if (StatusId != null)
                            {
                                _HCUAccount.StatusId = (int)StatusId;
                            }
                            else
                            {
                                _HCUAccount.StatusId = HelperStatus.Default.Inactive;
                            }
                            if (_HCUAccountOwners != null && _HCUAccountOwners.Count > 0)
                            {
                                _HCUAccount.HCUAccountOwnerAccount = _HCUAccountOwners;
                            }
                            if (_HCUAccountParameters != null && _HCUAccountParameters.Count > 0)
                            {
                                _HCUAccount.HCUAccountParameterAccount = _HCUAccountParameters;
                            }
                            _HCUAccount.AccountPercentage = _Request.AccountPercentage;
                            _HCUAccount.Name = _Request.Name;
                            _HCUAccount.FirstName = _Request.FirstName;
                            _HCUAccount.LastName = _Request.LastName;
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ContactNumber = _Request.ContactNumber;
                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                            _HCUAccount.SecondaryEmailAddress = _Request.SecondaryEmailAddress;
                            if (GenderId != null)
                            {
                                _HCUAccount.GenderId = GenderId;
                            }
                            if (_Request.DateOfBirth != null)
                            {
                                _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                            }
                            _HCUAccount.Address = _Request.Address;
                            if (_Request.Latitude != 0)
                            {
                                _HCUAccount.Latitude = _Request.Latitude;
                            }
                            if (_Request.Longitude != 0)
                            {
                                _HCUAccount.Longitude = _Request.Longitude;
                            }
                            if (CountryId != null)
                            {
                                _HCUAccount.CountryId = CountryId;
                            }
                            if (RegionId != null)
                            {
                                _HCUAccount.StateId = RegionId;
                            }
                            //if (RegionAreaId != null)
                            //{
                            //    _HCUAccount.RegionAreaId = RegionAreaId;
                            //}
                            if (CityId != null)
                            {
                                _HCUAccount.CityId = CityId;
                            }
                            if (CityAreaId != null)
                            {
                                _HCUAccount.CityAreaId = CityAreaId;
                            }
                            _HCUAccount.WebsiteUrl = _Request.WebsiteUrl;
                            _HCUAccount.EmailVerificationStatus = _Request.EmailVerificationStatus;
                            if (_Request.EmailVerificationStatusDate != null)
                            {
                                _HCUAccount.EmailVerificationStatusDate = _Request.EmailVerificationStatusDate;
                            }
                            else
                            {
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            }
                            _HCUAccount.NumberVerificationStatus = _Request.NumberVerificationStatus;
                            if (_Request.NumberVerificationStatusDate != null)
                            {
                                _HCUAccount.NumberVerificationStatusDate = _Request.NumberVerificationStatusDate;
                            }
                            else
                            {
                                _HCUAccount.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                            }
                            _HCUAccounts.Add(_HCUAccount);

                            _HCUAccountAuth = new HCUAccountAuth();
                            _HCUAccountAuth.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccountAuth.Username = _Request.UserName;
                            _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                            _HCUAccountAuth.SecondaryPassword = HCoreEncrypt.EncryptHash(_Request.SecondaryPassword);
                            _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());

                            _HCUAccountAuth.CreateDate = HCoreHelper.GetGMTDateTime();
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            }
                            _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                            _HCUAccountAuth.HCUAccount = _HCUAccounts;
                            _HCoreContext.HCUAccountAuth.Add(_HCUAccountAuth);
                            _HCoreContext.SaveChanges();
                            _Request.ReferenceId = _HCUAccount.Id;
                            long? IconStorageId = null;
                            long? PosterStorageId = null;
                            if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                            }
                            if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                            {
                                PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                            }
                            if (IconStorageId != null || PosterStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                    if (UserAccDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            UserAccDetails.IconStorageId = IconStorageId;
                                        }
                                        if (PosterStorageId != null)
                                        {
                                            UserAccDetails.PosterStorageId = PosterStorageId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }

                            if (_HCUAccount.AccountTypeId != UserAccountType.Appuser)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    OHCoreDataStore.Account Account = (from x in _HCoreContext.HCUAccount
                                                                       where x.Id == _HCUAccount.Id && (x.AccountTypeId == Helpers.UserAccountType.Acquirer
                                                                      || x.AccountTypeId == Helpers.UserAccountType.PgAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.PosAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                                      || x.AccountTypeId == Helpers.UserAccountType.Merchant
                                                                      //|| x.AccountTypeId == Helpers.UserAccountType.TerminalAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.MerchantCashier
                                                                      || x.AccountTypeId == Helpers.UserAccountType.AcquirerSubAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.MerchantSubAccount
                                                                      || x.AccountTypeId == Helpers.UserAccountType.Controller
                                                                      || x.AccountTypeId == Helpers.UserAccountType.Admin)
                                                                       select new OHCoreDataStore.Account
                                                                       {
                                                                           ReferenceId = x.Id,
                                                                           ReferenceKey = x.Guid,

                                                                           UserName = x.User.Username,

                                                                           DisplayName = x.DisplayName,
                                                                           Name = x.Name,
                                                                           FirstName = x.FirstName,
                                                                           LastName = x.LastName,

                                                                           EmailAddress = x.EmailAddress,
                                                                           SecondaryEmailAddress = x.SecondaryEmailAddress,

                                                                           ContactNumber = x.ContactNumber,
                                                                           MobileNumber = x.MobileNumber,

                                                                           Address = x.Address,
                                                                           Latitude = x.Latitude,
                                                                           Longitude = x.Longitude,

                                                                           IconUrl = x.IconStorage.Path,
                                                                           PosterUrl = x.PosterStorage.Path,

                                                                           Description = x.Description,


                                                                           AccountTypeId = x.AccountTypeId,
                                                                           AccountTypeCode = x.AccountType.SystemName,
                                                                           AccountTypeName = x.AccountType.Name,

                                                                           RoleId = x.RoleId,
                                                                           RoleKey = x.Role.Guid,
                                                                           RoleName = x.Role.Name,


                                                                           LastLoginDate = x.LastLoginDate,
                                                                           LastActivityDate = null,

                                                                           LastTransactionDate = x.LastTransactionDate,

                                                                           CreateDate = x.CreateDate,
                                                                           CreatedById = x.CreatedById,
                                                                           CreatedByKey = x.CreatedBy.Guid,
                                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                           CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                                           CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                                                           CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                                                           ModifyDate = x.ModifyDate,
                                                                           ModifyById = x.ModifyById,
                                                                           ModifyByKey = x.ModifyBy.Guid,
                                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                           ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                                                           ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                                                           ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                                                           StatusId = x.StatusId,
                                                                           StatusCode = x.Status.SystemName,
                                                                           StatusName = x.Status.Name,

                                                                           OwnerId = x.OwnerId,
                                                                           OwnerKey = x.Owner.Guid,
                                                                           OwnerDisplayName = x.Owner.DisplayName,
                                                                           OwnerIconUrl = x.Owner.IconStorage.Path,

                                                                           SubOwnerId = x.SubOwnerId,
                                                                           SubOwnerKey = x.SubOwner.Guid,
                                                                           SubOwnerDisplayName = x.SubOwner.DisplayName,
                                                                           SubOwnerIconUrl = x.SubOwner.IconStorage.Path,

                                                                           BankId = x.BankId,
                                                                           BankKey = x.Bank.Guid,
                                                                           BankDisplayName = x.Bank.DisplayName,
                                                                           BankIconUrl = x.Bank.IconStorage.Path,

                                                                           MerchantsCount = 0,
                                                                           StoresCount = 0,
                                                                           AcquirersCount = 0,
                                                                           TerminalsCount = 0,
                                                                           CashiersCount = 0,
                                                                           PgAccountsCount = 0,
                                                                           PosAccountsCount = 0,
                                                                           SubAccountsCount = 0,
                                                                       }).FirstOrDefault();
                                    if (Account != null)
                                    {
                                        //if (_HCUAccount.AccountTypeId == UserAccountType.TerminalAccount)
                                        //{
                                        //    #region Terminals
                                        //    var Terminal = (from x in _HCoreContext.HCUAccount
                                        //                    where x.Guid == _Request.ReferenceKey && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.TerminalAccount
                                        //                    select new OHCoreDataStore.Terminal
                                        //                    {
                                        //                        ReferenceId = x.Id,
                                        //                        ReferenceKey = x.Guid,
                                        //                        DisplayName = x.DisplayName,
                                        //                        ProviderId = x.OwnerId,
                                        //                        MerchantId = x.SubOwner.OwnerId,
                                        //                        StoreId = x.SubOwnerId,
                                        //                        AcquirerId = x.BankId,

                                        //                        ApplicationStatusId = x.ApplicationStatusId,
                                        //                        LastTransactionDate = x.LastTransactionDate,

                                        //                        CreateDate = x.CreateDate,
                                        //                        CreatedById = x.CreatedById,
                                        //                        CreatedByKey = x.CreatedBy.Guid,
                                        //                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                        //                        CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                        //                        CreatedByAccountTypeCode = x.CreatedBy.AccountType.SystemName,
                                        //                        CreatedByAccountTypeName = x.CreatedBy.AccountType.Name,

                                        //                        ModifyDate = x.ModifyDate,
                                        //                        ModifyById = x.ModifyById,
                                        //                        ModifyByKey = x.ModifyBy.Guid,
                                        //                        ModifyByDisplayName = x.ModifyBy.DisplayName,

                                        //                        ModifyByAccountTypeId = x.ModifyBy.AccountTypeId,
                                        //                        ModifyByAccountTypeCode = x.ModifyBy.AccountType.SystemName,
                                        //                        ModifyByAccountTypeName = x.ModifyBy.AccountType.Name,

                                        //                        StatusId = x.StatusId,
                                        //                        StatusCode = x.Status.SystemName,
                                        //                        StatusName = x.Status.Name,

                                        //                    }).FirstOrDefault();

                                        //    if (Terminal != null)
                                        //    {
                                        //        #region Accounts Data Update
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.Merchant)
                                        //        {
                                        //            Account.StoresCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore);
                                        //            Account.CashiersCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantCashier);
                                        //            Account.SubAccountsCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantSubAccount);
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        //            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        //        }
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.Acquirer)
                                        //        {
                                        //            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        //            Account.SubAccountsCount = HCoreDataStore.Accounts.Where(x => x.OwnerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        //            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        //        }
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.PosAccount)
                                        //        {
                                        //            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        //            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        //        }
                                        //        if (Account.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                                        //        {
                                        //            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                        //            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        //            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.IconUrl))
                                        //        {
                                        //            Account.IconUrl = _AppConfig.StorageUrl + Account.IconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.IconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.OwnerIconUrl))
                                        //        {
                                        //            Account.OwnerIconUrl = _AppConfig.StorageUrl + Account.OwnerIconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.OwnerIconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.SubOwnerIconUrl))
                                        //        {
                                        //            Account.SubOwnerIconUrl = _AppConfig.StorageUrl + Account.SubOwnerIconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.SubOwnerIconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        if (!string.IsNullOrEmpty(Account.BankIconUrl))
                                        //        {
                                        //            Account.BankIconUrl = _AppConfig.StorageUrl + Account.BankIconUrl;
                                        //        }
                                        //        else
                                        //        {
                                        //            Account.BankIconUrl = _AppConfig.Default_Icon;
                                        //        }
                                        //        #endregion
                                        //        #region Terminals Data Update
                                        //        Terminal.RmId = _HCoreContext.HCUAccountOwner.Where(x => x.OwnerId == Terminal.StoreId
                                        //        && x.Account.OwnerId == Terminal.AcquirerId && x.StatusId == HelperStatus.Default.Active
                                        //        && x.Account.AccountTypeId == HCoreConstant.Helpers.UserAccountType.RelationshipManager).Select(x => x.AccountId).FirstOrDefault();
                                        //        Terminal.LastTransactionDate = _HCoreContext.HCUAccountTransaction.Where(x => x.CreatedById == Terminal.ReferenceId).OrderByDescending(x => x.Id).Select(x => x.TransactionDate).FirstOrDefault();
                                        //        var ProviderDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.ProviderId).FirstOrDefault();
                                        //        if (ProviderDetails != null)
                                        //        {
                                        //            Terminal.ProviderKey = ProviderDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(ProviderDetails.IconUrl))
                                        //            {
                                        //                Terminal.ProviderIconUrl = ProviderDetails.IconUrl;
                                        //            }
                                        //            Terminal.ProviderDisplayName = ProviderDetails.DisplayName;
                                        //        }

                                        //        var MerchantDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.MerchantId).FirstOrDefault();
                                        //        if (MerchantDetails != null)
                                        //        {
                                        //            Terminal.MerchantKey = MerchantDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(MerchantDetails.IconUrl))
                                        //            {
                                        //                Terminal.MerchantIconUrl = MerchantDetails.IconUrl;
                                        //            }
                                        //            Terminal.MerchantDisplayName = MerchantDetails.DisplayName;
                                        //        }

                                        //        var StoreDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.StoreId).FirstOrDefault();
                                        //        if (StoreDetails != null)
                                        //        {
                                        //            Terminal.StoreKey = StoreDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(StoreDetails.IconUrl))
                                        //            {
                                        //                Terminal.StoreIconUrl = StoreDetails.IconUrl;
                                        //            }
                                        //            Terminal.StoreDisplayName = StoreDetails.DisplayName;
                                        //        }
                                        //        var AcquirerDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.AcquirerId).FirstOrDefault();
                                        //        if (AcquirerDetails != null)
                                        //        {
                                        //            Terminal.AcquirerKey = AcquirerDetails.ReferenceKey;
                                        //            if (!string.IsNullOrEmpty(AcquirerDetails.IconUrl))
                                        //            {
                                        //                Terminal.AcquirerIconUrl = AcquirerDetails.IconUrl;
                                        //            }
                                        //            Terminal.AcquirerDisplayName = AcquirerDetails.DisplayName;
                                        //        }
                                        //        var RmDetails = HCoreDataStore.Accounts.Where(x => x.ReferenceId == Terminal.RmId).FirstOrDefault();
                                        //        if (RmDetails != null)
                                        //        {
                                        //            Terminal.RmKey = RmDetails.ReferenceKey;
                                        //            Terminal.RmDisplayName = RmDetails.DisplayName;
                                        //        }
                                        //        #endregion
                                        //        HCoreDataStore.Accounts.Add(Account);
                                        //        HCoreDataStore.Terminals.Add(Terminal);
                                        //    }
                                        //    #endregion
                                        //    _HCoreContext.Dispose();
                                        //}
                                        //else
                                        //{
                                        #region Accounts Data Update
                                        if (Account.AccountTypeId == Helpers.UserAccountType.Merchant)
                                        {
                                            Account.StoresCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantStore);
                                            Account.CashiersCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantCashier);
                                            Account.SubAccountsCount = HCoreDataStore.Accounts.Count(x => x.OwnerId == Account.ReferenceId && x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.MerchantSubAccount);
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.MerchantId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        }
                                        if (Account.AccountTypeId == Helpers.UserAccountType.Acquirer)
                                        {
                                            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                            Account.SubAccountsCount = HCoreDataStore.Accounts.Where(x => x.OwnerId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.AcquirerId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                        }
                                        if (Account.AccountTypeId == Helpers.UserAccountType.PosAccount)
                                        {
                                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                            Account.MerchantsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.StoresCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.StoreId).Distinct().Count();
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.ProviderId == Account.ReferenceId).Select(x => x.ReferenceId).Count();
                                        }
                                        if (Account.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                                        {
                                            Account.TerminalsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.MerchantId).Distinct().Count();
                                            Account.PosAccountsCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.ProviderId).Distinct().Count();
                                            Account.AcquirersCount = HCoreDataStore.Terminals.Where(x => x.StoreId == Account.ReferenceId).Select(x => x.AcquirerId).Distinct().Count();
                                        }
                                        if (!string.IsNullOrEmpty(Account.IconUrl))
                                        {
                                            Account.IconUrl = _AppConfig.StorageUrl + Account.IconUrl;
                                        }
                                        else
                                        {
                                            Account.IconUrl = _AppConfig.Default_Icon;
                                        }
                                        if (!string.IsNullOrEmpty(Account.OwnerIconUrl))
                                        {
                                            Account.OwnerIconUrl = _AppConfig.StorageUrl + Account.OwnerIconUrl;
                                        }
                                        else
                                        {
                                            Account.OwnerIconUrl = _AppConfig.Default_Icon;
                                        }
                                        if (!string.IsNullOrEmpty(Account.SubOwnerIconUrl))
                                        {
                                            Account.SubOwnerIconUrl = _AppConfig.StorageUrl + Account.SubOwnerIconUrl;
                                        }
                                        else
                                        {
                                            Account.SubOwnerIconUrl = _AppConfig.Default_Icon;
                                        }
                                        if (!string.IsNullOrEmpty(Account.BankIconUrl))
                                        {
                                            Account.BankIconUrl = _AppConfig.StorageUrl + Account.BankIconUrl;
                                        }
                                        else
                                        {
                                            Account.BankIconUrl = _AppConfig.Default_Icon;
                                        }
                                        #endregion

                                        HCoreDataStore.Accounts.Add(Account);
                                        _HCoreContext.Dispose();
                                        //}
                                    }
                                    else
                                    {
                                        _HCoreContext.Dispose();
                                    }
                                }
                            }
                            _Request.UserReference = null;
                            #region Send Response
                            return _Request;
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return null;
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("SaveUserAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return null;
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description:  Updates the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserAccount(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else
                {
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    int? AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                    long? AccountOperationTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountOperationTypeCode, _Request.UserReference);
                    long? OwnerId = HCoreHelper.GetUserAccountId(_Request.OwnerKey, _Request.UserReference);
                    int? GenderId = HCoreHelper.GetSystemHelperId(_Request.GenderCode, HelperType.Gender, _Request.UserReference);


                    int? CountryId = HCoreHelper.GetCountryId(_Request.CountryKey, _Request.UserReference);
                    long? RegionId = HCoreHelper.GetCoreParameterId(_Request.RegionKey, _Request.UserReference);
                    long? RegionAreaId = HCoreHelper.GetCoreParameterId(_Request.RegionAreaKey, _Request.UserReference);
                    long? CityId = HCoreHelper.GetCoreParameterId(_Request.CityKey, _Request.UserReference);
                    long? CityAreaId = HCoreHelper.GetCoreParameterId(_Request.CityAreaKey, _Request.UserReference);
                    long? RoleId = HCoreHelper.GetCoreParameterId(_Request.RoleKey, _Request.UserReference);
                    long? SubscriptionId = HCoreHelper.GetCoreCommonId(_Request.SubscriptionKey, _Request.UserReference);
                    int? AccountLevelId = HCoreHelper.GetSystemHelperId(_Request.AccountLevelCode, _Request.UserReference);
                    int? ApplicationStatusId = HCoreHelper.GetSystemHelperId(_Request.ApplicationStatusCode, _Request.UserReference);
                    OAddressResponse Location = HCoreHelper.GetAddressComponent(_Request.Location, _Request.UserReference);
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserAccountDetails = (from n in _HCoreContext.HCUAccount
                                                         select n).Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (_Request.UserReference.AccountTypeId == Helpers.UserAccountType.Appuser)
                        {
                            UserAccountDetails = (from n in _HCoreContext.HCUAccount
                                                  select n).Where(x => x.Id == _Request.UserReference.AccountId).FirstOrDefault();
                        }
                        if (UserAccountDetails != null)
                        {
                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                          where n.Id == UserAccountDetails.UserId
                                                          select n).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                if (!string.IsNullOrEmpty(_Request.UserName) && UserDetails.Username != _Request.UserName)
                                {
                                    var CheckUserName = _HCoreContext.HCUAccountAuth.Where(x => x.Username == _Request.UserName).Select(x => x.Id).FirstOrDefault();
                                    if (CheckUserName == 0)
                                    {
                                        UserDetails.Username = _Request.UserName;
                                    }
                                    else
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1098", "Account already exists");
                                        #endregion
                                    }
                                }
                                long? OldIconStorageId = UserAccountDetails.IconStorageId;
                                long? OldPosterStorageId = UserAccountDetails.PosterStorageId;

                                #region Update user account details
                                if (AccountTypeId != null && UserAccountDetails.AccountTypeId != AccountTypeId)
                                {
                                    UserAccountDetails.AccountTypeId = (int)AccountTypeId;
                                }
                                if (AccountOperationTypeId != null && UserAccountDetails.AccountOperationTypeId != AccountOperationTypeId)
                                {
                                    UserAccountDetails.AccountOperationTypeId = (int)AccountOperationTypeId;
                                }
                                if (OwnerId != null && UserAccountDetails.OwnerId != OwnerId)
                                {
                                    UserAccountDetails.OwnerId = OwnerId;
                                }
                                if (RoleId != null && UserAccountDetails.RoleId != RoleId)
                                {
                                    UserAccountDetails.RoleId = RoleId;
                                }
                                if (!string.IsNullOrEmpty(_Request.DisplayName) && UserAccountDetails.DisplayName != _Request.DisplayName)
                                {
                                    UserAccountDetails.DisplayName = _Request.DisplayName;
                                }
                                if (!string.IsNullOrEmpty(_Request.OwnerName) && UserAccountDetails.FirstName != _Request.OwnerName)
                                {
                                    UserAccountDetails.FirstName = _Request.OwnerName;
                                }
                                if (!string.IsNullOrEmpty(_Request.MobileNumber) && UserAccountDetails.DisplayName != _Request.MobileNumber)
                                {
                                    UserAccountDetails.MobileNumber = _Request.MobileNumber;
                                }
                                if (!string.IsNullOrEmpty(_Request.ContactNumber) && UserAccountDetails.ContactNumber != _Request.ContactNumber)
                                {
                                    UserAccountDetails.ContactNumber = _Request.ContactNumber;
                                }
                                if (!string.IsNullOrEmpty(_Request.AccessPin))
                                {
                                    UserAccountDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                }
                                if (!string.IsNullOrEmpty(_Request.ReferralCode) && UserAccountDetails.ReferralCode != _Request.ReferralCode)
                                {
                                    UserAccountDetails.ReferralCode = _Request.ReferralCode;
                                }
                                if (_Request.AccountPercentage > 0 && UserAccountDetails.AccountPercentage != _Request.AccountPercentage)
                                {
                                    UserAccountDetails.AccountPercentage = _Request.AccountPercentage;
                                }
                                if (!string.IsNullOrEmpty(_Request.ReferralUrl) && UserAccountDetails.ReferralUrl != _Request.ReferralUrl)
                                {
                                    UserAccountDetails.ReferralUrl = _Request.ReferralUrl;
                                }
                                if (!string.IsNullOrEmpty(_Request.Description) && UserAccountDetails.Description != _Request.Description)
                                {
                                    UserAccountDetails.Description = _Request.Description;
                                }
                                if (StatusId != null && UserAccountDetails.StatusId != StatusId)
                                {
                                    UserAccountDetails.StatusId = (int)StatusId;
                                }
                                if (AccountLevelId != null && UserAccountDetails.AccountLevel != AccountLevelId)
                                {
                                    UserAccountDetails.AccountLevel = AccountLevelId;
                                    _HCUAccountParameter = new HCUAccountParameter();
                                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountParameter.AccountId = UserAccountDetails.Id;
                                    _HCUAccountParameter.TypeId = HelperType.AccountLevel;
                                    _HCUAccountParameter.HelperId = AccountLevelId;
                                    _HCUAccountParameter.Name = _Request.Comment;
                                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                }
                                if (SubscriptionId != null && UserAccountDetails.SubscriptionId != SubscriptionId)
                                {
                                    UserAccountDetails.SubscriptionId = SubscriptionId;
                                }
                                if (ApplicationStatusId != null && UserAccountDetails.ApplicationStatusId != ApplicationStatusId)
                                {
                                    UserAccountDetails.ApplicationStatusId = ApplicationStatusId;
                                    _HCUAccountParameter = new HCUAccountParameter();
                                    _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountParameter.AccountId = UserAccountDetails.Id;
                                    _HCUAccountParameter.TypeId = HelperType.AccountFlag;
                                    _HCUAccountParameter.HelperId = ApplicationStatusId;
                                    _HCUAccountParameter.Name = _Request.Comment;
                                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                    _HCUAccountParameter.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    //_HCUAccountParameter.TypeId = HCoreConstant.HelperType
                                }
                                UserAccountDetails.ModifyById = _Request.UserReference.AccountId;
                                UserAccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                #endregion

                                #region Update User Information
                                if (!string.IsNullOrEmpty(_Request.Password))
                                {
                                    UserDetails.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                                    UserDetails.SystemPassword = HCoreEncrypt.EncryptHash(_Request.Password);
                                }
                                if (!string.IsNullOrEmpty(_Request.SecondaryPassword))
                                {
                                    UserDetails.SecondaryPassword = HCoreEncrypt.EncryptHash(_Request.SecondaryPassword);
                                }
                                if (!string.IsNullOrEmpty(_Request.DisplayName) && UserAccountDetails.DisplayName != _Request.DisplayName)
                                {
                                    UserAccountDetails.DisplayName = _Request.DisplayName;
                                }
                                if (!string.IsNullOrEmpty(_Request.Name) && UserAccountDetails.Name != _Request.Name)
                                {
                                    UserAccountDetails.Name = _Request.Name;
                                }
                                if (!string.IsNullOrEmpty(_Request.FirstName) && UserAccountDetails.FirstName != _Request.FirstName)
                                {
                                    UserAccountDetails.FirstName = _Request.FirstName;
                                }
                                if (!string.IsNullOrEmpty(_Request.LastName) && UserAccountDetails.LastName != _Request.LastName)
                                {
                                    UserAccountDetails.LastName = _Request.LastName;
                                }
                                if (!string.IsNullOrEmpty(_Request.MobileNumber) && UserAccountDetails.MobileNumber != _Request.MobileNumber)
                                {
                                    UserAccountDetails.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber, 10);
                                    UserAccountDetails.MobileNumber = UserAccountDetails.MobileNumber;
                                    UserAccountDetails.NumberVerificationStatus = _Request.NumberVerificationStatus;
                                    if (_Request.NumberVerificationStatusDate != null)
                                    {
                                        UserAccountDetails.NumberVerificationStatusDate = _Request.NumberVerificationStatusDate;
                                    }
                                    else
                                    {
                                        UserAccountDetails.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                    }
                                }
                                if (!string.IsNullOrEmpty(_Request.ContactNumber) && UserAccountDetails.ContactNumber != _Request.ContactNumber)
                                {
                                    UserAccountDetails.ContactNumber = _Request.ContactNumber;
                                }
                                UserAccountDetails.SecondaryEmailAddress = _Request.SecondaryEmailAddress;
                                if (!string.IsNullOrEmpty(_Request.EmailAddress) && UserAccountDetails.EmailAddress != _Request.EmailAddress)
                                {
                                    UserAccountDetails.EmailAddress = _Request.EmailAddress;

                                    UserAccountDetails.EmailVerificationStatus = _Request.EmailVerificationStatus;
                                    if (_Request.EmailVerificationStatusDate != null)
                                    {
                                        UserAccountDetails.EmailVerificationStatusDate = _Request.EmailVerificationStatusDate;
                                    }
                                    else
                                    {
                                        UserAccountDetails.EmailVerificationStatusDate = HCoreHelper.GetGMTDateTime();
                                    }
                                }
                                if (GenderId != null && UserAccountDetails.GenderId != GenderId)
                                {
                                    UserAccountDetails.GenderId = GenderId;
                                }
                                if (_Request.DateOfBirth != null && _Request.DateOfBirth != UserAccountDetails.DateOfBirth)
                                {
                                    UserAccountDetails.DateOfBirth = _Request.DateOfBirth;
                                }



                                if (!string.IsNullOrEmpty(_Request.WebsiteUrl) && UserAccountDetails.WebsiteUrl != _Request.WebsiteUrl)
                                {
                                    UserAccountDetails.WebsiteUrl = _Request.WebsiteUrl;
                                }

                                #region Address Manager
                                if (_Request.CountryId != null && UserAccountDetails.CountryId != _Request.CountryId)
                                {
                                    UserAccountDetails.CountryId = _Request.CountryId;
                                }
                                if (_Request.StateId != null && UserAccountDetails.StateId != _Request.StateId)
                                {
                                    UserAccountDetails.StateId = _Request.StateId;
                                }
                                if (_Request.CityId != null && UserAccountDetails.CityId != _Request.CityId)
                                {
                                    UserAccountDetails.CityId = _Request.CityId;
                                }
                                if (_Request.CityAreaId != null && UserAccountDetails.CityAreaId != _Request.CityAreaId)
                                {
                                    UserAccountDetails.CityAreaId = _Request.CityAreaId;
                                }
                                if (_Request.Latitude != 0 && UserAccountDetails.Latitude != _Request.Latitude)
                                {
                                    UserAccountDetails.Latitude = _Request.Latitude;
                                }
                                if (_Request.Longitude != 0 && UserAccountDetails.Longitude != _Request.Longitude)
                                {
                                    UserAccountDetails.Longitude = _Request.Longitude;
                                }
                                if (!string.IsNullOrEmpty(_Request.Address) && UserAccountDetails.Address != _Request.Address)
                                {
                                    UserAccountDetails.Address = _Request.Address;
                                }
                                if (_Request.Location != null && Location != null)
                                {
                                    if (Location.CountryId > 0 && UserAccountDetails.CountryId != Location.CountryId)
                                    {
                                        UserAccountDetails.CountryId = Location.CountryId;
                                    }
                                    if (Location.StateId > 0 && UserAccountDetails.StateId != Location.StateId)
                                    {
                                        UserAccountDetails.StateId = Location.StateId;
                                    }
                                    if (Location.CityId > 0 && UserAccountDetails.CityId != Location.CityId)
                                    {
                                        UserAccountDetails.CityId = Location.CityId;
                                    }
                                    if (Location.CityAreaId > 0 && UserAccountDetails.CityAreaId != Location.CityAreaId)
                                    {
                                        UserAccountDetails.CityAreaId = Location.CityAreaId;
                                    }
                                    if (Location.Latitude != 0 && UserAccountDetails.Latitude != Location.Latitude)
                                    {
                                        UserAccountDetails.Latitude = Location.Latitude;
                                    }
                                    if (Location.Longitude != 0 && UserAccountDetails.Longitude != Location.Longitude)
                                    {
                                        UserAccountDetails.Longitude = Location.Longitude;
                                    }
                                    if (!string.IsNullOrEmpty(Location.Address) && UserAccountDetails.Address != Location.Address)
                                    {
                                        UserAccountDetails.Address = Location.Address;
                                    }
                                }
                                #endregion

                                UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                UserDetails.ModifyById = _Request.UserReference.AccountId;
                                if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                                {
                                    UserAccountDetails.IconStorageId = null;
                                }
                                if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                                {
                                    UserAccountDetails.PosterStorageId = null;
                                }
                                #endregion
                                #region Save Changes
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #endregion

                                long? IconStorageId = null;
                                long? PosterStorageId = null;
                                if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                                {
                                    IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, OldIconStorageId, _Request.UserReference);
                                }
                                if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                                {
                                    PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, OldPosterStorageId, _Request.UserReference);
                                }
                                if (IconStorageId != null || PosterStorageId != null)
                                {
                                    using (_HCoreContext = new HCoreContext())
                                    {
                                        var UserAccDetails = _HCoreContext.HCUAccount.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                        if (UserAccDetails != null)
                                        {
                                            if (IconStorageId != null)
                                            {
                                                UserAccDetails.IconStorageId = IconStorageId;
                                            }
                                            else if (IconStorageId == null)
                                            {
                                                UserAccDetails.IconStorageId = null;
                                            }

                                            if (PosterStorageId != null)
                                            {
                                                UserAccDetails.PosterStorageId = PosterStorageId;
                                            }
                                            else if (PosterStorageId == null)
                                            {
                                                UserAccDetails.PosterStorageId = null;
                                            }
                                            _HCoreContext.SaveChanges();
                                        }
                                    }
                                }

                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1097");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1099");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1101");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1102");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description:  Updates the user password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserPassword(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094", "Reference key required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.OldPassword))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094", "Old password required");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Password))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094", "New password required");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserAccountDetails = (from n in _HCoreContext.HCUAccount
                                                         select n).Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (UserAccountDetails != null)
                        {
                            HCUAccountAuth UserDetails = (from n in _HCoreContext.HCUAccountAuth
                                                          where n.Id == UserAccountDetails.UserId
                                                          select n).FirstOrDefault();
                            if (UserDetails != null)
                            {
                                string UserOldPassword = HCoreEncrypt.DecryptHash(UserDetails.Password);
                                if (UserOldPassword == _Request.OldPassword)
                                {
                                    #region Update user account details
                                    UserAccountDetails.ModifyById = _Request.UserReference.AccountId;
                                    UserAccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    #endregion
                                    #region Update User Information
                                    UserDetails.Password = HCoreEncrypt.EncryptHash(_Request.Password);
                                    UserDetails.SystemPassword = HCoreEncrypt.EncryptHash(_Request.Password);
                                    UserDetails.SecondaryPassword = HCoreEncrypt.EncryptHash(_Request.Password);
                                    UserDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserDetails.ModifyById = _Request.UserReference.AccountId;
                                    #endregion
                                    #region Save Changes
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1097", "Password updated successfully");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094", "Worng old password entered");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1099", "Invalid user details");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1101", "Invalid user details");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserPassword", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1102", "Unable to process your request.");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description:  Updates the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserAccessPin(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.OldAccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.AccessPin))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserAccountDetails = (from n in _HCoreContext.HCUAccount
                                                         select n).Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (UserAccountDetails != null)
                        {
                            if (!string.IsNullOrEmpty(UserAccountDetails.AccessPin))
                            {
                                string UserOldAccessPin = HCoreEncrypt.DecryptHash(UserAccountDetails.AccessPin);
                                if (UserOldAccessPin == _Request.OldAccessPin)
                                {
                                    #region Update user account details
                                    UserAccountDetails.ModifyById = _Request.UserReference.AccountId;
                                    UserAccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    UserAccountDetails.AccessPin = HCoreEncrypt.EncryptHash(_Request.AccessPin);
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    if (!string.IsNullOrEmpty(UserAccountDetails.MobileNumber))
                                    {
                                        HCoreHelper.SendSMS(SmsType.Transaction, "234", UserAccountDetails.MobileNumber, "Your Thank U Cash Pin. To redeem your cash, use pin: " + _Request.AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", UserAccountDetails.Id, null);
                                    }
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1097");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                                    #endregion
                                }
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1101");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccessPin", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1102");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Resets the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ResetUserAccessPin(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1094");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccount UserAccountDetails = (from n in _HCoreContext.HCUAccount
                                                         select n).Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (UserAccountDetails != null)
                        {
                            string NewAccessPin = HCoreHelper.GenerateRandomNumber(4);
                            #region Update user account details
                            UserAccountDetails.ModifyById = _Request.UserReference.AccountId;
                            UserAccountDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            UserAccountDetails.AccessPin = HCoreEncrypt.EncryptHash(NewAccessPin);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #endregion
                            if (!string.IsNullOrEmpty(UserAccountDetails.MobileNumber))
                            {
                                HCoreHelper.SendSMS(SmsType.Transaction, "234", UserAccountDetails.MobileNumber, "Your Thank U Cash Pin. To redeem your cash, use pin: " + NewAccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", UserAccountDetails.Id, null);
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1097");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1101");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("UpdateUserAccessPin", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1102");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                long? ParentId = HCoreHelper.GetUserParameterId(_Request.ParentKey, _Request.UserReference);
                long? SubParentId = HCoreHelper.GetUserParameterId(_Request.SubParentKey, _Request.UserReference);
                long? UserId = HCoreHelper.GetUserId(_Request.UserKey, _Request.UserReference);
                long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                long? UserDeviceId = HCoreHelper.GetUserDeviceId(_Request.UserDeviceKey, _Request.UserReference);
                long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                long? ParameterId = HCoreHelper.GetCoreParameterId(_Request.ParameterKey, _Request.UserReference);
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else if (TypeId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else if (UserAccountId == null && UserId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCR005");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        _HCUAccountParameter = new HCUAccountParameter();
                        _HCUAccountParameter.Guid = HCoreHelper.GenerateGuid();
                        _HCUAccountParameter.TypeId = (int)TypeId;

                        _HCUAccountParameter.ParentId = ParentId;
                        _HCUAccountParameter.SubParentId = SubParentId;

                        //_HCUAccountParameter.UserId = UserId;
                        _HCUAccountParameter.AccountId = UserAccountId;
                        _HCUAccountParameter.DeviceId = UserDeviceId;
                        _HCUAccountParameter.CommonId = CommonId;

                        _HCUAccountParameter.ParameterId = ParameterId;
                        _HCUAccountParameter.HelperId = HelperId;
                        _HCUAccountParameter.Name = _Request.Name;

                        if (string.IsNullOrEmpty(_Request.SystemName))
                        {
                            _HCUAccountParameter.SystemName = HCoreHelper.GenerateSystemName(_Request.SystemName);
                        }
                        else
                        {
                            _HCUAccountParameter.SystemName = _Request.SystemName;
                        }
                        _HCUAccountParameter.Value = _Request.Value;
                        _HCUAccountParameter.SubValue = _Request.SubValue;
                        _HCUAccountParameter.Description = _Request.Description;
                        _HCUAccountParameter.Data = _Request.Data;
                        if (_Request.StartTime != null)
                        {
                            _HCUAccountParameter.StartTime = _Request.StartTime;
                        }
                        if (_Request.EndTime != null)
                        {
                            _HCUAccountParameter.EndTime = _Request.EndTime;
                        }
                        if (_Request.CountValue != null)
                        {
                            _HCUAccountParameter.CountValue = _Request.CountValue;
                        }
                        if (_Request.AverageValue != null)
                        {
                            _HCUAccountParameter.AverageValue = _Request.AverageValue;
                        }
                        _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                        _HCUAccountParameter.StatusId = (int)StatusId;
                        _HCUAccountParameter.RequestKey = _Request.UserReference.RequestKey;
                        _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                        HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                        _HCoreContext.SaveChanges();
                        _Request.ReferenceKey = _HCUAccountParameter.Guid;
                        long RId = _HCUAccountParameter.Id;

                        if (_Request.TypeCode == "hcore.userreview")
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                long? star5 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 5).Select(x => x.CountValue).Count();
                                long? star4 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 4).Select(x => x.CountValue).Count();
                                long? star3 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 3).Select(x => x.CountValue).Count();
                                long? star2 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 4).Select(x => x.CountValue).Count();
                                long? star1 = _HCoreContext.HCUAccountParameter.Where(c => c.AccountId == UserAccountId && c.CountValue != null && c.CountValue == 1).Select(x => x.CountValue).Count();
                                double rating = (double?)(5 * star5 + 4 * star4 + 3 * star3 + 2 * star2 + 1 * star1) / (star1 + star2 + star3 + star4 + star5) ?? 0;
                                double? Average = Math.Round(rating, 1);
                                //double? Average = _HCoreContext.HCUAccountParameter
                                //.Where(c => c.AccountId == UserAccountId && c.CountValue != null).Select(x => (double?)x.CountValue).Average();
                                var Ratings = _HCoreContext.HCUAccountParameter
                                                           .Where(c => c.AccountId == UserAccountId && c.CountValue != null).Count();
                                var UserAccount = _HCoreContext.HCUAccount.Where(x => x.Id == UserAccountId).FirstOrDefault();
                                if (UserAccount != null)
                                {
                                    UserAccount.AverageValue = Average;
                                    UserAccount.CountValue = Ratings;
                                    UserAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                }
                                var UserParameter = _HCoreContext.HCUAccountParameter.Where(x => x.Id == RId).FirstOrDefault();
                                if (UserParameter != null)
                                {
                                    UserParameter.AverageValue = Average;
                                    UserParameter.ModifyDate = HCoreHelper.GetGMTDateTime();
                                }
                                _HCoreContext.SaveChanges();
                            }

                        }

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description:  Updates the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    int? TypeId = HCoreHelper.GetSystemHelperId(_Request.TypeCode, _Request.UserReference);
                    long? ParentId = HCoreHelper.GetUserParameterId(_Request.ParentKey, _Request.UserReference);
                    long? SubParentId = HCoreHelper.GetUserParameterId(_Request.SubParentKey, _Request.UserReference);
                    long? UserId = HCoreHelper.GetUserId(_Request.UserKey, _Request.UserReference);
                    long? UserAccountId = HCoreHelper.GetUserAccountId(_Request.UserAccountKey, _Request.UserReference);
                    long? UserDeviceId = HCoreHelper.GetUserDeviceId(_Request.UserDeviceKey, _Request.UserReference);
                    long? CommonId = HCoreHelper.GetCoreCommonId(_Request.CommonKey, _Request.UserReference);
                    int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                    long? ParameterId = HCoreHelper.GetCoreParameterId(_Request.ParameterKey, _Request.UserReference);
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountParameter Details = _HCoreContext.HCUAccountParameter
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            long? OldIconStorageId = Details.IconStorageId;
                            long? OldPosterStorageId = Details.PosterStorageId;
                            if (TypeId != null)
                            {
                                Details.TypeId = (int)TypeId;
                            }
                            if (ParentId != null)
                            {
                                Details.ParentId = ParentId;
                            }
                            if (SubParentId != null)
                            {
                                Details.SubParentId = SubParentId;
                            }
                            if (UserAccountId != null)
                            {
                                Details.AccountId = UserAccountId;
                            }

                            if (UserDeviceId != null)
                            {
                                Details.DeviceId = UserDeviceId;
                            }

                            if (CommonId != null)
                            {
                                Details.CommonId = CommonId;
                            }
                            if (ParameterId != null)
                            {
                                Details.ParameterId = ParameterId;
                            }

                            if (HelperId != null)
                            {
                                Details.HelperId = HelperId;
                            }
                            if (!string.IsNullOrEmpty(_Request.Name))
                            {
                                Details.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName) && Details.SystemName != _Request.SystemName)
                            {
                                Details.SystemName = _Request.SystemName;
                            }
                            if (!string.IsNullOrEmpty(_Request.Value) && Details.Value != _Request.Value)
                            {
                                Details.Value = _Request.Value;
                            }
                            if (!string.IsNullOrEmpty(_Request.SubValue) && Details.SubValue != _Request.SubValue)
                            {
                                Details.SubValue = _Request.SubValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.Description) && Details.Description != _Request.Description)
                            {
                                Details.Description = _Request.Description;
                            }
                            if (!string.IsNullOrEmpty(_Request.Data) && Details.Data != _Request.Data)
                            {
                                Details.Data = _Request.Data;
                            }

                            if (_Request.CountValue != null)
                            {
                                Details.CountValue = _Request.CountValue;
                            }
                            if (_Request.AverageValue != null)
                            {
                                Details.AverageValue = _Request.AverageValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.VerificationKey))
                            {
                                long VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == _Request.VerificationKey).Select(x => x.Id).FirstOrDefault();
                                if (VerificationId != 0)
                                {
                                    Details.VerificationId = VerificationId;
                                }
                            }
                            if (!string.IsNullOrEmpty(_Request.TransactionKey))
                            {
                                long TransactionId = _HCoreContext.HCUAccountTransaction.Where(x => x.Guid == _Request.TransactionKey).Select(x => x.Id).FirstOrDefault();
                                if (TransactionId != 0)
                                {
                                    Details.TransactionId = TransactionId;
                                }
                            }
                            Details.RequestKey = _Request.UserReference.RequestKey;
                            Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                            Details.ModifyById = _Request.UserReference.AccountId;
                            if (StatusId != null)
                            {
                                Details.StatusId = (int)StatusId;
                            }
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();

                            long? IconStorageId = null;
                            long? PosterStorageId = null;
                            if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                            {

                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, OldIconStorageId, _Request.UserReference);
                            }
                            if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                            {
                                PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, OldPosterStorageId, _Request.UserReference);
                            }
                            if (IconStorageId != null || PosterStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var UserParameterDetails = _HCoreContext.HCUAccountParameter.Where(_Request.Reference).FirstOrDefault();
                                    if (UserParameterDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            UserParameterDetails.IconStorageId = IconStorageId;
                                        }
                                        if (PosterStorageId != null)
                                        {
                                            UserParameterDetails.PosterStorageId = PosterStorageId;
                                        }
                                        _HCoreContext.SaveChanges();

                                    }
                                }
                            }


                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description:  Updates the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserSession(OUserSession.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountSession Details = _HCoreContext.HCUAccountSession
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            if (Details.StatusId == HelperStatus.Default.Active)
                            {
                                Details.StatusId = HelperStatus.Default.Blocked;
                                Details.LogoutDate = HCoreHelper.GetGMTDateTime();
                                Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                Details.ModifyById = _Request.UserReference.AccountId;
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000", "Session already exipred.");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description:  Updates the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateUserDevice(OUserDevice.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    long? OsVersionId = HCoreHelper.GetCoreParameterId(_Request.OsVersionKey, _Request.UserReference);
                    long? AppVersionId = HCoreHelper.GetCoreCommonId(_Request.AppVersionKey, _Request.UserReference);
                    int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountDevice Details = _HCoreContext.HCUAccountDevice
                            .Where(x => x.Guid == _Request.ReferenceKey)
                           .FirstOrDefault();
                        if (Details != null)
                        {
                            if (Details.StatusId == HelperStatus.Default.Active)
                            {
                                if (StatusId != null)
                                {
                                    Details.StatusId = (int)StatusId;
                                }
                                if (OsVersionId != null)
                                {
                                    Details.OsVersionId = OsVersionId;
                                }
                                if (AppVersionId != null)
                                {
                                    Details.AppVersionId = (long)AppVersionId;
                                }
                                if (!string.IsNullOrEmpty(_Request.NotificationUrl))
                                {
                                    Details.NotificationUrl = _Request.NotificationUrl;
                                }
                                Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                Details.ModifyById = _Request.UserReference.AccountId;
                            }
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCUAccountParameter Details = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.HCUAccountParameter.RemoveRange(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Request, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteUserAccount(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000", "Reference missing");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        var _AccDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Guid == _Request.ReferenceKey)
                            .Select(x => new
                            {
                                Id = x.Id,
                                AccountTypeId = x.AccountTypeId,
                                BankTr = x.HCUAccountTransactionBank.Count,
                                CreatedByTr = x.HCUAccountTransactionCreatedBy.Count,
                                ModifyByTr = x.HCUAccountTransactionModifyBy.Count,
                                ParentTr = x.HCUAccountTransactionParent.Count,
                                ProviderTr = x.HCUAccountTransactionProvider.Count,
                                SubParentTr = x.HCUAccountTransactionSubParent.Count,
                                UserAccountTr = x.HCUAccountTransactionAccount.Count,
                                TerminalStatusCount = x.TUCTerminalStatus.Count,
                            }).FirstOrDefault();
                        if (_AccDetails != null)
                        {
                            if (_AccDetails.BankTr == 0
                                && _AccDetails.CreatedByTr == 0
                                && _AccDetails.ModifyByTr == 0
                                && _AccDetails.ParentTr == 0
                                && _AccDetails.ProviderTr == 0
                                && _AccDetails.SubParentTr == 0
                                && _AccDetails.UserAccountTr == 0
                                )
                            {

                                var UserSessions = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _AccDetails.Id).ToList();
                                var UserParameters = _HCoreContext.HCUAccountParameter.Where(x => x.AccountId == _AccDetails.Id).ToList();
                                var UserDevice = _HCoreContext.HCUAccountDevice.Where(x => x.AccountId == _AccDetails.Id).ToList();
                                var UserActivity = _HCoreContext.HCUAccountActivity.Where(x => x.AccountId == _AccDetails.Id).ToList();
                                var UserAccountOwner = _HCoreContext.HCUAccountOwner.Where(x => x.AccountId == _AccDetails.Id || x.OwnerId == _AccDetails.Id).ToList();
                                var TerminalStatus = _HCoreContext.TUCTerminalStatus.Where(x => x.TerminalId == _AccDetails.Id).ToList();
                                if (UserSessions.Count > 0)
                                {
                                    _HCoreContext.HCUAccountSession.RemoveRange(UserSessions);
                                }
                                if (UserParameters.Count > 0)
                                {
                                    _HCoreContext.HCUAccountParameter.RemoveRange(UserParameters);
                                }
                                if (UserDevice.Count > 0)
                                {
                                    _HCoreContext.HCUAccountDevice.RemoveRange(UserDevice);
                                }
                                if (UserActivity.Count > 0)
                                {
                                    _HCoreContext.HCUAccountActivity.RemoveRange(UserActivity);
                                }
                                if (UserAccountOwner.Count > 0)
                                {
                                    _HCoreContext.HCUAccountOwner.RemoveRange(UserAccountOwner);
                                }
                                if (TerminalStatus.Count > 0)
                                {
                                    _HCoreContext.TUCTerminalStatus.RemoveRange(TerminalStatus);
                                }
                                _HCoreContext.SaveChanges();
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var _Account = _HCoreContext.HCUAccount.Where(x => x.Id == _AccDetails.Id).FirstOrDefault();
                                    if (_Account != null)
                                    {
                                        _HCoreContext.HCUAccount.Remove(_Account);
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC1000", "Account deleted successfully");
                                #endregion
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000", "Operation failed. Transactions associated with the account. Please contact technical support to delete account");
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000", "Account details not found");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
    }
}
