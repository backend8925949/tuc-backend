//==================================================================================
// FileName: FrameworkCoreAccountOverview.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core account overview
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Operations;
using HCore.Helper;
using HCore.Operations.Actor;
using HCore.Operations.Object;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.Helper.HCoreConstant.HelperStatus;
using Microsoft.EntityFrameworkCore;
using static HCore.Operations.Object.OConfigurationManager;

namespace HCore.Operations.Framework
{
    public class FrameworkCoreTransactionProcessor
    {
        public class BinStatus
        {
            public bool status { get; set; }
            public string? message { get; set; }
            public BinInfo data { get; set; }
        }
        public class BinInfo
        {
            public string? bin { get; set; }
            public string? bin_systemname { get; set; }
            public string? brand { get; set; }
            public string? brand_systemname { get; set; }
            public string? sub_brand { get; set; }
            public string? sub_brand_systemname { get; set; }
            public string? country_code { get; set; }
            public string? country_code_systemname { get; set; }
            public string? country_name { get; set; }
            public string? country_name_systemname { get; set; }
            public string? card_type { get; set; }
            public string? card_type_systemname { get; set; }
            public string? bank { get; set; }
            public string? bank_systemname { get; set; }
        }
        private class OUserAccounts
        {
            public long UserAccountId { get; set; }
            public long UserAccountTypeId { get; set; }
        }
        HCoreContext _HCoreContext;
        HCCoreBinNumber _HCCoreBinNumber;
        HCCoreCommon _HCCoreCommon;
        HCUAccountBalance _HCUAccountBalance;
        HCCoreCountry _HCCoreCountry;
        cmt_loyalty_customer _cmt_loyalty_customer;
        /// <summary>
        /// Validates the transactions.
        /// </summary>
        /// <param name="_Request">The request.</param>
        internal async void ValidateTransactions(OCoreTransaction.Request _Request)
        {
            // User Suspension Code Removed As Per Discussion With Suraj Sir, Simeon -21 - 06 - 2019 05:01 IST - HARSHAL GANDOLE
            try
            {
                if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                {
                    double SecurityMaximumInvoiceAmountPerTransaction = 100000;
                    double SecurityMaximumTransactionPerDayPerCustomerCap = 15;
                    double SecurityMaximumInvoiceAmountPerDayPerCustomerCap = 250000;
                    string TSecurityMaximumInvoiceAmountPerTransaction = HCoreHelper.GetConfigurationValueByUserAccount("securitymaximuminvoiceamountpertransactioncap", _Request.ParentId, _Request.UserReference);
                    string TSecurityMaximumTransactionPerDayCap = HCoreHelper.GetConfigurationValueByUserAccount("securitymaximumtransactionperdaycap", _Request.ParentId, _Request.UserReference);
                    string TSecurtiyMaximumInvoiceAmountPerDayCap = HCoreHelper.GetConfigurationValueByUserAccount("securitymaximuminvoiceamountperdaycap", _Request.ParentId, _Request.UserReference);
                    string SecurityNotificationEmails = HCoreHelper.GetConfigurationValueByUserAccount("securitynotificationemails", _Request.ParentId, _Request.UserReference);
                    try
                    {
                        if (!string.IsNullOrEmpty(TSecurityMaximumInvoiceAmountPerTransaction))
                        {
                            if (TSecurityMaximumInvoiceAmountPerTransaction != "0")
                            {
                                SecurityMaximumInvoiceAmountPerTransaction = Convert.ToDouble(TSecurityMaximumInvoiceAmountPerTransaction);
                            }
                        }
                        if (!string.IsNullOrEmpty(TSecurityMaximumTransactionPerDayCap))
                        {
                            if (TSecurityMaximumTransactionPerDayCap != "0")
                            {
                                SecurityMaximumTransactionPerDayPerCustomerCap = Convert.ToInt64(TSecurityMaximumTransactionPerDayCap);
                            }
                        }
                        if (!string.IsNullOrEmpty(TSecurtiyMaximumInvoiceAmountPerDayCap))
                        {
                            if (TSecurtiyMaximumInvoiceAmountPerDayCap != "0")
                            {
                                SecurityMaximumInvoiceAmountPerDayPerCustomerCap = Convert.ToDouble(TSecurtiyMaximumInvoiceAmountPerDayCap);
                            }
                        }
                    }
                    catch (Exception _Exception)
                    {
                        HCoreHelper.LogException("ValidateTransactions-CONFIG-CONVERSION-ERROR", _Exception);
                    }

                    foreach (OCoreTransaction.TransactionItem _TransactionItem in _Request.Transactions)
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            if (_TransactionItem.SourceId == TransactionSource.TUC || _TransactionItem.SourceId == TransactionSource.TUCBlack || _TransactionItem.SourceId == TransactionSource.ThankUCashPlus || _TransactionItem.SourceId == TransactionSource.GiftPoints)
                            {
                                if (_TransactionItem.Amount > 0 && _Request.ProgramId < 1)
                                {
                                    DateTime StartDate = HCoreHelper.GetGMTDate();
                                    DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1).AddSeconds(-1);
                                    if (_Request.SubParentId != 0)
                                    {
                                        long Transactions = await _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.SubParentId == _Request.SubParentId
                                                                        && m.AccountId == _TransactionItem.UserAccountId
                                                                        && m.TransactionDate > StartDate
                                                                        && m.TransactionDate < EndDate
                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                        && m.StatusId == Transaction.Success
                                                                      ).CountAsync();
                                        if (Transactions > SecurityMaximumTransactionPerDayPerCustomerCap)
                                        {
                                            var UserAccount = _HCoreContext.HCUAccount
                                            .Where(x => x.Id == _TransactionItem.UserAccountId)
                                            .FirstOrDefault();
                                            if (UserAccount != null)
                                            {
                                                if (UserAccount.StatusId == Default.Active)
                                                {
                                                    //UserAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    //UserAccount.StatusId = Default.Suspended;
                                                    //UserAccount.ModifyById = 2;
                                                    //_HCoreContext.SaveChanges();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            double? PurchaseAmount = await _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.SubParentId == _Request.SubParentId
                                                                        && m.AccountId == _TransactionItem.UserAccountId
                                                                        && m.TransactionDate > StartDate
                                                                        && m.TransactionDate < EndDate
                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                        && m.StatusId == Transaction.Success
                                                                      ).SumAsync(x => (double?)x.PurchaseAmount) ?? 0;
                                            if (PurchaseAmount > SecurityMaximumTransactionPerDayPerCustomerCap)
                                            {
                                                var UserAccount = _HCoreContext.HCUAccount
                                               .Where(x => x.Id == _TransactionItem.UserAccountId)
                                               .FirstOrDefault();
                                                if (UserAccount != null)
                                                {
                                                    if (UserAccount.StatusId == Default.Active)
                                                    {
                                                        //UserAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                        //UserAccount.StatusId = Default.Suspended;
                                                        //UserAccount.ModifyById = 2;
                                                        //_HCoreContext.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        long Transactions = await _HCoreContext.HCUAccountTransaction
                                                                      .Where(m => m.ParentId == _Request.ParentId
                                                                       && m.AccountId == _TransactionItem.UserAccountId
                                                                       && m.TransactionDate > StartDate
                                                                       && m.TransactionDate < EndDate
                                                                       && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                       || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                       && m.StatusId == HelperStatus.Transaction.Success
                                                                     ).CountAsync();
                                        if (Transactions > SecurityMaximumTransactionPerDayPerCustomerCap)
                                        {
                                            var UserAccount = _HCoreContext.HCUAccount
                                            .Where(x => x.Id == _TransactionItem.UserAccountId)
                                            .FirstOrDefault();
                                            if (UserAccount != null)
                                            {
                                                if (UserAccount.StatusId == Default.Active)
                                                {
                                                    //UserAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                    //UserAccount.StatusId = Default.Suspended;
                                                    //UserAccount.ModifyById = 2;
                                                    //_HCoreContext.SaveChanges();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            double? PurchaseAmount = await _HCoreContext.HCUAccountTransaction
                                                                       .Where(m => m.ParentId == _Request.ParentId
                                                                        && m.AccountId == _TransactionItem.UserAccountId
                                                                        && m.TransactionDate > StartDate
                                                                        && m.TransactionDate < EndDate
                                                                        && (((m.ModeId == TransactionMode.Credit || m.ModeId == TransactionMode.Debit) && m.TypeId != TransactionType.ThankUCashPlusCredit && (m.SourceId == TransactionSource.TUC || m.SourceId == TransactionSource.TUCBlack))
                                                                        || (m.ModeId == TransactionMode.Credit && m.SourceId == TransactionSource.ThankUCashPlus))
                                                                        && m.StatusId == HelperStatus.Transaction.Success
                                                                      ).SumAsync(x => (double?)x.PurchaseAmount) ?? 0;
                                            if (PurchaseAmount > SecurityMaximumInvoiceAmountPerDayPerCustomerCap)
                                            {
                                                var UserAccount = _HCoreContext.HCUAccount
                                               .Where(x => x.Id == _TransactionItem.UserAccountId)
                                               .FirstOrDefault();
                                                if (UserAccount != null)
                                                {
                                                    if (UserAccount.StatusId == Default.Active)
                                                    {
                                                        //UserAccount.ModifyDate = HCoreHelper.GetGMTDateTime();
                                                        //UserAccount.StatusId = Default.Suspended;
                                                        //UserAccount.ModifyById = 2;
                                                        //_HCoreContext.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if (_Request.InvoiceAmount > SecurityMaximumInvoiceAmountPerDayPerCustomerCap)
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            var UserAccountDetails = await _HCoreContext.HCUAccount
                                                                    .Where(x => x.Id == _TransactionItem.UserAccountId)
                                                                    .Select(x => new
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,
                                                                        DisplayName = x.DisplayName,
                                                                        MobileNumber = x.MobileNumber,
                                                                        StatusName = x.Status.Name,
                                                                    }).FirstOrDefaultAsync();
                                            if (UserAccountDetails != null)
                                            {
                                                EmailBoradCasterParameter _EmailBoradCasterParameter = new EmailBoradCasterParameter();
                                                _EmailBoradCasterParameter.UserDisplayname = UserAccountDetails.DisplayName;
                                                _EmailBoradCasterParameter.UserMobileNumber = UserAccountDetails.MobileNumber;
                                                _EmailBoradCasterParameter.Message = "Suspecious activity detected by user " + UserAccountDetails.DisplayName + ". User current status is " + UserAccountDetails.StatusName;
                                                _EmailBoradCasterParameter.InvoiceAmount = _Request.InvoiceAmount.ToString();
                                                var StoreDetails = _HCoreContext.HCUAccount
                                                                                .Where(x => x.Id == _Request.SubParentId)
                                                                                .Select(x => new
                                                                                {
                                                                                    StoreId = x.Id,
                                                                                    MerchantId = x.OwnerId,
                                                                                    MerchantDisplayName = x.Owner.DisplayName,
                                                                                    StoreDisplayName = x.DisplayName,
                                                                                    StoreAddress = x.Address
                                                                                }).FirstOrDefault();
                                                if (StoreDetails != null)
                                                {
                                                    _EmailBoradCasterParameter.MerchantDisplayName = StoreDetails.MerchantDisplayName;
                                                    _EmailBoradCasterParameter.StoreDisplayName = StoreDetails.StoreDisplayName;
                                                    _EmailBoradCasterParameter.StoreAddress = StoreDetails.StoreAddress;
                                                }
                                                var CashierDetails = _HCoreContext.HCUAccount
                                                                              .Where(x => x.Id == _Request.CashierId)
                                                                              .Select(x => new
                                                                              {
                                                                                  CashierCode = x.DisplayName,
                                                                                  CashierName = x.Name,
                                                                              }).FirstOrDefault();
                                                if (CashierDetails != null)
                                                {
                                                    _EmailBoradCasterParameter.CashierCode = CashierDetails.CashierCode;
                                                    _EmailBoradCasterParameter.CashierName = CashierDetails.CashierName;
                                                }
                                                if (HostEnvironment == HostEnvironmentType.Live)
                                                {
                                                    // HCoreHelper.BroadCastEmail(NotificationTemplates.Admin_Transaction_HeavyAmount, "Thank U Cash Alert", "harshal@thankucash.com", _EmailBoradCasterParameter, null);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("PROCESS TRANSACTION - TRANSACTION CHECK", _Exception);
            }
        }
        /// <summary>
        /// Processes the account balance overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        ///
        internal async void ProcessAccountBalanceOverview(OCoreTransaction.Request _Request)
        {
            foreach (var _TransactionItem in _Request.Transactions)
            {
                using (_HCoreContext = new HCoreContext())
                {
                    DateTime CurrentDate = HCoreHelper.GetGMTDate();
                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                    var AccountDetails = await _HCoreContext.HCUAccount.Where(x => x.Id == _TransactionItem.UserAccountId)
                    .Select(x => new
                    {
                        AccountTypeId = x.AccountTypeId,
                    })
                        .FirstOrDefaultAsync();
                    if (AccountDetails != null)
                    {
                        double InvoiceAmount = 0;
                        int Transactions = 0;
                        double Credit = 0;
                        double Debit = 0;
                        double Balance = 0;
                        DateTime LastTransactionDate = HCoreHelper.GetGMTDateTime();
                        if (AccountDetails.AccountTypeId == UserAccountType.Appuser)
                        {
                            long OwnerId = _Request.ParentId;
                            if (_Request.ProgramId > 0)
                            {
                                OwnerId = _HCoreContext.TUCLProgram.Where(x => x.Id == _Request.ProgramId)
                                       .Select(x => x.AccountId).FirstOrDefault();
                                #region Update Parent Balance | Merchant Customer Balance
                                Transactions = await _HCoreContext.HCUAccountTransaction
                                                    .CountAsync(x => x.AccountId == _TransactionItem.UserAccountId
                                                          && x.SourceId == _TransactionItem.SourceId
                                                          && x.ProgramId == _Request.ProgramId
                                                          && x.StatusId == Transaction.Success);
                                InvoiceAmount = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                       .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                                          && x.SourceId == _TransactionItem.SourceId
                                                          && x.ProgramId == _Request.ProgramId
                                                          && x.StatusId == Transaction.Success)
                                                       .SumAsync(x => (double?)x.PurchaseAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                Credit = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                        .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                                          && x.SourceId == _TransactionItem.SourceId
                                                          && x.ModeId == TransactionMode.Credit
                                                          && x.ProgramId == _Request.ProgramId
                                                          && x.StatusId == Transaction.Success)
                                                        .SumAsync(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                Debit = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                                .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                                                  && x.SourceId == _TransactionItem.SourceId
                                                                  && x.ModeId == TransactionMode.Debit
                                                                  && x.ProgramId == _Request.ProgramId
                                                                  && x.StatusId == Transaction.Success)
                                                                .SumAsync(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                double ParentBalance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble);
                                var ParentUserBalance = await _HCoreContext.HCUAccountBalance
                                           .Where(x =>
                                           x.AccountId == _TransactionItem.UserAccountId
                                           && x.ParentId == OwnerId
                                           && x.SourceId == _TransactionItem.SourceId
                                           ).FirstOrDefaultAsync();
                                if (ParentUserBalance != null)
                                {
                                    ParentUserBalance.Credit = Credit;
                                    ParentUserBalance.Debit = Debit;
                                    ParentUserBalance.Balance = ParentBalance;
                                    ParentUserBalance.ProgramId = _Request.ProgramId;
                                    ParentUserBalance.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                }
                                else
                                {
                                    _HCUAccountBalance = new HCUAccountBalance();
                                    _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountBalance.AccountId = _TransactionItem.UserAccountId;
                                    _HCUAccountBalance.ParentId = OwnerId;
                                    _HCUAccountBalance.SourceId = _TransactionItem.SourceId;
                                    _HCUAccountBalance.Credit = Credit;
                                    _HCUAccountBalance.Debit = Debit;
                                    _HCUAccountBalance.Balance = ParentBalance;
                                    _HCUAccountBalance.ProgramId = _Request.ProgramId;
                                    _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                    await _HCoreContext.HCUAccountBalance.AddAsync(_HCUAccountBalance);
                                }
                                #endregion
                            }
                            else
                            {
                                #region Update Parent Balance | Merchant Customer Balance
                                #region Balance Calculation
                                Transactions = await _HCoreContext.HCUAccountTransaction
                                                 .CountAsync(x => x.AccountId == _TransactionItem.UserAccountId
                                                       && x.ParentId == _Request.ParentId
                                                       && x.SourceId == _TransactionItem.SourceId
                                                       && x.ProgramId == null
                                                       && x.StatusId == Transaction.Success);

                                InvoiceAmount = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                  .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                                       && x.ParentId == _Request.ParentId
                                                       && x.SourceId == _TransactionItem.SourceId
                                                       && x.ProgramId == null
                                                       && x.StatusId == Transaction.Success)
                                                  .SumAsync(x => (double?)x.PurchaseAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                Credit = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                   .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                                       && x.ParentId == _Request.ParentId
                                                       && x.SourceId == _TransactionItem.SourceId
                                                       && x.ModeId == TransactionMode.Credit
                                                       && x.ProgramId == null
                                                       && x.StatusId == Transaction.Success)
                                                   .SumAsync(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);

                                Debit = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                                           && x.ParentId == _Request.ParentId
                                                           && x.SourceId == _TransactionItem.SourceId
                                                           && x.ModeId == TransactionMode.Debit
                                                           && x.ProgramId == null
                                                           && x.StatusId == Transaction.Success)
                                                     .SumAsync(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                                Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble);
                                #endregion
                                #region Owner Balance
                                var OwnerUserBalance = await _HCoreContext.HCUAccountBalance
                                           .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                                   && x.ParentId == _Request.ParentId
                                                   && x.SourceId == _TransactionItem.SourceId)
                                           .FirstOrDefaultAsync();
                                if (OwnerUserBalance != null)
                                {
                                    OwnerUserBalance.Credit = Credit;
                                    OwnerUserBalance.Debit = Debit;
                                    OwnerUserBalance.Balance = Balance;
                                    OwnerUserBalance.Transactions = Transactions;
                                    OwnerUserBalance.LastTransactionDate = LastTransactionDate;
                                }
                                else
                                {
                                    _HCUAccountBalance = new HCUAccountBalance();
                                    _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountBalance.AccountId = _TransactionItem.UserAccountId;
                                    _HCUAccountBalance.ParentId = _Request.ParentId;
                                    _HCUAccountBalance.SourceId = _TransactionItem.SourceId;
                                    _HCUAccountBalance.Credit = Credit;
                                    _HCUAccountBalance.Debit = Debit;
                                    _HCUAccountBalance.Balance = Balance;
                                    _HCUAccountBalance.Transactions = Transactions;
                                    _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                    await _HCoreContext.HCUAccountBalance.AddAsync(_HCUAccountBalance);
                                }
                                #endregion
                                #region Root User Balance
                                var UserRootUserBalance = await _HCoreContext.HCUAccountBalance
                                           .Where(x =>
                                           x.AccountId == _TransactionItem.UserAccountId
                                           && x.ParentId == 1
                                           && x.SourceId == _TransactionItem.SourceId
                                           ).FirstOrDefaultAsync();
                                if (UserRootUserBalance != null)
                                {
                                    UserRootUserBalance.Credit = Credit;
                                    UserRootUserBalance.Debit = Debit;
                                    UserRootUserBalance.Balance = Balance;
                                    UserRootUserBalance.Transactions = Transactions;
                                    UserRootUserBalance.LastTransactionDate = LastTransactionDate;
                                }
                                else
                                {
                                    _HCUAccountBalance = new HCUAccountBalance();
                                    _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                    _HCUAccountBalance.AccountId = _TransactionItem.UserAccountId;
                                    _HCUAccountBalance.ParentId = 1;
                                    _HCUAccountBalance.SourceId = _TransactionItem.SourceId;
                                    _HCUAccountBalance.Credit = Credit;
                                    _HCUAccountBalance.Debit = Debit;
                                    _HCUAccountBalance.Balance = Balance;
                                    _HCUAccountBalance.Transactions = Transactions;
                                    _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                    await _HCoreContext.HCUAccountBalance.AddAsync(_HCUAccountBalance);
                                }
                                #endregion
                                #endregion
                            }


                            cmt_loyalty_customer? MerchantCustomer = null;
                            if (_Request.ProgramId > 0)
                            {
                                MerchantCustomer = await _HCoreContext.cmt_loyalty_customer
                                             .Where(x => x.customer_id == _TransactionItem.UserAccountId
                                             && x.owner_id == OwnerId
                                             && x.program_id == _Request.ProgramId
                                             && x.source_id == _TransactionItem.SourceId
                                             ).FirstOrDefaultAsync();
                            }
                            else
                            {
                                MerchantCustomer = await _HCoreContext.cmt_loyalty_customer
                                             .Where(x => x.customer_id == _TransactionItem.UserAccountId
                                             && x.owner_id == OwnerId
                                             && x.source_id == _TransactionItem.SourceId
                                             ).FirstOrDefaultAsync();
                            }
                            if (MerchantCustomer != null)
                            {
                                MerchantCustomer.credit = Credit;
                                MerchantCustomer.debit = Debit;
                                MerchantCustomer.balance = Balance;
                                MerchantCustomer.invoice_amount = InvoiceAmount;
                                MerchantCustomer.total_transactions = Transactions;
                                MerchantCustomer.last_transaction_date = LastTransactionDate;
                            }
                            else
                            {
                                _cmt_loyalty_customer = new cmt_loyalty_customer();
                                _cmt_loyalty_customer.guid = HCoreHelper.GenerateGuid();
                                _cmt_loyalty_customer.owner_id = OwnerId;
                                if (_Request.ProgramId > 0)
                                {
                                    _cmt_loyalty_customer.program_id = _Request.ProgramId;
                                }
                                _cmt_loyalty_customer.customer_id = _TransactionItem.UserAccountId;
                                _cmt_loyalty_customer.source_id = _TransactionItem.SourceId;
                                _cmt_loyalty_customer.credit = Credit;
                                _cmt_loyalty_customer.debit = Debit;
                                _cmt_loyalty_customer.balance = Balance;
                                _cmt_loyalty_customer.invoice_amount = InvoiceAmount;
                                _cmt_loyalty_customer.total_transactions = Transactions;
                                _cmt_loyalty_customer.last_transaction_date = HCoreHelper.GetGMTDateTime();
                                _cmt_loyalty_customer.create_date = HCoreHelper.GetGMTDateTime();
                                _cmt_loyalty_customer.status_id = HelperStatus.Default.Active;
                                await _HCoreContext.cmt_loyalty_customer.AddAsync(_cmt_loyalty_customer);
                            }
                        }
                        else
                        {
                            Credit = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                    .Where(x => x.AccountId == _TransactionItem.UserAccountId &&
                                                           x.SourceId == _TransactionItem.SourceId &&
                                                           x.ModeId == TransactionMode.Credit &&
                                                           x.StatusId == Transaction.Success)
                                                    .SumAsync(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                            Debit = HCoreHelper.RoundNumber(await _HCoreContext.HCUAccountTransaction
                                                            .Where(x => x.AccountId == _TransactionItem.UserAccountId &&
                                                                     x.SourceId == _TransactionItem.SourceId &&
                                                                    x.ModeId == TransactionMode.Debit &&
                                                                    x.StatusId == Transaction.Success)
                                                             .SumAsync(x => (double?)x.TotalAmount) ?? 0, _AppConfig.SystemEntryRoundDouble);
                            Balance = HCoreHelper.RoundNumber((Credit - Debit), _AppConfig.SystemEntryRoundDouble); // Math.Round((TotalCredit - TotalDebit), HCoreConstant._AppConfig.AmountRoundDigit);
                            var UserBalance = await _HCoreContext.HCUAccountBalance
                                       .Where(x => x.AccountId == _TransactionItem.UserAccountId
                                       && x.SourceId == _TransactionItem.SourceId
                                       ).FirstOrDefaultAsync();
                            if (UserBalance != null)
                            {
                                UserBalance.Credit = Credit;
                                UserBalance.Debit = Debit;
                                UserBalance.Balance = Balance;
                                UserBalance.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                            }
                            else
                            {
                                _HCUAccountBalance = new HCUAccountBalance();
                                _HCUAccountBalance.Guid = HCoreHelper.GenerateGuid();
                                _HCUAccountBalance.AccountId = _TransactionItem.UserAccountId;
                                _HCUAccountBalance.SourceId = _TransactionItem.SourceId;
                                _HCUAccountBalance.Credit = Credit;
                                _HCUAccountBalance.Debit = Debit;
                                _HCUAccountBalance.Balance = Balance;
                                _HCUAccountBalance.StatusId = HelperStatus.Default.Active;
                                _HCUAccountBalance.LastTransactionDate = HCoreHelper.GetGMTDateTime();
                                await _HCoreContext.HCUAccountBalance.AddAsync(_HCUAccountBalance);
                            }
                        }
                        if (_TransactionItem.TransactionId > 0)
                        {
                            var TransactionDetails = await _HCoreContext.HCUAccountTransaction.Where(x => x.Id == _TransactionItem.TransactionId).FirstOrDefaultAsync();
                            if (TransactionDetails != null)
                            {
                                TransactionDetails.Balance = Balance;
                            }
                        }
                    }
                    await _HCoreContext.SaveChangesAsync();
                    await _HCoreContext.DisposeAsync();
                }
            }
            //AccountNumber
        }
        /// <summary>
        /// Processes the bin number.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>System.Int64.</returns>
        internal async Task<long> ProcessBinNumber(OCoreTransaction.Request _Request)
        {
            int CardBinNumberId = 0;
            long BrandId = 0;
            try
            {
                if (!string.IsNullOrEmpty(_Request.AccountNumber))
                {
                    if (_Request.AccountNumber.Length >= 6)
                    {
                        int CountryId = 0;
                        long CardTypeId = 0;
                        long BankId = 0;
                        string AccountNumber = _Request.AccountNumber;
                        string Bin = AccountNumber.Substring(0, 6);
                        using (_HCoreContext = new HCoreContext())
                        {
                            var BinCheck = await _HCoreContext.HCCoreBinNumber.Where(x => x.Bin == Bin).FirstOrDefaultAsync();
                            if (BinCheck != null)
                            {
                                CardBinNumberId = BinCheck.Id;
                                if (BinCheck.BrandId != null)
                                {
                                    BrandId = (long)BinCheck.BrandId;
                                }
                                if (BinCheck.CountryId != null)
                                {
                                    CountryId = (int)BinCheck.CountryId;
                                }
                                if (BinCheck.BankId != null)
                                {
                                    BankId = (long)BinCheck.BankId;
                                }
                                await _HCoreContext.DisposeAsync();
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    #region Send SMS Messagae
                                    var smsapiurl = "https://api.paystack.co/decision/bin/" + Bin;
                                    HttpWebRequest _HttpWebRequest = (HttpWebRequest)WebRequest.Create(smsapiurl);
                                    HttpWebResponse _HttpWebResponse = (HttpWebResponse)_HttpWebRequest.GetResponse();
                                    System.IO.StreamReader _StreamReader = new System.IO.StreamReader(_HttpWebResponse.GetResponseStream());
                                    string responseString = _StreamReader.ReadToEnd();
                                    _StreamReader.Close();
                                    _HttpWebResponse.Close();
                                    BinStatus? _BinInfo = JsonConvert.DeserializeObject<BinStatus>(responseString);
                                    if (_BinInfo?.status == true)
                                    {
                                        if (_BinInfo.data.brand != "Unknown")
                                        {
                                            _BinInfo.data.brand_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.brand);
                                            _BinInfo.data.sub_brand_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.sub_brand);
                                            _BinInfo.data.country_code_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.country_code);
                                            _BinInfo.data.country_name_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.country_name);
                                            _BinInfo.data.card_type_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.card_type);
                                            _BinInfo.data.bank_systemname = HCoreHelper.GenerateSystemName(_BinInfo.data.bank);
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                BrandId = await _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBrand && x.SystemName == _BinInfo.data.brand_systemname).Select(x => x.Id).FirstOrDefaultAsync();
                                                CountryId = await _HCoreContext.HCCoreCountry.Where(x => x.SystemName == _BinInfo.data.country_name_systemname).Select(x => x.Id).FirstOrDefaultAsync();
                                                CardTypeId = await _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardType && x.SystemName == _BinInfo.data.card_type_systemname).Select(x => x.Id).FirstOrDefaultAsync();
                                                BankId = await _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.BinCardBank && x.SystemName == _BinInfo.data.bank_systemname).Select(x => x.Id).FirstOrDefaultAsync();
                                                await _HCoreContext.DisposeAsync();
                                                #region Save Bin Country
                                                if (BrandId == 0 && !string.IsNullOrEmpty(_BinInfo.data.brand))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCommon = new HCCoreCommon();
                                                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCommon.TypeId = HelperType.BinCardBrand;
                                                        _HCCoreCommon.Name = _BinInfo.data.brand;
                                                        _HCCoreCommon.SystemName = _BinInfo.data.brand_systemname;
                                                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                        await _HCoreContext.HCCoreCommon.AddAsync(_HCCoreCommon);
                                                        await _HCoreContext.SaveChangesAsync();
                                                        BrandId = _HCCoreCommon.Id;
                                                    }
                                                }
                                                #endregion
                                                #region Save Bin Country
                                                if (CountryId == 0 && !string.IsNullOrEmpty(_BinInfo.data.country_name))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCountry = new HCCoreCountry();
                                                        _HCCoreCountry.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCountry.Name = _BinInfo.data.country_name;
                                                        _HCCoreCountry.SystemName = _BinInfo.data.country_name_systemname;
                                                        _HCCoreCountry.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCountry.CreatedById = 1;
                                                        _HCCoreCountry.StatusId = HelperStatus.Default.Active;
                                                        await _HCoreContext.HCCoreCountry.AddAsync(_HCCoreCountry);
                                                        await _HCoreContext.SaveChangesAsync();
                                                        CountryId = _HCCoreCountry.Id;
                                                    }
                                                }
                                                #endregion
                                                #region Save Bin Card Type
                                                if (CardTypeId == 0 && !string.IsNullOrEmpty(_BinInfo.data.card_type))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCommon = new HCCoreCommon();
                                                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCommon.TypeId = HelperType.BinCardType;
                                                        _HCCoreCommon.Name = _BinInfo.data.card_type;
                                                        _HCCoreCommon.SystemName = _BinInfo.data.card_type_systemname;
                                                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                        await _HCoreContext.HCCoreCommon.AddAsync(_HCCoreCommon);
                                                        await _HCoreContext.SaveChangesAsync();
                                                        CardTypeId = _HCCoreCommon.Id;
                                                    }
                                                }
                                                #endregion
                                                #region Save Bin Bank
                                                if (BankId == 0 && !string.IsNullOrEmpty(_BinInfo.data.bank))
                                                {
                                                    using (_HCoreContext = new HCoreContext())
                                                    {
                                                        _HCCoreCommon = new HCCoreCommon();
                                                        _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                                        _HCCoreCommon.TypeId = HelperType.BinCardBank;
                                                        _HCCoreCommon.Name = _BinInfo.data.bank;
                                                        _HCCoreCommon.SystemName = _BinInfo.data.bank_systemname;
                                                        _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                                        _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                                        await _HCoreContext.HCCoreCommon.AddAsync(_HCCoreCommon);
                                                        await _HCoreContext.SaveChangesAsync();
                                                        BankId = _HCCoreCommon.Id;
                                                    }
                                                }
                                                #endregion
                                            }
                                            using (_HCoreContext = new HCoreContext())
                                            {
                                                _HCCoreBinNumber = new HCCoreBinNumber();
                                                _HCCoreBinNumber.Guid = HCoreHelper.GenerateGuid();
                                                _HCCoreBinNumber.Bin = Bin;
                                                //_HCCoreBinNumber.BinEnd = TBinEnd;
                                                //_HCCoreBinNumber.BinNumber = AccountNumber;
                                                if (CardTypeId != 0)
                                                {
                                                    _HCCoreBinNumber.TypeId = CardTypeId;
                                                }
                                                if (BrandId != 0)
                                                {
                                                    _HCCoreBinNumber.BrandId = BrandId;
                                                }
                                                if (BankId != 0)
                                                {
                                                    _HCCoreBinNumber.BankId = BankId;
                                                }
                                                if (CountryId != 0)
                                                {
                                                    _HCCoreBinNumber.CountryId = CountryId;
                                                }
                                                _HCCoreBinNumber.CreateDate = HCoreHelper.GetGMTDateTime();
                                                _HCCoreBinNumber.StatusId = HCoreConstant.HelperStatus.Default.Active;
                                                await _HCoreContext.HCCoreBinNumber.AddAsync(_HCCoreBinNumber);
                                                await _HCoreContext.SaveChangesAsync();
                                                CardBinNumberId = _HCCoreBinNumber.Id;
                                            }
                                        }
                                    }
                                    #endregion
                                }
                            }
                        }
                        if (CardBinNumberId > 0)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var BinNumberTransactions = await _HCoreContext.HCUAccountTransaction
                                .Where(x => x.GroupKey == _Request.GroupKey && x.AccountNumber == _Request.AccountNumber && x.CardBrandId == null).ToListAsync();
                                foreach (var BinNumberTransaction in BinNumberTransactions)
                                {
                                    if (CardBinNumberId != 0)
                                    {
                                        BinNumberTransaction.BinNumberId = CardBinNumberId;
                                    }
                                    if (BrandId != 0)
                                    {
                                        BinNumberTransaction.CardBrandId = BrandId;
                                    }
                                    if (CountryId != 0)
                                    {
                                        BinNumberTransaction.CardTypeId = CountryId;
                                    }
                                    if (CardTypeId != 0)
                                    {
                                        BinNumberTransaction.CardTypeId = CardTypeId;
                                    }
                                    if (BankId != 0)
                                    {
                                        BinNumberTransaction.CardBankId = BankId;
                                    }
                                }
                                await _HCoreContext.SaveChangesAsync();
                            }
                        }
                    }
                }
                return BrandId;
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("PROCESS TRANSACTION - ADD BIN", _Exception);
                return BrandId;
            }

        }
    }
}
