//==================================================================================
// FileName: FrameworkCoreUserAccess.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core user access
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using HCore.Data;
using HCore.Data.Models;
using HCore.Helper;
using HCore.Operations.Object;
using RestSharp;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations.Framework
{
    internal class FrameworkCoreUserAccess
    {
        #region Declare 
        HCoreContext _HCoreContext;
        HCUAccount _HCUAccount;
        Random _Random;
        HCUAccountAuth _HCUAccountAuth;
        OAppProfile.Response _AppProfileResponse;
        #endregion
        /// <summary>
        /// Assign Cards to user
        /// </summary>
        /// <param name="UserAccountId"></param>
        /// <param name="CardId"></param>
        /// <param name="ModifyById"></param>
        private void AssignCardToUser(long UserAccountId, long CardId, long ModifyById)
        {
            using (_HCoreContext = new HCoreContext())
            {
                var CardDetails = _HCoreContext.TUCard.Where(x => x.Id == CardId).FirstOrDefault();
                if (CardDetails != null)
                {
                    CardDetails.ActiveUserAccountId = UserAccountId;
                    if (ModifyById != 0)
                    {
                        CardDetails.ModifyBy = ModifyById;
                    }
                    CardDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                    CardDetails.StatusId = HelperStatus.Card.Assigned;
                    _HCoreContext.SaveChanges();
                }
                else
                {
                    _HCoreContext.Dispose();
                }
            }
        }

        /// <summary>
        /// Create Account Of App User / Customer registering through PTSP / PSSP / USSD And Other Providers
        /// </summary>
        /// <returns>The profile information</returns>
        internal OAppProfile.Response CreateAppUserAccount(OAppProfile.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AppProfileResponse = new OAppProfile.Response();
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                    _AppProfileResponse.StatusMessage = "Mobile number required";
                    _AppProfileResponse.StatusResponseCode = "AUA100";
                    return _AppProfileResponse;
                }
                #region Declare
                DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                #endregion
                #region Manage Operations
                int GenderId = 0;
                long CardId = 0;
                long CardMerchantId = 0;
                string MerchantDisplayName = "";
                string CountryIsd = "234";
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.OwnerId != 0)
                    {
                        MerchantDisplayName = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.OwnerId && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer))
                        .Select(x => x.DisplayName)
                        .FirstOrDefault();
                    }
                    if (!string.IsNullOrEmpty(_Request.CardNumber) && !string.IsNullOrEmpty(_Request.CardSerialNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber && x.SerialNumber == _Request.CardSerialNumber)
                        .Select(x => new
                        {
                            CardId = x.Id,
                            CardStatusId = x.StatusId,
                            CardActiveUser = x.ActiveUserAccountId,
                            CardActiveMerchant = x.ActiveMerchantId,
                        })
                        .FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.NotAssigned && CardDetails.CardActiveUser == null)
                            {
                                CardId = CardDetails.CardId;
                                CardMerchantId = CardDetails.CardActiveMerchant;
                            }
                            else if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Card already assigned";
                                _AppProfileResponse.StatusResponseCode = "AUA101";
                                return _AppProfileResponse;
                            }
                            else if (CardDetails.CardStatusId == HelperStatus.Card.Blocked)
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Card blocked. Please contact support";
                                _AppProfileResponse.StatusResponseCode = "AUA102";
                                return _AppProfileResponse;
                            }
                            else
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Invalid card details";
                                _AppProfileResponse.StatusResponseCode = "AUA103";
                                return _AppProfileResponse;
                            }

                        }
                        else
                        {
                            _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                            _AppProfileResponse.StatusMessage = "CardId details not found";
                            _AppProfileResponse.StatusResponseCode = "AUA104";
                            return _AppProfileResponse;
                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.CardNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == _Request.CardNumber)
                        .Select(x => new
                        {
                            CardId = x.Id,
                            CardStatusId = x.StatusId,
                            CardActiveUser = x.ActiveUserAccountId,
                            CardActiveMerchant = x.ActiveMerchantId,
                        })
                        .FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.NotAssigned && CardDetails.CardActiveUser == null)
                            {
                                CardId = CardDetails.CardId;
                                CardMerchantId = CardDetails.CardActiveMerchant;
                            }
                            else if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Card already assigned";
                                _AppProfileResponse.StatusResponseCode = "AUA101";
                                return _AppProfileResponse;
                            }
                            else if (CardDetails.CardStatusId == HelperStatus.Card.Blocked)
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Card blocked. Please contact support";
                                _AppProfileResponse.StatusResponseCode = "AUA102";
                                return _AppProfileResponse;
                            }
                            else
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Invalid card details";
                                _AppProfileResponse.StatusResponseCode = "AUA103";
                                return _AppProfileResponse;
                            }

                        }
                        else
                        {
                            _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                            _AppProfileResponse.StatusMessage = "CardId details not found";
                            _AppProfileResponse.StatusResponseCode = "AUA104";
                            return _AppProfileResponse;
                        }
                    }
                    else if (!string.IsNullOrEmpty(_Request.CardSerialNumber))
                    {
                        var CardDetails = _HCoreContext.TUCard
                       .Where(x => x.SerialNumber == _Request.CardSerialNumber)
                       .Select(x => new
                       {
                           CardId = x.Id,
                           CardStatusId = x.StatusId,
                           CardActiveUser = x.ActiveUserAccountId,
                           CardActiveMerchant = x.ActiveMerchantId,
                       })
                       .FirstOrDefault();
                        if (CardDetails != null)
                        {
                            if (CardDetails.CardStatusId == HelperStatus.Card.NotAssigned && CardDetails.CardActiveUser == null)
                            {
                                CardId = CardDetails.CardId;
                                CardMerchantId = CardDetails.CardActiveMerchant;
                            }
                            else if (CardDetails.CardStatusId == HelperStatus.Card.Assigned)
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Card already assigned";
                                _AppProfileResponse.StatusResponseCode = "AUA101";
                                return _AppProfileResponse;
                            }
                            else if (CardDetails.CardStatusId == HelperStatus.Card.Blocked)
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Card blocked. Please contact support";
                                _AppProfileResponse.StatusResponseCode = "AUA102";
                                return _AppProfileResponse;
                            }
                            else
                            {
                                _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                                _AppProfileResponse.StatusMessage = "Invalid card details";
                                _AppProfileResponse.StatusResponseCode = "AUA103";
                                return _AppProfileResponse;
                            }

                        }
                        else
                        {
                            _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                            _AppProfileResponse.StatusMessage = "CardId details not found";
                            _AppProfileResponse.StatusResponseCode = "AUA104";
                            return _AppProfileResponse;
                        }
                    }
                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        _Request.GenderCode = _Request.GenderCode.ToLower().Trim();
                        GenderId = _HCoreContext.HCCore.Where(x => x.SystemName.ToLower() == _Request.GenderCode && x.ParentId == HelperType.Gender && x.StatusId == HelperStatus.Default.Active).Select(x => x.Id).FirstOrDefault();
                    }
                    if (!string.IsNullOrEmpty(_Request.MobileNumber))
                    {
                        if (_Request.CountryId > 0)
                        {
                            var CountryDetails = _HCoreContext.HCCoreCountry
                                .Where(x => x.Id == _Request.CountryId)
                                .Select(x => new
                                {
                                    Isd = x.Isd,
                                    Length = x.MobileNumberLength
                                }).FirstOrDefault();
                            CountryIsd = CountryDetails.Isd;
                            _Request.MobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.Isd, _Request.MobileNumber, CountryDetails.Length);
                        }
                        //else
                        //{
                        //    _Request.MobileNumber = HCoreHelper.FormatMobileNumber(_Request.UserReference.CountryIsd, _Request.MobileNumber);
                        //}
                    }
                    #region  Process Registration
                    //int? CustomerCountryId = null;
                    //var ValidateMobileNumber = HCoreHelper.ValidateMobileNumber(Item.MobileNumber);
                    //string CustomerMobileNumber = HCoreHelper.FormatMobileNumber(ValidateMobileNumber.Isd, ValidateMobileNumber.MobileNumber, CountryDetails.MobileNumberLength);

                    string AppAccount = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    long ExistingUserId = (from n in _HCoreContext.HCUAccountAuth
                                           where n.Username == AppAccount
                                           select n.Id).FirstOrDefault();
                    if (ExistingUserId != 0)
                    {
                        var AccountDetails = _HCoreContext.HCUAccount
                            .Where(x => x.UserId == ExistingUserId && x.AccountTypeId == UserAccountType.Appuser)
                            .Select(x => new
                            {
                                AccountId = x.Id,
                                AccountKey = x.Guid,
                                x.CreateDate,
                                x.OwnerId,
                                Name = x.Name,
                                Gender = x.Gender.Name,
                                EmailAddress = x.EmailAddress,
                                MobileNumber = x.MobileNumber,
                                StatusId = x.StatusId,
                                AccountCode = x.AccountCode,
                                CountryId = x.CountryId,
                            }).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (CardId != 0 && CardMerchantId != 0)
                            {
                                AssignCardToUser(AccountDetails.AccountId, CardId, _Request.UserReference.AccountId);
                            }
                            if (_Request.AddressComponent != null && _Request.AddressComponent.CountryId > 0 && _Request.AddressComponent.StateId > 0 && _Request.AddressComponent.CityId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    HCCoreAddress _HCCoreAddress;
                                    _HCCoreAddress = new HCCoreAddress();
                                    _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                    _HCCoreAddress.LocationTypeId = 800;
                                    //_HCCoreAddress.IsPrimaryAddress = 1;
                                    _HCCoreAddress.AccountId = AccountDetails.AccountId;
                                    _HCCoreAddress.Name = _Request.AddressComponent.Name;
                                    _HCCoreAddress.ContactNumber = _Request.AddressComponent.ContactNumber;
                                    _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                                    _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                                    _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                                    if (_Request.AddressComponent.Latitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Latitude;
                                    }
                                    if (_Request.AddressComponent.Longitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Longitude;
                                    }
                                    _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                                    if (_Request.AddressComponent.CityAreaId > 0)
                                    {
                                        _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                                    }
                                    if (_Request.AddressComponent.CityId > 0)
                                    {
                                        _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                                    }
                                    if (_Request.AddressComponent.StateId > 0)
                                    {
                                        _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                                    }
                                    if (_Request.AddressComponent.CountryId > 0)
                                    {
                                        _HCCoreAddress.CountryId = (int)_Request.AddressComponent.CountryId;
                                    }
                                    _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreAddress.CreatedById = AccountDetails.AccountId;
                                    _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                                    _HCoreContext.SaveChanges();
                                    _AppProfileResponse.AddressReferenceId = _HCCoreAddress.Id;
                                    _AppProfileResponse.AddressReferenceKey = _HCCoreAddress.Guid;
                                }
                            }
                            _AppProfileResponse.AccountId = AccountDetails.AccountId;
                            _AppProfileResponse.AccountKey = AccountDetails.AccountKey;
                            _AppProfileResponse.OwnerId = AccountDetails.OwnerId;
                            _AppProfileResponse.CreateDate = AccountDetails.CreateDate;
                            _AppProfileResponse.MobileNumber = _Request.MobileNumber;
                            _AppProfileResponse.StatusId = AccountDetails.StatusId;
                            _AppProfileResponse.Name = AccountDetails.Name;
                            _AppProfileResponse.CountryId = (int)AccountDetails.CountryId;
                            _AppProfileResponse.Gender = AccountDetails.Gender;
                            _AppProfileResponse.EmailAddress = AccountDetails.EmailAddress;
                            _AppProfileResponse.MobileNumber = AccountDetails.MobileNumber;
                            _AppProfileResponse.AccountCode = AccountDetails.AccountCode;
                            _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Success;
                            _AppProfileResponse.StatusResponseCode = "Account created";
                            _AppProfileResponse.StatusResponseCode = "AUA105";
                            return _AppProfileResponse;
                        }
                        else
                        {
                            #region Online Account
                            _Random = new Random();
                            string AccessPin = _Random.Next(1111, 9999).ToString();
                            if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                AccessPin = "1234";
                            }
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                            _HCUAccount.UserId = ExistingUserId;
                            if (_Request.OwnerId != 0)
                            {
                                _HCUAccount.OwnerId = _Request.OwnerId;
                            }
                            if (_Request.SubOwnerId != 0)
                            {
                                _HCUAccount.SubOwnerId = _Request.SubOwnerId;
                            }
                            if (!string.IsNullOrEmpty(_Request.DisplayName))
                            {
                                _HCUAccount.DisplayName = _Request.DisplayName;
                            }
                            else
                            {
                                _HCUAccount.DisplayName = _Request.MobileNumber;
                            }
                            if (_HCUAccount.DisplayName.Length > 30)
                            {
                                _HCUAccount.DisplayName = _HCUAccount.DisplayName.Substring(0, 29);
                            }
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                            _Random = new Random();
                            string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                            _HCUAccount.AccountCode = AccountCode;
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ReferralCode = _Request.MobileNumber;
                            if (!string.IsNullOrEmpty(_Request.FirstName))
                            {
                                _HCUAccount.FirstName = _Request.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.MiddleName))
                            {
                                _HCUAccount.MiddleName = _Request.MiddleName;
                            }
                            if (!string.IsNullOrEmpty(_Request.LastName))
                            {
                                _HCUAccount.LastName = _Request.LastName;
                            }
                            _HCUAccount.RegistrationSourceId = RegistrationSource.App;
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }
                            _HCUAccount.CreateDate = CreateDate;
                            _HCUAccount.StatusId = HelperStatus.Default.Active;
                            if (!string.IsNullOrEmpty(_Request.UserReference.RequestKey))
                            {
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            }
                            if (_Request.CreatedById != 0)
                            {
                                _HCUAccount.CreatedById = _Request.CreatedById;
                            }
                            else
                            {
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                            }
                            if (_Request.CountryId > 0)
                            {
                                _HCUAccount.CountryId = _Request.CountryId;
                            }
                            else
                            {
                                _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                            }
                            _HCoreContext.HCUAccount.Add(_HCUAccount);
                            _HCoreContext.SaveChanges();

                            if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                            {
                                if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                {
                                    string Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    if (!string.IsNullOrEmpty(MerchantDisplayName))
                                    {
                                        Message = "Welcome to " + MerchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    }
                                    else
                                    {
                                        Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    }
                                    if ((HostEnvironment == HostEnvironmentType.Live && _Request.UserReference.AppId == 4418) || (HostEnvironment == HostEnvironmentType.Test && _Request.UserReference.AppId == 4375))
                                    {
                                        // Sterling Live
                                        string StrSubject = "Welcome to Sterling Rewards by ThankUCash";
                                        string StrMessage = "Welcome to Sterling Rewards by ThankUCash. To redeem your reward cash, use pin: " + AccessPin + ". To check you balance dial *822# to see merchants visit our website.";
                                        string FinalMobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber, 10);
                                        var client = new RestSharp.RestClient("https://lcctag.sterlingapps.p.azurewebsites.net/api/ThankUCash/notify");
                                        var request = new RestSharp.RestRequest();
                                        request.Method = RestSharp.Method.Post;
                                        request.AddHeader("accept", "application/json");
                                        request.AddHeader("content-type", "application/json");
                                        if (HostEnvironment == HostEnvironmentType.Live)
                                        {
                                            request.AddHeader("AppId", "1000");
                                        }
                                        else
                                        {
                                            request.AddHeader("AppId", "1000");
                                        }
                                        request.AddParameter("application/json", "{\"email\":\" " + _Request.EmailAddress + " \", \"phoneNumber\":\" " + FinalMobileNumber + " \",\"message\":\"" + StrMessage + "\",\"subject\":\"" + StrSubject + "\"}", RestSharp.ParameterType.RequestBody);
                                        var response = client.Execute(request);
                                    }
                                    else
                                    {
                                        HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, _Request.MobileNumber, Message, _HCUAccount.Id, null);
                                    }
                                }
                            }
                            if (CardId != 0 && CardMerchantId != 0)
                            {
                                AssignCardToUser(_HCUAccount.Id, CardId, _Request.UserReference.AccountId);
                            }
                            if (_Request.AddressComponent != null && _Request.AddressComponent.CountryId > 0 && _Request.AddressComponent.StateId > 0 && _Request.AddressComponent.CityId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    HCCoreAddress _HCCoreAddress;
                                    _HCCoreAddress = new HCCoreAddress();
                                    _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                    _HCCoreAddress.LocationTypeId = 800;
                                    //_HCCoreAddress.IsPrimaryAddress = 1;
                                    _HCCoreAddress.AccountId = _HCUAccount.Id;
                                    _HCCoreAddress.Name = _Request.AddressComponent.Name;
                                    _HCCoreAddress.ContactNumber = _Request.AddressComponent.ContactNumber;
                                    _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                                    _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                                    _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                                    if (_Request.AddressComponent.Latitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Latitude;
                                    }
                                    if (_Request.AddressComponent.Longitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Longitude;
                                    }
                                    _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                                    if (_Request.AddressComponent.CityAreaId > 0)
                                    {
                                        _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                                    }
                                    if (_Request.AddressComponent.CityId > 0)
                                    {
                                        _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                                    }
                                    if (_Request.AddressComponent.StateId > 0)
                                    {
                                        _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                                    }
                                    if (_Request.AddressComponent.CountryId > 0)
                                    {
                                        _HCCoreAddress.CountryId = (int)_Request.AddressComponent.CountryId;
                                    }
                                    _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                    _HCCoreAddress.CreatedById = _HCUAccount.Id;
                                    _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                                    _HCoreContext.SaveChanges();
                                    _AppProfileResponse.AddressReferenceId = _HCCoreAddress.Id;
                                    _AppProfileResponse.AddressReferenceKey = _HCCoreAddress.Guid;
                                }
                            }
                            _AppProfileResponse.AccountId = _HCUAccount.Id;
                            _AppProfileResponse.AccountKey = _HCUAccount.Guid;
                            _AppProfileResponse.OwnerId = _HCUAccount.OwnerId;
                            _AppProfileResponse.SubOwnerId = _HCUAccount.SubOwnerId;
                            _AppProfileResponse.CreateDate = _HCUAccount.CreateDate;
                            _AppProfileResponse.CountryId = _HCUAccount.CountryId;
                            _AppProfileResponse.MobileNumber = _Request.MobileNumber;
                            _AppProfileResponse.AccountCode = AccountCode;
                            _AppProfileResponse.StatusResponseCode = "Account created";
                            _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Success;
                            _AppProfileResponse.StatusResponseCode = "AUA106";
                            _AppProfileResponse.StatusId = HelperStatus.Default.Active;
                            return _AppProfileResponse;
                            #endregion

                        }
                    }
                    else
                    {
                        List<HCCoreAddress> _HCCoreAddresList = new List<HCCoreAddress>();
                        if (_Request.AddressComponent != null && _Request.AddressComponent.CountryId > 0 && _Request.AddressComponent.StateId > 0 && _Request.AddressComponent.CityId > 0)
                        {
                            HCCoreAddress _HCCoreAddress;
                            _HCCoreAddress = new HCCoreAddress();
                            _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreAddress.LocationTypeId = 800;
                            //_HCCoreAddress.IsPrimaryAddress = 1;
                            _HCCoreAddress.Name = _Request.AddressComponent.Name;
                            _HCCoreAddress.ContactNumber = _Request.AddressComponent.ContactNumber;
                            _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                            _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                            _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                            if (_Request.AddressComponent.Latitude != null)
                            {
                                _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Latitude;
                            }
                            if (_Request.AddressComponent.Longitude != null)
                            {
                                _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Longitude;
                            }
                            _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                            if (_Request.AddressComponent.CityAreaId > 0)
                            {
                                _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                            }
                            if (_Request.AddressComponent.CityId > 0)
                            {
                                _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                            }
                            if (_Request.AddressComponent.StateId > 0)
                            {
                                _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                            }
                            if (_Request.AddressComponent.CountryId > 0)
                            {
                                _HCCoreAddress.CountryId = (int)_Request.AddressComponent.CountryId;
                            }
                            _HCCoreAddress.CreatedById = 1;
                            _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                            _HCCoreAddresList.Add(_HCCoreAddress);
                        }
                        string UserKey = HCoreHelper.GenerateGuid();
                        string UserAccountKey = HCoreHelper.GenerateGuid();
                        _Random = new Random();
                        string AccessPin = _Random.Next(1111, 9999).ToString();
                        if (HostEnvironment == HostEnvironmentType.Test)
                        {
                            AccessPin = "1234";
                        }
                        #region Save User
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = UserKey;
                        _HCUAccountAuth.Username = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = CreateDate;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        if (_Request.CreatedById != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.CreatedById;
                        }
                        else
                        {
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            }
                        }
                        #endregion
                        #region Online Account
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = UserAccountKey;
                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                        if (_Request.OwnerId != 0)
                        {
                            _HCUAccount.OwnerId = _Request.OwnerId;
                        }
                        if (_Request.SubOwnerId != 0)
                        {
                            _HCUAccount.SubOwnerId = _Request.SubOwnerId;
                        }
                        _HCUAccount.MobileNumber = _Request.MobileNumber;
                        if (!string.IsNullOrEmpty(_Request.DisplayName))
                        {
                            _HCUAccount.DisplayName = _Request.DisplayName;
                        }
                        else
                        {
                            _HCUAccount.DisplayName = _Request.MobileNumber;
                        }
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                        if (!string.IsNullOrEmpty(_Request.FirstName))
                        {
                            _HCUAccount.FirstName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.MiddleName))
                        {
                            _HCUAccount.MiddleName = _Request.MiddleName;
                        }
                        if (!string.IsNullOrEmpty(_Request.LastName))
                        {
                            _HCUAccount.LastName = _Request.LastName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _HCUAccount.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ContactNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                            _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.Address))
                        {
                            _HCUAccount.Address = _Request.Address;
                        }
                        if (_Request.AddressLatitude != 0)
                        {
                            _HCUAccount.Latitude = _Request.AddressLatitude;
                        }
                        if (_Request.AddressLongitude != 0)
                        {
                            _HCUAccount.Longitude = _Request.AddressLongitude;
                        }
                        if (GenderId != 0)
                        {
                            _HCUAccount.GenderId = GenderId;
                        }
                        if (_Request.DateOfBirth != null)
                        {
                            _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                        }
                        if (_Request.CountryId > 0)
                        {
                            _HCUAccount.CountryId = _Request.CountryId;
                        }
                        else
                        {
                            _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                        }
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = CreateDate;
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.NumberVerificationStatusDate = CreateDate;
                        _Random = new Random();
                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                        _HCUAccount.AccountCode = AccountCode;
                        _HCUAccount.ReferralCode = _Request.MobileNumber;
                        _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }

                        if (_Request.CreatedById != 0)
                        {
                            _HCUAccount.CreatedById = _Request.CreatedById;
                        }
                        else
                        {
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            }
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.CreateDate = CreateDate;
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.User = _HCUAccountAuth;
                        if (_HCCoreAddresList.Count > 0)
                        {
                            _HCUAccount.HCCoreAddressAccount = _HCCoreAddresList;
                        }
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();
                        #endregion
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                        {
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                if (!string.IsNullOrEmpty(MerchantDisplayName))
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, _Request.MobileNumber, "Welcome to " + MerchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                                }
                                else
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, _Request.MobileNumber, "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                                }
                            }
                        }
                        if (CardId != 0 && CardMerchantId != 0 && _Request.UserReference.AccountId > 0)
                        {
                            AssignCardToUser(_HCUAccount.Id, CardId, _Request.UserReference.AccountId);
                        }


                        if (_HCCoreAddresList.Count > 0)
                        {
                            _AppProfileResponse.AddressReferenceId = _HCCoreAddresList[0].Id;
                            _AppProfileResponse.AddressReferenceKey = _HCCoreAddresList[0].Guid;
                        }
                        _AppProfileResponse.AccountId = _HCUAccount.Id;
                        _AppProfileResponse.AccountKey = UserAccountKey;
                        _AppProfileResponse.OwnerId = _Request.OwnerId;
                        _AppProfileResponse.SubOwnerId = _HCUAccount.SubOwnerId;
                        _AppProfileResponse.CreateDate = CreateDate;
                        _AppProfileResponse.CountryId = _HCUAccount.CountryId;
                        _AppProfileResponse.MobileNumber = _Request.MobileNumber;
                        _AppProfileResponse.AccountCode = AccountCode;
                        _AppProfileResponse.StatusResponseCode = "Account created";
                        _AppProfileResponse.StatusResponseCode = "AUA107";
                        _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Success;
                        _AppProfileResponse.StatusId = HelperStatus.Default.Active;
                        return _AppProfileResponse;
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("CreateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return null;
                #endregion
            }
            #endregion
        }

        internal OAppProfile.Response NewCreateAppUserAccount(OAppProfile.Request request)
        {
            OAppProfile.Response profileResponse = new();
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(request.MobileNumber))
                {
                    return new OAppProfile.Response
                    {
                        Status = HCoreConstant.ResponseStatus.Error,
                        StatusMessage = "Mobile number required",
                        StatusResponseCode = "AUA100"
                    };
                }
                DateTime dateCreated = DateTime.UtcNow;

                int genderId = 0;
                long cardId = 0;
                long cardMerchantId = 0;
                string? merchantDisplayName = "";
                using (_HCoreContext = new HCoreContext())
                {
                    if (request.OwnerId != 0)
                    {
                        merchantDisplayName = _HCoreContext.HCUAccount
                        .Where(x => x.Id == request.OwnerId && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer))
                        .Select(x => x.DisplayName)
                        .FirstOrDefault();
                    }

                    if (!string.IsNullOrEmpty(request.CardNumber) && !string.IsNullOrEmpty(request.CardSerialNumber))
                    {
                        var cardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == request.CardNumber && x.SerialNumber == request.CardSerialNumber)
                        .Select(x => new
                        {
                            CardId = x.Id,
                            CardStatusId = x.StatusId,
                            CardActiveUser = x.ActiveUserAccountId,
                            CardActiveMerchant = x.ActiveMerchantId,
                        })
                        .FirstOrDefault();

                        if (cardDetails != null)
                        {
                            if (cardDetails.CardStatusId == HelperStatus.Card.NotAssigned && cardDetails.CardActiveUser == null)
                            {
                                cardId = cardDetails.CardId;
                                cardMerchantId = cardDetails.CardActiveMerchant;
                            }
                            else
                            {
                                profileResponse.Status = HCoreConstant.ResponseStatus.Error;

                                profileResponse.StatusMessage = cardDetails.CardStatusId == HelperStatus.Card.Assigned ? "Card already assigned" :
                                    cardDetails.CardStatusId == HelperStatus.Card.Blocked ? "Card blocked. Please contact support" :
                                    "Invalid card details";

                                profileResponse.StatusResponseCode = cardDetails.CardStatusId == HelperStatus.Card.Assigned ? "AUA101" :
                                    cardDetails.CardStatusId == HelperStatus.Card.Blocked ? "AUA102" :
                                    "AUA103";

                                return profileResponse;
                            }
                        }
                        else
                        {
                            profileResponse.Status = HCoreConstant.ResponseStatus.Error;
                            profileResponse.StatusMessage = "CardId details not found";
                            profileResponse.StatusResponseCode = "AUA104";
                            return profileResponse;
                        }
                    }
                    else if (!string.IsNullOrEmpty(request.CardNumber))
                    {
                        var cardDetails = _HCoreContext.TUCard
                        .Where(x => x.CardNumber == request.CardNumber)
                        .Select(x => new
                        {
                            CardId = x.Id,
                            CardStatusId = x.StatusId,
                            CardActiveUser = x.ActiveUserAccountId,
                            CardActiveMerchant = x.ActiveMerchantId,
                        })
                        .FirstOrDefault();
                        if (cardDetails != null)
                        {
                            if (cardDetails.CardStatusId == HelperStatus.Card.NotAssigned && cardDetails.CardActiveUser == null)
                            {
                                cardId = cardDetails.CardId;
                                cardMerchantId = cardDetails.CardActiveMerchant;
                            }
                            else
                            {
                                profileResponse.Status = HCoreConstant.ResponseStatus.Error;

                                profileResponse.StatusMessage = cardDetails.CardStatusId == HelperStatus.Card.Assigned ? "Card already assigned" :
                                    cardDetails.CardStatusId == HelperStatus.Card.Blocked ? "Card blocked. Please contact support" :
                                    "Invalid card details";

                                profileResponse.StatusResponseCode = cardDetails.CardStatusId == HelperStatus.Card.Assigned ? "AUA101" :
                                    cardDetails.CardStatusId == HelperStatus.Card.Blocked ? "AUA102" :
                                    "AUA103";

                                return profileResponse;
                            }
                        }
                        else
                        {
                            profileResponse.Status = HCoreConstant.ResponseStatus.Error;
                            profileResponse.StatusMessage = "CardId details not found";
                            profileResponse.StatusResponseCode = "AUA104";
                            return profileResponse;
                        }
                    }
                    else if (!string.IsNullOrEmpty(request.CardSerialNumber))
                    {
                        var cardDetails = _HCoreContext.TUCard
                       .Where(x => x.SerialNumber == request.CardSerialNumber)
                       .Select(x => new
                       {
                           CardId = x.Id,
                           CardStatusId = x.StatusId,
                           CardActiveUser = x.ActiveUserAccountId,
                           CardActiveMerchant = x.ActiveMerchantId,
                       })
                       .FirstOrDefault();
                        if (cardDetails != null)
                        {
                            if (cardDetails.CardStatusId == HelperStatus.Card.NotAssigned && cardDetails.CardActiveUser == null)
                            {
                                cardId = cardDetails.CardId;
                                cardMerchantId = cardDetails.CardActiveMerchant;
                            }
                            else
                            {
                                profileResponse.Status = HCoreConstant.ResponseStatus.Error;

                                profileResponse.StatusMessage = cardDetails.CardStatusId == HelperStatus.Card.Assigned ? "Card already assigned" :
                                    cardDetails.CardStatusId == HelperStatus.Card.Blocked ? "Card blocked. Please contact support" :
                                    "Invalid card details";

                                profileResponse.StatusResponseCode = cardDetails.CardStatusId == HelperStatus.Card.Assigned ? "AUA101" :
                                    cardDetails.CardStatusId == HelperStatus.Card.Blocked ? "AUA102" :
                                    "AUA103";

                                return profileResponse;
                            }
                        }
                        else
                        {
                            profileResponse.Status = HCoreConstant.ResponseStatus.Error;
                            profileResponse.StatusMessage = "CardId details not found";
                            profileResponse.StatusResponseCode = "AUA104";
                            return profileResponse;
                        }
                    }
                    if (!string.IsNullOrEmpty(request.GenderCode))
                    {
                        request.GenderCode = request.GenderCode.ToLower().Trim();
                        genderId = _HCoreContext.HCCore.Where(x => x.SystemName.ToLower() == request.GenderCode && x.ParentId == HelperType.Gender && x.StatusId == HelperStatus.Default.Active).Select(x => x.Id).FirstOrDefault();
                    }

                    #region  Process Registration
                    string appAccount = _AppConfig.AppUserPrefix + request.MobileNumber;
                    long existingUserId = (from n in _HCoreContext.HCUAccountAuth
                                           where n.Username == appAccount
                                           select n.Id).FirstOrDefault();
                    if (existingUserId != 0)
                    {
                        var accountDetails = _HCoreContext.HCUAccount
                            .Where(x => x.UserId == existingUserId && x.AccountTypeId == UserAccountType.Appuser)
                            .Select(x => new
                            {
                                AccountId = x.Id,
                                AccountKey = x.Guid,
                                x.CreateDate,
                                x.OwnerId,
                                Name = x.Name,
                                Gender = x.Gender.Name,
                                EmailAddress = x.EmailAddress,
                                MobileNumber = x.MobileNumber,
                                StatusId = x.StatusId,
                                AccountCode = x.AccountCode,
                                CountryId = x.CountryId,
                            }).FirstOrDefault();
                        if (accountDetails != null)
                        {
                            if (cardId != 0 && cardMerchantId != 0)
                            {
                                AssignCardToUser(accountDetails.AccountId, cardId, request.UserReference.AccountId);
                            }
                            if (request.AddressComponent != null && request.AddressComponent.CountryId > 0 && request.AddressComponent.StateId > 0 && request.AddressComponent.CityId > 0)
                            {
                                HCCoreAddress hcCoreAddress = new HCCoreAddress
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    LocationTypeId = 800,
                                    AccountId = accountDetails.AccountId,
                                    Name = request.AddressComponent.Name,
                                    ContactNumber = request.AddressComponent.ContactNumber,
                                    EmailAddress = request.AddressComponent.EmailAddress,
                                    AddressLine1 = request.AddressComponent.AddressLine1,
                                    AddressLine2 = request.AddressComponent.AddressLine2,
                                    Latitude = request.AddressComponent.Latitude != null ? (double)request.AddressComponent.Latitude : 0,
                                    Longitude = request.AddressComponent.Longitude != null ? (double)request.AddressComponent.Longitude : 0,
                                    MapAddress = request.AddressComponent.MapAddress,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    CreatedById = accountDetails.AccountId,
                                    StatusId = HelperStatus.Default.Active,

                                };
                                if (request.AddressComponent.CityAreaId > 0)
                                {
                                    hcCoreAddress.CityAreaId = request.AddressComponent.CityAreaId;
                                }
                                if (request.AddressComponent.CityId > 0)
                                {
                                    hcCoreAddress.CityId = request.AddressComponent.CityId;
                                }
                                if (request.AddressComponent.StateId > 0)
                                {
                                    hcCoreAddress.StateId = request.AddressComponent.StateId;
                                }
                                if (request.AddressComponent.CountryId > 0)
                                {
                                    hcCoreAddress.CountryId = (int)request.AddressComponent.CountryId;
                                }

                                _HCoreContext.HCCoreAddress.Add(hcCoreAddress);
                                _HCoreContext.SaveChanges();
                                profileResponse.AddressReferenceId = hcCoreAddress.Id;
                                profileResponse.AddressReferenceKey = hcCoreAddress.Guid;
                            }

                            profileResponse.AccountId = accountDetails.AccountId;
                            profileResponse.AccountKey = accountDetails.AccountKey;
                            profileResponse.OwnerId = accountDetails.OwnerId;
                            profileResponse.CreateDate = accountDetails.CreateDate;
                            profileResponse.MobileNumber = request.MobileNumber;
                            profileResponse.StatusId = accountDetails.StatusId;
                            profileResponse.Name = accountDetails.Name;
                            profileResponse.CountryId = (int)accountDetails.CountryId;
                            profileResponse.Gender = accountDetails.Gender;
                            profileResponse.EmailAddress = accountDetails.EmailAddress;
                            profileResponse.MobileNumber = accountDetails.MobileNumber;
                            profileResponse.AccountCode = accountDetails.AccountCode;
                            profileResponse.Status = HCoreConstant.ResponseStatus.Success;
                            profileResponse.StatusResponseCode = "Account created";
                            profileResponse.StatusResponseCode = "AUA105";
                            return profileResponse;
                        }
                        else
                        {
                            var random = new Random();
                            string accessPin = random.Next(1111, 9999).ToString();
                            if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                accessPin = "1234";
                            }

                            var hcuAccount = new HCUAccount();
                            hcuAccount.Guid = HCoreHelper.GenerateGuid();
                            hcuAccount.AccountTypeId = UserAccountType.Appuser;
                            hcuAccount.AccountOperationTypeId = AccountOperationType.Online;
                            hcuAccount.UserId = existingUserId;
                            if (request.OwnerId != 0)
                            {
                                hcuAccount.OwnerId = request.OwnerId;
                            }
                            if (request.SubOwnerId != 0)
                            {
                                hcuAccount.SubOwnerId = request.SubOwnerId;
                            }

                            hcuAccount.DisplayName = !string.IsNullOrEmpty(request.DisplayName) ? request.DisplayName : request.MobileNumber;

                            if (request.DisplayName?.Length > 30)
                            {
                                hcuAccount.DisplayName = request.DisplayName.Substring(0, 29);
                            }

                            hcuAccount.AccessPin = HCoreEncrypt.EncryptHash(accessPin);

                            string accountCode = HCoreHelper.GenerateAccountCode(11, "20");
                            hcuAccount.AccountCode = accountCode;
                            hcuAccount.MobileNumber = request.MobileNumber;
                            hcuAccount.ReferralCode = request.MobileNumber;
                            if (!string.IsNullOrEmpty(request.FirstName))
                            {
                                hcuAccount.FirstName = request.FirstName;
                            }
                            if (!string.IsNullOrEmpty(request.MiddleName))
                            {
                                hcuAccount.MiddleName = request.MiddleName;
                            }
                            if (!string.IsNullOrEmpty(request.LastName))
                            {
                                hcuAccount.LastName = request.LastName;
                            }
                            hcuAccount.RegistrationSourceId = RegistrationSource.App;
                            if (request.UserReference?.AppVersionId != 0)
                            {
                                hcuAccount.AppVersionId = request.UserReference?.AppVersionId;
                            }
                            hcuAccount.CreateDate = dateCreated;
                            hcuAccount.StatusId = HelperStatus.Default.Active;
                            if (!string.IsNullOrEmpty(request.UserReference?.RequestKey))
                            {
                                hcuAccount.RequestKey = request.UserReference.RequestKey;
                            }
                            if (request.CreatedById != 0)
                            {
                                hcuAccount.CreatedById = request.CreatedById;
                            }
                            else
                            {
                                if (request.UserReference?.AccountId != 0)
                                {
                                    hcuAccount.CreatedById = request.UserReference?.AccountId;
                                }
                            }
                            if (request.CountryId > 0)
                            {
                                hcuAccount.CountryId = request.CountryId;
                            }
                            else
                            {
                                hcuAccount.CountryId = request.UserReference?.CountryId;
                            }
                            _HCoreContext.HCUAccount.Add(hcuAccount);
                            _HCoreContext.SaveChanges();

                            if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                            {
                                if (!string.IsNullOrEmpty(request.MobileNumber))
                                {
                                    string Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + accessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    if (!string.IsNullOrEmpty(merchantDisplayName))
                                    {
                                        Message = "Welcome to " + merchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + accessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    }
                                    else
                                    {
                                        Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + accessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    }
                                    if ((HostEnvironment == HostEnvironmentType.Live && request.UserReference.AppId == 4418) || (HostEnvironment == HostEnvironmentType.Test && request.UserReference.AppId == 4375))
                                    {
                                        // Sterling Live
                                        string StrSubject = "Welcome to Sterling Rewards by ThankUCash";
                                        string StrMessage = "Welcome to Sterling Rewards by ThankUCash. To redeem your reward cash, use pin: " + accessPin + ". To check you balance dial *822# to see merchants visit our website.";
                                        string FinalMobileNumber = HCoreHelper.FormatMobileNumber("234", request.MobileNumber, 10);
                                        var client = new RestSharp.RestClient("https://lcctag.sterlingapps.p.azurewebsites.net/api/ThankUCash/notify");
                                        var rRequest = new RestSharp.RestRequest();
                                        rRequest.Method = RestSharp.Method.Post;
                                        rRequest.AddHeader("accept", "application/json");
                                        rRequest.AddHeader("content-type", "application/json");
                                        rRequest.AddHeader("AppId", "1000");
                                        rRequest.AddParameter("application/json", "{\"email\":\" " + request.EmailAddress + " \", \"phoneNumber\":\" " + FinalMobileNumber + " \",\"message\":\"" + StrMessage + "\",\"subject\":\"" + StrSubject + "\"}", RestSharp.ParameterType.RequestBody);
                                        var response = client.Execute(rRequest);
                                    }
                                    else
                                    {
                                        HCoreHelper.SendSMS(SmsType.Transaction, "234", request.MobileNumber, Message, _HCUAccount.Id, null);
                                    }
                                }
                            }
                            if (cardId != 0 && cardMerchantId != 0)
                            {
                                AssignCardToUser(hcuAccount.Id, cardId, request.UserReference.AccountId);
                            }
                            if (request.AddressComponent != null && request.AddressComponent.CountryId > 0 && request.AddressComponent.StateId > 0 && request.AddressComponent.CityId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    HCCoreAddress hcCoreAddress = new HCCoreAddress();
                                    hcCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                    hcCoreAddress.LocationTypeId = 800;
                                    //_HCCoreAddress.IsPrimaryAddress = 1;
                                    hcCoreAddress.AccountId = hcuAccount.Id;
                                    hcCoreAddress.Name = request.AddressComponent.Name;
                                    hcCoreAddress.ContactNumber = request.AddressComponent.ContactNumber;
                                    hcCoreAddress.EmailAddress = request.AddressComponent.EmailAddress;
                                    hcCoreAddress.AddressLine1 = request.AddressComponent.AddressLine1;
                                    hcCoreAddress.AddressLine2 = request.AddressComponent.AddressLine2;
                                    if (request.AddressComponent.Latitude != null)
                                    {
                                        hcCoreAddress.Latitude = (double)request.AddressComponent.Latitude;
                                    }
                                    if (request.AddressComponent.Longitude != null)
                                    {
                                        hcCoreAddress.Latitude = (double)request.AddressComponent.Longitude;
                                    }
                                    hcCoreAddress.MapAddress = request.AddressComponent.MapAddress;
                                    if (request.AddressComponent.CityAreaId > 0)
                                    {
                                        hcCoreAddress.CityAreaId = request.AddressComponent.CityAreaId;
                                    }
                                    if (request.AddressComponent.CityId > 0)
                                    {
                                        hcCoreAddress.CityId = request.AddressComponent.CityId;
                                    }
                                    if (request.AddressComponent.StateId > 0)
                                    {
                                        hcCoreAddress.StateId = request.AddressComponent.StateId;
                                    }
                                    if (request.AddressComponent.CountryId > 0)
                                    {
                                        hcCoreAddress.CountryId = (int)request.AddressComponent.CountryId;
                                    }
                                    hcCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                    hcCoreAddress.StatusId = HelperStatus.Default.Active;
                                    hcCoreAddress.CreatedById = hcuAccount.Id;
                                    _HCoreContext.HCCoreAddress.Add(hcCoreAddress);
                                    _HCoreContext.SaveChanges();
                                    profileResponse.AddressReferenceId = hcCoreAddress.Id;
                                    profileResponse.AddressReferenceKey = hcCoreAddress.Guid;
                                }
                            }
                            profileResponse.AccountId = hcuAccount.Id;
                            profileResponse.AccountKey = hcuAccount.Guid;
                            profileResponse.OwnerId = hcuAccount.OwnerId;
                            profileResponse.SubOwnerId = hcuAccount.SubOwnerId;
                            profileResponse.CreateDate = hcuAccount.CreateDate;
                            profileResponse.CountryId = hcuAccount.CountryId;
                            profileResponse.MobileNumber = request.MobileNumber;
                            profileResponse.AccountCode = accountCode;
                            profileResponse.Status = HCoreConstant.ResponseStatus.Success;
                            profileResponse.StatusResponseCode = "AUA106";
                            profileResponse.StatusId = HelperStatus.Default.Active;
                            return profileResponse;
                        }
                    }
                    else
                    {
                        List<HCCoreAddress> hcCoreAddresList = new List<HCCoreAddress>();
                        if (request.AddressComponent != null && request.AddressComponent.CountryId > 0 && request.AddressComponent.StateId > 0 && request.AddressComponent.CityId > 0)
                        {
                            HCCoreAddress hcCoreAddress;
                            hcCoreAddress = new HCCoreAddress();
                            hcCoreAddress.Guid = HCoreHelper.GenerateGuid();
                            hcCoreAddress.LocationTypeId = 800;
                            //_HCCoreAddress.IsPrimaryAddress = 1;
                            hcCoreAddress.Name = request.AddressComponent.Name;
                            hcCoreAddress.ContactNumber = request.AddressComponent.ContactNumber;
                            hcCoreAddress.EmailAddress = request.AddressComponent.EmailAddress;
                            hcCoreAddress.AddressLine1 = request.AddressComponent.AddressLine1;
                            hcCoreAddress.AddressLine2 = request.AddressComponent.AddressLine2;
                            if (request.AddressComponent.Latitude != null)
                            {
                                hcCoreAddress.Latitude = (double)request.AddressComponent.Latitude;
                            }
                            if (request.AddressComponent.Longitude != null)
                            {
                                hcCoreAddress.Latitude = (double)request.AddressComponent.Longitude;
                            }
                            hcCoreAddress.MapAddress = request.AddressComponent.MapAddress;
                            if (request.AddressComponent.CityAreaId > 0)
                            {
                                hcCoreAddress.CityAreaId = request.AddressComponent.CityAreaId;
                            }
                            if (request.AddressComponent.CityId > 0)
                            {
                                hcCoreAddress.CityId = request.AddressComponent.CityId;
                            }
                            if (request.AddressComponent.StateId > 0)
                            {
                                hcCoreAddress.StateId = request.AddressComponent.StateId;
                            }
                            if (request.AddressComponent.CountryId > 0)
                            {
                                hcCoreAddress.CountryId = (int)request.AddressComponent.CountryId;
                            }
                            hcCoreAddress.CreatedById = 1;
                            hcCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                            hcCoreAddress.StatusId = HelperStatus.Default.Active;
                            hcCoreAddresList.Add(hcCoreAddress);
                        }
                        string UserKey = HCoreHelper.GenerateGuid();
                        string UserAccountKey = HCoreHelper.GenerateGuid();
                        var random = new Random();
                        string AccessPin = random.Next(1111, 9999).ToString();
                        if (HostEnvironment == HostEnvironmentType.Test)
                        {
                            AccessPin = "1234";
                        }
                        #region Save User
                        var hcuAccountAuth = new HCUAccountAuth();
                        hcuAccountAuth.Guid = UserKey;
                        hcuAccountAuth.Username = _AppConfig.AppUserPrefix + request.MobileNumber;
                        hcuAccountAuth.Password = HCoreEncrypt.EncryptHash(request.MobileNumber);
                        hcuAccountAuth.SecondaryPassword = hcuAccountAuth.Password;
                        hcuAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        hcuAccountAuth.CreateDate = dateCreated;
                        hcuAccountAuth.StatusId = HelperStatus.Default.Active;
                        if (request.CreatedById != 0)
                        {
                            hcuAccountAuth.CreatedById = request.CreatedById;
                        }
                        else
                        {
                            if (request.UserReference?.AccountId != 0)
                            {
                                hcuAccountAuth.CreatedById = request.UserReference?.AccountId;
                            }
                        }
                        #endregion
                        #region Online Account
                        var hcuAccount = new HCUAccount();
                        hcuAccount.Guid = UserAccountKey;
                        hcuAccount.AccountTypeId = UserAccountType.Appuser;
                        hcuAccount.AccountOperationTypeId = AccountOperationType.Online;
                        if (request.OwnerId != 0)
                        {
                            hcuAccount.OwnerId = request.OwnerId;
                        }
                        if (request.SubOwnerId != 0)
                        {
                            hcuAccount.SubOwnerId = request.SubOwnerId;
                        }
                        hcuAccount.MobileNumber = request.MobileNumber;
                        if (!string.IsNullOrEmpty(request.DisplayName))
                        {
                            hcuAccount.DisplayName = request.DisplayName;
                        }
                        else
                        {
                            hcuAccount.DisplayName = request.MobileNumber;
                        }
                        hcuAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                        if (!string.IsNullOrEmpty(request.FirstName))
                        {
                            hcuAccount.FirstName = request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(request.MiddleName))
                        {
                            hcuAccount.MiddleName = request.MiddleName;
                        }
                        if (!string.IsNullOrEmpty(request.LastName))
                        {
                            hcuAccount.LastName = request.LastName;
                        }
                        if (!string.IsNullOrEmpty(request.Name))
                        {
                            hcuAccount.Name = request.Name;
                        }
                        if (!string.IsNullOrEmpty(request.MobileNumber))
                        {
                            hcuAccount.MobileNumber = request.MobileNumber;
                            hcuAccount.ContactNumber = request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(request.EmailAddress))
                        {
                            hcuAccount.EmailAddress = request.EmailAddress;
                            hcuAccount.SecondaryEmailAddress = request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(request.Address))
                        {
                            hcuAccount.Address = request.Address;
                        }
                        if (request.AddressLatitude != 0)
                        {
                            hcuAccount.Latitude = request.AddressLatitude;
                        }
                        if (request.AddressLongitude != 0)
                        {
                            hcuAccount.Longitude = request.AddressLongitude;
                        }
                        if (genderId != 0)
                        {
                            hcuAccount.GenderId = genderId;
                        }
                        if (request.DateOfBirth != null)
                        {
                            hcuAccount.DateOfBirth = request.DateOfBirth;
                        }
                        if (request.CountryId > 0)
                        {
                            hcuAccount.CountryId = request.CountryId;
                        }
                        else
                        {
                            hcuAccount.CountryId = request.UserReference?.CountryId;
                        }
                        hcuAccount.EmailVerificationStatus = 0;
                        hcuAccount.EmailVerificationStatusDate = dateCreated;
                        hcuAccount.NumberVerificationStatus = 0;
                        hcuAccount.NumberVerificationStatusDate = dateCreated;
                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                        hcuAccount.AccountCode = AccountCode;
                        hcuAccount.ReferralCode = request.MobileNumber;
                        hcuAccount.RegistrationSourceId = RegistrationSource.System;
                        if (request.UserReference?.AppVersionId != 0)
                        {
                            hcuAccount.AppVersionId = request.UserReference?.AppVersionId;
                        }

                        if (request.CreatedById != 0)
                        {
                            hcuAccount.CreatedById = request.CreatedById;
                        }
                        else
                        {
                            if (request.UserReference?.AccountId != 0)
                            {
                                hcuAccount.CreatedById = request.UserReference?.AccountId;
                            }
                        }
                        hcuAccount.RequestKey = request.UserReference.RequestKey;
                        hcuAccount.CreateDate = dateCreated;
                        hcuAccount.StatusId = HelperStatus.Default.Active;
                        hcuAccount.User = hcuAccountAuth;
                        if (hcCoreAddresList.Count > 0)
                        {
                            hcuAccount.HCCoreAddressAccount = hcCoreAddresList;
                        }
                        _HCoreContext.HCUAccount.Add(hcuAccount);
                        _HCoreContext.SaveChanges();
                        #endregion
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                        {
                            if (!string.IsNullOrEmpty(request.MobileNumber))
                            {
                                if (!string.IsNullOrEmpty(merchantDisplayName))
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", request.MobileNumber, "Welcome to " + merchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                                }
                                else
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", request.MobileNumber, "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                                }
                            }
                        }
                        if (cardId != 0 && cardMerchantId != 0 && request.UserReference.AccountId > 0)
                        {
                            AssignCardToUser(hcuAccount.Id, cardId, request.UserReference.AccountId);
                        }


                        if (hcCoreAddresList.Count > 0)
                        {
                            profileResponse.AddressReferenceId = hcCoreAddresList[0].Id;
                            profileResponse.AddressReferenceKey = hcCoreAddresList[0].Guid;
                        }
                        profileResponse.AccountId = hcuAccount.Id;
                        profileResponse.AccountKey = UserAccountKey;
                        profileResponse.OwnerId = request.OwnerId;
                        profileResponse.SubOwnerId = hcuAccount.SubOwnerId;
                        profileResponse.CreateDate = dateCreated;
                        profileResponse.CountryId = hcuAccount.CountryId;
                        profileResponse.MobileNumber = request.MobileNumber;
                        profileResponse.AccountCode = AccountCode;
                        profileResponse.StatusResponseCode = "AUA107";
                        profileResponse.Status = HCoreConstant.ResponseStatus.Success;
                        profileResponse.StatusId = HelperStatus.Default.Active;
                        return profileResponse;
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("CreateAccount", _Exception, request.UserReference);
                profileResponse = new OAppProfile.Response
                {
                    Status = HCoreConstant.ResponseStatus.Error,
                    StatusMessage = "Internal Server Error",
                    StatusResponseCode = "500"
                };
            }
            #endregion
            return profileResponse;
        }

        internal OAppProfile.Response CreateAppUserAccountLite(OAppProfileLite.Request _Request)
        {
            #region Manage Exception
            try
            {
                _AppProfileResponse = new OAppProfile.Response();
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Error;
                    _AppProfileResponse.StatusMessage = "Mobile number required";
                    _AppProfileResponse.StatusResponseCode = "AUA100";
                    return _AppProfileResponse;
                }
                #region Declare
                DateTime CreateDate = HCoreHelper.GetGMTDateTime();
                #endregion
                #region Manage Operations
                int GenderId = 0;
                long CardId = 0;
                long CardMerchantId = 0;
                string MerchantDisplayName = "";
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.OwnerId != 0)
                    {
                        MerchantDisplayName = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.OwnerId && (x.AccountTypeId == UserAccountType.Merchant || x.AccountTypeId == UserAccountType.Acquirer))
                        .Select(x => x.DisplayName)
                        .FirstOrDefault();
                    }

                    if (!string.IsNullOrEmpty(_Request.GenderCode))
                    {
                        _Request.GenderCode = _Request.GenderCode.ToLower().Trim();
                        if (_Request.GenderCode == "male")
                        {
                            GenderId = Helpers.Gender.Male;
                        }
                        else if (_Request.GenderCode == "female")
                        {
                            GenderId = Helpers.Gender.Female;
                        }
                    }
                    #region  Process Registration
                    string AppAccount = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    long ExistingUserId = (from n in _HCoreContext.HCUAccountAuth
                                           where n.Username == AppAccount
                                           select n.Id).FirstOrDefault();
                    if (ExistingUserId != 0)
                    {
                        var AccountDetails = _HCoreContext.HCUAccount
                            .Where(x => x.UserId == ExistingUserId && x.AccountTypeId == UserAccountType.Appuser)
                            .Select(x => new
                            {
                                AccountId = x.Id,
                                AccountKey = x.Guid,
                                x.CreateDate,
                                x.OwnerId,
                                Name = x.Name,
                                Gender = x.Gender.Name,
                                EmailAddress = x.EmailAddress,
                                MobileNumber = x.MobileNumber,
                                StatusId = x.StatusId,
                                AccountCode = x.AccountCode,
                                CountryId = x.CountryId,
                            }).FirstOrDefault();
                        if (AccountDetails != null)
                        {
                            if (CardId != 0 && CardMerchantId != 0)
                            {
                                AssignCardToUser(AccountDetails.AccountId, CardId, _Request.UserReference.AccountId);
                            }
                            if (_Request.AddressComponent != null && _Request.AddressComponent.CountryId > 0 && _Request.AddressComponent.StateId > 0 && _Request.AddressComponent.CityId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    HCCoreAddress _HCCoreAddress;
                                    _HCCoreAddress = new HCCoreAddress();
                                    _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                    _HCCoreAddress.LocationTypeId = 800;
                                    //_HCCoreAddress.IsPrimaryAddress = 1;
                                    _HCCoreAddress.AccountId = AccountDetails.AccountId;
                                    _HCCoreAddress.Name = _Request.AddressComponent.Name;
                                    _HCCoreAddress.ContactNumber = _Request.AddressComponent.ContactNumber;
                                    _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                                    _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                                    _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                                    if (_Request.AddressComponent.Latitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Latitude;
                                    }
                                    if (_Request.AddressComponent.Longitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Longitude;
                                    }
                                    _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                                    if (_Request.AddressComponent.CityAreaId > 0)
                                    {
                                        _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                                    }
                                    if (_Request.AddressComponent.CityId > 0)
                                    {
                                        _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                                    }
                                    if (_Request.AddressComponent.StateId > 0)
                                    {
                                        _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                                    }
                                    if (_Request.AddressComponent.CountryId > 0)
                                    {
                                        _HCCoreAddress.CountryId = (int)_Request.AddressComponent.CountryId;
                                    }
                                    _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreAddress.CreatedById = AccountDetails.AccountId;
                                    _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                    _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                                    _HCoreContext.SaveChanges();
                                    _AppProfileResponse.AddressReferenceId = _HCCoreAddress.Id;
                                    _AppProfileResponse.AddressReferenceKey = _HCCoreAddress.Guid;
                                }
                            }
                            _AppProfileResponse.AccountId = AccountDetails.AccountId;
                            _AppProfileResponse.AccountKey = AccountDetails.AccountKey;
                            _AppProfileResponse.OwnerId = AccountDetails.OwnerId;
                            _AppProfileResponse.CreateDate = AccountDetails.CreateDate;
                            _AppProfileResponse.MobileNumber = _Request.MobileNumber;
                            _AppProfileResponse.StatusId = AccountDetails.StatusId;
                            _AppProfileResponse.Name = AccountDetails.Name;
                            _AppProfileResponse.CountryId = (int)AccountDetails.CountryId;
                            _AppProfileResponse.Gender = AccountDetails.Gender;
                            _AppProfileResponse.EmailAddress = AccountDetails.EmailAddress;
                            _AppProfileResponse.MobileNumber = AccountDetails.MobileNumber;
                            _AppProfileResponse.AccountCode = AccountDetails.AccountCode;
                            _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Success;
                            _AppProfileResponse.StatusResponseCode = "Account created";
                            _AppProfileResponse.StatusResponseCode = "AUA105";
                            return _AppProfileResponse;
                        }
                        else
                        {
                            #region Online Account
                            _Random = new Random();
                            string AccessPin = _Random.Next(1111, 9999).ToString();
                            if (HostEnvironment == HostEnvironmentType.Test)
                            {
                                AccessPin = "1234";
                            }
                            _HCUAccount = new HCUAccount();
                            _HCUAccount.Guid = HCoreHelper.GenerateGuid();
                            _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                            _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                            _HCUAccount.UserId = ExistingUserId;
                            if (_Request.OwnerId != 0)
                            {
                                _HCUAccount.OwnerId = _Request.OwnerId;
                            }
                            if (_Request.SubOwnerId != 0)
                            {
                                _HCUAccount.SubOwnerId = _Request.SubOwnerId;
                            }
                            if (!string.IsNullOrEmpty(_Request.DisplayName))
                            {
                                _HCUAccount.DisplayName = _Request.DisplayName;
                            }
                            else
                            {
                                _HCUAccount.DisplayName = _Request.MobileNumber;
                            }
                            if (_HCUAccount.DisplayName.Length > 30)
                            {
                                _HCUAccount.DisplayName = _HCUAccount.DisplayName.Substring(0, 29);
                            }
                            _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                            _Random = new Random();
                            string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                            _HCUAccount.AccountCode = AccountCode;
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ReferralCode = _Request.MobileNumber;
                            if (!string.IsNullOrEmpty(_Request.FirstName))
                            {
                                _HCUAccount.FirstName = _Request.FirstName;
                            }
                            if (!string.IsNullOrEmpty(_Request.MiddleName))
                            {
                                _HCUAccount.MiddleName = _Request.MiddleName;
                            }
                            if (!string.IsNullOrEmpty(_Request.LastName))
                            {
                                _HCUAccount.LastName = _Request.LastName;
                            }
                            _HCUAccount.RegistrationSourceId = RegistrationSource.App;
                            if (_Request.UserReference.AppVersionId != 0)
                            {
                                _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                            }
                            _HCUAccount.CreateDate = CreateDate;
                            _HCUAccount.StatusId = HelperStatus.Default.Active;
                            if (!string.IsNullOrEmpty(_Request.UserReference.RequestKey))
                            {
                                _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                            }
                            if (_Request.CreatedById != 0)
                            {
                                _HCUAccount.CreatedById = _Request.CreatedById;
                            }
                            else
                            {
                                if (_Request.UserReference.AccountId != 0)
                                {
                                    _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                                }
                            }
                            if (_Request.CountryId > 0)
                            {
                                _HCUAccount.CountryId = _Request.CountryId;
                            }
                            else
                            {
                                _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                            }
                            _HCoreContext.HCUAccount.Add(_HCUAccount);
                            _HCoreContext.SaveChanges();

                            if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                            {
                                if (!string.IsNullOrEmpty(_Request.MobileNumber))
                                {
                                    string Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    if (!string.IsNullOrEmpty(MerchantDisplayName))
                                    {
                                        Message = "Welcome to " + MerchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    }
                                    else
                                    {
                                        Message = "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app";
                                    }
                                    if ((HostEnvironment == HostEnvironmentType.Live && _Request.UserReference.AppId == 4418) || (HostEnvironment == HostEnvironmentType.Test && _Request.UserReference.AppId == 4375))
                                    {
                                        // Sterling Live
                                        string StrSubject = "Welcome to Sterling Rewards by ThankUCash";
                                        string StrMessage = "Welcome to Sterling Rewards by ThankUCash. To redeem your reward cash, use pin: " + AccessPin + ". To check you balance dial *822# to see merchants visit our website.";
                                        string FinalMobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber, 10);
                                        var client = new RestSharp.RestClient("https://lcctag.sterlingapps.p.azurewebsites.net/api/ThankUCash/notify");
                                        var request = new RestSharp.RestRequest();
                                        request.Method = RestSharp.Method.Post;
                                        request.AddHeader("accept", "application/json");
                                        request.AddHeader("content-type", "application/json");
                                        if (HostEnvironment == HostEnvironmentType.Live)
                                        {
                                            request.AddHeader("AppId", "1000");
                                        }
                                        else
                                        {
                                            request.AddHeader("AppId", "1000");
                                        }
                                        request.AddParameter("application/json", "{\"email\":\" " + _Request.EmailAddress + " \", \"phoneNumber\":\" " + FinalMobileNumber + " \",\"message\":\"" + StrMessage + "\",\"subject\":\"" + StrSubject + "\"}", RestSharp.ParameterType.RequestBody);
                                        var response = client.Execute(request);
                                    }
                                    else
                                    {
                                        HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, Message, _HCUAccount.Id, null);
                                    }
                                }
                            }
                            if (CardId != 0 && CardMerchantId != 0)
                            {
                                AssignCardToUser(_HCUAccount.Id, CardId, _Request.UserReference.AccountId);
                            }
                            if (_Request.AddressComponent != null && _Request.AddressComponent.CountryId > 0 && _Request.AddressComponent.StateId > 0 && _Request.AddressComponent.CityId > 0)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    HCCoreAddress _HCCoreAddress;
                                    _HCCoreAddress = new HCCoreAddress();
                                    _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                                    _HCCoreAddress.LocationTypeId = 800;
                                    //_HCCoreAddress.IsPrimaryAddress = 1;
                                    _HCCoreAddress.AccountId = _HCUAccount.Id;
                                    _HCCoreAddress.Name = _Request.AddressComponent.Name;
                                    _HCCoreAddress.ContactNumber = _Request.AddressComponent.ContactNumber;
                                    _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                                    _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                                    _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                                    if (_Request.AddressComponent.Latitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Latitude;
                                    }
                                    if (_Request.AddressComponent.Longitude != null)
                                    {
                                        _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Longitude;
                                    }
                                    _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                                    if (_Request.AddressComponent.CityAreaId > 0)
                                    {
                                        _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                                    }
                                    if (_Request.AddressComponent.CityId > 0)
                                    {
                                        _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                                    }
                                    if (_Request.AddressComponent.StateId > 0)
                                    {
                                        _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                                    }
                                    if (_Request.AddressComponent.CountryId > 0)
                                    {
                                        _HCCoreAddress.CountryId = (int)_Request.AddressComponent.CountryId;
                                    }
                                    _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                                    _HCCoreAddress.CreatedById = _HCUAccount.Id;
                                    _HCoreContext.HCCoreAddress.Add(_HCCoreAddress);
                                    _HCoreContext.SaveChanges();
                                    _AppProfileResponse.AddressReferenceId = _HCCoreAddress.Id;
                                    _AppProfileResponse.AddressReferenceKey = _HCCoreAddress.Guid;
                                }
                            }
                            _AppProfileResponse.AccountId = _HCUAccount.Id;
                            _AppProfileResponse.AccountKey = _HCUAccount.Guid;
                            _AppProfileResponse.OwnerId = _HCUAccount.OwnerId;
                            _AppProfileResponse.SubOwnerId = _HCUAccount.SubOwnerId;
                            _AppProfileResponse.CreateDate = _HCUAccount.CreateDate;
                            _AppProfileResponse.CountryId = _HCUAccount.CountryId;
                            _AppProfileResponse.MobileNumber = _Request.MobileNumber;
                            _AppProfileResponse.AccountCode = AccountCode;
                            _AppProfileResponse.StatusResponseCode = "Account created";
                            _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Success;
                            _AppProfileResponse.StatusResponseCode = "AUA106";
                            _AppProfileResponse.StatusId = HelperStatus.Default.Active;
                            return _AppProfileResponse;
                            #endregion

                        }
                    }
                    else
                    {
                        List<HCCoreAddress> _HCCoreAddresList = new List<HCCoreAddress>();
                        if (_Request.AddressComponent != null && _Request.AddressComponent.CountryId > 0 && _Request.AddressComponent.StateId > 0 && _Request.AddressComponent.CityId > 0)
                        {
                            HCCoreAddress _HCCoreAddress;
                            _HCCoreAddress = new HCCoreAddress();
                            _HCCoreAddress.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreAddress.LocationTypeId = 800;
                            //_HCCoreAddress.IsPrimaryAddress = 1;
                            _HCCoreAddress.Name = _Request.AddressComponent.Name;
                            _HCCoreAddress.ContactNumber = _Request.AddressComponent.ContactNumber;
                            _HCCoreAddress.EmailAddress = _Request.AddressComponent.EmailAddress;
                            _HCCoreAddress.AddressLine1 = _Request.AddressComponent.AddressLine1;
                            _HCCoreAddress.AddressLine2 = _Request.AddressComponent.AddressLine2;
                            if (_Request.AddressComponent.Latitude != null)
                            {
                                _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Latitude;
                            }
                            if (_Request.AddressComponent.Longitude != null)
                            {
                                _HCCoreAddress.Latitude = (double)_Request.AddressComponent.Longitude;
                            }
                            _HCCoreAddress.MapAddress = _Request.AddressComponent.MapAddress;
                            if (_Request.AddressComponent.CityAreaId > 0)
                            {
                                _HCCoreAddress.CityAreaId = _Request.AddressComponent.CityAreaId;
                            }
                            if (_Request.AddressComponent.CityId > 0)
                            {
                                _HCCoreAddress.CityId = _Request.AddressComponent.CityId;
                            }
                            if (_Request.AddressComponent.StateId > 0)
                            {
                                _HCCoreAddress.StateId = _Request.AddressComponent.StateId;
                            }
                            if (_Request.AddressComponent.CountryId > 0)
                            {
                                _HCCoreAddress.CountryId = (int)_Request.AddressComponent.CountryId;
                            }
                            _HCCoreAddress.CreatedById = 1;
                            _HCCoreAddress.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreAddress.StatusId = HelperStatus.Default.Active;
                            _HCCoreAddresList.Add(_HCCoreAddress);
                        }

                        string UserKey = HCoreHelper.GenerateGuid();
                        string UserAccountKey = HCoreHelper.GenerateGuid();
                        _Random = new Random();
                        string AccessPin = _Random.Next(1111, 9999).ToString();
                        if (HostEnvironment == HostEnvironmentType.Test)
                        {
                            AccessPin = "1234";
                        }
                        #region Save User
                        _HCUAccountAuth = new HCUAccountAuth();
                        _HCUAccountAuth.Guid = UserKey;
                        _HCUAccountAuth.Username = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                        _HCUAccountAuth.Password = HCoreEncrypt.EncryptHash(_Request.MobileNumber);
                        _HCUAccountAuth.SecondaryPassword = _HCUAccountAuth.Password;
                        _HCUAccountAuth.SystemPassword = HCoreEncrypt.EncryptHash(HCoreHelper.GenerateGuid());
                        _HCUAccountAuth.CreateDate = CreateDate;
                        _HCUAccountAuth.StatusId = HelperStatus.Default.Active;
                        if (_Request.CreatedById != 0)
                        {
                            _HCUAccountAuth.CreatedById = _Request.CreatedById;
                        }
                        else
                        {
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccountAuth.CreatedById = _Request.UserReference.AccountId;
                            }
                        }
                        #endregion
                        #region Online Account
                        _HCUAccount = new HCUAccount();
                        _HCUAccount.Guid = UserAccountKey;
                        _HCUAccount.AccountTypeId = UserAccountType.Appuser;
                        _HCUAccount.AccountOperationTypeId = AccountOperationType.Online;
                        if (_Request.OwnerId != 0)
                        {
                            _HCUAccount.OwnerId = _Request.OwnerId;
                        }
                        if (_Request.SubOwnerId != 0)
                        {
                            _HCUAccount.SubOwnerId = _Request.SubOwnerId;
                        }
                        _HCUAccount.MobileNumber = _Request.MobileNumber;
                        if (!string.IsNullOrEmpty(_Request.DisplayName))
                        {
                            _HCUAccount.DisplayName = _Request.DisplayName;
                        }
                        else
                        {
                            _HCUAccount.DisplayName = _Request.MobileNumber;
                        }
                        _HCUAccount.AccessPin = HCoreEncrypt.EncryptHash(AccessPin);
                        if (!string.IsNullOrEmpty(_Request.FirstName))
                        {
                            _HCUAccount.FirstName = _Request.FirstName;
                        }
                        if (!string.IsNullOrEmpty(_Request.MiddleName))
                        {
                            _HCUAccount.MiddleName = _Request.MiddleName;
                        }
                        if (!string.IsNullOrEmpty(_Request.LastName))
                        {
                            _HCUAccount.LastName = _Request.LastName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Name))
                        {
                            _HCUAccount.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            _HCUAccount.MobileNumber = _Request.MobileNumber;
                            _HCUAccount.ContactNumber = _Request.MobileNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            _HCUAccount.EmailAddress = _Request.EmailAddress;
                            _HCUAccount.SecondaryEmailAddress = _Request.EmailAddress;
                        }
                        if (!string.IsNullOrEmpty(_Request.Address))
                        {
                            _HCUAccount.Address = _Request.Address;
                        }
                        if (_Request.AddressLatitude != 0)
                        {
                            _HCUAccount.Latitude = _Request.AddressLatitude;
                        }
                        if (_Request.AddressLongitude != 0)
                        {
                            _HCUAccount.Longitude = _Request.AddressLongitude;
                        }
                        if (GenderId != 0)
                        {
                            _HCUAccount.GenderId = GenderId;
                        }
                        if (_Request.DateOfBirth != null)
                        {
                            _HCUAccount.DateOfBirth = _Request.DateOfBirth;
                        }
                        if (_Request.CountryId > 0)
                        {
                            _HCUAccount.CountryId = _Request.CountryId;
                        }
                        else
                        {
                            _HCUAccount.CountryId = (int?)_Request.UserReference.CountryId;
                        }
                        _HCUAccount.EmailVerificationStatus = 0;
                        _HCUAccount.EmailVerificationStatusDate = CreateDate;
                        _HCUAccount.NumberVerificationStatus = 0;
                        _HCUAccount.NumberVerificationStatusDate = CreateDate;
                        _Random = new Random();
                        //string AccountCode = "20" + _Random.Next(0000000, 9999999).ToString() + _Random.Next(00, 99).ToString();
                        string AccountCode = HCoreHelper.GenerateAccountCode(11, "20");
                        _HCUAccount.AccountCode = AccountCode;
                        _HCUAccount.ReferralCode = _Request.MobileNumber;
                        _HCUAccount.RegistrationSourceId = RegistrationSource.System;
                        if (_Request.UserReference.AppVersionId != 0)
                        {
                            _HCUAccount.AppVersionId = _Request.UserReference.AppVersionId;
                        }

                        if (_Request.CreatedById != 0)
                        {
                            _HCUAccount.CreatedById = _Request.CreatedById;
                        }
                        else
                        {
                            if (_Request.UserReference.AccountId != 0)
                            {
                                _HCUAccount.CreatedById = _Request.UserReference.AccountId;
                            }
                        }
                        _HCUAccount.RequestKey = _Request.UserReference.RequestKey;
                        _HCUAccount.CreateDate = CreateDate;
                        _HCUAccount.StatusId = HelperStatus.Default.Active;
                        _HCUAccount.User = _HCUAccountAuth;
                        if (_HCCoreAddresList.Count > 0)
                        {
                            _HCUAccount.HCCoreAddressAccount = _HCCoreAddresList;
                        }
                        _HCoreContext.HCUAccount.Add(_HCUAccount);
                        _HCoreContext.SaveChanges();
                        #endregion

                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Local)
                        {
                            if (!string.IsNullOrEmpty(_Request.MobileNumber))
                            {
                                if (!string.IsNullOrEmpty(MerchantDisplayName))
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, "Welcome to " + MerchantDisplayName + " Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                                }
                                else
                                {
                                    HCoreHelper.SendSMS(SmsType.Transaction, "234", _Request.MobileNumber, "Welcome to Thank U Cash. To redeem your cash, use pin: " + AccessPin + ". Download TUC App to see stores and more benefits: https://bit.ly/tuc-app", _HCUAccount.Id, null);
                                }
                            }
                        }
                        if (CardId != 0 && CardMerchantId != 0 && _Request.UserReference.AccountId > 0)
                        {
                            AssignCardToUser(_HCUAccount.Id, CardId, _Request.UserReference.AccountId);
                        }

                        if (_HCCoreAddresList.Count > 0)
                        {
                            _AppProfileResponse.AddressReferenceId = _HCCoreAddresList[0].Id;
                            _AppProfileResponse.AddressReferenceKey = _HCCoreAddresList[0].Guid;
                        }
                        _AppProfileResponse.AccountId = _HCUAccount.Id;
                        _AppProfileResponse.AccountKey = UserAccountKey;
                        _AppProfileResponse.OwnerId = _Request.OwnerId;
                        _AppProfileResponse.SubOwnerId = _HCUAccount.SubOwnerId;
                        _AppProfileResponse.CreateDate = CreateDate;
                        _AppProfileResponse.CountryId = _HCUAccount.CountryId;
                        _AppProfileResponse.MobileNumber = _Request.MobileNumber;
                        _AppProfileResponse.AccountCode = AccountCode;
                        _AppProfileResponse.StatusResponseCode = "Account created";
                        _AppProfileResponse.StatusResponseCode = "AUA107";
                        _AppProfileResponse.Status = HCoreConstant.ResponseStatus.Success;
                        _AppProfileResponse.StatusId = HelperStatus.Default.Active;
                        return _AppProfileResponse;
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("CreateAccount", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return null;
                #endregion
            }
            #endregion
        }

    }
}
