//==================================================================================
// FileName: FrameworkConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to configuration
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.Operations.Resources;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Operations.Resources.Resources;

namespace HCore.Operations.Framework
{
    /// <summary>
    /// Class FrameworkConfiguration.
    /// </summary>
    internal class FrameworkConfiguration
    {
        OSystemConfiguration.Response _ConfigurationResponse;
        HCoreContext _HCoreContext;
        HCCoreCommon _HCCoreCommon;
        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfiguration(OConfigurationManager.Save _Request)
        {
            #region Manage Exception
            try
            {


                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                int? DataTypeId = HCoreHelper.GetSystemHelperId(_Request.DataTypeCode, _Request.UserReference);
                int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);

                //bool isNum = double.TryParse(_Request.PrimaryValue, out double num);
                //if (!isNum)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC007", ResourceItems.HC007);
                //}

                if (StatusId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC100", ResourceItems.HCC100);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Name))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC101", ResourceItems.HCC101);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.SystemName))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC102", ResourceItems.HCC102);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.Description))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC103", ResourceItems.HCC103);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.PrimaryValue))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC104", ResourceItems.HCC104);
                    #endregion
                }
                else if (DataTypeId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC105", ResourceItems.HCC105);
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        long CheckConfiguration = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.Configuration && x.SystemName == _Request.SystemName).Select(x => x.Id).FirstOrDefault();
                        if (CheckConfiguration == 0)
                        {
                            _HCCoreCommon = new HCCoreCommon();
                            _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreCommon.TypeId = HelperType.Configuration;
                            _HCCoreCommon.Name = _Request.Name;
                            _HCCoreCommon.SystemName = _Request.SystemName;
                            _HCCoreCommon.Value = _Request.PrimaryValue;
                            _HCCoreCommon.SubValue = _Request.SecondaryValue;
                            _HCCoreCommon.Description = _Request.Description;
                            _HCCoreCommon.HelperId = HelperId;
                            _HCCoreCommon.SubTypeId = DataTypeId;
                            _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                            _HCCoreCommon.StatusId = (int)StatusId;
                            _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            using (_HCoreContext = new HCoreContext())
                            {
                                _HCCoreCommon = new HCCoreCommon();
                                _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                                _HCCoreCommon.TypeId = HelperType.ConfigurationValue;
                                _HCCoreCommon.ParentId = _HCCoreCommon.Id;
                                _HCCoreCommon.HelperId = HelperId;
                                _HCCoreCommon.Value = _Request.PrimaryValue;
                                _HCCoreCommon.SubValue = _Request.SecondaryValue;
                                _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                                _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                                _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                                _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _HCCoreCommon.Guid, "HCC107", ResourceItems.HCC107);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC106", ResourceItems.HCC106);
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateConfiguration(OConfigurationManager.Save _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                int? DataTypeId = HCoreHelper.GetSystemHelperId(_Request.DataTypeCode, _Request.UserReference);
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC108", ResourceItems.HCC108);
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.Configuration && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (ConfigurationDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.Name) && ConfigurationDetails.Name != _Request.Name)
                            {
                                ConfigurationDetails.Name = _Request.Name;
                            }
                            if (!string.IsNullOrEmpty(_Request.SystemName) && ConfigurationDetails.SystemName != _Request.SystemName)
                            {
                                ConfigurationDetails.SystemName = _Request.SystemName;
                            }
                            //if (string.IsNullOrEmpty(_Request.PrimaryValue) && ConfigurationDetails.Value != _Request.PrimaryValue)
                            //{
                            //    ConfigurationDetails.Value = _Request.PrimaryValue;
                            //}
                            //if (string.IsNullOrEmpty(_Request.SecondaryValue) && ConfigurationDetails.SubValue != _Request.SecondaryValue)
                            //{
                            //    ConfigurationDetails.SubValue = _Request.SecondaryValue;
                            //}
                            if (!string.IsNullOrEmpty(_Request.Description) && ConfigurationDetails.Description != _Request.Description)
                            {
                                ConfigurationDetails.Description = _Request.Description;
                            }
                            if (DataTypeId != null && ConfigurationDetails.SubTypeId != DataTypeId)
                            {
                                ConfigurationDetails.SubTypeId = DataTypeId;
                            }
                            if (StatusId != null && ConfigurationDetails.StatusId != StatusId)
                            {
                                ConfigurationDetails.StatusId = (int)StatusId;
                            }
                            ConfigurationDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            ConfigurationDetails.ModifyById = _Request.UserReference.AccountId;
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC110", ResourceItems.HCC110);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC109", ResourceItems.HCC109);
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteConfiguration(OConfigurationManager.Save _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC108", ResourceItems.HCC108);
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.Configuration && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (ConfigurationDetails != null)
                        {
                            var ConfigValues = _HCoreContext.HCCoreCommon.Where(x => x.ParentId == ConfigurationDetails.Id).ToList();
                            if (ConfigValues.Count > 0)
                            {
                                _HCoreContext.HCCoreCommon.RemoveRange(ConfigValues);
                                _HCoreContext.SaveChanges();
                                using (_HCoreContext = new HCoreContext())
                                {
                                    HCCoreCommon Configuration = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.Configuration && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                    _HCoreContext.HCCoreCommon.Remove(Configuration);
                                    HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                    _HCoreContext.SaveChanges();
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC111", ResourceItems.HCC111);
                                    #endregion
                                }
                            }
                            else
                            {
                                _HCoreContext.HCCoreCommon.Remove(ConfigurationDetails);
                                HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                                _HCoreContext.SaveChanges();
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC111", ResourceItems.HCC111);
                                #endregion
                            }
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC109", ResourceItems.HCC109);
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfigurationValue(OConfigurationManager.Save _Request)
        {
            #region Manage Exception
            try
            {
                int? HelperId = HCoreHelper.GetSystemHelperId(_Request.HelperCode, _Request.UserReference);
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC108", ResourceItems.HCC108);
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.PrimaryValue))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC104", ResourceItems.HCC104);
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.Configuration && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (ConfigurationDetails != null)
                        {
                            if (!string.IsNullOrEmpty(_Request.PrimaryValue) && ConfigurationDetails.Value != _Request.PrimaryValue)
                            {
                                ConfigurationDetails.Value = _Request.PrimaryValue;
                            }
                            if (!string.IsNullOrEmpty(_Request.SecondaryValue) && ConfigurationDetails.SubValue != _Request.SecondaryValue)
                            {
                                ConfigurationDetails.SubValue = _Request.SecondaryValue;
                            }
                            if (HelperId != null && ConfigurationDetails.HelperId != HelperId)
                            {
                                ConfigurationDetails.HelperId = HelperId;
                            }
                            ConfigurationDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            ConfigurationDetails.ModifyById = _Request.UserReference.AccountId;
                            var ExistingActiveValues = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.ConfigurationValue && x.ParentId == ConfigurationDetails.Id && x.StatusId == HelperStatus.Default.Active).ToList();
                            if (ExistingActiveValues.Count > 0)
                            {
                                foreach (var ExistingActiveValue in ExistingActiveValues)
                                {
                                    ExistingActiveValue.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    ExistingActiveValue.ModifyById = _Request.UserReference.AccountId;
                                    ExistingActiveValue.StatusId = HelperStatus.Default.Blocked;
                                }
                            }
                            _HCCoreCommon = new HCCoreCommon();
                            _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreCommon.TypeId = HelperType.ConfigurationValue;
                            _HCCoreCommon.ParentId = ConfigurationDetails.Id;
                            _HCCoreCommon.HelperId = HelperId;
                            _HCCoreCommon.Value = _Request.PrimaryValue;
                            _HCCoreCommon.SubValue = _Request.SecondaryValue;
                            _HCCoreCommon.Description = _Request.Description;
                            _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                            _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC112", ResourceItems.HCC112);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC109", ResourceItems.HCC109);
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Saves the type of the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfigurationAccountType(OConfigurationManager.Save _Request)
        {
            #region Manage Exception
            try
            {
                int? AccountTypeId = HCoreHelper.GetSystemHelperId(_Request.AccountTypeCode, _Request.UserReference);
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC108", ResourceItems.HCC108);
                    #endregion
                }
                else if (AccountTypeId == null)
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC114", ResourceItems.HCC114);
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon ConfigurationDetails = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.Configuration && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (ConfigurationDetails != null)
                        {
                            _HCCoreCommon = new HCCoreCommon();
                            _HCCoreCommon.Guid = HCoreHelper.GenerateGuid();
                            _HCCoreCommon.TypeId = HelperType.ConfigurationValue;
                            _HCCoreCommon.ParentId = ConfigurationDetails.Id;
                            _HCCoreCommon.HelperId = AccountTypeId;
                            _HCCoreCommon.CreateDate = HCoreHelper.GetGMTDateTime();
                            _HCCoreCommon.CreatedById = _Request.UserReference.AccountId;
                            _HCCoreCommon.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.HCCoreCommon.Add(_HCCoreCommon);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC115", ResourceItems.HCC115);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC109", ResourceItems.HCC109);
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the type of the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteConfigurationAccountType(OConfigurationManager.Save _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC108", ResourceItems.HCC108);
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        HCCoreCommon Details = _HCoreContext.HCCoreCommon.Where(x => x.TypeId == HelperType.ConfigurationAccountType && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                        if (Details != null)
                        {
                            _HCoreContext.HCCoreCommon.Remove(Details);
                            HCoreHelper.LogActivity(_HCoreContext, _Request.UserReference);
                            _HCoreContext.SaveChanges();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC116", ResourceItems.HCC116);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC117", ResourceItems.HCC117);
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        ///Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfiguration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "!=");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreCommon
                                                 where x.TypeId == HelperType.Configuration
                                                 select new OConfigurationManager.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     DataTypeCode = x.SubType.SystemName,
                                                     DataTypeName = x.SubType.Name,
                                                     Name = x.Name,
                                                     SystemName = x.SystemName,
                                                     PrimaryValue = x.Value,
                                                     SecondaryValue = x.SubValue,
                                                     HelperCode = x.Helper.SystemName,
                                                     HelperName = x.Helper.Name,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                              .Where(_Request.SearchCondition)
                                                      .Count();
                    }

                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OConfigurationManager.List> Data = (from x in _HCoreContext.HCCoreCommon
                                                             where x.TypeId == HelperType.Configuration
                                                             select new OConfigurationManager.List
                                                             {
                                                                 ReferenceId = x.Id,
                                                                 ReferenceKey = x.Guid,

                                                                 DataTypeCode = x.SubType.SystemName,
                                                                 DataTypeName = x.SubType.Name,
                                                                 Name = x.Name,
                                                                 SystemName = x.SystemName,
                                                                 PrimaryValue = x.Value,
                                                                 SecondaryValue = x.SubValue,
                                                                 HelperCode = x.Helper.SystemName,
                                                                 HelperName = x.Helper.Name,

                                                                 CreateDate = x.CreateDate,
                                                                 CreatedByKey = x.CreatedBy.Guid,
                                                                 CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                 ModifyDate = x.ModifyDate,
                                                                 ModifyByKey = x.ModifyBy.Guid,
                                                                 ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                 StatusId = x.StatusId,
                                                                 StatusCode = x.Status.SystemName,
                                                                 StatusName = x.Status.Name
                                                             })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        ///Description:  Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfiguration(OConfigurationManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OConfigurationManager.List Data = (from x in _HCoreContext.HCCoreCommon
                                                       where x.Guid == _Request.ReferenceKey
                                                       select new OConfigurationManager.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,

                                                           DataTypeCode = x.SubType.SystemName,
                                                           DataTypeName = x.SubType.Name,
                                                           Name = x.Name,
                                                           SystemName = x.SystemName,

                                                           Description = x.Description,

                                                           PrimaryValue = x.Value,
                                                           SecondaryValue = x.SubValue,

                                                           HelperCode = x.Helper.SystemName,
                                                           HelperName = x.Helper.Name,


                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                           ModifyDate = x.ModifyDate,
                                                           ModifyByKey = x.ModifyBy.Guid,
                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       }).FirstOrDefault();
                    _HCoreContext.Dispose();
                    if (Data != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HCC113", ResourceItems.HCC113);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC109", ResourceItems.HCC109);
                        #endregion
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        ///Description:  Gets the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfigurationValue(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "!=");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreCommon
                                                 where x.TypeId == HelperType.ConfigurationValue
                                                 select new OConfigurationManager.SubList
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     ConfigurationKey = x.Parent.Guid,

                                                     PrimaryValue = x.Value,
                                                     SecondaryValue = x.SubValue,

                                                     HelperCode = x.Helper.SystemName,
                                                     HelperName = x.Helper.Name,

                                                     Description = x.Description,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                              .Where(_Request.SearchCondition)
                                                      .Count();
                    }

                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OConfigurationManager.SubList> Data = (from x in _HCoreContext.HCCoreCommon
                                                                where x.TypeId == HelperType.ConfigurationValue
                                                                select new OConfigurationManager.SubList
                                                                {
                                                                    ReferenceId = x.Id,
                                                                    ReferenceKey = x.Guid,

                                                                    ConfigurationKey = x.Parent.Guid,

                                                                    HelperCode = x.Helper.SystemName,
                                                                    HelperName = x.Helper.Name,

                                                                    PrimaryValue = x.Value,
                                                                    SecondaryValue = x.SubValue,
                                                                    Description = x.Description,

                                                                    CreateDate = x.CreateDate,
                                                                    CreatedByKey = x.CreatedBy.Guid,
                                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                    ModifyDate = x.ModifyDate,
                                                                    ModifyByKey = x.ModifyBy.Guid,
                                                                    ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                    StatusId = x.StatusId,
                                                                    StatusCode = x.Status.SystemName,
                                                                    StatusName = x.Status.Name
                                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfigurationValue", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        ///Description:  Gets the type of the configuration account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfigurationAccountType(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "!=");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCCoreCommon
                                                 where x.TypeId == HelperType.ConfigurationAccountType
                                                 select new OConfigurationManager.AccountType
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     AccountTypeCode = x.Helper.SystemName,
                                                     AccountTypeName = x.Helper.Name,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                              .Where(_Request.SearchCondition)
                                                      .Count();
                    }

                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OConfigurationManager.AccountType> Data = (from x in _HCoreContext.HCCoreCommon
                                                                    select new OConfigurationManager.AccountType
                                                                    {
                                                                        ReferenceId = x.Id,
                                                                        ReferenceKey = x.Guid,

                                                                        AccountTypeCode = x.Helper.SystemName,
                                                                        AccountTypeName = x.Helper.Name,

                                                                        CreateDate = x.CreateDate,
                                                                        CreatedByKey = x.CreatedBy.Guid,
                                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                        ModifyDate = x.ModifyDate,
                                                                        ModifyByKey = x.ModifyBy.Guid,
                                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                        StatusId = x.StatusId,
                                                                        StatusCode = x.Status.SystemName,
                                                                        StatusName = x.Status.Name
                                                                    })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfigurationAccountType", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        ///Description:  Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfiguration(OSystemConfiguration.Request _Request)
        {
            _ConfigurationResponse = new OSystemConfiguration.Response();
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.HCCoreCommon
                                          where n.SystemName == _Request.ConfigurationCode
                                          && n.StatusId == HelperStatus.Default.Active
                                          && n.TypeId == HelperType.Configuration
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              IsDefaultValue = 1,
                                              TypeCode = n.Helper.SystemName,
                                              TypeName = n.Helper.Name,
                                          }).FirstOrDefault();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        string ConfigurationValue = (from n in _HCoreContext.HCCoreCommon
                                                     where n.ParentId == _Configuration.ReferenceId && n.SubParentId == null && n.StatusId == HelperStatus.Default.Active && n.TypeId == HelperType.ConfigurationValue
                                                     select n.Value).FirstOrDefault();
                        if (!string.IsNullOrEmpty(ConfigurationValue))
                        {
                            ConfigValue = ConfigurationValue;
                        }
                        if (_Request.UserAccountId != 0)
                        {
                            string CountryConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                where n.CommonId == _Configuration.ReferenceId
                                                                && n.AccountId == _Request.UserAccountId
                                                                && n.StatusId == HelperStatus.Default.Active
                                                                && n.TypeId == HelperType.ConfigurationValue
                                                                select n.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(CountryConfigurationValue))
                            {
                                ConfigValue = CountryConfigurationValue;
                            }
                            else
                            {
                                if (_Request.ParentId != 0)
                                {
                                    string ParentConfigurationValue = (from n in _HCoreContext.HCUAccountParameter
                                                                       where n.CommonId == _Configuration.ReferenceId
                                                                       && n.AccountId == _Request.ParentId
                                                                       && n.StatusId == HelperStatus.Default.Active
                                                                       && n.TypeId == HelperType.ConfigurationValue
                                                                       select n.Value).FirstOrDefault();
                                    if (!string.IsNullOrEmpty(ParentConfigurationValue))
                                    {
                                        ConfigValue = ParentConfigurationValue;
                                    }
                                }
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(ConfigValue))
                        {
                            _ConfigurationResponse.Value = ConfigValue;
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ConfigurationResponse, "HC902", "Configuration loaded");
                        }
                        else
                        {
                            _ConfigurationResponse.Value = "0";
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ConfigurationResponse, "HC901", "Configuration loaded");
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC900", "Configuration details not found");
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {

                #region  Log Exception
                return HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
    }
}
