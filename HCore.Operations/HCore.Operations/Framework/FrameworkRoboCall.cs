//==================================================================================
// FileName: FrameworkRoboCall.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to robo call
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using Newtonsoft.Json;
using RestSharp;
using static HCore.Helper.HCoreConstant;
using System.Text;
using System.Net;
using System.IO;

namespace HCore.Operations.Framework
{
    public static class FrameworkRoboCall
    {
        /// <summary>
        /// Sends the code
        /// </summary>
        /// <param name="MobileNumber"></param>
        /// <param name="AccessCode"></param>
        /// <param name="RequestToken"></param>
        /// <param name="CountryIsd"></param>
        /// <param name="Message"></param>
        internal static void SendCode(string MobileNumber, string? AccessCode, string? RequestToken, string? CountryIsd, string? Message)
        {
            try
            {
                var client = new RestSharp.RestClient("https://app.smartsmssolutions.com/io/api/client/v1/voiceotp/send/");
                var request = new RestSharp.RestRequest();
                request.Method = RestSharp.Method.Post;
                request.AlwaysMultipartFormData = true;
                request.AddHeader("Content-Type", "multipart/form-data");
                request.AddParameter("token", "8GySmFbQIkOVkjx0vtLjMkp3nCwFqPAw1LTEXNWe2qslgeToV4", ParameterType.RequestBody);
                request.AddParameter("phone", MobileNumber, ParameterType.RequestBody);
                request.AddParameter("otp", AccessCode, ParameterType.RequestBody);
                request.AddParameter("class", "LXNBWB05H2", ParameterType.RequestBody);
                request.AddParameter("ref_id", RequestToken, ParameterType.RequestBody);

                var response = client.Execute(request);
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                }
                else
                {
                    #region Send Message
                    HCoreHelper.SendSMS(SmsType.Transaction, CountryIsd, MobileNumber, Message, 0, null);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("SendCode", _Exception);
            }
        }
    }
}

