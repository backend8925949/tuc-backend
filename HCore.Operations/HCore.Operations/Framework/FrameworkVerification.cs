//==================================================================================
// FileName: FrameworkVerification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to verification
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations.Framework
{
    public class FrameworkVerification
    {
        OCoreVerificationManager.Response _VerificationResponse;
        HCoreContext _HCoreContext;
        HCCoreVerification _HCCoreVerification;
        Random _Random;
        #region Manage Requests
        /// <summary>
        /// Description: Requests the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestOtp(OCoreVerificationManager.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.Type == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV001", "Verification type missing");
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV002", "Country code missing");
                    #endregion
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV003", "Mobile number missing");
                    #endregion
                }
                else if (_Request.Type == 2 && string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV004", "Email address missing");
                    #endregion
                }
                else if (_Request.Type == 3 && string.IsNullOrEmpty(_Request.EmailAddress) && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV005", "Mobile number and email address missing");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string VMobileNumber = null;
                        string VEmailAddress = null;
                        #region Set V Mode
                        if (_Request.Type == 1 || _Request.Type == 3)
                        {
                            VMobileNumber = _Request.MobileNumber;
                            var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.CountryIsd).Select(x => new { CountryIsd = x.Isd, MobileNumberLength = x.MobileNumberLength }).FirstOrDefault();
                            if (CountryDetails != null)
                            {
                                VMobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.CountryIsd, VMobileNumber, CountryDetails.MobileNumberLength);
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV006", "Country details not found");
                                #endregion
                            }
                        }
                        if (_Request.Type == 2 || _Request.Type == 3)
                        {
                            VEmailAddress = _Request.EmailAddress;
                        }
                        #endregion
                        #region  Process Request
                        if (_Request.Type == 1)
                        {
                            #region Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       MobileMessage = n.MobileMessage,
                                                       MobileNumber = n.MobileNumber,
                                                       ReferenceKey = n.ReferenceKey,
                                                       CountryIsd = n.CountryIsd,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" && VMobileNumber != "2341001001000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage, PreviousRequest.MobileMessage, 0, null);
                                    #endregion
                                }
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.ReferenceKey;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV007", "Verification code sent to your mobile number. Please enter code to verify mobile");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String CodeStart = "CA";
                                String NumberVerificationToken = _Random.Next(1000, 9999).ToString();
                                if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000" || VMobileNumber == "2347007007000" || VMobileNumber == "2346006006000" || VMobileNumber == "2345005005000" || VMobileNumber == "2344004004000" || VMobileNumber == "2341001001000")
                                {
                                    NumberVerificationToken = "1247";
                                }
                                string AccessKey = HCoreHelper.GenerateGuid();
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMessage = NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" || VMobileNumber == "2341001001000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMessage, 0, null);
                                    #endregion
                                }
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = CodeStart;
                                _HCCoreVerification.AccessCode = NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddDays(1);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = _HCCoreVerification.Id;
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = CodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008", "Verification code sent to  your mobile number. Enter verification code to verify your mobile.");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 2)
                        {
                            #region Email Address Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,
                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                #region Send Email 
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV009", "Verification email sent to your email address. Verify your email address.");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "CA";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid();
                                string AccessKey = HCoreHelper.GenerateGuid();
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                #region Send Email

                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV010", "Verification code sent to your email address. Verify your email address");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 3)
                        {
                            #region Email And Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,

                                                       CountryIsd = n.CountryIsd,

                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,

                                                       MobileNumber = n.MobileNumber,
                                                       MobileMessage = n.MobileMessage,

                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,

                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage, PreviousRequest.MobileMessage, 0, null);
                                    #endregion
                                }
                                #region Send Email
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV011", "Verification details sent to your mobile and email address. Please verify your mobile number and email address.");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "CA";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                string AccessKey = HCoreHelper.GenerateGuid();
                                if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000")
                                {
                                    NumberVerificationToken = "1247";
                                }
                                #endregion
                                #region Build Mobile Message
                                string TMobileMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Build Email Message
                                string TEmailMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMobileMessage;
                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TEmailMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMobileMessage, 0, null);
                                    #endregion
                                }

                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV012", "Verification details sent to your mobile and email address. Please verify your mobile number and email address.");
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV013", "Invalid verification type");
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014", "Unable to start verification. Please try after some time");
                #endregion
            }
        }
        /// <summary>
        /// Description: Verifies the otp.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VerifyOtp(OCoreVerificationManager.RequestVerify _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Declare

                    #endregion
                    #region Process Request
                    if (string.IsNullOrEmpty(_Request.RequestToken))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015", "Verification token missing");
                        #endregion
                    }
                    else
                    {
                        //string CheckMobiApiKey = HCoreHelper.GetConfiguration("checkmobiapikey");
                        using (_HCoreContext = new HCoreContext())
                        {
                            var RequestDetails = (from n in _HCoreContext.HCCoreVerification
                                                  where n.Guid == _Request.RequestToken
                                                  select n).FirstOrDefault();
                            if (RequestDetails != null)
                            {
                                if (RequestDetails.StatusId == 1)
                                {
                                    if (RequestDetails.VerificationTypeId == 1 && string.IsNullOrEmpty(_Request.AccessCode))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV016", "Verification code missing");
                                        #endregion
                                    }
                                    else if (RequestDetails.VerificationTypeId == 2 && string.IsNullOrEmpty(_Request.AccessCode) && string.IsNullOrEmpty(_Request.AccessKey))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV017", "Verification code missing");
                                        #endregion
                                    }
                                    else
                                    {
                                        DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                        if (RequestDetails.ExpiaryDate > StartDate)
                                        {
                                            if (RequestDetails.VerificationTypeId == 1)
                                            {
                                                if (RequestDetails.AccessCode == _Request.AccessCode)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV018", "Verification successful");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Update Request
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV019", "Invalid verification code");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                if (RequestDetails.AccessCode == _Request.AccessCode || RequestDetails.AccessKey == _Request.AccessKey)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV020");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Update Request
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV021");
                                                    #endregion
                                                }
                                            }
                                        }
                                        else
                                        {
                                            #region Update Request
                                            RequestDetails.StatusId = 3;
                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                            RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Send Response
                                            _VerificationResponse = new OCoreVerificationManager.Response();
                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                            _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV022", "Verification code expired. Please process verification again");
                                            #endregion
                                        }
                                    }
                                }
                                else if (RequestDetails.StatusId == 2)
                                {
                                    #region Build Response
                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV023", "Verification already done");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV024", "Invalid status code");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV025", "Invalid request. Please process verification again.");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("VerifyOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV026", "Unable to verify request. Please try after some time.");
                #endregion
            }
        }

        #endregion
        /// <summary>
        /// Description: Requests the otp country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestOtpCountry(OCoreVerificationManager.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.Type == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV001", "Verification type missing");
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV002", "Country code missing");
                    #endregion
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV003", "Mobile number missing");
                    #endregion
                }
                else if (_Request.Type == 2 && string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV004", "Email address missing");
                    #endregion
                }
                else if (_Request.Type == 3 && string.IsNullOrEmpty(_Request.EmailAddress) && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV005", "Mobile number and email address missing");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string VMobileNumber = null;
                        string VEmailAddress = null;
                        #region Set V Mode
                        if (_Request.Type == 1 || _Request.Type == 3)
                        {
                            VMobileNumber = _Request.MobileNumber;
                            var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId).Select(x => new { CountryIsd = x.Isd, Length = x.MobileNumberLength }).FirstOrDefault();
                            if (CountryDetails != null)
                            {
                                VMobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.CountryIsd, VMobileNumber, CountryDetails.Length);
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV006", "Country details not found");
                                #endregion
                            }
                        }
                        if (_Request.Type == 2 || _Request.Type == 3)
                        {
                            VEmailAddress = _Request.EmailAddress;
                        }
                        #endregion
                        #region  Process Request
                        if (_Request.Type == 1)
                        {
                            #region Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       MobileMessage = n.MobileMessage,
                                                       MobileNumber = n.MobileNumber,
                                                       ReferenceKey = n.ReferenceKey,
                                                       CountryIsd = n.CountryIsd,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" && VMobileNumber != "2341001001000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage, PreviousRequest.MobileMessage, 0, null);
                                    #endregion
                                }
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.ReferenceKey;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV007", "Verification code sent to your mobile number. Please enter code to verify mobile");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String CodeStart = "CA";
                                String NumberVerificationToken = _Random.Next(1000, 9999).ToString();
                                if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000" || VMobileNumber == "2347007007000" || VMobileNumber == "2346006006000" || VMobileNumber == "2345005005000" || VMobileNumber == "2344004004000" || VMobileNumber == "2341001001000")
                                {
                                    NumberVerificationToken = "1247";
                                }
                                if (HostEnvironment == HostEnvironmentType.Test || HostEnvironment == HostEnvironmentType.Local)
                                {
                                    NumberVerificationToken = "1234";
                                }
                                string AccessKey = HCoreHelper.GenerateGuid();
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMessage = NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" || VMobileNumber == "2341001001000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMessage, 0, null);
                                    #endregion
                                }
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = CodeStart;
                                _HCCoreVerification.AccessCode = NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddDays(1);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = _HCCoreVerification.Id;
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = CodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008", "Verification code sent to  your mobile number. Enter verification code to verify your mobile.");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 2)
                        {
                            #region Email Address Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,
                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                #region Send Email 
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV009", "Verification email sent to your email address. Verify your email address.");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "CA";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid();
                                string AccessKey = HCoreHelper.GenerateGuid();
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                #region Send Email

                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV010", "Verification code sent to your email address. Verify your email address");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 3)
                        {
                            #region Email And Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,

                                                       CountryIsd = n.CountryIsd,

                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,

                                                       MobileNumber = n.MobileNumber,
                                                       MobileMessage = n.MobileMessage,

                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,

                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage, PreviousRequest.MobileMessage, 0, null);
                                    #endregion
                                }
                                #region Send Email
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV011", "Verification details sent to your mobile and email address. Please verify your mobile number and email address.");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "CA";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                string AccessKey = HCoreHelper.GenerateGuid();
                                if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000")
                                {
                                    NumberVerificationToken = "1247";
                                }
                                #endregion
                                #region Build Mobile Message
                                string TMobileMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Build Email Message
                                string TEmailMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMobileMessage;
                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TEmailMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMobileMessage, 0, null);
                                    #endregion
                                }

                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV012", "Verification details sent to your mobile and email address. Please verify your mobile number and email address.");
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV013", "Invalid verification type");
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014", "Unable to start verification. Please try after some time");
                #endregion
            }
        }
        /// <summary>
        /// Description: Verifies the otp country.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VerifyOtpCountry(OCoreVerificationManager.RequestVerify _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Declare

                    #endregion
                    #region Process Request
                    if (string.IsNullOrEmpty(_Request.RequestToken))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015", "Verification token missing");
                        #endregion
                    }
                    else
                    {
                        //string CheckMobiApiKey = HCoreHelper.GetConfiguration("checkmobiapikey");
                        using (_HCoreContext = new HCoreContext())
                        {
                            var RequestDetails = (from n in _HCoreContext.HCCoreVerification
                                                  where n.Guid == _Request.RequestToken
                                                  select n).FirstOrDefault();
                            if (RequestDetails != null)
                            {
                                if (RequestDetails.StatusId == 1)
                                {
                                    if (RequestDetails.VerificationTypeId == 1 && string.IsNullOrEmpty(_Request.AccessCode))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV016", "Verification code missing");
                                        #endregion
                                    }
                                    else if (RequestDetails.VerificationTypeId == 2 && string.IsNullOrEmpty(_Request.AccessCode) && string.IsNullOrEmpty(_Request.AccessKey))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV017", "Verification code missing");
                                        #endregion
                                    }
                                    else
                                    {
                                        DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                        if (RequestDetails.ExpiaryDate > StartDate)
                                        {
                                            if (RequestDetails.VerificationTypeId == 1)
                                            {
                                                if (RequestDetails.AccessCode == _Request.AccessCode)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV018", "Verification successful");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Update Request
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV019", "Invalid verification code");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                if (RequestDetails.AccessCode == _Request.AccessCode || RequestDetails.AccessKey == _Request.AccessKey)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV020");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Update Request
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV021");
                                                    #endregion
                                                }
                                            }
                                        }
                                        else
                                        {
                                            #region Update Request
                                            RequestDetails.StatusId = 3;
                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                            RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Send Response
                                            _VerificationResponse = new OCoreVerificationManager.Response();
                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                            _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV022", "Verification code expired. Please process verification again");
                                            #endregion
                                        }
                                    }
                                }
                                else if (RequestDetails.StatusId == 2)
                                {
                                    #region Build Response
                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV023", "Verification already done");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV024", "Invalid status code");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV025", "Invalid request. Please process verification again.");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("VerifyOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV026", "Unable to verify request. Please try after some time.");
                #endregion
            }
        }




        /// <summary>
        /// Description: Requests the otp country v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestOtpCountryV2(OCoreVerificationManager.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (_Request.Type == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV001", "Verification type missing");
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV002", "Country code missing");
                    #endregion
                }
                else if (_Request.Type == 1 && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV003", "Mobile number missing");
                    #endregion
                }
                else if (_Request.Type == 2 && string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV004", "Email address missing");
                    #endregion
                }
                else if (_Request.Type == 3 && string.IsNullOrEmpty(_Request.EmailAddress) && string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV005", "Mobile number and email address missing");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string VMobileNumber = null;
                        string VEmailAddress = null;
                        #region Set V Mode
                        if (_Request.Type == 1 || _Request.Type == 3)
                        {
                            VMobileNumber = _Request.MobileNumber;
                            var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Id == _Request.CountryId)
                                .Select(x => new
                                {
                                    CountryIsd = x.Isd,
                                    Length = x.MobileNumberLength
                                }).FirstOrDefault();
                            if (CountryDetails != null)
                            {
                                VMobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.CountryIsd, VMobileNumber, CountryDetails.Length);
                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV006", "Country details not found");
                                #endregion
                            }
                        }
                        if (_Request.Type == 2 || _Request.Type == 3)
                        {
                            VEmailAddress = _Request.EmailAddress;
                        }
                        #endregion
                        #region  Process Request
                        if (_Request.Type == 1)
                        {
                            #region Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       MobileMessage = n.MobileMessage,
                                                       MobileNumber = n.MobileNumber,
                                                       ReferenceKey = n.ReferenceKey,
                                                       CountryIsd = n.CountryIsd,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" && VMobileNumber != "2341001001000")
                                {
                                    if (_Request.CountryId != 1)
                                    {
                                        HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage, PreviousRequest.MobileMessage, 0, null);
                                    }
                                    else
                                    {
                                        FrameworkRoboCall.SendCode(PreviousRequest.MobileMessage, PreviousRequest.AccessCode, PreviousRequest.RequestToken, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage);
                                    }
                                }

                                //8GySmFbQIkOVkjx0vtLjMkp3nCwFqPAw1LTEXNWe2qslgeToV4


                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.ReferenceKey;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV007", "Verification code sent to your mobile number. Please enter code to verify mobile");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String CodeStart = "CA";
                                String NumberVerificationToken = _Random.Next(1000, 9999).ToString();
                                if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000" || VMobileNumber == "2347007007000" || VMobileNumber == "2346006006000" || VMobileNumber == "2345005005000" || VMobileNumber == "2344004004000" || VMobileNumber == "2341001001000")
                                {
                                    NumberVerificationToken = "1247";
                                }
                                if (HostEnvironment == HostEnvironmentType.Test || HostEnvironment == HostEnvironmentType.Local)
                                {
                                    NumberVerificationToken = "1234";
                                }
                                string AccessKey = HCoreHelper.GenerateGuid();
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMessage = NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" || VMobileNumber == "2341001001000")
                                {
                                    if (_Request.CountryId != 1)
                                    {
                                        #region Send Message
                                        HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMessage, 0, null);
                                        #endregion
                                    }
                                    else
                                    {
                                        FrameworkRoboCall.SendCode(VMobileNumber, NumberVerificationToken, AccessKey, _Request.CountryIsd, TMessage);

                                        //var client = new RestSharp.RestClient("https://app.smartsmssolutions.com/io/api/client/v1/voiceotp/send/");
                                        ////client.Timeout = -1;

                                        //var request = new RestSharp.RestRequest();
                                        //request.Method = RestSharp.Method.Post;
                                        //request.AlwaysMultipartFormData = true;

                                        ////request.AddParameter("token", "8GySmFbQIkOVkjx0vtLjMkp3nCwFqPAw1LTEXNWe2qslgeToV4", RestSharp.ParameterType.RequestBody);
                                        //request.AddParameter("phone", VMobileNumber, RestSharp.ParameterType.RequestBody);
                                        //request.AddParameter("otp", NumberVerificationToken, RestSharp.ParameterType.RequestBody);
                                        //request.AddParameter("class", "LXNBWB05H2", RestSharp.ParameterType.RequestBody);
                                        //request.AddParameter("ref_id", Guid, RestSharp.ParameterType.RequestBody);
                                        //var response = client.Execute(request);
                                        ////if (response.StatusCode == System.Net.HttpStatusCode.OK)
                                        ////{

                                        ////}
                                        ////else
                                        ////{
                                        ////    #region Send Message
                                        ////    HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMessage, 0, null);
                                        ////    #endregion
                                        ////}
                                    }
                                }
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = CodeStart;
                                _HCCoreVerification.AccessCode = NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddDays(1);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = _HCCoreVerification.Id;
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = CodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008", "Verification code sent to  your mobile number. Enter verification code to verify your mobile.");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 2)
                        {
                            #region Email Address Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,
                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,
                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,
                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                #region Send Email 
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV009", "Verification email sent to your email address. Verify your email address.");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "CA";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid();
                                string AccessKey = HCoreHelper.GenerateGuid();
                                #endregion
                                #region Build Message
                                string TMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                #region Send Email

                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV010", "Verification code sent to your email address. Verify your email address");
                                #endregion
                            }
                            #endregion
                        }
                        else if (_Request.Type == 3)
                        {
                            #region Email And Mobile Number Verification
                            DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                            DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                            var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                                   where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                                   select new
                                                   {
                                                       ReferenceId = n.Id,
                                                       RequestToken = n.Guid,

                                                       CountryIsd = n.CountryIsd,

                                                       AccessCode = n.AccessCode,
                                                       AccessCodeStart = n.AccessCodeStart,

                                                       MobileNumber = n.MobileNumber,
                                                       MobileMessage = n.MobileMessage,

                                                       EmailAddress = n.EmailAddress,
                                                       EmailMessage = n.EmailMessage,

                                                       Reference = n.ReferenceKey,
                                                       Type = n.VerificationTypeId,
                                                   }).FirstOrDefault();
                            if (PreviousRequest != null)
                            {
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, PreviousRequest.CountryIsd, PreviousRequest.MobileMessage, PreviousRequest.MobileMessage, 0, null);
                                    #endregion
                                }
                                #region Send Email
                                #endregion
                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                                _VerificationResponse.MobileNumber = PreviousRequest.MobileNumber;
                                _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                                _VerificationResponse.Type = PreviousRequest.Type;
                                _VerificationResponse.Reference = PreviousRequest.Reference;
                                _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                                _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV011", "Verification details sent to your mobile and email address. Please verify your mobile number and email address.");
                                #endregion
                            }
                            else
                            {
                                #region Get code
                                DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                                _Random = new Random();
                                String NumberVerificationPrefix = "CA";
                                String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                                string Guid = HCoreHelper.GenerateGuid(); ;
                                string AccessKey = HCoreHelper.GenerateGuid();
                                if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000")
                                {
                                    NumberVerificationToken = "1247";
                                }
                                #endregion
                                #region Build Mobile Message
                                string TMobileMessage = null;
                                if (!string.IsNullOrEmpty(_Request.MobileMessage))
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.MobileMessage;
                                }
                                else
                                {
                                    TMobileMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Build Email Message
                                string TEmailMessage = null;
                                if (!string.IsNullOrEmpty(_Request.EmailMessage))
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " " + _Request.EmailMessage;
                                }
                                else
                                {
                                    TEmailMessage = NumberVerificationPrefix + NumberVerificationToken + " is your verification code";
                                }
                                #endregion
                                #region Save Reqest
                                _HCCoreVerification = new HCCoreVerification();
                                _HCCoreVerification.Guid = Guid;
                                _HCCoreVerification.VerificationTypeId = _Request.Type;
                                _HCCoreVerification.CountryIsd = _Request.CountryIsd;
                                _HCCoreVerification.MobileNumber = VMobileNumber;
                                _HCCoreVerification.MobileMessage = TMobileMessage;
                                _HCCoreVerification.EmailAddress = VEmailAddress;
                                _HCCoreVerification.EmailMessage = TEmailMessage;
                                _HCCoreVerification.AccessKey = AccessKey;
                                _HCCoreVerification.AccessCodeStart = NumberVerificationPrefix;
                                _HCCoreVerification.AccessCode = NumberVerificationPrefix + NumberVerificationToken;
                                _HCCoreVerification.CreateDate = CurrentTime;
                                _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                                _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                                _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                                _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                                _HCCoreVerification.ReferenceKey = _Request.Reference;
                                _HCCoreVerification.VerifyAttemptCount = 0;
                                _HCCoreVerification.StatusId = 1;
                                _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                                _HCoreContext.SaveChanges();
                                long VerificationId = 0;
                                using (_HCoreContext = new HCoreContext())
                                {
                                    VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                                }
                                #endregion
                                if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000")
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMobileMessage, 0, null);
                                    #endregion
                                }

                                #region Build Response
                                _VerificationResponse = new OCoreVerificationManager.Response();
                                _VerificationResponse.ReferenceId = VerificationId;
                                _VerificationResponse.EmailAddress = VEmailAddress;
                                _VerificationResponse.MobileNumber = VMobileNumber;
                                _VerificationResponse.Type = _Request.Type;
                                _VerificationResponse.Reference = _Request.Reference;
                                _VerificationResponse.RequestToken = Guid;
                                _VerificationResponse.CodeStart = NumberVerificationPrefix;
                                #endregion
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV012", "Verification details sent to your mobile and email address. Please verify your mobile number and email address.");
                                #endregion
                            }
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV013", "Invalid verification type");
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014", "Unable to start verification. Please try after some time");
                #endregion
            }
        }
        /// <summary>
        /// Description: Verifies the otp country v2.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VerifyOtpCountryV2(OCoreVerificationManager.RequestVerify _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Declare

                    #endregion
                    #region Process Request
                    if (string.IsNullOrEmpty(_Request.RequestToken))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015", "Verification token missing");
                        #endregion
                    }
                    else
                    {
                        //string CheckMobiApiKey = HCoreHelper.GetConfiguration("checkmobiapikey");
                        using (_HCoreContext = new HCoreContext())
                        {
                            var RequestDetails = (from n in _HCoreContext.HCCoreVerification
                                                  where n.Guid == _Request.RequestToken
                                                  select n).FirstOrDefault();
                            if (RequestDetails != null)
                            {
                                if (RequestDetails.StatusId == 1)
                                {
                                    if (RequestDetails.VerificationTypeId == 1 && string.IsNullOrEmpty(_Request.AccessCode))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV016", "Verification code missing");
                                        #endregion
                                    }
                                    else if (RequestDetails.VerificationTypeId == 2 && string.IsNullOrEmpty(_Request.AccessCode) && string.IsNullOrEmpty(_Request.AccessKey))
                                    {
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV017", "Verification code missing");
                                        #endregion
                                    }
                                    else
                                    {
                                        DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                        if (RequestDetails.ExpiaryDate > StartDate)
                                        {
                                            if (RequestDetails.VerificationTypeId == 1)
                                            {
                                                if (RequestDetails.AccessCode == _Request.AccessCode)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV018", "Verification successful");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Update Request
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV019", "Invalid verification code");
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                if (RequestDetails.AccessCode == _Request.AccessCode || RequestDetails.AccessKey == _Request.AccessKey)
                                                {
                                                    #region Update Request
                                                    RequestDetails.StatusId = 2;
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                                    RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                                    RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                                    RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Build Response
                                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV020");
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Update Request
                                                    RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                                    _HCoreContext.SaveChanges();
                                                    #endregion
                                                    #region Send Response
                                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV021");
                                                    #endregion
                                                }
                                            }
                                        }
                                        else
                                        {
                                            #region Update Request
                                            RequestDetails.StatusId = 3;
                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                            RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Send Response
                                            _VerificationResponse = new OCoreVerificationManager.Response();
                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                            _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV022", "Verification code expired. Please process verification again");
                                            #endregion
                                        }
                                    }
                                }
                                else if (RequestDetails.StatusId == 2)
                                {
                                    #region Build Response
                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV023", "Verification already done");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV024", "Invalid status code");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV025", "Invalid request. Please process verification again.");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("VerifyOtp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV026", "Unable to verify request. Please try after some time.");
                #endregion
            }
        }

        /// <summary>
        /// Description: Requests the otp over voice call.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestOtpVoiceCall(OCoreVerificationManager.Request _Request)
        {
            try
            {
                #region Manage Operations               
                if (string.IsNullOrEmpty(_Request.CountryIsd))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV002", "Country code missing");
                    #endregion
                }
                else if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV003", "Mobile number missing");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        string VMobileNumber = null;
                        string VEmailAddress = null;
                        #region Set V Mode
                        VMobileNumber = _Request.MobileNumber;
                        var CountryDetails = _HCoreContext.HCCoreCountry.Where(x => x.Isd == _Request.CountryIsd)
                            .Select(x => new
                            {
                                CountryId = x.Id,
                                CountryIsd = x.Isd,
                                Length = x.MobileNumberLength
                            }).FirstOrDefault();
                        if (CountryDetails != null)
                        {
                            VMobileNumber = HCoreHelper.FormatMobileNumber(CountryDetails.CountryIsd, VMobileNumber, CountryDetails.Length);
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV006", "Country details not found");
                            #endregion
                        }
                        #endregion

                        string UserName = _AppConfig.AppUserPrefix + VMobileNumber;
                        var UserDetails = _HCoreContext.HCUAccount.Where(x => x.User.Username == UserName && x.AccountTypeId == UserAccountType.Appuser).FirstOrDefault();
                        if (UserDetails != null)
                        {

                            #region  Process Request                        
                            #region Mobile Number Verification                            
                            #region Get code
                            string Guid = HCoreHelper.GenerateGuid();
                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                            _Random = new Random();
                            String CodeStart = "FP"; //Forgot Pin
                            String NumberVerificationToken = _Random.Next(1000, 9999).ToString();
                            if (VMobileNumber == "2349009009000" || VMobileNumber == "2348008008000" || VMobileNumber == "2347007007000" || VMobileNumber == "2346006006000" || VMobileNumber == "2345005005000" || VMobileNumber == "2344004004000" || VMobileNumber == "2341001001000")
                            {
                                NumberVerificationToken = "1247";
                            }
                            if (HostEnvironment == HostEnvironmentType.Test || HostEnvironment == HostEnvironmentType.Local || HostEnvironment == HostEnvironmentType.Tech || HostEnvironment == HostEnvironmentType.Dev)
                            {
                                NumberVerificationToken = "1234";
                            }
                            string AccessKey = HCoreHelper.GenerateGuid();
                            #endregion
                            #region Build Message
                            string TMessage = null;
                            if (!string.IsNullOrEmpty(_Request.MobileMessage))
                            {
                                TMessage = NumberVerificationToken + " " + _Request.MobileMessage;
                            }
                            else
                            {
                                TMessage = NumberVerificationToken + " is your verification code";
                            }
                            #endregion
                            if (VMobileNumber != "2349009009000" && VMobileNumber != "2348008008000" && VMobileNumber != "2347007007000" && VMobileNumber != "2346006006000" && VMobileNumber != "2345005005000" && VMobileNumber != "2344004004000" || VMobileNumber == "2341001001000")
                            {
                                if (CountryDetails.CountryId != 1)
                                {
                                    #region Send Message
                                    HCoreHelper.SendSMS(SmsType.Transaction, _Request.CountryIsd, VMobileNumber, TMessage, 0, null);
                                    #endregion
                                }
                                else
                                {
                                    FrameworkRoboCall.SendCode(VMobileNumber, NumberVerificationToken, AccessKey, _Request.CountryIsd, TMessage);
                                }
                            }
                            #region Update Access Pin Temporary
                            UserDetails.AccessPin = HCoreEncrypt.EncryptHash(NumberVerificationToken);
                            UserDetails.IsTempPin = true;
                            _HCoreContext.HCUAccount.Update(UserDetails);
                            #endregion
                            _HCoreContext.SaveChanges();
                            #region Build Response
                            _VerificationResponse = new OCoreVerificationManager.Response();
                            // _VerificationResponse.ReferenceId = VerificationId;
                            _VerificationResponse.MobileNumber = VMobileNumber;
                            _VerificationResponse.Type = 1;
                            _VerificationResponse.Reference = _Request.Reference;
                            _VerificationResponse.RequestToken = Guid;
                            _VerificationResponse.CodeStart = CodeStart;
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV008", "Verification code sent to  your mobile number. Enter verification code to verify your mobile.");
                            #endregion

                            #endregion

                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV013", "User details not found.");
                            #endregion
                        }
                    }


                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestOtpVoiceCall", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014", "Unable to start verification. Please try after some time");
                #endregion
            }
        }
        /// <summary>
        /// Description: Requests the otp for merchant username update.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse RequestOtpUserNameUpdate(OCoreVerificationManager.Request _Request)
        {
            try
            {
                #region Manage Operations
                if (string.IsNullOrEmpty(_Request.EmailAddress))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV004", "Email address missing");
                    #endregion
                }
                else
                {
                    using (_HCoreContext = new HCoreContext())
                    {
                        DateTime StartDate = HCoreHelper.GetGMTDate().AddSeconds(-1);
                        DateTime EndDate = HCoreHelper.GetGMTDate().AddDays(1);
                        var PreviousRequest = (from n in _HCoreContext.HCCoreVerification
                                               where n.Guid == _Request.RequestToken && n.CreateDate > StartDate && n.CreateDate < EndDate && n.StatusId == 1 && n.VerifyDate == null
                                               select new
                                               {
                                                   ReferenceId = n.Id,
                                                   RequestToken = n.Guid,
                                                   AccessCode = n.AccessCode,
                                                   AccessCodeStart = n.AccessCodeStart,
                                                   EmailAddress = n.EmailAddress,
                                                   EmailMessage = n.EmailMessage,
                                                   Reference = n.ReferenceKey,
                                                   Type = n.VerificationTypeId,
                                               }).FirstOrDefault();
                        if (PreviousRequest != null)
                        {
                            #region Send Email 
                            #endregion
                            #region Build Response
                            _VerificationResponse = new OCoreVerificationManager.Response();
                            _VerificationResponse.ReferenceId = PreviousRequest.ReferenceId;
                            _VerificationResponse.EmailAddress = PreviousRequest.EmailAddress;
                            _VerificationResponse.Type = PreviousRequest.Type;
                            _VerificationResponse.Reference = PreviousRequest.Reference;
                            _VerificationResponse.RequestToken = PreviousRequest.RequestToken;
                            _VerificationResponse.CodeStart = PreviousRequest.AccessCodeStart;
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV009", "Verification email sent to your email address. Verify your email address.");
                            #endregion
                        }
                        else
                        {
                            #region Get code
                            DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                            _Random = new Random();
                            String NumberVerificationToken = _Random.Next(100000, 999999).ToString();
                            string Guid = HCoreHelper.GenerateGuid();
                            string AccessKey = HCoreHelper.GenerateGuid();
                            #endregion                            
                            #region Save Reqest
                            _HCCoreVerification = new HCCoreVerification();
                            _HCCoreVerification.Guid = Guid;
                            _HCCoreVerification.VerificationTypeId = _Request.Type;
                            _HCCoreVerification.EmailAddress = _Request.EmailAddress;
                            _HCCoreVerification.EmailMessage = _Request.EmailMessage;
                            _HCCoreVerification.AccessKey = AccessKey;
                            _HCCoreVerification.AccessCodeStart = "CA";
                            _HCCoreVerification.AccessCode = NumberVerificationToken;
                            _HCCoreVerification.CreateDate = CurrentTime;
                            _HCCoreVerification.ExpiaryDate = CurrentTime.AddHours(12);
                            _HCCoreVerification.RequestIpAddress = _Request.UserReference.RequestIpAddress;
                            _HCCoreVerification.RequestLatitude = _Request.UserReference.RequestLatitude;
                            _HCCoreVerification.RequestLongitude = _Request.UserReference.RequestLongitude;
                            _HCCoreVerification.ReferenceKey = _Request.Reference;
                            _HCCoreVerification.VerifyAttemptCount = 0;
                            _HCCoreVerification.StatusId = 1;
                            _HCoreContext.HCCoreVerification.Add(_HCCoreVerification);
                            _HCoreContext.SaveChanges();
                            long VerificationId = 0;
                            using (_HCoreContext = new HCoreContext())
                            {
                                VerificationId = _HCoreContext.HCCoreVerification.Where(x => x.Guid == Guid).Select(x => x.Id).FirstOrDefault();
                            }
                            #endregion
                            #region Send Email

                            #endregion
                            #region Build Response
                            _VerificationResponse = new OCoreVerificationManager.Response();
                            _VerificationResponse.ReferenceId = VerificationId;
                            _VerificationResponse.EmailAddress = _Request.EmailAddress;
                            _VerificationResponse.Type = _Request.Type;
                            _VerificationResponse.Reference = _Request.Reference;
                            _VerificationResponse.RequestToken = Guid;
                            _VerificationResponse.Accesskey = AccessKey;
                            _VerificationResponse.AccessCode = NumberVerificationToken;
                            #endregion
                            var EmailObject = new
                            {
                                Code = NumberVerificationToken,
                                UserDisplayName = _Request.BussinessName
                            };
                            HCoreHelper.BroadCastEmail(NotificationTemplates.UsernameUpdateVerificationCode, _Request.BussinessName, _Request.EmailAddress, EmailObject, _Request.UserReference);
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV010", "Verification code sent to your email address. Verify your email address");
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("RequestOtpUserNameUpdate", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV014", "Unable to start verification. Please try after some time");
                #endregion
            }
        }

        /// <summary>
        /// Description: Verifies the otp for merchant username update.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse VerifyOtpforUserName(OCoreVerificationManager.RequestVerify _Request)
        {
            try
            {
                using (_HCoreContext = new HCoreContext())
                {
                    #region Declare

                    #endregion
                    #region Process Request
                    if (string.IsNullOrEmpty(_Request.RequestToken))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015", "Verification token missing");
                        #endregion
                    }
                    else if (string.IsNullOrEmpty(_Request.AccessCode))
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV015", "Verification token missing");
                        #endregion
                    }
                    else
                    {
                        using (_HCoreContext = new HCoreContext())
                        {
                            var RequestDetails = (from n in _HCoreContext.HCCoreVerification
                                                  where n.Guid == _Request.RequestToken
                                                  select n).FirstOrDefault();
                            if (RequestDetails != null)
                            {
                                if (RequestDetails.StatusId == 1)
                                {

                                    DateTime StartDate = HCoreHelper.GetGMTDateTime();
                                    if (RequestDetails.ExpiaryDate > StartDate)
                                    {
                                        if (RequestDetails.AccessCode == _Request.AccessCode && RequestDetails.AccessKey == _Request.AccessKey)
                                        {
                                            #region Update Request
                                            RequestDetails.StatusId = 2;
                                            RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                            RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                            RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                            RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                            RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Build Response
                                            _VerificationResponse = new OCoreVerificationManager.Response();
                                            _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                            _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV020");
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Update Request
                                            RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                            _HCoreContext.SaveChanges();
                                            #endregion
                                            #region Send Response
                                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV021", "Invalid Verfication Token");
                                            #endregion
                                        }

                                    }
                                    else
                                    {
                                        #region Update Request
                                        RequestDetails.StatusId = 3;
                                        RequestDetails.VerifyDate = HCoreHelper.GetGMTDateTime();
                                        RequestDetails.VerifyAttemptCount = RequestDetails.VerifyAttemptCount + 1;
                                        RequestDetails.VerifyIpAddress = _Request.UserReference.RequestIpAddress;
                                        RequestDetails.VerifyLatitude = _Request.UserReference.RequestLatitude;
                                        RequestDetails.VerifyLongitude = _Request.UserReference.RequestLongitude;
                                        _HCoreContext.SaveChanges();
                                        #endregion
                                        #region Send Response
                                        _VerificationResponse = new OCoreVerificationManager.Response();
                                        _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                        _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                        #endregion
                                        #region Send Response
                                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _VerificationResponse, "HCV022", "Verification code expired. Please process verification again");
                                        #endregion
                                    }

                                }
                                else if (RequestDetails.StatusId == 2)
                                {
                                    #region Build Response
                                    _VerificationResponse = new OCoreVerificationManager.Response();
                                    _VerificationResponse.MobileNumber = RequestDetails.MobileNumber;
                                    _VerificationResponse.EmailAddress = RequestDetails.EmailAddress;
                                    #endregion
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _VerificationResponse, "HCV023", "Verification already done");
                                    #endregion
                                }
                                else
                                {
                                    #region Send Response
                                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV024", "Invalid status code");
                                    #endregion
                                }

                            }
                            else
                            {
                                #region Send Response
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV025", "Invalid request. Please process verification again.");
                                #endregion
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("VerifyOtpforUsername", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCV026", "Unable to verify request. Please try after some time.");
                #endregion
            }
        }

    }
}
