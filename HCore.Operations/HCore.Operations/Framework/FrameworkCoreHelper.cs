//==================================================================================
// FileName: FrameworkCoreHelper.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to core helper
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations.Framework
{
    public class FrameworkCoreHelper
    {
        #region Declare
        HCoreContext _HCoreContext;
        HCoreContextLogging _HCoreContextLogging;
        #endregion
        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreHelper(OCoreHelper.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreHelper.Details Details = _HCoreContext.HCCore
                            .Select(x => new OCoreHelper.Details
                            {
                                ReferenceKey = x.Guid,


                                ParentCode = x.Parent.SystemName,
                                ParentName = x.Parent.Name,

                                SubParentCode = x.SubParent.SystemName,
                                SubParentName = x.SubParent.Name,

                                Name = x.Name,
                                SystemName = x.SystemName,

                                Value = x.Value,
                                TypeName = x.TypeName,
                                //Description = x.Description,
                                Sequence = x.Sequence,

                                //IconUrl = x.IconStorage.Path,
                                //PosterUrl = x.PosterStorage.Path,

                                //CreateDate = x.CreateDate,
                                //CreatedByKey = x.CreatedBy.Guid,
                                //CreatedByDisplayName = x.CreatedBy.DisplayName,
                                //CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                //ModifyDate = x.ModifyDate,
                                //ModifyByKey = x.ModifyBy.Guid,
                                //ModifyByDisplayName = x.ModifyBy.DisplayName,
                                //ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .Where(_Request.Reference)
                            .FirstOrDefault();
                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion

                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreHelper", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="Reference">The reference.</param>
        /// <param name="UserReference">The user reference.</param>
        /// <returns>OCoreHelper.Details.</returns>
        internal OCoreHelper.Details GetCoreHelper(string Reference, OUserReference UserReference)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(Reference))
                {
                    #region Send Response
                    return null;
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreHelper.Details Details = _HCoreContext.HCCore
                            .Select(x => new OCoreHelper.Details
                            {
                                ReferenceKey = x.Guid,


                                ParentCode = x.Parent.SystemName,
                                ParentName = x.Parent.Name,

                                SubParentCode = x.SubParent.SystemName,
                                SubParentName = x.SubParent.Name,

                                Name = x.Name,
                                SystemName = x.SystemName,

                                Value = x.Value,
                                TypeName = x.TypeName,
                                //Description = x.Description,
                                Sequence = x.Sequence,

                                //IconUrl = x.IconStorage.Path,
                                //PosterUrl = x.PosterStorage.Path,

                                //CreateDate = x.CreateDate,
                                //CreatedByKey = x.CreatedBy.Guid,
                                //CreatedByDisplayName = x.CreatedBy.DisplayName,
                                //CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                //ModifyDate = x.ModifyDate,
                                //ModifyByKey = x.ModifyBy.Guid,
                                //ModifyByDisplayName = x.ModifyBy.DisplayName,
                                //ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .Where(Reference)
                            .FirstOrDefault();
                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            #endregion
                            return Details;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    #endregion

                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("GetCoreHelper", _Exception, UserReference);
                #endregion
                return null;
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core helper.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreHelper(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCore
                                        select new OCoreHelper.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            ParentCode = x.Parent.SystemName,
                                            ParentName = x.Parent.Name,

                                            SubParentCode = x.SubParent.SystemName,
                                            SubParentName = x.SubParent.Name,

                                            Name = x.Name,
                                            SystemName = x.SystemName,

                                            Value = x.Value,
                                            TypeName = x.TypeName,
                                            //Description = x.Description,
                                            Sequence = x.Sequence,

                                            //IconUrl = x.IconStorage.Path,

                                            //eateDate = x.CreateDate,
                                            //CreatedByKey = x.CreatedBy.Guid,
                                            //CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            //ModifyDate = x.ModifyDate,
                                            //ModifyByKey = x.ModifyBy.Guid,
                                            //ModifyByDisplayName = x.ModifyBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OCoreHelper.Details> Data = (from x in _HCoreContext.HCCore
                                                      select new OCoreHelper.Details
                                                      {
                                                          ReferenceKey = x.Guid,


                                                          ParentCode = x.Parent.SystemName,
                                                          ParentName = x.Parent.Name,

                                                          SubParentCode = x.SubParent.SystemName,
                                                          SubParentName = x.SubParent.Name,

                                                          Name = x.Name,
                                                          SystemName = x.SystemName,

                                                          Value = x.Value,
                                                          TypeName = x.TypeName,
                                                          //Description = x.Description,
                                                          Sequence = x.Sequence,

                                                          //IconUrl = x.IconStorage.Path,

                                                          //CreateDate = x.CreateDate,
                                                          //CreatedByKey = x.CreatedBy.Guid,
                                                          //CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                          //ModifyDate = x.ModifyDate,
                                                          //ModifyByKey = x.ModifyBy.Guid,
                                                          //ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Format List
                    _HCoreContext.Dispose();
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreHelper", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core helper lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreHelperLite(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCore
                                        orderby x.Name ascending
                                        select new
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            ParentId = x.ParentId,
                                            ParentCode = x.Parent.SystemName,
                                            SubParentId = x.SubParentId,
                                            SubParentCode = x.SubParent.SystemName,

                                            Name = x.Name,
                                            SystemName = x.SystemName,


                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    var Data = (from x in _HCoreContext.HCCore
                                orderby x.Name ascending
                                select new
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,

                                    ParentId = x.ParentId,
                                    ParentCode = x.Parent.SystemName,
                                    SubParentId = x.SubParentId,
                                    SubParentCode = x.SubParent.SystemName,

                                    Name = x.Name,
                                    SystemName = x.SystemName,


                                    StatusId = x.StatusId,
                                    StatusCode = x.Status.SystemName,
                                })
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Format List
                    _HCoreContext.Dispose();

                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreHelperLite", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the status list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStatusList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    var Data = _HCoreContext.HCCore
                        .Where(x => x.ParentId ==
                        HCoreConstant.HelperType.StatusDefault ||
                       x.ParentId == HCoreConstant.HelperType.StatusOrder ||
                      x.ParentId == HCoreConstant.HelperType.StatusCardOrder ||
                      x.ParentId == HCoreConstant.HelperType.StatusDevice ||
                      x.ParentId == HCoreConstant.HelperType.StatusCard ||
                       x.ParentId == HCoreConstant.HelperType.StatusTransaction ||
                      x.ParentId == HCoreConstant.HelperType.StatusRewardInvoice ||
                      x.ParentId == HCoreConstant.HelperType.StatusInvoice
                         ).Select(x => new
                         {

                             ReferenceId = x.Id,
                             ParentId = x.ParentId,
                             ParentCode = x.Parent.SystemName,
                             Name = x.Name,
                             SystemName = x.SystemName,
                         }).ToList();
                    #endregion
                    #region Format List
                    _HCoreContext.Dispose();
                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(Data.Count, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreHelperLite", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core common lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreCommonLite(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCoreCommon
                                        orderby x.Name ascending
                                        select new
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            TypeId = x.TypeId,
                                            TypeCode = x.Type.SystemName,
                                            ParentId = x.ParentId,
                                            ParentCode = x.Parent.SystemName,
                                            SubParentId = x.SubParentId,
                                            SubParentCode = x.SubParent.SystemName,
                                            Name = x.Name,
                                            SystemName = x.SystemName,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    var Data = (from x in _HCoreContext.HCCoreCommon
                                orderby x.Name ascending
                                select new
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    TypeId = x.TypeId,
                                    TypeCode = x.Type.SystemName,
                                    ParentId = x.ParentId,
                                    ParentCode = x.Parent.SystemName,
                                    SubParentId = x.SubParentId,
                                    SubParentCode = x.SubParent.SystemName,
                                    Name = x.Name,
                                    SystemName = x.SystemName,
                                    StatusId = x.StatusId,
                                    StatusCode = x.Status.SystemName,
                                })
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Format List
                    _HCoreContext.Dispose();

                    #endregion
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreCommonLite", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreCommon(OCoreCommon.Manage _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreCommon.Details Details = _HCoreContext.HCCoreCommon
                            .Select(x => new OCoreCommon.Details
                            {
                                ReferenceKey = x.Guid,
                                TypeCode = x.Type.SystemName,
                                TypeName = x.Type.Name,
                                HelperCode = x.Helper.SystemName,
                                HelperName = x.Helper.Name,
                                UserAccountKey = x.Account.Guid,
                                UserAccountDisplayName = x.Account.DisplayName,
                                UserAccountIconUrl = x.Account.IconStorage.Path,
                                ParentKey = x.Parent.Guid,
                                ParentCode = x.Parent.SystemName,
                                ParentName = x.Parent.Name,
                                SubParentKey = x.SubParent.Guid,
                                SubParentCode = x.SubParent.SystemName,
                                SubParentName = x.SubParent.Name,
                                Name = x.Name,
                                SystemName = x.SystemName,
                                Value = x.Value,
                                SubValue = x.SubValue,
                                Data = x.Data,
                                Description = x.Description,
                                Sequence = x.Sequence,
                                Count = x.Count,
                                SubItemsCount = x.InverseParent.Count,
                                IconUrl = x.IconStorage.Path,
                                PosterUrl = x.PosterStorage.Path,
                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .Where(_Request.Reference)
                            .FirstOrDefault();
                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            #endregion

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreCommon", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core common.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreCommon(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "!=");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCoreCommon
                                        select new OCoreCommon.Details
                                        {
                                            ReferenceKey = x.Guid,
                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,
                                            HelperCode = x.Helper.SystemName,
                                            HelperName = x.Helper.Name,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            ParentKey = x.Parent.Guid,
                                            ParentCode = x.Parent.SystemName,
                                            ParentName = x.Parent.Name,
                                            SubParentKey = x.SubParent.Guid,
                                            SubParentCode = x.SubParent.SystemName,
                                            SubParentName = x.SubParent.Name,
                                            Name = x.Name,
                                            SystemName = x.SystemName,
                                            Value = x.Value,
                                            Sequence = x.Sequence,
                                            Count = x.Count,
                                            IconUrl = x.IconStorage.Path,
                                            PosterUrl = x.PosterStorage.Path,
                                            SubItemsCount = x.InverseParent.Count,
                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreCommon.Details> Data = (from x in _HCoreContext.HCCoreCommon
                                                      select new OCoreCommon.Details
                                                      {
                                                          ReferenceKey = x.Guid,
                                                          TypeCode = x.Type.SystemName,
                                                          TypeName = x.Type.Name,
                                                          HelperCode = x.Helper.SystemName,
                                                          HelperName = x.Helper.Name,
                                                          UserAccountKey = x.Account.Guid,
                                                          UserAccountDisplayName = x.Account.DisplayName,
                                                          UserAccountIconUrl = x.Account.IconStorage.Path,
                                                          ParentKey = x.Parent.Guid,
                                                          ParentCode = x.Parent.SystemName,
                                                          ParentName = x.Parent.Name,
                                                          SubParentKey = x.SubParent.Guid,
                                                          SubParentCode = x.SubParent.SystemName,
                                                          SubParentName = x.SubParent.Name,
                                                          Name = x.Name,
                                                          SystemName = x.SystemName,
                                                          Value = x.Value,
                                                          Sequence = x.Sequence,
                                                          Count = x.Count,
                                                          IconUrl = x.IconStorage.Path,
                                                          PosterUrl = x.PosterStorage.Path,
                                                          SubItemsCount = x.InverseParent.Count,
                                                          CreateDate = x.CreateDate,
                                                          CreatedByKey = x.CreatedBy.Guid,
                                                          CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                          ModifyDate = x.ModifyDate,
                                                          ModifyByKey = x.ModifyBy.Guid,
                                                          ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var Details in Data)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion

                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreCommon", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreParameter(OCoreParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreParameter.Details Details = _HCoreContext.HCCoreParameter
                            .Select(x => new OCoreParameter.Details
                            {
                                ReferenceKey = x.Guid,

                                TypeCode = x.Type.SystemName,
                                TypeName = x.Type.Name,

                                HelperCode = x.Helper.SystemName,
                                HelperName = x.Helper.Name,

                                CommonKey = x.Common.Guid,
                                CommonCode = x.Common.SystemName,
                                CommonName = x.Common.Name,

                                SubCommonKey = x.SubCommon.Guid,
                                SubCommonCode = x.SubCommon.SystemName,
                                SubCommonName = x.SubCommon.Name,

                                UserAccountKey = x.Account.Guid,
                                UserAccountDisplayName = x.Account.DisplayName,
                                UserAccountIconUrl = x.Account.IconStorage.Path,

                                ParentKey = x.Parent.Guid,
                                ParentCode = x.Parent.SystemName,
                                ParentName = x.Parent.Name,

                                SubParentKey = x.SubParent.Guid,
                                SubParentCode = x.SubParent.SystemName,
                                SubParentName = x.SubParent.Name,

                                Name = x.Name,
                                SystemName = x.SystemName,
                                Value = x.Value,
                                SubValue = x.SubValue,
                                Data = x.Data,
                                Description = x.Description,
                                Sequence = x.Sequence,
                                Count = x.Count,
                                IconUrl = x.IconStorage.Path,
                                PosterUrl = x.PosterStorage.Path,
                                SubItemsCount = x.InverseParent.Count,
                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name
                            })
                            .Where(_Request.Reference)
                            .FirstOrDefault();
                        _HCoreContext.Dispose();
                        if (Details != null)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            #endregion
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreParameter(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCoreParameter
                                        select new OCoreParameter.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,

                                            HelperCode = x.Helper.SystemName,
                                            HelperName = x.Helper.Name,

                                            CommonKey = x.Common.Guid,
                                            CommonCode = x.Common.SystemName,
                                            CommonName = x.Common.Name,

                                            SubCommonKey = x.SubCommon.Guid,
                                            SubCommonCode = x.SubCommon.SystemName,
                                            SubCommonName = x.SubCommon.Name,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                            ParentKey = x.Parent.Guid,
                                            ParentCode = x.Parent.SystemName,
                                            ParentName = x.Parent.Name,

                                            SubParentKey = x.SubParent.Guid,
                                            SubParentCode = x.SubParent.SystemName,
                                            SubParentName = x.SubParent.Name,

                                            Name = x.Name,
                                            SystemName = x.SystemName,
                                            Value = x.Value,
                                            Sequence = x.Sequence,
                                            Count = x.Count,
                                            IconUrl = x.IconStorage.Path,

                                            SubItemsCount = x.InverseParent.Count,
                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreParameter.Details> Data = (from x in _HCoreContext.HCCoreParameter
                                                         select new OCoreParameter.Details
                                                         {
                                                             ReferenceKey = x.Guid,

                                                             TypeCode = x.Type.SystemName,
                                                             TypeName = x.Type.Name,

                                                             HelperCode = x.Helper.SystemName,
                                                             HelperName = x.Helper.Name,

                                                             CommonKey = x.Common.Guid,
                                                             CommonCode = x.Common.SystemName,
                                                             CommonName = x.Common.Name,

                                                             SubCommonKey = x.SubCommon.Guid,
                                                             SubCommonCode = x.SubCommon.SystemName,
                                                             SubCommonName = x.SubCommon.Name,

                                                             UserAccountKey = x.Account.Guid,
                                                             UserAccountDisplayName = x.Account.DisplayName,
                                                             UserAccountIconUrl = x.Account.IconStorage.Path,

                                                             ParentKey = x.Parent.Guid,
                                                             ParentCode = x.Parent.SystemName,
                                                             ParentName = x.Parent.Name,

                                                             SubParentKey = x.SubParent.Guid,
                                                             SubParentCode = x.SubParent.SystemName,
                                                             SubParentName = x.SubParent.Name,

                                                             Name = x.Name,
                                                             SystemName = x.SystemName,
                                                             Value = x.Value,
                                                             Sequence = x.Sequence,
                                                             Count = x.Count,
                                                             IconUrl = x.IconStorage.Path,

                                                             SubItemsCount = x.InverseParent.Count,
                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyByKey = x.ModifyBy.Guid,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion


                    }
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the role list for user.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRoleListForUser(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCoreRole
                                        select new OCoreParameter.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            ReferenceId = x.Id,
                                            Name = x.Name,
                                            SystemName = x.SystemName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreParameter.Details> Data = (from x in _HCoreContext.HCCoreRole
                                                         select new OCoreParameter.Details
                                                         {
                                                             ReferenceKey = x.Guid,

                                                             ReferenceId = x.Id,
                                                             Name = x.Name,
                                                             SystemName = x.SystemName,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion

                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetRoleListForUser", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreLog(OCoreLog _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operation
                    using (_HCoreContextLogging = new HCoreContextLogging())
                    {

                        #region Get Data
                        OCoreLog Details = (from n in _HCoreContextLogging.HCLCoreLog
                                            select new OCoreLog
                                            {
                                                ReferenceId = n.Id,
                                                //ReferenceKey = n.Guid,
                                                //TypeId = n.TypeId,
                                                // TypeCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.SystemName).FirstOrDefault(),
                                                //  TypeName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.Name).FirstOrDefault(),


                                                Title = n.Title,
                                                Description = n.Message,
                                                //Comment = n.Comment,
                                                UserReferenceContent = n.RequestReference,
                                                Data = n.Data,

                                                CreateDate = n.CreateDate,
                                                //  CreatedByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.Guid).FirstOrDefault(),
                                                //  CreatedByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.DisplayName).FirstOrDefault(),


                                                ModifyDate = n.CreateDate,
                                                //   ModifyByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.Guid).FirstOrDefault(),
                                                //   ModifyByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.DisplayName).FirstOrDefault(),


                                                //StatusId = n.StatusId,
                                                //  StatusCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.SystemName).FirstOrDefault(),
                                                //  StatusName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.Name).FirstOrDefault(),

                                            })
                                         .Where(_Request.Reference)
                                         .FirstOrDefault();
                        _HCoreContextLogging.Dispose();
                        if (Details != null)
                        {
                            #region Format List
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            #endregion

                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC0001");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, Details, "HC0002");
                            #endregion
                        }
                        #endregion

                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetLog", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core log.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreLog(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContextLogging.HCLCoreLog
                                        select new OCoreLog
                                        {
                                            ReferenceId = n.Id,
                                            //ReferenceKey = n.Guid,
                                            //TypeId = n.TypeId,
                                            // TypeCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.SystemName).FirstOrDefault(),
                                            //  TypeName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.Name).FirstOrDefault(),


                                            Title = n.Title,
                                            Description = n.Message,
                                            //Comment = n.Comment,
                                            UserReferenceContent = n.RequestReference,
                                            Data = n.Data,

                                            CreateDate = n.CreateDate,
                                            //  CreatedByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.Guid).FirstOrDefault(),
                                            //  CreatedByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.DisplayName).FirstOrDefault(),


                                            ModifyDate = n.CreateDate,
                                            //   ModifyByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.Guid).FirstOrDefault(),
                                            //   ModifyByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.DisplayName).FirstOrDefault(),


                                            //StatusId = n.StatusId,
                                            //  StatusCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.SystemName).FirstOrDefault(),
                                            //  StatusName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.Name).FirstOrDefault(),

                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OCoreLog> Data = (from n in _HCoreContextLogging.HCLCoreLog
                                           select new OCoreLog
                                           {
                                               ReferenceId = n.Id,
                                               //ReferenceKey = n.Guid,
                                               //TypeId = n.TypeId,
                                               // TypeCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.SystemName).FirstOrDefault(),
                                               //  TypeName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.TypeId).Select(x => x.Name).FirstOrDefault(),


                                               Title = n.Title,
                                               Description = n.Message,
                                               //Comment = n.Comment,
                                               UserReferenceContent = n.RequestReference,
                                               Data = n.Data,

                                               CreateDate = n.CreateDate,
                                               //  CreatedByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.Guid).FirstOrDefault(),
                                               //  CreatedByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.CreatedById).Select(x => x.DisplayName).FirstOrDefault(),


                                               ModifyDate = n.CreateDate,
                                               //   ModifyByKey = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.Guid).FirstOrDefault(),
                                               //   ModifyByDisplayName = _HCoreContext.HCUAccount.Where(x => x.Id == n.ModifyById).Select(x => x.DisplayName).FirstOrDefault(),


                                               //StatusId = n.StatusId,
                                               //  StatusCode = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.SystemName).FirstOrDefault(),
                                               //  StatusName = _HCoreContext.HCCoreHelper.Where(x => x.Id == n.StatusId).Select(x => x.Name).FirstOrDefault(),

                                           })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var Details in Data)
                    {
                        #region Format List
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        #endregion
                    }
                    _HCoreContextLogging.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetLogs", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreUsage(OCoreUsage.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    // return null;
                    #region Operations
                    using (_HCoreContextLogging = new HCoreContextLogging())
                    {

                        OCoreUsage.Details Details = (from x in _HCoreContextLogging.HCLCoreUsage
                                                      select new OCoreUsage.Details
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          AppId = x.ApiId,
                                                          RequestTime = x.RequestTime,
                                                          ResponseTime = x.ResponseTime,
                                                          IpAddress = x.IpAddress,
                                                          StatusId = x.StatusId
                                                      })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContextLogging.Dispose();

                        if (Details != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }

                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core usage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreUsage(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "RequestTime", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextLogging = new HCoreContextLogging())
                {
                    //return null;
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContextLogging.HCLCoreUsage
                                        select new OCoreUsage.Details
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            AppId = x.ApiId,
                                            RequestTime = x.RequestTime,
                                            ResponseTime = x.ResponseTime,
                                            IpAddress = x.IpAddress,
                                            StatusId = x.StatusId
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreUsage.Details> Data = (from x in _HCoreContextLogging.HCLCoreUsage
                                                     select new OCoreUsage.Details
                                                     {

                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         AppId = x.ApiId,
                                                         RequestTime = x.RequestTime,
                                                         ResponseTime = x.ResponseTime,
                                                         IpAddress = x.IpAddress,
                                                         StatusId = x.StatusId
                                                     })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContextLogging.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreUsage", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStorage(OStorage.Request _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OStorage.Details Details = (from x in _HCoreContext.HCCoreStorage
                                                    select new OStorage.Details
                                                    {
                                                        ReferenceKey = x.Guid,

                                                        UserAccountKey = x.Account.Guid,
                                                        UserAccountDisplayName = x.Account.DisplayName,
                                                        UserAccountIconUrl = x.Account.IconStorage.Path,

                                                        StorageTypeCode = x.StorageType.SystemName,
                                                        StorageTypeName = x.StorageType.Name,

                                                        SourceCode = x.Source.SystemName,
                                                        SourceName = x.Source.Name,

                                                        Name = x.Name,
                                                        Url = _AppConfig.StorageUrl + x.Path,
                                                        ThumbnailUrl = _AppConfig.StorageUrl + x.Path,
                                                        Extension = x.Extension,
                                                        Size = x.Size,

                                                        HelperCode = x.Helper.SystemName,
                                                        HelperName = x.Helper.Name,


                                                        CreateDate = x.CreateDate,
                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name

                                                    })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the storage.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStorage(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCCoreStorage
                                        select new OStorage.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                            StorageTypeCode = x.StorageType.SystemName,
                                            StorageTypeName = x.StorageType.Name,

                                            SourceCode = x.Source.SystemName,
                                            SourceName = x.Source.Name,

                                            Name = x.Name,
                                            Url = _AppConfig.StorageUrl + x.Path,
                                            ThumbnailUrl = _AppConfig.StorageUrl + x.Path,
                                            Extension = x.Extension,
                                            Size = x.Size,

                                            HelperCode = x.Helper.SystemName,
                                            HelperName = x.Helper.Name,

                                            CreateDate = x.CreateDate,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OStorage.Details> Data = (from x in _HCoreContext.HCCoreStorage
                                                   select new OStorage.Details
                                                   {
                                                       ReferenceKey = x.Guid,

                                                       UserAccountKey = x.Account.Guid,
                                                       UserAccountDisplayName = x.Account.DisplayName,
                                                       UserAccountIconUrl = x.Account.IconStorage.Path,

                                                       StorageTypeCode = x.StorageType.SystemName,
                                                       StorageTypeName = x.StorageType.Name,

                                                       SourceCode = x.Source.SystemName,
                                                       SourceName = x.Source.Name,

                                                       Name = x.Name,
                                                       Url = _AppConfig.StorageUrl + x.Path,
                                                       ThumbnailUrl = _AppConfig.StorageUrl + x.Path,
                                                       Extension = x.Extension,
                                                       Size = x.Size,



                                                       HelperCode = x.Helper.SystemName,
                                                       HelperName = x.Helper.Name,


                                                       CreateDate = x.CreateDate,
                                                       StatusId = x.StatusId,
                                                       StatusCode = x.Status.SystemName,
                                                       StatusName = x.Status.Name
                                                   })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("Description: GetstorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreVerification(OCoreVerification.Request _Request)
        {

            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OCoreVerification.Details Details = (from x in _HCoreContext.HCCoreVerification
                                                             select new OCoreVerification.Details
                                                             {
                                                                 ReferenceId = x.Id,
                                                                 ReferenceKey = x.Guid,
                                                                 TypeCode = x.VerificationType.SystemName,
                                                                 TypeName = x.VerificationType.Name,
                                                                 CountryIsd = x.CountryIsd,
                                                                 MobileNumber = x.MobileNumber,
                                                                 EmailAddress = x.EmailAddress,
                                                                 AccessKey = x.AccessKey,
                                                                 AccessCode = x.AccessCode,
                                                                 AccessCodeStart = x.AccessCodeStart,
                                                                 EmailMessage = x.EmailMessage,
                                                                 MobileMessage = x.MobileMessage,
                                                                 ExpiaryDate = x.ExpiaryDate,
                                                                 VerifyDate = x.VerifyDate,
                                                                 RequestIpAddress = x.RequestIpAddress,
                                                                 RequestLatitude = x.RequestLatitude,
                                                                 RequestLongitude = x.RequestLongitude,
                                                                 RequestLocation = x.RequestLocation,
                                                                 VerifyAttemptCount = x.VerifyAttemptCount,
                                                                 VerifyIpAddress = x.VerifyIpAddress,
                                                                 VerifyLatitude = x.VerifyLatitude,
                                                                 VerifyLongitude = x.VerifyLongitude,
                                                                 VerifyLocation = x.VerifyAddress,
                                                                 ReferenceSubKey = x.ReferenceKey,
                                                                 CreateDate = x.CreateDate,
                                                                 StatusId = x.StatusId,
                                                                 StatusCode = x.Status.SystemName,
                                                                 StatusName = x.Status.Name

                                                             })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();
                        _HCoreContext.Dispose();
                        if (Details != null)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetCoreVerification", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the core verification.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCoreVerification(OList.Request _Request)
        {
            #region Declare

            #endregion
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from n in _HCoreContext.HCCoreVerification
                                        select new OCoreVerification.Details
                                        {
                                            ReferenceId = n.Id,
                                            ReferenceKey = n.Guid,
                                            TypeCode = n.VerificationType.SystemName,
                                            TypeName = n.VerificationType.Name,
                                            CountryIsd = n.CountryIsd,
                                            MobileNumber = n.MobileNumber,
                                            EmailAddress = n.EmailAddress,
                                            AccessCodeStart = n.AccessCodeStart,
                                            RequestIpAddress = n.RequestIpAddress,
                                            VerifyIpAddress = n.VerifyIpAddress,
                                            ExpiaryDate = n.ExpiaryDate,
                                            VerifyDate = n.VerifyDate,
                                            CreateDate = n.CreateDate,
                                            VerifyAttemptCount = n.VerifyAttemptCount,
                                            StatusId = n.StatusId,
                                            StatusCode = n.Status.SystemName,
                                            StatusName = n.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;

                    }
                    #endregion
                    #region Get Data
                    List<OCoreVerification.Details> Data = (from n in _HCoreContext.HCCoreVerification
                                                            select new OCoreVerification.Details
                                                            {
                                                                ReferenceId = n.Id,
                                                                ReferenceKey = n.Guid,
                                                                TypeCode = n.VerificationType.SystemName,
                                                                TypeName = n.VerificationType.Name,
                                                                CountryIsd = n.CountryIsd,
                                                                MobileNumber = n.MobileNumber,
                                                                EmailAddress = n.EmailAddress,
                                                                AccessCodeStart = n.AccessCodeStart,
                                                                RequestIpAddress = n.RequestIpAddress,
                                                                VerifyIpAddress = n.VerifyIpAddress,
                                                                ExpiaryDate = n.ExpiaryDate,
                                                                VerifyDate = n.VerifyDate,
                                                                CreateDate = n.CreateDate,
                                                                VerifyAttemptCount = n.VerifyAttemptCount,
                                                                StatusId = n.StatusId,
                                                                StatusCode = n.Status.SystemName,
                                                                StatusName = n.Status.Name
                                                            })
                                               .Where(_Request.SearchCondition)
                                               .OrderBy(_Request.SortExpression)
                                               .Skip(_Request.Offset)
                                               .Take(_Request.Limit)
                                               .ToList();
                    #endregion
                    _HCoreContext.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCoreVerification", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccount(OUserAccount.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserAccount.Details Details = (from x in _HCoreContext.HCUAccount
                                                        select new OUserAccount.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            AccountTypeId = x.AccountTypeId,
                                                            AccountTypeCode = x.AccountType.SystemName,
                                                            AccountTypeName = x.AccountType.Name,

                                                            AccountOperationTypeCode = x.AccountOperationType.SystemName,
                                                            AccountOperationTypeName = x.AccountOperationType.Name,

                                                            OwnerId = x.OwnerId,
                                                            OwnerKey = x.Owner.Guid,
                                                            OwnerDisplayName = x.Owner.DisplayName,
                                                            OwnerAccountTypeId = x.Owner.AccountTypeId,
                                                            OwnerAccounttypeCode = x.Owner.AccountType.SystemName,

                                                            SubOwnerKey = x.SubOwner.Guid,
                                                            SubOwnerDisplayName = x.SubOwner.DisplayName,
                                                            SubOwnerAddress = x.SubOwner.Address,

                                                            SubOwnerLatitude = x.SubOwner.Latitude,
                                                            SubOwnerLongitude = x.SubOwner.Longitude,


                                                            BankKey = x.Bank.Guid,
                                                            BankDisplayName = x.Bank.DisplayName,

                                                            DisplayName = x.DisplayName,
                                                            AccessPin = x.AccessPin,
                                                            AccountCode = x.AccountCode,

                                                            IconUrl = x.IconStorage.Path,
                                                            PosterUrl = x.PosterStorage.Path,

                                                            AppKey = x.AppVersion.Parent.Guid,
                                                            AppName = x.AppVersion.Parent.Name,

                                                            ReferralCode = x.ReferralCode,
                                                            ReferralUrl = x.ReferralUrl,

                                                            Description = x.Description,

                                                            RegistrationSourceCode = x.RegistrationSource.Name,
                                                            RegistrationSourceName = x.RegistrationSource.Name,

                                                            RoleKey = x.Role.Guid,
                                                            RoleName = x.Role.Name,

                                                            AppVersionKey = x.AppVersion.Guid,
                                                            AppVersionName = x.AppVersion.Name,

                                                            ApplicationStatusCode = x.ApplicationStatus.SystemName,
                                                            ApplicationStatusName = x.ApplicationStatus.Name,


                                                            LastLoginDate = x.LastLoginDate,
                                                            // LastTransactionDate = x.LastTransactionDate,
                                                            RequestKey = x.RequestKey,

                                                            CountValue = x.CountValue,
                                                            AverageValue = x.AverageValue,


                                                            UserName = x.User.Username,
                                                            Password = x.User.Password,
                                                            SecondaryPassword = x.User.SecondaryPassword,
                                                            SystemPassword = x.User.SystemPassword,
                                                            Name = x.Name,
                                                            FirstName = x.FirstName,
                                                            LastName = x.LastName,
                                                            MobileNumber = x.MobileNumber,
                                                            ContactNumber = x.ContactNumber,
                                                            EmailAddress = x.EmailAddress,
                                                            SecondaryEmailAddress = x.SecondaryEmailAddress,
                                                            GenderCode = x.Gender.SystemName,
                                                            GenderName = x.Gender.Name,
                                                            DateOfBirth = x.DateOfBirth,
                                                            Address = x.Address,
                                                            WebsiteUrl = x.WebsiteUrl,
                                                            EmailVerificationStatus = x.EmailVerificationStatus,
                                                            EmailVerificationStatusDate = x.EmailVerificationStatusDate,

                                                            NumberVerificationStatus = x.NumberVerificationStatus,
                                                            NumberVerificationStatusDate = x.NumberVerificationStatusDate,


                                                            CreateDate = x.CreateDate,
                                                            CreatedByKey = x.CreatedBy.Guid,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                                            CreatedByAccountTypeId = x.CreatedBy.AccountTypeId,
                                                            CreatedById = x.CreatedById,

                                                            ModifyDate = x.ModifyDate,
                                                            ModifyByKey = x.ModifyBy.Guid,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name,

                                                            BnplDetails = null

                                                        })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();


                        if (Details != null)
                        {
                            if (Details.BnplDetails == null)
                            {
                                Details.BnplDetails = _HCoreContext.BNPLAccount.Where(x => x.AccountId == Details.ReferenceId && x.Account.AccountTypeId == UserAccountType.Appuser)
                                    .Select(x => new OUserAccount.BnplDetails
                                    {
                                        CreditLimitCheckDate = x.CreateDate,
                                        CreditLimit = x.CreditLimit
                                    }).FirstOrDefault();
                            }

                            Details.LastTransactionDate = _HCoreContext.HCUAccountTransaction
                                                           .Where(m => m.AccountId == Details.ReferenceId
                                                           && m.StatusId == HelperStatus.Transaction.Success
                                                           && (m.ModeId == Helpers.TransactionMode.Debit || m.ModeId == Helpers.TransactionMode.Credit))
                                                           //&& m.SourceId == TransactionSource.TUC)
                                                           .OrderByDescending(m => m.TransactionDate)
                                                           .Select(m => (DateTime?)m.TransactionDate).FirstOrDefault();

                            Details.AddressComponent = _HCoreContext.HCUAccount.Where(x => x.Id == Details.ReferenceId)
                          .Select(x => new OAddress
                          {
                              Address = x.Address,

                              Latitude = x.Latitude,
                              Longitude = x.Longitude,

                              CityAreaId = x.CityAreaId,
                              CityAreaCode = x.CityArea.Guid,
                              CityAreaName = x.CityArea.Name,

                              CityId = x.CityId,
                              CityCode = x.City.Guid,
                              CityName = x.City.Name,

                              StateId = x.StateId,
                              StateCode = x.State.Guid,
                              StateName = x.State.Name,

                              CountryId = x.CountryId,
                              CountryCode = x.Country.Guid,
                              CountryName = x.Country.Name,


                          }).FirstOrDefault();
                            using (_HCoreContextLogging = new HCoreContextLogging())
                            {
                                string AccessPin = HCoreEncrypt.DecryptHash(Details.AccessPin);
                                var SmsDetails = _HCoreContextLogging.HCLSmsNotification.Where(x => (x.AccountId == Details.ReferenceId || x.MobileNumber == Details.MobileNumber) && x.Message.Contains(AccessPin)).FirstOrDefault();
                                if (SmsDetails != null)
                                {
                                    Details.IsWelcomeSmsSent = true;
                                    if (SmsDetails.DeliveryDate != null)
                                    {
                                        Details.IsWelcomeSmsDelivered = false;
                                    }
                                }
                                else
                                {
                                    Details.IsWelcomeSmsSent = false;
                                }
                            }
                            if (Details.AccountTypeId == UserAccountType.Merchant)
                            {
                                Details.SubAccounts = _HCoreContext.HCUAccount.Where(x => x.OwnerId == Details.ReferenceId && x.AccountTypeId == UserAccountType.MerchantStore).Count();
                            }
                            if (Details.AccountTypeId == UserAccountType.MerchantStore)
                            {
                                Details.OwnerIconUrl = _HCoreContext.HCUAccount.Where(x => x.Guid == Details.OwnerKey).Select(x => x.IconStorage.Path).FirstOrDefault();
                            }
                            if (!string.IsNullOrEmpty(Details.OwnerIconUrl))
                            {
                                Details.OwnerIconUrl = _AppConfig.StorageUrl + Details.OwnerIconUrl;
                            }
                            else
                            {
                                Details.OwnerIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.PosterUrl))
                            {
                                Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                            }
                            else
                            {
                                Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            //Details.Password= HCoreEncrypt.DecryptHash(Details.Password);
                            _HCoreContext.Dispose();
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetUserAccount", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccount(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
                                                 where x.CountryId == _Request.UserReference.CountryId
                                                 select new OUserAccount.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     AccountTypeCode = x.AccountType.SystemName,
                                                     //AccountTypeName = x.AccountType.Name,
                                                     //AccountOperationTypeCode = x.AccountOperationType.SystemName,
                                                     //AccountOperationTypeName = x.AccountOperationType.Name,
                                                     OwnerKey = x.Owner.Guid,
                                                     OwnerDisplayName = x.Owner.DisplayName,
                                                     DisplayName = x.DisplayName,
                                                     Name = x.Name,
                                                     //AccountCode = x.AccountCode,
                                                     IconUrl = x.IconStorage.Path,
                                                     //RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                     //RegistrationSourceName = x.RegistrationSource.Name,
                                                     //AppKey = x.AppVersion.Parent.Guid,
                                                     //AppName = x.AppVersion.Parent.Name,
                                                     LastLoginDate = x.LastLoginDate,
                                                     RoleKey = x.Role.Guid,
                                                     RoleName = x.Role.Name,
                                                     //RequestKey = x.Request.Guid,
                                                     UserName = x.User.Username,
                                                     //FirstName = x.FirstName,
                                                     //LastName = x.LastName,
                                                     MobileNumber = x.MobileNumber,
                                                     //ContactNumber = x.ContactNumber,
                                                     EmailAddress = x.EmailAddress,
                                                     //GenderCode = x.Gender.SystemName,
                                                     //GenderName = x.Gender.Name,
                                                     //DateOfBirth = x.DateOfBirth,
                                                     //Address = x.Address,
                                                     CountryKey = x.Country.Guid,
                                                     CountryName = x.Country.Name,
                                                     //WebsiteUrl = x.WebsiteUrl,
                                                     //EmailVerificationStatus = x.EmailVerificationStatus,
                                                     //NumberVerificationStatus = x.NumberVerificationStatus,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                       .Where(_Request.SearchCondition)
                               .Count();
                    }

                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = _Request.TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserAccount.Details> Data = (from x in _HCoreContext.HCUAccount
                                                       where x.CountryId == _Request.UserReference.CountryId
                                                       select new OUserAccount.Details
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           AccountTypeCode = x.AccountType.SystemName,
                                                           //AccountTypeName = x.AccountType.Name,
                                                           //AccountOperationTypeCode = x.AccountOperationType.SystemName,
                                                           //AccountOperationTypeName = x.AccountOperationType.Name,
                                                           OwnerKey = x.Owner.Guid,
                                                           OwnerDisplayName = x.Owner.DisplayName,
                                                           DisplayName = x.DisplayName,
                                                           //AccountCode = x.AccountCode,
                                                           IconUrl = x.IconStorage.Path,
                                                           //RegistrationSourceCode = x.RegistrationSource.SystemName,
                                                           //RegistrationSourceName = x.RegistrationSource.Name,
                                                           //AppKey = x.AppVersion.Parent.Guid,
                                                           //AppName = x.AppVersion.Parent.Name,
                                                           LastLoginDate = x.LastLoginDate,
                                                           RoleKey = x.Role.Guid,
                                                           RoleName = x.Role.Name,
                                                           //RequestKey = x.Request.Guid,
                                                           UserName = x.User.Username,
                                                           Name = x.Name,
                                                           //FirstName = x.FirstName,
                                                           //LastName = x.LastName,
                                                           MobileNumber = x.MobileNumber,
                                                           //ContactNumber = x.ContactNumber,
                                                           EmailAddress = x.EmailAddress,
                                                           //GenderCode = x.Gender.SystemName,
                                                           //GenderName = x.Gender.Name,
                                                           //DateOfBirth = x.DateOfBirth,
                                                           //Address = x.Address,
                                                           CountryKey = x.Country.Guid,
                                                           CountryName = x.Country.Name,
                                                           //WebsiteUrl = x.WebsiteUrl,
                                                           //EmailVerificationStatus = x.EmailVerificationStatus,
                                                           //NumberVerificationStatus = x.NumberVerificationStatus,
                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                           ModifyDate = x.ModifyDate,
                                                           ModifyByKey = x.ModifyBy.Guid,
                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                        else
                        {
                            Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("Description: GetstorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccountLite(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    //#region Total Records
                    //int TotalRecords = (from x in _HCoreContext.HCUAccount
                    //                    orderby x.DisplayName ascending
                    //                    select new
                    //                    {
                    //                        ReferenceId = x.Id,
                    //                        ReferenceKey = x.Guid,
                    //                        AccountTypeCode = x.AccountType.SystemName,
                    //                        OwnerId = x.OwnerId,
                    //                        SubOwnerId = x.SubOwnerId,
                    //                        BankId = x.BankId,
                    //                        DisplayName = x.DisplayName,
                    //                        MobileNumber = x.MobileNumber,
                    //                        StatusId = x.StatusId,
                    //                    })
                    //                    .Where(_Request.SearchCondition)
                    //            .Count();
                    //#endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = 10;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    var Data = (from x in _HCoreContext.HCUAccount
                                where x.CountryId == _Request.UserReference.CountryId
                                && x.StatusId == HelperStatus.Default.Active
                                select new
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    AccountTypeCode = x.AccountType.SystemName,
                                    OwnerId = x.OwnerId,
                                    ParentOwnerId = x.Owner.OwnerId,
                                    SubOwnerId = x.SubOwnerId,
                                    SubOwnerOwnerId = x.SubOwner.OwnerId,
                                    BankId = x.BankId,
                                    DisplayName = x.DisplayName,
                                    Name = x.Name,
                                    MobileNumber = x.MobileNumber,
                                    StatusId = x.StatusId,
                                })
                                              .Where(_Request.SearchCondition)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(Data.Count, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("Description: GetstorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account owner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccountOwner(OUserAccountOwner.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserAccountOwner.Details Details = (from x in _HCoreContext.HCUAccountOwner
                                                             select new OUserAccountOwner.Details
                                                             {
                                                                 ReferenceKey = x.Guid,


                                                                 UserAccountKey = x.Account.Guid,
                                                                 UserAccountDisplayName = x.Account.DisplayName,
                                                                 UserAccountIconUrl = x.Account.IconStorage.Path,


                                                                 UserAccountTypeCode = x.AccountType.SystemName,
                                                                 UserAccountTypeName = x.AccountType.Name,

                                                                 OwnerAccountKey = x.Owner.Guid,
                                                                 OwnerAccountDisplayName = x.Owner.DisplayName,
                                                                 OwnerAccountIconUrl = x.Owner.IconStorage.Path,

                                                                 OwnerAccountTypeCode = x.Owner.AccountType.SystemName,
                                                                 OwnerAccountTypeName = x.Owner.AccountType.Name,

                                                                 StartTime = x.StartDate,
                                                                 EndTime = x.EndDate,

                                                                 CreateDate = x.CreateDate,
                                                                 CreatedByKey = x.CreatedBy.Guid,
                                                                 CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                                 CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                                 ModifyDate = x.ModifyDate,
                                                                 ModifyByKey = x.ModifyBy.Guid,
                                                                 ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                                 ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                                 StatusId = x.StatusId,
                                                                 StatusCode = x.Status.SystemName,
                                                                 StatusName = x.Status.Name

                                                             })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.OwnerAccountIconUrl))
                            {
                                Details.OwnerAccountIconUrl = _AppConfig.StorageUrl + Details.OwnerAccountIconUrl;
                            }
                            else
                            {
                                Details.OwnerAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetUserAccountOwner", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user account owner.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserAccountOwner(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountOwner
                                        select new OUserAccountOwner.Details
                                        {
                                            ReferenceKey = x.Guid,


                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,


                                            UserAccountTypeCode = x.AccountType.SystemName,
                                            UserAccountTypeName = x.AccountType.Name,

                                            OwnerAccountKey = x.Owner.Guid,
                                            OwnerAccountDisplayName = x.Owner.DisplayName,

                                            OwnerAccountTypeCode = x.Owner.AccountType.SystemName,
                                            OwnerAccountTypeName = x.Owner.AccountType.Name,

                                            StartTime = x.StartDate,
                                            EndTime = x.EndDate,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name

                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserAccountOwner.Details> Data = (from x in _HCoreContext.HCUAccountOwner
                                                            select new OUserAccountOwner.Details
                                                            {
                                                                ReferenceKey = x.Guid,


                                                                UserAccountKey = x.Account.Guid,
                                                                UserAccountDisplayName = x.Account.DisplayName,


                                                                UserAccountTypeCode = x.AccountType.SystemName,
                                                                UserAccountTypeName = x.AccountType.Name,

                                                                OwnerAccountKey = x.Owner.Guid,
                                                                OwnerAccountDisplayName = x.Owner.DisplayName,

                                                                OwnerAccountTypeCode = x.Owner.AccountType.SystemName,
                                                                OwnerAccountTypeName = x.Owner.AccountType.Name,

                                                                StartTime = x.StartDate,
                                                                EndTime = x.EndDate,

                                                                CreateDate = x.CreateDate,
                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                ModifyDate = x.ModifyDate,
                                                                ModifyByKey = x.ModifyBy.Guid,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name
                                                            })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetUserAccountOwner", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransaction(OTransaction.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OTransaction.Details Details = (from x in _HCoreContext.HCUAccountTransaction
                                                        select new OTransaction.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            ParentKey = x.Parent.Guid,
                                                            ParentDisplayName = x.Parent.DisplayName,
                                                            ParentIconUrl = x.Parent.IconStorage.Path,

                                                            SubParentKey = x.SubParent.Guid,
                                                            SubParentDisplayName = x.SubParent.DisplayName,
                                                            SubParentAddress = x.SubParent.Address,
                                                            SubParentLatitude = x.SubParent.Latitude,
                                                            SubParentLongitude = x.SubParent.Longitude,
                                                            SubParentIconUrl = x.SubParent.IconStorage.Path,

                                                            ParentTransactionKey = x.Parent.Guid,

                                                            UserAccountKey = x.Account.Guid,
                                                            UserAccountDisplayName = x.Account.DisplayName,
                                                            UserAccountMobileNumber = x.Account.MobileNumber,
                                                            UserAccountEmailAddress = x.Account.EmailAddress,
                                                            //UserAccountOwnerKey = x.Account.Owner.Guid,
                                                            //UserAccountOwnerDisplayName = x.Account.Owner.Guid,
                                                            UserAccountIconUrl = x.Account.IconStorage.Path,
                                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                            UserAccountTypeName = x.Account.AccountType.Name,


                                                            ModeCode = x.Mode.SystemName,
                                                            ModeName = x.Mode.Name,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            SourceCode = x.Source.SystemName,
                                                            SourceName = x.Source.Name,
                                                            Amount = x.Amount,
                                                            Charge = x.Charge,
                                                            ComissionAmount = x.ComissionAmount,
                                                            TotalAmount = x.TotalAmount,
                                                            //TotalAmountPercentage = x.TotalAmountPercentage,
                                                            Balance = x.Balance,
                                                            PurchaseAmount = x.PurchaseAmount,
                                                            ReferenceAmount = x.ReferenceAmount,
                                                            ReferenceInvoiceAmount = x.ReferenceInvoiceAmount,
                                                            TransactionDate = x.TransactionDate,
                                                            InvoiceNumber = x.InoviceNumber,
                                                            AccountNumber = x.AccountNumber,
                                                            ReferenceNumber = x.ReferenceNumber,
                                                            Comment = x.Comment,
                                                            GroupKey = x.GroupKey,
                                                            RequestKey = x.RequestKey,
                                                            //InvoiceKey = x.Invoice.Guid,
                                                            //InvoiceItemKey = x.InvoiceItem.Guid,

                                                            BankId = x.BankId,
                                                            BankKey = x.Bank.Guid,
                                                            BankDisplayName = x.Bank.DisplayName,
                                                            BankIconUrl = x.Bank.IconStorage.Path,


                                                            ProviderId = x.ProviderId,
                                                            ProviderKey = x.Provider.Guid,
                                                            ProviderDisplayName = x.Provider.DisplayName,
                                                            ProviderIconUrl = x.Provider.IconStorage.Path,

                                                            CardBrandId = x.CardBrandId,
                                                            CardBrandKey = x.CardBrand.Guid,
                                                            CardBrandDisplayName = x.CardBrand.Name,

                                                            CardSubBrandId = x.CardSubBrandId,
                                                            CardSubBrandKey = x.CardSubBrand.Guid,
                                                            CardSubBrandName = x.CardSubBrand.Name,

                                                            CardTypeId = x.CardTypeId,
                                                            CardTypeKey = x.CardType.Guid,
                                                            CardTypeName = x.CardType.Name,

                                                            CardBankId = x.CardBankId,
                                                            CardBankKey = x.CardBank.Guid,
                                                            CardBankName = x.CardBank.Name,

                                                            CashierId = x.CashierId,
                                                            CashierKey = x.Cashier.Guid,
                                                            CashierDisplayName = x.Cashier.DisplayName,


                                                            CreateDate = x.CreateDate,
                                                            CreatedByKey = x.CreatedBy.Guid,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                            ModifyDate = x.ModifyDate,
                                                            ModifyByKey = x.ModifyBy.Guid,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();
                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ParentIconUrl))
                            {
                                Details.ParentIconUrl = _AppConfig.StorageUrl + Details.ParentIconUrl;
                            }
                            else
                            {
                                Details.ParentIconUrl = _AppConfig.Default_Icon;
                            }

                            if (!string.IsNullOrEmpty(Details.GroupKey))
                            {
                                Details.Items = (from x in _HCoreContext.HCUAccountTransaction
                                                 where x.GroupKey == Details.GroupKey
                                                 select new OTransaction.Item
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     ParentKey = x.Parent.Guid,
                                                     ParentDisplayName = x.Parent.DisplayName,
                                                     ParentIconUrl = x.Parent.IconStorage.Path,
                                                     ParentTransactionKey = x.Parent.Guid,
                                                     UserAccountKey = x.Account.Guid,
                                                     UserAccountDisplayName = x.Account.DisplayName,
                                                     UserAccountIconUrl = x.Account.IconStorage.Path,
                                                     UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                     UserAccountTypeName = x.Account.AccountType.Name,
                                                     ModeCode = x.Mode.SystemName,
                                                     ModeName = x.Mode.Name,
                                                     TypeCode = x.Type.SystemName,
                                                     TypeName = x.Type.Name,
                                                     SourceCode = x.Source.SystemName,
                                                     SourceName = x.Source.Name,
                                                     Amount = x.Amount,
                                                     Charge = x.Charge,
                                                     ComissionAmount = x.ComissionAmount,
                                                     TotalAmount = x.TotalAmount,
                                                     //TotalAmountPercentage = x.TotalAmountPercentage,
                                                     Balance = x.Balance,
                                                     PurchaseAmount = x.PurchaseAmount,
                                                     ReferenceAmount = x.ReferenceAmount,
                                                     TransactionDate = x.TransactionDate,
                                                     InvoiceNumber = x.InoviceNumber,
                                                     AccountNumber = x.AccountNumber,
                                                     ReferenceNumber = x.ReferenceNumber,
                                                     Comment = x.Comment,
                                                     GroupKey = x.GroupKey,
                                                     RequestKey = x.RequestKey,
                                                     //InvoiceKey = x.Invoice.Guid,
                                                     //InvoiceItemKey = x.InvoiceItem.Guid,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                     CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                     ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name

                                                 }).ToList();
                            }
                            _HCoreContext.Dispose();
                            if (!string.IsNullOrEmpty(Details.RequestKey))
                            {
                                using (_HCoreContextLogging = new HCoreContextLogging())
                                {
                                    Details.RequestLog = (from n in _HCoreContextLogging.HCLCoreUsage
                                                          where n.Guid == Details.RequestKey
                                                          select new OTransaction.RequestLog
                                                          {
                                                              Request = n.Request,
                                                              Response = n.Response,
                                                          }).FirstOrDefault();
                                    _HCoreContextLogging.Dispose();
                                }

                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetTransaction", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetTransaction(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "TransactionDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                        select new OTransaction.Details
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            ParentKey = x.Parent.Guid,
                                            ParentDisplayName = x.Parent.DisplayName,
                                            ParentIconUrl = x.Parent.IconStorage.Path,
                                            ParentTransactionKey = x.Parent.Guid,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,
                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                            UserAccountTypeName = x.Account.AccountType.Name,
                                            ModeCode = x.Mode.SystemName,
                                            ModeName = x.Mode.Name,
                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,
                                            SourceCode = x.Source.SystemName,
                                            SourceName = x.Source.Name,
                                            Amount = x.Amount,
                                            Charge = x.Charge,
                                            ComissionAmount = x.ComissionAmount,
                                            TotalAmount = x.TotalAmount,
                                            PurchaseAmount = x.PurchaseAmount,
                                            TransactionDate = x.TransactionDate,
                                            InvoiceNumber = x.InoviceNumber,
                                            AccountNumber = x.AccountNumber,
                                            ReferenceNumber = x.ReferenceNumber,
                                            GroupKey = x.GroupKey,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            ModifyDate = x.ModifyDate,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OTransaction.Details> Data = (from x in _HCoreContext.HCUAccountTransaction
                                                       select new OTransaction.Details
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           ParentKey = x.Parent.Guid,
                                                           ParentDisplayName = x.Parent.DisplayName,
                                                           ParentIconUrl = x.Parent.IconStorage.Path,
                                                           ParentTransactionKey = x.Parent.Guid,
                                                           UserAccountKey = x.Account.Guid,
                                                           UserAccountDisplayName = x.Account.DisplayName,
                                                           UserAccountIconUrl = x.Account.IconStorage.Path,
                                                           UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                           UserAccountTypeName = x.Account.AccountType.Name,
                                                           ModeCode = x.Mode.SystemName,
                                                           ModeName = x.Mode.Name,
                                                           TypeCode = x.Type.SystemName,
                                                           TypeName = x.Type.Name,
                                                           SourceCode = x.Source.SystemName,
                                                           SourceName = x.Source.Name,
                                                           Amount = x.Amount,
                                                           Charge = x.Charge,
                                                           ComissionAmount = x.ComissionAmount,
                                                           TotalAmount = x.TotalAmount,
                                                           PurchaseAmount = x.PurchaseAmount,
                                                           TransactionDate = x.TransactionDate,
                                                           InvoiceNumber = x.InoviceNumber,
                                                           AccountNumber = x.AccountNumber,
                                                           ReferenceNumber = x.ReferenceNumber,
                                                           GroupKey = x.GroupKey,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                           ModifyDate = x.ModifyDate,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                        //if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        //{
                        //    Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        //}
                        //else
                        //{
                        //    Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        //}
                        //if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        //{
                        //    Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        //}
                        //else
                        //{
                        //Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        //}
                        if (!string.IsNullOrEmpty(Details.ParentIconUrl))
                        {
                            Details.ParentIconUrl = _AppConfig.StorageUrl + Details.ParentIconUrl;
                        }
                        else
                        {
                            Details.ParentIconUrl = _AppConfig.Default_Poster;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("Description: GetstorageList", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserActivity(OUserActivity.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserActivity.Details Details = (from x in _HCoreContext.HCUAccountActivity
                                                         select new OUserActivity.Details
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             UserAccountKey = x.Account.Guid,
                                                             UserAccountDisplayName = x.Account.DisplayName,
                                                             UserAccountIconUrl = x.Account.IconStorage.Path,
                                                             UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                             UserAccountTypeName = x.Account.AccountType.Name,

                                                             UserDeviceKey = x.Device.Guid,
                                                             UserDeviceSerialNumber = x.Device.SerialNumber,

                                                             FeatureKey = x.Feature.Guid,
                                                             FeatureName = x.Feature.Name,
                                                             ActivityTypeCode = x.ActivityType.SystemName,
                                                             ActivityTypeName = x.ActivityType.Name,

                                                             ParentReferenceKey = x.ReferenceKey,
                                                             SubReferenceKey = x.SubReferenceKey,

                                                             EntityName = x.EntityName,
                                                             FieldName = x.FieldName,
                                                             OldValue = x.OldValue,
                                                             NewValue = x.NewValue,
                                                             Description = x.Description,
                                                             CreateDate = x.CreateDate,

                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetTransaction", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserActivity(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountActivity
                                        select new OUserActivity.Details
                                        {

                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,
                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                            UserAccountTypeName = x.Account.AccountType.Name,

                                            UserDeviceKey = x.Device.Guid,
                                            UserDeviceSerialNumber = x.Device.SerialNumber,

                                            FeatureKey = x.Feature.Guid,
                                            FeatureName = x.Feature.Name,
                                            ActivityTypeCode = x.ActivityType.SystemName,
                                            ActivityTypeName = x.ActivityType.Name,

                                            ParentReferenceKey = x.ReferenceKey,
                                            SubReferenceKey = x.SubReferenceKey,

                                            EntityName = x.EntityName,
                                            FieldName = x.FieldName,
                                            OldValue = x.OldValue,
                                            NewValue = x.NewValue,
                                            Description = x.Description,
                                            CreateDate = x.CreateDate,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserActivity.Details> Data = (from x in _HCoreContext.HCUAccountActivity
                                                        select new OUserActivity.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            UserAccountKey = x.Account.Guid,
                                                            UserAccountDisplayName = x.Account.DisplayName,
                                                            UserAccountIconUrl = x.Account.IconStorage.Path,
                                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                            UserAccountTypeName = x.Account.AccountType.Name,

                                                            UserDeviceKey = x.Device.Guid,
                                                            UserDeviceSerialNumber = x.Device.SerialNumber,

                                                            FeatureKey = x.Feature.Guid,
                                                            FeatureName = x.Feature.Name,
                                                            ActivityTypeCode = x.ActivityType.SystemName,
                                                            ActivityTypeName = x.ActivityType.Name,

                                                            ParentReferenceKey = x.ReferenceKey,
                                                            SubReferenceKey = x.SubReferenceKey,

                                                            EntityName = x.EntityName,
                                                            FieldName = x.FieldName,
                                                            OldValue = x.OldValue,
                                                            NewValue = x.NewValue,
                                                            Description = x.Description,
                                                            CreateDate = x.CreateDate,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetUserActivity", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserDevice(OUserDevice.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserDevice.Details Details = (from x in _HCoreContext.HCUAccountDevice
                                                       select new OUserDevice.Details
                                                       {
                                                           ReferenceKey = x.Guid,

                                                           UserAccountKey = x.Account.Guid,
                                                           UserAccountDisplayName = x.Account.DisplayName,
                                                           UserAccountIconUrl = x.Account.IconStorage.Path,

                                                           UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                           UserAccountTypeName = x.Account.AccountType.Name,

                                                           SerialNumber = x.SerialNumber,

                                                           OsCode = x.OsVersion.Parent.SystemName,
                                                           OsName = x.OsVersion.Parent.Name,

                                                           OsVersionKey = x.OsVersion.Guid,
                                                           OsVersionName = x.OsVersion.Name,

                                                           BrandKey = x.Model.Parent.Guid,
                                                           BrandName = x.Model.Parent.Name,

                                                           ModelKey = x.Model.Guid,
                                                           ModelName = x.Model.Name,

                                                           ResolutionKey = x.Resolution.Guid,
                                                           ResolutionName = x.Resolution.Name,

                                                           NetworkOperatorKey = x.NetworkOperator.Guid,
                                                           NetworkOperatorName = x.NetworkOperator.Name,

                                                           NotificationUrl = x.NotificationUrl,


                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                           CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                           ModifyDate = x.ModifyDate,
                                                           ModifyByKey = x.ModifyBy.Guid,
                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                           ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetTransaction", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserDevice(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountDevice
                                        select new OUserDevice.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                            UserAccountTypeName = x.Account.AccountType.Name,

                                            SerialNumber = x.SerialNumber,

                                            OsCode = x.OsVersion.Parent.SystemName,
                                            OsName = x.OsVersion.Parent.Name,

                                            OsVersionKey = x.OsVersion.Guid,
                                            OsVersionName = x.OsVersion.Name,

                                            BrandKey = x.Model.Parent.Guid,
                                            BrandName = x.Model.Parent.Name,

                                            ModelKey = x.Model.Guid,
                                            ModelName = x.Model.Name,

                                            ResolutionKey = x.Resolution.Guid,
                                            ResolutionName = x.Resolution.Name,

                                            NetworkOperatorKey = x.NetworkOperator.Guid,
                                            NetworkOperatorName = x.NetworkOperator.Name,

                                            NotificationUrl = x.NotificationUrl,


                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserDevice.Details> Data = (from x in _HCoreContext.HCUAccountDevice
                                                      select new OUserDevice.Details
                                                      {
                                                          ReferenceKey = x.Guid,

                                                          UserAccountKey = x.Account.Guid,
                                                          UserAccountDisplayName = x.Account.DisplayName,
                                                          UserAccountIconUrl = x.Account.IconStorage.Path,

                                                          UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                          UserAccountTypeName = x.Account.AccountType.Name,

                                                          SerialNumber = x.SerialNumber,

                                                          OsCode = x.OsVersion.Parent.SystemName,
                                                          OsName = x.OsVersion.Parent.Name,

                                                          OsVersionKey = x.OsVersion.Guid,
                                                          OsVersionName = x.OsVersion.Name,

                                                          BrandKey = x.Model.Parent.Guid,
                                                          BrandName = x.Model.Parent.Name,

                                                          ModelKey = x.Model.Guid,
                                                          ModelName = x.Model.Name,

                                                          ResolutionKey = x.Resolution.Guid,
                                                          ResolutionName = x.Resolution.Name,

                                                          NetworkOperatorKey = x.NetworkOperator.Guid,
                                                          NetworkOperatorName = x.NetworkOperator.Name,

                                                          NotificationUrl = x.NotificationUrl,


                                                          CreateDate = x.CreateDate,
                                                          CreatedByKey = x.CreatedBy.Guid,
                                                          CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                          CreatedByIconUrl = x.CreatedBy.IconStorage.Path,

                                                          ModifyDate = x.ModifyDate,
                                                          ModifyByKey = x.ModifyBy.Guid,
                                                          ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                          ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetUserActivity", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserSession(OUserSession.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserSession.Details Details = (from x in _HCoreContext.HCUAccountSession
                                                        select new OUserSession.Details
                                                        {
                                                            ReferenceKey = x.Guid,

                                                            UserAccountKey = x.Account.Guid,
                                                            UserAccountDisplayName = x.Account.DisplayName,
                                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                            UserAccountTypeName = x.Account.AccountType.Name,

                                                            DeviceKey = x.Device.Guid,
                                                            DeviceSerialNumber = x.Device.SerialNumber,

                                                            //AppVersioName = x.AppVersion.Name,
                                                            //AppVersionKey = x.AppVersion.Guid,

                                                            //AppKey = x.AppVersion.Parent.Guid,
                                                            //AppName = x.AppVersion.Name,
                                                            LastActivityDate = x.LastActivityDate,

                                                            LoginDate = x.LoginDate,
                                                            LogoutDate = x.LogoutDate,

                                                            IpAddress = x.IPAddress,
                                                            Latitude = x.Latitude,
                                                            Longitude = x.Longitude,

                                                            ModifyDate = x.ModifyDate,
                                                            ModifyByKey = x.ModifyBy.Guid,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetTransaction", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserSession(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "LoginDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountSession
                                        where x.Account.CountryId == _Request.UserReference.CountryId
                                        select new OUserSession.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,

                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                            UserAccountTypeName = x.Account.AccountType.Name,

                                            DeviceKey = x.Device.Guid,
                                            DeviceSerialNumber = x.Device.SerialNumber,

                                            //AppVersioName = x.AppVersion.Name,
                                            //AppVersionKey = x.AppVersion.Guid,

                                            //AppKey = x.AppVersion.Parent.Guid,
                                            //AppName = x.AppVersion.Parent.Name,
                                            LastActivityDate = x.LastActivityDate,

                                            LoginDate = x.LoginDate,
                                            LogoutDate = x.LogoutDate,

                                            IpAddress = x.IPAddress,
                                            Latitude = x.Latitude,
                                            Longitude = x.Longitude,

                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,

                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserSession.Details> Data = (from x in _HCoreContext.HCUAccountSession
                                                       where x.Account.CountryId == _Request.UserReference.CountryId
                                                       select new OUserSession.Details
                                                       {
                                                           ReferenceKey = x.Guid,

                                                           UserAccountKey = x.Account.Guid,
                                                           UserAccountDisplayName = x.Account.DisplayName,

                                                           UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                           UserAccountTypeName = x.Account.AccountType.Name,

                                                           DeviceKey = x.Device.Guid,
                                                           DeviceSerialNumber = x.Device.SerialNumber,

                                                           //AppVersioName = x.AppVersion.Name,
                                                           //AppVersionKey = x.AppVersion.Guid,

                                                           //AppKey = x.AppVersion.Parent.Guid,
                                                           //AppName = x.AppVersion.Parent.Name,
                                                           LastActivityDate = x.LastActivityDate,

                                                           LoginDate = x.LoginDate,
                                                           LogoutDate = x.LogoutDate,

                                                           IpAddress = x.IPAddress,
                                                           Latitude = x.Latitude,
                                                           Longitude = x.Longitude,

                                                           ModifyDate = x.ModifyDate,
                                                           ModifyByKey = x.ModifyBy.Guid,
                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetUserSession", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserParameter(OUserParameter.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserParameter.Details Details = (from x in _HCoreContext.HCUAccountParameter
                                                          select new OUserParameter.Details
                                                          {
                                                              ReferenceKey = x.Guid,

                                                              TypeCode = x.Type.SystemName,
                                                              TypeName = x.Type.Name,

                                                              ParentKey = x.Parent.Guid,
                                                              ParentName = x.Parent.Name,

                                                              SubParentKey = x.SubParent.Guid,
                                                              SubParentName = x.SubParent.Name,

                                                              UserAccountKey = x.Account.Guid,
                                                              UserAccountDisplayName = x.Account.DisplayName,
                                                              UserAccountIconUrl = x.Account.IconStorage.Path,

                                                              UserDeviceKey = x.Device.Guid,
                                                              UserDeviceSerialNumber = x.Device.SerialNumber,

                                                              CommonKey = x.Common.Guid,
                                                              CommonName = x.Common.Name,

                                                              ParameterKey = x.Parameter.Guid,
                                                              ParameterName = x.Parameter.Name,

                                                              HelperCode = x.Helper.SystemName,
                                                              HelperName = x.Helper.Name,

                                                              Name = x.Name,
                                                              SystemName = x.SystemName,
                                                              Value = x.Value,
                                                              SubValue = x.SubValue,
                                                              Description = x.Description,
                                                              Data = x.Data,
                                                              VerificationKey = x.Verification.Guid,
                                                              CountValue = x.CountValue,
                                                              AverageValue = x.AverageValue,
                                                              StartTime = x.StartTime,
                                                              EndTime = x.EndTime,
                                                              TransactionKey = x.Transaction.Guid,
                                                              IconUrl = x.IconStorage.Path,
                                                              PosterUrl = x.PosterStorage.Path,
                                                              RequestKey = x.RequestKey,
                                                              CreateDate = x.CreateDate,
                                                              CreatedByKey = x.CreatedBy.Guid,
                                                              CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                              CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                                              ModifyDate = x.ModifyDate,
                                                              ModifyByKey = x.ModifyBy.Guid,
                                                              ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                              ModifyByIconUrl = x.ModifyBy.IconStorage.Path,

                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name
                                                          })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                            {
                                Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                            }
                            else
                            {
                                Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                            {
                                Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                            }
                            else
                            {
                                Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetUserParameter", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserParameter(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountParameter
                                        select new OUserParameter.Details
                                        {
                                            ReferenceKey = x.Guid,
                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,
                                            ParentKey = x.Parent.Guid,
                                            ParentName = x.Parent.Name,
                                            SubParentKey = x.SubParent.Guid,
                                            SubParentName = x.SubParent.Name,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,
                                            UserDeviceKey = x.Device.Guid,
                                            UserDeviceSerialNumber = x.Device.SerialNumber,
                                            CommonKey = x.Common.Guid,
                                            CommonName = x.Common.Name,
                                            ParameterKey = x.Parameter.Guid,
                                            ParameterName = x.Parameter.Name,
                                            HelperCode = x.Helper.SystemName,
                                            HelperName = x.Helper.Name,
                                            Name = x.Name,
                                            SystemName = x.SystemName,
                                            Value = x.Value,
                                            VerificationKey = x.Verification.Guid,
                                            CountValue = x.CountValue,
                                            AverageValue = x.AverageValue,
                                            StartTime = x.StartTime,
                                            EndTime = x.EndTime,
                                            TransactionKey = x.Transaction.Guid,
                                            IconUrl = x.IconStorage.Path,
                                            PosterUrl = x.PosterStorage.Path,
                                            RequestKey = x.RequestKey,
                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                  .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserParameter.Details> Data = (from x in _HCoreContext.HCUAccountParameter
                                                         select new OUserParameter.Details
                                                         {
                                                             ReferenceKey = x.Guid,
                                                             TypeCode = x.Type.SystemName,
                                                             TypeName = x.Type.Name,
                                                             ParentKey = x.Parent.Guid,
                                                             ParentName = x.Parent.Name,
                                                             SubParentKey = x.SubParent.Guid,
                                                             SubParentName = x.SubParent.Name,
                                                             UserAccountKey = x.Account.Guid,
                                                             UserAccountDisplayName = x.Account.DisplayName,
                                                             UserAccountIconUrl = x.Account.IconStorage.Path,
                                                             UserDeviceKey = x.Device.Guid,
                                                             UserDeviceSerialNumber = x.Device.SerialNumber,
                                                             CommonKey = x.Common.Guid,
                                                             CommonName = x.Common.Name,
                                                             ParameterKey = x.Parameter.Guid,
                                                             ParameterName = x.Parameter.Name,
                                                             HelperCode = x.Helper.SystemName,
                                                             HelperName = x.Helper.Name,
                                                             Name = x.Name,
                                                             SystemName = x.SystemName,
                                                             Value = x.Value,
                                                             VerificationKey = x.Verification.Guid,
                                                             CountValue = x.CountValue,
                                                             AverageValue = x.AverageValue,
                                                             StartTime = x.StartTime,
                                                             EndTime = x.EndTime,
                                                             TransactionKey = x.Transaction.Guid,
                                                             IconUrl = x.IconStorage.Path,
                                                             PosterUrl = x.PosterStorage.Path,
                                                             RequestKey = x.RequestKey,
                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                             CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                                             ModifyDate = x.ModifyDate,
                                                             ModifyByKey = x.ModifyBy.Guid,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                             ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                               .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        if (!string.IsNullOrEmpty(Details.PosterUrl))
                        {
                            Details.PosterUrl = _AppConfig.StorageUrl + Details.PosterUrl;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserParameter", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserLocation(OUserLocation.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {
                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserLocation.Details Details = (from x in _HCoreContext.HCUAccountLocation
                                                         select new OUserLocation.Details
                                                         {
                                                             ReferenceKey = x.Guid,

                                                             UserAccountKey = x.Account.Guid,
                                                             UserAccountDisplayName = x.Account.DisplayName,
                                                             UserAccountIconUrl = x.Account.IconStorage.Path,

                                                             UserDeviceKey = x.Device.Guid,
                                                             UserDeviceSerialNumber = x.Device.SerialNumber,

                                                             Location = x.Location,
                                                             Latitude = x.Latitude,
                                                             Longitude = x.Longitude,

                                                             CountryKey = x.Country.Guid,
                                                             CountryName = x.Country.Name,

                                                             RegionKey = x.Region.Guid,
                                                             RegionName = x.Region.Name,

                                                             RegionAreaKey = x.RegionArea.Guid,
                                                             RegionAreaName = x.RegionArea.Name,

                                                             CityKey = x.City.Guid,
                                                             CityName = x.City.Name,

                                                             CityAreaKey = x.CityArea.Guid,
                                                             CityAreaName = x.CityArea.Name,

                                                             CreateDate = x.CreateDate,
                                                         })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetUserLocation", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user location.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserLocation(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountLocation
                                        select new OUserLocation.Details
                                        {
                                            ReferenceKey = x.Guid,

                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                            UserDeviceKey = x.Device.Guid,
                                            UserDeviceSerialNumber = x.Device.SerialNumber,

                                            Location = x.Location,
                                            Latitude = x.Latitude,
                                            Longitude = x.Longitude,

                                            CountryKey = x.Country.Guid,
                                            CountryName = x.Country.Name,

                                            RegionKey = x.Region.Guid,
                                            RegionName = x.Region.Name,

                                            RegionAreaKey = x.RegionArea.Guid,
                                            RegionAreaName = x.RegionArea.Name,

                                            CityKey = x.City.Guid,
                                            CityName = x.City.Name,

                                            CityAreaKey = x.CityArea.Guid,
                                            CityAreaName = x.CityArea.Name,

                                            CreateDate = x.CreateDate,
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserLocation.Details> Data = (from x in _HCoreContext.HCUAccountLocation
                                                        select new OUserLocation.Details
                                                        {
                                                            ReferenceKey = x.Guid,

                                                            UserAccountKey = x.Account.Guid,
                                                            UserAccountDisplayName = x.Account.DisplayName,
                                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                                            UserDeviceKey = x.Device.Guid,
                                                            UserDeviceSerialNumber = x.Device.SerialNumber,

                                                            Location = x.Location,
                                                            Latitude = x.Latitude,
                                                            Longitude = x.Longitude,

                                                            CountryKey = x.Country.Guid,
                                                            CountryName = x.Country.Name,

                                                            RegionKey = x.Region.Guid,
                                                            RegionName = x.Region.Name,

                                                            RegionAreaKey = x.RegionArea.Guid,
                                                            RegionAreaName = x.RegionArea.Name,

                                                            CityKey = x.City.Guid,
                                                            CityName = x.City.Name,

                                                            CityAreaKey = x.CityArea.Guid,
                                                            CityAreaName = x.CityArea.Name,

                                                            CreateDate = x.CreateDate,
                                                        })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserLocation", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserInvoice(OUserInvoice.Manage _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Reference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                    #endregion
                }
                else
                {

                    #region Operations
                    using (_HCoreContext = new HCoreContext())
                    {
                        OUserInvoice.Details Details = (from x in _HCoreContext.HCUAccountInvoice
                                                        select new OUserInvoice.Details
                                                        {
                                                            ReferenceKey = x.Guid,
                                                            TypeCode = x.Type.SystemName,
                                                            TypeName = x.Type.Name,
                                                            ParentKey = x.Parent.Guid,
                                                            ParentName = x.Parent.Name,
                                                            UserAccountKey = x.Account.Guid,
                                                            UserAccountDisplayName = x.Account.DisplayName,
                                                            UserAccountIconUrl = x.Account.IconStorage.Path,
                                                            InvoiceNumber = x.InoviceNumber,
                                                            Name = x.Name,
                                                            Description = x.Description,
                                                            FromName = x.FromName,
                                                            FromAddress = x.FromAddress,
                                                            FromContactNumber = x.FromContactNumber,
                                                            FromEmailAddress = x.FromEmailAddress,
                                                            FromFax = x.FromFax,
                                                            ToName = x.ToName,
                                                            ToAddress = x.ToAddress,
                                                            ToContactNumber = x.ToContactNumber,
                                                            ToEmailAddress = x.ToEmailAddress,
                                                            ToFax = x.ToFax,
                                                            Amount = x.Amount,
                                                            Charge = x.Charge,
                                                            ComissionAmount = x.ComissionAmount,
                                                            TotalAmount = x.TotalAmount,
                                                            InvoiceDate = x.InoviceDate,
                                                            StartDate = x.StartDate,
                                                            EndDate = x.EndDate,
                                                            Comment = x.Comment,
                                                            CreateDate = x.CreateDate,
                                                            CreatedByKey = x.CreatedBy.Guid,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                                            ModifyDate = x.ModifyDate,
                                                            ModifyByKey = x.ModifyBy.Guid,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                                      .Where(_Request.Reference)
                                                      .FirstOrDefault();

                        _HCoreContext.Dispose();

                        if (Details != null)
                        {
                            if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                            {
                                Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                            }
                            else
                            {
                                Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Details, "HC1000");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC1000");
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetUserLocation", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the user invoice.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetUserInvoice(OList.Request _Request)
        {

            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.HCUAccountInvoice
                                        where x.ParentId == null
                                        select new OUserInvoice.Details
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            TypeCode = x.Type.SystemName,
                                            TypeName = x.Type.Name,
                                            ParentKey = x.Parent.Guid,
                                            ParentName = x.Parent.Name,
                                            UserAccountKey = x.Account.Guid,
                                            UserAccountDisplayName = x.Account.DisplayName,
                                            UserAccountIconUrl = x.Account.IconStorage.Path,

                                            UserAccountTypeCode = x.Account.AccountType.SystemName,
                                            UserAccountTypeName = x.Account.AccountType.Name,

                                            InvoiceNumber = x.InoviceNumber,
                                            Name = x.Name,
                                            Description = x.Description,
                                            FromName = x.FromName,
                                            ToName = x.ToName,
                                            Amount = x.Amount,
                                            Charge = x.Charge,
                                            TotalAmount = x.TotalAmount,
                                            InvoiceDate = x.InoviceDate,
                                            StartDate = x.StartDate,
                                            EndDate = x.EndDate,
                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                            ModifyDate = x.ModifyDate,
                                            ModifyByKey = x.ModifyBy.Guid,
                                            ModifyByDisplayName = x.ModifyBy.DisplayName,
                                            ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OUserInvoice.Details> Data = (from x in _HCoreContext.HCUAccountInvoice
                                                       where x.ParentId == null
                                                       select new OUserInvoice.Details
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,
                                                           TypeCode = x.Type.SystemName,
                                                           TypeName = x.Type.Name,
                                                           ParentKey = x.Parent.Guid,
                                                           ParentName = x.Parent.Name,
                                                           UserAccountKey = x.Account.Guid,
                                                           UserAccountDisplayName = x.Account.DisplayName,
                                                           UserAccountIconUrl = x.Account.IconStorage.Path,
                                                           UserAccountTypeCode = x.Account.AccountType.SystemName,
                                                           UserAccountTypeName = x.Account.AccountType.Name,
                                                           InvoiceNumber = x.InoviceNumber,
                                                           Name = x.Name,
                                                           Description = x.Description,
                                                           FromName = x.FromName,
                                                           ToName = x.ToName,
                                                           Amount = x.Amount,
                                                           Charge = x.Charge,
                                                           TotalAmount = x.TotalAmount,
                                                           InvoiceDate = x.InoviceDate,
                                                           StartDate = x.StartDate,
                                                           EndDate = x.EndDate,
                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                           CreatedByIconUrl = x.CreatedBy.IconStorage.Path,
                                                           ModifyDate = x.ModifyDate,
                                                           ModifyByKey = x.ModifyBy.Guid,
                                                           ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                           ModifyByIconUrl = x.ModifyBy.IconStorage.Path,
                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.UserAccountIconUrl))
                        {
                            Details.UserAccountIconUrl = _AppConfig.StorageUrl + Details.UserAccountIconUrl;
                        }
                        else
                        {
                            Details.UserAccountIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.CreatedByIconUrl))
                        {
                            Details.CreatedByIconUrl = _AppConfig.StorageUrl + Details.CreatedByIconUrl;
                        }
                        else
                        {
                            Details.CreatedByIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.ModifyByIconUrl))
                        {
                            Details.ModifyByIconUrl = _AppConfig.StorageUrl + Details.ModifyByIconUrl;
                        }
                        else
                        {
                            Details.ModifyByIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetUserInvoice", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
