//==================================================================================
// FileName: FrameworkDevice.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to device
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Data;
using HCore.Data.Models;
using HCore.Data.Logging;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using static HCore.Helper.HCoreConstant.Helpers;

namespace HCore.Operations.Framework
{
    public class FrameworkDevice
    {
        HCoreContext _HCoreContext;
        ODevice.Response.DeviceConnect _AppResponse;
        internal class OAppVersions
        {
            public string? Key { get; set; }
            public int? AppStatusId { get; set; }
            public int? AppVersionStatusId { get; set; }
        }
        internal static List<OAppVersions> _AppVersions = new List<OAppVersions>();
        /// <summary>
        /// Description: Checks the application version.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CheckAppVersion(AppVersionCheck.Request _Request)
        {
            #region Manage Exception
            try
            {
                var CAppDetails = _AppVersions
                        .Where(x => x.Key == _Request.AppKey)
                        .FirstOrDefault();
                if (CAppDetails != null)
                {
                    if (CAppDetails.AppStatusId != HelperStatus.Default.Active)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "APPE002", "App update required");
                    }
                    else if (CAppDetails.AppVersionStatusId != HelperStatus.Default.Active)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "APPE003", "App update required");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "APPE004", "App version ok");
                    }
                }
                else
                {

                    #region Operation
                    using (_HCoreContext = new HCoreContext())
                    {
                        var AppDetails = _HCoreContext.HCCoreAppVersion
                            .Where(x => x.App.AppKey == _Request.AppKey)
                            .OrderByDescending(x => x.CreateDate)
                            .Select(x => new OAppVersions
                            {
                                Key = x.App.AppKey,
                                AppStatusId = x.App.StatusId,
                                AppVersionStatusId = x.StatusId,
                            })
                            .FirstOrDefault();
                        if (AppDetails != null)
                        {
                            _AppVersions.Add(AppDetails);
                            if (AppDetails.AppStatusId != HelperStatus.Default.Active)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "APPE002", "App update required");
                            }
                            else if (AppDetails.AppVersionStatusId != HelperStatus.Default.Active)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "APPE003", "App update required");
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "APPE004", "App version ok");
                            }
                        }
                        else
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "APPE001", "App update required");
                        }
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("CheckAppVersion", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Connects the application.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ConnectApp(ODevice.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Update Device Details
                    HCUAccountDevice DeviceDetails = _HCoreContext.HCUAccountDevice.Where(x => x.AccountId == _Request.UserReference.AccountId && x.SerialNumber == _Request.SerialNumber).FirstOrDefault();
                    if (DeviceDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.NotificationUrl))
                        {
                            DeviceDetails.NotificationUrl = _Request.NotificationUrl;
                        }
                        DeviceDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                    }
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ConnectApp", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Connects the device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ConnectDevice(ODevice.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _AppResponse = new ODevice.Response.DeviceConnect();
                    #region CheckAccountDetails
                    string TempPrefix = _AppConfig.AppUserPrefix + _Request.MobileNumber;
                    var UserAccountDetails = _HCoreContext.HCUAccount.Where(x => x.User.Username == TempPrefix && x.AccountTypeId == UserAccountType.Appuser)
                                                          .Select(x => new
                                                          {
                                                              Id = x.Id,
                                                              StatusId = x.StatusId,

                                                              FirstName = x.FirstName,
                                                              MiddleName = x.MiddleName,
                                                              LastName = x.LastName,

                                                              GenderCode = x.Gender.SystemName,
                                                              EmailAddress = x.EmailAddress,
                                                              DateOfBirth = x.DateOfBirth,

                                                              CityAreaId = x.CityAreaId,
                                                              CityAreaKey = x.CityArea.Guid,
                                                              CityAreaName = x.CityArea.Name,

                                                              StateId = x.StateId,
                                                              StateKey = x.State.Guid,
                                                              StateName = x.State.Name,

                                                              CityId = x.CityId,
                                                              CityName = x.City.Name,
                                                              CityKey = x.City.Guid,
                                                              AddressLine1 = x.Address,
                                                          })
                                                          .FirstOrDefault();
                    if (UserAccountDetails != null)
                    {
                        _AppResponse.IsAccountExists = true;
                        _AppResponse.FirstName = UserAccountDetails.FirstName;
                        _AppResponse.MiddleName = UserAccountDetails.MiddleName;
                        _AppResponse.LastName = UserAccountDetails.LastName;
                        _AppResponse.EmailAddress = UserAccountDetails.EmailAddress;
                        _AppResponse.GenderCode = UserAccountDetails.GenderCode;
                        _AppResponse.CityId = UserAccountDetails.CityId;
                        _AppResponse.CityKey = UserAccountDetails.CityKey;
                        _AppResponse.CityName = UserAccountDetails.CityName;
                        _AppResponse.CityAreaId = UserAccountDetails.CityAreaId;
                        _AppResponse.StateKey = UserAccountDetails.StateKey;
                        _AppResponse.StateId = UserAccountDetails.StateId;
                        _AppResponse.StateName = UserAccountDetails.StateName;
                        _AppResponse.AddressLine1 = UserAccountDetails.AddressLine1;
                        _AppResponse.DateOfBirth = UserAccountDetails.DateOfBirth;
                    }
                    else
                    {
                        _AppResponse.IsAccountExists = false;
                    }
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _AppResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ConnectDevice", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the device notification URL.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateDeviceNotificationUrl(ODevice.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    //var Session = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.AccountId  && x.StatusId == 2).OrderByDescending(x=>x.LoginDate).FirstOrDefault();
                    //if (Session != null)
                    //{
                    //    Session.NotificationUrl = _Request.NotificationUrl;
                    //    Session.ModifyById = _Request.UserReference.AccountId;
                    //    Session.ModifyDate = HCoreHelper.GetGMTDateTime();
                    //    _HCoreContext.SaveChanges();
                    //}
                    var ASession = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == _Request.AccountId && x.Id == _Request.UserReference.AccountSessionId).OrderByDescending(x => x.LoginDate).FirstOrDefault();
                    if (ASession != null)
                    {
                        ASession.NotificationUrl = _Request.NotificationUrl;
                        ASession.ModifyById = _Request.UserReference.AccountId;
                        ASession.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                    }
                    var Device = _HCoreContext.HCUAccountDevice.Where(x => x.AccountId == _Request.AccountId && x.Id == _Request.UserReference.DeviceId).FirstOrDefault();
                    if (Device != null)
                    {
                        Device.NotificationUrl = _Request.NotificationUrl;
                        Device.ModifyById = _Request.UserReference.AccountId;
                        Device.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                    }
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("ConnectDevice", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

    }
}
