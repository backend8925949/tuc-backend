//==================================================================================
// FileName: FrameworkSystem.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to system actions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 19-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant;
using HCore.Data.Operations;
using HCore.Data.Operations.Models;

namespace HCore.Operations.Framework
{
    public class FrameworkSystem
    {
        HCOTableState _HCOTableState;
        HCoreContextOperations _HCoreContextOperations;

        /// <summary>
        /// Description: Saves the state of the list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveListState(OListState _Request)
        {
            #region Manage Exception
            try
            {
                #region Operations
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var DataCheck = _HCoreContextOperations.HCOTableState.Where(x => x.AccountId == _Request.AccountId && x.AccountKey == _Request.AccountKey && x.SystemName == _Request.SystemName).FirstOrDefault();
                    if (DataCheck != null)
                    {
                        DataCheck.Data = _Request.Data;
                        _HCoreContextOperations.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC200", "View saved successfully");
                    }
                    else
                    {
                        _HCOTableState = new HCOTableState();
                        _HCOTableState.Guid = HCoreHelper.GenerateGuid();
                        _HCOTableState.AccessTypeId = _Request.AccessTypeId;
                        _HCOTableState.AccountId = _Request.AccountId;
                        _HCOTableState.AccountKey = _Request.AccountKey;
                        _HCOTableState.SystemName = _Request.SystemName;
                        _HCOTableState.Data = _Request.Data;
                        _HCOTableState.CreateDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContextOperations.HCOTableState.Add(_HCOTableState);
                        _HCoreContextOperations.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC200", "View saved successfully");
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveListState", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Deletes the state of the list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteListState(OListState _Request)
        {
            #region Manage Exception
            try
            {
                #region Operations
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    var DataCheck = _HCoreContextOperations.HCOTableState.Where(x => x.AccountId == _Request.AccountId && x.AccountKey == _Request.AccountKey && x.SystemName == _Request.SystemName).FirstOrDefault();
                    if (DataCheck != null)
                    {
                        _HCoreContextOperations.HCOTableState.Remove(DataCheck);
                        _HCoreContextOperations.SaveChanges();
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HC200", "View delete successfully");
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC200", "View not found");
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("SaveListState", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the state of the list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetListState(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContextOperations = new HCoreContextOperations())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        int TotalRecords = (from x in _HCoreContextOperations.HCOTableState
                                            where x.AccountId == _Request.ReferenceId && x.AccountKey == _Request.ReferenceKey && x.SystemName == _Request.SubReferenceKey
                                            select new
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                AccessTypeId = x.AccessTypeId,
                                                SystemName = x.SystemName,
                                                Data = x.Data,
                                            })
                                             .Where(_Request.SearchCondition)
                                                       .Count();
                    }
                    #endregion
                    #region Get Data
                    var Data = (from x in _HCoreContextOperations.HCOTableState
                                where x.AccountId == _Request.ReferenceId && x.AccountKey == _Request.ReferenceKey && x.SystemName == _Request.SubReferenceKey
                                select new
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    AccessTypeId = x.AccessTypeId,
                                    SystemName = x.SystemName,
                                    Data = x.Data,
                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    _HCoreContextOperations.Dispose();
                    #region Create  Response Object
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetWorkHorse", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }


    }
}
