//==================================================================================
// FileName: ManageCoreUserOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Operations.Framework;
using HCore.Operations.Object;

namespace HCore.Operations
{
    public class ManageCoreUserOperations
    {
        FrameworkCoreUser _FrameworkCoreUser;
        /// <summary>
        /// Description: Saves the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveUserAccount(OUserAccount.Request _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.SaveUserAccount(_Request);
        }
        /// <summary>
        /// Description: Saves the user account parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OUserAccount.Request.</returns>
        public OUserAccount.Request SaveUserAccountParameter(OUserAccount.Request _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.SaveUserAccountParameter(_Request);
        }
        /// <summary>
        /// Description: Updates the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserAccount(OUserAccount.Request _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.UpdateUserAccount(_Request);
        }
        /// <summary>
        /// Description: Updates the user password.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserPassword(OUserAccount.Request _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.UpdateUserPassword(_Request);
        }
        /// <summary>
        /// Description: Updates the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserAccessPin(OUserAccount.Request _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.UpdateUserAccessPin(_Request);
        }
        /// <summary>
        /// Description: Resets the user access pin.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ResetUserAccessPin(OUserAccount.Request _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.ResetUserAccessPin(_Request);
        }
        /// <summary>
        /// Description: Saves the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.SaveUserParameter(_Request);
        }
        /// <summary>
        /// Description: Updates the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.UpdateUserParameter(_Request);
        }
        /// <summary>
        /// Description: Updates the user session.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserSession(OUserSession.Manage _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.UpdateUserSession(_Request);
        }
        /// <summary>
        /// Description: Updates the user device.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateUserDevice(OUserDevice.Manage _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.UpdateUserDevice(_Request);
        }
        /// <summary>
        /// Description: Deletes the user parameter.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteUserParameter(OUserParameter.Manage _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.DeleteUserParameter(_Request);
        }
        /// <summary>
        /// Description: Deletes the user account.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteUserAccount(OUserAccount.Request _Request)
        {
            _FrameworkCoreUser = new FrameworkCoreUser();
            return _FrameworkCoreUser.DeleteUserAccount(_Request);
        }

    }
}
