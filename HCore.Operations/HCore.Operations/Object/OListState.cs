//==================================================================================
// FileName: OListState.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Operations.Object
{
    public class OListState
    {
        public long ReferenceId { get; set; }
        public string? ReferenceKey { get; set; }

       public long AccessTypeId { get; set; }
       public long AccountId { get; set; }
        public string? AccountKey { get; set; }
        public string? Data { get; set; }
        public string? SystemName { get; set; }
        public OUserReference? UserReference { get; set; }
    }
}
