//==================================================================================
// FileName: OCoreTransaction.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Operations.Object
{
    public class OCoreTransaction
    {
        public class Initialize
        {
            public class Request
            {
                public long TransactionId { get; set; }
                public string? TransactionKey { get; set; }

                public long UserAccountId { get; set; }

                /// <summary>
                /// Merchant Id
                /// </summary>
                /// <value>The parent identifier.</value>
                public long ParentId { get; set; }
                /// <summary>
                /// Store Id
                /// </summary>
                /// <value>The sub parent identifier.</value>
                public long SubParentId { get; set; }



                public int ModeId { get; set; }
                public int TypeId { get; set; }
                public int SourceId { get; set; }

                public double Amount { get; set; }
                public double Charge { get; set; }
                public double Comission { get; set; }
                public double TotalAmount { get; set; }

                public double InvoiceAmount { get; set; }
                public double ReferenceAmount { get; set; }
                public double ReferenceInvoiceAmount { get; set; }


                public string? Description { get; set; }
                public string? Comment { get; set; }
                //public DateTime? TransactionDate { get; set; }
                //public long UserAccountTypeId { get; set; }


                //Accounts
                public long BankId { get; set; }
                public long CustomerId { get; set; }
                public long CashierId { get; set; }
                public long ProviderId { get; set; }
                public long CampaignId { get; set; }

                //Account Number & Content
                public string? AccountNumber { get; set; }
                public string? ReferenceNumber { get; set; }
                public string? InvoiceNumber { get; set; }
                public string? Data { get; set; }

                // Card & Bin
                //public long CardId { get; set; }
                //public long CardBrandId { get; set; }
                //public long BinNumberId { get; set; }


                /// <summary>
                /// Terminal Or PSSP Or Cashier Id
                /// </summary>
                /// <value>The created by identifier.</value>
                public long CreatedById { get; set; }
                //public long CreatedByAccountTypeId { get; set; }
                public OUserReference? UserReference { get; set; }
            }
            public class Response
            {
                public long Status { get; set; }
                public string? Message { get; set; }
                public long ReferenceId { get; set; }
                public string? ReferenceKey { get; set; }
                public DateTime TransactionDate { get; set; }
            }
        }
        public class Confirm
        {
            public class Request
            {
                public string? ReferenceKey { get; set; }
                public OUserReference? UserReference { get; set; }
                public List<TransactionItem> Transactions { get; set; }
            }
            public class TransactionItem
            {
                public string? TransactionKey { get; set; }
                public long UserAccountTypeId { get; set; }
                public long UserAccountId { get; set; }
                public int ModeId { get; set; }
                public int TypeId { get; set; }
                public int SourceId { get; set; }
                public double Amount { get; set; }
                public double Charge { get; set; }
                public double Comission { get; set; }
                public double TotalAmount { get; set; }
                public string? Description { get; set; }
                public string? Comment { get; set; }
            }
        }
        public class BulkTransaction
        {
            public List<BulkRequest> Items { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class BulkRequest
        {
            public DateTime? TTransactionDate { get; set; }
            internal DateTime TransactionDate { get; set; }
            public string? GroupKey { get; set; }
            public long ParentTransactionId { get; set; }
            public string? ParentTransactionKey { get; set; }
            public long ProgramId { get; set; }
            /// <summary>
            /// Merchant Id
            /// </summary>
            /// <value>The parent identifier.</value>
            public long ParentId { get; set; }

            /// <summary>
            /// Store Id
            /// </summary>
            /// <value>The sub parent identifier.</value>
            public long SubParentId { get; set; }
            //public long CardId { get; set; }
            public long CardBrandId { get; set; }
            public long BankId { get; set; }
            public long CustomerId { get; set; }
            public long CashierId { get; set; }
            public long ProviderId { get; set; }
            public long CreatedByAccountTypeId { get; set; }
            public int BinNumberId { get; set; }
            public string? AccountNumber { get; set; }
            public string? ReferenceNumber { get; set; }
            public double ReferenceAmount { get; set; }
            public double ReferenceInvoiceAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public string? InvoiceNumber { get; set; }
            public string? Data { get; set; }
            public int StatusId { get; set; }
            public int PaymentMethodId { get; set; }
            public long CampaignId { get; set; }
            public long TerminalId { get; set; }
            public TransactionItem FromAccount { get; set; }
            public TransactionItem ToAccount { get; set; }
            public List<TransactionItem> Transactions { get; set; }

            /// <summary>
            /// Terminal Or PSSP Or Cashier Id
            /// </summary>
            /// <value>The created by identifier.</value>
            public long CreatedById
            {
                get; set;

            }


        }
        public class Request
        {
            public DateTime? TTransactionDate { get; set; }
            public DateTime TransactionDate { get; set; }

            public string? GroupKey { get; set; }
            public long ParentTransactionId { get; set; }
            public string? ParentTransactionKey { get; set; }
            public long ProgramId { get; set; }
            /// <summary>
            /// Merchant Id
            /// </summary>
            /// <value>The parent identifier.</value>
            public long ParentId { get; set; }

            /// <summary>
            /// Store Id
            /// </summary>
            /// <value>The sub parent identifier.</value>
            public long SubParentId { get; set; }
            //public long CardId { get; set; }
            public long CardBrandId { get; set; }
            public long BankId { get; set; }
            public long CustomerId { get; set; }
            public long CashierId { get; set; }
            public long ProviderId { get; set; }
            public long CreatedByAccountTypeId { get; set; }
            public int BinNumberId { get; set; }
            public string? AccountNumber { get; set; }
            public string? ReferenceNumber { get; set; }
            public double ReferenceAmount { get; set; }
            public double ReferenceInvoiceAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public string? InvoiceNumber { get; set; }
            public string? Data { get; set; }
            public int StatusId { get; set; }
            public int PaymentMethodId { get; set; }
            public long CampaignId { get; set; }
            public long TerminalId { get; set; }
            public TransactionItem FromAccount { get; set; }
            public TransactionItem ToAccount { get; set; }
            public List<TransactionItem> Transactions { get; set; }

            /// <summary>
            /// Terminal Or PSSP Or Cashier Id
            /// </summary>
            /// <value>The created by identifier.</value>
            public long CreatedById { get; set; }

            public OUserReference? UserReference { get; set; }
            public double? CashierReward { get; set; }
        }
        public class TransactionItem
        {
            public bool IsPrimaryTransaction { get; set; } = false;
            public long TransactionId { get; set; }
            public string TransactionKey { get; set; }
            public int UserAccountTypeId { get; set; }
            public long UserAccountId { get; set; }
            public int ModeId { get; set; }
            public int TypeId { get; set; }
            public int SourceId { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double Comission { get; set; }
            public double TotalAmount { get; set; }
            public string? Description { get; set; }
            public string? Comment { get; set; }
            public double? ReferenceAmount { get; set; }
            public DateTime? TransactionDate { get; set; }
            public int StatusId { get; set; }
            public long RedeemFrom { get; set; }
            public string? TransactionHash { get; set; }
        }
        public class Response
        {
            public long Status { get; set; }
            public string? Message { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TCode { get; set; }
            public string? GroupKey { get; set; }
            public DateTime TransactionDate { get; set; }
        }
    }
    public class OAccountBalance
    {
        public long SourceId { get; set; }
        public long Transactions { get; set; }
        public double Credit { get; set; }
        public double Debit { get; set; }
        public double Balance { get; set; }
        public int BalanceValidity { get; set; } = 0;
        public DateTime LastTransactionDate { get; set; }

        public string? BankName { get; set; }
        public string? AccountNumber { get; set; }
        public string? AccountHolderName { get; set; }
        public string? BankCode { get; set; }
        public string? StoreName { get; set; }
    }
    public class OHCCoreTransaction
    {
        public class Request
        {
            public DateTime? TTransactionDate { get; set; }

            internal DateTime TransactionDate { get; set; }
            public string? GroupKey { get; set; }
            public long ParentTransactionId { get; set; }
            public string? ParentTransactionKey { get; set; }

            /// <summary>
            /// Merchant Id
            /// </summary>
            /// <value>The parent identifier.</value>
            public long ParentId { get; set; }

            /// <summary>
            /// Store Id
            /// </summary>
            /// <value>The sub parent identifier.</value>
            public long SubParentId { get; set; }
            public long CardId { get; set; }
            public long CardBrandId { get; set; }
            public long BankId { get; set; }
            public long CustomerId { get; set; }
            public long CashierId { get; set; }
            public long ProviderId { get; set; }
            public long CreatedByAccountTypeId { get; set; }

            public string? AccountNumber { get; set; }
            public string? ReferenceNumber { get; set; }
            public double ReferenceAmount { get; set; }
            public double ReferenceInvoiceAmount { get; set; }
            public double InvoiceAmount { get; set; }
            public string? InvoiceNumber { get; set; }
            public string? Data { get; set; }
            public int StatusId { get; set; }
            public long CampaignId { get; set; }
            public List<TransactionItem> Transactions { get; set; }

            /// <summary>
            /// Terminal Or PSSP Or Cashier Id
            /// </summary>
            /// <value>The created by identifier.</value>
            public long CreatedById { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class TransactionItem
        {
            internal long TransactionId { get; set; }
            public long UserAccountTypeId { get; set; }
            public long UserAccountId { get; set; }
            public long ModeId { get; set; }
            public long TypeId { get; set; }
            public long SourceId { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double Comission { get; set; }
            public double TotalAmount { get; set; }
            public string? Description { get; set; }
            public string? Comment { get; set; }
            public DateTime? TransactionDate { get; set; }
        }
        public class Response
        {
            public long Status { get; set; }
            public string? Message { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TCode { get; set; }
            public string? GroupKey { get; set; }
            public DateTime TransactionDate { get; set; }
        }
    }

}
