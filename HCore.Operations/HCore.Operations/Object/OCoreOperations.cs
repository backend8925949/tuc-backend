//==================================================================================
// FileName: OCoreOperations.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Operations.Object
{
    public class OSystemConfiguration
    {
        public class Request
        {
            public string? ConfigurationCode { get; set; }
            public long UserAccountId { get; set; }
            public long ParentId { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public string? Value { get; set; }
        }
    }
    public class OConfigurationManager
    {
        public class Request
        {
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Save
        {
            public string? ReferenceKey { get; set; }
            public string? DataTypeCode { get; set; }
            public string? HelperCode { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? PrimaryValue { get; set; }
            public string? SecondaryValue { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DataTypeCode { get; set; }
            public string? DataTypeName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? PrimaryValue { get; set; }
            public string? SecondaryValue { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class SubList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ConfigurationKey { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? PrimaryValue { get; set; }
            public string? SecondaryValue { get; set; }
            public string? Description { get; set; }
            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class AccountType
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ConfigurationKey { get; set; }

            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }

    public class OCoreHelper
    {
        public class Manage
        {
            public bool? AllowDuplicateName { get; set; }
            public bool? AllowDuplicateSystemName { get; set; }
            public bool? AllowDuplicateParentCode { get; set; }
            public string? Reference { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ParentCode { get; set; }
            public string? SubParentCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? TypeName { get; set; }
            public string? Description { get; set; }
            public int Sequence { get; set; }
            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }
            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {

            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }

            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentCode { get; set; }
            public string? SubParentName { get; set; }

            public string? Name { get; set; }
            public string? Value { get; set; }
            public string? TypeName { get; set; }
            public string? Description { get; set; }
            public int? Sequence { get; set; }

            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }


            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OCoreCommon
    {
        public class Manage
        {
            public bool? DisableChilds { get; set; }
            public bool? AllowDuplicateParent { get; set; }
            public bool? AllowDuplicateSubParent { get; set; }
            public bool? AllowDuplicateName { get; set; }
            public bool? AllowDuplicateSystemName { get; set; }
            public bool? AllowDuplicateValue { get; set; }
            public bool? AllowDuplicateSubValue { get; set; }
            public bool? AllowDuplicateHelperCode { get; set; }

            public string? Reference { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? HelperCode { get; set; }

            public string? UserAccountKey { get; set; }
            public string? ParentKey { get; set; }
            public string? SubParentKey { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public long? Sequence { get; set; }
            public long? Count { get; set; }




            public bool DisableChild { get; set; }

            public OStorageContent IconContent { get; set; }

            public OStorageContent PosterContent { get; set; }

            public string? StatusCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }


            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentCode { get; set; }
            public string? SubParentName { get; set; }

            public string? Name { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public long? Sequence { get; set; }
            public long? Count { get; set; }
            public long? SubItemsCount { get; set; }

            public string? IconUrl { get; set; }

            public string? PosterUrl { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OCoreParameter
    {
        public class Manage
        {
            public bool? AllowDuplicateParent { get; set; }
            public bool? AllowDuplicateSubParent { get; set; }
            public bool? AllowDuplicateName { get; set; }
            public bool? AllowDuplicateSystemName { get; set; }
            public bool? AllowDuplicateValue { get; set; }
            public bool? AllowDuplicateSubValue { get; set; }
            public bool? AllowDuplicateCommonKey { get; set; }
            public bool? AllowDuplicateSubCommonKey { get; set; }
            public bool? AllowDuplicateHelperCode { get; set; }

            public string? Reference { get; set; }
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }

            public string? ParentKey { get; set; }
            public string? SubParentKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? TypeCode { get; set; }

            public string? Name { get; set; }

            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public string? HelperCode { get; set; }

            public string? CommonKey { get; set; }
            public string? SubCommonKey { get; set; }

            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }

            public string? StatusCode { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? SystemName { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentCode { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentCode { get; set; }
            public string? SubParentName { get; set; }


            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public long? SubItemsCount { get; set; }

            public string? Name { get; set; }

            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Data { get; set; }
            public string? Description { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? CommonKey { get; set; }
            public string? CommonCode { get; set; }
            public string? CommonName { get; set; }

            public string? SubCommonKey { get; set; }
            public string? SubCommonCode { get; set; }
            public string? SubCommonName { get; set; }


            public long? Sequence { get; set; }
            public long? Count { get; set; }

            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OCoreLog
    {
        public long ReferenceId { get; set; }
        public long? TypeId { get; set; }
        public string? Reference { get; set; }
        public string? ReferenceKey { get; set; }
        public string? TypeCode { get; set; }
        public string? TypeName { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public string? Comment { get; set; }
        public string? UserReferenceContent { get; set; }
        public string? Data { get; set; }

        public DateTime? CreateDate { get; set; }
        public string? CreatedByKey { get; set; }
        public string? CreatedByDisplayName { get; set; }
        public string? CreatedByIconUrl { get; set; }

        public DateTime? ModifyDate { get; set; }
        public string? ModifyByKey { get; set; }
        public string? ModifyByDisplayName { get; set; }
        public string? ModifyByIconUrl { get; set; }

        public int StatusId { get; set; }
        public string? StatusCode { get; set; }
        public string? StatusName { get; set; }

        public OUserReference? UserReference { get; set; }
    }
    public class OCoreUsage
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? AppOwnerKey { get; set; }
            public string? AppOwnerName { get; set; }

            public string? AppKey { get; set; }
            public string? AppName { get; set; }

            public string? ApiKey { get; set; }
            public string? ApiName { get; set; }

            public string? AppVersionKey { get; set; }
            public string? AppVersionName { get; set; }

            public string? FeatureKey { get; set; }
            public string? FeatureName { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }

            public long? SessionId { get; set; }
            public string? SessionKey { get; set; }

            public string? IpAddress { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public string? Request { get; set; }
            public string? Response { get; set; }

            public long? StatusId { get; set; }
            public string? StatusName { get; set; }
            public string? StatusCode { get; set; }
            public DateTime RequestTime { get; set; }
            public DateTime? ResponseTime { get; set; }
            public double? ProcessingTime { get; set; }
            public long? AppId { get; set; }
        }
    }
    public class OStorage
    {
        public class Request
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public string? ReferenceKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? StorageTypeCode { get; set; }
            public string? StorageTypeName { get; set; }

            public string? SourceCode { get; set; }
            public string? SourceName { get; set; }

            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? Name { get; set; }
            public string? Url { get; set; }
            public string? ThumbnailUrl { get; set; }

            public string? Extension { get; set; }
            public string? Tags { get; set; }
            public double? Size { get; set; }
            public DateTime CreateDate { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

    }
    public class OCoreVerificationManager
    {
        public class Request
        {
            public int Type { get; set; }
            public string? RequestToken { get; set; }

            public string? Reference { get; set; }

            public string? CountryIsd { get; set; }

            public string? MobileNumber { get; set; }
            public string? MobileMessage { get; set; }

            public string? EmailAddress { get; set; }
            public string? EmailMessage { get; set; }

            public long CountryId { get; set; }
            public string? CountryKey { get; set; }

            public string? BussinessName { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            internal long ReferenceId { get; set; }
            public long Type { get; set; }
            public string? Reference { get; set; }

            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? CodeStart { get; set; }

            public string? RequestToken { get; set; }
            public string? AccessCode { get; set; }
            public string? Accesskey { get; set; }
        }
        public class RequestVerify
        {
            public string? RequestToken { get; set; }

            public string? AccessKey { get; set; }
            public string? AccessCode { get; set; }

            public OUserReference? UserReference { get; set; }
        }
    }
    public class OCoreVerification
    {
        public class Request
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }
            public string? CountryIsd { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? AccessKey { get; set; }
            public string? AccessCode { get; set; }
            public string? AccessCodeStart { get; set; }
            public string? EmailMessage { get; set; }
            public string? MobileMessage { get; set; }
            public DateTime? ExpiaryDate { get; set; }
            public DateTime? VerifyDate { get; set; }
            public string? RequestIpAddress { get; set; }
            public double? RequestLatitude { get; set; }
            public double? RequestLongitude { get; set; }
            public string? RequestLocation { get; set; }
            public int? VerifyAttemptCount { get; set; }
            public string? VerifyIpAddress { get; set; }
            public double? VerifyLatitude { get; set; }
            public double? VerifyLongitude { get; set; }
            public string? VerifyLocation { get; set; }
            public string? ReferenceSubKey { get; set; }
            public DateTime? CreateDate { get; set; }
            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OUserAccount
    {
        public class Request
        {
            public long? ReferenceId { get; set; }
            public string? Reference { get; set; }
            public string? ReferenceKey { get; set; }

            public string? AccountTypeCode { get; set; }
            public string? AccountOperationTypeCode { get; set; }
            public string? RegistrationSourceCode { get; set; }

            public string? OwnerKey { get; set; }
            public string? SubOwnerKey { get; set; }
            public string? BankKey { get; set; }

            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? SecondaryPassword { get; set; }
            public string? AccessPin { get; set; }

            public string? OwnerName { get; set; }
            public string? DisplayName { get; set; }
            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? SecondaryEmailAddress { get; set; }
            public string? GenderCode { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public string? Description { get; set; }
            public string? WebsiteUrl { get; set; }
            public string? Address { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }

            public int? CountryId { get; set; }
            public long? StateId { get; set; }
            public long? CityId { get; set; }
            public long? CityAreaId { get; set; }

            public string? ReferralCode { get; set; }
            public string? ReferralUrl { get; set; }

            public string? RoleKey { get; set; }
            public string? TimeZoneKey { get; set; }

            public string? AccountLevelCode { get; set; }
            public string? SubscriptionKey { get; set; }

            //public string? AppKey { get; set; }
            //public string? AppVersionKey { get; set; }

            public string? ApplicationStatusCode { get; set; }

            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }


            public string? CountryKey { get; set; }
            public string? RegionKey { get; set; }
            public string? RegionAreaKey { get; set; }
            public string? CityKey { get; set; }
            public string? CityAreaKey { get; set; }
            public string? Comment { get; set; }

            public sbyte EmailVerificationStatus { get; set; }
            public DateTime? EmailVerificationStatusDate { get; set; }

            public sbyte NumberVerificationStatus { get; set; }
            public DateTime? NumberVerificationStatusDate { get; set; }

            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }

            public string? StatusCode { get; set; }
            public OAddress Location { get; set; }

            public List<Owner> Owners { get; set; }
            public List<Configuration> Configurations { get; set; }
            public OUserReference? UserReference { get; set; }

            public string? OldPassword { get; set; }
            public string? OldAccessPin { get; set; }

            //public double Credit { get; set; }
            //public double Debit { get; set; }
            public double AccountPercentage { get; set; }
            public long SourceId { get; set; }
        }
        public class Owner
        {
            public string? OwnerKey { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
        }
        public class Configuration
        {
            public string? TypeCode { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? HelperCode { get; set; }
            public string? CommonKey { get; set; }
            public string? ParameterKey { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }
        }

        public class Details
        {
            internal long? AccountTypeId { get; set; }
            public long? ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? AccountTypeCode { get; set; }
            public string? AccountTypeName { get; set; }
            public string? AccountOperationTypeCode { get; set; }
            public string? AccountOperationTypeName { get; set; }
            internal long? OwnerId { get; set; }
            public string? OwnerKey { get; set; }
            public string? OwnerDisplayName { get; set; }
            public int? OwnerAccountTypeId { get; set; }
            public string? OwnerAccounttypeCode { get; set; }
            public string? OwnerIconUrl { get; set; }
            public string? BankKey { get; set; }
            public string? BankDisplayName { get; set; }

            public string? DisplayName { get; set; }
            public string? AccessPin { get; set; }
            public string? AccountCode { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }
            public string? ReferralCode { get; set; }
            public string? ReferralUrl { get; set; }
            public string? Description { get; set; }
            public string? RegistrationSourceCode { get; set; }
            public string? RegistrationSourceName { get; set; }

            public string? RoleKey { get; set; }
            public string? RoleName { get; set; }

            public string? AppKey { get; set; }
            public string? AppName { get; set; }

            public string? AppVersionKey { get; set; }
            public string? AppVersionName { get; set; }

            public string? ApplicationStatusCode { get; set; }
            public string? ApplicationStatusName { get; set; }

            public DateTime? LastLoginDate { get; set; }

            public string? MerchantKey { get; set; }
            public string? MerchantDisplayName { get; set; }

            public string? RequestKey { get; set; }

            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }
            internal long? CreatedByAccountTypeId { get; set; }
            internal long? CreatedById { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }


            public string? UserName { get; set; }
            public string? Password { get; set; }
            public string? SecondaryPassword { get; set; }
            public string? SystemPassword { get; set; }
            public string? Name { get; set; }
            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? ContactNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? SecondaryEmailAddress { get; set; }
            public string? GenderCode { get; set; }
            public string? GenderName { get; set; }
            public DateTime? DateOfBirth { get; set; }
            public DateTime? LastTransactionDate { get; set; }

            public string? Address { get; set; }
            public OAddress AddressComponent { get; set; }
            //public double? Latitude { get; set; }
            //public double? Longitude { get; set; }
            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }
            //public string? RegionKey { get; set; }
            //public string? RegionName { get; set; }
            //public string? RegionAreaKey { get; set; }
            //public string? RegionAreaName { get; set; }
            //public string? CityKey { get; set; }
            //public string? CityName { get; set; }
            //public string? CityAreaKey { get; set; }
            //public string? CityAreaName { get; set; }
            public string? WebsiteUrl { get; set; }
            public double? SubOwnerLatitude { get; set; }
            public double? SubOwnerLongitude { get; set; }
            public string? SubOwnerKey { get; set; }
            public string? SubOwnerDisplayName { get; set; }
            public string? SubOwnerAddress { get; set; }


            public sbyte? EmailVerificationStatus { get; set; }
            public DateTime? EmailVerificationStatusDate { get; set; }

            public sbyte? NumberVerificationStatus { get; set; }
            public DateTime? NumberVerificationStatusDate { get; set; }
            public long? SubAccounts { get; set; }

            public bool IsWelcomeSmsSent { get; set; }
            public bool IsWelcomeSmsDelivered { get; set; }
            public BnplDetails BnplDetails { get; set; }
        }

        public class BnplDetails
        {
            public DateTime? CreditLimitCheckDate { get; set; }
            public double? CreditLimit { get; set; }
        }
    }
    public class OAppProfile
    {
        public class Request
        {
            /// <summary>
            /// Mobile number is mandatory
            /// </summary>
            /// <value>The mobile number.</value>
            public string? MobileNumber { get; set; }
            public string? FirstName { get; set; }
            public string? MiddleName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? GenderCode { get; set; }
            public long GenderId { get; set; }
            public string ReferralCode { get; set; }
            public string Address { get; set; }
            public double AddressLatitude { get; set; }
            public double AddressLongitude { get; set; }

            public long OwnerId { get; set; }
            public long SubOwnerId { get; set; }
            public long CreatedById { get; set; }

            public string? CardNumber { get; set; }
            public string? CardSerialNumber { get; set; }

            public int CountryId { get; set; }

            public DateTime? DateOfBirth { get; set; }
            public OAddress AddressComponent { get; set; }
            /// <summary>
            /// User reference is mandatory. Pass OUserReference object available after authentication
            /// </summary>
            /// <value>The user reference.</value>
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public HCoreConstant.ResponseStatus Status { get; set; } // users
            public string? StatusResponseCode { get; set; } // used
            public string? StatusMessage { get; set; } // used

            public long AccountId { get; set; } // used
            public string? AccountKey { get; set; } // used
            public string? MobileNumber { get; set; } // used
            public long? OwnerId { get; set; } // used
            public long? SubOwnerId { get; set; } // used
            public DateTime CreateDate { get; set; }
            public int StatusId { get; set; }

            public string? AccountCode { get; set; }
            public int? CountryId { get; set; }


            public long AddressReferenceId { get; set; }
            public string? AddressReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? Gender { get; set; }
            public string? EmailAddress { get; set; }
            //public long OwnerId { get; set; }
            //public string? ReferralCode { get; set; }
            //public long CardId { get; set; }
            //public string? CardNumber { get; set; }
            //public string? CardSerialNumber { get; set; }
            //public OUserReference? UserReference { get; set; }
        }
    }
    public class OAppProfileLite
    {
        public class Request
        {
            /// <summary>
            /// Mobile number with country isd code. Mobile number is mandatory and must be attached with country isd code
            /// </summary>
            /// <value>The mobile number.</value>
            ///
            public string? MobileNumber { get; set; }
            public string? FirstName { get; set; }
            public string? MiddleName { get; set; }
            public string? LastName { get; set; }
            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? EmailAddress { get; set; }
            public string? GenderCode { get; set; }
            //public long GenderId { get; set; }

            public string? Address { get; set; }
            public double AddressLatitude { get; set; }
            public double AddressLongitude { get; set; }

            public long OwnerId { get; set; }
            public long SubOwnerId { get; set; }
            public long CreatedById { get; set; }

            //public string? CardNumber { get; set; }
            //public string? CardSerialNumber { get; set; }

            public int CountryId { get; set; }

            public DateTime? DateOfBirth { get; set; }
            public OAddress AddressComponent { get; set; }
            /// <summary>
            /// User reference is mandatory. Pass OUserReference object available after authentication
            /// </summary>
            /// <value>The user reference.</value>
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public HCoreConstant.ResponseStatus Status { get; set; } // users
            public string? StatusResponseCode { get; set; } // used
            public string? StatusMessage { get; set; } // used

            public long AccountId { get; set; } // used
            public string? AccountKey { get; set; } // used
            public string? MobileNumber { get; set; } // used
            public long? OwnerId { get; set; } // used
            public long? SubOwnerId { get; set; } // used
            public DateTime CreateDate { get; set; }
            public int StatusId { get; set; }

            public string? AccountCode { get; set; }
            public int? CountryId { get; set; }


            public long AddressReferenceId { get; set; }
            public string? AddressReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? DisplayName { get; set; }
            public string? Gender { get; set; }
            public string? EmailAddress { get; set; }
            //public long OwnerId { get; set; }
            //public string? ReferralCode { get; set; }
            //public long CardId { get; set; }
            //public string? CardNumber { get; set; }
            //public string? CardSerialNumber { get; set; }
            //public OUserReference? UserReference { get; set; }
        }
    }
    public class OUserAccountOwner
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }

        }

        public class Details
        {
            public string? ReferenceKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }
            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }


            public string? OwnerAccountKey { get; set; }
            public string? OwnerAccountDisplayName { get; set; }
            public string? OwnerAccountIconUrl { get; set; }
            public string? OwnerAccountTypeCode { get; set; }
            public string? OwnerAccountTypeName { get; set; }

            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
    }
    public class OTransaction
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Item
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }


            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }
            public string? SubParentIconUrl { get; set; }

            public string? ParentTransactionKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountMobileNumber { get; set; }
            public string? UserAccountEmailAddress { get; set; }
            public string? UserAccountOwnerKey { get; set; }
            public string? UserAccountOwnerDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }


            public string? ModeCode { get; set; }
            public string? ModeName { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }
            public string? SourceCode { get; set; }
            public string? SourceName { get; set; }


            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? ComissionAmount { get; set; }
            public double? TotalAmount { get; set; }
            //public double? TotalAmountPercentage { get; set; }
            public double? Balance { get; set; }
            public double? PurchaseAmount { get; set; }
            public double? ReferenceAmount { get; set; }
            public double? ReferenceInvoiceAmount { get; set; }
            public DateTime TransactionDate { get; set; }
            public string? InvoiceNumber { get; set; }
            public string? AccountNumber { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? Comment { get; set; }
            public string? GroupKey { get; set; }
            public string? RequestKey { get; set; }
            //public string? InvoiceKey { get; set; }
            //public string? InvoiceItemKey { get; set; }

            public long? BankId { get; set; }
            public string? BankKey { get; set; }
            public string? BankDisplayName { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }

            public long? CardId { get; set; }
            public string? CardKey { get; set; }
            public string? CardDisplayName { get; set; }


            public long? CardBrandId { get; set; }
            public string? CardBrandKey { get; set; }
            public string? CardBrandDisplayName { get; set; }

            public long? CardSubBrandId { get; set; }
            public string? CardSubBrandKey { get; set; }
            public string? CardSubBrandDisplayName { get; set; }

            public long? CardTypeId { get; set; }
            public string? CardTypeKey { get; set; }
            public string? CardTypeDisplayName { get; set; }

            public long? CardBankId { get; set; }
            public string? CardBankKey { get; set; }
            public string? CardBankDisplayName { get; set; }

            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierDisplayName { get; set; }


            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentDisplayName { get; set; }
            public string? SubParentAddress { get; set; }
            public double? SubParentLatitude { get; set; }
            public double? SubParentLongitude { get; set; }
            public string? SubParentIconUrl { get; set; }

            public string? ParentTransactionKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountMobileNumber { get; set; }
            public string? UserAccountEmailAddress { get; set; }
            public string? UserAccountOwnerKey { get; set; }
            public string? UserAccountOwnerDisplayName { get; set; }
            public string? UserAccountOwnerIconUrl { get; set; }
            public string? UserAccountIconUrl { get; set; }
            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }


            public string? ModeCode { get; set; }
            public string? ModeName { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }
            public string? SourceCode { get; set; }
            public string? SourceName { get; set; }


            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? ComissionAmount { get; set; }
            public double? TotalAmount { get; set; }
            public double? TotalAmountPercentage { get; set; }
            public double? Balance { get; set; }
            public double? PurchaseAmount { get; set; }
            public double? ReferenceAmount { get; set; }
            public double? ReferenceInvoiceAmount { get; set; }
            public DateTime TransactionDate { get; set; }
            public string? InvoiceNumber { get; set; }
            public string? AccountNumber { get; set; }
            public string? ReferenceNumber { get; set; }
            public string? Comment { get; set; }
            public string? GroupKey { get; set; }
            public string? RequestKey { get; set; }
            //public string? InvoiceKey { get; set; }
            //public string? InvoiceItemKey { get; set; }

            public long? BankId { get; set; }
            public string? BankKey { get; set; }
            public string? BankDisplayName { get; set; }
            public string? BankIconUrl { get; set; }

            public long? ProviderId { get; set; }
            public string? ProviderKey { get; set; }
            public string? ProviderDisplayName { get; set; }
            public string? ProviderIconUrl { get; set; }

            public long? CardId { get; set; }
            public string? CardKey { get; set; }
            public string? CardName { get; set; }


            public long? CardBrandId { get; set; }
            public string? CardBrandKey { get; set; }
            public string? CardBrandDisplayName { get; set; }

            public long? CardSubBrandId { get; set; }
            public string? CardSubBrandKey { get; set; }
            public string? CardSubBrandName { get; set; }

            public long? CardTypeId { get; set; }
            public string? CardTypeKey { get; set; }
            public string? CardTypeName { get; set; }

            public long? CardBankId { get; set; }
            public string? CardBankKey { get; set; }
            public string? CardBankName { get; set; }

            public long? CashierId { get; set; }
            public string? CashierKey { get; set; }
            public string? CashierDisplayName { get; set; }


            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public List<OTransaction.Item> Items { get; set; }
            public RequestLog RequestLog { get; set; }
        }

        public class RequestLog
        {
            public string? Request { get; set; }
            public string? Response { get; set; }
        }
    }
    public class OUserActivity
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }

        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }

            public string? UserDeviceKey { get; set; }
            public string? UserDeviceSerialNumber { get; set; }
            public string? FeatureKey { get; set; }
            public string? FeatureName { get; set; }
            public string? ActivityTypeCode { get; set; }
            public string? ActivityTypeName { get; set; }
            public string? ParentReferenceKey { get; set; }
            public string? SubReferenceKey { get; set; }


            public string? EntityName { get; set; }
            public string? FieldName { get; set; }
            public string? OldValue { get; set; }
            public string? NewValue { get; set; }
            public string? Description { get; set; }

            public DateTime? CreateDate { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
    }
    public class OUserDevice
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public string? ReferenceKey { get; set; }
            public string? OsVersionKey { get; set; }
            public string? NotificationUrl { get; set; }
            public string? StatusCode { get; set; }
            public string? AppVersionKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public string? ReferenceKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }

            public string? SerialNumber { get; set; }
            public string? OsCode { get; set; }
            public string? OsName { get; set; }
            public string? OsVersionKey { get; set; }
            public string? OsVersionName { get; set; }
            public string? AppKey { get; set; }
            public string? AppName { get; set; }
            public string? AppVersionKey { get; set; }
            public string? AppVersionName { get; set; }
            public string? BrandKey { get; set; }
            public string? BrandName { get; set; }
            public string? ModelKey { get; set; }
            public string? ModelName { get; set; }
            public string? ResolutionKey { get; set; }
            public string? ResolutionName { get; set; }
            public string? NetworkOperatorKey { get; set; }
            public string? NetworkOperatorName { get; set; }

            public string? NotificationUrl { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OUserSession
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public string? ReferenceKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Details
        {
            public string? ReferenceKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }

            public string? DeviceKey { get; set; }
            public string? DeviceSerialNumber { get; set; }

            public string? AppVersionKey { get; set; }
            public string? AppVersioName { get; set; }
            public string? AppKey { get; set; }
            public string? AppName { get; set; }

            public DateTime LastActivityDate { get; set; }
            public DateTime? LoginDate { get; set; }
            public DateTime? LogoutDate { get; set; }
            public string? IpAddress { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OUserParameter
    {
        public class Manage
        {

            public string? ReferenceKey { get; set; }
            public string? UserKey { get; set; }

            public string? TypeCode { get; set; }
            public string? ParentKey { get; set; }
            public string? SubParentKey { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserDeviceKey { get; set; }
            public string? CommonKey { get; set; }
            public string? ParameterKey { get; set; }
            public string? HelperCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Description { get; set; }
            public string? Data { get; set; }
            public string? VerificationKey { get; set; }
            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }

            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }

            public string? TransactionKey { get; set; }
            public string? StatusCode { get; set; }

            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public string? ReferenceKey { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }

            public string? SubParentKey { get; set; }
            public string? SubParentName { get; set; }


            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserDeviceKey { get; set; }
            public string? UserDeviceSerialNumber { get; set; }


            public string? CommonKey { get; set; }
            public string? CommonName { get; set; }

            public string? ParameterKey { get; set; }
            public string? ParameterName { get; set; }


            public string? HelperCode { get; set; }
            public string? HelperName { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Value { get; set; }
            public string? SubValue { get; set; }
            public string? Description { get; set; }
            public string? Data { get; set; }
            public string? VerificationKey { get; set; }

            public long? CountValue { get; set; }
            public double? AverageValue { get; set; }

            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }
            public DateTime? StartTime { get; set; }
            public DateTime? EndTime { get; set; }

            public string? TransactionKey { get; set; }
            public string? RequestKey { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OUserLocation
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public string? ReferenceKey { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserDeviceKey { get; set; }
            public string? UserDeviceSerialNumber { get; set; }

            public string? Location { get; set; }
            public double? Latitude { get; set; }
            public double? Longitude { get; set; }

            public string? CountryKey { get; set; }
            public string? CountryName { get; set; }

            public string? RegionKey { get; set; }
            public string? RegionName { get; set; }

            public string? RegionAreaKey { get; set; }
            public string? RegionAreaName { get; set; }


            public string? CityKey { get; set; }
            public string? CityName { get; set; }

            public string? CityAreaKey { get; set; }
            public string? CityAreaName { get; set; }

            public DateTime? CreateDate { get; set; }
        }
    }
    public class OUserInvoice
    {
        public class Manage
        {
            public string? Reference { get; set; }
            public OUserReference? UserReference { get; set; }

        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public string? ParentKey { get; set; }
            public string? ParentName { get; set; }

            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountIconUrl { get; set; }

            public string? UserAccountTypeCode { get; set; }
            public string? UserAccountTypeName { get; set; }


            public string? InvoiceNumber { get; set; }
            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? FromName { get; set; }
            public string? FromAddress { get; set; }
            public string? FromContactNumber { get; set; }
            public string? FromEmailAddress { get; set; }
            public string? FromFax { get; set; }
            public string? ToName { get; set; }
            public string? ToAddress { get; set; }
            public string? ToContactNumber { get; set; }
            public string? ToEmailAddress { get; set; }
            public string? ToFax { get; set; }

            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? ComissionAmount { get; set; }
            public double? TotalAmount { get; set; }


            public DateTime? InvoiceDate { get; set; }
            public DateTime? StartDate { get; set; }
            public DateTime? EndDate { get; set; }
            public string? Comment { get; set; }

            public DateTime? CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public string? CreatedByIconUrl { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
            public string? ModifyByIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OBalance
    {
        public class Request
        {
            public long UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? Source { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public DateTime SystemDate { get; set; }
            public long Credit { get; set; }
            public long Debit { get; set; }
            public long Balance { get; set; }
            public long? BalanceValidity { get; set; }
            public double InvoiceAmount { get; set; }
            public string? AccountClass { get; set; }
            public double? AccountClassLimit { get; set; }
            public double? PendingReward { get; set; }
            public long TucPlusCredit { get; set; }
            public long TucPlusDebit { get; set; }
            public long TucPlusBalance { get; set; }
            public long? TucPlusBalanceValidity { get; set; }
            public List<UserMerchantBalance> UserMerchantBalance { get; set; }
        }


        public class BalanceList
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? UserAccountId { get; set; }
            public string? UserAccountKey { get; set; }
            public string? UserAccountDisplayName { get; set; }
            public string? UserAccountMobileNumber { get; set; }

            public long? ParentId { get; set; }
            public string? ParentKey { get; set; }
            public string? ParentDisplayName { get; set; }
            public string? ParentIconUrl { get; set; }

            public long? SourceId { get; set; }
            public string? SourceKey { get; set; }
            public string? SourceName { get; set; }

            public double? Credit { get; set; }
            public double? Debit { get; set; }
            public double? Balance { get; set; }

            public long? Transactions { get; set; }

            public DateTime? LastTransactionDate { get; set; }
            public DateTime? CreateDate { get; set; }
            public DateTime? ModifyDate { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class UserMerchantBalance
        {
            public long ReferenceId { get; set; }
            public string? DisplayName { get; set; }
            public string? IconUrl { get; set; }
            public double Credit { get; set; }
            public double Debit { get; set; }
            public double Balance { get; set; }
            public double InvoiceAmount { get; set; }
        }
    }

    public class AppVersionCheck
    {
        public class Request
        {
            public string? AppKey { get; set; }
            public string? AppVersionKey { get; set; }
            public string? UserAccountKey { get; set; }
            public OUserReference? UserReference { get; set; }
        }
    }


    public class ODevice
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? Type { get; set; }
            public string? ReferenceKey { get; set; }
            public string? MobileNumber { get; set; }
            public string? SerialNumber { get; set; }
            public string? OsName { get; set; }
            public string? OsVersion { get; set; }
            public string? Brand { get; set; }
            public string? Model { get; set; }
            public int Width { get; set; }
            public int Height { get; set; }
            public string? CarrierName { get; set; }
            public string? CountryCode { get; set; }
            public string? Mcc { get; set; }
            public string? Mnc { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string? NotificationUrl { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Response
        {
            public class DeviceConnect
            {
                public bool IsAccountExists { get; set; }
                public string? FirstName { get; set; }
                public string? MiddleName { get; set; }
                public string? LastName { get; set; }
                public string? EmailAddress { get; set; }
                public string? GenderCode { get; set; }
                public long? CityId { get; set; }
                public string? CityKey { get; set; }
                public string? CityName { get; set; }
                public long? StateId { get; set; }
                public long? CityAreaId { get; set; }
                public string? StateKey { get; set; }
                public string? StateName { get; set; }
                public string? AddressLine1 { get; set; }
                public DateTime? DateOfBirth { get; set; }
            }
        }
    }

}
