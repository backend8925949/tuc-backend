﻿using System;
namespace HCore.Operations.Object
{
    public class OLoyaltyProgram
    {
        public long ProgramId { get; set; }
        public string? Name { get; set; }
        public double Credit { get; set; }
        public double Debit { get; set; }
        public double Balance { get; set; }
        public string IconUrl { get; set; }
    }
}

