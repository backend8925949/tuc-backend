//==================================================================================
// FileName: Resources.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Operations.Resources
{
    public class Resources
    {
        public class ResourceItems
        {
            public const string HCC100 = "Invalid status code";
            public const string HCC101 = "Configuration name required";
            public const string HCC102 = "Configuration system name required";
            public const string HCC103 = "Configuration description required";
            public const string HCC104 = "Configuration value required";
            public const string HCC105 = "Configuration data type required";
            public const string HCC106 = "Configuration already exists";
            public const string HCC107 = "Configuration added successfully";
            public const string HCC108 = "Configuration key missing";
            public const string HCC109 = "Configuration details not found";
            public const string HCC110 = "Configuration details  updated successfully";
            public const string HCC111 = "Configuration deleted successfully";
            public const string HCC112 = "Configuration value updated successfully";
            public const string HCC113 = "Configuration loaded";
            public const string HCC114 = "Account type required";
            public const string HCC115 = "Account type added to configuration";
            public const string HCC116 = "Account type removed from configuration";
            public const string HCC117 = "Account type not found for configuration";
            public const string HC1000 = "Record deleted successfully ";
            public const string HC1001 = "Details not found. It might be removed or not available at the moment ";
            public const string HC1002 = "Reference required";
            public const string HC1003 = "Operation failed. Record in use. Please delete related records and try again";
            public const string HC007 = "Invalid input";
        }
    }
}
