//==================================================================================
// FileName: ManageProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Product.Framework;
using HCore.Product.Object;
namespace HCore.Product
{
    public class ManageProduct
    {
        FrameworkProduct _FrameworkProduct;
        /// <summary>
        /// Description: Saves the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.SaveProduct(_Request);
        }
        /// <summary>
        /// Description: Updates the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.UpdateProduct(_Request);
        }
        //public OResponse UpdateProductStock(OProduct.Request _Request)
        //{
        //    _FrameworkProduct = new FrameworkProduct();
        //    return _FrameworkProduct.UpdateProductStock(_Request);
        //}
        /// <summary>
        /// Description: Deletes the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.DeleteProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProduct(OProduct.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProduct(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProduct(_Request);
        }
        /// <summary>
        /// Description: Gets the product overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductOverview(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProductOverview(_Request);
        }


        /// <summary>
        /// Description: Gets the product lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductLite(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProductLite(_Request);
        }

        /// <summary>
        /// Description: Gets the store product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreProduct(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetStoreProduct(_Request);
        }

        /// <summary>
        /// Description: Gets the product dealer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetProductDealer(OList.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetProductDealer(_Request);
        }

        /// <summary>
        /// Description: Gets the store product varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetStoreProductVarient(OProductVarient.Request _Request)
        {
            _FrameworkProduct = new FrameworkProduct();
            return _FrameworkProduct.GetStoreProductVarient(_Request);
        }
    }
}
