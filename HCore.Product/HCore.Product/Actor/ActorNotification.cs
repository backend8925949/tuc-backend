//==================================================================================
// FileName: ActorNotification.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using HCore.Data;
using HCore.Data.Models;
using Newtonsoft.Json;
using static HCore.Helper.HCoreConstant;
using HCore.Helper.DataStore;
using HCore.Product.Object;
using HCore.Helper;

namespace HCore.Product.Actor
{

    public class ActorOrderNotification : ReceiveActor
    {
        public ActorOrderNotification()
        {
            Receive<OOrderManager.Request>(_Request =>
            {

                //if (HostEnvironment == HostEnvironmentType.Live)
                //{
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var OrderDetails = _HCoreContext.SCOrder.Where(x => x.Id == _Request.OrderId).FirstOrDefault();
                    string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == OrderDetails.CustomerId && x.Id == _Request.UserReference.AccountSessionId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                    string NinjaNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == OrderDetails.RiderId && x.Id == _Request.UserReference.AccountSessionId && x.StatusId == 2).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.PendingConfirmation)
                    {
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.Confirmed)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                            HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "Your have new order", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.Preparing)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                        HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to preparing", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.Ready)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                        HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to ready", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.ReadyToPickup)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                        HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to ready to pickup", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.OutForDelivery)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                        HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order received by ambassador and its out for delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.DeliveryFailed)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                        HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Ambassador unable to deliver order to customer", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.CancelledByUser)
                    {
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.CancelledBySeller)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                        HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order cancelled from your seller account", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.CancelledBySystem)
                    {

                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.Delivered)
                    {
                        #region Merchant
                        string MerchantTopic = "tucmerchant_" + OrderDetails.DealerId;
                        if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                        {
                            MerchantTopic = "tucmerchant_test_" + OrderDetails.DealerId;
                        }
                        HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order delivered to customer", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        #endregion
                    }





                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.Confirmed)
                    {
                        //if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                        //{
                        //    HCoreHelper.SendPushToTopicMerchant("tucmerchant_" + OrderDetails.DealerId, "order", "Your have new order", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        //}
                        //else
                        //{
                        //    HCoreHelper.SendPushToTopicMerchant("tucmerchant_test_" + OrderDetails.DealerId, "order", "Your have new order", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        //}
                    }
                    if (OrderDetails.StatusId == HelperStatus.OrderStatus.PendingConfirmation)
                    {
                        if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                        {
                            HCoreHelper.SendPushToTopic("ninjaorders", "ninjaneworder", "New order available", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        else
                        {
                            HCoreHelper.SendPushToTopic("ninjaorders_test", "ninjaneworder", "New order available", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        //if (!string.IsNullOrEmpty(UserNotificationUrl))
                        //{
                        //    HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order confirmed", "#" + OrderDetails.Id + "Order worth N" + OrderDetails.TotalAmount + " confirmed", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        //}
                    }
                    if (!string.IsNullOrEmpty(UserNotificationUrl))
                    {
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.PendingConfirmation)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "New order placed", "Once order confirmed by ambassador and seller delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Confirmed)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Order confirmed by seller", "Seller has confirmed your order " + "#" + OrderDetails.Id + ". Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Preparing)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order inprocess", "Dealer preparing your order " + "#" + OrderDetails.Id + ". Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Ready)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " is ready", "Dealer prepared your order. Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.ReadyToPickup)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " is ready", "Your order is ready to pickup by agent. Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.OutForDelivery)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " is out for delivery", "Your order is out for delivery. Order delivery code is " + OrderDetails.OrderDeliveryCode + ". Share delivery code with person after delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Delivered)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " deliverd.", "Thank U for buying with us.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.DeliveryFailed)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " delivery failed.", "We are not able to deliver your order.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.CancelledByUser)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " cancelled.", "You have cancelled your order.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.CancelledBySeller)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " cancelled.", "Seller have cancelled your order. Your order amount will be redunded to your account within 24 hours", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.CancelledBySystem)
                        {
                            HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " cancelled.", "System have cancelled your order. Your order amount will be redunded to your account within 24 hours", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                    }

                    if (!string.IsNullOrEmpty(NinjaNotificationUrl))
                    {
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Confirmed)
                        {
                            HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order confirmed by dealer", "Dealer has confirmed  order #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Preparing)
                        {
                            HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "Dealer preparing order #" + OrderDetails.Id, "Dealer started preparing order #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Ready)
                        {
                            HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order ready to pickup.", "Order ready to pickup #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.ReadyToPickup)
                        {
                            HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order ready to pickup.", "Order ready to pickup #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.OutForDelivery)
                        {
                            HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "Your order #" + OrderDetails.Id + " is out for delivery", "Get delivery code from customer for closing delivery.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.Delivered)
                        {
                            HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order delivered.", "Order #" + OrderDetails.Id + " delivered. Thank U for delivering customer order.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                        if (OrderDetails.StatusId == HelperStatus.OrderStatus.DeliveryFailed)
                        {
                            HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order delivery failed.", "Order #" + OrderDetails.Id + " delivered. Order delivery failed.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                        }
                    }
                }
                //}
            });
        }
    }



}
