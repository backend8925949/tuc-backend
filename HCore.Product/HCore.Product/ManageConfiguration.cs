//==================================================================================
// FileName: ManageConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.Product.Framework;
using HCore.Product.Object;

namespace HCore.Product
{
    public class ManageConfiguration
    {
        FrameworkConfiguration _FrameworkConfiguration;
        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfiguration(OTUCConfiguration.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.SaveConfiguration(_Request);
        }
        /// <summary>
        /// Description: Updates the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateConfiguration(OTUCConfiguration.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.UpdateConfiguration(_Request);
        }
        /// <summary>
        /// Description: Saves the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveConfigurationValue(OTUCConfigurationValue.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.SaveConfigurationValue(_Request);
        }
        /// <summary>
        /// Description: Saves the account configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveAccountConfiguration(OTUCConfigurationValue.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.SaveAccountConfiguration(_Request);
        }

        /// <summary>
        /// Description: Gets the account configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetAccountConfiguration(OList.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetAccountConfiguration(_Request);
        }

        /// <summary>
        /// Description: Gets the dealer configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetDealerConfiguration(OList.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetDealerConfiguration(_Request);
        }
        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfiguration(OList.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfiguration(_Request);
        }

        /// <summary>
        /// Description: Gets the configuration history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetConfigurationHistory(OList.Request _Request)
        {
            _FrameworkConfiguration = new FrameworkConfiguration();
            return _FrameworkConfiguration.GetConfigurationHistory(_Request);
        }
    }
}
