//==================================================================================
// FileName: ManageNinja.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Product.Framework;
using HCore.Product.Object;

namespace HCore.Product
{
    public class ManageNinja
    {
        FrameworkNinja _FrameworkNinja;
        /// <summary>
        /// Description: Updates the ninja reg bank details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateNinjaRegBankDetails(ONinja.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.UpdateNinjaRegBankDetails(_Request);
        }

        /// <summary>
        /// Description: Gets the ninja registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNinjaRegistration(ONinja.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.GetNinjaRegistration(_Request);
        }
        /// <summary>
        /// Description: Saves the ninja registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveNinjaRegistration(ONinja.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.SaveNinjaRegistration(_Request);
        }
        /// <summary>
        /// Description: Updates the ninja registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateNinjaRegistration(ONinja.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.UpdateNinjaRegistration(_Request);
        }
        /// <summary>
        /// Description: Updates the ninja registration profile.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateNinjaRegistrationProfile(ONinja.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.UpdateNinjaRegistrationProfile(_Request);
        }
        /// <summary>
        /// Description: Gets the registrations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetRegistrations(OList.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.GetRegistrations(_Request);
        }
        /// <summary>
        /// Description: Gets the ninja list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNinjaList(OList.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.GetNinjaList(_Request);
        }
        /// <summary>
        /// Description: Gets the ninja transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNinjaTransaction(OList.Request _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.GetNinjaTransaction(_Request);
        }
        /// <summary>
        /// Description: Gets the ninja balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNinjaBalance(ONinja.ONinjaTransferRequest _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.GetNinjaBalance(_Request);
        }

        /// <summary>
        /// Description: Transfers the ninja amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse TransferNinjaAmount(ONinja.ONinjaTransferRequest _Request)
        {
            _FrameworkNinja = new FrameworkNinja();
            return _FrameworkNinja.TransferNinjaAmount(_Request);
        }
    }
}
