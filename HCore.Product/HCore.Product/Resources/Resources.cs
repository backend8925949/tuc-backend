//==================================================================================
// FileName: Resources.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
namespace HCore.Product.Resources
{
    internal class Resources
    {
      


        internal class ResourceItems
        {
            internal const string HC500 = "HC500";
            internal const string HC500M = "Operation failed. Server error occured. Please try after some time";

            internal const string HC100 = "HCO100";
            internal const string HC100M = "Reference id required";
            internal const string HC101 = "HCO101";
            internal const string HC101M = "Reference key required";
            internal const string HC102 = "HC102";
            internal const string HC102M = "Status code required";
            internal const string HC103 = "HC103";
            internal const string HC103M = "Invalid status code";


            internal const string HC404 = "HC404";
            internal const string HC404M = "Details not found. It might be removed or temporarily not available at the moment";
            internal const string HC200 = "HCP200";
            internal const string HC200M = "Details loaded";
            internal const string HC201 = "HC201";
            internal const string HC201M = "Details deleted successfully";

            internal const string HCPREFID = "Reference missing";
            internal const string HCPREFKEY = "Reference key missing";
            internal const string HCP200 = "HCP200";
            internal const string HCP200M = "Details loaded";
            internal const string HCP404 = "HCP404";
            internal const string HCP404M = "Details not found. It might be removed or temporarily not available at the moment";


            internal const string HCP100 = "Merchant code required";
            internal const string HCP101 = "Merchant details not found";
            internal const string HCP102 = "Product udpate successful";
            internal const string HCP500 = "Error occured while processing request. Try after some time";
            internal const string HCC104 = "Configuration value required";
            internal const string HCC105 = "Configuration data type required";
            internal const string HCC106 = "Configuration already exists";
            internal const string HCC107 = "Configuration added successfully";
            internal const string HCC108 = "Configuration key missing";
            internal const string HCC109 = "Configuration details not found";
            internal const string HCC110 = "Configuration details  updated successfully";
            internal const string HCC111 = "Configuration deleted successfully";
            internal const string HCC112 = "Configuration value updated successfully";
            internal const string HCC113 = "Configuration loaded";
            internal const string HCC114 = "Account type required";
            internal const string HCC115 = "Account type added to configuration";
            internal const string HCC116 = "Account type removed from configuration";
            internal const string HCC117 = "Account type not found for configuration";


            internal const string HCC118 = "Account key missing";
            internal const string HCC119 = "Account id missing";
            internal const string HCC120 = "Product name required";
            internal const string HCC121 = "Enter product minimum quantity";
            internal const string HCC122 = "Enter product maximum quantity";
            internal const string HCC123 = "Enter valid product minimum and maximum quantity";
            internal const string HCC124 = "Enter product actual price";
            internal const string HCC125 = "Enter product selling price";
            internal const string HCC126 = "Brand details not found";
            internal const string HCC127 = "Account details not found. Account may be inactive or suspended. Please contact support";
            internal const string HCC128 = "Operation failed unable to save product details. Please try after some time";
            internal const string HCC129 = "Operation failed. Product with the same name already exists.";
            internal const string HCC130 = "Varients exists for the product. Operation failed";
            internal const string HCC131 = "Product reference missing";
            internal const string HCC132 = "Product id missing";
            internal const string HCC133 = "Product details updated successfully";
            internal const string HCC134 = "Product details not found. It might be removed or temporarily not available at the moment";
            internal const string HCC135 = "Enter valid stock quantity";
            internal const string HCC136 = "Product deleted successfully";
            internal const string HCC137 = "Short description required";

            internal const string HCC138 = "Brand details missing";
            internal const string HCC139 = "Category details missing";
            internal const string HCC140 = "Subcategory details missing";




            internal const string HCC141 = "HCC140";
            internal const string HCC141M = "Name required";

            internal const string HCC142 = "HCC142";
            internal const string HCC142M = "Description required";

           

            internal const string HCC145 = "HCC145";
            internal const string HCC145M = "Details saved successfully";

            internal const string HCC146 = "HCC146";
            internal const string HCC146M = "Details already exists";

            internal const string HCC147 = "HCC147";
            internal const string HCC147M = "Category name already present";

            internal const string HCC148 = "HCC148";
            internal const string HCC148M = "Details updated successfully";

            internal const string HCC149 = "HCC149";
            internal const string HCC149M = "Operation failed, item in use";


            internal const string HCC150 = "HCC150";
            internal const string HCC150M = "Category id missing";

            internal const string HCC151 = "HCC151";
            internal const string HCC151M = "Category key missing";

            internal const string HCC152 = "HCC152";
            internal const string HCC152M = "Category details not found";

            internal const string HCC153 = "HCC153";
            internal const string HCC153M = "Dealer key missing";

            internal const string HCC154 = "HCC154";
            internal const string HCC154M = "Dealer details not found";


            internal const string HCC155 = "HCC155";
            internal const string HCC155M = "Operation failed, brand details in use";

            internal const string HCC156 = "HCC156";
            internal const string HCC156M = "Order status updated";


            internal const string HCC157 = "HCC157";
            internal const string HCC157M = "Order status updated";

            internal const string HCC158 = "HCC158";
            internal const string HCC158M = "Title required";

            internal const string HCC159 = "HCC159";
            internal const string HCC159M = "Value required";

            internal const string HCC160 = "HCC160";
            internal const string HCC160M = "Varient key missing";

            internal const string HCC161 = "HCC161";
            internal const string HCC161M = "Varient reference missing";

            internal const string HCC162 = "HCC162";
            internal const string HCC162M = "Varient details not found. It  might be removed or not available at the moment";

            internal const string HCC163 = "HCC163";
            internal const string HCC163M = "Specification title already exists";

            internal const string HCC164 = "HCC164";
            internal const string HCC164M = "Details updated successfully";

            internal const string HCC165 = "HCC165";
            internal const string HCC165M = "Image content missing";

            internal const string HCC166 = "HCC166";
            internal const string HCC166M = "Unable to save image";

            internal const string HCC167 = "HCC167";
            internal const string HCC167M = "Dealer location key missing";

            internal const string HCC168 = "HCC168";
            internal const string HCC168M = "Dealer location reference missing";

            internal const string HCC169 = "HCC169";
            internal const string HCC169M = "Stock details already present";

            internal const string HCC170 = "HCC170";
            internal const string HCC170M = "Stock items present for the varient. Please remove them and try again";



            internal const string HCC171 = "HCC171";
            internal const string HCC171M = "Product reference missing";
            internal const string HCC172 = "HCC172";
            internal const string HCC172M = "Product key missing";
            internal const string HCC173 = "HCC173";
            internal const string HCC173M = "Sku required";
            internal const string HCC174 = "HCC174";
            internal const string HCC174M = "Name required";
            internal const string HCC175 = "HCC175";
            internal const string HCC175M = "Short description required";
            internal const string HCC176 = "HCC176";
            internal const string HCC176M = "Description required";
            internal const string HCC177 = "HCC177";
            internal const string HCC177M = "Actual price must be greater than 0";
            internal const string HCC178 = "HCC178";
            internal const string HCC178M = "Selling price must be greater than 0";
            internal const string HCC179 = "HCC179";
            internal const string HCC179M = "Reward percentage must be greater than 0";
            internal const string HCC180 = "HCC180";
            internal const string HCC180M = "Maximum reward amount must be greater than 0";
            internal const string HCC181 = "HCC181";
            internal const string HCC181M = "Product deatils not found";
            internal const string HCC182 = "HCC182";
            internal const string HCC182M = "Product name already exists";
            internal const string HCC183 = "HCC183";
            internal const string HCC183M = "Category details not found";
            internal const string HCC184 = "HCC184";
            internal const string HCC184M = "Secondary category details not found";
            internal const string HCC185 = "HCC185";
            internal const string HCC185M = "Configuration name required";
            internal const string HCC186 = "HCC186";
            internal const string HCC186M = "Configuration system name required";
            internal const string HCC187 = "HCC187";
            internal const string HCC187M = "Configuration value required";
            internal const string HCC188 = "HCC188";
            internal const string HCC188M = "Status code required";

            internal const string HCC189 = "HCC189";
            internal const string HCC189M = "Configuration details already exists";
            internal const string HCC190 = "HCC190";
            internal const string HCC190M = "Configuration description required";
            internal const string HCC191 = "HCC191";
            internal const string HCC191M = "Invalid data type code";

            internal const string HCC192 = "HCC192";
            internal const string HCC192M = "Configuration value missing";

            internal const string HCC193 = "HCC193";
            internal const string HCC193M = "Configuration key missing";

            internal const string HCC194 = "HCC194";
            internal const string HCC194M = "Comment missing";

            internal const string HCC195 = "HCC195";
            internal const string HCC195M = "Configuration id missing";
            internal const string HCC196 = "HCC196";
            internal const string HCC196M = "Configuration details not found";
            internal const string HCC197 = "HCC197";
            internal const string HCC197M = "Configuration name already exists";

            internal const string HCC198 = "HCC198";
            internal const string HCC198M = "Configuration code already present";

            internal const string HCC199 = "HCC199";
            internal const string HCC199M = "Configuration details updated";

            internal const string HCC200 = "HCC200";
            internal const string HCC200M = "No products selected for order. Please select products";

            internal const string HCC201 = "HCC201";
            internal const string HCC201M = "Unable to verify your payement. Please contact support";

            internal const string HCC202 = "HCC202";
            internal const string HCC202M = "Your account is not active. Please contact support to activate account.";
            internal const string HCC203 = "HCC203";
            internal const string HCC203M = "Thank U for rating our service.";
            internal const string HCC204 = "HCC204";
            internal const string HCC204M = "Order cannot be cancelled. Order processing already started";
            internal const string HCC205 = "HCC205";
            internal const string HCC205M = "Enter reason for order cancellation";
            internal const string HCC206 = "HCC205";
            internal const string HCC206M = "Order cancelled successfully. Your amount will be refunded to your bank account with 24 hours";
            internal const string HCC207 = "HCC207";
            internal const string HCC207M = "Order cancelled successfully";
            internal const string HCC209 = "HCC209";
            internal const string HCC209M = "Your account is suspended or blocked. Please contact support"; 
            internal const string HCC208 = "HCC208";
            internal const string HCC208M = "Amount must be greater than 0";
            internal const string HCC210 = "HCC210";
            internal const string HCC210M = "Your account do not have enough balance to transfer";
            internal const string HCC211 = "HCC211";
            internal const string HCC211M = "Your order placed successfully.";
            internal const string HCC212 = "HCC212";
            internal const string HCC212M = " order confirmed for delivery";

            internal const string HCC213 = "HCC213";
            internal const string HCC213M = " order status updated to preparing";
            internal const string HCC214 = "HCC214";
            internal const string HCC214M = " order status updated to ready";

            internal const string HCC215 = "HCC215";
            internal const string HCC215M = " order status updated to ready to pickup";

            internal const string HCC216 = "HCC216";
            internal const string HCC216M = " order out for delivery";

            internal const string HCC217 = "HCC217";
            internal const string HCC217M = " order delivery failed , status updated";

            internal const string HCC218 = "HCC218";
            internal const string HCC218M = " order cancelled successfully";

            internal const string HCC219 = "HCC219";
            internal const string HCC219M = " order cancelled successfully";

            internal const string HCC220 = "HCC220";
            internal const string HCC220M = " order cancelled successfully";

            internal const string HCC221 = "HCC221";
            internal const string HCC221M = " order delivered successfully";

            internal const string HCC222 = "HCC222";
            internal const string HCC222M = "Ambassador information missing";

            internal const string HCC223 = "HCC223";
            internal const string HCC223M = "Order status already updated. Please refresh order and try again";
            internal const string HCC224 = "HCC224";
            internal const string HCC224M = "Invalid Operation. Please refresh order and try again";

            //internal const string HCC194 = "HCC194";
            //internal const string HCC194M = "xxxxxxxxxx";
            internal const string HCP600 = "HCP600";
            internal const string HCP600M = "Order reference missing";

            internal const string HCP601 = "HCP601";
            internal const string HCP601M = "Order account reference missing";

            internal const string HCP602 = "HCP602";
            internal const string HCP602M = "Order cancelled";


            internal const string HCP603 = "HCP603";
            internal const string HCP603M = "Order delivery code required";

            internal const string HCP604 = "HCP604";
            internal const string HCP604M = "Invalid delivery code";
            internal const string HCP605 = "HCP605";
            internal const string HCP605M = "Comment required";

            internal const string HCP606 = "HCP606";
            internal const string HCP606M = "Other ambassador already grabed delivery. Please try other orders";
        }
    }
}
