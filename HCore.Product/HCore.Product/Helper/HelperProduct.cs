//==================================================================================
// FileName: HelperProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using RestSharp;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Operations.Object;
using HCore.Operations;
using Akka.Actor;
using HCore.Product.Actor;
using HCore.Integration.Paystack;

namespace HCore.Product.Helper
{

    internal static class HelperProduct
    {
        public static string GetConfiguration(string ConfigurationCode, long UserAccountId, OUserReference UserReference)
        {
            #region Manage Exception
            try
            {
                #region Perform Operations
                using (HCoreContext _HCoreContext = new HCoreContext())
                {
                    var _Configuration = (from n in _HCoreContext.SCConfiguration
                                          where n.Guid == ConfigurationCode && n.AccountId == 1 && n.StatusId == HCoreConstant.HelperStatus.Default.Active
                                          select new
                                          {
                                              ReferenceId = n.Id,
                                              Value = n.Value,
                                              DataTypeId = n.DataTypeId,
                                              DataTypeCode = n.DataType.SystemName,
                                          }).FirstOrDefault();
                    if (_Configuration != null)
                    {
                        string ConfigValue = "0";
                        ConfigValue = _Configuration.Value;
                        if (UserAccountId != 0)
                        {
                            string CountryConfigurationValue = (from n in _HCoreContext.SCConfiguration
                                                                where n.ParentId == _Configuration.ReferenceId && n.AccountId == UserAccountId && n.StatusId == HelperStatus.Default.Active
                                                                select n.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(CountryConfigurationValue))
                            {
                                ConfigValue = CountryConfigurationValue;
                            }
                        }
                        _HCoreContext.Dispose();
                        #region Send Response
                        if (!string.IsNullOrEmpty(_Configuration.Value))
                        {
                            return ConfigValue;
                        }
                        else
                        {
                            return "0";
                        }
                        #endregion
                    }
                    else
                    {
                        _HCoreContext.Dispose();
                        #region Send Response
                        return "0";
                        #endregion
                    }

                }
                #endregion
            }
            catch (Exception _Exception)
            {
                HCoreHelper.LogException("GetConfiguration", _Exception, UserReference);
                return "0";
            }
            #endregion
        }
    }

    internal class HelperProductConstant
    {
        internal class PaymentStatus
        {
            internal const int Pending = 483;
            internal const int Paid = 484;
        }

        internal class DeliveryPriority
        {
            internal const int Low = 508;
            internal const int Medium = 509;
            internal const int High = 510;
        }

        internal class DeliveryMode
        {
            internal const int Address = 583;
            internal const string AddressS = "deliverymode.address";
            internal const int PickUp = 584;
            internal const string PickUpS = "deliverymode.pickup";
        }
    }
}
