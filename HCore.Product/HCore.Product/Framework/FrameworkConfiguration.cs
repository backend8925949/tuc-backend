//==================================================================================
// FileName: FrameworkConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to configuration
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;
namespace HCore.Product.Framework
{
    internal class FrameworkConfiguration
    {
        SCConfiguration _SCConfiguration;
        HCoreContext _HCoreContext;
        /// <summary>
        /// Description: Saves the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfiguration(OTUCConfiguration.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC185, ResourceItems.HCC185M);
                }
                if (string.IsNullOrEmpty(_Request.SystemName))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC186, ResourceItems.HCC186M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC190, ResourceItems.HCC190M);
                }
                if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC187, ResourceItems.HCC187M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC188, ResourceItems.HCC188M);
                }
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                }
                int? DataTypeId = HCoreHelper.GetSystemHelperId(_Request.DataTypeCode, _Request.UserReference);
                if (DataTypeId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC191, ResourceItems.HCC191M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool ProductCategoryDetails = _HCoreContext.SCConfiguration
                                                        .Any(x =>
                                                        x.Guid == _Request.SystemName);
                    if (!ProductCategoryDetails)
                    {
                        _SCConfiguration = new SCConfiguration();
                        _SCConfiguration.Guid = HCoreHelper.GenerateGuid();
                        _SCConfiguration.Name = _Request.Name;
                        _SCConfiguration.Guid = _Request.SystemName;
                        _SCConfiguration.DataTypeId = (int)DataTypeId;
                        _SCConfiguration.Value = _Request.Value;
                        _SCConfiguration.AccountId = 1;
                        _SCConfiguration.Description = _Request.Description;
                        _SCConfiguration.CreateDate = HCoreHelper.GetGMTDateTime();
                        _SCConfiguration.CreatedById = _Request.UserReference.AccountId;
                        _SCConfiguration.StatusId = (int)StatusId;
                        _HCoreContext.SCConfiguration.Add(_SCConfiguration);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _SCConfiguration.Id,
                            ReferenceKey = _SCConfiguration.Guid,
                        };
                        long? IconStorageId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.SCConfiguration.Where(x => x.Id == _SCConfiguration.Id).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.ImageStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC145, ResourceItems.HCC145M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC189, ResourceItems.HCC189M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC500, ResourceItems.HC500M);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateConfiguration(OTUCConfiguration.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                        }
                    }

                    int DataTypeId = 0;
                    if (!string.IsNullOrEmpty(_Request.DataTypeCode))
                    {
                        DataTypeId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.DataTypeCode).Select(x => x.Id).FirstOrDefault();
                        if (DataTypeId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC191, ResourceItems.HCC191M);
                        }
                    }
                    var ProductCategoryDetails = _HCoreContext.SCConfiguration
                                                                 .Where(x =>
                                                                 x.ParentId == null &&
                                                        x.AccountId == 1 &&
                                                        x.Id == _Request.ReferenceId
                                                                 && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductCategoryDetails != null)
                    {

                        if (!string.IsNullOrEmpty(_Request.Name) && ProductCategoryDetails.Name != _Request.Name)
                        {
                            bool NameCheck = _HCoreContext.SCConfiguration
                                                        .Any(x =>
                                                        x.Name == _Request.Name &&
                                                        x.ParentId == null &&
                                                        x.AccountId == 1);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC197, ResourceItems.HCC197M);
                            }
                            ProductCategoryDetails.Name = _Request.Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.SystemName) && ProductCategoryDetails.Guid != _Request.SystemName)
                        {
                            bool NameCheck = _HCoreContext.SCConfiguration
                                                        .Any(x =>
                                                        x.Guid == _Request.SystemName &&
                                                        x.ParentId == null &&
                                                        x.AccountId == 1);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC198, ResourceItems.HCC198M);
                            }
                            ProductCategoryDetails.Guid = _Request.SystemName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && ProductCategoryDetails.Description != _Request.Description)
                        {
                            ProductCategoryDetails.Description = _Request.Description;
                        }
                        if (DataTypeId != 0 && ProductCategoryDetails.DataTypeId != DataTypeId)
                        {
                            ProductCategoryDetails.DataTypeId = DataTypeId;
                        }
                        ProductCategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ProductCategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && ProductCategoryDetails.StatusId != StatusId)
                        {
                            ProductCategoryDetails.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC199, ResourceItems.HCC199M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCategory", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Saves the configuration value.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveConfigurationValue(OTUCConfigurationValue.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC192, ResourceItems.HCC192M);
                }
                if (string.IsNullOrEmpty(_Request.ConfigurationKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC193, ResourceItems.HCC193M);
                }
                if (string.IsNullOrEmpty(_Request.Comment))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC194, ResourceItems.HCC194M);
                }
                if (_Request.ConfigurationId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC195, ResourceItems.HCC195M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    SCConfiguration ConfigurationDetails = _HCoreContext.SCConfiguration
                                                        .Where(x =>
                                                        x.Id == _Request.ConfigurationId &&
                                                        x.Guid == _Request.ConfigurationKey).FirstOrDefault();
                    if (ConfigurationDetails != null)
                    {
                        ConfigurationDetails.Value = _Request.Value;
                        ConfigurationDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ConfigurationDetails.ModifyById = _Request.UserReference.AccountId;
                        ConfigurationDetails.Comment = _Request.Comment;

                        _SCConfiguration = new SCConfiguration();
                        _SCConfiguration.Guid = HCoreHelper.GenerateGuid();
                        _SCConfiguration.ParentId = _Request.ConfigurationId;
                        _SCConfiguration.Value = _Request.Value;
                        _SCConfiguration.Comment = _Request.Comment;
                        _SCConfiguration.StartDate = HCoreHelper.GetGMTDateTime();
                        _SCConfiguration.CreateDate = HCoreHelper.GetGMTDateTime();
                        _SCConfiguration.CreatedById = _Request.UserReference.AccountId;
                        _SCConfiguration.StatusId = HelperStatus.Default.Active;
                        _HCoreContext.SCConfiguration.Add(_SCConfiguration);
                        _HCoreContext.SaveChanges();

                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _SCConfiguration.Id,
                            ReferenceKey = _SCConfiguration.Guid,
                        };
                        long? IconStorageId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.SCConfiguration.Where(x => x.Id == _Request.ConfigurationId).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.ImageStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }

                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.SCConfiguration.Where(x => x.Id == _SCConfiguration.Id).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.ImageStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC145, ResourceItems.HCC145M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC196, ResourceItems.HCC196M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveConfigurationValue", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC500, ResourceItems.HC500M);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfiguration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.SCConfiguration
                                                 where x.ParentId == null && x.AccountId == 1
                                                 select new OTUCConfiguration.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     DataTypeCode = x.DataType.SystemName,
                                                     DataTypeName = x.DataType.Name,

                                                     Name = x.Name,
                                                     SystemName = x.Guid,
                                                     Description = x.Description,

                                                     ImageUrl = x.ImageStorage.Path,

                                                     Value = x.Value,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OTUCConfiguration.List> Data = (from x in _HCoreContext.SCConfiguration
                                                         where x.ParentId == null && x.AccountId == 1
                                                         select new OTUCConfiguration.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             DataTypeCode = x.DataType.SystemName,
                                                             DataTypeName = x.DataType.Name,

                                                             Name = x.Name,
                                                             SystemName = x.Guid,
                                                             Description = x.Description,

                                                             ImageUrl = x.ImageStorage.Path,

                                                             Value = x.Value,

                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyByKey = x.ModifyBy.Guid,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                        {
                            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                        }
                        else
                        {
                            _Details.ImageUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the dealer configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetDealerConfiguration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.SCConfiguration
                                                 where x.ParentId == null && x.AccountId == 1  && x.LevelId ==  1
                                                 select new OTUCConfiguration.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     DataTypeCode = x.DataType.SystemName,
                                                     DataTypeName = x.DataType.Name,

                                                     Name = x.Name,
                                                     SystemName = x.Guid,
                                                     Description = x.Description,

                                                     ImageUrl = x.ImageStorage.Path,

                                                     Value = x.Value,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OTUCConfiguration.List> Data = (from x in _HCoreContext.SCConfiguration
                                                 where x.ParentId == null && x.AccountId == 1  && x.LevelId ==  1
                                                         select new OTUCConfiguration.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             DataTypeCode = x.DataType.SystemName,
                                                             DataTypeName = x.DataType.Name,

                                                             Name = x.Name,
                                                             SystemName = x.Guid,
                                                             Description = x.Description,

                                                             ImageUrl = x.ImageStorage.Path,

                                                             Value = x.Value,

                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyByKey = x.ModifyBy.Guid,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                        {
                            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                        }
                        else
                        {
                            _Details.ImageUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }



        /// <summary>
        /// Description: Gets the configuration history.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetConfigurationHistory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.SCConfiguration
                                                 where x.ParentId == _Request.ReferenceId && x.Parent.Guid == _Request.ReferenceKey
                                                 select new OTUCConfigurationValue.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     ImageUrl = x.ImageStorage.Path,

                                                     Value = x.Value,

                                                     Comment = x.Comment,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OTUCConfigurationValue.List> Data = (from x in _HCoreContext.SCConfiguration
                                                              where x.ParentId == _Request.ReferenceId && x.Parent.Guid == _Request.ReferenceKey
                                                              select new OTUCConfigurationValue.List
                                                              {
                                                                  ReferenceId = x.Id,
                                                                  ReferenceKey = x.Guid,

                                                                  ImageUrl = x.ImageStorage.Path,

                                                                  Value = x.Value,

                                                                  Comment = x.Comment,

                                                                  CreateDate = x.CreateDate,
                                                                  CreatedByKey = x.CreatedBy.Guid,
                                                                  CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                  ModifyDate = x.ModifyDate,
                                                                  ModifyByKey = x.ModifyBy.Guid,
                                                                  ModifyByDisplayName = x.ModifyBy.DisplayName,

                                                                  StatusId = x.StatusId,
                                                                  StatusCode = x.Status.SystemName,
                                                                  StatusName = x.Status.Name
                                                              })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                        {
                            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                        }
                        else
                        {
                            _Details.ImageUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfigurationHistory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Saves the account configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveAccountConfiguration(OTUCConfigurationValue.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC192, ResourceItems.HCC192M);
                }
                if (_Request.AccountId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC195, ResourceItems.HCC195M);
                }
                if (string.IsNullOrEmpty(_Request.Value))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC192, ResourceItems.HCC192M);
                }
                if (string.IsNullOrEmpty(_Request.ConfigurationKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC193, ResourceItems.HCC193M);
                }
                if (string.IsNullOrEmpty(_Request.Comment))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC194, ResourceItems.HCC194M);
                }
                if (_Request.ConfigurationId < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC195, ResourceItems.HCC195M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    SCConfiguration ConfigurationDetails = _HCoreContext.SCConfiguration
                                                        .Where(x =>
                                                        x.Id == _Request.ConfigurationId &&
                                                        x.Guid == _Request.ConfigurationKey).FirstOrDefault();
                    if (ConfigurationDetails != null)
                    {

                        var tItems = _HCoreContext.SCConfiguration.Where(x => x.AccountId == _Request.AccountId && x.ParentId == _Request.ConfigurationId).ToList();
                        foreach (var tItem in tItems)
                        {
                            tItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            tItem.ModifyById = _Request.UserReference.AccountId;
                            tItem.StatusId = HelperStatus.Default.Inactive;
                        }
                            _HCoreContext.SaveChanges();
 

                        using (_HCoreContext = new HCoreContext())
                        {
                            _SCConfiguration = new SCConfiguration();
                            _SCConfiguration.Guid = HCoreHelper.GenerateGuid();
                            _SCConfiguration.ParentId = _Request.ConfigurationId;
                            _SCConfiguration.AccountId = _Request.AccountId;
                            _SCConfiguration.Value = _Request.Value;
                            _SCConfiguration.Comment = _Request.Comment;
                            _SCConfiguration.StartDate = HCoreHelper.GetGMTDateTime();
                            _SCConfiguration.CreateDate = HCoreHelper.GetGMTDateTime();
                            _SCConfiguration.CreatedById = _Request.UserReference.AccountId;
                            _SCConfiguration.StatusId = HelperStatus.Default.Active;
                            _HCoreContext.SCConfiguration.Add(_SCConfiguration);
                            _HCoreContext.SaveChanges();
                            var _Response = new
                            {
                                ReferenceId = _SCConfiguration.Id,
                                ReferenceKey = _SCConfiguration.Guid,
                            };
                            long? IconStorageId = null;
                            if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                            }
                            if (IconStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var PrDetails = _HCoreContext.SCConfiguration.Where(x => x.Id == _SCConfiguration.Id).FirstOrDefault();
                                    if (PrDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            PrDetails.ImageStorageId = IconStorageId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                    else
                                    {
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC145, ResourceItems.HCC145M);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC196, ResourceItems.HCC196M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveAccountConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC500, ResourceItems.HC500M);
                #endregion
            }
            #endregion
        }


        /// <summary>
        /// Description: Gets the account configuration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetAccountConfiguration(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.SCConfiguration
                                                 where x.ParentId == null && x.AccountId == 1 && x.LevelId == 1
                                                 select new OTUCAccountConfiguration.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     DataTypeCode = x.DataType.SystemName,
                                                     DataTypeName = x.DataType.Name,

                                                     Name = x.Name,
                                                     SystemName = x.Guid,
                                                     Description = x.Description,

                                                     ImageUrl = x.ImageStorage.Path,

                                                     OriginalValue = x.Value,
                                                     CustomValue = _HCoreContext.SCConfiguration.Where(a=>a.ParentId == x.Id && a.AccountId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active).OrderByDescending(a => a.CreateDate).Select(a=>a.Value).FirstOrDefault(),

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OTUCAccountConfiguration.List> Data = (from x in _HCoreContext.SCConfiguration 
                                                         where x.ParentId == null && x.AccountId == 1 && x.LevelId == 1
                                                                select new OTUCAccountConfiguration.List
                                                         {
                                                             ReferenceId = x.Id,
                                                             ReferenceKey = x.Guid,

                                                             DataTypeCode = x.DataType.SystemName,
                                                             DataTypeName = x.DataType.Name,

                                                             Name = x.Name,
                                                             SystemName = x.Guid,
                                                             Description = x.Description,

                                                             ImageUrl = x.ImageStorage.Path,

                                                             OriginalValue = x.Value,
                                                             CustomValue = _HCoreContext.SCConfiguration.Where(a => a.ParentId == x.Id && a.AccountId == _Request.ReferenceId && x.StatusId == HelperStatus.Default.Active).OrderByDescending(a=>a.CreateDate).Select(a => a.Value).FirstOrDefault(),

                                                             CreateDate = x.CreateDate,
                                                             CreatedByKey = x.CreatedBy.Guid,
                                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                             ModifyDate = x.ModifyDate,
                                                             ModifyByKey = x.ModifyBy.Guid,
                                                             ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                             StatusId = x.StatusId,
                                                             StatusCode = x.Status.SystemName,
                                                             StatusName = x.Status.Name
                                                         })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.ImageUrl))
                        {
                            _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
                        }
                        else
                        {
                            _Details.ImageUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }


        //internal OResponse GetAccountConfiguration(OList.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        #region Add Default Request
        //        if (string.IsNullOrEmpty(_Request.SearchCondition))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
        //            #endregion
        //        }
        //        if (string.IsNullOrEmpty(_Request.SortExpression))
        //        {
        //            #region Default Conditions
        //            HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
        //            #endregion
        //        }
        //        #endregion
        //        #region Operation
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            #region Total Records
        //            if (_Request.RefreshCount)
        //            {
        //                _Request.TotalRecords = (from x in _HCoreContext.SCConfiguration
        //                                         where x.AccountId == _Request.ReferenceId && x.StatusId  == 2
        //                                         select new OTUCConfiguration.List
        //                                         {
        //                                             ReferenceId = x.Id,
        //                                             ReferenceKey = x.Guid,

        //                                             DataTypeCode = x.Parent.DataType.SystemName,
        //                                             DataTypeName = x.Parent.DataType.Name,

        //                                             Name = x.Parent.Name,
        //                                             SystemName = x.Parent.Guid,
        //                                             Description = x.Parent.Description,

        //                                             ImageUrl = x.Parent.ImageStorage.Path,

        //                                             Value = x.Value,

        //                                             CreateDate = x.CreateDate,
        //                                             CreatedByKey = x.CreatedBy.Guid,
        //                                             CreatedByDisplayName = x.CreatedBy.DisplayName,

        //                                             ModifyDate = x.ModifyDate,
        //                                             ModifyByKey = x.ModifyBy.Guid,
        //                                             ModifyByDisplayName = x.ModifyBy.DisplayName,


        //                                             StatusId = x.StatusId,
        //                                             StatusCode = x.Status.SystemName,
        //                                             StatusName = x.Status.Name
        //                                         })
        //                                                        .Where(_Request.SearchCondition)
        //                                                .Count();
        //            }
        //            #endregion
        //            #region Get Data
        //            List<OTUCConfiguration.List> Data = (from x in _HCoreContext.SCConfiguration
        //                                                 where x.AccountId == _Request.ReferenceId && x.StatusId == 2
        //                                                 select new OTUCConfiguration.List
        //                                                 {
        //                                                     ReferenceId = x.Id,
        //                                                     ReferenceKey = x.Guid,

        //                                                     DataTypeCode = x.Parent.DataType.SystemName,
        //                                                     DataTypeName = x.Parent.DataType.Name,

        //                                                     Name = x.Parent.Name,
        //                                                     SystemName = x.Parent.Guid,
        //                                                     Description = x.Parent.Description,

        //                                                     ImageUrl = x.Parent.ImageStorage.Path,

        //                                                     Value = x.Value,

        //                                                     CreateDate = x.CreateDate,
        //                                                     CreatedByKey = x.CreatedBy.Guid,
        //                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

        //                                                     ModifyDate = x.ModifyDate,
        //                                                     ModifyByKey = x.ModifyBy.Guid,
        //                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,


        //                                                     StatusId = x.StatusId,
        //                                                     StatusCode = x.Status.SystemName,
        //                                                     StatusName = x.Status.Name
        //                                                 })
        //                                      .Where(_Request.SearchCondition)
        //                                      .OrderBy(_Request.SortExpression)
        //                                      .Skip(_Request.Offset)
        //                                      .Take(_Request.Limit)
        //                                      .ToList();
        //            #endregion
        //            #region Create  Response Object
        //            foreach (var _Details in Data)
        //            {
        //                if (!string.IsNullOrEmpty(_Details.ImageUrl))
        //                {
        //                    _Details.ImageUrl = _AppConfig.StorageUrl + _Details.ImageUrl;
        //                }
        //                else
        //                {
        //                    _Details.ImageUrl = _AppConfig.Default_Icon;
        //                }
        //            }
        //            _HCoreContext.Dispose();
        //            OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
        //            #endregion
        //            #region Send Response
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
        //            #endregion
        //        }
        //        #endregion

        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region Log Bug
        //        HCoreHelper.LogException("GetConfiguration", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Create DataTable Response Object
        //        OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
        //        #endregion
        //    }
        //    #endregion
        //}

    }
}
