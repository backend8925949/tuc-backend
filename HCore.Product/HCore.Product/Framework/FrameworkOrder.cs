//==================================================================================
// FileName: FrameworkOrder.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to orders
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;

namespace HCore.Product.Framework
{
    public class FrameworkOrder
    {
        HCoreContext _HCoreContext;
        OOrder.Overview _OrderOverview;
        /// <summary>
        /// Description: Gets the customer order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerOrder(OOrderManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var OrderDetails = _HCoreContext.SCOrder
                            .Where(x => x.Id == _Request.OrderId && x.Guid == _Request.OrderKey
                            && x.Guid == _Request.OrderKey)
                            .Select(x => new OrderDetails.Order
                            {
                                OrderId = x.Id,
                                OrderReference = x.Guid,
                                OrderNumber = x.OrderId,
                                OpenDate = x.OpenDate,
                                CloseDate = x.CloseDate,
                                EPTime = x.EPTime,
                                CustomerId = x.CustomerId,
                                CustomerKey = x.Customer.Guid,
                                CustomerDisplayName = x.Customer.DisplayName,
                                CustomerEmailAddress = x.Customer.EmailAddress,
                                CustomerMobileNumber = x.Customer.MobileNumber,

                                TotalItem = x.TotalItem,
                                Amount = x.Amount,
                                Charge = x.Charge,
                                OtherCharge = x.OtherCharge,
                                DeliveryCharge = x.DeliveryCharge,
                                DiscountAmount = x.DiscountAmount,
                                RewardAmount = x.RewardAmount,
                                TotalAmount = x.TotalAmount,

                                DealerLocationRating = x.DealerLocationRating,
                                DealerLocationReview = x.DealerLocationReview,

                                PaymentModeCode = x.PaymentMode.SystemName,
                                PaymentModeName = x.PaymentMode.Name,

                                PaymentStatusCode = x.PaymentStatus.SystemName,
                                PaymentStatusName = x.PaymentStatus.Name,

                                PaymentReference = x.PaymentReference,

                                DealerId = x.DealerId,
                                DealerKey = x.Dealer.Guid,
                                DealerName = x.Dealer.DisplayName,
                                DealerIconUrl = x.Dealer.IconStorage.Path,

                                DealerLocationId = x.DealerLocationId,
                                DealerLocationKey = x.DealerLocation.Guid,
                                DealerLocationName = x.DealerLocation.Name,
                                DealerLocationAddress = x.DealerLocation.Address,
                                DealerLocationLatitude = x.DealerLocation.Latitude,
                                DealerLocationLongitude = x.DealerLocation.Longitude,

                                ExpectedDeliveryDate = x.ExpectedDeliveryDate,
                                ActualDeliveryDate = x.ActualDeliveryDate,

                                AgentId = x.RiderId,
                                AgentKey = x.Rider.Guid,
                                AgentDisplayName = x.Rider.DisplayName,
                                AgentMobileNumber = x.Rider.MobileNumber,
                                AgentEmailAddress = x.Rider.EmailAddress,
                                AgentIconUrl = x.Rider.IconStorage.Path,

                                CustomerRating = x.CustomerRating,
                                CustomerReview = x.CustomerReview,

                                RiderRating = x.RiderRating,
                                RiderReview = x.RiderReview,
                                OrderDeliveryCode = x.OrderDeliveryCode,

                                DeliveryTypeId = x.DeliveryTypeId,
                                DeliveryTypeCode = x.DeliveryType.SystemName,
                                DeliveryTypeName = x.DeliveryType.Name,

                                PriorityTypeId = x.PriorityId,
                                PriorityTypeCode = x.Priority.SystemName,
                                PriotityTypeName = x.Priority.Name,



                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                ModifyDate = x.ModifyDate,
                                ModifyById = x.ModifyById,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,
                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .FirstOrDefault();
                    if (OrderDetails != null)
                    {
                        if (!string.IsNullOrEmpty(OrderDetails.AgentIconUrl))
                        {
                            OrderDetails.AgentIconUrl = _AppConfig.StorageUrl + OrderDetails.AgentIconUrl;
                        }
                        else
                        {
                            OrderDetails.AgentIconUrl = OrderDetails.DealerIconUrl;
                        }

                        if (!string.IsNullOrEmpty(OrderDetails.DealerIconUrl))
                        {
                            OrderDetails.DealerIconUrl = _AppConfig.StorageUrl + OrderDetails.DealerIconUrl;
                        }
                        else
                        {
                            OrderDetails.DealerIconUrl = OrderDetails.DealerIconUrl;
                        }
                        OrderDetails.Activity = _HCoreContext.SCOrderActivity.Where(x => x.StatusId == HelperStatus.Default.Active && x.OrderId == OrderDetails.OrderId && x.OrderItemId == null)
                                              .OrderByDescending(x => x.CreateDate)
                                              .Select(x => new OrderDetails.OrderActivity
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  Title = x.Title,
                                                  Description = x.Description,
                                                  Comment = x.Comment,
                                                  OrderStatusId = x.OrderStatusId,
                                                  OrderStatusCode = x.OrderStatus.SystemName,
                                                  OrderStatusName = x.OrderStatus.Name,
                                                  CreateDate = x.CreateDate,
                                                  CreatedById = x.CreatedById,
                                                  CreatedByDisplayName = x.CreatedBy.DisplayName,
                                              }).ToList();

                        OrderDetails.Address = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == OrderDetails.OrderId)
                                                .Select(x => new OrderDetails.OrderAddress
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    AddressLine1 = x.AddressLine1,
                                                    AddressLine2 = x.AddressLine2,
                                                    Landmark = x.Landmark,
                                                    AlternateMobileNumber = x.AlternateMobileNumber,
                                                    ZipCode = x.ZipCode,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                }).FirstOrDefault();

                        OrderDetails.OrderItems = _HCoreContext.SCOrderItem.Where(x => x.OrderId == OrderDetails.OrderId)
                                                .Select(x => new OrderDetails.OrderItem
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    ProductId = x.Varient.ProductId,
                                                    ProductKey = x.Varient.Guid,
                                                    ProductName = x.Varient.Name,
                                                    VarientId = x.VarientId,
                                                    VarientKey = x.Varient.Guid,
                                                    VarientName = x.Varient.Name,
                                                    UnitPrice = x.UnitPrice,
                                                    Quantity = x.Quantity,
                                                    TotalAmount = x.TotalAmount,
                                                    ProductIconUrl = x.Varient.Product.PrimaryStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    CreatedById = x.CreatedById,
                                                    CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name,
                                                }).ToList();
                        foreach (var OrderItem in OrderDetails.OrderItems)
                        {
                            if (!string.IsNullOrEmpty(OrderItem.ProductIconUrl))
                            {
                                OrderItem.ProductIconUrl = _AppConfig.StorageUrl + OrderItem.ProductIconUrl;
                            }
                            if (!string.IsNullOrEmpty(OrderItem.VarientIconUrl))
                            {
                                OrderItem.VarientIconUrl = _AppConfig.StorageUrl + OrderItem.VarientIconUrl;
                            }
                            if (string.IsNullOrEmpty(OrderItem.VarientIconUrl))
                            {
                                OrderItem.VarientIconUrl = OrderItem.ProductIconUrl;
                            }

                            OrderItem.Activity = _HCoreContext.SCOrderActivity.Where(x => x.OrderId == OrderDetails.OrderId && x.OrderItemId == OrderItem.ReferenceId)
                                            .OrderByDescending(x => x.CreateDate)
                                            .Select(x => new OrderDetails.OrderActivity
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,
                                                Title = x.Title,
                                                Description = x.Description,
                                                Comment = x.Comment,
                                                OrderStatusId = x.OrderStatusId,
                                                OrderStatusCode = x.OrderStatus.SystemName,
                                                OrderStatusName = x.OrderStatus.Name,
                                                CreateDate = x.CreateDate,
                                                CreatedById = x.CreatedById,
                                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                                            }).ToList();
                        }
                    }
                    if (OrderDetails != null)
                    {
                        //if (OrderDetails.StatusId == HelperStatus.OrderStatus.PendingConfirmation)
                        //{
                        //    string AccMob = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.MobileNumber).FirstOrDefault();
                        //    long NinjaId = _HCoreContext.TUCNinjaRegistration.Where(x => x.MobileNumber == AccMob).Select(x => x.Id).FirstOrDefault();
                        //    if (NinjaId > 0)
                        //    {
                        //        OrderDetails.StatusId = HelperStatus.OrderStatus.Ready;
                        //        OrderDetails.StatusCode = "orderstatus.ready";
                        //        OrderDetails.StatusName = "Pending Confirmation";
                        //    }

                        //    //OrderDetails.UserCards = _HCoreContext.HCUAccountParameter
                        //    //    .Where(x => x.AccountId == OrderDetails.CustomerId
                        //    //    && x.TypeId == 465)
                        //    //    .OrderByDescending(x => x.ModifyDate)
                        //    //    .Select(x => new OrderDetails.OUserCard
                        //    //    {
                        //    //        ReferenceId = x.Id,
                        //    //        Brand = x.Name,
                        //    //        Bin = x.SystemName,
                        //    //        End = x.Value,
                        //    //        ExpMonth = x.SubValue,
                        //    //        ExpYear = x.Description,
                        //    //        Channel = x.Data,
                        //    //    }).Skip(0).Take(10).ToList();

                        //}

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, OrderDetails, "HCP200", "Order details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP400", "Order details not found");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetOrder", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the customer order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCustomerOrder(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.SCOrder
                                             where x.CustomerId == _Request.UserReference.AccountId
                                             select new OOrder.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 OrderId = x.OrderId,


                                                 DealerId = x.Dealer.Id,
                                                 DealerKey = x.Dealer.Guid,
                                                 DealerDisplayName = x.Dealer.DisplayName,
                                                 DealerIconUrl = x.Dealer.IconStorage.Path,

                                                 DealerLocationId = x.DealerLocation.Id,
                                                 DealerLocationKey = x.DealerLocation.Guid,
                                                 DealerLocationName = x.DealerLocation.DisplayName,
                                                 DealerLocationAddress = x.DealerLocation.Address,

                                                 TotalItem = x.TotalItem,
                                                 TotalAmount = x.TotalAmount,


                                                 OrderDate = x.OpenDate,
                                                 DeliveryDate = x.ActualDeliveryDate,
                                                 CreateDate = x.CreateDate,

                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name
                                             })
                                                    .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion

                    #region Get Data
                    List<OOrder.List> Data = (from x in _HCoreContext.SCOrder
                                              where x.CustomerId == _Request.UserReference.AccountId
                                              select new OOrder.List
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  OrderId = x.OrderId,



                                                  DealerId = x.Dealer.Id,
                                                  DealerKey = x.Dealer.Guid,
                                                  DealerDisplayName = x.Dealer.DisplayName,
                                                  DealerIconUrl = x.Dealer.IconStorage.Path,

                                                  DealerLocationId = x.DealerLocation.Id,
                                                  DealerLocationKey = x.DealerLocation.Guid,
                                                  DealerLocationName = x.DealerLocation.DisplayName,
                                                  DealerLocationAddress = x.DealerLocation.Address,

                                                  TotalItem = x.TotalItem,
                                                  TotalAmount = x.TotalAmount,


                                                  OrderDate = x.OpenDate,
                                                  DeliveryDate = x.ActualDeliveryDate,
                                                  CreateDate = x.CreateDate,

                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name
                                              })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.DealerIconUrl))
                        {
                            DataItem.DealerIconUrl = _AppConfig.StorageUrl + DataItem.DealerIconUrl;
                        }
                        else
                        {
                            DataItem.DealerIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetCustomerOrder", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOrder(OOrder.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OOrder.Details _Details = (from x in _HCoreContext.SCOrder
                                               where x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey
                                               select new OOrder.Details
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   EPTime = x.EPTime,

                                                   OrderId = x.OrderId,

                                                   OpenDate = x.OpenDate,
                                                   CloseDate = x.CloseDate,


                                                   CustomerId = x.Customer.Id,
                                                   CustomerKey = x.Customer.Guid,
                                                   CustomerDisplayName = x.Customer.DisplayName,
                                                   CustomerMobileNumber = x.Customer.MobileNumber,
                                                   CustomerEmailAddress = x.Customer.EmailAddress,
                                                   CustomerIconUrl = x.Customer.IconStorage.Path,



                                                   TotalItem = x.TotalItem,
                                                   Amount = x.Amount,
                                                   Charge = x.Charge,
                                                   OtherCharge = x.OtherCharge,
                                                   DeliveryCharge = x.DeliveryCharge,
                                                   DiscountAmount = x.DiscountAmount,
                                                   RewardAmount = x.RewardAmount,
                                                   TotalAmount = x.TotalAmount,




                                                   PaymentModeCode = x.PaymentMode.SystemName,
                                                   PaymentModeName = x.PaymentMode.Name,

                                                   PaymentStatusCode = x.PaymentStatus.SystemName,
                                                   PaymentStatusName = x.PaymentStatus.Name,

                                                   PaymentReference = x.PaymentReference,

                                                   DealerId = x.Dealer.Id,
                                                   DealerKey = x.Dealer.Guid,
                                                   DealerDisplayName = x.Dealer.DisplayName,
                                                   DealerIconUrl = x.Dealer.IconStorage.Path,
                                                   DealerContactNumber = x.Dealer.ContactNumber,

                                                   DealerLocationId = x.DealerLocation.Id,
                                                   DealerLocationKey = x.DealerLocation.Guid,
                                                   DealerLocationName = x.DealerLocation.DisplayName,
                                                   DealerLocationAddress = x.DealerLocation.Address,
                                                   DealerLocationLatitude = x.DealerLocation.Latitude,
                                                   DealerLocationLongitude = x.DealerLocation.Longitude,
                                                   DealerLocationContactNumber = x.DealerLocation.ContactNumber,

                                                   AgentId = x.Rider.Id,
                                                   AgentKey = x.Rider.Guid,
                                                   AgentDisplayName = x.Rider.DisplayName,
                                                   AgentMobileNumber = x.Rider.MobileNumber,
                                                   AgentIconUrl = x.Rider.IconStorage.Path,



                                                   ExpectedDeliveryDate = x.ExpectedDeliveryDate,
                                                   ActualDeliveryDate = x.ActualDeliveryDate,

                                                   DeliveryTypeCode = x.DeliveryType.SystemName,
                                                   DeliveryTypeName = x.DeliveryType.Name,

                                                   CustomerRating = x.CustomerRating,
                                                   CustomerReview = x.CustomerReview,

                                                   CreateDate = x.CreateDate,
                                                   CreatedByKey = x.CreatedBy.Guid,
                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                   ModifyDate = x.ModifyDate,
                                                   ModifyByKey = x.ModifyBy.Guid,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                   StatusId = x.StatusId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name
                                               }).FirstOrDefault();

                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.AgentIconUrl))
                        {
                            _Details.AgentIconUrl = _AppConfig.StorageUrl + _Details.AgentIconUrl;
                        }
                        else
                        {
                            _Details.AgentIconUrl = _AppConfig.Default_Icon;
                        }


                        if (!string.IsNullOrEmpty(_Details.CustomerIconUrl))
                        {
                            _Details.CustomerIconUrl = _AppConfig.StorageUrl + _Details.CustomerIconUrl;
                        }
                        else
                        {
                            _Details.CustomerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.DealerIconUrl))
                        {
                            _Details.DealerIconUrl = _AppConfig.StorageUrl + _Details.DealerIconUrl;
                        }
                        else
                        {
                            _Details.DealerIconUrl = _AppConfig.Default_Icon;
                        }
                        _Details.OrderActivity = _HCoreContext.SCOrderActivity.Where(x => x.OrderId == _Details.ReferenceId)
                            .Select(x => new OOrder.OrderActivity
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                OrderItemId = x.OrderItem.Id,
                                OrderItemKey = x.OrderItem.Guid,

                                Title = x.Title,
                                Description = x.Description,
                                SystemDescription = x.SystemDescription,
                                Comment = x.Comment,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                            }).OrderByDescending(x => x.CreateDate).ToList();
                        _Details.OrderItems = _HCoreContext.SCOrderItem.Where(x => x.OrderId == _Details.ReferenceId)
                            .Select(x => new OOrder.OrderItem
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                ProductId = x.Varient.ProductId,
                                ProductKey = x.Varient.Product.Guid,
                                ProductName = x.Varient.Product.Name,

                                VarientId = x.VarientId,
                                VarientKey = x.Varient.Guid,
                                VarientName = x.Varient.Name,
                                VarientIconUrl = x.Varient.Product.PrimaryStorage.Path,

                                UnitPrice = x.UnitPrice,
                                Quantity = x.Quantity,
                                TotalAmount = x.TotalAmount,

                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name

                            }).ToList();
                        foreach (var item in _Details.OrderItems)
                        {
                            if (!string.IsNullOrEmpty(item.VarientIconUrl))
                            {
                                item.VarientIconUrl = _AppConfig.StorageUrl + item.VarientIconUrl;
                            }
                            else
                            {
                                item.VarientIconUrl = _AppConfig.Default_Icon;
                            }

                        }

                        _Details.BillingAddress = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == _Details.ReferenceId && x.TypeId == 505)
                            .Select(x => new OOrder.OrderAddress
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                Name = x.Name,
                                MobileNumber = x.MobileNumber,
                                AlternateMobileNumber = x.AlternateMobileNumber,
                                EmailAddress = x.EmailAddress,
                                AddressLine1 = x.AddressLine1,
                                AddressLine2 = x.AddressLine2,
                                LandMark = x.Landmark,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                MapAddress = x.MapAddress,


                                AreaId = x.AreaId,
                                AreaName = x.Area.Name,
                                CityId = x.CityId,
                                CityName = x.City.Name,
                                StateId = x.StateId,
                                StateName = x.State.Name,
                                CountryId = x.CountryId,
                                CountryName = x.Country.Name,
                                ZipCode = x.ZipCode,
                                Instruction = x.Instructions,

                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name

                            }).FirstOrDefault();

                        _Details.ShippingAddress = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == _Details.ReferenceId && x.TypeId == 506)
                           .Select(x => new OOrder.OrderAddress
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,

                               Name = x.Name,
                               MobileNumber = x.MobileNumber,
                               AlternateMobileNumber = x.AlternateMobileNumber,
                               EmailAddress = x.EmailAddress,
                               AddressLine1 = x.AddressLine1,
                               AddressLine2 = x.AddressLine2,
                               LandMark = x.Landmark,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                               MapAddress = x.MapAddress,

                               AreaId = x.AreaId,
                               AreaName = x.Area.Name,
                               CityId = x.CityId,
                               CityName = x.City.Name,
                               StateId = x.StateId,
                               StateName = x.State.Name,
                               CountryId = x.CountryId,
                               CountryName = x.Country.Name,
                               ZipCode = x.ZipCode,
                               Instruction = x.Instructions,

                               CreateDate = x.CreateDate,
                               CreatedByKey = x.CreatedBy.Guid,
                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                               ModifyDate = x.ModifyDate,
                               ModifyByKey = x.ModifyBy.Guid,
                               ModifyByDisplayName = x.ModifyBy.DisplayName,


                               StatusId = x.StatusId,
                               StatusCode = x.Status.SystemName,
                               StatusName = x.Status.Name

                           }).FirstOrDefault();

                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Details, ResourceItems.HC101, ResourceItems.HC101);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetOrders", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOrder(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.SCOrder
                                             select new OOrder.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 OrderId = x.OrderId,


                                                 CustomerId = x.Customer.Id,
                                                 CustomerKey = x.Customer.Guid,
                                                 CustomerDisplayName = x.Customer.DisplayName,
                                                 CustomerMobileNumber = x.Customer.MobileNumber,
                                                 DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                 DealerId = x.Dealer.Id,
                                                 DealerKey = x.Dealer.Guid,
                                                 DealerDisplayName = x.Dealer.DisplayName,
                                                 DealerIconUrl = x.Dealer.IconStorage.Path,


                                                 DealerLocationId = x.DealerLocation.Id,
                                                 DealerLocationKey = x.DealerLocation.Guid,
                                                 DealerLocationName = x.DealerLocation.DisplayName,
                                                 DealerLocationAddress = x.DealerLocation.Address,
                                                 DealerLocationRating = x.DealerLocationRating,

                                                 AgentId = x.Rider.Id,
                                                 AgentKey = x.Rider.Guid,
                                                 AgentDisplayName = x.Rider.DisplayName,
                                                 AgentMobileNumber = x.Rider.MobileNumber,
                                                 AgentIconUrl = x.Rider.IconStorage.Path,

                                                 TotalItem = x.TotalItem,
                                                 TotalAmount = x.TotalAmount,

                                                 OrderDate = x.OpenDate,
                                                 DeliveryDate = x.ActualDeliveryDate,
                                                 CreateDate = x.CreateDate,

                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name
                                             })
                                                    .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion

                    #region Get Data
                    List<OOrder.List> Data = (from x in _HCoreContext.SCOrder
                                              select new OOrder.List
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  OrderId = x.OrderId,


                                                  CustomerId = x.Customer.Id,
                                                  CustomerKey = x.Customer.Guid,
                                                  CustomerDisplayName = x.Customer.DisplayName,
                                                  CustomerMobileNumber = x.Customer.MobileNumber,
                                                  DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                  DealerId = x.Dealer.Id,
                                                  DealerKey = x.Dealer.Guid,
                                                  DealerDisplayName = x.Dealer.DisplayName,
                                                  DealerIconUrl = x.Dealer.IconStorage.Path,


                                                  DealerLocationId = x.DealerLocation.Id,
                                                  DealerLocationKey = x.DealerLocation.Guid,
                                                  DealerLocationName = x.DealerLocation.DisplayName,
                                                  DealerLocationAddress = x.DealerLocation.Address,
                                                  DealerLocationRating = x.DealerLocationRating,

                                                  AgentId = x.Rider.Id,
                                                  AgentKey = x.Rider.Guid,
                                                  AgentDisplayName = x.Rider.DisplayName,
                                                  AgentMobileNumber = x.Rider.MobileNumber,
                                                  AgentIconUrl = x.Rider.IconStorage.Path,

                                                  TotalItem = x.TotalItem,
                                                  TotalAmount = x.TotalAmount,


                                                  OrderDate = x.OpenDate,
                                                  DeliveryDate = x.ActualDeliveryDate,
                                                  CreateDate = x.CreateDate,

                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name
                                              })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.DealerIconUrl))
                        {
                            DataItem.DealerIconUrl = _AppConfig.StorageUrl + DataItem.DealerIconUrl;
                        }
                        else
                        {
                            DataItem.DealerIconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(DataItem.AgentIconUrl))
                        {
                            DataItem.AgentIconUrl = _AppConfig.StorageUrl + DataItem.AgentIconUrl;
                        }
                        else
                        {
                            DataItem.AgentIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetOrders", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the order overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOrderOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _OrderOverview = new OOrder.Overview();
                    #region Total Records
                    _OrderOverview.Total = (from x in _HCoreContext.SCOrder
                                            select new OOrder.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                OrderId = x.OrderId,


                                                CustomerId = x.Customer.Id,
                                                CustomerKey = x.Customer.Guid,
                                                CustomerDisplayName = x.Customer.DisplayName,
                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                DealerId = x.Dealer.Id,
                                                DealerKey = x.Dealer.Guid,
                                                DealerDisplayName = x.Dealer.DisplayName,
                                                DealerIconUrl = x.Dealer.IconStorage.Path,


                                                DealerLocationId = x.DealerLocation.Id,
                                                DealerLocationKey = x.DealerLocation.Guid,
                                                DealerLocationName = x.DealerLocation.DisplayName,
                                                DealerLocationAddress = x.DealerLocation.Address,
                                                DealerLocationRating = x.DealerLocationRating,

                                                AgentId = x.Rider.Id,
                                                AgentKey = x.Rider.Guid,
                                                AgentDisplayName = x.Rider.DisplayName,
                                                AgentMobileNumber = x.Rider.MobileNumber,
                                                AgentIconUrl = x.Rider.IconStorage.Path,

                                                TotalItem = x.TotalItem,
                                                TotalAmount = x.TotalAmount,

                                                OrderDate = x.OpenDate,
                                                DeliveryDate = x.ActualDeliveryDate,
                                                CreateDate = x.CreateDate,

                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name
                                            })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region New Records
                    _OrderOverview.New = (from x in _HCoreContext.SCOrder
                                          where x.StatusId == HelperStatus.OrderStatus.New
                                          select new OOrder.List
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,

                                              OrderId = x.OrderId,

                                              CustomerId = x.Customer.Id,
                                              CustomerKey = x.Customer.Guid,
                                              CustomerDisplayName = x.Customer.DisplayName,
                                              CustomerMobileNumber = x.Customer.MobileNumber,
                                              DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                              DealerId = x.Dealer.Id,
                                              DealerKey = x.Dealer.Guid,
                                              DealerDisplayName = x.Dealer.DisplayName,
                                              DealerIconUrl = x.Dealer.IconStorage.Path,


                                              DealerLocationId = x.DealerLocation.Id,
                                              DealerLocationKey = x.DealerLocation.Guid,
                                              DealerLocationName = x.DealerLocation.DisplayName,
                                              DealerLocationAddress = x.DealerLocation.Address,
                                              DealerLocationRating = x.DealerLocationRating,

                                              AgentId = x.Rider.Id,
                                              AgentKey = x.Rider.Guid,
                                              AgentDisplayName = x.Rider.DisplayName,
                                              AgentMobileNumber = x.Rider.MobileNumber,
                                              AgentIconUrl = x.Rider.IconStorage.Path,

                                              TotalItem = x.TotalItem,
                                              TotalAmount = x.TotalAmount,

                                              OrderDate = x.OpenDate,
                                              DeliveryDate = x.ActualDeliveryDate,
                                              CreateDate = x.CreateDate,

                                              StatusId = x.StatusId,
                                              StatusCode = x.Status.SystemName,
                                              StatusName = x.Status.Name
                                          })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region PendingConfirmation Records
                    _OrderOverview.PendingConfirmation = (from x in _HCoreContext.SCOrder
                                          where x.StatusId == HelperStatus.OrderStatus.PendingConfirmation
                                          select new OOrder.List
                                          {
                                              ReferenceId = x.Id,
                                              ReferenceKey = x.Guid,

                                              OrderId = x.OrderId,

                                              CustomerId = x.Customer.Id,
                                              CustomerKey = x.Customer.Guid,
                                              CustomerDisplayName = x.Customer.DisplayName,
                                              CustomerMobileNumber = x.Customer.MobileNumber,
                                              DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                              DealerId = x.Dealer.Id,
                                              DealerKey = x.Dealer.Guid,
                                              DealerDisplayName = x.Dealer.DisplayName,
                                              DealerIconUrl = x.Dealer.IconStorage.Path,


                                              DealerLocationId = x.DealerLocation.Id,
                                              DealerLocationKey = x.DealerLocation.Guid,
                                              DealerLocationName = x.DealerLocation.DisplayName,
                                              DealerLocationAddress = x.DealerLocation.Address,
                                              DealerLocationRating = x.DealerLocationRating,

                                              AgentId = x.Rider.Id,
                                              AgentKey = x.Rider.Guid,
                                              AgentDisplayName = x.Rider.DisplayName,
                                              AgentMobileNumber = x.Rider.MobileNumber,
                                              AgentIconUrl = x.Rider.IconStorage.Path,

                                              TotalItem = x.TotalItem,
                                              TotalAmount = x.TotalAmount,

                                              OrderDate = x.OpenDate,
                                              DeliveryDate = x.ActualDeliveryDate,
                                              CreateDate = x.CreateDate,

                                              StatusId = x.StatusId,
                                              StatusCode = x.Status.SystemName,
                                              StatusName = x.Status.Name
                                          })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region Confirmed Records
                    _OrderOverview.Confirmed = (from x in _HCoreContext.SCOrder
                                                          where x.StatusId == HelperStatus.OrderStatus.Confirmed
                                                          select new OOrder.List
                                                          {
                                                              ReferenceId = x.Id,
                                                              ReferenceKey = x.Guid,

                                                              OrderId = x.OrderId,

                                                              CustomerId = x.Customer.Id,
                                                              CustomerKey = x.Customer.Guid,
                                                              CustomerDisplayName = x.Customer.DisplayName,
                                                              CustomerMobileNumber = x.Customer.MobileNumber,
                                                              DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                              DealerId = x.Dealer.Id,
                                                              DealerKey = x.Dealer.Guid,
                                                              DealerDisplayName = x.Dealer.DisplayName,
                                                              DealerIconUrl = x.Dealer.IconStorage.Path,


                                                              DealerLocationId = x.DealerLocation.Id,
                                                              DealerLocationKey = x.DealerLocation.Guid,
                                                              DealerLocationName = x.DealerLocation.DisplayName,
                                                              DealerLocationAddress = x.DealerLocation.Address,
                                                              DealerLocationRating = x.DealerLocationRating,

                                                              AgentId = x.Rider.Id,
                                                              AgentKey = x.Rider.Guid,
                                                              AgentDisplayName = x.Rider.DisplayName,
                                                              AgentMobileNumber = x.Rider.MobileNumber,
                                                              AgentIconUrl = x.Rider.IconStorage.Path,

                                                              TotalItem = x.TotalItem,
                                                              TotalAmount = x.TotalAmount,

                                                              OrderDate = x.OpenDate,
                                                              DeliveryDate = x.ActualDeliveryDate,
                                                              CreateDate = x.CreateDate,

                                                              StatusId = x.StatusId,
                                                              StatusCode = x.Status.SystemName,
                                                              StatusName = x.Status.Name
                                                          })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region Preparing Records
                    _OrderOverview.Preparing = (from x in _HCoreContext.SCOrder
                                                where x.StatusId == HelperStatus.OrderStatus.Preparing
                                                select new OOrder.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    OrderId = x.OrderId,

                                                    CustomerId = x.Customer.Id,
                                                    CustomerKey = x.Customer.Guid,
                                                    CustomerDisplayName = x.Customer.DisplayName,
                                                    CustomerMobileNumber = x.Customer.MobileNumber,
                                                    DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                    DealerId = x.Dealer.Id,
                                                    DealerKey = x.Dealer.Guid,
                                                    DealerDisplayName = x.Dealer.DisplayName,
                                                    DealerIconUrl = x.Dealer.IconStorage.Path,


                                                    DealerLocationId = x.DealerLocation.Id,
                                                    DealerLocationKey = x.DealerLocation.Guid,
                                                    DealerLocationName = x.DealerLocation.DisplayName,
                                                    DealerLocationAddress = x.DealerLocation.Address,
                                                    DealerLocationRating = x.DealerLocationRating,

                                                    AgentId = x.Rider.Id,
                                                    AgentKey = x.Rider.Guid,
                                                    AgentDisplayName = x.Rider.DisplayName,
                                                    AgentMobileNumber = x.Rider.MobileNumber,
                                                    AgentIconUrl = x.Rider.IconStorage.Path,

                                                    TotalItem = x.TotalItem,
                                                    TotalAmount = x.TotalAmount,

                                                    OrderDate = x.OpenDate,
                                                    DeliveryDate = x.ActualDeliveryDate,
                                                    CreateDate = x.CreateDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region Ready Records
                    _OrderOverview.Ready = (from x in _HCoreContext.SCOrder
                                                where x.StatusId == HelperStatus.OrderStatus.Ready
                                                select new OOrder.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    OrderId = x.OrderId,

                                                    CustomerId = x.Customer.Id,
                                                    CustomerKey = x.Customer.Guid,
                                                    CustomerDisplayName = x.Customer.DisplayName,
                                                    CustomerMobileNumber = x.Customer.MobileNumber,
                                                    DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                    DealerId = x.Dealer.Id,
                                                    DealerKey = x.Dealer.Guid,
                                                    DealerDisplayName = x.Dealer.DisplayName,
                                                    DealerIconUrl = x.Dealer.IconStorage.Path,


                                                    DealerLocationId = x.DealerLocation.Id,
                                                    DealerLocationKey = x.DealerLocation.Guid,
                                                    DealerLocationName = x.DealerLocation.DisplayName,
                                                    DealerLocationAddress = x.DealerLocation.Address,
                                                    DealerLocationRating = x.DealerLocationRating,

                                                    AgentId = x.Rider.Id,
                                                    AgentKey = x.Rider.Guid,
                                                    AgentDisplayName = x.Rider.DisplayName,
                                                    AgentMobileNumber = x.Rider.MobileNumber,
                                                    AgentIconUrl = x.Rider.IconStorage.Path,

                                                    TotalItem = x.TotalItem,
                                                    TotalAmount = x.TotalAmount,

                                                    OrderDate = x.OpenDate,
                                                    DeliveryDate = x.ActualDeliveryDate,
                                                    CreateDate = x.CreateDate,

                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region ReadyToPickup Records
                    _OrderOverview.Ready = (from x in _HCoreContext.SCOrder
                                            where x.StatusId == HelperStatus.OrderStatus.ReadyToPickup
                                            select new OOrder.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                OrderId = x.OrderId,

                                                CustomerId = x.Customer.Id,
                                                CustomerKey = x.Customer.Guid,
                                                CustomerDisplayName = x.Customer.DisplayName,
                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                DealerId = x.Dealer.Id,
                                                DealerKey = x.Dealer.Guid,
                                                DealerDisplayName = x.Dealer.DisplayName,
                                                DealerIconUrl = x.Dealer.IconStorage.Path,


                                                DealerLocationId = x.DealerLocation.Id,
                                                DealerLocationKey = x.DealerLocation.Guid,
                                                DealerLocationName = x.DealerLocation.DisplayName,
                                                DealerLocationAddress = x.DealerLocation.Address,
                                                DealerLocationRating = x.DealerLocationRating,

                                                AgentId = x.Rider.Id,
                                                AgentKey = x.Rider.Guid,
                                                AgentDisplayName = x.Rider.DisplayName,
                                                AgentMobileNumber = x.Rider.MobileNumber,
                                                AgentIconUrl = x.Rider.IconStorage.Path,

                                                TotalItem = x.TotalItem,
                                                TotalAmount = x.TotalAmount,

                                                OrderDate = x.OpenDate,
                                                DeliveryDate = x.ActualDeliveryDate,
                                                CreateDate = x.CreateDate,

                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name
                                            })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region OutForDelivery Records
                    _OrderOverview.Ready = (from x in _HCoreContext.SCOrder
                                            where x.StatusId == HelperStatus.OrderStatus.OutForDelivery
                                            select new OOrder.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                OrderId = x.OrderId,

                                                CustomerId = x.Customer.Id,
                                                CustomerKey = x.Customer.Guid,
                                                CustomerDisplayName = x.Customer.DisplayName,
                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                DealerId = x.Dealer.Id,
                                                DealerKey = x.Dealer.Guid,
                                                DealerDisplayName = x.Dealer.DisplayName,
                                                DealerIconUrl = x.Dealer.IconStorage.Path,


                                                DealerLocationId = x.DealerLocation.Id,
                                                DealerLocationKey = x.DealerLocation.Guid,
                                                DealerLocationName = x.DealerLocation.DisplayName,
                                                DealerLocationAddress = x.DealerLocation.Address,
                                                DealerLocationRating = x.DealerLocationRating,

                                                AgentId = x.Rider.Id,
                                                AgentKey = x.Rider.Guid,
                                                AgentDisplayName = x.Rider.DisplayName,
                                                AgentMobileNumber = x.Rider.MobileNumber,
                                                AgentIconUrl = x.Rider.IconStorage.Path,

                                                TotalItem = x.TotalItem,
                                                TotalAmount = x.TotalAmount,

                                                OrderDate = x.OpenDate,
                                                DeliveryDate = x.ActualDeliveryDate,
                                                CreateDate = x.CreateDate,

                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name
                                            })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region DeliveryFailed Records
                    _OrderOverview.Ready = (from x in _HCoreContext.SCOrder
                                            where x.StatusId == HelperStatus.OrderStatus.DeliveryFailed
                                            select new OOrder.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                OrderId = x.OrderId,

                                                CustomerId = x.Customer.Id,
                                                CustomerKey = x.Customer.Guid,
                                                CustomerDisplayName = x.Customer.DisplayName,
                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                DealerId = x.Dealer.Id,
                                                DealerKey = x.Dealer.Guid,
                                                DealerDisplayName = x.Dealer.DisplayName,
                                                DealerIconUrl = x.Dealer.IconStorage.Path,


                                                DealerLocationId = x.DealerLocation.Id,
                                                DealerLocationKey = x.DealerLocation.Guid,
                                                DealerLocationName = x.DealerLocation.DisplayName,
                                                DealerLocationAddress = x.DealerLocation.Address,
                                                DealerLocationRating = x.DealerLocationRating,

                                                AgentId = x.Rider.Id,
                                                AgentKey = x.Rider.Guid,
                                                AgentDisplayName = x.Rider.DisplayName,
                                                AgentMobileNumber = x.Rider.MobileNumber,
                                                AgentIconUrl = x.Rider.IconStorage.Path,

                                                TotalItem = x.TotalItem,
                                                TotalAmount = x.TotalAmount,

                                                OrderDate = x.OpenDate,
                                                DeliveryDate = x.ActualDeliveryDate,
                                                CreateDate = x.CreateDate,

                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name
                                            })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region DeliveryFailed Records
                    _OrderOverview.Cancelled = (from x in _HCoreContext.SCOrder
                                            where x.StatusId == HelperStatus.OrderStatus.CancelledByUser || x.StatusId == HelperStatus.OrderStatus.CancelledBySeller || x.StatusId == HelperStatus.OrderStatus.CancelledBySystem
                                            select new OOrder.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                OrderId = x.OrderId,

                                                CustomerId = x.Customer.Id,
                                                CustomerKey = x.Customer.Guid,
                                                CustomerDisplayName = x.Customer.DisplayName,
                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                DealerId = x.Dealer.Id,
                                                DealerKey = x.Dealer.Guid,
                                                DealerDisplayName = x.Dealer.DisplayName,
                                                DealerIconUrl = x.Dealer.IconStorage.Path,


                                                DealerLocationId = x.DealerLocation.Id,
                                                DealerLocationKey = x.DealerLocation.Guid,
                                                DealerLocationName = x.DealerLocation.DisplayName,
                                                DealerLocationAddress = x.DealerLocation.Address,
                                                DealerLocationRating = x.DealerLocationRating,

                                                AgentId = x.Rider.Id,
                                                AgentKey = x.Rider.Guid,
                                                AgentDisplayName = x.Rider.DisplayName,
                                                AgentMobileNumber = x.Rider.MobileNumber,
                                                AgentIconUrl = x.Rider.IconStorage.Path,

                                                TotalItem = x.TotalItem,
                                                TotalAmount = x.TotalAmount,

                                                OrderDate = x.OpenDate,
                                                DeliveryDate = x.ActualDeliveryDate,
                                                CreateDate = x.CreateDate,

                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name
                                            })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region Delivered Records
                    _OrderOverview.Delivered = (from x in _HCoreContext.SCOrder
                                            where x.StatusId == HelperStatus.OrderStatus.Delivered
                                            select new OOrder.List
                                            {
                                                ReferenceId = x.Id,
                                                ReferenceKey = x.Guid,

                                                OrderId = x.OrderId,

                                                CustomerId = x.Customer.Id,
                                                CustomerKey = x.Customer.Guid,
                                                CustomerDisplayName = x.Customer.DisplayName,
                                                CustomerMobileNumber = x.Customer.MobileNumber,
                                                DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                DealerId = x.Dealer.Id,
                                                DealerKey = x.Dealer.Guid,
                                                DealerDisplayName = x.Dealer.DisplayName,
                                                DealerIconUrl = x.Dealer.IconStorage.Path,


                                                DealerLocationId = x.DealerLocation.Id,
                                                DealerLocationKey = x.DealerLocation.Guid,
                                                DealerLocationName = x.DealerLocation.DisplayName,
                                                DealerLocationAddress = x.DealerLocation.Address,
                                                DealerLocationRating = x.DealerLocationRating,

                                                AgentId = x.Rider.Id,
                                                AgentKey = x.Rider.Guid,
                                                AgentDisplayName = x.Rider.DisplayName,
                                                AgentMobileNumber = x.Rider.MobileNumber,
                                                AgentIconUrl = x.Rider.IconStorage.Path,

                                                TotalItem = x.TotalItem,
                                                TotalAmount = x.TotalAmount,

                                                OrderDate = x.OpenDate,
                                                DeliveryDate = x.ActualDeliveryDate,
                                                CreateDate = x.CreateDate,

                                                StatusId = x.StatusId,
                                                StatusCode = x.Status.SystemName,
                                                StatusName = x.Status.Name
                                            })
                                             .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    _HCoreContext.Dispose();
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OrderOverview, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetOrders", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant orders.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantOrders(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.SCOrder
                                             where x.StatusId != HCoreConstant.HelperStatus.OrderStatus.New
                                             select new OOrder.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 OrderId = x.OrderId,


                                                 CustomerId = x.Customer.Id,
                                                 CustomerKey = x.Customer.Guid,
                                                 CustomerDisplayName = x.Customer.DisplayName,
                                                 CustomerMobileNumber = x.Customer.MobileNumber,
                                                 DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                 DealerId = x.Dealer.Id,
                                                 DealerKey = x.Dealer.Guid,
                                                 DealerDisplayName = x.Dealer.DisplayName,
                                                 DealerIconUrl = x.Dealer.IconStorage.Path,


                                                 DealerLocationId = x.DealerLocation.Id,
                                                 DealerLocationKey = x.DealerLocation.Guid,
                                                 DealerLocationName = x.DealerLocation.DisplayName,
                                                 DealerLocationAddress = x.DealerLocation.Address,
                                                 DealerLocationRating = x.DealerLocationRating,

                                                 AgentId = x.Rider.Id,
                                                 AgentKey = x.Rider.Guid,
                                                 AgentDisplayName = x.Rider.DisplayName,
                                                 AgentMobileNumber = x.Rider.MobileNumber,
                                                 AgentIconUrl = x.Rider.IconStorage.Path,

                                                 TotalItem = x.TotalItem,
                                                 TotalAmount = (x.TotalAmount - x.DeliveryCharge),

                                                 OrderDate = x.OpenDate,
                                                 DeliveryDate = x.ActualDeliveryDate,
                                                 CreateDate = x.CreateDate,

                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name
                                             })
                                                    .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion

                    #region Get Data
                    List<OOrder.List> Data = (from x in _HCoreContext.SCOrder
                                              where x.StatusId != HCoreConstant.HelperStatus.OrderStatus.New
                                              select new OOrder.List
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  OrderId = x.OrderId,


                                                  CustomerId = x.Customer.Id,
                                                  CustomerKey = x.Customer.Guid,
                                                  CustomerDisplayName = x.Customer.DisplayName,
                                                  CustomerMobileNumber = x.Customer.MobileNumber,
                                                  DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                  DealerId = x.Dealer.Id,
                                                  DealerKey = x.Dealer.Guid,
                                                  DealerDisplayName = x.Dealer.DisplayName,
                                                  DealerIconUrl = x.Dealer.IconStorage.Path,


                                                  DealerLocationId = x.DealerLocation.Id,
                                                  DealerLocationKey = x.DealerLocation.Guid,
                                                  DealerLocationName = x.DealerLocation.DisplayName,
                                                  DealerLocationAddress = x.DealerLocation.Address,
                                                  DealerLocationRating = x.DealerLocationRating,

                                                  AgentId = x.Rider.Id,
                                                  AgentKey = x.Rider.Guid,
                                                  AgentDisplayName = x.Rider.DisplayName,
                                                  AgentMobileNumber = x.Rider.MobileNumber,
                                                  AgentIconUrl = x.Rider.IconStorage.Path,

                                                  TotalItem = x.TotalItem,
                                                  TotalAmount = (x.TotalAmount - x.DeliveryCharge),

                                                  OrderDate = x.OpenDate,
                                                  DeliveryDate = x.ActualDeliveryDate,
                                                  CreateDate = x.CreateDate,

                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name
                                              })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.DealerIconUrl))
                        {
                            DataItem.DealerIconUrl = _AppConfig.StorageUrl + DataItem.DealerIconUrl;
                        }
                        else
                        {
                            DataItem.DealerIconUrl = _AppConfig.Default_Icon;
                        }

                        if (!string.IsNullOrEmpty(DataItem.AgentIconUrl))
                        {
                            DataItem.AgentIconUrl = _AppConfig.StorageUrl + DataItem.AgentIconUrl;
                        }
                        else
                        {
                            DataItem.AgentIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetMerchantOrders", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the merchant order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetMerchantOrder(OOrder.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    OOrder.Details _Details = (from x in _HCoreContext.SCOrder
                                               where x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey
                                               select new OOrder.Details
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,
                                                   EPTime = x.EPTime,

                                                   OrderId = x.OrderId,

                                                   OpenDate = x.OpenDate,
                                                   CloseDate = x.CloseDate,


                                                   CustomerId = x.Customer.Id,
                                                   CustomerKey = x.Customer.Guid,
                                                   CustomerDisplayName = x.Customer.DisplayName,
                                                   CustomerMobileNumber = x.Customer.MobileNumber,
                                                   CustomerEmailAddress = x.Customer.EmailAddress,
                                                   CustomerIconUrl = x.Customer.IconStorage.Path,

                                                   NinjaAmount = x.RiderAmount,

                                                   TotalItem = x.TotalItem,
                                                   Amount = x.Amount,
                                                   Charge = x.Charge,
                                                   OtherCharge = x.OtherCharge,
                                                   DeliveryCharge = x.DeliveryCharge,
                                                   DiscountAmount = x.DiscountAmount,
                                                   RewardAmount = x.RewardAmount,
                                                   TotalAmount = (x.TotalAmount - x.DeliveryCharge),


                                                   PaymentModeCode = x.PaymentMode.SystemName,
                                                   PaymentModeName = x.PaymentMode.Name,

                                                   PaymentStatusCode = x.PaymentStatus.SystemName,
                                                   PaymentStatusName = x.PaymentStatus.Name,

                                                   PaymentReference = x.PaymentReference,

                                                   DealerId = x.Dealer.Id,
                                                   DealerKey = x.Dealer.Guid,
                                                   DealerDisplayName = x.Dealer.DisplayName,
                                                   DealerIconUrl = x.Dealer.IconStorage.Path,
                                                   DealerContactNumber = x.Dealer.ContactNumber,

                                                   DealerLocationId = x.DealerLocation.Id,
                                                   DealerLocationKey = x.DealerLocation.Guid,
                                                   DealerLocationName = x.DealerLocation.DisplayName,
                                                   DealerLocationAddress = x.DealerLocation.Address,
                                                   DealerLocationLatitude = x.DealerLocation.Latitude,
                                                   DealerLocationLongitude = x.DealerLocation.Longitude,
                                                   DealerLocationContactNumber = x.DealerLocation.ContactNumber,

                                                   AgentId = x.Rider.Id,
                                                   AgentKey = x.Rider.Guid,
                                                   AgentDisplayName = x.Rider.DisplayName,
                                                   AgentMobileNumber = x.Rider.MobileNumber,
                                                   AgentIconUrl = x.Rider.IconStorage.Path,



                                                   ExpectedDeliveryDate = x.ExpectedDeliveryDate,
                                                   ActualDeliveryDate = x.ActualDeliveryDate,

                                                   DeliveryTypeCode = x.DeliveryType.SystemName,
                                                   DeliveryTypeName = x.DeliveryType.Name,

                                                   CustomerRating = x.CustomerRating,
                                                   CustomerReview = x.CustomerReview,

                                                   CreateDate = x.CreateDate,
                                                   CreatedByKey = x.CreatedBy.Guid,
                                                   CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                   ModifyDate = x.ModifyDate,
                                                   ModifyByKey = x.ModifyBy.Guid,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                   StatusId = x.StatusId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name
                                               }).FirstOrDefault();

                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.AgentIconUrl))
                        {
                            _Details.AgentIconUrl = _AppConfig.StorageUrl + _Details.AgentIconUrl;
                        }
                        else
                        {
                            _Details.AgentIconUrl = _AppConfig.Default_Icon;
                        }


                        if (!string.IsNullOrEmpty(_Details.CustomerIconUrl))
                        {
                            _Details.CustomerIconUrl = _AppConfig.StorageUrl + _Details.CustomerIconUrl;
                        }
                        else
                        {
                            _Details.CustomerIconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.DealerIconUrl))
                        {
                            _Details.DealerIconUrl = _AppConfig.StorageUrl + _Details.DealerIconUrl;
                        }
                        else
                        {
                            _Details.DealerIconUrl = _AppConfig.Default_Icon;
                        }
                        _Details.OrderActivity = _HCoreContext.SCOrderActivity.Where(x => x.OrderId == _Details.ReferenceId)
                            .Select(x => new OOrder.OrderActivity
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                OrderItemId = x.OrderItem.Id,
                                OrderItemKey = x.OrderItem.Guid,

                                Title = x.Title,
                                Description = x.Description,
                                SystemDescription = x.SystemDescription,
                                Comment = x.Comment,

                                CreateDate = x.CreateDate,
                                CreatedById = x.CreatedById,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,
                            }).OrderByDescending(x => x.CreateDate).ToList();
                        _Details.OrderItems = _HCoreContext.SCOrderItem.Where(x => x.OrderId == _Details.ReferenceId)
                            .Select(x => new OOrder.OrderItem
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                ProductId = x.Varient.ProductId,
                                ProductKey = x.Varient.Product.Guid,
                                ProductName = x.Varient.Product.Name,

                                VarientId = x.VarientId,
                                VarientKey = x.Varient.Guid,
                                VarientName = x.Varient.Name,
                                VarientIconUrl = x.Varient.Product.PrimaryStorage.Path,

                                UnitPrice = x.UnitPrice,
                                Quantity = x.Quantity,
                                TotalAmount = x.TotalAmount,

                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name

                            }).ToList();
                        foreach (var item in _Details.OrderItems)
                        {
                            if (!string.IsNullOrEmpty(item.VarientIconUrl))
                            {
                                item.VarientIconUrl = _AppConfig.StorageUrl + item.VarientIconUrl;
                            }
                            else
                            {
                                item.VarientIconUrl = _AppConfig.Default_Icon;
                            }

                        }

                        _Details.BillingAddress = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == _Details.ReferenceId && x.TypeId == 505)
                            .Select(x => new OOrder.OrderAddress
                            {
                                ReferenceId = x.Id,
                                ReferenceKey = x.Guid,

                                Name = x.Name,
                                MobileNumber = x.MobileNumber,
                                AlternateMobileNumber = x.AlternateMobileNumber,
                                EmailAddress = x.EmailAddress,
                                AddressLine1 = x.AddressLine1,
                                AddressLine2 = x.AddressLine2,
                                LandMark = x.Landmark,
                                Latitude = x.Latitude,
                                Longitude = x.Longitude,
                                MapAddress = x.MapAddress,


                                AreaId = x.AreaId,
                                AreaName = x.Area.Name,
                                CityId = x.CityId,
                                CityName = x.City.Name,
                                StateId = x.StateId,
                                StateName = x.State.Name,
                                CountryId = x.CountryId,
                                CountryName = x.Country.Name,
                                ZipCode = x.ZipCode,
                                Instruction = x.Instructions,

                                CreateDate = x.CreateDate,
                                CreatedByKey = x.CreatedBy.Guid,
                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                ModifyDate = x.ModifyDate,
                                ModifyByKey = x.ModifyBy.Guid,
                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name

                            }).FirstOrDefault();

                        _Details.ShippingAddress = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == _Details.ReferenceId && x.TypeId == 506)
                           .Select(x => new OOrder.OrderAddress
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,

                               Name = x.Name,
                               MobileNumber = x.MobileNumber,
                               AlternateMobileNumber = x.AlternateMobileNumber,
                               EmailAddress = x.EmailAddress,
                               AddressLine1 = x.AddressLine1,
                               AddressLine2 = x.AddressLine2,
                               LandMark = x.Landmark,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                               MapAddress = x.MapAddress,

                               AreaId = x.AreaId,
                               AreaName = x.Area.Name,
                               CityId = x.CityId,
                               CityName = x.City.Name,
                               StateId = x.StateId,
                               StateName = x.State.Name,
                               CountryId = x.CountryId,
                               CountryName = x.Country.Name,
                               ZipCode = x.ZipCode,
                               Instruction = x.Instructions,

                               CreateDate = x.CreateDate,
                               CreatedByKey = x.CreatedBy.Guid,
                               CreatedByDisplayName = x.CreatedBy.DisplayName,

                               ModifyDate = x.ModifyDate,
                               ModifyByKey = x.ModifyBy.Guid,
                               ModifyByDisplayName = x.ModifyBy.DisplayName,


                               StatusId = x.StatusId,
                               StatusCode = x.Status.SystemName,
                               StatusName = x.Status.Name

                           }).FirstOrDefault();

                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Details, ResourceItems.HC101, ResourceItems.HC101);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetOrders", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the order activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetOrderActivity(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.SCOrderActivity
                                        where x.StatusId == HelperStatus.Default.Active
                                        select new OOrder.OrderActivity
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,

                                            OrderId = x.OrderId,
                                            OrderKey = x.Order.Guid,

                                            OrderItemId = x.OrderItemId,
                                            OrderItemKey = x.OrderItem.Guid,

                                            Title = x.Title,
                                            Description = x.Description,

                                            SystemDescription = x.SystemDescription,
                                            Comment = x.Comment,

                                            OrderStatusId = x.OrderStatusId,
                                            OrderStatusCode = x.OrderStatus.SystemName,
                                            OrderStatusName = x.OrderStatus.Name,

                                            CreateDate = x.CreateDate,
                                            CreatedByKey = x.CreatedBy.Guid,
                                            CreatedByDisplayName = x.CreatedBy.DisplayName,
                                        })
                                                    .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OOrder.OrderActivity> Data = (from x in _HCoreContext.SCOrderActivity
                                                       select new OOrder.OrderActivity
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,

                                                           OrderId = x.OrderId,
                                                           OrderKey = x.Order.Guid,

                                                           OrderItemId = x.OrderItemId,
                                                           OrderItemKey = x.OrderItem.Guid,

                                                           Title = x.Title,
                                                           Description = x.Description,

                                                           SystemDescription = x.SystemDescription,
                                                           Comment = x.Comment,

                                                           OrderStatusId = x.OrderStatusId,
                                                           OrderStatusCode = x.OrderStatus.SystemName,
                                                           OrderStatusName = x.OrderStatus.Name,

                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetOrderActivity", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the ninja order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNinjaOrder(OOrderManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Code Block  
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    var OrderDetails = _HCoreContext.SCOrder
                            .Where(x => x.Id == _Request.OrderId && x.Guid == _Request.OrderKey)
                            .Select(x => new ONinjaOrderDetails.Order
                            {
                                OrderId = x.Id,
                                OrderReference = x.Guid,
                                OrderNumber = x.OrderId,
                                OpenDate = x.OpenDate,
                                EPTime = x.EPTime,

                                CustomerId = x.CustomerId,
                                CustomerKey = x.Customer.Guid,
                                CustomerDisplayName = x.Customer.DisplayName,
                                CustomerEmailAddress = x.Customer.EmailAddress,
                                CustomerMobileNumber = x.Customer.MobileNumber,

                                TotalItem = x.TotalItem,
                                TotalAmount = x.TotalAmount,
                                RiderAmount = x.RiderAmount,
                                RiderCommissionAmount = x.RiderCommissionAmount,
                                RiderTotalAmount = x.RiderTotalAmount,

                                DealerId = x.DealerId,
                                DealerKey = x.Dealer.Guid,
                                DealerName = x.Dealer.DisplayName,
                                DealerIconUrl = x.Dealer.IconStorage.Path,

                                DealerLocationId = x.DealerLocationId,
                                DealerLocationKey = x.DealerLocation.Guid,
                                DealerLocationName = x.DealerLocation.Name,
                                DealerLocationAddress = x.DealerLocation.Address,
                                DealerLocationLatitude = x.DealerLocation.Latitude,
                                DealerLocationLongitude = x.DealerLocation.Longitude,

                                ExpectedDeliveryDate = x.ExpectedDeliveryDate,
                                ActualDeliveryDate = x.ActualDeliveryDate,

                                AgentId = x.RiderId,
                                AgentKey = x.Rider.Guid,

                                StatusId = x.StatusId,
                                StatusCode = x.Status.SystemName,
                                StatusName = x.Status.Name,
                            })
                            .FirstOrDefault();
                    if (OrderDetails != null)
                    {
                        if (!string.IsNullOrEmpty(OrderDetails.DealerIconUrl))
                        {
                            OrderDetails.DealerIconUrl = _AppConfig.StorageUrl + OrderDetails.DealerIconUrl;
                        }
                        else
                        {
                            OrderDetails.DealerIconUrl = OrderDetails.DealerIconUrl;
                        }
                        OrderDetails.Address = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == OrderDetails.OrderId)
                                                .Select(x => new ONinjaOrderDetails.OrderAddress
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    MobileNumber = x.MobileNumber,
                                                    EmailAddress = x.EmailAddress,
                                                    AddressLine1 = x.AddressLine1,
                                                    AddressLine2 = x.AddressLine2,
                                                    Landmark = x.Landmark,
                                                    AlternateMobileNumber = x.AlternateMobileNumber,
                                                    ZipCode = x.ZipCode,
                                                    CityName = x.City.Name,
                                                    AreaName = x.Area.Name,
                                                    Latitude = x.Latitude,
                                                    Longitude = x.Longitude,
                                                }).FirstOrDefault();
                        OrderDetails.OrderActivity = _HCoreContext.SCOrderActivity.Where(x => x.OrderId == OrderDetails.OrderId)
                           .Select(x => new ONinjaOrderDetails.OrderActivity
                           {
                               ReferenceId = x.Id,
                               ReferenceKey = x.Guid,

                               OrderItemId = x.OrderItem.Id,
                               OrderItemKey = x.OrderItem.Guid,

                               Title = x.Title,
                               Description = x.Description,
                               SystemDescription = x.SystemDescription,
                               Comment = x.Comment,

                               CreateDate = x.CreateDate,
                               CreatedById = x.CreatedById,
                               CreatedByKey = x.CreatedBy.Guid,
                               CreatedByDisplayName = x.CreatedBy.DisplayName,
                           }).OrderByDescending(x => x.CreateDate).ToList();
                    }
                    if (OrderDetails != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, OrderDetails, "HCP200", "Order details loaded");
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP400", "Order details not found");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("GetOrder", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the ninja order summary.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNinjaOrderSummary(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    string MobileNumber = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.UserReference.AccountId).Select(x => x.MobileNumber).FirstOrDefault();
                    bool AccCheck = _HCoreContext.TUCNinjaRegistration.Any(x => x.MobileNumber == MobileNumber && x.StatusId == 555);
                    if (AccCheck)
                    {
                        //long TotalOrders = _HCoreContext.SCOrder.Count(x => x.RiderId == _Request.UserReference.AccountId);
                        double? RiderAmount = _HCoreContext.SCOrder.Where(x => x.RiderId == _Request.UserReference.AccountId && x.StatusId == HelperStatus.OrderStatus.Delivered).Sum(x => x.RiderAmount);
                        double? CommissionAmount = _HCoreContext.SCOrder.Where(x => x.RiderId == _Request.UserReference.AccountId && x.StatusId == HelperStatus.OrderStatus.Delivered).Sum(x => x.RiderCommissionAmount);
                        double? TotalAmount = _HCoreContext.SCOrder.Where(x => x.RiderId == _Request.UserReference.AccountId && x.StatusId == HelperStatus.OrderStatus.Delivered).Sum(x => x.RiderTotalAmount);

                        long DeliveredOrders = _HCoreContext.SCOrder.Count(x => x.RiderId == _Request.UserReference.AccountId && x.StatusId == HelperStatus.OrderStatus.Delivered);
                        long ActiveOrders = _HCoreContext.SCOrder.Count(x => x.RiderId == _Request.UserReference.AccountId && (x.StatusId == HelperStatus.OrderStatus.Ready || x.StatusId == HelperStatus.OrderStatus.Preparing || x.StatusId == HelperStatus.OrderStatus.Confirmed || x.StatusId == HelperStatus.OrderStatus.OutForDelivery || x.StatusId == HelperStatus.OrderStatus.ReadyToPickup || x.StatusId == HelperStatus.OrderStatus.DeliveryFailed));
                        long AvailableOrders = _HCoreContext.SCOrder.Count(x => x.RiderId == null && (x.StatusId == HelperStatus.OrderStatus.PendingConfirmation));
                        var Summary = new
                        {
                            //TotalOrders = TotalOrders,
                            DeliveredOrders = DeliveredOrders,
                            ActiveOrders = ActiveOrders,
                            AvailableOrders = AvailableOrders,

                            Amount = RiderAmount,
                            CommissionAmount = CommissionAmount,
                            TotalAmount = TotalAmount,


                            IsNinjaAvailable = true,
                        };
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, Summary, "HC0001");
                        #endregion
                    }
                    else
                    {
                        var Summary = new
                        {
                            DeliveredOrders = 0,
                            CommissionAmount = 0,
                            ActiveOrders = 0,
                            AvailableOrders = 0,
                            IsNinjaAvailable = false,
                        };
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, Summary, "HC0001");
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetNinjaOrderSummary", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the ninja orders.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNinjaOrders(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Type == "available")
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.SCOrder
                                                 where (x.StatusId == HelperStatus.OrderStatus.PendingConfirmation)
                                                 && x.RiderId == null
                                                 select new OOrder.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     OrderId = x.OrderId,


                                                     CustomerId = x.Customer.Id,
                                                     CustomerKey = x.Customer.Guid,
                                                     CustomerDisplayName = x.Customer.DisplayName,
                                                     CustomerMobileNumber = x.Customer.MobileNumber,


                                                     DealerId = x.Dealer.Id,
                                                     DealerKey = x.Dealer.Guid,
                                                     DealerDisplayName = x.Dealer.DisplayName,

                                                     DealerLocationId = x.DealerLocation.Id,
                                                     DealerLocationKey = x.DealerLocation.Guid,
                                                     DealerLocationName = x.DealerLocation.DisplayName,
                                                     DealerLocationAddress = x.DealerLocation.Address,

                                                     TotalItem = x.TotalItem,
                                                     TotalAmount = x.TotalAmount,

                                                     RiderAmount = x.RiderAmount,
                                                     RiderCommissionAmount = x.RiderCommissionAmount,
                                                     RiderTotalAmount = x.RiderTotalAmount,

                                                     DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                     OrderDate = x.OpenDate,
                                                     DeliveryDate = x.ActualDeliveryDate,
                                                     CreateDate = x.CreateDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                        .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                        #region Get Data
                        List<OOrder.List> Data = (from x in _HCoreContext.SCOrder
                                                  where x.StatusId == HelperStatus.OrderStatus.PendingConfirmation
                                                  && x.RiderId == null
                                                  select new OOrder.List
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      OrderId = x.OrderId,

                                                      CustomerId = x.Customer.Id,
                                                      CustomerKey = x.Customer.Guid,
                                                      CustomerDisplayName = x.Customer.DisplayName,
                                                      CustomerMobileNumber = x.Customer.MobileNumber,


                                                      DealerId = x.Dealer.Id,
                                                      DealerKey = x.Dealer.Guid,
                                                      DealerDisplayName = x.Dealer.DisplayName,

                                                      DealerLocationId = x.DealerLocation.Id,
                                                      DealerLocationKey = x.DealerLocation.Guid,
                                                      DealerLocationName = x.DealerLocation.DisplayName,
                                                      DealerLocationAddress = x.DealerLocation.Address,

                                                      TotalItem = x.TotalItem,
                                                      TotalAmount = x.TotalAmount,

                                                      RiderAmount = x.RiderAmount,
                                                      RiderCommissionAmount = x.RiderCommissionAmount,
                                                      RiderTotalAmount = x.RiderTotalAmount,
                                                      DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                      OrderDate = x.OpenDate,
                                                      DeliveryDate = x.ActualDeliveryDate,
                                                      CreateDate = x.CreateDate,

                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var DataItem in Data)
                        {
                            DataItem.StatusId = HelperStatus.OrderStatus.Ready;
                            DataItem.StatusCode = "orderstatus.ready";
                            DataItem.StatusName = "Pending Confirmation";
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else if (_Request.Type == "delivered")
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.SCOrder
                                                 where x.StatusId == HelperStatus.OrderStatus.Delivered
                                                 && x.RiderId == _Request.UserReference.AccountId
                                                 select new OOrder.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     OrderId = x.OrderId,


                                                     CustomerId = x.Customer.Id,
                                                     CustomerKey = x.Customer.Guid,
                                                     CustomerDisplayName = x.Customer.DisplayName,
                                                     CustomerMobileNumber = x.Customer.MobileNumber,


                                                     RiderAmount = x.RiderAmount,
                                                     RiderCommissionAmount = x.RiderCommissionAmount,
                                                     RiderTotalAmount = x.RiderTotalAmount,
                                                     DealerId = x.Dealer.Id,
                                                     DealerKey = x.Dealer.Guid,
                                                     DealerDisplayName = x.Dealer.DisplayName,

                                                     DealerLocationId = x.DealerLocation.Id,
                                                     DealerLocationKey = x.DealerLocation.Guid,
                                                     DealerLocationName = x.DealerLocation.DisplayName,
                                                     DealerLocationAddress = x.DealerLocation.Address,

                                                     TotalItem = x.TotalItem,
                                                     TotalAmount = x.TotalAmount,
                                                     DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                     OrderDate = x.OpenDate,
                                                     DeliveryDate = x.ActualDeliveryDate,
                                                     CreateDate = x.CreateDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                        .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion

                        #region Get Data
                        List<OOrder.List> Data = (from x in _HCoreContext.SCOrder
                                                  where x.StatusId == HelperStatus.OrderStatus.Delivered
                                                 && x.RiderId == _Request.UserReference.AccountId
                                                  select new OOrder.List
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      OrderId = x.OrderId,

                                                      CustomerId = x.Customer.Id,
                                                      CustomerKey = x.Customer.Guid,
                                                      CustomerDisplayName = x.Customer.DisplayName,
                                                      CustomerMobileNumber = x.Customer.MobileNumber,

                                                      RiderAmount = x.RiderAmount,
                                                      RiderCommissionAmount = x.RiderCommissionAmount,
                                                      RiderTotalAmount = x.RiderTotalAmount,

                                                      DealerId = x.Dealer.Id,
                                                      DealerKey = x.Dealer.Guid,
                                                      DealerDisplayName = x.Dealer.DisplayName,

                                                      DealerLocationId = x.DealerLocation.Id,
                                                      DealerLocationKey = x.DealerLocation.Guid,
                                                      DealerLocationName = x.DealerLocation.DisplayName,
                                                      DealerLocationAddress = x.DealerLocation.Address,

                                                      TotalItem = x.TotalItem,
                                                      TotalAmount = x.TotalAmount,

                                                      DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                      OrderDate = x.OpenDate,
                                                      DeliveryDate = x.ActualDeliveryDate,
                                                      CreateDate = x.CreateDate,

                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else if (_Request.Type == "active")
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.SCOrder
                                                 where (x.StatusId == HelperStatus.OrderStatus.Confirmed || x.StatusId == HelperStatus.OrderStatus.Preparing || x.StatusId == HelperStatus.OrderStatus.Ready || x.StatusId == HelperStatus.OrderStatus.ReadyToPickup || x.StatusId == HelperStatus.OrderStatus.OutForDelivery || x.StatusId == HelperStatus.OrderStatus.DeliveryFailed)
                                                 && x.RiderId == _Request.UserReference.AccountId
                                                 select new OOrder.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     OrderId = x.OrderId,


                                                     CustomerId = x.Customer.Id,
                                                     CustomerKey = x.Customer.Guid,
                                                     CustomerDisplayName = x.Customer.DisplayName,
                                                     CustomerMobileNumber = x.Customer.MobileNumber,

                                                     RiderAmount = x.RiderAmount,
                                                     RiderCommissionAmount = x.RiderCommissionAmount,
                                                     RiderTotalAmount = x.RiderTotalAmount,

                                                     DealerId = x.Dealer.Id,
                                                     DealerKey = x.Dealer.Guid,
                                                     DealerDisplayName = x.Dealer.DisplayName,

                                                     DealerLocationId = x.DealerLocation.Id,
                                                     DealerLocationKey = x.DealerLocation.Guid,
                                                     DealerLocationName = x.DealerLocation.DisplayName,
                                                     DealerLocationAddress = x.DealerLocation.Address,

                                                     TotalItem = x.TotalItem,
                                                     TotalAmount = x.TotalAmount,
                                                     DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,
                                                     RiderRating = x.RiderRating,
                                                     OrderDate = x.OpenDate,
                                                     DeliveryDate = x.ActualDeliveryDate,
                                                     CreateDate = x.CreateDate,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                        .Where(_Request.SearchCondition)
                                                .Count();
                        #endregion
                        #region Get Data
                        List<OOrder.List> Data = (from x in _HCoreContext.SCOrder
                                                  where (x.StatusId == HelperStatus.OrderStatus.Confirmed || x.StatusId == HelperStatus.OrderStatus.Preparing || x.StatusId == HelperStatus.OrderStatus.Ready || x.StatusId == HelperStatus.OrderStatus.ReadyToPickup || x.StatusId == HelperStatus.OrderStatus.OutForDelivery || x.StatusId == HelperStatus.OrderStatus.DeliveryFailed)
                                                 && x.RiderId == _Request.UserReference.AccountId
                                                  select new OOrder.List
                                                  {
                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      OrderId = x.OrderId,

                                                      CustomerId = x.Customer.Id,
                                                      CustomerKey = x.Customer.Guid,
                                                      CustomerDisplayName = x.Customer.DisplayName,
                                                      CustomerMobileNumber = x.Customer.MobileNumber,

                                                      RiderAmount = x.RiderAmount,
                                                      RiderCommissionAmount = x.RiderCommissionAmount,
                                                      RiderTotalAmount = x.RiderTotalAmount,

                                                      DealerId = x.Dealer.Id,
                                                      DealerKey = x.Dealer.Guid,
                                                      DealerDisplayName = x.Dealer.DisplayName,
                                                      RiderRating = x.RiderRating,

                                                      DealerLocationId = x.DealerLocation.Id,
                                                      DealerLocationKey = x.DealerLocation.Guid,
                                                      DealerLocationName = x.DealerLocation.DisplayName,
                                                      DealerLocationAddress = x.DealerLocation.Address,

                                                      TotalItem = x.TotalItem,
                                                      TotalAmount = x.TotalAmount,

                                                      DeliveryAddress = x.SCOrderAddress.FirstOrDefault().AddressLine1,

                                                      OrderDate = x.OpenDate,
                                                      DeliveryDate = x.ActualDeliveryDate,
                                                      CreateDate = x.CreateDate,

                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, null, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetOrders", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
