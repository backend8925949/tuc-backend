//==================================================================================
// FileName: FrameworkCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to category
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;
namespace HCore.Product.Framework
{
    public class FrameworkCategory
    {
        HCoreContext _HCoreContext;
        SCCategory _SCCategory;

        /// <summary>
        /// Description: Saves the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC141, ResourceItems.HCC141M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC142, ResourceItems.HCC142M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                }

                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool ProductCategoryDetails = _HCoreContext.SCCategory
                                                        .Any(x =>
                                                        x.ParentId == null
                                                        && x.SystemName == SystemName);
                    if (!ProductCategoryDetails)
                    {
                        _SCCategory = new SCCategory();
                        _SCCategory.Guid = HCoreHelper.GenerateGuid();
                        _SCCategory.Name = _Request.Name;
                        _SCCategory.PlatformCharge = _Request.PlatformCharge;
                        _SCCategory.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        _SCCategory.Description = _Request.Description;
                        _SCCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                        _SCCategory.CreatedById = _Request.UserReference.AccountId;
                        _SCCategory.StatusId = (int)StatusId;
                        _HCoreContext.SCCategory.Add(_SCCategory);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _SCCategory.Id,
                            ReferenceKey = _SCCategory.Guid,
                        };
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.SCCategory.Where(x => x.Guid == _SCCategory.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.PrimaryStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId !=null)
                                    {
                                        PrDetails.SecondaryStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC145, ResourceItems.HCC145M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC146, ResourceItems.HCC146M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                        }
                    }
                    var ProductCategoryDetails = _HCoreContext.SCCategory
                                                                 .Where(x => x.Id == _Request.ReferenceId
                                                                 && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductCategoryDetails != null)
                    {

                        if (!string.IsNullOrEmpty(_Request.Name) && ProductCategoryDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool NameCheck = _HCoreContext.SCCategory
                                                        .Any(x =>
                                                        x.ParentId == null
                                                        && x.SystemName == SystemName);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC147, ResourceItems.HCC147M);
                            }
                            ProductCategoryDetails.Name = _Request.Name;
                            ProductCategoryDetails.SystemName = SystemName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && ProductCategoryDetails.Description != _Request.Description)
                        {
                            ProductCategoryDetails.Description = _Request.Description;
                        }
                        if (ProductCategoryDetails.PlatformCharge != _Request.PlatformCharge)
                        {
                            ProductCategoryDetails.PlatformCharge = _Request.PlatformCharge;
                        }
                        ProductCategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ProductCategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && ProductCategoryDetails.StatusId != StatusId)
                        {
                            ProductCategoryDetails.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, ProductCategoryDetails.PrimaryStorageId, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, ProductCategoryDetails.SecondaryStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.SCCategory.Where(x => x.Guid == _SCCategory.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.PrimaryStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.SecondaryStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                                else
                                {
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }

                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC148, ResourceItems.HCC148M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateCategory", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Deletes the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    SCCategory Details = _HCoreContext.SCCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        bool ProductCheck = _HCoreContext.SCProduct.Any(x => x.CategoryId == Details.Id );
                        if (ProductCheck )
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC149, ResourceItems.HCC149M);
                        }
                        _HCoreContext.SCCategory.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, ResourceItems.HC201, ResourceItems.HC201M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OCategory.Details _Details = (from x in _HCoreContext.SCCategory
                                                  where x.Guid == _Request.ReferenceKey
                                                  && x.Id == _Request.ReferenceId
                                                  && x.ParentId == null
                                                  select new OCategory.Details
                                                  {

                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      Name = x.Name,
                                                      SystemName = x.SystemName,
                                                      Description = x.Description,

                                                      PlatformCharge = x.PlatformCharge,

                                                      IconUrl = x.PrimaryStorage.Path,
                                                      PosterUrl = x.SecondaryStorage.Path,

                                                      CreateDate = x.CreateDate,
                                                      CreatedByKey = x.CreatedBy.Guid,
                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                      ModifyDate = x.ModifyDate,
                                                      ModifyByKey = x.ModifyBy.Guid,
                                                      ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, ResourceItems.HC200, ResourceItems.HC200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.SCCategory
                                                 where x.ParentId == null
                                                 select new OCategory.Details
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     Name = x.Name,
                                                     SystemName = x.SystemName,
                                                     Description = x.Description,

                                                     IconUrl = x.PrimaryStorage.Path,
                                                     PosterUrl = x.SecondaryStorage.Path,
                                                     PlatformCharge = x.PlatformCharge,

                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     ModifyDate = x.ModifyDate,
                                                     ModifyByKey = x.ModifyBy.Guid,
                                                     ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                                                .Where(_Request.SearchCondition)
                                                        .Count();
                    }
                    #endregion
                    #region Get Data
                    List<OCategory.Details> Data = (from x in _HCoreContext.SCCategory
                                                 where x.ParentId == null
                                                    select new OCategory.Details
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Name = x.Name,
                                                        SystemName = x.SystemName,
                                                        Description = x.Description,

                                                        IconUrl = x.PrimaryStorage.Path,
                                                        PosterUrl = x.SecondaryStorage.Path,
                                                        PlatformCharge = x.PlatformCharge,

                                                        CreateDate = x.CreateDate,
                                                        CreatedByKey = x.CreatedBy.Guid,
                                                        CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                        ModifyDate = x.ModifyDate,
                                                        ModifyByKey = x.ModifyBy.Guid,
                                                        ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                        StatusId = x.StatusId,
                                                        StatusCode = x.Status.SystemName,
                                                        StatusName = x.Status.Name
                                                    })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                    }
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Saves the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC151, ResourceItems.HCC151M);
                }
                if (_Request.CategoryId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC150, ResourceItems.HCC150M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC141, ResourceItems.HCC141M);
                }
                if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC142, ResourceItems.HCC142M);
                }
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                }

                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    bool CategoryCheck = _HCoreContext.SCCategory.Any(x => x.Id == _Request.CategoryId && x.Guid == _Request.CategoryKey);
                    if (!CategoryCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC152, ResourceItems.HCC152M);
                    }

                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                    bool ProductCategoryDetails = _HCoreContext.SCCategory
                                                        .Any(x =>
                                                        x.ParentId == _Request.CategoryId
                                                        && x.SystemName == SystemName);
                    if (!ProductCategoryDetails)
                    {
                        _SCCategory = new SCCategory();
                        _SCCategory.Guid = HCoreHelper.GenerateGuid();
                        _SCCategory.ParentId = _Request.CategoryId;
                        _SCCategory.Name = _Request.Name;
                        _SCCategory.PlatformCharge = _Request.PlatformCharge;
                        _SCCategory.SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        _SCCategory.Description = _Request.Description;
                        _SCCategory.CreateDate = HCoreHelper.GetGMTDateTime();
                        _SCCategory.CreatedById = _Request.UserReference.AccountId;
                        _SCCategory.StatusId = (int)StatusId;
                        _HCoreContext.SCCategory.Add(_SCCategory);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _SCCategory.Id,
                            ReferenceKey = _SCCategory.Guid,
                        };
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, null, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.SCCategory.Where(x => x.Guid == _SCCategory.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.PrimaryStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.SecondaryStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC145, ResourceItems.HCC145M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC146, ResourceItems.HCC146M);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveSubCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Updates the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                        }
                    }
                    var ProductCategoryDetails = _HCoreContext.SCCategory
                                                                 .Where(x => x.Id == _Request.ReferenceId
                                                                 && x.Guid == _Request.ReferenceKey
                                                                 && x.ParentId != null).FirstOrDefault();
                    if (ProductCategoryDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && ProductCategoryDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool NameCheck = _HCoreContext.SCCategory
                                                        .Any(x =>
                                                        x.ParentId == ProductCategoryDetails.ParentId
                                                        && x.SystemName == SystemName);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC147, ResourceItems.HCC147M);
                            }
                            ProductCategoryDetails.Name = _Request.Name;
                            ProductCategoryDetails.SystemName = SystemName;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && ProductCategoryDetails.Description != _Request.Description)
                        {
                            ProductCategoryDetails.Description = _Request.Description;
                        }
                        if (ProductCategoryDetails.PlatformCharge != _Request.PlatformCharge )
                        {
                            ProductCategoryDetails.PlatformCharge = _Request.PlatformCharge;
                        }
                        ProductCategoryDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ProductCategoryDetails.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && ProductCategoryDetails.StatusId != StatusId)
                        {
                            ProductCategoryDetails.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        long? PosterStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, ProductCategoryDetails.PrimaryStorageId, _Request.UserReference);
                        }
                        if (_Request.PosterContent != null && !string.IsNullOrEmpty(_Request.PosterContent.Content))
                        {
                            PosterStorageId = HCoreHelper.SaveStorage(_Request.PosterContent.Name, _Request.PosterContent.Extension, _Request.PosterContent.Content, ProductCategoryDetails.SecondaryStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null || PosterStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.SCCategory.Where(x => x.Guid == _SCCategory.Guid).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        PrDetails.PrimaryStorageId = IconStorageId;
                                    }
                                    if (PosterStorageId != null)
                                    {
                                        PrDetails.SecondaryStorageId = PosterStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC148, ResourceItems.HCC148M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateSubCategory", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Deletes the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    SCCategory Details = _HCoreContext.SCCategory.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey && x.ParentId != null).FirstOrDefault();
                    if (Details != null)
                    {
                        bool ProductCheck = _HCoreContext.SCProduct.Any(x => x.CategoryId == Details.Id );
                        if (ProductCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC149, ResourceItems.HCC149M);
                        }
                        _HCoreContext.SCCategory.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HC201, ResourceItems.HC201M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteSubCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubCategory(OCategory.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OCategory.Details _Details = (from x in _HCoreContext.SCCategory
                                                  where x.Guid == _Request.ReferenceKey
                                                  && x.Id == _Request.ReferenceId
                                                  && x.ParentId != null
                                                  select new OCategory.Details
                                                  {

                                                      ReferenceId = x.Id,
                                                      ReferenceKey = x.Guid,

                                                      Name = x.Name,
                                                      SystemName = x.SystemName,
                                                      Description = x.Description,
                                                      PlatformCharge = x.PlatformCharge,

                                                      IconUrl = x.PrimaryStorage.Path,
                                                      PosterUrl = x.SecondaryStorage.Path,

                                                      CreateDate = x.CreateDate,
                                                      CreatedByKey = x.CreatedBy.Guid,
                                                      CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                      ModifyDate = x.ModifyDate,
                                                      ModifyByKey = x.ModifyBy.Guid,
                                                      ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                      StatusId = x.StatusId,
                                                      StatusCode = x.Status.SystemName,
                                                      StatusName = x.Status.Name
                                                  })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(_Details.PosterUrl))
                        {
                            _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                        }
                        else
                        {
                            _Details.PosterUrl = _AppConfig.Default_Poster;
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, ResourceItems.HC200, ResourceItems.HC200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetSubCategory", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetSubCategory(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.ReferenceKey) && _Request.ReferenceId != 0)
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.SCCategory
                                                     where x.Parent.Guid == _Request.ReferenceKey
                                                     & x.ParentId == _Request.ReferenceId
                                                     select new OCategory.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         Name = x.Name,
                                                         SystemName = x.SystemName,
                                                         Description = x.Description,

                                                         IconUrl = x.PrimaryStorage.Path,
                                                         PosterUrl = x.SecondaryStorage.Path,
                                                         PlatformCharge = x.PlatformCharge,

                                                         CreateDate = x.CreateDate,
                                                         CreatedByKey = x.CreatedBy.Guid,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         ModifyDate = x.ModifyDate,
                                                         ModifyByKey = x.ModifyBy.Guid,
                                                         ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        }
                        #endregion
                        #region Get Data
                        List<OCategory.Details> Data = (from x in _HCoreContext.SCCategory
                                                        where x.Parent.Guid == _Request.ReferenceKey
                                                    & x.ParentId == _Request.ReferenceId
                                                        select new OCategory.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            Name = x.Name,
                                                            SystemName = x.SystemName,
                                                            Description = x.Description,

                                                            IconUrl = x.PrimaryStorage.Path,
                                                            PosterUrl = x.SecondaryStorage.Path,
                                                            PlatformCharge = x.PlatformCharge,

                                                            CreateDate = x.CreateDate,
                                                            CreatedByKey = x.CreatedBy.Guid,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                            ModifyDate = x.ModifyDate,
                                                            ModifyByKey = x.ModifyBy.Guid,
                                                            ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var _Details in Data)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.SCCategory
                                                     select new OCategory.Details
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         ParentCategoryKey = x.Parent.Guid,
                                                         ParentCategoryName = x.Parent.Name,

                                                         Name = x.Name,
                                                         SystemName = x.SystemName,
                                                         Description = x.Description,
                                                         PlatformCharge = x.PlatformCharge,

                                                         IconUrl = x.PrimaryStorage.Path,
                                                         PosterUrl = x.SecondaryStorage.Path,

                                                         CreateDate = x.CreateDate,
                                                         CreatedByKey = x.CreatedBy.Guid,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         ModifyDate = x.ModifyDate,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name
                                                     })
                                               .Where(_Request.SearchCondition)
                                       .Count();
                        }
                        #endregion
                        #region Get Data
                        List<OCategory.Details> Data = (from x in _HCoreContext.SCCategory
                                                        select new OCategory.Details
                                                        {
                                                            ReferenceId = x.Id,
                                                            ReferenceKey = x.Guid,

                                                            ParentCategoryKey = x.Parent.Guid,
                                                            ParentCategoryName = x.Parent.Name,

                                                            Name = x.Name,
                                                            SystemName = x.SystemName,
                                                            Description = x.Description,
                                                            PlatformCharge = x.PlatformCharge,

                                                            IconUrl = x.PrimaryStorage.Path,
                                                            PosterUrl = x.SecondaryStorage.Path,

                                                            CreateDate = x.CreateDate,
                                                            CreatedByKey = x.CreatedBy.Guid,
                                                            CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                            ModifyDate = x.ModifyDate,

                                                            StatusId = x.StatusId,
                                                            StatusCode = x.Status.SystemName,
                                                            StatusName = x.Status.Name
                                                        })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        foreach (var _Details in Data)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                        }
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetSubCategory", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

    }
}
