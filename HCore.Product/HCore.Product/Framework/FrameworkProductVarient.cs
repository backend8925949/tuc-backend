//==================================================================================
// FileName: FrameworkProductVarient.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to product varient
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;
namespace HCore.Product.Framework
{
    public class FrameworkProductVarient
    {
        HCoreContext _HCoreContext;
        SCProductVarientStock _SCProductVarientStock;
        SCProductVarient _SCProductVarient;
        List<SCProductVarientStock> _SCProductVarientStocks;
        /// <summary>
        /// Description: Saves the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveVarient(OProductVarient.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ProductId < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC171, ResourceItems.HCC171M);
                }
                if (string.IsNullOrEmpty(_Request.ProductKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC172, ResourceItems.HCC172M);
                }
                if (string.IsNullOrEmpty(_Request.Sku))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC173, ResourceItems.HCC173M);
                }
                if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC174, ResourceItems.HCC174M);
                }
                //if (string.IsNullOrEmpty(_Request.ShortDescription))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC175, ResourceItems.HCC175M);
                //}
                //if (string.IsNullOrEmpty(_Request.Description))
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC176, ResourceItems.HCC176M);
                //}
                if (_Request.ActualPrice < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC177, ResourceItems.HCC177M);
                }
                if (_Request.SellingPrice < 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC178, ResourceItems.HCC178M);
                }
                //if (_Request.RewardPercentage < 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC179, ResourceItems.HCC179M);
                //}
                //if (_Request.MaximumRewardAmount < 0)
                //{
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC180, ResourceItems.HCC180M);
                //}
                if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                }
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    bool ProductCheck = _HCoreContext.SCProduct.Any(x => x.Id == _Request.ProductId && x.Guid == _Request.ProductKey);
                    if (!ProductCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC181, ResourceItems.HCC181M);
                    }
                    string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);

                    //bool ProductCategoryDetails = _HCoreContext.SCProductVarient
                    //                                    .Any(x =>
                    //                                    x.ProductId == null
                    //                                    && x.SystemName == SystemName);
                    //if (!ProductCategoryDetails)
                    //{


                    _SCProductVarientStocks = new List<SCProductVarientStock>();
                    if (_Request.DealerLocations != null && _Request.DealerLocations.Count > 0)
                    {
                        foreach (var DealerLocation in _Request.DealerLocations)
                        {
                            long DealerId = (long)_HCoreContext.HCUAccount.Where(x => x.Id == DealerLocation.DealerLocationId).Select(x => x.OwnerId).FirstOrDefault();
                            _SCProductVarientStock = new SCProductVarientStock();
                            _SCProductVarientStock.Guid = HCoreHelper.GenerateGuid();
                            _SCProductVarientStock.SubAccountId = DealerLocation.DealerLocationId;
                            _SCProductVarientStock.Quantity = DealerLocation.Quantity;
                            _SCProductVarientStock.ActualPrice = DealerLocation.ActualPrice;
                            _SCProductVarientStock.SellingPrice = DealerLocation.SellingPrice;
                            _SCProductVarientStock.CreateDate = HCoreHelper.GetGMTDateTime();
                            _SCProductVarientStock.CreatedById = _Request.UserReference.AccountId;
                            _SCProductVarientStock.StatusId = (int)StatusId;
                            _SCProductVarientStocks.Add(_SCProductVarientStock);
                        }
                    }
                    _SCProductVarient = new SCProductVarient();
                    _SCProductVarient.Guid = HCoreHelper.GenerateGuid();
                    _SCProductVarient.ProductId = _Request.ProductId;
                    _SCProductVarient.Sku = _Request.Sku;
                    _SCProductVarient.Name = _Request.Name;
                    _SCProductVarient.SystemName = SystemName;
                    _SCProductVarient.Description = _Request.Description;
                    _SCProductVarient.MinimumQuantity = _Request.MinimumQuantity;
                    _SCProductVarient.MaximumQuantity = _Request.MaximumQuantity;
                    _SCProductVarient.ActualPrice = _Request.ActualPrice;
                    _SCProductVarient.SellingPrice = _Request.SellingPrice;
                    _SCProductVarient.ReferenceNumber = _Request.ReferenceNumber;
                    _SCProductVarient.CreateDate = HCoreHelper.GetGMTDateTime();
                    _SCProductVarient.CreatedById = _Request.UserReference.AccountId;
                    _SCProductVarient.StatusId = (int)StatusId;
                    _SCProductVarient.SCProductVarientStock = _SCProductVarientStocks;
                    _HCoreContext.SCProductVarient.Add(_SCProductVarient);
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _SCProductVarient.Id,
                        ReferenceKey = _SCProductVarient.Guid,
                        Name = _Request.Name,
                        Sku = _Request.Sku,
                        ActualPrice  = _Request.ActualPrice,
                        SellingPrice = _Request.SellingPrice,
                    };
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC145, ResourceItems.HCC145M);
                    #endregion
                    //}
                    //else
                    //{
                    //    #region Send Response
                    //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC146, ResourceItems.HCC146M);
                    //    #endregion
                    //}
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveVarient", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateVarient(OProductVarient.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                        }
                    }
                    var _Details = _HCoreContext.SCProductVarient
                                                                .Where(x => x.Id == _Request.ReferenceId
                                                                && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && _Details.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            //bool NameCheck = _HCoreContext.HCPCategory
                            //                            .Any(x =>
                            //                            x.ParentCategoryId == null
                            //                            && x.SystemName == SystemName);
                            //if (NameCheck)
                            //{
                            //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC147, ResourceItems.HCC147M);
                            //}
                            _Details.Name = _Request.Name;
                            _Details.SystemName = SystemName;
                        }

                        if (!string.IsNullOrEmpty(_Request.Sku) && _Details.Sku != _Request.Sku)
                        {
                            _Details.Sku = _Request.Sku;
                        }
                        if (!string.IsNullOrEmpty(_Request.ShortDescription) && _Details.Description != _Request.ShortDescription)
                        {
                            _Details.Description = _Request.ShortDescription;
                        }
                        if (!string.IsNullOrEmpty(_Request.Description) && _Details.Description != _Request.Description)
                        {
                            _Details.Description = _Request.Description;
                        }
                        if (!string.IsNullOrEmpty(_Request.ReferenceNumber) && _Details.ReferenceNumber != _Request.ReferenceNumber)
                        {
                            _Details.ReferenceNumber = _Request.ReferenceNumber;
                        }
                        if (_Details.MinimumQuantity != _Request.MinimumQuantity && _Request.MinimumQuantity > 0)
                        {
                            _Details.MinimumQuantity = _Request.MinimumQuantity;
                        }
                        if (_Details.MaximumQuantity != _Request.MaximumQuantity && _Request.MaximumQuantity > 0)
                        {
                            _Details.MaximumQuantity = _Request.MaximumQuantity;
                        }
                        if (_Details.ActualPrice != _Request.ActualPrice && _Request.ActualPrice > 0 )
                        {
                            _Details.ActualPrice = _Request.ActualPrice;
                        }
                        if (_Details.SellingPrice != _Request.SellingPrice  && _Request.SellingPrice > 0)
                        {
                            _Details.SellingPrice = _Request.SellingPrice;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && _Details.StatusId != StatusId)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC148, ResourceItems.HCC148M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }




                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("UpdateVarient", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Deletes the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteVarient(OProductVarient.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    SCProductVarient Details = _HCoreContext.SCProductVarient.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        bool SubItems = _HCoreContext.SCProductVarientStock.Any(x => x.VarientId == _Request.ReferenceId);
                        if (SubItems)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC170, ResourceItems.HCC170M);
                        }
                        _HCoreContext.SCProductVarient.Remove(Details);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HC201, ResourceItems.HC201M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteVarient", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVarient(OProductVarient.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.ReferenceKey) && _Request.ReferenceId > 0)
                    {
                        #region Get Data
                        OProductVarient.Details _Details = (from x in _HCoreContext.SCProductVarient
                                                            where x.Guid == _Request.ReferenceKey
                                                            && x.Id == _Request.ReferenceId
                                                            select new OProductVarient.Details
                                                            {

                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,

                                                                ProductId = x.ProductId,
                                                                ProductKey = x.Product.Guid,
                                                                ProductName = x.Product.Name,


                                                                Sku = x.Sku,
                                                                Name = x.Name,

                                                                CategoryKey = x.Product.Category.Guid,
                                                                CategoryName = x.Product.Category.Name,


                                                                ActualPrice = x.ActualPrice,
                                                                SellingPrice = x.SellingPrice,

                                                                Description = x.Description,
                                                                MaximumQuantity = x.MaximumQuantity,
                                                                MinimumQuantity = x.MinimumQuantity,
                                                                ReferenceNumber = x.ReferenceNumber,

                                                                IconUrl = x.Product.PrimaryStorage.Path,

                                                                CreateDate = x.CreateDate,
                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                ModifyDate = x.ModifyDate,
                                                                ModifyByKey = x.ModifyBy.Guid,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,

                                                                ProductStatusId = x.Product.StatusId,
                                                                ProductStatusCode = x.Product.Status.SystemName,
                                                                ProductStatusName = x.Product.Status.Name,


                                                            })
                                                  .FirstOrDefault();
                        #endregion

                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        if (_Details != null)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, ResourceItems.HC200, ResourceItems.HC200M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Get Data
                        OProductVarient.Details _Details = (from x in _HCoreContext.SCProductVarient
                                                            where x.Guid == _Request.ReferenceKey
                                                            && x.Id == _Request.ReferenceId
                                                            select new OProductVarient.Details
                                                            {

                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,

                                                                ProductId = x.ProductId,
                                                                ProductKey = x.Product.Guid,
                                                                ProductName = x.Product.Name,



                                                                Sku = x.Sku,
                                                                Name = x.Name,


                                                                ActualPrice = x.ActualPrice,
                                                                SellingPrice = x.SellingPrice,

                                                                Description = x.Description,
                                                                MaximumQuantity = x.MaximumQuantity,
                                                                MinimumQuantity = x.MinimumQuantity,
                                                                ReferenceNumber = x.ReferenceNumber,

                                                                IconUrl = x.Product.PrimaryStorage.Path,

                                                                CreateDate = x.CreateDate,
                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                ModifyDate = x.ModifyDate,
                                                                ModifyByKey = x.ModifyBy.Guid,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,

                                                                ProductStatusId = x.Product.StatusId,
                                                                ProductStatusCode = x.Product.Status.SystemName,
                                                                ProductStatusName = x.Product.Status.Name,
                                                            })
                                                  .FirstOrDefault();
                        #endregion

                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        if (_Details != null)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, ResourceItems.HC200, ResourceItems.HC200M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                            #endregion
                        }
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetVarient", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVarient(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.ReferenceId != 0 && !string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.SCProductVarient
                                                     where x.ProductId == _Request.ReferenceId && x.Product.Guid == _Request.ReferenceKey
                                                     select new OProductVarient.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,
                                                         Sku = x.Sku,
                                                         Name = x.Name,
                                                         ActualPrice = x.ActualPrice,
                                                         SellingPrice = x.SellingPrice,
                                                         CreateDate = x.CreateDate,
                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,
                                                         TotalStock = x.SCProductVarientStock.Sum(a => a.Quantity),
                                                     })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                        }

                        #endregion
                        #region Get Data
                        List<OProductVarient.List> Data = (from x in _HCoreContext.SCProductVarient
                                                            where x.ProductId == _Request.ReferenceId && x.Product.Guid == _Request.ReferenceKey
                                                           select new OProductVarient.List
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               Sku = x.Sku,
                                                               Name = x.Name,
                                                               ActualPrice = x.ActualPrice,
                                                               SellingPrice = x.SellingPrice,
                                                               CreateDate = x.CreateDate,
                                                               StatusId = x.StatusId,
                                                               StatusCode = x.Status.SystemName,
                                                               StatusName = x.Status.Name,
                                                               TotalStock = x.SCProductVarientStock.Sum(a => a.Quantity),
                                                           })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var _Details in Data)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        if (_Request.RefreshCount)
                        {
                            _Request.TotalRecords = (from x in _HCoreContext.SCProductVarient
                                                     select new OProductVarient.List
                                                     {
                                                         ReferenceId = x.Id,
                                                         ReferenceKey = x.Guid,

                                                         ProductId = x.ProductId,
                                                         ProductKey = x.Product.Guid,
                                                         ProductName = x.Product.Name,

                                                         DealerId = x.Product.AccountId,
                                                         DealerKey = x.Product.Account.Guid,
                                                         DealerDisplayName = x.Product.Account.DisplayName,

                                                         CategoryKey = x.Product.Category.Guid,
                                                         CategoryName = x.Product.Category.Name,

                                                         Sku = x.Sku,
                                                         Name = x.Name,
                                                         IconUrl = x.Product.PrimaryStorage.Path,

                                                         ActualPrice = x.ActualPrice,
                                                         SellingPrice = x.SellingPrice,
                                                         CreateDate = x.CreateDate,
                                                         CreatedByKey = x.CreatedBy.Guid,
                                                         CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                         StatusId = x.StatusId,
                                                         StatusCode = x.Status.SystemName,
                                                         StatusName = x.Status.Name,

                                                         TotalStock = x.SCProductVarientStock.Sum(a => a.Quantity),

                                                         ProductStatusId = x.Product.StatusId,
                                                         ProductStatusCode = x.Product.Status.SystemName,
                                                         ProductStatusName = x.Product.Status.Name,
                                                     })
                                           .Where(_Request.SearchCondition)
                                   .Count();
                        }

                        #endregion
                        #region Get Data
                        List<OProductVarient.List> Data = (from x in _HCoreContext.SCProductVarient
                                                           select new OProductVarient.List
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,

                                                               DealerId = x.Product.AccountId,
                                                               DealerKey = x.Product.Account.Guid,
                                                               DealerDisplayName = x.Product.Account.DisplayName,

                                                               ProductId = x.ProductId,
                                                               ProductKey = x.Product.Guid,
                                                               ProductName = x.Product.Name,
                                                               CategoryKey = x.Product.Category.Guid,
                                                               CategoryName = x.Product.Category.Name,
                                                               Sku = x.Sku,
                                                               Name = x.Name,
                                                               IconUrl = x.Product.PrimaryStorage.Path,

                                                               ActualPrice = x.ActualPrice,
                                                               SellingPrice = x.SellingPrice,
                                                               CreateDate = x.CreateDate,
                                                               CreatedByKey = x.CreatedBy.Guid,
                                                               CreatedByDisplayName = x.CreatedBy.DisplayName,
                                                               TotalStock = x.SCProductVarientStock.Sum(a => a.Quantity),
                                                               StatusId = x.StatusId,
                                                               StatusCode = x.Status.SystemName,
                                                               StatusName = x.Status.Name,

                                                               ProductStatusId = x.Product.StatusId,
                                                               ProductStatusCode = x.Product.Status.SystemName,
                                                               ProductStatusName = x.Product.Status.Name,
                                                           })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var _Details in Data)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetVarient", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Saves the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveVarientStock(OProductVarientStock.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.VarientId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC161, ResourceItems.HCC161M);
                }
                if (string.IsNullOrEmpty(_Request.VarientKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC160, ResourceItems.HCC160M);
                }
                if (_Request.DealerLocationId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC168, ResourceItems.HCC168M);
                }
                if (string.IsNullOrEmpty(_Request.DealerLocationKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC167, ResourceItems.HCC167M);
                }
                int? StatusId = HCoreHelper.GetSystemHelperId(_Request.StatusCode, _Request.UserReference);
                if (StatusId == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    bool VarientCheck = _HCoreContext.SCProductVarientStock.Any(x =>
                    x.SubAccountId == _Request.DealerLocationId
                    && x.SubAccount.Guid == _Request.DealerLocationKey
                    && x.VarientId == _Request.VarientId
                    && x.Varient.Guid == _Request.VarientKey);
                    if (VarientCheck)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC169, ResourceItems.HCC169M);
                    }
                    var DealerLocationDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.DealerLocationId && x.Guid == _Request.DealerLocationKey)
                        .Select(x => new
                        {
                            DealerId = x.OwnerId,
                        }).FirstOrDefault();
                    _SCProductVarientStock = new SCProductVarientStock();
                    _SCProductVarientStock.Guid = HCoreHelper.GenerateGuid();
                    _SCProductVarientStock.SubAccountId = _Request.DealerLocationId;
                    _SCProductVarientStock.VarientId = _Request.VarientId;
                    _SCProductVarientStock.Quantity = _Request.Quantity;
                    _SCProductVarientStock.MinimumQuantity = _Request.MinimumQuantity;
                    _SCProductVarientStock.MaximumQuantity = _Request.MaximumQuantity;
                    _SCProductVarientStock.ActualPrice = _Request.ActualPrice;
                    _SCProductVarientStock.SellingPrice = _Request.SellingPrice;
                    _SCProductVarientStock.CreateDate = HCoreHelper.GetGMTDateTime();
                    _SCProductVarientStock.CreatedById = _Request.UserReference.AccountId;
                    _SCProductVarientStock.StatusId = (int)StatusId;
                    _HCoreContext.SCProductVarientStock.Add(_SCProductVarientStock);
                    _HCoreContext.SaveChanges();
                    var _Response = new
                    {
                        ReferenceId = _SCProductVarientStock.Id,
                        ReferenceKey = _SCProductVarientStock.Guid,
                    };
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC145, ResourceItems.HCC145M);
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveVarientStock", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC500, ResourceItems.HC500M);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateVarientStock(OProductVarientStock.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                        }
                    }
                    var _Details = _HCoreContext.SCProductVarientStock
                                                                 .Where(x => x.Id == _Request.ReferenceId
                                                                 && x.Guid == _Request.ReferenceKey)
                                                                 .FirstOrDefault();
                    if (_Details != null)
                    {
                        if (_Request.Quantity > -1 && _Details.Quantity != _Request.Quantity)
                        {
                            if (_Request.OperationType == "remove")
                            {
                                _Details.Quantity -= _Request.Quantity;
                            }
                            else
                            {
                                _Details.Quantity += _Request.Quantity;

                            }
                        }
                        if (_Request.MinimumQuantity > 0 && _Details.MinimumQuantity != _Request.MinimumQuantity)
                        {
                            _Details.MinimumQuantity = _Request.MinimumQuantity;
                        }
                        if (_Request.MaximumQuantity > 0 && _Details.MaximumQuantity != _Request.MaximumQuantity)
                        {
                            _Details.MaximumQuantity = _Request.MaximumQuantity;
                        }
                        if (_Request.ActualPrice > 0 && _Details.ActualPrice != _Request.ActualPrice)
                        {
                            _Details.ActualPrice = _Request.ActualPrice;
                        }
                        if (_Request.SellingPrice > 0 && _Details.SellingPrice != _Request.SellingPrice)
                        {
                            _Details.SellingPrice = _Request.SellingPrice;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && _Details.StatusId != StatusId)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        using (_HCoreContext = new HCoreContext())
                        {
                            var VarientDetails = _HCoreContext.SCProductVarient
                                .Where(x => x.Id == _Details.VarientId)
                               .FirstOrDefault();
                            var ProductDetails = _HCoreContext.SCProduct
                            .Where(x => x.Id == VarientDetails.ProductId)
                           .FirstOrDefault();

                            VarientDetails.ModifyById = _Request.UserReference.AccountId;
                            VarientDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();
                        }

                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC148, ResourceItems.HCC148M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateVarientStock", _Exception, _Request.UserReference);
                #endregion
            }

        }
        /// <summary>
        /// Description: Updates the varient stock item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateVarientStockItem(OProductVarientStock.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.VarientId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.VarientKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                if (_Request.DealerLocationId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.DealerLocationKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    if (_Request.Quantity < 0)
                    {
                        _Request.Quantity = 0;
                    }
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102M);
                        }
                    }
                    var _Details = _HCoreContext.SCProductVarientStock
                                                                 .Where(x => x.VarientId == _Request.VarientId
                                                                 && x.Varient.Guid == _Request.VarientKey
                                                                 && x.SubAccountId == _Request.DealerLocationId
                                                                 && x.SubAccount.Guid  == _Request.DealerLocationKey)
                                                                 .FirstOrDefault();
                    if (_Details != null)
                    {
                        if (_Request.Quantity > -1 && _Details.Quantity != _Request.Quantity)
                        {
                            if (_Request.OperationType == "remove")
                            {
                                _Details.Quantity -= _Request.Quantity;
                            }
                            else
                            {
                                _Details.Quantity += _Request.Quantity;

                            }
                        }
                        if (_Details.Quantity < 0)
                        {
                            _Details.Quantity = 0; 
                        }
                        //if (_Request.MinimumQuantity > 0 && _Details.MinimumQuantity != _Request.MinimumQuantity)
                        //{
                        //    _Details.MinimumQuantity = _Request.MinimumQuantity;
                        //}
                        //if (_Request.MaximumQuantity > 0 && _Details.MaximumQuantity != _Request.MaximumQuantity)
                        //{
                        //    _Details.MaximumQuantity = _Request.MaximumQuantity;
                        //}
                        if (_Request.ActualPrice > 0 && _Details.ActualPrice != _Request.ActualPrice)
                        {
                            _Details.ActualPrice = _Request.ActualPrice;
                        }
                        if (_Request.SellingPrice > 0 && _Details.SellingPrice != _Request.SellingPrice)
                        {
                            _Details.SellingPrice = _Request.SellingPrice;
                        }
                        _Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _Details.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && _Details.StatusId != StatusId)
                        {
                            _Details.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC148, ResourceItems.HCC148M);
                        #endregion
                    }
                    else
                    {
                        bool VarientCheck = _HCoreContext.SCProductVarientStock.Any(x =>
                            x.SubAccountId == _Request.DealerLocationId
                            && x.SubAccount.Guid == _Request.DealerLocationKey
                            && x.VarientId == _Request.VarientId
                            && x.Varient.Guid == _Request.VarientKey);
                        if (VarientCheck)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC169, ResourceItems.HCC169M);
                        }
                        var DealerLocationDetails = _HCoreContext.HCUAccount.Where(x => x.Id == _Request.DealerLocationId && x.Guid == _Request.DealerLocationKey)
                            .Select(x => new
                            {
                                DealerId = x.OwnerId,
                            }).FirstOrDefault();
                        
                        _SCProductVarientStock = new SCProductVarientStock();
                        _SCProductVarientStock.Guid = HCoreHelper.GenerateGuid();
                        _SCProductVarientStock.SubAccountId = _Request.DealerLocationId;
                        _SCProductVarientStock.VarientId = _Request.VarientId;
                        _SCProductVarientStock.Quantity = _Request.Quantity;
                      
                        _SCProductVarientStock.MinimumQuantity = _Request.MinimumQuantity;
                        _SCProductVarientStock.MaximumQuantity = _Request.MaximumQuantity;
                        _SCProductVarientStock.ActualPrice = _Request.ActualPrice;
                        _SCProductVarientStock.SellingPrice = _Request.SellingPrice;
                        _SCProductVarientStock.CreateDate = HCoreHelper.GetGMTDateTime();
                        _SCProductVarientStock.CreatedById = _Request.UserReference.AccountId;
                        _SCProductVarientStock.StatusId = HelperStatus.HCProduct.Active;
                        _HCoreContext.SCProductVarientStock.Add(_SCProductVarientStock);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _SCProductVarientStock.Id,
                            ReferenceKey = _SCProductVarientStock.Guid,
                        };

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HCC148, ResourceItems.HCC148M);
                        #endregion
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("UpdateVarientStock", _Exception, _Request.UserReference);
                #endregion
            }

        }

        /// <summary>
        /// Description: Deletes the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteVarientStock(OProductVarientStock.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    SCProductVarientStock _Details = _HCoreContext.SCProductVarientStock.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (_Details != null)
                    {
                        _HCoreContext.SCProductVarientStock.Remove(_Details);
                        _HCoreContext.SaveChanges();
                        using (_HCoreContext = new HCoreContext())
                        {
                            var VarientDetails = _HCoreContext.SCProductVarient
                                .Where(x => x.Id == _Details.VarientId)
                               .FirstOrDefault();
                            var ProductDetails = _HCoreContext.SCProduct
                            .Where(x => x.Id == VarientDetails.ProductId)
                           .FirstOrDefault();

                            VarientDetails.ModifyById = _Request.UserReference.AccountId;
                            VarientDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                            _HCoreContext.SaveChanges();
                        }
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, ResourceItems.HC201, ResourceItems.HC201M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteVarientStock", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVarientStock(OProductVarientStock.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OProductVarientStock.Details _Details = (from x in _HCoreContext.SCProductVarientStock
                                                             where x.Guid == _Request.ReferenceKey
                                                             && x.Id == _Request.ReferenceId
                                                             select new OProductVarientStock.Details
                                                             {

                                                                 ReferenceId = x.Id,
                                                                 ReferenceKey = x.Guid,

                                                                 DealerId = x.SubAccount.OwnerId,
                                                                 DealerKey = x.SubAccount.Owner.Guid,
                                                                 DealerDisplayName = x.SubAccount.Owner.DisplayName,

                                                                 DealerLocationId = x.SubAccountId,
                                                                 DealerLocationKey = x.SubAccount.Guid,
                                                                 DealerLocationName = x.SubAccount.DisplayName,

                                                                 Quantity = x.Quantity,
                                                                 MinimumQuantity = x.MinimumQuantity,
                                                                 MaximumQuantity = x.MaximumQuantity,

                                                                 ActualPrice = x.ActualPrice,
                                                                 SellingPrice = x.SellingPrice,


                                                                 CreateDate = x.CreateDate,
                                                                 CreatedByKey = x.CreatedBy.Guid,
                                                                 CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                 ModifyDate = x.ModifyDate,
                                                                 ModifyByKey = x.ModifyBy.Guid,
                                                                 ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                                 StatusId = x.StatusId,
                                                                 StatusCode = x.Status.SystemName,
                                                                 StatusName = x.Status.Name
                                                             })
                                              .FirstOrDefault();
                    #endregion

                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    if (_Details != null)
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, ResourceItems.HC200, ResourceItems.HC200M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetVarientStock", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVarientStock(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.SCProductVarientStock
                                        where x.Varient.Guid == _Request.ReferenceKey
                                                            && x.VarientId == _Request.ReferenceId
                                        select new OProductVarientStock.Details
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            DealerLocationId = x.SubAccountId,
                                            DealerLocationKey = x.SubAccount.Guid,
                                            DealerLocationName = x.SubAccount.DisplayName,
                                            DealerLocationAddress  = x.SubAccount.Address,
                                            Quantity = x.Quantity,
                                            CreateDate = x.CreateDate,
                                            ModifyDate = x.ModifyDate,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OProductVarientStock.Details> Data = (from x in _HCoreContext.SCProductVarientStock
                                                               where x.Varient.Guid == _Request.ReferenceKey
                                                              && x.VarientId == _Request.ReferenceId
                                                               select new OProductVarientStock.Details
                                                               {
                                                                   ReferenceId = x.Id,
                                                                   ReferenceKey = x.Guid,
                                                                   DealerLocationId = x.SubAccountId,
                                                                   DealerLocationKey = x.SubAccount.Guid,
                                                                   DealerLocationName = x.SubAccount.DisplayName,
                                                                   DealerLocationAddress = x.SubAccount.Address,
                                                                   Quantity = x.Quantity,
                                                                   CreateDate = x.CreateDate,
                                                                   ModifyDate = x.ModifyDate,
                                                                   StatusId = x.StatusId,
                                                                   StatusCode = x.Status.SystemName,
                                                                   StatusName = x.Status.Name
                                                               })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetVarientStock", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
    }
}
