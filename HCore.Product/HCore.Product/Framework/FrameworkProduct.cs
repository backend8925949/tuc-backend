//==================================================================================
// FileName: FrameworkProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to product
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;
using GeoCoordinatePortable;

namespace HCore.Product.Framework
{
    public class FrameworkProduct
    {
        HCoreContext _HCoreContext;
        SCProduct _SCProduct;
        SCProductVarient _SCProductVarient;
        List<SCProductVarient> _SCProductVarients;
        List<SCProductVarientStock> _SCProductVarientStocks;
        SCProductVarientStock _SCProductVarientStock;
        /// <summary>
        /// Description: Saves the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.DealerKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC118", ResourceItems.HCC118);
                }
                else if (_Request.DealerId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC119", ResourceItems.HCC119);
                }
                else if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC120", ResourceItems.HCC120);
                }
                else if (string.IsNullOrEmpty(_Request.Description))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC120", ResourceItems.HCC120);
                }
                else if (string.IsNullOrEmpty(_Request.Name))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC138", ResourceItems.HCC138);
                }
                else if (string.IsNullOrEmpty(_Request.CategoryKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC139", ResourceItems.HCC139);
                }
                else if (_Request.ActualPrice < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC124", ResourceItems.HCC124);
                }
                else if (_Request.SellingPrice < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC125", ResourceItems.HCC125);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC102", ResourceItems.HC102M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC103", ResourceItems.HC103M);
                    }

                    long CategoryId = 0;
                    if (!string.IsNullOrEmpty(_Request.CategoryKey))
                    {
                        CategoryId = _HCoreContext.SCCategory.Where(x => x.Guid == _Request.CategoryKey).Select(x => x.Id).FirstOrDefault();
                    }
                    var MerchantDetails = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Merchant
                        && x.StatusId == HelperStatus.Default.Active
                        && x.Id == _Request.DealerId
                        && x.Guid == _Request.DealerKey)
                        .FirstOrDefault();
                    if (MerchantDetails != null)
                    {
                        string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                        var ProductDetails = _HCoreContext.SCProduct
                                                            .Any(x => x.AccountId == _Request.DealerId
                                                            && x.Name == SystemName
                                                            && x.Account.Guid == _Request.DealerKey);
                        if (!ProductDetails)
                        {
                            _SCProductVarientStocks = new List<SCProductVarientStock>();
                            if (_Request.DealerLocations != null && _Request.DealerLocations.Count > 0)
                            {
                                foreach (var DealerLocation in _Request.DealerLocations)
                                {
                                    _SCProductVarientStock = new SCProductVarientStock();
                                    _SCProductVarientStock.Guid = HCoreHelper.GenerateGuid();
                                    _SCProductVarientStock.SubAccountId = DealerLocation.DealerLocationId;
                                    _SCProductVarientStock.Quantity = DealerLocation.Quantity;
                                    _SCProductVarientStock.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _SCProductVarientStock.CreatedById = _Request.UserReference.AccountId;
                                    _SCProductVarientStock.StatusId = StatusId;
                                    _SCProductVarientStocks.Add(_SCProductVarientStock);
                                }
                            }
                            _SCProductVarient = new SCProductVarient();
                            _SCProductVarient.Guid = HCoreHelper.GenerateGuid();
                            _SCProductVarient.Sku = _Request.Sku;
                            if (!string.IsNullOrEmpty(_Request.VarientName))
                            {
                                _SCProductVarient.Name = _Request.Name;
                            }
                            else
                            {
                                _SCProductVarient.Name = _Request.VarientName;
                            }
                            _SCProductVarient.SystemName = SystemName;
                            _SCProductVarient.Description = _Request.Description;
                            _SCProductVarient.MinimumQuantity = _Request.MinimumQuantity;
                            _SCProductVarient.MaximumQuantity = _Request.MaximumQuantity;
                            _SCProductVarient.ActualPrice = _Request.ActualPrice;
                            _SCProductVarient.SellingPrice = _Request.SellingPrice;
                            _SCProductVarient.ReferenceNumber = _Request.ReferenceNumber;
                            _SCProductVarient.BarcodeNumber = _Request.BarcodeNumber;
                            _SCProductVarient.CreateDate = HCoreHelper.GetGMTDateTime();
                            _SCProductVarient.CreatedById = _Request.UserReference.AccountId;
                            _SCProductVarient.StatusId = StatusId;
                            _SCProductVarient.SCProductVarientStock = _SCProductVarientStocks;
                            _SCProductVarients = new List<SCProductVarient>();
                            _SCProductVarients.Add(_SCProductVarient);


                            _SCProduct = new SCProduct();
                            _SCProduct.Guid = HCoreHelper.GenerateGuid();
                            _SCProduct.AccountId = _Request.DealerId;
                            _SCProduct.Name = _Request.Name;
                            _SCProduct.SystemName = SystemName;
                            _SCProduct.Description = _Request.Description;
                            _SCProduct.RewardPercentage = _Request.RewardPercentage;
                            _SCProduct.CategoryId = CategoryId;
                            _SCProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                            _SCProduct.CreatedById = _Request.UserReference.AccountId;
                            _SCProduct.StatusId = StatusId;
                            _SCProduct.SCProductVarient = _SCProductVarients;
                            _HCoreContext.SCProduct.Add(_SCProduct);
                            _HCoreContext.SaveChanges();
                            var _Response = new
                            {
                                ReferenceId = _SCProduct.Id,
                                ReferenceKey = _SCProduct.Guid,
                            };
                            long? IconStorageId = null;
                            if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                            {
                                IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, null, _Request.UserReference);
                            }
                            if (IconStorageId != null)
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    var PrDetails = _HCoreContext.SCProduct.Where(x => x.Guid == _SCProduct.Guid).FirstOrDefault();
                                    if (PrDetails != null)
                                    {
                                        if (IconStorageId != null)
                                        {
                                            PrDetails.PrimaryStorageId = IconStorageId;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                }
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCC129", ResourceItems.HCC129);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC129", ResourceItems.HCC129);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC127", ResourceItems.HCC127);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC131", ResourceItems.HCC131);
                }
                else if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC132", ResourceItems.HCC132);
                }

                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = 0;
                    if (!string.IsNullOrEmpty(_Request.StatusCode))
                    {
                        StatusId = _HCoreContext.HCCore.Where(x =>  x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC130", ResourceItems.HCC130);
                        }
                    }
                    long CategoryId = 0;
                    if (!string.IsNullOrEmpty(_Request.CategoryKey))
                    {
                        CategoryId = _HCoreContext.SCCategory.Where(x => x.Guid == _Request.CategoryKey).Select(x => x.Id).FirstOrDefault();
                    }
                    var ProductDetails = _HCoreContext.SCProduct
                                                             .Where(x => x.Id == _Request.ReferenceId
                                                             && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.Name) && ProductDetails.Name != _Request.Name)
                        {
                            string SystemName = HCoreHelper.GenerateSystemName(_Request.Name);
                            bool NameCheck = _HCoreContext.SCProduct
                                                        .Any(x =>
                                                        x.AccountId == ProductDetails.AccountId
                                                        && x.SystemName == SystemName);
                            if (NameCheck)
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HCC182, ResourceItems.HCC182M);
                            }
                            ProductDetails.Name = _Request.Name;
                            ProductDetails.SystemName = SystemName;
                        }
                        long? OldIconStorageId = ProductDetails.PrimaryStorageId;
                        if (!string.IsNullOrEmpty(_Request.Description) && ProductDetails.Description != _Request.Description)
                        {
                            ProductDetails.Description = _Request.Description;
                        }
                        if (CategoryId != 0)
                        {
                            ProductDetails.CategoryId = CategoryId;
                        }
                        ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        ProductDetails.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId != 0 && ProductDetails.StatusId != StatusId)
                        {
                            ProductDetails.StatusId = StatusId;
                        }
                        _HCoreContext.SaveChanges();
                        long? IconStorageId = null;
                        if (_Request.IconContent != null && !string.IsNullOrEmpty(_Request.IconContent.Content))
                        {
                            IconStorageId = HCoreHelper.SaveStorage(_Request.IconContent.Name, _Request.IconContent.Extension, _Request.IconContent.Content, OldIconStorageId, _Request.UserReference);
                        }
                        if (IconStorageId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var UserAccDetails = _HCoreContext.SCProduct.Where(x => x.Guid == _Request.ReferenceKey).FirstOrDefault();
                                if (UserAccDetails != null)
                                {
                                    if (IconStorageId != null)
                                    {
                                        UserAccDetails.PrimaryStorageId = IconStorageId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        var _Response = new
                        {
                            ReferenceId = ProductDetails.Id,
                            ReferenceKey = ProductDetails.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCC133", ResourceItems.HCC133);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC134", ResourceItems.HCC134);
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("UpdateProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetVarient(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    if (_Request.RefreshCount)
                    {
                        _Request.TotalRecords = (from x in _HCoreContext.SCProductVarient
                                                 select new OProductVarient.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,


                                                     ProductId = x.ProductId,
                                                     ProductKey = x.Product.Guid,
                                                     ProductName = x.Product.Name,

                                                     DealerId = x.Product.AccountId,
                                                     DealerKey = x.Product.Account.Guid,
                                                     DealerDisplayName = x.Product.Account.DisplayName,

                                                     CategoryKey = x.Product.Category.Guid,
                                                     CategoryName = x.Product.Category.Name,

                                                     Sku = x.Sku,
                                                     Name = x.Name,
                                                     IconUrl = x.Product.PrimaryStorage.Path,


                                                     ActualPrice = x.ActualPrice,
                                                     SellingPrice = x.SellingPrice,
                                                     CreateDate = x.CreateDate,
                                                     CreatedByKey = x.CreatedBy.Guid,
                                                     CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name,

                                                     ProductStatusId = x.Product.StatusId,
                                                     ProductStatusCode = x.Product.Status.SystemName,
                                                     ProductStatusName = x.Product.Status.Name,
                                                 })
                                       .Where(_Request.SearchCondition)
                               .Count();
                    }

                    #endregion
                    #region Get Data
                    List<OProductVarient.List> Data = (from x in _HCoreContext.SCProductVarient
                                                       select new OProductVarient.List
                                                       {
                                                           ReferenceId = x.Id,
                                                           ReferenceKey = x.Guid,

                                                           DealerId = x.Product.AccountId,
                                                           DealerKey = x.Product.Account.Guid,
                                                           DealerDisplayName = x.Product.Account.DisplayName,

                                                           ProductId = x.ProductId,
                                                           ProductKey = x.Product.Guid,
                                                           ProductName = x.Product.Name,
                                                           CategoryKey = x.Product.Category.Guid,
                                                           CategoryName = x.Product.Category.Name,
                                                           Sku = x.Sku,
                                                           Name = x.Name,
                                                           IconUrl = x.Product.PrimaryStorage.Path,

                                                           ActualPrice = x.ActualPrice,
                                                           SellingPrice = x.SellingPrice,
                                                           CreateDate = x.CreateDate,
                                                           CreatedByKey = x.CreatedBy.Guid,
                                                           CreatedByDisplayName = x.CreatedBy.DisplayName,


                                                           StatusId = x.StatusId,
                                                           StatusCode = x.Status.SystemName,
                                                           StatusName = x.Status.Name,

                                                           ProductStatusId = x.Product.StatusId,
                                                           ProductStatusCode = x.Product.Status.SystemName,
                                                           ProductStatusName = x.Product.Status.Name,
                                                       })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var _Details in Data)
                    {
                        if (!string.IsNullOrEmpty(_Details.IconUrl))
                        {
                            _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                        }
                        else
                        {
                            _Details.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug
                HCoreHelper.LogException("GetVarient", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        //internal OResponse UpdateProductStock(OProduct.Request _Request)
        //{
        //    #region Manage Exception
        //    try
        //    {
        //        if (string.IsNullOrEmpty(_Request.ReferenceKey))
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC131", ResourceItems.HCC131);
        //        }
        //        else if (_Request.ReferenceId == 0)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC132", ResourceItems.HCC132);
        //        }
        //        else if (_Request.TotalStock < 1)
        //        {
        //            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC135", ResourceItems.HCC135);
        //        }
        //        using (_HCoreContext = new HCoreContext())
        //        {
        //            var ProductDetails = _HCoreContext.SCProduct
        //                                                     .Where(x => x.Id == _Request.ReferenceId
        //                                                     && x.Guid == _Request.ReferenceKey).FirstOrDefault();
        //            if (ProductDetails != null)
        //            {
        //                ProductDetails.TotalStock = _Request.TotalStock;
        //                ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
        //                ProductDetails.ModifyById = _Request.UserReference.AccountId;
        //                _HCoreContext.SaveChanges();
        //                var _Response = new
        //                {
        //                    ReferenceId = ProductDetails.Id,
        //                    ReferenceKey = ProductDetails.Guid,
        //                };
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Response, "HCC133", ResourceItems.HCC133);
        //                #endregion
        //            }
        //            else
        //            {
        //                #region Send Response
        //                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC134", ResourceItems.HCC134);
        //                #endregion
        //            }
        //        }
        //    }
        //    catch (Exception _Exception)
        //    {
        //        #region  Log Exception
        //        HCoreHelper.LogException("UpdateProduct", _Exception, _Request.UserReference);
        //        #endregion
        //        #region Send Response
        //        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
        //        #endregion
        //    }
        //    #endregion
        //}
        /// <summary>
        /// Description: Deletes the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse DeleteProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC131", ResourceItems.HCC131);
                }
                else if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC132", ResourceItems.HCC132);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var ProductDetails = _HCoreContext.SCProduct
                                                             .Where(x => x.Id == _Request.ReferenceId
                                                             && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (ProductDetails != null)
                    {
                        bool ProductCheck = _HCoreContext.SCProductVarient.Any(x => x.ProductId == ProductDetails.Id);
                        if (ProductCheck)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC130", ResourceItems.HCC130);
                        }
                        _HCoreContext.SCProduct.Remove(ProductDetails);
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC136", ResourceItems.HCC136);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC134", ResourceItems.HCC134);
                        #endregion
                    }

                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("DeleteProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProduct(OProduct.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Get Data
                    OProduct.Details Data = (from x in _HCoreContext.SCProduct
                                             where x.Guid == _Request.ReferenceKey
                                             && x.Id == _Request.ReferenceId
                                             select new OProduct.Details
                                             {

                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 AccountId = x.AccountId,
                                                 AccountKey = x.Account.Guid,
                                                 AccountDisplayName = x.Account.DisplayName,

                                                 TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                                 VarientId = x.SCProductVarient.FirstOrDefault().Id,
                                                 VarientKey = x.SCProductVarient.FirstOrDefault().Guid,
                                                 ActualPrice = x.SCProductVarient.FirstOrDefault().ActualPrice,
                                                 SellingPrice = x.SCProductVarient.FirstOrDefault().SellingPrice,
                                                 DealerId = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccountId,
                                                 DealerKey = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.Guid,
                                                 DealerName = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.Name,
                                                 DealerIconUrl = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.IconStorage.Path,

                                                 CategoryKey = x.Category.Guid,
                                                 CategoryName = x.Category.Name,
                                                 Name = x.Name,
                                                 Description = x.Description,
                                                 IconUrl = x.PrimaryStorage.Path,
                                                 CreateDate = x.CreateDate,
                                                 CreatedByKey = x.CreatedBy.Guid,
                                                 CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                 ModifyDate = x.ModifyDate,
                                                 ModifyByKey = x.ModifyBy.Guid,
                                                 ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name
                                             })
                                              .FirstOrDefault();
                    #endregion
                    if (Data != null)
                    {
                        if (!string.IsNullOrEmpty(Data.IconUrl))
                        {
                            Data.IconUrl = _AppConfig.StorageUrl + Data.IconUrl;
                        }
                        else
                        {
                            Data.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, Data, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProduct(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Total Records
                    int TotalRecords = (from x in _HCoreContext.SCProduct
                                        where x.AccountId == _Request.ReferenceId
                                        && x.Account.Guid == _Request.ReferenceKey
                                        select new OProduct.List
                                        {
                                            ReferenceId = x.Id,
                                            ReferenceKey = x.Guid,
                                            Name = x.Name,
                                            CategoryKey = x.Category.Guid,
                                            CategoryName = x.Category.Name,
                                            IconUrl = x.PrimaryStorage.Path,
                                            CreateDate = x.CreateDate,
                                            TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                            VarientId = x.SCProductVarient.FirstOrDefault().Id,
                                            VarientKey = x.SCProductVarient.FirstOrDefault().Guid,
                                            ActualPrice = x.SCProductVarient.FirstOrDefault().ActualPrice,
                                            SellingPrice = x.SCProductVarient.FirstOrDefault().SellingPrice,
                                            DealerId = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccountId,
                                            DealerKey = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.Guid,
                                            DealerName = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.Name,
                                            DealerIconUrl = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.IconStorage.Path,
                                            StatusId = x.StatusId,
                                            StatusCode = x.Status.SystemName,
                                            StatusName = x.Status.Name
                                        })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Set Default Limit
                    if (_Request.Limit == -1)
                    {
                        _Request.Limit = TotalRecords;
                    }
                    else if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Get Data
                    List<OProduct.List> Data = (from x in _HCoreContext.SCProduct
                                                where x.AccountId == _Request.ReferenceId
                                                && x.Account.Guid == _Request.ReferenceKey
                                                select new OProduct.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    CategoryKey = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    IconUrl = x.PrimaryStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                                    VarientId = x.SCProductVarient.FirstOrDefault().Id,
                                                    VarientKey = x.SCProductVarient.FirstOrDefault().Guid,
                                                    ActualPrice = x.SCProductVarient.FirstOrDefault().ActualPrice,
                                                    SellingPrice = x.SCProductVarient.FirstOrDefault().SellingPrice,
                                                    DealerId = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccountId,
                                                    DealerKey = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.Guid,
                                                    DealerName = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.Name,
                                                    DealerIconUrl = x.SCProductVarient.FirstOrDefault().SCProductVarientStock.FirstOrDefault().SubAccount.IconStorage.Path,
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(Details.DealerIconUrl))
                        {
                            Details.DealerIconUrl = _AppConfig.StorageUrl + Details.DealerIconUrl;
                        }
                        else
                        {
                            Details.DealerIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProducts", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

        OProduct.Overview _ProductOverview;

        /// <summary>
        /// Description: Gets the product overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductOverview(OList.Request _Request)
        {
            #region Manage Exception
            try
            {

                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "StatusId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    _ProductOverview = new OProduct.Overview();

                    #region Total Records
                    _ProductOverview.Total = (from x in _HCoreContext.SCProduct
                                              where x.AccountId == _Request.ReferenceId
                                              && x.Account.Guid == _Request.ReferenceKey
                                              && x.StatusId == HelperStatus.HCProduct.Active
                                              select new OProduct.List
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,
                                                  Name = x.Name,
                                                  CategoryKey = x.Category.Guid,
                                                  CategoryName = x.Category.Name,
                                                  IconUrl = x.PrimaryStorage.Path,
                                                  CreateDate = x.CreateDate,
                                                  TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name
                                              })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region InStock Records
                    _ProductOverview.InStock = (from x in _HCoreContext.SCProduct
                                                where x.AccountId == _Request.ReferenceId
                                                && x.Account.Guid == _Request.ReferenceKey
                                                && x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)) > 0
                                                   && x.StatusId == HelperStatus.HCProduct.Active
                                                select new OProduct.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,
                                                    Name = x.Name,
                                                    CategoryKey = x.Category.Guid,
                                                    CategoryName = x.Category.Name,
                                                    IconUrl = x.PrimaryStorage.Path,
                                                    CreateDate = x.CreateDate,
                                                    TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                                    StatusId = x.StatusId,
                                                    StatusCode = x.Status.SystemName,
                                                    StatusName = x.Status.Name
                                                })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region OutOfStock Records
                    _ProductOverview.OutOfStock = (from x in _HCoreContext.SCProduct
                                                   where x.AccountId == _Request.ReferenceId
                                                   && x.Account.Guid == _Request.ReferenceKey
                                                   && x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)) < 1
                                                    && x.StatusId == HelperStatus.HCProduct.Active
                                                   select new OProduct.List
                                                   {
                                                       ReferenceId = x.Id,
                                                       ReferenceKey = x.Guid,
                                                       Name = x.Name,
                                                       CategoryKey = x.Category.Guid,
                                                       CategoryName = x.Category.Name,
                                                       IconUrl = x.PrimaryStorage.Path,
                                                       CreateDate = x.CreateDate,
                                                       TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                                       StatusId = x.StatusId,
                                                       StatusCode = x.Status.SystemName,
                                                       StatusName = x.Status.Name
                                                   })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region LowStock Records
                    _ProductOverview.LowStock = (from x in _HCoreContext.SCProduct
                                                 where x.AccountId == _Request.ReferenceId
                                                 && x.Account.Guid == _Request.ReferenceKey
                                                 && x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)) < 20
                                               && x.StatusId == HelperStatus.HCProduct.Active
                                                 select new OProduct.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,
                                                     Name = x.Name,
                                                     CategoryKey = x.Category.Guid,
                                                     CategoryName = x.Category.Name,
                                                     IconUrl = x.PrimaryStorage.Path,
                                                     CreateDate = x.CreateDate,
                                                     TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                                     StatusId = x.StatusId,
                                                     StatusCode = x.Status.SystemName,
                                                     StatusName = x.Status.Name
                                                 })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region LowStock Records
                    _ProductOverview.UnavailableProduct = (from x in _HCoreContext.SCProduct
                                                           where x.AccountId == _Request.ReferenceId
                                                           && x.Account.Guid == _Request.ReferenceKey
                                                           && x.StatusId != HelperStatus.HCProduct.Active
                                                           select new OProduct.List
                                                           {
                                                               ReferenceId = x.Id,
                                                               ReferenceKey = x.Guid,
                                                               Name = x.Name,
                                                               CategoryKey = x.Category.Guid,
                                                               CategoryName = x.Category.Name,
                                                               IconUrl = x.PrimaryStorage.Path,
                                                               CreateDate = x.CreateDate,
                                                               TotalStock = x.SCProductVarient.Sum(a => a.SCProductVarientStock.Sum(m => m.Quantity)),
                                                               StatusId = x.StatusId,
                                                               StatusCode = x.Status.SystemName,
                                                               StatusName = x.Status.Name
                                                           })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _ProductOverview, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProductOverview", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }


        /// <summary>
        /// Description: Gets the product lite.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductLite(OList.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "TotalStock", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Set Default Limit
                    if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    if (_Request.ReferenceId != 0 && !string.IsNullOrEmpty(_Request.ReferenceKey))
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.SCProduct
                                                 where x.AccountId == _Request.ReferenceId
                                                 && x.Account.Guid == _Request.ReferenceKey
                                                 && x.StatusId == HelperStatus.Default.Active
                                                 select new OProduct.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     Name = x.Name,


                                                     CategoryKey = x.Category.Guid,
                                                     CategoryName = x.Category.Name,

                                                     IconUrl = x.PrimaryStorage.Path,
                                                     CreateDate = x.CreateDate,


                                                     VarientId = x.SCProductVarient.FirstOrDefault().Id,
                                                     VarientKey = x.SCProductVarient.FirstOrDefault().Guid,
                                                     ActualPrice = x.SCProductVarient.FirstOrDefault().ActualPrice,
                                                     SellingPrice = x.SCProductVarient.FirstOrDefault().SellingPrice,

                                                 })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                        #region Get Data
                        List<OProduct.List> Data = (from x in _HCoreContext.SCProduct
                                                    where x.AccountId == _Request.ReferenceId
                                           && x.Account.Guid == _Request.ReferenceKey
                                           && x.StatusId == HelperStatus.Default.Active
                                                    select new OProduct.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Name = x.Name,


                                                        CategoryKey = x.Category.Guid,
                                                        CategoryName = x.Category.Name,

                                                        IconUrl = x.PrimaryStorage.Path,


                                                        CreateDate = x.CreateDate,


                                                        VarientId = x.SCProductVarient.FirstOrDefault().Id,
                                                        VarientKey = x.SCProductVarient.FirstOrDefault().Guid,
                                                        ActualPrice = x.SCProductVarient.FirstOrDefault().ActualPrice,
                                                        SellingPrice = x.SCProductVarient.FirstOrDefault().SellingPrice,

                                                    })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }
                    else
                    {
                        #region Total Records
                        _Request.TotalRecords = (from x in _HCoreContext.SCProduct
                                                 where x.StatusId == HelperStatus.Default.Active
                                                 select new OProduct.List
                                                 {
                                                     ReferenceId = x.Id,
                                                     ReferenceKey = x.Guid,

                                                     Name = x.Name,


                                                     CategoryKey = x.Category.Guid,
                                                     CategoryName = x.Category.Name,

                                                     IconUrl = x.PrimaryStorage.Path,

                                                     DealerId = x.AccountId,
                                                     DealerKey = x.Account.Guid,
                                                     DealerName = x.Account.DisplayName,
                                                     DealerIconUrl = x.Account.IconStorage.Path,

                                                     VarientId = x.SCProductVarient.FirstOrDefault().Id,
                                                     VarientKey = x.SCProductVarient.FirstOrDefault().Guid,
                                                     ActualPrice = x.SCProductVarient.FirstOrDefault().ActualPrice,
                                                     SellingPrice = x.SCProductVarient.FirstOrDefault().SellingPrice,


                                                     CreateDate = x.CreateDate,

                                                 })
                                            .Where(_Request.SearchCondition)
                                    .Count();
                        #endregion
                        #region Get Data
                        List<OProduct.List> Data = (from x in _HCoreContext.SCProduct
                                                    where x.StatusId == HelperStatus.Default.Active
                                                    select new OProduct.List
                                                    {
                                                        ReferenceId = x.Id,
                                                        ReferenceKey = x.Guid,

                                                        Name = x.Name,


                                                        CategoryKey = x.Category.Guid,
                                                        CategoryName = x.Category.Name,

                                                        IconUrl = x.PrimaryStorage.Path,

                                                        DealerId = x.AccountId,
                                                        DealerKey = x.Account.Guid,
                                                        DealerName = x.Account.DisplayName,
                                                        DealerIconUrl = x.Account.IconStorage.Path,

                                                        VarientId = x.SCProductVarient.FirstOrDefault().Id,
                                                        VarientKey = x.SCProductVarient.FirstOrDefault().Guid,
                                                        ActualPrice = x.SCProductVarient.FirstOrDefault().ActualPrice,
                                                        SellingPrice = x.SCProductVarient.FirstOrDefault().SellingPrice,
                                                        CreateDate = x.CreateDate,
                                                    })
                                                  .Where(_Request.SearchCondition)
                                                  .OrderBy(_Request.SortExpression)
                                                  .Skip(_Request.Offset)
                                                  .Take(_Request.Limit)
                                                  .ToList();
                        #endregion
                        foreach (var Details in Data)
                        {
                            if (!string.IsNullOrEmpty(Details.IconUrl))
                            {
                                Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                                Details.DealerIconUrl = _AppConfig.StorageUrl + Details.DealerIconUrl;
                            }
                            else
                            {
                                Details.IconUrl = _AppConfig.Default_Icon;
                                Details.DealerIconUrl = _AppConfig.Default_Icon;
                            }
                        }
                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                        #endregion
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                        #endregion
                    }

                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store product varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreProductVarient(OProductVarient.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    if (!string.IsNullOrEmpty(_Request.ReferenceKey) && _Request.ReferenceId > 0)
                    {
                        #region Get Data
                        OProductVarient.Details _Details = (from x in _HCoreContext.SCProductVarient
                                                            where x.Guid == _Request.ReferenceKey
                                                            && x.Id == _Request.ReferenceId
                                                            select new OProductVarient.Details
                                                            {

                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,

                                                                ProductId = x.ProductId,
                                                                ProductKey = x.Product.Guid,
                                                                ProductName = x.Product.Name,


                                                                Sku = x.Sku,
                                                                Name = x.Name,

                                                                CategoryKey = x.Product.Category.Guid,
                                                                CategoryName = x.Product.Category.Name,


                                                                ActualPrice = x.ActualPrice,
                                                                SellingPrice = x.SellingPrice,

                                                                Description = x.Description,
                                                                MaximumQuantity = x.MaximumQuantity,
                                                                MinimumQuantity = x.MinimumQuantity,
                                                                ReferenceNumber = x.ReferenceNumber,

                                                                IconUrl = x.Product.PrimaryStorage.Path,

                                                                CreateDate = x.CreateDate,
                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                ModifyDate = x.ModifyDate,
                                                                ModifyByKey = x.ModifyBy.Guid,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,

                                                                ProductStatusId = x.Product.StatusId,
                                                                ProductStatusCode = x.Product.Status.SystemName,
                                                                ProductStatusName = x.Product.Status.Name,




                                                            })
                                                  .FirstOrDefault();
                        #endregion

                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        if (_Details != null)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, ResourceItems.HC200, ResourceItems.HC200M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                            #endregion
                        }
                    }
                    else
                    {
                        #region Get Data
                        OProductVarient.Details _Details = (from x in _HCoreContext.SCProductVarient
                                                            where x.Guid == _Request.ReferenceKey
                                                            && x.Id == _Request.ReferenceId
                                                            select new OProductVarient.Details
                                                            {

                                                                ReferenceId = x.Id,
                                                                ReferenceKey = x.Guid,

                                                                ProductId = x.ProductId,
                                                                ProductKey = x.Product.Guid,
                                                                ProductName = x.Product.Name,

                                                                Sku = x.Sku,
                                                                Name = x.Name,

                                                                ActualPrice = x.ActualPrice,
                                                                SellingPrice = x.SellingPrice,

                                                                Description = x.Description,
                                                                MaximumQuantity = x.MaximumQuantity,
                                                                MinimumQuantity = x.MinimumQuantity,
                                                                ReferenceNumber = x.ReferenceNumber,

                                                                IconUrl = x.Product.PrimaryStorage.Path,

                                                                CreateDate = x.CreateDate,
                                                                CreatedByKey = x.CreatedBy.Guid,
                                                                CreatedByDisplayName = x.CreatedBy.DisplayName,

                                                                ModifyDate = x.ModifyDate,
                                                                ModifyByKey = x.ModifyBy.Guid,
                                                                ModifyByDisplayName = x.ModifyBy.DisplayName,


                                                                StatusId = x.StatusId,
                                                                StatusCode = x.Status.SystemName,
                                                                StatusName = x.Status.Name,

                                                                ProductStatusId = x.Product.StatusId,
                                                                ProductStatusCode = x.Product.Status.SystemName,
                                                                ProductStatusName = x.Product.Status.Name,
                                                            })
                                                  .FirstOrDefault();
                        #endregion

                        #region Create  Response Object
                        _HCoreContext.Dispose();
                        #endregion
                        if (_Details != null)
                        {
                            if (!string.IsNullOrEmpty(_Details.IconUrl))
                            {
                                _Details.IconUrl = _AppConfig.StorageUrl + _Details.IconUrl;
                            }
                            else
                            {
                                _Details.IconUrl = _AppConfig.Default_Icon;
                            }
                            if (!string.IsNullOrEmpty(_Details.PosterUrl))
                            {
                                _Details.PosterUrl = _AppConfig.StorageUrl + _Details.PosterUrl;
                            }
                            else
                            {
                                _Details.PosterUrl = _AppConfig.Default_Poster;
                            }
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _Details, ResourceItems.HC200, ResourceItems.HC200M);
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                            #endregion
                        }
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetVarient", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }


        // Customer App Api
        /// <summary>
        /// Description: Gets the product dealer.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetProductDealer(OList.Request _Request)
        {

            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "DealerId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "DealerId", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                var _UserCoordinates = new GeoCoordinate(_Request.Latitude, _Request.Longitude);
                using (_HCoreContext = new HCoreContext())
                {
                    #region Set Default Limit
                    if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
                                             where x.StatusId == HelperStatus.Default.Active
                                             && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                             && x.SCProductVarientStockSubAccount.Any(a => a.Quantity > 0)
                                             && x.Latitude != 0
                                             && x.Longitude != 0
                                             && ((new GeoCoordinate(x.Latitude, x.Longitude).GetDistanceTo(_UserCoordinates)) / 1000 < 22)
                                             select new OProduct.DealerList
                                             {

                                                 DealerId = x.Owner.Id,
                                                 DealerKey = x.Owner.Guid,
                                                 DealerLocationId = x.Id,
                                                 DealerLocationKey = x.Guid,
                                                 DealerName = x.Owner.DisplayName,
                                                 DealerIconUrl = x.Owner.IconStorage.Path,

                                                 DealerLocationName = x.DisplayName,
                                                 DealerLocationAddress = x.Address,
                                                 DealerLocationLatitude = x.Latitude,
                                                 DealerLocationLongitude = x.Longitude,

                                                 Distance = 0,
                                                 Location = new GeoCoordinate(x.Latitude, x.Longitude),
                                                 Products = x.SCProductVarientStockSubAccount.Count(),
                                             })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(x => x.Location.GetDistanceTo(_UserCoordinates))
                                .Count();
                    #endregion
                    #region Get Data
                    List<OProduct.DealerList> Data = (from x in _HCoreContext.HCUAccount
                                                      where x.StatusId == HelperStatus.Default.Active
                                                      && x.AccountTypeId == Helpers.UserAccountType.MerchantStore
                                                      && x.SCProductVarientStockSubAccount.Any(a => a.Quantity > 0)
                                                      && x.Latitude != 0
                                                      && x.Longitude != 0
                                                      && ((new GeoCoordinate(x.Latitude, x.Longitude).GetDistanceTo(_UserCoordinates)) / 1000 < 22)
                                                      select new OProduct.DealerList
                                                      {

                                                          DealerId = x.Owner.Id,
                                                          DealerKey = x.Owner.Guid,
                                                          DealerLocationId = x.Id,
                                                          DealerLocationKey = x.Guid,
                                                          DealerName = x.Owner.DisplayName,
                                                          DealerIconUrl = x.Owner.IconStorage.Path,

                                                          DealerLocationName = x.DisplayName,
                                                          DealerLocationAddress = x.Address,
                                                          DealerLocationLatitude = x.Latitude,
                                                          DealerLocationLongitude = x.Longitude,

                                                          Distance = 0,
                                                          Location = new GeoCoordinate(x.Latitude, x.Longitude),
                                                          Products = x.SCProductVarientStockSubAccount.Count(),
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(x => x.Location.GetDistanceTo(_UserCoordinates))
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        Details.Distance = Math.Round(Details.Location.GetDistanceTo(_UserCoordinates) / 1000, 2);
                        if (!string.IsNullOrEmpty(Details.DealerIconUrl))
                        {
                            Details.DealerIconUrl = _AppConfig.StorageUrl + Details.DealerIconUrl;
                        }
                        else
                        {
                            Details.DealerIconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    //return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HC0001");
                    #endregion

                }
                #endregion


                //#region Add Default Request
                //if (string.IsNullOrEmpty(_Request.SearchCondition))
                //{
                //    #region Default Conditions
                //    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                //    #endregion
                //}
                //if (string.IsNullOrEmpty(_Request.SortExpression))
                //{
                //    #region Default Conditions
                //    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                //    #endregion
                //}
                //#endregion
                //#region Operation
                //using (_HCoreContext = new HCoreContext())
                //{
                //    #region Set Default Limit
                //    if (_Request.Limit == 0)
                //    {
                //        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                //    }
                //    #endregion
                //    #region Total Records
                //    _Request.TotalRecords = (from x in _HCoreContext.HCUAccount
                //                             where x.StatusId == HelperStatus.Default.Active
                //                             && x.AccountTypeId == Helpers.UserAccountType.Merchant
                //                             && x.SCProductAccount.Any() 
                //                             && x.HCUAccountOwnerOwner.Any(m => m.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                //                             select new OProduct.DealerList
                //                             {
                //                                 ReferenceId = x.Id,
                //                                 ReferenceKey = x.Guid,
                //                                 Name = x.Name,
                //                                 IconUrl = x.IconStorage.Path,
                //                                    Locations = x.HCUAccountOwnerOwner.Count(m=> m.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                //                             })
                //                        .Where(_Request.SearchCondition)
                //                .Count();
                //    #endregion
                //    #region Get Data
                //    List<OProduct.DealerList> Data = (from x in _HCoreContext.HCUAccount
                //                                where x.StatusId == HelperStatus.Default.Active
                //                             && x.AccountTypeId == Helpers.UserAccountType.Merchant
                //                                && x.SCProductAccount.Any()
                //                             && x.HCUAccountOwnerOwner.Any(m => m.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                //                                      select new OProduct.DealerList
                //                                {
                //                                    ReferenceId = x.Id,
                //                                    ReferenceKey = x.Guid,
                //                                    Name = x.Name,
                //                                    IconUrl = x.IconStorage.Path,
                //                                    Locations = x.HCUAccountOwnerOwner.Count(m=> m.AccountTypeId == Helpers.UserAccountType.MerchantStore)
                //                                })
                //                              .Where(_Request.SearchCondition)
                //                              .OrderBy(_Request.SortExpression)
                //                              .Skip(_Request.Offset)
                //                              .Take(_Request.Limit)
                //                              .ToList();
                //    #endregion
                //    foreach (var Details in Data)
                //    {
                //        if (!string.IsNullOrEmpty(Details.IconUrl))
                //        {
                //            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                //        }
                //        else
                //        {
                //            Details.IconUrl = _AppConfig.Default_Icon;
                //        }
                //    }
                //    #region Create  Response Object
                //    _HCoreContext.Dispose();
                //    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                //    #endregion
                //    #region Send Response
                //    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                //    #endregion
                //}
                //#endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetProduct", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the store product.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetStoreProduct(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "ReferenceId", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {
                    #region Set Default Limit
                    if (_Request.Limit == 0)
                    {
                        _Request.Limit = _AppConfig.DefaultRecordsLimit;
                    }
                    #endregion
                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.SCProductVarient
                                             where x.SCProductVarientStock.Any(m => m.SubAccountId == _Request.ReferenceId && (m.StatusId == HelperStatus.Default.Active || m.StatusId == HelperStatus.HCProduct.Active) && m.Quantity > 0)
                                                && (x.Product.StatusId == HelperStatus.Default.Active || x.Product.StatusId == HelperStatus.HCProduct.Active)
                                                && (x.StatusId == HelperStatus.Default.Active || x.StatusId == HelperStatus.HCProduct.Active)
                                             orderby x.SellingPrice ascending
                                             select new OProduct.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 Name = x.Name,

                                                 CategoryKey = x.Product.Category.Guid,
                                                 CategoryName = x.Product.Category.Name,

                                                 IconUrl = x.Product.PrimaryStorage.Path,

                                                 ActualPrice = x.ActualPrice,
                                                 SellingPrice = x.SellingPrice,

                                                 TActualPrice = x.SCProductVarientStock.Where(m => m.SubAccount.OwnerId == _Request.ReferenceId).FirstOrDefault().ActualPrice,
                                                 TSellingPrice = x.SCProductVarientStock.Where(m => m.SubAccount.OwnerId == _Request.ReferenceId).FirstOrDefault().SellingPrice,
                                             })
                                        .Where(_Request.SearchCondition)
                                .Count();
                    #endregion
                    #region Get Data
                    List<OProduct.List> Data = (from x in _HCoreContext.SCProductVarient
                                                where x.SCProductVarientStock.Any(m => m.SubAccountId == _Request.ReferenceId && (m.StatusId == HelperStatus.Default.Active || m.StatusId == HelperStatus.HCProduct.Active) && m.Quantity > 0)
                                                && (x.Product.StatusId == HelperStatus.Default.Active || x.Product.StatusId == HelperStatus.HCProduct.Active)
                                                && (x.StatusId == HelperStatus.Default.Active || x.StatusId == HelperStatus.HCProduct.Active)
                                                orderby x.SellingPrice ascending
                                                select new OProduct.List
                                                {
                                                    ReferenceId = x.Id,
                                                    ReferenceKey = x.Guid,

                                                    Name = x.Name,

                                                    CategoryKey = x.Product.Category.Guid,
                                                    CategoryName = x.Product.Category.Name,

                                                    IconUrl = x.Product.PrimaryStorage.Path,

                                                    ActualPrice = x.ActualPrice,
                                                    SellingPrice = x.SellingPrice,

                                                    TActualPrice = x.SCProductVarientStock.Where(m => m.SubAccount.OwnerId == _Request.ReferenceId).FirstOrDefault().ActualPrice,
                                                    TSellingPrice = x.SCProductVarientStock.Where(m => m.SubAccount.OwnerId == _Request.ReferenceId).FirstOrDefault().SellingPrice,
                                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var Details in Data)
                    {
                        if (Details.TActualPrice > 0)
                        {
                            Details.ActualPrice = Details.TActualPrice;
                        }
                        if (Details.TSellingPrice > 0)
                        {
                            Details.SellingPrice = Details.TSellingPrice;
                        }
                        if (!string.IsNullOrEmpty(Details.IconUrl))
                        {
                            Details.IconUrl = _AppConfig.StorageUrl + Details.IconUrl;
                        }
                        else
                        {
                            Details.IconUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetStoreProduct", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }

    }
}
