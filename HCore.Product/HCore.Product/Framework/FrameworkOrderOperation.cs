//==================================================================================
// FileName: FrameworkOrderOperation.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to order operation
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;
using static HCore.Helper.HCoreConstant.Helpers;
using HCore.Operations.Object;
using HCore.Operations;
using HCore.Integration.Paystack;
using HCore.Product.Helper;
using static HCore.Product.Helper.HelperProductConstant;
using static HCore.Helper.HCoreConstant.HelperStatus;
using static HCore.CoreConstant;

namespace HCore.Product.Framework
{
    public class ONotificationOrder
    {
        public long Id { get; set; }
        public string? Guid { get; set; }

        public string? OrderId { get; set; }
        public DateTime? OpenDate { get; set; }
        public string? OpenDateS { get; set; }
        public DateTime? CloseDate { get; set; }

        public long? CustomerId { get; set; }
        public string? CustomerName { get; set; }
        public string? CustomerMobileNumber { get; set; }
        public string? CustomerEmailAddress { get; set; }
        public string? CustomerIconUrl { get; set; }

        public long TotalItem { get; set; }
        public double? Amount { get; set; }
        public double? Charge { get; set; }
        public double? OtherCharge { get; set; }
        public double? DeliveryCharge { get; set; }
        public double? DiscountAmount { get; set; }
        public double? Commission { get; set; }
        public double? TotalAmount { get; set; }
        public string? PaymentStatusName { get; set; }
        public string? PaymentReference { get; set; }
        public double? RewardAmount { get; set; }
        public double? RewardUserAmount { get; set; }
        public double? RewardSystemAmount { get; set; }

        public long? DealerId { get; set; }
        public string? DealerKey { get; set; }
        public string? DealerName { get; set; }
        public string? DealerContactNumber { get; set; }
        public string? DealerEmailAddress { get; set; }
        public string? DealerIconUrl { get; set; }

        public string? DealerLocationName { get; set; }
        public string? DealerLocationAddress { get; set; }

        public int? DealerLocationRating { get; set; }
        public string? DealerLocationReview { get; set; }

        public DateTime? ExpectedDeliveryDate { get; set; }
        public string? ExpectedDeliveryDateS { get; set; }
        public DateTime? ActualDeliveryDate { get; set; }
        public string? ActualDeliveryDateS { get; set; }

        public string? DeliveryTypeName { get; set; }
        public string? DeliveryModeName { get; set; }
        public int? CustomerRating { get; set; }
        public string? CustomerReview { get; set; }

        public long? RiderId { get; set; }
        public string? RiderIconUrl { get; set; }
        public string? RiderDisplayName { get; set; }
        public string? RiderMobileNumber { get; set; }
        public string? RiderEmailAddress { get; set; }

        public int? RiderRating { get; set; }
        public string? RiderReview { get; set; }
        public double? RiderAmount { get; set; }
        public double? RiderCommissionAmount { get; set; }
        public double? RiderTotalAmount { get; set; }

        public int? EPTime { get; set; }

        public long? StatusId { get; set; }
        public string? OrderDeliveryCode { get; set; }
        public ONotificationOrderDeliverAddress DeliveryAddress { get; set; }
        public List<ONotificationOrderItem> Items { get; set; }
    }
    public class ONotificationOrderItem
    {
        public string? IconUrl { get; set; }
        public string? Name { get; set; }
        internal string ProductName { get; set; }
        public long? Quantity { get; set; }
        public double TotalAmount { get; set; }
    }
    public class ONotificationOrderDeliverAddress
    {
        public string? Name { get; set; }
        public string? MobileNumber { get; set; }
        public string? AddressLine1 { get; set; }
        public string? CityName { get; set; }
        public string? Instructions { get; set; }
    }
    public class FrameworkOrderOperation
    {
        OCoreTransaction.Request _CoreTransactionRequest;
        List<OCoreTransaction.TransactionItem> _TransactionItems;
        ManageCoreTransaction _ManageCoreTransaction;
        HCoreContext _HCoreContext;
        SCOrder _SCOrder;
        List<SCOrderItem> _SCOrderItems;
        SCOrderActivity _SCOrderActivity;
        SCOrderAddress _SCOrderAddress;
        HCUAccountParameter _HCUAccountParameter;
        List<SCOrderActivity> _SCOrderActivities;
        /// <summary>
        /// Description: Manages the notification.
        /// </summary>
        /// <param name="OrderId">The order identifier.</param>
        /// <param name="_UserReference">The user reference.</param>
        private void ManageNotification(long OrderId, OUserReference _UserReference)
        {
            //if (HostEnvironment == HostEnvironmentType.Live)
            //{
            using (HCoreContext _HCoreContext = new HCoreContext())
            {
                string CustomerEmailAddress = "harshal@thankucash.com";
                string DealerEmailAddress = "harshal@thankucash.com";
                string RiderEmailAddress = "harshal@thankucash.com";
                string SystemEmailAddress = "harshal@thankucash.com";
                var OrderDetails = _HCoreContext.SCOrder.Where(x => x.Id == OrderId).
                    Select(x => new ONotificationOrder
                    {
                        Id = x.Id,
                        Guid = x.Guid,
                        OrderId = x.OrderId,
                        OpenDate = x.OpenDate,
                        CloseDate = x.CloseDate,

                        CustomerId = x.CustomerId,
                        CustomerName = x.Customer.Name,
                        CustomerMobileNumber = x.Customer.MobileNumber,
                        CustomerEmailAddress = x.Customer.EmailAddress,
                        CustomerIconUrl = x.Customer.IconStorage.Path,

                        TotalItem = x.TotalItem,
                        Amount = x.Amount,
                        Charge = x.Charge,
                        OtherCharge = x.OtherCharge,
                        DeliveryCharge = x.DeliveryCharge,
                        DiscountAmount = x.DiscountAmount,
                        Commission = x.Commission,
                        TotalAmount = x.TotalAmount,
                        PaymentStatusName = x.PaymentStatus.Name,
                        PaymentReference = x.PaymentReference,
                        RewardAmount = x.RewardAmount,
                        RewardUserAmount = x.RewardUserAmount,
                        RewardSystemAmount = x.RewardSystemAmount,
                        DealerId = x.DealerId,
                        DealerKey = x.Dealer.Guid,
                        DealerName = x.Dealer.Name,
                        DealerContactNumber = x.Dealer.ContactNumber,
                        DealerEmailAddress = x.Dealer.EmailAddress,
                        DealerIconUrl = x.Dealer.IconStorage.Path,

                        DealerLocationName = x.DealerLocation.DisplayName,
                        DealerLocationAddress = x.DealerLocation.Address,

                        DealerLocationRating = x.DealerLocationRating,
                        DealerLocationReview = x.DealerLocationReview,

                        ExpectedDeliveryDate = x.ExpectedDeliveryDate,
                        ActualDeliveryDate = x.ActualDeliveryDate,

                        DeliveryTypeName = x.DeliveryType.Name,
                        DeliveryModeName = x.DeliveryMode.Name,
                        CustomerRating = x.CustomerRating,
                        CustomerReview = x.CustomerReview,

                        RiderId = x.RiderId,
                        RiderDisplayName = x.Rider.DisplayName,
                        RiderMobileNumber = x.Rider.MobileNumber,
                        RiderEmailAddress = x.Rider.EmailAddress,

                        RiderRating = x.RiderRating,
                        RiderReview = x.RiderReview,
                        RiderAmount = x.RiderAmount,
                        RiderCommissionAmount = x.RiderCommissionAmount,
                        RiderTotalAmount = x.RiderTotalAmount,
                        RiderIconUrl = x.Rider.IconStorage.Path,
                        EPTime = x.EPTime,
                        StatusId = x.StatusId,
                        OrderDeliveryCode = x.OrderDeliveryCode,

                    }).FirstOrDefault();

                if (OrderDetails.OpenDate != null)
                {
                    OrderDetails.OpenDateS = OrderDetails.OpenDate.Value.AddHours(1).ToString("dd-MM-yyyy HH:mm tt");
                }
                if (OrderDetails.ExpectedDeliveryDate != null)
                {
                    OrderDetails.ExpectedDeliveryDateS = OrderDetails.ExpectedDeliveryDate.Value.AddHours(1).ToString("dd-MM-yyyy HH:mm tt");
                }
                if (OrderDetails.ActualDeliveryDate != null)
                {
                    OrderDetails.ActualDeliveryDateS = OrderDetails.ActualDeliveryDate.Value.AddHours(1).ToString("dd-MM-yyyy HH:mm tt");
                }
                if (!string.IsNullOrEmpty(OrderDetails.CustomerIconUrl))
                {
                    OrderDetails.CustomerIconUrl = _AppConfig.StorageUrl + OrderDetails.CustomerIconUrl;
                }
                else
                {
                    OrderDetails.CustomerIconUrl = _AppConfig.Default_Icon;
                }
                if (!string.IsNullOrEmpty(OrderDetails.DealerIconUrl))
                {
                    OrderDetails.DealerIconUrl = _AppConfig.StorageUrl + OrderDetails.DealerIconUrl;
                }
                else
                {
                    OrderDetails.DealerIconUrl = _AppConfig.Default_Icon;
                }
                if (!string.IsNullOrEmpty(OrderDetails.RiderIconUrl))
                {
                    OrderDetails.RiderIconUrl = _AppConfig.StorageUrl + OrderDetails.RiderIconUrl;
                }
                else
                {
                    OrderDetails.RiderIconUrl = _AppConfig.Default_Icon;
                }
                OrderDetails.Items = _HCoreContext.SCOrderItem.Where(x => x.OrderId == OrderDetails.Id)
                    .Select(x => new ONotificationOrderItem
                    {
                        IconUrl = x.Varient.Product.PrimaryStorage.Path,
                        Name = x.Varient.Name,
                        ProductName = x.Varient.Product.Name,
                        Quantity = x.Quantity,
                        TotalAmount = x.TotalAmount,
                    }).ToList();
                foreach (var item in OrderDetails.Items)
                {
                    if (item.Name != item.ProductName)
                    {
                        item.Name = item.ProductName + " " + item.Name;
                    }
                    if (!string.IsNullOrEmpty(item.IconUrl))
                    {
                        item.IconUrl = _AppConfig.StorageUrl + item.IconUrl;
                    }
                    else
                    {
                        item.IconUrl = _AppConfig.Default_Icon;
                    }
                }
                OrderDetails.DeliveryAddress = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == OrderDetails.Id)
                    .Select(x => new ONotificationOrderDeliverAddress
                    {
                        Name = x.Name,
                        MobileNumber = x.MobileNumber,
                        AddressLine1 = x.AddressLine1,
                        CityName = x.City.Name,
                        Instructions = x.Instructions,

                    }).FirstOrDefault();
                string MerchantNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == OrderDetails.DealerId && x.StatusId == 2 && x.NotificationUrl != null).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                string UserNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == OrderDetails.CustomerId && x.StatusId == 2 && x.NotificationUrl != null).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                string NinjaNotificationUrl = _HCoreContext.HCUAccountSession.Where(x => x.AccountId == OrderDetails.RiderId && x.StatusId == 2 && x.NotificationUrl != null).OrderByDescending(x => x.LoginDate).Select(x => x.NotificationUrl).FirstOrDefault();
                if (!string.IsNullOrEmpty(OrderDetails.CustomerEmailAddress) && HostEnvironment == HostEnvironmentType.Live)
                {
                    CustomerEmailAddress = OrderDetails.CustomerEmailAddress;
                }
                if (!string.IsNullOrEmpty(OrderDetails.DealerEmailAddress) && HostEnvironment == HostEnvironmentType.Live)
                {
                    DealerEmailAddress = OrderDetails.DealerEmailAddress;
                }
                if (!string.IsNullOrEmpty(OrderDetails.RiderEmailAddress) && HostEnvironment == HostEnvironmentType.Live)
                {
                    RiderEmailAddress = OrderDetails.RiderEmailAddress;
                }
                if (HostEnvironment == HostEnvironmentType.Live)
                {
                    SystemEmailAddress = _AppConfig.CompanyNotificationEmailAddress;
                }
                #region System Notification
                if (OrderDetails.StatusId == OrderStatus.PendingConfirmation)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_PendingConfirmation, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.Confirmed)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_Confirmed, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.Preparing)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_Preparing, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.Ready)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_Ready, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.ReadyToPickup)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_ReadyToPickup, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.OutForDelivery)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_OutForDelivery, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.DeliveryFailed)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_DeliveryFailed, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.CancelledByUser)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_CancelledByUser, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.CancelledBySeller)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_CancelledBySeller, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.CancelledBySystem)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_CancelledBySystem, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                if (OrderDetails.StatusId == OrderStatus.Delivered)
                {
                    HCoreHelper.BroadCastEmail(SendGridEmailTemplateIds.ProductsManager.System.Order_Delivered, "ThankUCash", SystemEmailAddress, OrderDetails, _UserReference);
                }
                #endregion
                #region Manage Merchant


                if (OrderDetails.StatusId == OrderStatus.PendingConfirmation)
                {

                }
                if (OrderDetails.StatusId == OrderStatus.Confirmed)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "Your have new order", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    #endregion
                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "Your have new order", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                }
                if (OrderDetails.StatusId == OrderStatus.Preparing)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to preparing", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to preparing", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    #endregion
                }
                if (OrderDetails.StatusId == OrderStatus.Ready)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to ready", "order", OrderDetails.Id, OrderDetails.Guid, "View details");

                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to ready", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    #endregion
                }
                if (OrderDetails.StatusId == OrderStatus.ReadyToPickup)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to ready to pickup", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "#" + OrderDetails.Id + " Order Status Update", "Order status updated to ready to pickup", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    #endregion
                }
                if (OrderDetails.StatusId == OrderStatus.OutForDelivery)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order received by ambassador and its out for delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "#" + OrderDetails.Id + " Order Status Update", "Order received by ambassador and its out for delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    #endregion
                }
                if (OrderDetails.StatusId == OrderStatus.DeliveryFailed)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Ambassador unable to deliver order to customer", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "#" + OrderDetails.Id + " Order Status Update", "Ambassador unable to deliver order to customer", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    #endregion
                }
                if (OrderDetails.StatusId == OrderStatus.CancelledByUser)
                {
                }
                if (OrderDetails.StatusId == OrderStatus.CancelledBySeller)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order cancelled from your seller account", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    #endregion
                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "#" + OrderDetails.Id + " Order Status Update", "Order cancelled from your seller account", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                }
                if (OrderDetails.StatusId == OrderStatus.CancelledBySystem)
                {

                }
                if (OrderDetails.StatusId == OrderStatus.Delivered)
                {
                    #region Merchant
                    string MerchantTopic = "tucmerchant" + OrderDetails.DealerId;
                    if (HCoreConstant.HostEnvironment != HostEnvironmentType.Live)
                    {
                        MerchantTopic = "tucmerchant_test" + OrderDetails.DealerId;
                    }
                    HCoreHelper.SendPushToTopicMerchant(MerchantTopic, "order", "#" + OrderDetails.Id + " Order Status Update", "Order delivered to customer", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    if (!string.IsNullOrEmpty(MerchantNotificationUrl))
                    {
                        //HCoreHelper.SendPushToDeviceMerchant(MerchantNotificationUrl, "order", "#" + OrderDetails.Id + " Order Status Update", "Order delivered to customer", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    #endregion
                }
                if (OrderDetails.StatusId == OrderStatus.Confirmed)
                {
                    //if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                    //{
                    //    HCoreHelper.SendPushToTopicMerchant("tucmerchant" + OrderDetails.DealerId, "order", "Your have new order", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    //}
                    //else
                    //{
                    //    HCoreHelper.SendPushToTopicMerchant("tucmerchant_test" + OrderDetails.DealerId, "order", "Your have new order", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    //}
                }
                if (OrderDetails.StatusId == OrderStatus.PendingConfirmation)
                {
                    if (HCoreConstant.HostEnvironment == HostEnvironmentType.Live)
                    {
                        HCoreHelper.SendPushToTopic("ninjaorders", "ninjaneworder", "New order available", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    else
                    {
                        HCoreHelper.SendPushToTopic("ninjaorders_test", "ninjaneworder", "New order available", "New order received worth N" + OrderDetails.TotalAmount, "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    //if (!string.IsNullOrEmpty(UserNotificationUrl))
                    //{
                    //    HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order confirmed", "#" + OrderDetails.Id + "Order worth N" + OrderDetails.TotalAmount + " confirmed", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    //}
                }
                #endregion
                #region Manage Customer
                if (!string.IsNullOrEmpty(UserNotificationUrl))
                {
                    if (OrderDetails.StatusId == OrderStatus.PendingConfirmation)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "New order placed", "Once order confirmed by ambassador and seller delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.Confirmed)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Order confirmed by seller", "Seller has confirmed your order " + "#" + OrderDetails.Id + ". Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.Preparing)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order inprocess", "Dealer preparing your order " + "#" + OrderDetails.Id + ". Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.Ready)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " is ready", "Dealer prepared your order. Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.ReadyToPickup)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " is ready", "Your order is ready to pickup by agent. Delivery will start soon", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.OutForDelivery)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " is out for delivery", "Your order is out for delivery. Order delivery code is " + OrderDetails.OrderDeliveryCode + ". Share delivery code with person after delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.Delivered)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " deliverd.", "Thank U for buying with us.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.DeliveryFailed)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " delivery failed.", "We are not able to deliver your order.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.CancelledByUser)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " cancelled.", "You have cancelled your order.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.CancelledBySeller)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " cancelled.", "Seller have cancelled your order. Your order amount will be redunded to your account within 24 hours", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.CancelledBySystem)
                    {
                        HCoreHelper.SendPushToDevice(UserNotificationUrl, "order", "Your order #" + OrderDetails.Id + " cancelled.", "System have cancelled your order. Your order amount will be redunded to your account within 24 hours", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                }
                #endregion
                #region Manage Ninja
                if (!string.IsNullOrEmpty(NinjaNotificationUrl))
                {
                    if (OrderDetails.StatusId == OrderStatus.Confirmed)
                    {
                        HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order confirmed by dealer", "Dealer has confirmed  order #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.Preparing)
                    {
                        HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "Dealer preparing order #" + OrderDetails.Id, "Dealer started preparing order #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.Ready)
                    {
                        HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order ready to pickup.", "Order ready to pickup #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.ReadyToPickup)
                    {
                        HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order ready to pickup.", "Order ready to pickup #" + OrderDetails.Id + ". Reach dealer location to start delivery", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.OutForDelivery)
                    {
                        HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "Your order #" + OrderDetails.Id + " is out for delivery", "Get delivery code from customer for closing delivery.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.Delivered)
                    {
                        HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order delivered.", "Order #" + OrderDetails.Id + " delivered. Thank U for delivering customer order.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                    if (OrderDetails.StatusId == OrderStatus.DeliveryFailed)
                    {
                        HCoreHelper.SendPushToDevice(NinjaNotificationUrl, "ninjaneworder", "#" + OrderDetails.Id + " order delivery failed.", "Order #" + OrderDetails.Id + " delivered. Order delivery failed.", "order", OrderDetails.Id, OrderDetails.Guid, "View details");
                    }
                }
                #endregion

            }
            //}
        }
        /// <summary>
        /// Description: Initializes the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse InitializeOrder(OOrderManager.NewOrder _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.Products == null)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC200, ResourceItems.HCC200M);
                }
                if (_Request.Products.Count() == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC200, ResourceItems.HCC200M);
                }
                double OrderDeliveryCharge = HCoreHelper.RoundNumber(Convert.ToDouble(HelperProduct.GetConfiguration("orderdeliverycharge", _Request.DealerId, _Request.UserReference)), _AppConfig.SystemRoundPercentage);
                double OrderCommissionPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HelperProduct.GetConfiguration("ordercommission", _Request.DealerId, _Request.UserReference)), _AppConfig.SystemRoundPercentage);
                double NinjaCommissionAmount = HCoreHelper.RoundNumber(Convert.ToDouble(HelperProduct.GetConfiguration("ninjacommissionamount", _Request.DealerId, _Request.UserReference)), _AppConfig.SystemRoundPercentage);
                //double OrderDeliveryCharge = 500; 
                //double OrderCommissionPercentage = 15; 
                //double NinjaCommission = 1000;
                using (_HCoreContext = new HCoreContext())
                {
                    #region Code Block
                    DateTime CurrentTime = HCoreHelper.GetGMTDateTime();
                    long AddressId = _Request.Address.ReferenceId;
                    int DeliveryModeId = HelperProductConstant.DeliveryMode.Address;
                    if (!string.IsNullOrEmpty(_Request.DeliveryModeCode))
                    {
                        if (_Request.DeliveryModeCode == HelperProductConstant.DeliveryMode.PickUpS)
                        {
                            DeliveryModeId = HelperProductConstant.DeliveryMode.PickUp;
                        }
                        else
                        {
                            DeliveryModeId = HelperProductConstant.DeliveryMode.Address;
                        }
                    }
                    using (_HCoreContext = new HCoreContext())
                    {
                        var DealerDetails = _HCoreContext.HCUAccount
                            .Where(x => x.Id == _Request.DealerLocationId)
                            .Select(x => new
                            {
                                DealerId = x.OwnerId,
                            }).FirstOrDefault();

                        var CustomerInformation = _HCoreContext.HCUAccount
                           .Where(x => x.Id == _Request.AccountId)
                           .Select(x => new
                           {
                               StatusId = x.StatusId,
                           }).FirstOrDefault();
                        if (CustomerInformation.StatusId != Default.Active)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC202, ResourceItems.HCC202M);
                        }


                        double Amount = 0;
                        double Charge = 0;
                        double DiscountAmount = 0;
                        double TotalAmount = 0;
                        long TotalQuantity = 0;
                        _SCOrderItems = new List<SCOrderItem>();
                        foreach (var ProductVarient in _Request.Products)
                        {
                            var ProductVarientDetails = _HCoreContext.SCProductVarient
                                .Where(x => x.Id == ProductVarient.VarientId
                                && x.StatusId == HCProduct.Active
                                && x.Product.StatusId == HCProduct.Active
                                && x.ProductId == ProductVarient.ProductId)
                                .Select(x => new
                                {
                                    ReferenceId = x.Id,
                                    ReferenceKey = x.Guid,
                                    //ProductId = x.ProductId,
                                    //ProductKey = x.Product.Guid,
                                    //ProductName = x.Product.Name,
                                    //Name = x.Name,
                                    //SystemName = x.SystemName,
                                    //Description = x.Description,
                                    ActualPrice = x.ActualPrice,
                                    SellingPrice = x.SellingPrice,
                                    //MinimumQuantity = x.MinimumQuantity,
                                    //MaximumQuantity = x.MaximumQuantity,
                                    //IconUrl = x.Product.PrimaryStorage.Path,
                                    //CreateDate = x.CreateDate,
                                    //CreatedByKey = x.CreatedBy.Guid,
                                    //CreatedByDisplayName = x.CreatedBy.DisplayName,
                                    //ModifyDate = x.ModifyDate,
                                    //ModifyByKey = x.ModifyBy.Guid,
                                    //ModifyByDisplayName = x.ModifyBy.DisplayName,
                                    //StatusId = x.StatusId,
                                    //StatusCode = x.Status.SystemName,
                                    //StatusName = x.Status.Name
                                }).FirstOrDefault();
                            if (ProductVarientDetails != null)
                            {
                                Amount += Math.Round(ProductVarientDetails.ActualPrice * ProductVarient.Quantity, 2);
                                DiscountAmount += Math.Round((ProductVarientDetails.ActualPrice * ProductVarient.Quantity) - (ProductVarientDetails.SellingPrice * ProductVarient.Quantity), 2);
                                TotalAmount += Math.Round(ProductVarientDetails.SellingPrice * ProductVarient.Quantity, 2);
                                TotalQuantity += ProductVarient.Quantity;
                                _SCOrderItems.Add(new SCOrderItem
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    VarientId = ProductVarientDetails.ReferenceId,
                                    UnitPrice = ProductVarientDetails.SellingPrice,
                                    Quantity = (int)ProductVarient.Quantity,
                                    Amount = Math.Round(ProductVarientDetails.ActualPrice * ProductVarient.Quantity, 2),
                                    DiscountAmount = Math.Round((ProductVarientDetails.ActualPrice * ProductVarient.Quantity) - (ProductVarientDetails.SellingPrice * ProductVarient.Quantity)),
                                    TotalAmount = Math.Round(ProductVarientDetails.SellingPrice * ProductVarient.Quantity, 2),
                                    CreateDate = CurrentTime,
                                    CreatedById = _Request.UserReference.AccountId,
                                    StatusId = OrderStatus.New,
                                });
                            }
                        }
                        double CommissionAmount = 0;
                        if (OrderCommissionPercentage > 0)
                        {
                            CommissionAmount = HCoreHelper.GetPercentage(TotalAmount, OrderCommissionPercentage);
                        }
                        _SCOrder = new SCOrder();
                        //_SCOrder.Guid = HCoreHelper.GenerateGuid();
                        _SCOrder.Guid = "TUCM" + _Request.AccountId + "O" + HCoreHelper.GenerateDateString();
                        _SCOrder.OrderId = HCoreHelper.GenerateRandomNumber(10);
                        _SCOrder.OpenDate = CurrentTime;
                        _SCOrder.CustomerId = _Request.AccountId;
                        _SCOrder.TotalItem = (int)TotalQuantity;
                        _SCOrder.Amount = Amount;
                        _SCOrder.Charge = Charge;
                        _SCOrder.OtherCharge = 0;
                        if (DeliveryModeId == HelperProductConstant.DeliveryMode.Address)
                        {
                            _SCOrder.DeliveryCharge = OrderDeliveryCharge;
                        }
                        else
                        {
                            _SCOrder.DeliveryCharge = 0;
                        }
                        _SCOrder.DiscountAmount = DiscountAmount;
                        if (CommissionAmount > 0)
                        {
                            _SCOrder.Commission = CommissionAmount;
                        }
                        else
                        {
                            _SCOrder.Commission = 0;
                        }
                        if (DeliveryModeId == HelperProductConstant.DeliveryMode.Address)
                        {
                            _SCOrder.TotalAmount = Math.Round(TotalAmount + OrderDeliveryCharge, 2);
                        }
                        else
                        {
                            _SCOrder.TotalAmount = Math.Round(TotalAmount, 2);
                        }
                        _SCOrder.PaymentStatusId = HelperProductConstant.PaymentStatus.Pending;
                        _SCOrder.DealerId = DealerDetails.DealerId;
                        _SCOrder.DealerLocationId = _Request.DealerLocationId;
                        _SCOrder.ExpectedDeliveryDate = CurrentTime.AddHours(2);
                        _SCOrder.DeliveryTypeId = DeliveryModeId;
                        _SCOrder.DeliveryModeId = DeliveryModeId;
                        _SCOrder.PriorityId = DeliveryPriority.Low;
                        _SCOrder.RiderAmount = ((TotalAmount - OrderDeliveryCharge) - CommissionAmount) + OrderDeliveryCharge;
                        _SCOrder.RiderCommissionAmount = Math.Round(NinjaCommissionAmount, 2);
                        _SCOrder.RiderTotalAmount = _SCOrder.RiderAmount + _SCOrder.RiderCommissionAmount;
                        _SCOrder.OrderDeliveryCode = HCoreHelper.GenerateRandomNumber(4);
                        _SCOrder.CreateDate = CurrentTime;
                        _SCOrder.CreatedById = _Request.UserReference.AccountId;
                        _SCOrder.StatusId = OrderStatus.New;
                        if (DeliveryModeId == HelperProductConstant.DeliveryMode.Address)
                        {
                            _SCOrderAddress = new SCOrderAddress();
                            _SCOrderAddress.Guid = HCoreHelper.GenerateGuid();
                            _SCOrderAddress.TypeId = 506;
                            _SCOrderAddress.Name = _Request.Address.Name;
                            _SCOrderAddress.MobileNumber = _Request.Address.MobileNumber;
                            _SCOrderAddress.AlternateMobileNumber = _Request.Address.AlternateMobileNumber;
                            _SCOrderAddress.EmailAddress = _Request.Address.EmailAddress;
                            _SCOrderAddress.AddressLine1 = _Request.Address.AddressLine1;
                            _SCOrderAddress.AddressLine2 = _Request.Address.AddressLine2;
                            _SCOrderAddress.Landmark = _Request.Address.Landmark;
                            _SCOrderAddress.ZipCode = _Request.Address.ZipCode;
                            _SCOrderAddress.MapAddress = _Request.Address.MapAddress;
                            _SCOrderAddress.CityId = 1;
                            _SCOrderAddress.AreaId = 1;
                            _SCOrderAddress.StateId = 1;
                            _SCOrderAddress.CountryId = 1;
                            _SCOrderAddress.Latitude = _Request.Address.Latitude;
                            _SCOrderAddress.Longitude = _Request.Address.Longitude;
                            _SCOrderAddress.Instructions = _Request.Address.Instructions;
                            _SCOrderAddress.CreateDate = CurrentTime;
                            _SCOrderAddress.CreatedById = _Request.UserReference.AccountId;
                            _SCOrderAddress.StatusId = Default.Active;
                        }
                        _SCOrder.SCOrderAddress.Add(_SCOrderAddress);
                        _SCOrder.SCOrderActivity.Add(new SCOrderActivity
                        {
                            Guid = HCoreHelper.GenerateGuid(),
                            Title = "Order created",
                            Description = "New order created",
                            OrderStatusId = OrderStatus.New,
                            CreateDate = CurrentTime,
                            CreatedById = _Request.UserReference.AccountId,
                            StatusId = Default.Active
                        });
                        _SCOrder.SCOrderItem = _SCOrderItems;
                        _HCoreContext.SCOrder.Add(_SCOrder);
                        _HCoreContext.SaveChanges();
                        var _OrderDetails = new
                        {
                            ReferenceId = _SCOrder.Id,
                            ReferenceKey = _SCOrder.Guid,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OrderDetails, "HCP120", "Order created");
                        #endregion
                    }
                    #endregion
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("InitializeOrder", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Confirms the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse ConfirmOrder(OOrderManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.PaymentReference))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP114", "Payment reference missing");
                    #endregion
                }
                #region Code Block
                #region Operations
                //if (_Request.Type == "cardc" && _Request.CardId > 0)
                //{
                //    using (_HCoreContext = new HCoreContext())
                //    {
                //        var OrderDetails = _HCoreContext.SCOrder
                //             .Where(x => x.Id == _Request.OrderId && x.Guid == _Request.OrderKey
                //             && x.Guid == _Request.OrderKey).FirstOrDefault();
                //        if (OrderDetails != null)
                //        {
                //            var Varients = _HCoreContext.SCOrderItem.Where(x => x.OrderId == OrderDetails.Id).ToList();
                //            foreach (var VarientItem in Varients)
                //            {
                //                var OrderStockLocation = _HCoreContext.SCProductVarientStock.Where(x => x.VarientId == VarientItem.VarientId && x.SubAccountId == OrderDetails.DealerLocationId).FirstOrDefault();
                //                if (OrderStockLocation != null)
                //                {
                //                    OrderStockLocation.Quantity -= VarientItem.Quantity;
                //                    OrderStockLocation.ModifyDate = HCoreHelper.GetGMTDateTime();
                //                    if (OrderStockLocation.Quantity < 0)
                //                    {
                //                        OrderStockLocation.Quantity = 0;
                //                    }
                //                }
                //            }
                //            var ChargeDetails = _HCoreContext.HCUAccountParameter.Where(x => x.Id == _Request.CardId && x.AccountId == _Request.AccountId)
                //            .Select(x => new { AuthCode = x.Guid, x.Account.EmailAddress }).FirstOrDefault();
                //            if (ChargeDetails != null)
                //            {
                //                var CInfo = _HCoreContext.HCUAccountParameter.Where(x => x.Id == _Request.CardId && x.AccountId == _Request.AccountId)
                //                .FirstOrDefault();
                //                if (CInfo != null)
                //                {
                //                    CInfo.ModifyDate = HCoreHelper.GetGMTDateTime();
                //                    CInfo.ModifyById = _Request.UserReference.AccountId;
                //                    _HCoreContext.SaveChanges();
                //                }
                //                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "CHARGEFAIL", "Unable to make payment from selected card. Please try other card or select new method");
                //                //OChargeData _PayStackCharge = GetPayStackCharge(ChargeDetails.EmailAddress, OrderDetails.TotalAmount, ChargeDetails.AuthCode, _Request.AccountId + "" + HCoreHelper.GenerateRandomNumber(10), _Request.UserReference);
                //                //if (_PayStackCharge.status == true)
                //                //{
                //                //    _Request.PaymentReference = _PayStackCharge.data.reference;
                //                //}
                //                //else
                //                //{
                //                //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "CHARGEFAIL", "Unable to make payment from selected card. Please try other card or select new method");
                //                //}
                //            }
                //            else
                //            {
                //                #region Send Response
                //                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0001");
                //                #endregion
                //            }
                //        }
                //        else
                //        {
                //            #region Send Response
                //            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP115", "Order details not found");
                //            #endregion
                //        }

                //    }
                //}
                OPayStackResponseData _PayStackResponseData = PaystackTransaction.GetTransaction(_Request.PaymentReference, _AppConfig.PaystackPrivateKey);
                if (_PayStackResponseData != null && _PayStackResponseData.status == "success")
                {
                    if (_PayStackResponseData.authorization != null)
                    {
                        if (_PayStackResponseData.authorization.reusable)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var CardDetailsCheck = _HCoreContext.HCUAccountParameter.Where(x => x.Guid == _PayStackResponseData.authorization.authorization_code && x.AccountId == _Request.AccountId && x.TypeId == 465).FirstOrDefault();
                                if (CardDetailsCheck == null)
                                {
                                    _HCUAccountParameter = new HCUAccountParameter();
                                    _HCUAccountParameter.Guid = _PayStackResponseData.authorization.authorization_code;
                                    _HCUAccountParameter.TypeId = 465;
                                    _HCUAccountParameter.AccountId = _Request.AccountId;
                                    _HCUAccountParameter.Name = _PayStackResponseData.authorization.brand;
                                    _HCUAccountParameter.SystemName = _PayStackResponseData.authorization.bin;
                                    _HCUAccountParameter.Value = _PayStackResponseData.authorization.last4;
                                    _HCUAccountParameter.SubValue = _PayStackResponseData.authorization.exp_month;
                                    _HCUAccountParameter.Description = _PayStackResponseData.authorization.exp_year;
                                    _HCUAccountParameter.Data = _PayStackResponseData.authorization.channel;
                                    _HCUAccountParameter.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _HCUAccountParameter.CreatedById = _Request.UserReference.AccountId;
                                    _HCUAccountParameter.StatusId = Default.Active;
                                    _HCoreContext.HCUAccountParameter.Add(_HCUAccountParameter);
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                else
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC201, ResourceItems.HCC201M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var OrderDetails = _HCoreContext.SCOrder
                            .Where(x => x.Id == _Request.OrderId && x.Guid == _Request.OrderKey
                            && x.Guid == _Request.OrderKey).FirstOrDefault();
                    if (OrderDetails != null)
                    {
                        var CustomerDetails = _HCoreContext.HCUAccount.Where(x => x.Id == OrderDetails.CustomerId)
                            .Select(x => new
                            {
                                EmailAddress = x.EmailAddress,
                                MobileNumber = x.MobileNumber,
                                FirstName = x.FirstName,
                            }).FirstOrDefault();
                        //if (OrderDetails.PaymentStatusId == 483)
                        //{
                        OrderDetails.PaymentReference = _Request.PaymentReference;
                        OrderDetails.PaymentStatusId = HelperProductConstant.PaymentStatus.Paid;
                        OrderDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        OrderDetails.ModifyById = _Request.UserReference.AccountId;
                        OrderDetails.StatusId = OrderStatus.PendingConfirmation;
                        var OrderItems = _HCoreContext.SCOrderItem.Where(x => x.OrderId == OrderDetails.Id).ToList();
                        foreach (var OrderItem in OrderItems)
                        {
                            var StoreStock = _HCoreContext.SCProductVarientStock.Where(x => x.SubAccountId == OrderDetails.DealerLocationId && x.VarientId == OrderItem.VarientId).FirstOrDefault();
                            if (StoreStock != null)
                            {
                                StoreStock.Quantity -= OrderItem.Quantity;
                                if (StoreStock.Quantity < 0)
                                {
                                    StoreStock.Quantity = 0;
                                }
                            }
                            OrderItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                            OrderItem.ModifyById = _Request.UserReference.AccountId;
                        }
                        _SCOrderActivities = new List<SCOrderActivity>();
                        _SCOrderActivity = new SCOrderActivity
                        {
                            Guid = HCoreHelper.GenerateGuid(),
                            OrderId = OrderDetails.Id,
                            Title = "Order payment received",
                            Description = "Payment received. Payment Reference Number : " + _Request.PaymentReference,
                            OrderStatusId = OrderStatus.PendingConfirmation,
                            CreateDate = HCoreHelper.GetGMTDateTime(),
                            CreatedById = _Request.UserReference.AccountId,
                            StatusId = Default.Active
                        };
                        _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                        _SCOrderActivity = new SCOrderActivity
                        {
                            Guid = HCoreHelper.GenerateGuid(),
                            OrderId = OrderDetails.Id,
                            Title = "Order Confirmation Pending For Delivery",
                            Description = "Order confirmation pending from system",
                            OrderStatusId = OrderStatus.PendingConfirmation,
                            CreateDate = HCoreHelper.GetGMTDateTime(),
                            CreatedById = _Request.UserReference.AccountId,
                            StatusId = Default.Active
                        };
                        _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                        _HCoreContext.SaveChanges();

                        #region PostProcessNewOrder
                        ManageNotification(OrderDetails.Id, _Request.UserReference);
                        //var system = ActorSystem.Create("ActorOrderNotification");
                        //var greeter = system.ActorOf<ActorOrderNotification>("ActorOrderNotification");
                        //greeter.Tell(_Request);
                        #endregion
                        var _OrderDetails = new
                        {
                            OrderId = _Request.OrderId,
                            OrderKey = _Request.OrderKey,
                        };
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OrderDetails, "HCP117", "Thank you for placing order. We have received your order. We will process your order soon.");
                        #endregion
                        //}
                        //else
                        //{
                        //    #region Send Response
                        //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP116", "Invalid order status");
                        //    #endregion
                        //}
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HCP115", "Order details not found");
                        #endregion
                    }
                }
                #endregion
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("ConfirmOrder", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Cancels the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse CancelOrder(OOrderManager.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.OrderId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCP600, ResourceItems.HCP600M);
                }
                if (string.IsNullOrEmpty(_Request.OrderKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCP600, ResourceItems.HCP600M);
                }
                if (_Request.AccountId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCP601, ResourceItems.HCP601M);
                }
                if (string.IsNullOrEmpty(_Request.AccountKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCP601, ResourceItems.HCP601M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    SCOrder Details = _HCoreContext.SCOrder.Where(x => x.Id == _Request.OrderId && x.Guid == _Request.OrderKey).FirstOrDefault();
                    if (Details != null)
                    {
                        int StatusId = OrderStatus.CancelledBySeller;
                        if (_Request.Type == "customer")
                        {
                            StatusId = OrderStatus.CancelledByUser;
                            if (Details.StatusId == OrderStatus.PendingConfirmation || Details.StatusId == OrderStatus.New)
                            {
                                var OrderItems = _HCoreContext.SCOrderItem.Where(x => x.OrderId == Details.Id).ToList();
                                foreach (var OrderItem in OrderItems)
                                {
                                    OrderItem.StatusId = StatusId;
                                    OrderItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    OrderItem.ModifyById = _Request.UserReference.AccountId;
                                }
                                Details.StatusId = StatusId;
                                Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                Details.ModifyById = _Request.UserReference.AccountId;
                                _SCOrderActivity = new SCOrderActivity
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    OrderId = Details.Id,
                                    Title = "Order cancelled by user",
                                    Description = "Order cancelled by user",
                                    OrderStatusId = StatusId,
                                    Comment = _Request.Comment,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    CreatedById = _Request.UserReference.AccountId,
                                    StatusId = Default.Active
                                };
                                _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                                _HCoreContext.SaveChanges();
                                #region PostProcessNewOrder
                                ManageNotification(_Request.OrderId, _Request.UserReference);
                                //var system = ActorSystem.Create("ActorOrderNotification");
                                //var greeter = system.ActorOf<ActorOrderNotification>("ActorOrderNotification");
                                //greeter.Tell(_Request);
                                #endregion
                                object _Response = new
                                {
                                    ReferenceId = _Request.OrderId,
                                    ReferenceKey = _Request.OrderKey,
                                };

                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC206, ResourceItems.HCC206M);

                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC204, ResourceItems.HCC204M);
                            }
                        }
                        else
                        {
                            StatusId = OrderStatus.CancelledBySeller;
                            if (Details.StatusId == OrderStatus.Confirmed)
                            {
                                var OrderItems = _HCoreContext.SCOrderItem.Where(x => x.OrderId == Details.Id).ToList();
                                foreach (var OrderItem in OrderItems)
                                {
                                    OrderItem.StatusId = StatusId;
                                    OrderItem.ModifyDate = HCoreHelper.GetGMTDateTime();
                                    OrderItem.ModifyById = _Request.UserReference.AccountId;
                                }
                                Details.StatusId = StatusId;
                                Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                                Details.ModifyById = _Request.UserReference.AccountId;
                                _SCOrderActivity = new SCOrderActivity
                                {
                                    Guid = HCoreHelper.GenerateGuid(),
                                    OrderId = Details.Id,
                                    Title = "Order cancelled by seller",
                                    Description = "Order cancelled by seller",
                                    OrderStatusId = StatusId,
                                    Comment = _Request.Comment,
                                    CreateDate = HCoreHelper.GetGMTDateTime(),
                                    CreatedById = _Request.UserReference.AccountId,
                                    StatusId = Default.Active
                                };
                                _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                                _HCoreContext.SaveChanges();
                                #region PostProcessNewOrder
                                ManageNotification(_Request.OrderId, _Request.UserReference);
                                //var system = ActorSystem.Create("ActorOrderNotification");
                                //var greeter = system.ActorOf<ActorOrderNotification>("ActorOrderNotification");
                                //greeter.Tell(_Request);
                                #endregion
                                object _Response = new
                                {
                                    ReferenceId = _Request.OrderId,
                                    ReferenceKey = _Request.OrderKey,
                                };
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC207, ResourceItems.HCC207M);
                            }
                            else
                            {
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC204, ResourceItems.HCC204M);
                            }
                        }
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                return HCoreHelper.LogException("CancelOrder", _Exception, _Request.UserReference);
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the order rating.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateOrderRating(OOrder.Rating _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    SCOrder Details = _HCoreContext.SCOrder.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        if (_Request.RiderRating > 0)
                        {
                            Details.RiderRating = _Request.RiderRating;
                        }
                        if (!string.IsNullOrEmpty(_Request.RiderReview))
                        {
                            Details.RiderReview = _Request.RiderReview;
                        }

                        if (_Request.CustomerRating > 0)
                        {
                            Details.CustomerRating = _Request.CustomerRating;
                        }
                        if (!string.IsNullOrEmpty(_Request.CustomerReview))
                        {
                            Details.CustomerReview = _Request.CustomerReview;
                        }

                        if (_Request.DealerLocationRating > 0)
                        {
                            Details.DealerLocationRating = _Request.DealerLocationRating;
                        }
                        if (!string.IsNullOrEmpty(_Request.DealerLocationReview))
                        {
                            Details.DealerLocationReview = _Request.DealerLocationReview;
                        }

                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC203, ResourceItems.HCC203M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Upates the order status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpateOrderStatus(OOrder.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                    }
                    SCOrder Details = _HCoreContext.SCOrder.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        if (StatusId == OrderStatus.Delivered && !string.IsNullOrEmpty(_Request.OrderDeliveryCode) && Details.OrderDeliveryCode != _Request.OrderDeliveryCode)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCP604, ResourceItems.HCP604M);
                        }
                        if (Details.RiderId != null && StatusId == OrderStatus.Confirmed)
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCP606, ResourceItems.HCP606M);
                        }
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        if (StatusId == OrderStatus.PendingConfirmation)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order placed",
                                Description = "#" + Details.OrderId + " order placed",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC211, ResourceItems.HCC211M);
                        }
                        else if (StatusId == OrderStatus.Confirmed)
                        {
                            if (_Request.AgentId < 0)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC222, ResourceItems.HCC222M);
                            }

                            Details.StatusId = StatusId;
                            Details.RiderId = _Request.AgentId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order confirmed for delivery",
                                Description = "Ambassador assigned for order and ready for processing.",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC212, "#" + Details.OrderId + ResourceItems.HCC212M);
                        }
                        else if (StatusId == OrderStatus.Preparing)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            if (_Request.EPTime > 0)
                            {
                                Details.EPTime = _Request.EPTime;
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order status updated to preparing",
                                Description = "Dealer started preparing your order",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC213, "#" + Details.OrderId + ResourceItems.HCC213M);
                        }
                        else if (StatusId == OrderStatus.Ready)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order ready to deliver.",
                                Description = "Order prepared by dealer",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC214, "#" + Details.OrderId + ResourceItems.HCC214M);
                        }
                        else if (StatusId == OrderStatus.ReadyToPickup)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order ready to pickup",
                                Description = "Order prepared by dealer and ready to pickup",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC215, "#" + Details.OrderId + ResourceItems.HCC215M);
                        }
                        else if (StatusId == OrderStatus.OutForDelivery)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order out for delivery",
                                Description = "Order prepared by dealer",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC216, "#" + Details.OrderId + ResourceItems.HCC216M);
                        }
                        else if (StatusId == OrderStatus.DeliveryFailed)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order delivery failed",
                                Description = "Ambassador unable to delivery order",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC217, "#" + Details.OrderId + ResourceItems.HCC217M);
                        }
                        else if (StatusId == OrderStatus.CancelledByUser)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order cancelled by customer",
                                Description = "Order cancelled by customer",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC218, "#" + Details.OrderId + ResourceItems.HCC218M);
                        }
                        else if (StatusId == OrderStatus.CancelledBySeller)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order cancelled by seller",
                                Description = "Order cancelled by seller",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC219, "#" + Details.OrderId + ResourceItems.HCC219M);
                        }
                        else if (StatusId == OrderStatus.CancelledBySystem)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order cancelled by system",
                                Description = "Order cancelled by system",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC220, "#" + Details.OrderId + ResourceItems.HCC220M);
                        }
                        else if (StatusId == OrderStatus.Delivered)
                        {
                            if (Details.StatusId == StatusId)
                            {
                                _HCoreContext.Dispose();
                                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC223, ResourceItems.HCC223M);
                            }
                            Details.StatusId = StatusId;
                            _SCOrderActivity = new SCOrderActivity
                            {
                                Guid = HCoreHelper.GenerateGuid(),
                                OrderId = Details.Id,
                                Title = "Order delivered",
                                Description = "Order delivered",
                                Comment = _Request.Comment,
                                OrderStatusId = OrderStatus.Confirmed,
                                CreateDate = HCoreHelper.GetGMTDateTime(),
                                CreatedById = _Request.UserReference.AccountId,
                                StatusId = Default.Active
                            };
                            _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                            _HCoreContext.SaveChanges();
                            double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", (long)Details.DealerId)), _AppConfig.SystemRoundPercentage);
                            double UserRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", (long)Details.DealerId)), _AppConfig.SystemRoundPercentage);
                            using (_HCoreContext = new HCoreContext())
                            {
                                double InvoiceAmount = (Details.TotalAmount - Details.DeliveryCharge);
                                double RewardAmount = HCoreHelper.GetPercentage(InvoiceAmount, RewardPercentage);
                                double UserRewardAmount = HCoreHelper.GetPercentage(RewardAmount, UserRewardPercentage);
                                double SystemRewardAmount = RewardAmount - UserRewardAmount;
                                if (RewardAmount > 0)
                                {
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.CustomerId = (long)Details.CustomerId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    _CoreTransactionRequest.ParentId = (long)Details.DealerId;
                                    _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceNumber = Details.PaymentReference;
                                    _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                    _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = (long)Details.DealerId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.Merchant,

                                        Amount = RewardAmount,
                                        Comission = 0,
                                        TotalAmount = RewardAmount,
                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = (long)Details.CustomerId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUC,
                                        Amount = UserRewardAmount,
                                        TotalAmount = UserRewardAmount,
                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    });
                                    if (SystemRewardAmount > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = SystemAccounts.ThankUCashMerchant,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.CashReward,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = SystemRewardAmount,
                                            TotalAmount = SystemRewardAmount,
                                        });
                                    }

                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                }
                                if (Details.RiderAmount > 0 && Details.RiderId != null)
                                {
                                    #region Credit Ninja Delivery Amount
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.CustomerId = (long)Details.RiderId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    _CoreTransactionRequest.ParentId = (long)Details.DealerId;
                                    _CoreTransactionRequest.InvoiceAmount = (double)Details.RiderAmount;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = (double)Details.TotalAmount;
                                    _CoreTransactionRequest.ReferenceNumber = Details.Id.ToString();
                                    _CoreTransactionRequest.ReferenceAmount = (double)Details.RiderTotalAmount;
                                    _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = (long)Details.RiderId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.Ninja.DeliveryAmount,
                                        SourceId = TransactionSource.Ninja,
                                        Amount = (double)Details.RiderAmount,
                                        Comission = 0,
                                        TotalAmount = (double)Details.RiderAmount,
                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    });
                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                    #endregion
                                }
                                if (Details.RiderCommissionAmount > 0 && Details.RiderId != null)
                                {
                                    #region Credit Ninja Delivery Commission Amount
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.CustomerId = (long)Details.RiderId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    _CoreTransactionRequest.ParentId = (long)Details.DealerId;
                                    _CoreTransactionRequest.InvoiceAmount = (double)Details.RiderCommissionAmount;
                                    _CoreTransactionRequest.ReferenceAmount = (double)Details.RiderTotalAmount;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = (double)Details.TotalAmount;
                                    _CoreTransactionRequest.ReferenceNumber = Details.Id.ToString();
                                    _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = (long)Details.RiderId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.Ninja.DeliveryCommission,
                                        SourceId = TransactionSource.Ninja,
                                        Amount = (double)Details.RiderCommissionAmount,
                                        Comission = 0,
                                        TotalAmount = (double)Details.RiderCommissionAmount,
                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    });
                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                    #endregion
                                }
                            }
                            #region PostProcessNewOrder
                            ManageNotification(_Request.ReferenceId, _Request.UserReference);
                            #endregion
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC221, "#" + Details.OrderId + ResourceItems.HCC221M);
                        }
                        else
                        {
                            _HCoreContext.Dispose();
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _Response, ResourceItems.HCC224, ResourceItems.HCC224M);
                        }




                        //if (_Request.AgentId != 0 && StatusId == OrderStatus.Confirmed)
                        //{
                        //    if (_Request.EPTime > 0)
                        //    {
                        //        Details.EPTime = _Request.EPTime;
                        //    }
                        //    Details.RiderId = _Request.AgentId;
                        //    Details.StatusId = OrderStatus.Confirmed;
                        //    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        //    Details.ModifyById = _Request.UserReference.AccountId;
                        //    _SCOrderActivity = new SCOrderActivity
                        //    {
                        //        Guid = HCoreHelper.GenerateGuid(),
                        //        OrderId = Details.Id,
                        //        Title = "Order confirmed for delivery",
                        //        Description = "Order confirmed for delivery",
                        //        OrderStatusId = OrderStatus.Confirmed,
                        //        CreateDate = HCoreHelper.GetGMTDateTime(),
                        //        CreatedById = _Request.UserReference.AccountId,
                        //        StatusId = Default.Active
                        //    };
                        //    _HCoreContext.SCOrderActivity.Add(_SCOrderActivity);
                        //    _HCoreContext.SaveChanges();
                        //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC156, ResourceItems.HCC156M);
                        //}
                        //else
                        //{
                        //    Details.StatusId = StatusId;
                        //    Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        //    Details.ModifyById = _Request.UserReference.AccountId;
                        //    _HCoreContext.SaveChanges();
                        //    #region PostProcessNewOrder
                        //    ManageNotification(_Request.ReferenceId);
                        //    #endregion
                        //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC156, ResourceItems.HCC156M);
                        //}
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Ninjas  reject order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse NinjaRejectOrder(OOrder.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                    }
                    SCOrder Details = _HCoreContext.SCOrder.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        Details.RiderId = null;
                        Details.StatusId = StatusId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC156, ResourceItems.HCC156M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Ninjas  order delivered.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse NinjaOrderDelivered(OOrder.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102);
                }
                else if (string.IsNullOrEmpty(_Request.OrderDeliveryCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCP603, ResourceItems.HCP603M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                    }
                    SCOrder Details = _HCoreContext.SCOrder.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        if (Details.OrderDeliveryCode != _Request.OrderDeliveryCode)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCP604, ResourceItems.HCP604M);
                        }
                        Details.StatusId = StatusId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        if (StatusId == OrderStatus.Delivered)
                        {
                            double RewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("rewardpercentage", (long)Details.DealerId)), _AppConfig.SystemRoundPercentage);
                            double UserRewardPercentage = HCoreHelper.RoundNumber(Convert.ToDouble(HCoreHelper.GetConfiguration("userrewardpercentage", (long)Details.DealerId)), _AppConfig.SystemRoundPercentage);
                            using (_HCoreContext = new HCoreContext())
                            {
                                double InvoiceAmount = (Details.TotalAmount - Details.DeliveryCharge);
                                double RewardAmount = HCoreHelper.GetPercentage(InvoiceAmount, RewardPercentage);
                                double UserRewardAmount = HCoreHelper.GetPercentage(RewardAmount, UserRewardPercentage);
                                double SystemRewardAmount = RewardAmount - UserRewardAmount;
                                if (RewardAmount > 0)
                                {
                                    _CoreTransactionRequest = new OCoreTransaction.Request();
                                    _CoreTransactionRequest.CustomerId = (long)Details.CustomerId;
                                    _CoreTransactionRequest.UserReference = _Request.UserReference;
                                    _CoreTransactionRequest.StatusId = Transaction.Success;
                                    _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                    _CoreTransactionRequest.ParentId = (long)Details.DealerId;
                                    _CoreTransactionRequest.InvoiceAmount = InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceInvoiceAmount = InvoiceAmount;
                                    _CoreTransactionRequest.ReferenceNumber = Details.PaymentReference;
                                    _CoreTransactionRequest.ReferenceAmount = RewardAmount;
                                    _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                    _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = (long)Details.DealerId,
                                        ModeId = TransactionMode.Debit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.Merchant,

                                        Amount = RewardAmount,
                                        Comission = 0,
                                        TotalAmount = RewardAmount,
                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    });
                                    _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                    {
                                        UserAccountId = (long)Details.CustomerId,
                                        ModeId = TransactionMode.Credit,
                                        TypeId = TransactionType.CashReward,
                                        SourceId = TransactionSource.TUC,
                                        Amount = UserRewardAmount,
                                        TotalAmount = UserRewardAmount,
                                        TransactionDate = HCoreHelper.GetGMTDateTime(),
                                    });
                                    if (SystemRewardAmount > 0)
                                    {
                                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                        {
                                            UserAccountId = SystemAccounts.ThankUCashMerchant,
                                            ModeId = TransactionMode.Credit,
                                            TypeId = TransactionType.CashReward,
                                            SourceId = TransactionSource.Settlement,

                                            Amount = SystemRewardAmount,
                                            TotalAmount = SystemRewardAmount,
                                        });
                                    }
                                    _CoreTransactionRequest.Transactions = _TransactionItems;
                                    _ManageCoreTransaction = new ManageCoreTransaction();
                                    OCoreTransaction.Response RTransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                }
                            }
                            if (Details.RiderAmount > 0 && Details.RiderId != null)
                            {
                                #region Credit Ninja Delivery Amount
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = (long)Details.RiderId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = (long)Details.DealerId;
                                _CoreTransactionRequest.InvoiceAmount = (double)Details.RiderAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = (double)Details.TotalAmount;
                                _CoreTransactionRequest.ReferenceNumber = Details.Id.ToString();
                                _CoreTransactionRequest.ReferenceAmount = (double)Details.RiderTotalAmount;
                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = (long)Details.RiderId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.Ninja.DeliveryAmount,
                                    SourceId = TransactionSource.Ninja,
                                    Amount = (double)Details.RiderAmount,
                                    Comission = 0,
                                    TotalAmount = (double)Details.RiderAmount,
                                    TransactionDate = HCoreHelper.GetGMTDateTime(),
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                #endregion
                            }
                            if (Details.RiderCommissionAmount > 0 && Details.RiderId != null)
                            {
                                #region Credit Ninja Delivery Commission Amount
                                _CoreTransactionRequest = new OCoreTransaction.Request();
                                _CoreTransactionRequest.CustomerId = (long)Details.RiderId;
                                _CoreTransactionRequest.UserReference = _Request.UserReference;
                                _CoreTransactionRequest.StatusId = Transaction.Success;
                                _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                                _CoreTransactionRequest.ParentId = (long)Details.DealerId;
                                _CoreTransactionRequest.InvoiceAmount = (double)Details.RiderCommissionAmount;
                                _CoreTransactionRequest.ReferenceAmount = (double)Details.RiderTotalAmount;
                                _CoreTransactionRequest.ReferenceInvoiceAmount = (double)Details.TotalAmount;
                                _CoreTransactionRequest.ReferenceNumber = Details.Id.ToString();
                                _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                                _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                                _TransactionItems.Add(new OCoreTransaction.TransactionItem
                                {
                                    UserAccountId = (long)Details.RiderId,
                                    ModeId = TransactionMode.Credit,
                                    TypeId = TransactionType.Ninja.DeliveryCommission,
                                    SourceId = TransactionSource.Ninja,
                                    Amount = (double)Details.RiderCommissionAmount,
                                    Comission = 0,
                                    TotalAmount = (double)Details.RiderCommissionAmount,
                                    TransactionDate = HCoreHelper.GetGMTDateTime(),
                                });
                                _CoreTransactionRequest.Transactions = _TransactionItems;
                                _ManageCoreTransaction = new ManageCoreTransaction();
                                OCoreTransaction.Response _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                                #endregion
                            }
                        }
                        #region PostProcessNewOrder
                        ManageNotification(_Request.ReferenceId, _Request.UserReference);
                        //var system = ActorSystem.Create("ActorOrderNotification");
                        //var greeter = system.ActorOf<ActorOrderNotification>("ActorOrderNotification");
                        //greeter.Tell(_Request);
                        #endregion
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC156, ResourceItems.HCC156M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("NinjaOrderDelivered", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Ninjas  order delivery failed.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse NinjaOrderDeliveryFailed(OOrder.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                else if (string.IsNullOrEmpty(_Request.StatusCode))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC102, ResourceItems.HC102);
                }
                else if (string.IsNullOrEmpty(_Request.Comment))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCP605, ResourceItems.HCP605M);
                }
                #region Operations
                using (_HCoreContext = new HCoreContext())
                {
                    int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                    if (StatusId == 0)
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                    }
                    SCOrder Details = _HCoreContext.SCOrder.Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey).FirstOrDefault();
                    if (Details != null)
                    {
                        Details.StatusId = StatusId;
                        Details.ModifyDate = HCoreHelper.GetGMTDateTime();
                        Details.ModifyById = _Request.UserReference.AccountId;
                        _HCoreContext.SaveChanges();
                        #region PostProcessNewOrder
                        ManageNotification(_Request.ReferenceId, _Request.UserReference);
                        //var system = ActorSystem.Create("ActorOrderNotification");
                        //var greeter = system.ActorOf<ActorOrderNotification>("ActorOrderNotification");
                        //greeter.Tell(_Request);
                        #endregion
                        #region Send Response
                        object _Response = new
                        {
                            ReferenceId = _Request.ReferenceId,
                            ReferenceKey = _Request.ReferenceKey,
                        };
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Response, ResourceItems.HCC156, ResourceItems.HCC156M);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC404, ResourceItems.HC404M);
                        #endregion
                    }
                }
                #endregion
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                return HCoreHelper.LogException("DeleteCategory", _Exception, _Request.UserReference);
                #endregion
            }
            #endregion
        }
    }
}
