//==================================================================================
// FileName: FrameworkNinja.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to ninja
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object;
using static HCore.Product.Resources.Resources;
using HCore.Operations;
using HCore.Operations.Object;
using static HCore.Helper.HCoreConstant.Helpers;
using static HCore.CoreConstant.CoreHelperStatus;

namespace HCore.Product.Framework
{
    /// <summary>
    /// Class FrameworkNinja.
    /// </summary>
    public class FrameworkNinja
    {
        HCoreContext _HCoreContext;
        TUCNinjaRegistration _TUCNinjaRegistration;
        /// <summary>
        /// Description: Saves the ninja registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse SaveNinjaRegistration(ONinja.Request _Request)
        {
            _Request.CountryId = 1;
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.MobileNumber))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC118", "Mobile number required");
                }
                using (_HCoreContext = new HCoreContext())
                {
                    string MobileNumber = HCoreHelper.FormatMobileNumber("234", _Request.MobileNumber, 10);
                    var MerchantDetails = _HCoreContext.TUCNinjaRegistration
                        .Where(x => x.MobileNumber == MobileNumber)
                        .FirstOrDefault();
                    if (MerchantDetails == null)
                    {
                        _TUCNinjaRegistration = new TUCNinjaRegistration();
                        _TUCNinjaRegistration.Guid = HCoreHelper.GenerateGuid();
                        _TUCNinjaRegistration.FirstName = _Request.FirstName;
                        _TUCNinjaRegistration.LastName = _Request.LastName;
                        _TUCNinjaRegistration.MobileNumber = MobileNumber;
                        _TUCNinjaRegistration.EmailAddress = _Request.EmailAddress;
                        _TUCNinjaRegistration.Address = _Request.Address;
                        _TUCNinjaRegistration.VehicleNumber = _Request.VehicleNumber;
                        _TUCNinjaRegistration.Reference1Name = _Request.Reference1Name;
                        _TUCNinjaRegistration.Reference1ContactNumber = _Request.Reference1ContactNumber;
                        _TUCNinjaRegistration.Reference2Name = _Request.Reference2Name;
                        _TUCNinjaRegistration.Reference2ContactNumber = _Request.Reference2ContactNumber;
                        _TUCNinjaRegistration.FacebookUrl = _Request.FacebookUrl;
                        _TUCNinjaRegistration.TwitterUrl = _Request.TwitterUrl;
                        _TUCNinjaRegistration.InstagramUrl = _Request.InstagramUrl;
                        _TUCNinjaRegistration.LinkedInUrl = _Request.LinkedInUrl;
                        _TUCNinjaRegistration.CreateDate = HCoreHelper.GetGMTDateTime();
                        _TUCNinjaRegistration.CreatedById = 1;
                        _TUCNinjaRegistration.StatusId = 554;
                        if (_Request.CountryId != 0)
                        {
                            _TUCNinjaRegistration.CountryId = _Request.CountryId;
                        }
                        if (_Request.RegionId != 0)
                        {
                            _TUCNinjaRegistration.RegionId = _Request.RegionId;
                        }
                        if (_Request.CityId != 0)
                        {
                            _TUCNinjaRegistration.CityId = _Request.CityId;
                        }
                        if (_Request.CityArea1Id != 0)
                        {
                            _TUCNinjaRegistration.CityArea1Id = _Request.CityArea1Id;
                        }
                        if (_Request.CityArea2Id != 0)
                        {
                            _TUCNinjaRegistration.CityArea2Id = _Request.CityArea2Id;
                        }
                        if (_Request.CityArea3Id != 0)
                        {
                            _TUCNinjaRegistration.CityArea3Id = _Request.CityArea3Id;
                        }
                        if (_Request.CityArea4Id != 0)
                        {
                            _TUCNinjaRegistration.CityArea4Id = _Request.CityArea4Id;
                        }

                        _HCoreContext.TUCNinjaRegistration.Add(_TUCNinjaRegistration);
                        _HCoreContext.SaveChanges();
                        var _Response = new
                        {
                            ReferenceId = _TUCNinjaRegistration.Id,
                            ReferenceKey = _TUCNinjaRegistration.Guid,
                        };
                        long? ImageContentId = null;
                        long? ProofContentId = null;
                        long? UtilityContentId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            ImageContentId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.ProofContent != null && !string.IsNullOrEmpty(_Request.ProofContent.Content))
                        {
                            ProofContentId = HCoreHelper.SaveStorage(_Request.ProofContent.Name, _Request.ProofContent.Extension, _Request.ProofContent.Content, null, _Request.UserReference);
                        }
                        if (_Request.UtilityContent != null && !string.IsNullOrEmpty(_Request.UtilityContent.Content))
                        {
                            UtilityContentId = HCoreHelper.SaveStorage(_Request.UtilityContent.Name, _Request.UtilityContent.Extension, _Request.UtilityContent.Content, null, _Request.UserReference);
                        }
                        if (ImageContentId != null || ProofContentId != null || UtilityContentId != null)
                        {
                            using (_HCoreContext = new HCoreContext())
                            {
                                var PrDetails = _HCoreContext.TUCNinjaRegistration.Where(x => x.Id == _Response.ReferenceId).FirstOrDefault();
                                if (PrDetails != null)
                                {
                                    if (ImageContentId != null)
                                    {
                                        PrDetails.ImageStorageId = ImageContentId;
                                    }
                                    if (ProofContentId != null)
                                    {
                                        PrDetails.ProofStorageId = ProofContentId;
                                    }
                                    if (UtilityContentId != null)
                                    {
                                        PrDetails.UtilityBillStorageId = UtilityContentId;
                                    }
                                    _HCoreContext.SaveChanges();
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC129", "Registration complete.");
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC127", "Registration details already exists");
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveNinjaRegistration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the ninja registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateNinjaRegistration(ONinja.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.TUCNinjaRegistration
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (MerchantDetails != null)
                    {
                        int StatusId = _HCoreContext.HCCore.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        if (StatusId == 0)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                        }
                        MerchantDetails.Comment = _Request.Comment;
                        MerchantDetails.StatusId = StatusId;
                        MerchantDetails.ModifyById = _Request.UserReference.AccountId;
                        MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();
                        if (StatusId == 555)
                        {

                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC129", "Registration status updated");
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC127", "Registration details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveNinjaRegistration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Updates the ninja reg bank details.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateNinjaRegBankDetails(ONinja.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _TUCNinjaRegistration = _HCoreContext.TUCNinjaRegistration
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_TUCNinjaRegistration != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.BankAccountNumber))
                        {
                            _TUCNinjaRegistration.BankAccountNumber = _Request.BankAccountNumber;
                        }

                        if (!string.IsNullOrEmpty(_Request.BankAccountName))
                        {
                            _TUCNinjaRegistration.BankAccountName = _Request.BankAccountName;
                        }

                        if (!string.IsNullOrEmpty(_Request.BankName))
                        {
                            _TUCNinjaRegistration.BankName = _Request.BankName;
                        }

                        if (!string.IsNullOrEmpty(_Request.BankCode))
                        {
                            _TUCNinjaRegistration.BankCode = _Request.BankCode;
                        }

                        if (_Request.UserReference != null)
                        {
                            _TUCNinjaRegistration.ModifyById = _Request.UserReference.AccountId;
                        }
                        _TUCNinjaRegistration.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC129", "Ninja Bank details updated");
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC127", "Ninja details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("UpdateNinjaRegistrationBankDetails", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Gets the ninja registration.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNinjaRegistration(ONinja.Request _Request)
        {
            #region Manage Exception
            try
            {
                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }

                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Get Data
                    ONinja.Details _Details = (from x in _HCoreContext.TUCNinjaRegistration
                                               where x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey
                                               select new ONinja.Details
                                               {
                                                   ReferenceId = x.Id,
                                                   ReferenceKey = x.Guid,

                                                   FirstName = x.FirstName,
                                                   LastName = x.LastName,
                                                   MobileNumber = x.MobileNumber,
                                                   EmailAddress = x.EmailAddress,
                                                   Address = x.Address,
                                                   VehicleNumber = x.VehicleNumber,
                                                   Reference1Name = x.Reference1Name,
                                                   Reference1ContactNumber = x.Reference1ContactNumber,
                                                   Reference2Name = x.Reference2Name,
                                                   Reference2ContactNumber = x.Reference2ContactNumber,
                                                   PhotoUrl = x.ImageStorage.Path,
                                                   IdProofUrl = x.ProofStorage.Path,
                                                   AddressProofUrl = x.UtilityBillStorage.Path,
                                                   CreateDate = x.CreateDate,
                                                   ModifyDate = x.ModifyDate,
                                                   ModifyById = x.ModifyById,
                                                   ModifyByKey = x.ModifyBy.Guid,
                                                   ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                   StatusId = x.StatusId,
                                                   StatusCode = x.Status.SystemName,
                                                   StatusName = x.Status.Name,

                                                   CountryId = x.CountryId,
                                                   CountryName = x.Country.Name,

                                                   RegionId = x.RegionId,
                                                   RegionName = x.Region.Name,

                                                   CityId = x.CityId,
                                                   CityName = x.City.Name,

                                                   CityArea1Id = x.CityArea1Id,
                                                   CityArea1Name = x.CityArea1.Name,

                                                   CityArea2Id = x.CityArea2Id,
                                                   CityArea2Name = x.CityArea2.Name,

                                                   CityArea3Id = x.CityArea3Id,
                                                   CityArea3Name = x.CityArea3.Name,

                                                   CityArea4Id = x.CityArea4Id,
                                                   CityArea4Name = x.CityArea4.Name,

                                                   BankAccountName = x.BankAccountName,
                                                   BankAccountNumber = x.BankAccountNumber,
                                                   BankName = x.BankName,
                                                   BankCode = x.BankCode,

                                                   FacebookUrl = x.FacebookUrl,
                                                   InstagramUrl = x.InstagramUrl,
                                                   LinkedInUrl = x.LinkedInUrl,
                                                   TwitterUrl = x.TwitterUrl,

                                               }).FirstOrDefault();

                    #endregion
                    if (_Details != null)
                    {

                        //if (!string.IsNullOrEmpty(_Details.DealerIconUrl))
                        //{
                        //    _Details.DealerIconUrl = _AppConfig.StorageUrl + _Details.DealerIconUrl;
                        //}
                        //else
                        //{
                        //    _Details.DealerIconUrl = _AppConfig.Default_Icon;
                        //}

                        //_Details.BillingAddress = _HCoreContext.SCOrderAddress.Where(x => x.OrderId == _Details.ReferenceId && x.TypeId == 505)
                        //    .Select(x => new OOrder.OrderAddress
                        //    {
                        //        ReferenceId = x.Id,
                        //        ReferenceKey = x.Guid,

                        //        Name = x.Name,

                        //        StatusId = x.StatusId,
                        //        StatusCode = x.Status.SystemName,
                        //        StatusName = x.Status.Name

                        //    }).FirstOrDefault();


                        _HCoreContext.Dispose();
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _Details, ResourceItems.HC101, ResourceItems.HC101);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101);
                    }
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetOrders", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, "HC0000");
                #endregion
            }
            #endregion
        }


        /// <summary>
        /// Description: Updates the ninja registration profile.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse UpdateNinjaRegistrationProfile(ONinja.Request _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.ReferenceId == 0)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC100, ResourceItems.HC100M);
                }
                if (string.IsNullOrEmpty(_Request.ReferenceKey))
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC101, ResourceItems.HC101M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var _TUCNinjaRegistration = _HCoreContext.TUCNinjaRegistration
                        .Where(x => x.Id == _Request.ReferenceId && x.Guid == _Request.ReferenceKey)
                        .FirstOrDefault();
                    if (_TUCNinjaRegistration != null)
                    {
                        if (!string.IsNullOrEmpty(_Request.FirstName))
                        {
                            _TUCNinjaRegistration.FirstName = _Request.FirstName;
                        }

                        if (!string.IsNullOrEmpty(_Request.LastName))
                        {
                            _TUCNinjaRegistration.LastName = _Request.LastName;
                        }

                        if (!string.IsNullOrEmpty(_Request.MobileNumber))
                        {
                            _TUCNinjaRegistration.MobileNumber = _Request.MobileNumber;
                        }

                        if (!string.IsNullOrEmpty(_Request.EmailAddress))
                        {
                            _TUCNinjaRegistration.EmailAddress = _Request.EmailAddress;
                        }

                        if (!string.IsNullOrEmpty(_Request.FacebookUrl))
                        {
                            _TUCNinjaRegistration.FacebookUrl = _Request.FacebookUrl;
                        }

                        if (!string.IsNullOrEmpty(_Request.TwitterUrl))
                        {
                            _TUCNinjaRegistration.TwitterUrl = _Request.TwitterUrl;
                        }

                        if (!string.IsNullOrEmpty(_Request.InstagramUrl))
                        {
                            _TUCNinjaRegistration.InstagramUrl = _Request.InstagramUrl;
                        }

                        if (!string.IsNullOrEmpty(_Request.Reference1Name))
                        {
                            _TUCNinjaRegistration.Reference1Name = _Request.Reference1Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.Reference1ContactNumber))
                        {
                            _TUCNinjaRegistration.Reference1ContactNumber = _Request.Reference1ContactNumber;
                        }
                        if (!string.IsNullOrEmpty(_Request.Reference2Name))
                        {
                            _TUCNinjaRegistration.Reference2Name = _Request.Reference2Name;
                        }
                        if (!string.IsNullOrEmpty(_Request.Reference2ContactNumber))
                        {
                            _TUCNinjaRegistration.Reference2ContactNumber = _Request.Reference2ContactNumber;
                        }

                        if (!string.IsNullOrEmpty(_Request.VehicleNumber))
                        {
                            _TUCNinjaRegistration.VehicleNumber = _Request.VehicleNumber;
                        }

                        if (_Request.CountryId != 0)
                        {
                            _TUCNinjaRegistration.CountryId = _Request.CountryId;
                        }
                        if (_Request.RegionId != 0)
                        {
                            _TUCNinjaRegistration.RegionId = _Request.RegionId;
                        }
                        if (_Request.CityId != 0)
                        {
                            _TUCNinjaRegistration.CityId = _Request.CityId;
                        }
                        if (_Request.CityArea1Id != 0)
                        {
                            _TUCNinjaRegistration.CityArea1Id = _Request.CityArea1Id;
                        }

                        if (!string.IsNullOrEmpty(_Request.Address))
                        {
                            _TUCNinjaRegistration.Address = _Request.Address;
                        }

                        if (!string.IsNullOrEmpty(_Request.Comment))
                        {
                            _TUCNinjaRegistration.Comment = _Request.Comment;
                        }

                        var PrDetails = _HCoreContext.TUCNinjaRegistration.Where(x => x.Id == _Request.ReferenceId).FirstOrDefault();

                        long? ImageContentId = null;
                        if (_Request.ImageContent != null && !string.IsNullOrEmpty(_Request.ImageContent.Content))
                        {
                            long? DeleteImageContentId = null;
                            if (PrDetails != null && PrDetails.ImageStorageId != null)
                            {
                                DeleteImageContentId = PrDetails.ImageStorageId;
                            }
                            ImageContentId = HCoreHelper.SaveStorage(_Request.ImageContent.Name, _Request.ImageContent.Extension, _Request.ImageContent.Content, DeleteImageContentId, _Request.UserReference);
                            PrDetails.ImageStorageId = ImageContentId;
                        }

                        long? ProofContentId = null;
                        if (_Request.ProofContent != null && !string.IsNullOrEmpty(_Request.ProofContent.Content))
                        {
                            long? DeleteProofContentId = null;
                            if (PrDetails != null && PrDetails.ProofStorageId != null)
                            {
                                DeleteProofContentId = PrDetails.ProofStorageId;
                            }
                            ProofContentId = HCoreHelper.SaveStorage(_Request.ProofContent.Name, _Request.ProofContent.Extension, _Request.ProofContent.Content, DeleteProofContentId, _Request.UserReference);
                            PrDetails.ProofStorageId = ProofContentId;
                        }

                        long? UtilityContentId = null;
                        if (_Request.UtilityContent != null && !string.IsNullOrEmpty(_Request.UtilityContent.Content))
                        {
                            long? DeleteUtilityContentId = null;
                            if (PrDetails != null && PrDetails.UtilityBillStorageId != null)
                            {
                                DeleteUtilityContentId = PrDetails.ImageStorageId;
                            }
                            UtilityContentId = HCoreHelper.SaveStorage(_Request.UtilityContent.Name, _Request.UtilityContent.Extension, _Request.UtilityContent.Content, DeleteUtilityContentId, _Request.UserReference);
                            PrDetails.UtilityBillStorageId = UtilityContentId;
                        }

                        if (_Request.UserReference != null)
                        {
                            _TUCNinjaRegistration.ModifyById = _Request.UserReference.AccountId;
                        }
                        _TUCNinjaRegistration.ModifyDate = HCoreHelper.GetGMTDateTime();
                        _HCoreContext.SaveChanges();

                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCC129", "Ninja details updated");
                        #endregion
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC127", "Ninja details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveNinjaRegistration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the registrations.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetRegistrations(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.TUCNinjaRegistration
                                             select new ONinja.List
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,

                                                 FirstName = x.FirstName,
                                                 LastName = x.LastName,
                                                 MobileNumber = x.MobileNumber,
                                                 EmailAddress = x.EmailAddress,
                                                 Address = x.Address,
                                                 VehicleNumber = x.VehicleNumber,
                                                 Reference1Name = x.Reference1Name,
                                                 Reference1ContactNumber = x.Reference1ContactNumber,
                                                 Reference2Name = x.Reference2Name,
                                                 Reference2ContactNumber = x.Reference2ContactNumber,
                                                 PhotoUrl = x.ImageStorage.Path,
                                                 IdProofUrl = x.ProofStorage.Path,
                                                 AddressProofUrl = x.UtilityBillStorage.Path,
                                                 CreateDate = x.CreateDate,
                                                 ModifyDate = x.ModifyDate,
                                                 ModifyById = x.ModifyById,
                                                 ModifyByKey = x.ModifyBy.Guid,
                                                 ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,

                                                 CountryId = x.CountryId,
                                                 CountryName = x.Country.Name,

                                                 RegionId = x.RegionId,
                                                 RegionName = x.Region.Name,

                                                 CityId = x.CityId,
                                                 CityName = x.City.Name,

                                                 CityArea1Id = x.CityArea1Id,
                                                 CityArea1Name = x.CityArea1.Name,

                                                 CityArea2Id = x.CityArea2Id,
                                                 CityArea2Name = x.CityArea2.Name,

                                                 CityArea3Id = x.CityArea3Id,
                                                 CityArea3Name = x.CityArea3.Name,

                                                 CityArea4Id = x.CityArea4Id,
                                                 CityArea4Name = x.CityArea4.Name,
                                             })
                                                    .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion

                    #region Get Data
                    List<ONinja.List> Data = (from x in _HCoreContext.TUCNinjaRegistration
                                              select new ONinja.List
                                              {
                                                  ReferenceId = x.Id,
                                                  ReferenceKey = x.Guid,

                                                  FirstName = x.FirstName,
                                                  LastName = x.LastName,
                                                  MobileNumber = x.MobileNumber,
                                                  EmailAddress = x.EmailAddress,
                                                  Address = x.Address,
                                                  VehicleNumber = x.VehicleNumber,
                                                  Reference1Name = x.Reference1Name,
                                                  Reference1ContactNumber = x.Reference1ContactNumber,
                                                  Reference2Name = x.Reference2Name,
                                                  Reference2ContactNumber = x.Reference2ContactNumber,
                                                  PhotoUrl = x.ImageStorage.Path,
                                                  IdProofUrl = x.ProofStorage.Path,
                                                  AddressProofUrl = x.UtilityBillStorage.Path,
                                                  CreateDate = x.CreateDate,
                                                  ModifyDate = x.ModifyDate,
                                                  ModifyById = x.ModifyById,
                                                  ModifyByKey = x.ModifyBy.Guid,
                                                  ModifyByDisplayName = x.ModifyBy.DisplayName,
                                                  StatusId = x.StatusId,
                                                  StatusCode = x.Status.SystemName,
                                                  StatusName = x.Status.Name,

                                                  CountryId = x.CountryId,
                                                  CountryName = x.Country.Name,

                                                  RegionId = x.RegionId,
                                                  RegionName = x.Region.Name,

                                                  CityId = x.CityId,
                                                  CityName = x.City.Name,


                                                  CityArea1Id = x.CityArea1Id,
                                                  CityArea1Name = x.CityArea1.Name,

                                                  CityArea2Id = x.CityArea2Id,
                                                  CityArea2Name = x.CityArea2.Name,

                                                  CityArea3Id = x.CityArea3Id,
                                                  CityArea3Name = x.CityArea3.Name,

                                                  CityArea4Id = x.CityArea4Id,
                                                  CityArea4Name = x.CityArea4.Name,
                                              })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.PhotoUrl))
                        {
                            DataItem.PhotoUrl = _AppConfig.StorageUrl + DataItem.PhotoUrl;
                        }
                        else
                        {
                            DataItem.PhotoUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.IdProofUrl))
                        {
                            DataItem.IdProofUrl = _AppConfig.StorageUrl + DataItem.IdProofUrl;
                        }
                        else
                        {
                            DataItem.IdProofUrl = _AppConfig.Default_Icon;
                        }
                        if (!string.IsNullOrEmpty(DataItem.AddressProofUrl))
                        {
                            DataItem.AddressProofUrl = _AppConfig.StorageUrl + DataItem.AddressProofUrl;
                        }
                        else
                        {
                            DataItem.AddressProofUrl = _AppConfig.Default_Icon;
                        }
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetRegistrations", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the ninja list.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNinjaList(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "DisplayName", "asc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = (from ninjareg in _HCoreContext.TUCNinjaRegistration
                                             join ninjaaccount in _HCoreContext.HCUAccount on ninjareg.MobileNumber equals ninjaaccount.MobileNumber
                                             where ninjareg.StatusId == 555 && ninjaaccount.StatusId == 2
                                             select new
                                             {
                                                 ReferenceId = ninjaaccount.Id,
                                                 ReferenceKey = ninjaaccount.Guid,
                                                 ninjaaccount.DisplayName,
                                                 ninjaaccount.MobileNumber,
                                                 CityName = ninjareg.City.Name,
                                                 AreaName1 = ninjareg.CityArea1.Name,

                                             })
                                                   .Where(_Request.SearchCondition)
                                           .Count();
                    #endregion

                    #region Get Data
                    var Data = (from ninjareg in _HCoreContext.TUCNinjaRegistration
                                join ninjaaccount in _HCoreContext.HCUAccount on ninjareg.MobileNumber equals ninjaaccount.MobileNumber
                                where ninjareg.StatusId == 555 && ninjaaccount.StatusId == 2
                                select new
                                {
                                    ReferenceId = ninjaaccount.Id,
                                    ReferenceKey = ninjaaccount.Guid,
                                    DisplayName = ninjaaccount.DisplayName,
                                    MobileNumber = ninjaaccount.MobileNumber,
                                    CityName = ninjareg.City.Name,
                                    AreaName1 = ninjareg.CityArea1.Name,

                                })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetRegistrations", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        /// <summary>
        /// Description: Gets the ninja transaction.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNinjaTransaction(OList.Request _Request)
        {
            #region Manage Exception
            try
            {
                #region Add Default Request
                if (string.IsNullOrEmpty(_Request.SearchCondition))
                {
                    #region Default Conditions
                    HCoreHelper.GetSearchCondition(_Request, "ReferenceId", "0", "<>");
                    #endregion
                }
                if (string.IsNullOrEmpty(_Request.SortExpression))
                {
                    #region Default Conditions
                    HCoreHelper.GetSortCondition(_Request, "CreateDate", "desc");
                    #endregion
                }
                #endregion
                #region Operation
                using (_HCoreContext = new HCoreContext())
                {

                    #region Total Records
                    _Request.TotalRecords = (from x in _HCoreContext.HCUAccountTransaction
                                             where x.SourceId == Helpers.TransactionSource.Ninja && x.AccountId == _Request.UserReference.AccountId
                                             select new ONinja.Transactions
                                             {
                                                 ReferenceId = x.Id,
                                                 ReferenceKey = x.Guid,
                                                 DealerDisplayName = x.Parent.DisplayName,
                                                 DealerIconUrl = x.Parent.IconStorage.Path,
                                                 ModeCode = x.Mode.SystemName,
                                                 ModeName = x.Mode.Name,
                                                 TypeCode = x.Type.SystemName,
                                                 TypeName = x.Type.Name,
                                                 TotalAmount = x.TotalAmount,
                                                 CreateDate = x.CreateDate,
                                                 StatusId = x.StatusId,
                                                 StatusCode = x.Status.SystemName,
                                                 StatusName = x.Status.Name,
                                             })
                                                    .Where(_Request.SearchCondition)
                                            .Count();
                    #endregion

                    #region Get Data
                    List<ONinja.Transactions> Data = (from x in _HCoreContext.HCUAccountTransaction
                                                      where x.SourceId == Helpers.TransactionSource.Ninja && x.AccountId == _Request.UserReference.AccountId
                                                      select new ONinja.Transactions
                                                      {
                                                          ReferenceId = x.Id,
                                                          ReferenceKey = x.Guid,
                                                          DealerDisplayName = x.Parent.DisplayName,
                                                          DealerIconUrl = x.Parent.IconStorage.Path,
                                                          ModeCode = x.Mode.SystemName,
                                                          ModeName = x.Mode.Name,
                                                          TypeCode = x.Type.SystemName,
                                                          TypeName = x.Type.Name,
                                                          TotalAmount = x.TotalAmount,
                                                          CreateDate = x.CreateDate,
                                                          StatusId = x.StatusId,
                                                          StatusCode = x.Status.SystemName,
                                                          StatusName = x.Status.Name,
                                                      })
                                              .Where(_Request.SearchCondition)
                                              .OrderBy(_Request.SortExpression)
                                              .Skip(_Request.Offset)
                                              .Take(_Request.Limit)
                                              .ToList();
                    #endregion
                    foreach (var DataItem in Data)
                    {
                        if (!string.IsNullOrEmpty(DataItem.DealerIconUrl))
                        {
                            DataItem.DealerIconUrl = _AppConfig.StorageUrl + DataItem.DealerIconUrl;
                        }
                        else
                        {
                            DataItem.DealerIconUrl = _AppConfig.Default_Icon;
                        }
                        //if (!string.IsNullOrEmpty(DataItem.IdProofUrl))
                        //{
                        //    DataItem.IdProofUrl = _AppConfig.StorageUrl + DataItem.IdProofUrl;
                        //}
                        //else
                        //{
                        //    DataItem.IdProofUrl = _AppConfig.Default_Icon;
                        //}
                        //if (!string.IsNullOrEmpty(DataItem.AddressProofUrl))
                        //{
                        //    DataItem.AddressProofUrl = _AppConfig.StorageUrl + DataItem.AddressProofUrl;
                        //}
                        //else
                        //{
                        //    DataItem.AddressProofUrl = _AppConfig.Default_Icon;
                        //}
                    }
                    #region Create  Response Object
                    _HCoreContext.Dispose();
                    OList.Response _OResponse = HCoreHelper.GetListResponse(_Request.TotalRecords, Data, _Request.Offset, _Request.Limit);
                    #endregion
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, _OResponse, "HC0001");
                    #endregion
                }
                #endregion

            }
            catch (Exception _Exception)
            {
                #region Log Bug

                HCoreHelper.LogException("GetRegistrations", _Exception, _Request.UserReference);
                #endregion
                #region Create DataTable Response Object
                OList.Response _OResponse = HCoreHelper.GetListResponse(0, null, _Request.Offset, _Request.Limit);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, _OResponse, "HC0000");
                #endregion
            }
            #endregion
        }
        ManageCoreTransaction _ManageCoreTransaction;
        /// <summary>
        /// Description: Gets the ninja balance.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse GetNinjaBalance(ONinja.ONinjaTransferRequest _Request)
        {
            #region Manage Exception
            try
            {

                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.UserReference.AccountId)
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId != HelperStatus.Default.Active)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC209, ResourceItems.HCC209M);
                        }
                        var NinjaDetails = _HCoreContext.TUCNinjaRegistration.Where(x => x.MobileNumber == AccountDetails.MobileNumber).FirstOrDefault();

                        _ManageCoreTransaction = new ManageCoreTransaction();
                        var Balance = _ManageCoreTransaction.GetAccountBalanceOverview(_Request.UserReference.AccountId, Helpers.TransactionSource.Ninja);
                        if (NinjaDetails != null)
                        {
                            Balance.BankName = NinjaDetails.BankName;
                            Balance.AccountNumber = NinjaDetails.BankAccountNumber;
                            Balance.AccountHolderName = NinjaDetails.BankAccountName;
                            Balance.BankCode = NinjaDetails.BankCode;
                        }
                        return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Success, Balance, ResourceItems.HCC210, ResourceItems.HCC210M);
                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC127", "Registration details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveNinjaRegistration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }

        /// <summary>
        /// Description: Transfers the ninja amount.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        internal OResponse TransferNinjaAmount(ONinja.ONinjaTransferRequest _Request)
        {
            #region Manage Exception
            try
            {

                if (_Request.Amount < 1)
                {
                    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC208, ResourceItems.HCC208M);
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var AccountDetails = _HCoreContext.HCUAccount
                        .Where(x => x.Id == _Request.UserReference.AccountId)
                        .FirstOrDefault();
                    if (AccountDetails != null)
                    {
                        if (AccountDetails.StatusId != HelperStatus.Default.Active)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC209, ResourceItems.HCC209M);
                        }
                        ManageCoreTransaction _ManageCoreTransaction;
                        OCoreTransaction.Request _CoreTransactionRequest;
                        _ManageCoreTransaction = new ManageCoreTransaction();
                        double Balance = _ManageCoreTransaction.GetAccountBalance(_Request.UserReference.AccountId, Helpers.TransactionSource.Ninja);
                        if (_Request.Amount > Balance)
                        {
                            return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HCC210, ResourceItems.HCC210M);
                        }

                        #region Credit Ninja Delivery Amount
                        _CoreTransactionRequest = new OCoreTransaction.Request();
                        _CoreTransactionRequest.CustomerId = AccountDetails.Id;
                        _CoreTransactionRequest.UserReference = _Request.UserReference;
                        _CoreTransactionRequest.StatusId = HelperStatus.Transaction.Success;
                        _CoreTransactionRequest.GroupKey = HCoreHelper.GenerateGuid();
                        _CoreTransactionRequest.ParentId = 2;
                        _CoreTransactionRequest.InvoiceAmount = _Request.Amount;
                        _CoreTransactionRequest.ReferenceInvoiceAmount = _Request.Amount;
                        //_CoreTransactionRequest.ReferenceNumber = Details.Id.ToString();
                        _CoreTransactionRequest.ReferenceAmount = _Request.Amount;
                        _CoreTransactionRequest.CreatedById = _Request.UserReference.AccountId;
                        List<OCoreTransaction.TransactionItem> _TransactionItems = new List<OCoreTransaction.TransactionItem>();
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = AccountDetails.Id,
                            ModeId = TransactionMode.Debit,
                            TypeId = TransactionType.Ninja.BankTransfer,
                            SourceId = TransactionSource.Ninja,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                            TransactionDate = HCoreHelper.GetGMTDateTime(),
                        });
                        _TransactionItems.Add(new OCoreTransaction.TransactionItem
                        {
                            UserAccountId = 3,
                            ModeId = TransactionMode.Credit,
                            TypeId = TransactionType.Ninja.BankTransfer,
                            SourceId = TransactionSource.Ninja,
                            Amount = _Request.Amount,
                            Comission = 0,
                            TotalAmount = _Request.Amount,
                            TransactionDate = HCoreHelper.GetGMTDateTime(),
                        });
                        _CoreTransactionRequest.Transactions = _TransactionItems;
                        OCoreTransaction.Response _TransactionResponse = _ManageCoreTransaction.ProcessTransaction(_CoreTransactionRequest);
                        if (_TransactionResponse.Status == Transaction.Success)
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCN200", "Transaction successful. Amount will be transfered to your bank account within 24 hours.");
                            #endregion
                        }
                        else
                        {
                            #region Send Response
                            return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCERR", "Unable to process your transaction. Please try after some time");
                            #endregion
                        }
                        #endregion

                        //long StatusId = _HCoreContext.HCCoreHelper.Where(x => x.SystemName == _Request.StatusCode).Select(x => x.Id).FirstOrDefault();
                        //if (StatusId == 0)
                        //{
                        //    return HCoreHelper.SendResponse(_Request.UserReference, HCoreConstant.ResponseStatus.Error, null, ResourceItems.HC103, ResourceItems.HC103M);
                        //}
                        //MerchantDetails.Comment = _Request.Comment;
                        //MerchantDetails.StatusId = StatusId;
                        //MerchantDetails.ModifyById = _Request.UserReference.AccountId;
                        //MerchantDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                        //_HCoreContext.SaveChanges();
                        //if (StatusId == 555)
                        //{

                        //}

                    }
                    else
                    {
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC127", "Registration details not found");
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveNinjaRegistration", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCC128", ResourceItems.HCC128);
                #endregion
            }
            #endregion
        }

    }
}
