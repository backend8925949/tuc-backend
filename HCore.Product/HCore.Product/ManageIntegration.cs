//==================================================================================
// FileName: ManageIntegration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Product.Integration;
using HCore.Product.Object.Integration;

namespace HCore.Product
{
    public class ManageIntegration
    {
        FrameworkIntegration _FrameworkIntegration;
        /// <summary>
        /// Save Product
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        public OResponse SoftCon_SaveProduct(OSoftCom.ProductUpdate _Request)
        {
            _FrameworkIntegration = new FrameworkIntegration();
            return _FrameworkIntegration.SoftCon_SaveProduct(_Request);
        }
    }
}
