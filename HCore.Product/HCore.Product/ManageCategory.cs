//==================================================================================
// FileName: ManageCategory.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Helper;
using HCore.Product.Framework;
using HCore.Product.Object;

namespace HCore.Product
{
    public class ManageCategory
    {
        FrameworkCategory _FrameworkCategory;
        /// <summary>
        /// Description: Saves the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.SaveCategory(_Request);
        }

        /// <summary>
        /// Description: Updates the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.UpdateCategory(_Request);
        }

        /// <summary>
        /// Description: Deletes the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.DeleteCategory(_Request);
        }

        /// <summary>
        /// Description: Gets the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCategory(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetCategory(_Request);
        }


        /// <summary>
        /// Description: Saves the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.SaveSubCategory(_Request);
        }

        /// <summary>
        /// Description: Updates the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.UpdateSubCategory(_Request);
        }

        /// <summary>
        /// Description: Deletes the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.DeleteSubCategory(_Request);
        }

        /// <summary>
        /// Description: Gets the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSubCategory(OCategory.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetSubCategory(_Request);
        }
        /// <summary>
        /// Description: Gets the sub category.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetSubCategory(OList.Request _Request)
        {
            _FrameworkCategory = new FrameworkCategory();
            return _FrameworkCategory.GetSubCategory(_Request);
        }

    }
}
