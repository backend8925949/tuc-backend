//==================================================================================
// FileName: ManageOrder.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Product.Framework;
using HCore.Product.Object;

namespace HCore.Product
{
    public class ManageOrder
    {
        FrameworkOrder _FrameworkOrder;
        FrameworkOrderOperation _FrameworkOrderOperation;
        /// <summary>
        /// Description: Initializes the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse InitializeOrder(OOrderManager.NewOrder _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.InitializeOrder(_Request);
        }
        /// <summary>
        /// Description: Confirms the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse ConfirmOrder(OOrderManager.Request _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.ConfirmOrder(_Request);
        }
        /// <summary>
        /// Description: Upates the order status.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpateOrderStatus(OOrder.Request _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.UpateOrderStatus(_Request);
        }
        /// <summary>
        /// Description: Updates the order rating.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateOrderRating(OOrder.Rating _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.UpdateOrderRating(_Request);
        }
        /// <summary>
        /// Description: Cancels the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse CancelOrder(OOrderManager.Request _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.CancelOrder(_Request);
        }
        /// <summary>
        /// Description: Ninjas  reject order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse NinjaRejectOrder(OOrder.Request _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.NinjaRejectOrder(_Request);
        }
        /// <summary>
        /// Description: Ninjas  order delivered.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse NinjaOrderDelivered(OOrder.Request _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.NinjaOrderDelivered(_Request);
        }
        /// <summary>
        /// Description: Ninjas  order delivery failed.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse NinjaOrderDeliveryFailed(OOrder.Request _Request)
        {
            _FrameworkOrderOperation = new FrameworkOrderOperation();
            return _FrameworkOrderOperation.NinjaOrderDeliveryFailed(_Request);
        }



        /// <summary>
        /// Description: Gets the customer order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerOrder(OOrderManager.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetCustomerOrder(_Request);
        }

        /// <summary>
        /// Description: Gets the customer order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetCustomerOrder(OList.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetCustomerOrder(_Request);
        }


        /// <summary>
        /// Description: Gets the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOrder(OOrder.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetOrder(_Request);
        }
        /// <summary>
        /// Description: Gets the order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOrder(OList.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetOrder(_Request);
        }
        /// <summary>
        /// Description: Gets the order overview.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOrderOverview(OList.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetOrderOverview(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant orders.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantOrders(OList.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetMerchantOrders(_Request);
        }
        /// <summary>
        /// Description: Gets the merchant order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetMerchantOrder(OOrder.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetMerchantOrder(_Request);
        }
        /// <summary>
        /// Description: Gets the order activity.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetOrderActivity(OList.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetOrderActivity(_Request);
        }


        /// <summary>
        /// Description: Gets the ninja order summary.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNinjaOrderSummary(OList.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetNinjaOrderSummary(_Request);
        }

        /// <summary>
        /// Description: Gets the ninja order.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNinjaOrder(OOrderManager.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetNinjaOrder(_Request);
        }
        /// <summary>
        /// Description: Gets the ninja orders.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetNinjaOrders(OList.Request _Request)
        {
            _FrameworkOrder = new FrameworkOrder();
            return _FrameworkOrder.GetNinjaOrders(_Request);
        }
       
    }
}
