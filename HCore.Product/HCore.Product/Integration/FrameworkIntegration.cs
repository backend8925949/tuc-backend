//==================================================================================
// FileName: FrameworkIntegration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining logics related to product
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 27-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using HCore.Data;
using HCore.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using HCore.Helper;
using static HCore.Helper.HCoreConstant;
using HCore.Product.Object.Integration;
using static HCore.Product.Resources.Resources;

namespace HCore.Product.Integration
{
    internal class FrameworkIntegration
    {
        HCoreContext _HCoreContext;
        SCProduct _SCProduct;
        /// <summary>
        /// Saves the product
        /// </summary>
        /// <param name="_Request"></param>
        /// <returns></returns>
        internal OResponse SoftCon_SaveProduct(OSoftCom.ProductUpdate _Request)
        {
            #region Manage Exception
            try
            {
                if (string.IsNullOrEmpty(_Request.merchantCode))
                {
                    #region Send Response
                    return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP100", ResourceItems.HCP100);
                    #endregion
                }
                using (_HCoreContext = new HCoreContext())
                {
                    var MerchantDetails = _HCoreContext.HCUAccount
                        .Where(x => x.AccountTypeId == HCoreConstant.Helpers.UserAccountType.Merchant
                        && x.StatusId == HelperStatus.Default.Active
                        && x.AccountCode == _Request.merchantCode)
                        .FirstOrDefault();
                    if (MerchantDetails != null)
                    {
                        if (_Request.Product != null)
                        {
                            var ProductDetails = _HCoreContext.SCProduct
                                                              .Where(x => x.Guid == _Request.Product._id)
                                                              .FirstOrDefault();
                            if (ProductDetails != null)
                            {
                                ProductDetails.Name = _Request.Product.name;
                                ProductDetails.SystemName = HCoreHelper.GenerateSystemName(_Request.Product.name);
                                //ProductDetails.MinimumAmount = _Request.Product.price;
                                //ProductDetails.MaximumAmount = _Request.Product.price;
                                //ProductDetails.MinimumQuantity = 0;
                                //ProductDetails.MaximumQuantity = 0;
                                //ProductDetails.ActualPrice = _Request.Product.price;
                                //ProductDetails.SellingPrice = _Request.Product.price;
                                //ProductDetails.AllowMultiple = 1;
                                //ProductDetails.ReferenceNumber = _Request.Product._id;
                                //ProductDetails.TotalStock = _Request.Product.totalStock;
                                ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                if (_Request.Product.visibleOnNearby)
                                {
                                    ProductDetails.StatusId = HelperStatus.Default.Active;
                                }
                                else
                                {
                                    ProductDetails.StatusId = HelperStatus.Default.Inactive;
                                }
                                if (_Request.Product.deleted)
                                {
                                    ProductDetails.StatusId = HelperStatus.Default.Blocked;
                                }
                                _HCoreContext.SaveChanges();
                            }
                            else
                            {
                                using (_HCoreContext = new HCoreContext())
                                {
                                    _SCProduct = new SCProduct();
                                    _SCProduct.Guid = HCoreHelper.GenerateGuid();
                                    //_SCProduct.Sku = HCoreHelper.GenerateRandomNumber(10);
                                    _SCProduct.AccountId = MerchantDetails.Id;
                                    _SCProduct.Name = _Request.Product.name;
                                    _SCProduct.SystemName = HCoreHelper.GenerateSystemName(_Request.Product.name);
                                    //_SCProduct.MinimumAmount = _Request.Product.price;
                                    //_SCProduct.MaximumAmount = _Request.Product.price;
                                    //_SCProduct.MinimumQuantity = 0;
                                    //_SCProduct.MaximumQuantity = 0;
                                    //_SCProduct.ActualPrice = _Request.Product.price;
                                    //_SCProduct.SellingPrice = _Request.Product.price;
                                    //_SCProduct.AllowMultiple = 1;
                                    //_SCProduct.ReferenceNumber = _Request.Product._id;
                                    //_SCProduct.TotalStock = _Request.Product.totalStock;
                                    _SCProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                                    _SCProduct.CreatedById = _Request.UserReference.AccountId;
                                    _SCProduct.StatusId = HelperStatus.Default.Active;
                                    if (_Request.Product.visibleOnNearby)
                                    {
                                        _SCProduct.StatusId = HelperStatus.Default.Active;
                                    }
                                    else
                                    {
                                        _SCProduct.StatusId = HelperStatus.Default.Inactive;
                                    }
                                    if (_Request.Product.deleted)
                                    {
                                        _SCProduct.StatusId = HelperStatus.Default.Blocked;
                                    }
                                    _HCoreContext.SCProduct.Add(_SCProduct);
                                    _HCoreContext.SaveChanges();
                                }
                            }

                        }
                        if (_Request.Products != null && _Request.Products.Count() > 0)
                        {
                            foreach (var Product in _Request.Products)
                            {
                                using(_HCoreContext =new HCoreContext())
                                {
                                    var ProductDetails = _HCoreContext.SCProduct
                                                             .Where(x => x.Guid == Product._id)
                                                             .FirstOrDefault();
                                    if (ProductDetails != null)
                                    {
                                        ProductDetails.Name = Product.name;
                                        ProductDetails.SystemName = HCoreHelper.GenerateSystemName(Product.name);
                                        //ProductDetails.MinimumAmount = Product.price;
                                        //ProductDetails.MaximumAmount = Product.price;
                                        //ProductDetails.MinimumQuantity = 0;
                                        //ProductDetails.MaximumQuantity = 0;
                                        //ProductDetails.ActualPrice = Product.price;
                                        //ProductDetails.SellingPrice = Product.price;
                                        //ProductDetails.AllowMultiple = 1;
                                        //ProductDetails.ReferenceNumber = Product._id;
                                        //ProductDetails.TotalStock = Product.totalStock;
                                        ProductDetails.ModifyDate = HCoreHelper.GetGMTDateTime();
                                        if (Product.visibleOnNearby)
                                        {
                                            ProductDetails.StatusId = HelperStatus.Default.Active;
                                        }
                                        else
                                        {
                                            ProductDetails.StatusId = HelperStatus.Default.Inactive;
                                        }
                                        if (Product.deleted)
                                        {
                                            ProductDetails.StatusId = HelperStatus.Default.Blocked;
                                        }
                                        _HCoreContext.SaveChanges();
                                    }
                                    else
                                    {
                                        using (_HCoreContext = new HCoreContext())
                                        {
                                            _SCProduct = new SCProduct();
                                            _SCProduct.Guid = HCoreHelper.GenerateGuid();
                                            //_SCProduct.Sku = HCoreHelper.GenerateRandomNumber(10);
                                            _SCProduct.AccountId = MerchantDetails.Id;
                                            _SCProduct.Name = Product.name;
                                            _SCProduct.SystemName = HCoreHelper.GenerateSystemName(Product.name);
                                            //_SCProduct.MinimumAmount = Product.price;
                                            //_SCProduct.MaximumAmount = Product.price;
                                            //_SCProduct.MinimumQuantity = 0;
                                            //_SCProduct.MaximumQuantity = 0;
                                            //_SCProduct.ActualPrice = Product.price;
                                            //_SCProduct.SellingPrice = Product.price;
                                            //_SCProduct.AllowMultiple = 1;
                                            //_SCProduct.TotalStock = Product.totalStock;
                                            _SCProduct.CreateDate = HCoreHelper.GetGMTDateTime();
                                            _SCProduct.CreatedById = _Request.UserReference.AccountId;
                                            _SCProduct.StatusId = HelperStatus.Default.Active;
                                            if (Product.visibleOnNearby)
                                            {
                                                _SCProduct.StatusId = HelperStatus.Default.Active;
                                            }
                                            else
                                            {
                                                _SCProduct.StatusId = HelperStatus.Default.Inactive;
                                            }
                                            if (Product.deleted)
                                            {
                                                _SCProduct.StatusId = HelperStatus.Default.Blocked;
                                            }
                                            _HCoreContext.SCProduct.Add(_SCProduct);
                                            _HCoreContext.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Success, null, "HCP102", ResourceItems.HCP102);
                        #endregion
                    }
                    else
                    {
                        #region Send Response
                        return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP101", ResourceItems.HCP101);
                        #endregion
                    }
                }
            }
            catch (Exception _Exception)
            {
                #region  Log Exception
                HCoreHelper.LogException("SaveProduct", _Exception, _Request.UserReference);
                #endregion
                #region Send Response
                return HCoreHelper.SendResponse(_Request.UserReference, ResponseStatus.Error, null, "HCP500", ResourceItems.HCP500);
                #endregion
            }
            #endregion
        }
    }
}
