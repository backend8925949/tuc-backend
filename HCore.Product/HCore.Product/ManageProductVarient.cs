//==================================================================================
// FileName: ManageProductVarient.cs
// Author : Harshal Gandole
// Created On : 
// Description : Connector class to communicate between framework logic and controller.
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
// 20-09-2022      | Priya Chavadiya   : Added Comments to the methods
//
//==================================================================================

using System;
using HCore.Helper;
using HCore.Product.Framework;
using HCore.Product.Object;

namespace HCore.Product
{
    public class ManageProductVarient
    {
        FrameworkProductVarient _FrameworkProductVarient;
        /// <summary>
        /// Description: Saves the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveVarient(OProductVarient.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.SaveVarient(_Request);
        }
        /// <summary>
        /// Description: Updates the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateVarient(OProductVarient.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.UpdateVarient(_Request);
        }
        /// <summary>
        /// Description: Deletes the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteVarient(OProductVarient.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.DeleteVarient(_Request);
        }
        /// <summary>
        /// Description: Gets the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVarient(OProductVarient.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.GetVarient(_Request);
        }
        /// <summary>
        /// Description: Gets the varient.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVarient(OList.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.GetVarient(_Request);
        }





        /// <summary>
        /// Description: Saves the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse SaveVarientStock(OProductVarientStock.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.SaveVarientStock(_Request);
        }
        /// <summary>
        /// Description: Updates the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateVarientStock(OProductVarientStock.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.UpdateVarientStock(_Request);
        }
        /// <summary>
        /// Description: Updates the varient stock item.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse UpdateVarientStockItem(OProductVarientStock.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.UpdateVarientStockItem(_Request);
        }
        /// <summary>
        /// Description: Deletes the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse DeleteVarientStock(OProductVarientStock.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.DeleteVarientStock(_Request);
        }
        /// <summary>
        /// Description: Gets the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVarientStock(OProductVarientStock.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.GetVarientStock(_Request);
        }
        /// <summary>
        /// Description: Gets the varient stock.
        /// </summary>
        /// <param name="_Request">The request.</param>
        /// <returns>OResponse.</returns>
        public OResponse GetVarientStock(OList.Request _Request)
        {
            _FrameworkProductVarient = new FrameworkProductVarient();
            return _FrameworkProductVarient.GetVarientStock(_Request);
        }

    }
}
