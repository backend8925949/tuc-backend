//==================================================================================
// FileName: OOrder.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Product.Object
{
    public class OOrderManager
    {
        public class Request
        {
            public long OrderId { get; set; }
            public string? OrderKey { get; set; }
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? PaymentReference { get; set; }
            public string? Type { get; set; }
            public long CardId { get; set; }
            public string? Comment { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class NewOrder
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? Comment { get; set; }
            public string? DeliveryModeCode { get; set; }
            public long DealerId { get; set; }
            public long DealerLocationId { get; set; }
            public OrderAddress Address { get; set; }
            public List<OrderVarient> Products { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class OrderVarient
        {
            public long ProductId { get; set; }
            public long VarientId { get; set; }
            public long Quantity { get; set; }
        }
        public class OrderAddress
        {
            public long ReferenceId { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            //public string? Locality { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? Landmark { get; set; }
            public string? AlternateMobileNumber { get; set; }
            public string? City { get; set; }
            public long StateId { get; set; }
            public string? StateName { get; set; }
            public string? ZipCode { get; set; }
            public string? MapAddress { get; set; }
            public string? Instructions { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }






    }

    public class OOrder
    {
        public class Overview
        {
            public long Total { get; set; }
            public long New { get; set; }
            public long PendingConfirmation { get; set; }
            public long Confirmed { get; set; }

            public long Preparing { get; set; }
            public long Ready { get; set; }

            public long ReadyToPickUp { get; set; }
            public long OutForDelivery { get; set; }
            public long DeliveryFailed { get; set; }

            public long Cancelled { get; set; }
            public long Delivered { get; set; }
        }
        public class Rating
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public int RiderRating { get; set; }
            public string? RiderReview { get; set; }

            public int CustomerRating { get; set; }
            public string? CustomerReview { get; set; }

            public int DealerLocationRating { get; set; }
            public string? DealerLocationReview { get; set; }

            public OUserReference? UserReference { get; set; }


        }
        public class Request
        {
            public string? Source { get; set; }
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? OrderId { get; set; }
            public DateTime OpenDate { get; set; }
            public DateTime? CloseDate { get; set; }

            public int EPTime { get; set; }
            public string? Comment { get; set; }

            public long CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerEmailAddress { get; set; }
            public string? CustomerIconUrl { get; set; }


            public int TotalItem { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double OtherCharge { get; set; }
            public double DeliveryCharge { get; set; }
            public double DiscountAmount { get; set; }
            public double RewardAmount { get; set; }
            public double TotalAmount { get; set; }
            public string? UserComment { get; set; }

            public string? PaymentModeCode { get; set; }
            public string? PaymentModeName { get; set; }

            public string? PaymentStatusCode { get; set; }
            public string? PaymentStatusName { get; set; }
            public string? PaymentReference { get; set; }


            public long DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerDisplayName { get; set; }
            public string? DealerIconUrl { get; set; }


            public long DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public string? DealerLocationName { get; set; }
            public string? DealerLocationAddress { get; set; }
            public double DealerLocationLatitude { get; set; }
            public double DealerLocationLongitude { get; set; }

            public long AgentId { get; set; }
            public string? OrderDeliveryCode { get; set; }

            public DateTime? ExpectedDeliveryDate { get; set; }
            public DateTime? ActualDeliveryDate { get; set; }

            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public int CustomerRating { get; set; }
            public string? CustomerReview { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {

            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? OrderId { get; set; }
            public DateTime? OpenDate { get; set; }
            public DateTime? CloseDate { get; set; }

            public int? EPTime { get; set; }

            public long CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerEmailAddress { get; set; }
            public string? CustomerIconUrl { get; set; }


            public int TotalItem { get; set; }
            public double Amount { get; set; }
            public double Charge { get; set; }
            public double OtherCharge { get; set; }
            public double DeliveryCharge { get; set; }
            public double DiscountAmount { get; set; }
            public double RewardAmount { get; set; }
            public double TotalAmount { get; set; }

            public string? PaymentModeCode { get; set; }
            public string? PaymentModeName { get; set; }

            public string? PaymentStatusCode { get; set; }
            public string? PaymentStatusName { get; set; }
            public string? PaymentReference { get; set; }


            public long DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerDisplayName { get; set; }
            public string? DealerIconUrl { get; set; }
            public string? DealerContactNumber { get; set; }


            public long DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public string? DealerLocationName { get; set; }
            public string? DealerLocationAddress { get; set; }
            public double DealerLocationLatitude { get; set; }
            public double DealerLocationLongitude { get; set; }
            public string? DealerLocationContactNumber { get; set; }

            public long? AgentId { get; set; }
            public string? AgentKey { get; set; }
            public string? AgentDisplayName { get; set; }
            public string? AgentMobileNumber { get; set; }
            public string? AgentIconUrl { get; set; }

            public DateTime? ExpectedDeliveryDate { get; set; }
            public DateTime? ActualDeliveryDate { get; set; }

            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public int? RiderRating { get; set; }
            public string? RiderReview { get; set; }

            public int? CustomerRating { get; set; }
            public string? CustomerReview { get; set; }

            public int DealerLocationRating { get; set; }
            public string? DealerLocationReview { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public double? NinjaAmount { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public List<OrderItem> OrderItems { get; set; }
            public List<OrderActivity> OrderActivity { get; set; }
            public OrderAddress BillingAddress { get; set; }
            public OrderAddress ShippingAddress { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? OrderId { get; set; }


            public long? CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerMobileNumber { get; set; }

            public int TotalItem { get; set; }
            public double TotalAmount { get; set; }

            public long? DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerDisplayName { get; set; }
            public string? DealerIconUrl { get; set; }

            public string? DeliveryAddress { get; set; }

            public double? RiderAmount { get; set; }
            public double? RiderCommissionAmount { get; set; }
            public double? RiderTotalAmount { get; set; }

            public long? AgentId { get; set; }
            public string? AgentKey { get; set; }
            public string? AgentDisplayName { get; set; }
            public string? AgentMobileNumber { get; set; }
            public string? AgentIconUrl { get; set; }
            public int? RiderRating { get; set; }
            public long? DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public string? DealerLocationName { get; set; }
            public string? DealerLocationAddress { get; set; }
            public int? DealerLocationRating { get; set; }
            public DateTime? OrderDate { get; set; }
            public DateTime? DeliveryDate { get; set; }

            public DateTime? CreateDate { get; set; }


            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }


        }
        public class OrderItem
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? ProductId { get; set; }
            public string? ProductKey { get; set; }
            public string? ProductName { get; set; }

            public long? VarientId { get; set; }
            public string? VarientKey { get; set; }
            public string? VarientName { get; set; }
            public string? VarientIconUrl { get; set; }


            public double UnitPrice { get; set; }
            public int Quantity { get; set; }
            public double TotalAmount { get; set; }


            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class OrderAddress
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? AlternateMobileNumber { get; set; }
            public string? EmailAddress { get; set; }

            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }

            public string? LandMark { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }

            public string? MapAddress { get; set; }
            public long? AreaId { get; set; }
            public string? AreaName { get; set; }
            public long? CityId { get; set; }
            public string? CityName { get; set; }
            public long? StateId { get; set; }
            public string? StateName { get; set; }
            public string? CountryName { get; set; }
            public long? CountryId { get; set; }
            public string? ZipCode { get; set; }
            public string? Instruction { get; set; }


            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class OrderActivity
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? OrderId { get; set; }
            public string? OrderKey { get; set; }

            public long? OrderItemId { get; set; }
            public string? OrderItemKey { get; set; }

            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? SystemDescription { get; set; }
            public string? Comment { get; set; }


            public long? OrderStatusId { get; set; }
            public string? OrderStatusCode { get; set; }
            public string? OrderStatusName { get; set; }

            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
        }
    }



    public class OrderDetails
    {
        public class OUserCard
        {
            public long ReferenceId { get; set; }
            public string? Brand { get; set; }
            public string? Bin { get; set; }
            public string? End { get; set; }
            public string? ExpMonth { get; set; }
            public string? ExpYear { get; set; }
            public string? Channel { get; set; }
        }
        public class Order
        {
            public long OrderId { get; set; }
            public string? OrderReference { get; set; }
            public string? OrderNumber { get; set; }
            public DateTime? OpenDate { get; set; }
            public DateTime? CloseDate { get; set; }
            public int? EPTime { get; set; }
            public long? CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerEmailAddress { get; set; }


            public long? TotalItem { get; set; }
            public double? Amount { get; set; }
            public double? Charge { get; set; }
            public double? OtherCharge { get; set; }
            public double? DeliveryCharge { get; set; }
            public double? DiscountAmount { get; set; }
            public double? RewardAmount { get; set; }
            public double? TotalAmount { get; set; }

            public int? RiderRating { get; set; }
            public string? RiderReview { get; set; }

            public int? CustomerRating { get; set; }
            public string? CustomerReview { get; set; }

            public int? DealerLocationRating { get; set; }
            public string? DealerLocationReview { get; set; }

            public string? PaymentModeCode { get; set; }
            public string? PaymentModeName { get; set; }
            public string? PaymentStatusCode { get; set; }
            public string? PaymentStatusName { get; set; }
            public string? PaymentReference { get; set; }

            public long? DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerName { get; set; }
            public string? DealerIconUrl { get; set; }

            public long? DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public string? DealerLocationName { get; set; }
            public string? DealerLocationAddress { get; set; }
            public double? DealerLocationLatitude { get; set; }
            public double? DealerLocationLongitude { get; set; }









            public long? AgentId { get; set; }
            public string? AgentKey { get; set; }
            public string? AgentDisplayName { get; set; }
            public string? AgentMobileNumber { get; set; }
            public string? AgentEmailAddress { get; set; }
            public string? AgentIconUrl { get; set; }
            //public double? AgentRating { get; set; }



            public long? TransactionId { get; set; }





            public DateTime? ExpectedDeliveryDate { get; set; }
            public DateTime? ActualDeliveryDate { get; set; }

            public long? DeliveryTypeId { get; set; }
            public string? DeliveryTypeCode { get; set; }
            public string? DeliveryTypeName { get; set; }

            public long? PriorityTypeId { get; set; }
            public string? PriorityTypeCode { get; set; }
            public string? PriotityTypeName { get; set; }

            public string? OrderDeliveryCode { get; set; }


            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OrderAddress Address { get; set; }
            public List<OrderItem> OrderItems { get; set; }
            public List<OrderActivity> Activity { get; set; }
            public List<OUserCard> UserCards { get; set; }
        }
        public class OrderAddress
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? Landmark { get; set; }
            public string? AlternateMobileNumber { get; set; }
            public string? City { get; set; }
            public long StateId { get; set; }
            public string? StateName { get; set; }
            public string? ZipCode { get; set; }
            public string? MapAddress { get; set; }
            public string? Instructions { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            //public string? ReferenceKey { get; set; }
            //public string? Name { get; set; }
            //public string? MobileNumber { get; set; }
            //public string? Locality { get; set; }
            //public string? AddressLine1 { get; set; }
            //public string? AddressLine2 { get; set; }
            //public string? AddressLine3 { get; set; }
            //public string? LandMark { get; set; }
            //public string? AlternateMobileNumber { get; set; }
            //public string? City { get; set; }
            //public long StateId { get; set; }
            //public string? StateName { get; set; }
            //public long CountryId { get; set; }
            //public string? CountryName { get; set; }
            //public string? ZipCode { get; set; }
            //public string? Comment { get; set; }
            //public double? Latitude { get; set; }
            //public double? Longitude { get; set; }
        }
        public class OrderItem
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long ProductId { get; set; }
            public string? ProductKey { get; set; }
            public string? ProductName { get; set; }
            public string? ProductIconUrl { get; set; }
            public long VarientId { get; set; }
            public string? VarientKey { get; set; }
            public string? VarientName { get; set; }
            public string? VarientIconUrl { get; set; }

            public long Quantity { get; set; }
            public double UnitPrice { get; set; }
            public double TotalAmount { get; set; }


            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
            public List<OrderActivity> Activity { get; set; }
        }
        public class OrderActivity
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? Comment { get; set; }
            public long? OrderStatusId { get; set; }
            public string? OrderStatusCode { get; set; }
            public string? OrderStatusName { get; set; }
            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
            public DateTime? ModifyDate { get; set; }
            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }
        }
    }



    public class ONinjaOrderDetails
    {
        public class Order
        {
            public long OrderId { get; set; }
            public string? OrderReference { get; set; }
            public string? OrderNumber { get; set; }
            public DateTime? OpenDate { get; set; }
            public int? EPTime { get; set; }
            public long? CustomerId { get; set; }
            public string? CustomerKey { get; set; }
            public string? CustomerDisplayName { get; set; }
            public string? CustomerMobileNumber { get; set; }
            public string? CustomerEmailAddress { get; set; }

            public long? TotalItem { get; set; }
            public double? TotalAmount { get; set; }
            public double? RiderAmount { get; set; }
            public double? RiderCommissionAmount { get; set; }
            public double? RiderTotalAmount { get; set; }

            public long? DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerName { get; set; }
            public string? DealerIconUrl { get; set; }

            public long? DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public string? DealerLocationName { get; set; }
            public string? DealerLocationAddress { get; set; }
            public double? DealerLocationLatitude { get; set; }
            public double? DealerLocationLongitude { get; set; }

            public long? AgentId { get; set; }
            public string? AgentKey { get; set; }

            public DateTime? ExpectedDeliveryDate { get; set; }
            public DateTime? ActualDeliveryDate { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public OrderAddress Address { get; set; }
            public List<OrderActivity> OrderActivity { get; set; }
        }
        public class OrderAddress
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Name { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? AddressLine1 { get; set; }
            public string? AddressLine2 { get; set; }
            public string? Landmark { get; set; }
            public string? AlternateMobileNumber { get; set; }
            public string? CityName { get; set; }
            public string? AreaName { get; set; }
            public long StateId { get; set; }
            public string? StateName { get; set; }
            public string? ZipCode { get; set; }
            public string? MapAddress { get; set; }
            public string? Instructions { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }
        public class OrderActivity
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? OrderId { get; set; }
            public string? OrderKey { get; set; }

            public long? OrderItemId { get; set; }
            public string? OrderItemKey { get; set; }

            public string? Title { get; set; }
            public string? Description { get; set; }
            public string? SystemDescription { get; set; }
            public string? Comment { get; set; }


            public long? OrderStatusId { get; set; }
            public string? OrderStatusCode { get; set; }
            public string? OrderStatusName { get; set; }

            public DateTime CreateDate { get; set; }
            public long? CreatedById { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }
        }

    }

}
