//==================================================================================
// FileName: OProductVarient.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;
namespace HCore.Product.Object
{
    public class OProductVarient
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }


            public long ProductId { get; set; }
            public string? ProductKey { get; set; }

            public string? Sku { get; set; }

            public string? Name { get; set; }



            public string? Description { get; set; }
            public string? ShortDescription { get; set; }
            //public OStorageContent IconContent { get; set; }
            //public OStorageContent PosterContent { get; set; }


         
            public long MinimumQuantity { get; set; }
            public long MaximumQuantity { get; set; }

            public long LowQuantityThreshold { get; set; }

            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public int AllowMultiple { get; set; }

            public string? ReferenceNumber { get; set; }
            public double RewardPercentage { get; set; }
            public long TotalStock { get; set; }
            public double MaximumRewardAmount { get; set; }
            public string? StatusCode { get; set; }
            public List<DealerLocation> DealerLocations { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class DealerLocation
        {
            public long DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }

            public long Quantity { get; set; }
            public long LowQuantityThreshold { get; set; }
            public long MinimumQuantity { get; set; }
            public long MaximumQuantity { get; set; }

            public double MinimumAmount { get; set; }
            public double MaximumAmount { get; set; }

            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public int AllowMultiple { get; set; }

            public double RewardPercentage { get; set; }
            public double MaximumRewardAmount { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? BrandId { get; set; }
            public string? BrandKey { get; set; }
            public string? BrandName { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public long? DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerDisplayName { get; set; }

            public long ProductId { get; set; }
            public string? ProductKey { get; set; }
            public string? ProductName { get; set; }

            public string? Sku { get; set; }
            public string? Name { get; set; }
            public string? IconUrl { get; set; }
            public string? ShortDescription { get; set; }

            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public long TotalStock { get; set; }

            public double RewardPercentage { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long ProductStatusId { get; set; }
            public string? ProductStatusCode { get; set; }
            public string? ProductStatusName { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long? BrandId { get; set; }
            public string? BrandKey { get; set; }
            public string? BrandName { get; set; }

            public long ProductId { get; set; }
            public string? ProductKey { get; set; }
            public string? ProductName { get; set; }

            public string? Sku { get; set; }
            public string? Name { get; set; }


            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }

            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName
            {
                get; set;
            }
            public long? LowQuantityThreshold { get; set; }
            public string? Description { get; set; }
            public string? ShortDescription { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public double? MinimumAmount { get; set; }
            public double? MaximumAmount { get; set; }

            public long? MinimumQuantity { get; set; }
            public long? MaximumQuantity { get; set; }

            public long TotalStock { get; set; }


            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public int AllowMultiple { get; set; }

            public string? ReferenceNumber { get; set; }
            public double RewardPercentage { get; set; }
            public double MaximumRewardAmount { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }


            public long ProductStatusId { get; set; }
            public string? ProductStatusCode { get; set; }
            public string? ProductStatusName { get; set; }
        }
      }
    public class OProductVarientSpecification
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long VarientId { get; set; }
            public string? VarientKey { get; set; }

            public string? Title { get; set; }
            public string? Value { get; set; }
            public string? StatusCode { get; set; }
            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Title { get; set; }
            public string? Value { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Title { get; set; }
            public string? Value { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OProductVarientGallery
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long VarientId { get; set; }
            public string? VarientKey { get; set; }
            public string? StatusCode { get; set; }
            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ImageUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? ImageUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
    public class OProductVarientStock
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }

            public long VarientId { get; set; }
            public string? VarientKey { get; set; }

            public long Quantity { get; set; }
            public long LowQuantityThreshold { get; set; }
            public long MinimumQuantity { get; set; }
            public long MaximumQuantity { get; set; }

            public double MinimumAmount { get; set; }
            public double MaximumAmount { get; set; }

            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public int AllowMultiple { get; set; }

            public double RewardPercentage { get; set; }
            public double MaximumRewardAmount { get; set; }

            public string? StatusCode { get; set; }

            public string? OperationType { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public long VarientId { get; set; }
            public string? VarientKey { get; set; }

            public long? DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerDisplayName { get; set; }

            public long? DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public string? DealerLocationName { get; set; }
            public string? DealerLocationAddress { get; set; }


            public long? ProductId { get; set; }


            public long Quantity { get; set; }
            public long LowQuantityThreshold { get; set; }
            public long MinimumQuantity { get; set; }
            public long MaximumQuantity { get; set; }

            public double MinimumAmount { get; set; }
            public double MaximumAmount { get; set; }

            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public int AllowMultiple { get; set; }

            public double RewardPercentage { get; set; }
            public double MaximumRewardAmount { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

  

        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }



            public long ProductId { get; set; }
            public string? ProductKey { get; set; }
            public string? ProductName { get; set; }

            public string? Sku { get; set; }
            public string? Name { get; set; }

            public string? Description { get; set; }
            public string? ShortDescription { get; set; }
            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }


            public double? MinimumAmount { get; set; }
            public double? MaximumAmount { get; set; }

            public long? MinimumQuantity { get; set; }
            public long? MaximumQuantity { get; set; }


            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public int AllowMultiple { get; set; }

            public string? ReferenceNumber { get; set; }
            public double RewardPercentage { get; set; }
            public double MaximumRewardAmount { get; set; }

            public OUserReference? UserReference { get; set; }
        }
     
        //public class Save
        //{


        //    public class Store
        //    {
        //        public long ReferenceId { get; set; }
        //        public string? ReferenceKey { get; set; }
        //        public string? Name { get; set; }
        //        public string? Address { get; set; }
        //        public double? Latitude { get; set; }
        //        public double? Longitude { get; set; }
        //    }
        //}
        //public class Update
        //{
        //    public class Request
        //    {
        //        public long AccountId { get; set; }
        //        public string? AccountKey { get; set; }


        //        public long ReferenceId { get; set; }
        //        public string? ReferenceKey { get; set; }

        //        public string? Sku { get; set; }
        //        public string? CategoryKey { get; set; }
        //        public string? SubCategoryKey { get; set; }
        //        public string? Name { get; set; }
        //        public string? SystemName { get; set; }
        //        public string? Description { get; set; }

        //        public double MinumumAmount { get; set; }
        //        public double MaximumAmount { get; set; }

        //        public long MinimumQuantity { get; set; }
        //        public long MaximumQuantity { get; set; }

        //        public double ActualPrice { get; set; }
        //        public double SellingPrice { get; set; }
        //        public int AllowMultiple { get; set; }

        //        public string? ReferenceNumber { get; set; }
        //        public string? HashTag { get; set; }

        //        public double RewardPercentage { get; set; }
        //        public double MaximumRewardAmount { get; set; }

        //        public long TotalStock { get; set; }

        //        public string? StatusCode { get; set; }
        //        public OStorageContent IconContent { get; set; }
        //        public List<Store> stores { get; set; }
        //        public OUserReference? UserReference { get; set; }
        //    }
        //    public class Store
        //    {
        //        public long ReferenceId { get; set; }
        //        public string? ReferenceKey { get; set; }
        //        public string? Name { get; set; }
        //        public string? Address { get; set; }
        //        public double? Latitude { get; set; }
        //        public double? Longitude { get; set; }
        //    }
        //}
        //public class Details
        //{
        //    public class Request
        //    {
        //        public long ReferenceId { get; set; }
        //        public string? ReferenceKey { get; set; }
        //        public OUserReference? UserReference { get; set; }
        //    }
        //    public class Response
        //    {
        //       public class List
        //        {
        //            public long ReferenceId { get; set; }
        //            public string? ReferenceKey { get; set; }
        //            public string? Sku { get; set; }
        //            public string? CategoryKey { get; set; }
        //            public string? CategoryName { get; set; }
        //            public string? Name { get; set; }
        //            public double SellingPrice { get; set; }
        //            public double RewardPercentage { get; set; }
        //            public long TotalStock { get; set; }
        //            public string? IconUrl { get; set; }
        //            public DateTime CreateDate { get; set; }
        //            public string? CreatedByKey { get; set; }
        //            public string? CreatedByDisplayName { get; set; }

        //            public DateTime? ModifyDate { get; set; }
        //            public string? ModifyByKey { get; set; }
        //            public string? ModifyByDisplayName { get; set; }


        //            public int StatusId { get; set; }
        //            public string? StatusCode { get; set; }
        //            public string? StatusName { get; set; }
        //        }
        //    }
        //}

    }


}
