//==================================================================================
// FileName: OBrand.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;
namespace HCore.Product.Object
{
    public class OBrand
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? DealerKey { get; set; }
            public string? PrimaryCategoryKey { get; set; }
            public string? SecondaryCategoryKey { get; set; }

            public string? Name { get; set; }
            public string? Description { get; set; }
            public string? StatusCode { get; set; }
            public OStorageContent IconContent { get; set; }
            public OStorageContent PosterContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? PrimaryCategoryKey { get; set; }
            public string? PrimaryCategoryName { get; set; }
            public string? SecondaryCategoryKey { get; set; }
            public string? SecondaryCategoryName { get; set; }

            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? PrimaryCategoryKey { get; set; }
            public string? PrimaryCategoryName { get; set; }



            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? IconUrl { get; set; }
            public string? PosterUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }
}
