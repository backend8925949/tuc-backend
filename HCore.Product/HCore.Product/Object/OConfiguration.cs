//==================================================================================
// FileName: OConfiguration.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Product.Object
{
    //public class OConfigurationDetails
    //{
    //    public bool error { get; set; }
    //    public string? Value { get; set; }
    //}
    public class OTUCAccountConfiguration
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DataTypeCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? Value { get; set; }
            public string? StatusCode { get; set; }

            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }


            public string? DataTypeCode { get; set; }
            public string? DataTypeName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? OriginalValue { get; set; }
            public string? CustomValue { get; set; }

            public string? ImageUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
    }
    public class OTUCConfiguration
    {
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? DataTypeCode { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? Value { get; set; }
            public string? StatusCode { get; set; }

            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }


            public string? DataTypeCode { get; set; }
            public string? DataTypeName { get; set; }
            public string? Name { get; set; }
            public string? SystemName { get; set; }
            public string? Description { get; set; }
            public string? Value { get; set; }

            public string? ImageUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }


            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
    }

    public class OTUCConfigurationValue
    {
        public class Request
        {
            public long AccountId { get; set; }
            public string? AccountKey { get; set; }
            public long ConfigurationId { get; set; }
            public string? ConfigurationKey { get; set; }

            public string? Comment { get; set; }
            public string? Value { get; set; }
            public OStorageContent ImageContent { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            public string? Comment { get; set; }
            public string? Value { get; set; }

            public string? ImageUrl { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

        }
    }
}
