//==================================================================================
// FileName: OSoftCom.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using HCore.Helper;

namespace HCore.Product.Object.Integration
{
    public class OSoftCom
    {
        public class ProductUpdate
        {
            public string? merchantCode { get; set; }
            public string? eventType { get; set; }
            public Product Product { get; set; }
            public List<Product> Products { get; set; }
            public string? url { get; set; }
            public OUserReference? UserReference { get; set; }
        }
        public class Product
        {
            public bool visibleOnNearby { get; set; }
            public string? _id { get; set; }
            public bool deleted { get; set; }
            public string? business { get; set; }
            public string? name { get; set; }
            public double price { get; set; }
            public string? businessCounter { get; set; }
            public string? counter { get; set; }
            public string? businessSKU { get; set; }
            public long totalStock { get; set; }
            public DateTime createdAt { get; set; }
            public DateTime updatedAt { get; set; }
            public long __v { get; set; }
            public List<ProductStore> stores { get; set; }
        }
        public class ProductStore
        {
            public string? _id { get; set; }
            public string? storeID { get; set; }
            public long stock { get; set; }
            public string? name { get; set; }
            public string? address { get; set; }
            public string? emailaddress { get; set; }
        }
    }
}
