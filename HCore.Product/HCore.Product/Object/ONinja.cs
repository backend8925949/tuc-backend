//==================================================================================
// FileName: ONinja.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using HCore.Helper;

namespace HCore.Product.Object
{
    public class ONinja
    {
        public class ONinjaTransferRequest
        {
            public double Amount { get; set; }
            public string? Comment { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }

            public string? VehicleNumber { get; set; }

            public string? Reference1Name { get; set; }
            public string? Reference1ContactNumber { get; set; }

            public string? Reference2Name { get; set; }
            public string? Reference2ContactNumber { get; set; }

            public string? FacebookUrl { get; set; }
            public string? TwitterUrl { get; set; }
            public string? InstagramUrl { get; set; }
            public string? LinkedInUrl { get; set; }
            public string? Comment { get; set; }

            public OStorageContent ImageContent { get; set; }
            public OStorageContent ProofContent { get; set; }
            public OStorageContent UtilityContent { get; set; }
            public string? StatusCode { get; set; }


            public int CountryId { get; set; }
            public long RegionId { get; set; }
            public long CityId { get; set; }
            public long CityArea1Id{ get; set; }
            public long CityArea2Id { get; set; }
            public long CityArea3Id { get; set; }
            public long CityArea4Id { get; set; }

            public string? BankName { get; set; }
            public string? BankAccountNumber { get; set; }
            public string? BankAccountName { get; set; }
            public string? BankCode { get; set; }


            public OUserReference? UserReference { get; set; }
        }


        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string?   Address { get; set; }
            public string? VehicleNumber { get; set; }
            public string? Reference1Name { get; set; }
            public string? Reference1ContactNumber { get; set; }
            public string? Reference2Name { get; set; }
            public string? Reference2ContactNumber { get; set; }
            public string? PhotoUrl { get; set; }
            public string? IdProofUrl { get; set; }
            public string? AddressProofUrl { get; set; }

            public DateTime? CreateDate { get; set; }
            public DateTime? ModifyDate { get; set; }

            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }



            public long? CountryId { get; set; }
            public string? CountryName { get; set; }

            public long? RegionId { get; set; }
            public string? RegionName { get; set; }


            public long? CityId { get; set; }
            public string? CityName { get; set; }

            public long? CityArea1Id { get; set; }
            public string? CityArea1Name { get; set; }

            public long? CityArea2Id { get; set; }
            public string? CityArea2Name { get; set; }

            public long? CityArea3Id { get; set; }
            public string? CityArea3Name { get; set; }

            public long? CityArea4Id { get; set; }
            public string? CityArea4Name { get; set; }
        }

        public class Transactions
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? ModeCode { get; set; }
            public string? ModeName { get; set; }

            public string? DealerDisplayName { get; set; }
            public string? DealerIconUrl { get; set; }

            public string? TypeCode { get; set; }
            public string? TypeName { get; set; }

            public double TotalAmount { get; set; }

            public DateTime? CreateDate { get; set; }
            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }

        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? FirstName { get; set; }
            public string? LastName { get; set; }
            public string? MobileNumber { get; set; }
            public string? EmailAddress { get; set; }
            public string? Address { get; set; }
            public string? VehicleNumber { get; set; }
            public string? Reference1Name { get; set; }
            public string? Reference1ContactNumber { get; set; }
            public string? Reference2Name { get; set; }
            public string? Reference2ContactNumber { get; set; }
            public string? PhotoUrl { get; set; }
            public string? IdProofUrl { get; set; }
            public string? AddressProofUrl { get; set; }

            public DateTime? CreateDate { get; set; }
            public DateTime? ModifyDate { get; set; }

            public long? ModifyById { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }



            public long? CountryId { get; set; }
            public string? CountryName { get; set; }

            public long? RegionId { get; set; }
            public string? RegionName { get; set; }


            public long? CityId { get; set; }
            public string? CityName { get; set; }

            public long? CityArea1Id { get; set; }
            public string? CityArea1Name { get; set; }

            public long? CityArea2Id { get; set; }
            public string? CityArea2Name { get; set; }

            public long? CityArea3Id { get; set; }
            public string? CityArea3Name { get; set; }

            public long? CityArea4Id { get; set; }
            public string? CityArea4Name { get; set; }

            public string? FacebookUrl { get; set; }
            public string? TwitterUrl { get; set; }
            public string? InstagramUrl { get; set; }
            public string? LinkedInUrl { get; set; }

            public string? BankName { get; set; }
            public string? BankAccountNumber { get; set; }
            public string? BankAccountName { get; set; }
            public string? BankCode { get; set; }

        }
    }
}
