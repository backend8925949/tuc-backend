//==================================================================================
// FileName: OProduct.cs
// Author : Harshal Gandole
// Created On : 
// Description : Class for defining database related functions
//
// COPYRIGHT NOTICE: (c) 2018-2022 Connected Analytics LLC.  All rights reserved.
//
// Revision History:
// Date             : Changed By        : Comments
// ---------------------------------------------------------------------------------
//                 | Harshal Gandole   : Created New File
//
//
//==================================================================================

﻿using System;
using System.Collections.Generic;
using GeoCoordinatePortable;
using HCore.Helper;
namespace HCore.Product.Object
{
    public class OProduct
    {

        public class Overview
        {
            public long Total { get; set; }
            public long InStock { get; set; }
            public long OutOfStock { get; set; }
            public long LowStock { get; set; }
            public long UnavailableProduct { get; set; }
        }
        public class Request
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }
            // Product Fields
            public long DealerId { get; set; }
            public string? DealerKey { get; set; }

            public string? Name { get; set; }
            public string? VarientName { get; set; }
            public string? Description { get; set; }
            public string? CategoryKey { get; set; }
            public OStorageContent IconContent { get; set; }
            public double RewardPercentage { get; set; }
            public string? StatusCode { get; set; }
            // Product Varient Fields
            public string? Sku { get; set; }

            public long MinimumQuantity { get; set; }
            public long MaximumQuantity { get; set; }

            public double ActualPrice { get; set; }
            public double SellingPrice { get; set; }

            public string? ReferenceNumber { get; set; }
            public string? BarcodeNumber { get; set; }
            public List<DealerLocation> DealerLocations { get; set; }
            public OUserReference? UserReference { get; set; }
        }

        public class DealerLocation
        {
            public long DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public long Quantity { get; set; }
        }

        public class List
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? ShortDescription { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public string? IconUrl { get; set; }
            public string? ReferenceNumber { get; set; }

            public long VarientId { get; set; }
            public string? VarientKey { get; set; }

            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }


            internal double? TActualPrice { get; set; }
            internal double? TSellingPrice { get; set; }

            public DateTime CreateDate { get; set; }
            //public string? CreatedByKey { get; set; }
            //public string? CreatedByDisplayName { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }

            public long? DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerName { get; set; }
            public string? DealerIconUrl { get; set; }

            public long TotalStock { get; set; }

            public OUserReference? UserReference { get; set; }
        }
        public class DealerList
        {
            public long DealerId { get; set; }
            public string? DealerKey { get; set; }
            public long DealerLocationId { get; set; }
            public string? DealerLocationKey { get; set; }
            public string? DealerName { get; set; }
            public string? DealerIconUrl { get; set; }
            public string? DealerLocationName { get; set; }
            public string? DealerLocationAddress { get; set; }
            public double DealerLocationLatitude { get; set; }
            public double DealerLocationLongitude { get; set; }

            public long Products { get; set; }

            internal GeoCoordinate Location { get; set; }
            public double Distance { get; set; }
        }
        public class Details
        {
            public long ReferenceId { get; set; }
            public string? ReferenceKey { get; set; }

            public string? Name { get; set; }
            public string? ShortDescription { get; set; }
            public string? BrandKey { get; set; }
            public string? BrandName { get; set; }
            public string? CategoryKey { get; set; }
            public string? CategoryName { get; set; }
            public string? IconUrl { get; set; }
            public string? ReferenceNumber { get; set; }

            public long TotalStock { get; set; }

            public long VarientId { get; set; }
            public string? VarientKey { get; set; }

            public double? ActualPrice { get; set; }
            public double? SellingPrice { get; set; }


            public long? AccountId { get; set; }
            public string? AccountKey { get; set; }
            public string? AccountDisplayName { get; set; }
            public string? SubCategoryKey { get; set; }
            public string? SubCategoryName { get; set; }

            public string? Description { get; set; }


            public string? HashTag { get; set; }

            public DateTime CreateDate { get; set; }
            public string? CreatedByKey { get; set; }
            public string? CreatedByDisplayName { get; set; }

            public DateTime? ModifyDate { get; set; }
            public string? ModifyByKey { get; set; }
            public string? ModifyByDisplayName { get; set; }

            public long? DealerId { get; set; }
            public string? DealerKey { get; set; }
            public string? DealerName { get; set; }
            public string? DealerIconUrl { get; set; }

            public int StatusId { get; set; }
            public string? StatusCode { get; set; }
            public string? StatusName { get; set; }
        }
    }

}
