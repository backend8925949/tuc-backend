FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
WORKDIR /app
# Copy csproj and restore as distinct layers
COPY ./ ./backend
RUN cd backend && ls
# RUN dotnet restore  ./backend/HCore.Data/HCore.Data
# RUN dotnet restore  ./backend/HCore.Helper/HCore.Helper
# RUN dotnet restore  ./backend/HCore.Data.Store/HCore.Data.Store
# RUN dotnet restore  ./backend/HCore.Analytics/HCore.Analytics
# RUN dotnet restore  ./backend/HCore.Operations/HCore.Operations
# RUN dotnet restore  ./backend/HCore.Integration/HCore.Integration
# RUN dotnet restore  ./backend/HCore.TUC.Core.Transaction/HCore.TUC.Core.Transaction
# RUN dotnet restore  ./backend/HCore/HCore 
# RUN dotnet restore  ./backend/HCore.ThankU/HCore.ThankU
# RUN dotnet restore  ./backend/HCore.TUC.Plugins/HCore.TUC.Plugins
# RUN dotnet restore  ./backend/HCore.TUC.Connect/HCore.TUC.Connect
# RUN dotnet restore  ./backend/HCore.ThankUCash/HCore.ThankUCash
# RUN dotnet restore  ./backend/HCore.ThankUCash.Gateway/HCore.ThankUCash.Gateway
# RUN dotnet restore  ./backend/HCore.Api.ThankUCash/HCore.Api.ThankUCash
# RUN dotnet restore  ./backend/HCore.Bot/HCore.Bot
# RUN dotnet restore  ./backend/HCore.TUC.SmsCampaign/HCore.TUC.SmsCampaign
# RUN dotnet restore  ./backend/HCore.TUC.Data.Store/HCore.TUC.Data.Store
# RUN dotnet restore  ./backend/HCore.Api/HCore.Api
# RUN dotnet clean    ./backend/HCore.Data/HCore.Data
RUN dotnet build    ./backend/HCore.Data/HCore.Data  -c=Release
RUN dotnet build    ./backend/HCore.Helper/HCore.Helper  -c=Release
RUN dotnet build    ./backend/HCore.Data.Store/HCore.Data.Store  -c=Release
RUN dotnet build    ./backend/HCore.Analytics/HCore.Analytics  -c=Release
RUN dotnet build    ./backend/HCore.Bot/HCore.Bot  -c=Release
RUN dotnet build    ./backend/HCore.Operations/HCore.Operations  -c=Release
RUN dotnet build    ./backend/HCore.Integration/HCore.Integration  -c=Release
RUN dotnet build    ./backend/HCore.TUC.Core.Transaction/HCore.TUC.Core.Transaction  -c=Release
RUN dotnet build    ./backend/HCore/HCore -c=Release
RUN dotnet build    ./backend/HCore.ThankU/HCore.ThankU  -c=Release
RUN dotnet build    ./backend/HCore.TUC.Plugins/HCore.TUC.Plugins  -c=Release
RUN dotnet build    ./backend/HCore.TUC.Connect/HCore.TUC.Connect  -c=Release
RUN dotnet build    ./backend/HCore.ThankUCash/HCore.ThankUCash  -c=Release
RUN dotnet build    ./backend/HCore.ThankUCash.Gateway/HCore.ThankUCash.Gateway  -c=Release
RUN dotnet build    ./backend/HCore.Api.ThankUCash/HCore.Api.ThankUCash  -c=Release
RUN dotnet build    ./backend/HCore.TUC.SmsCampaign/HCore.TUC.SmsCampaign  -c=Release
RUN dotnet build    ./backend/HCore.TUC.Data.Store/HCore.TUC.Data.Store  -c=Release
RUN dotnet build    ./backend/HCore.Api/HCore.Api  -c=Release
# Copy everything else and build
RUN dotnet publish -c Test -o out ./backend/HCore.Api/HCore.Api/HCore.Api.csproj
# build command: docker build -f dockerfile.builder . -t webconnect-builder